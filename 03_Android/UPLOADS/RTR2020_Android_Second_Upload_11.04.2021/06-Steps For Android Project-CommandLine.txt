00) Create A Directory ( Other Than The Default Android Studio's Project Directory ) 
    To Hold Our Android Projects, Say 'MyAndroid'. 
    Inside It Create Another Directory, Say '_Template'.
    And Copy All Directories And Files As It Is To It.

01) Delete following files and directoris from Explorer ( as we don't need them ) :

    a) 2 directories from project's root directory :
        i) .gradle
       ii) .idea

    b) 2 files :
        i) project's root directory\.gitignore
       ii) app\.gitignore

    c) 2 directories from app/src :
        i)'androidTest'
       ii) 'test'

    d) 1 directory from app/src/main/res :

       values-night

    e) 2 files from app/src/main/res/values directory :
        i) colors.xml
       ii) themes.xml
       [ But AndroidManifest.xml needs to be updated accordingly. See below in step 5 ]

02) From Command Prompt, 'cd' to the '_Template' directory.  

03) Edit app/build.gradle file :

    In 'android > defaultConfig' block, following statements can be removed,

    applicationId "<domain name in reverse order>"
    versionCode 1
    versionName "1.0"
    testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"

    So keep 2 version related statements only.
    Change minSdkVersion from 16 to 18.

    [ 
      Note : For 'minSdkVersion', the number should be less than the targetSdkVersion.
             We used 18, because OpenGL-ES 3.0 support starts from Android SDK 18
    ]

04) copy <youricon> and <youricon_round> to 5 directories : mipmap_hdpi, mipmap_mdpi, 
    mipmap_xhdpi, mipmap.xxhdpi and mipmap.xxxhdpi directories. 
    Keep original default android's png files as they are.

    * Don't Do The Same For "mipmap-anydpi-v26" Directory.

05) Do following steps :

    a) From <application> block, remove/replace following 3 un-necessary tags :

        Remove : android:allowBackup="true"

        Remove : android:supportsRtl="true"

        Replace : [ Because We Removed app/src/main/res/values/themes.xml and associated colors.xml ]

        android:theme="@style/Theme.Window

        By :

        android:theme="@style/Theme.AppCompat">

        [ Theme.AppCompat Is A Gray/Black Colored While Theme.AppCompat.Light Is White/Light Colored Theme ]

        [ IMPORTANT : but then do not forget to give '>' to relevant last statement ]

    b) change 'android:icon=@mipmap/ic_launcher' to 
              'android:icon=@mipmap/<youricon>'
    c) change 'android:roundIcon=@mipmap/ic_launcher_round' to 
              'android:roundIcon=@mipmap/<youricon>'

       * Your Icon images should be 72x72.
         Use Currently Present Icons i.e. ic_launcher And ic_launcher_round as guideline.

06) Edit app/src/main/res/layout/activity_main.xml, experiment with the 'android:text' tag value
    to print something other than "Hello World". But finally keep it as follows because we don't 
    need so many features in it for our OpenGL purpose.

<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="vertical"
    android:layout_width="fill_parent"
    android:layout_height="fill_parent"
  >
  <TextView
    android:layout_width="fill_parent"
    android:layout_height="wrap_content"
    android:text=""
  />
</LinearLayout>

07) Edit 'app/src/main/res/values/strings.xml' to give a displayable name to the application on 
    emulator or device.

    On Emulator/Device App Gets Installed By The Name That Is Specified In This File, 
    Which Is By Default The Name Of Project. But If We Want Our App To Get Installed 
    By Some Different Meaningful Name, We Can Change It In This File Under <resources> Block 
    At <app_name> Tag, Irrespective Of Name Of Project.

08) Project Directory/local.properties File : 
    Should contain SDK Or NDK Directory Paths : sdk.dir= And ndk.dir
    Keep both or keep relevant.

09) IMPORTANT : Gradle defines a Domain Specific Language (DSL).
                The 'buildscript' tag in project root directory's 'build.gradle' file is part of
                that DSL. So the 'dependancies' block in this file, which contain a line :
                classpath 'com.android.tools.build:gradle:<some_version>'
                must be resolved. This is gradle's android plugin and gets updated very frequently
                Time to time.

    * So during build following process, internet MUST be enabled ( i.e. We must be online ) or else
      Build process may fail.

    * When Gradle builds this project the first time, the plugin will be downloaded and cached, 
      so this task is only performed once. 

10) Edit app/src/main/java/<reverse_package_name>/MainActivity.java
    and comment-out following line :

    setContentView(R.layout.activity_main);

11) Assuming we are in project's directory, give following command :
    [ IMP : During Following Steps Internet Download Installation May Take Place At First Time ]

    > gradlew.bat clean

    > gradlew.bat build

    [ When needed Use : gradlew.bat --version ]

12) If everything goes well, the 'app\build\outputs\apk\debug' location will have the .apk File
    say 'app-debug.apk'.

13) Check Whether Emulator Or Device Is/Are Online Or Not By :

    > adb devices

    [ Our Connected Device ID Should Be Visible ]

14) To Deploy :
    [ Don't Forget To Uninstall The Same Named, Previously Deploied APK From The Device ]

    > adb -d install -r app\build\outputs\apk\debug\<apk file>

15) For debugging :
    ===============
    Open A New Console/Command Promp ( Need not to 'cd' in project's directory ) For 
    Monitoring Android Device Log And Check Log For Output Of :
    Any System.out.println() With ‘VDG:’ Tag, Used In Java Code. 
   
    > adb logcat | findstr /i VDG:

adb logcat -e HRH

    Where ‘VDG:’ Is Used As Tag To Locate Output In Debug Info On Console.

    * When Needed Use : 

    > adb logcat -c

    To Clear/Flush The Log

    * And Use Ctrl+C To Abort The ‘adb logcat’
