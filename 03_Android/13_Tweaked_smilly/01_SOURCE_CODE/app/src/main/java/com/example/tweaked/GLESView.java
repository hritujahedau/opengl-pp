package com.example.tweaked;

import androidx.appcompat.widget.AppCompatTextView;

import android.view.MotionEvent;

import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;

import android.content.Context;
import android.graphics.Color;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils;
import android.view.Gravity;

import android.opengl.GLSurfaceView;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32;
import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.Arrays;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private GestureDetector gestureDetector;
	
	private final Context context;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
	private int[] vao_rectangle = new int[1];
	private int[] vbo_rectangle_position = new int[1];
	private int[] vbo_rectangle_texcoord = new int[1];

	private int mvpUniform = 0;
	private int textureSamplerUniform = 0;
	private int isColorUniform = 0;
	
	private int[] tweaked_texture = new int[1];
	private float sqaureTexcoord_0[] = new float[] { 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f };
	private float sqaureTexcoord_1[] = new float[] { 0.5f, 0.5f, 0.0f, 0.5f, 0.0f, 0.0f, 0.5f, 0.0f };
	private float sqaureTexcoord_2[] = new float[] { 2.0f, 2.0f, 0.0f, 2.0f, 0.0f, 0.0f, 2.0f, 0.0f };
	private float sqaureTexcoord_3[] = new float[] { 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f };
	private float currentTexcoord[] = sqaureTexcoord_0;
	private static int isColor = 1;

	private float perspectiveProjectionMatrix[] = new float[16];

	private static float rotateByAngle = 0.0f;

	int width, hight;
	
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		
		context = drawingContext;
		
		// according to EGLContext to current supported version of OpenGL ES
		setEGLContextClientVersion(3);
		
		//set Render for drawing on the GLSurfaceView
		setRenderer(this);
		
		// Render the vuew only when there is change in the drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
				
		gestureDetector = new GestureDetector(context, this, null, false);
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// OpenGL-ES version check
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("HRH: "+ version);
		
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("HRH:  GLSL Version = " + glslVersion);
		
		initialize(gl);
	}
	
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int hight)
	{
		resize(width, hight);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
		draw();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		System.out.println("HRH: Double Tap");
		//setText("Double Tap");
		return (true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return (true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		//setText("Single Tap");
		System.out.println("HRH: Single Tap");
		isColor = isColor + 1;
		if(isColor > 5)
		{
			isColor = 1;
		} else if(isColor == 2)
		{
			currentTexcoord = sqaureTexcoord_0;
		}
		else if(isColor == 3)
		{
			currentTexcoord = sqaureTexcoord_1;
		}
		else if(isColor == 4)
		{
			currentTexcoord = sqaureTexcoord_2;
		}
		else if(isColor == 5)
		{
			currentTexcoord = sqaureTexcoord_3;
		}
		return (true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return (true);
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return (true);
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
		//setText("Long Press");
		System.out.println("HRH: Long Press");
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll");
		System.out.println("HRH: Scroll");
		uninitialize();
		System.exit(0);
		return (true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
		
	}
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return (true);
	}
	
	private void initialize(GL10 gl)
	{
		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es \n" +
			"in vec4 vPosition;" +
			"in vec2 vTexCoord;" +
			"out vec2 outTexCoord; " +
			"uniform mat4 u_mvpMatrix;" +
			"void main()" +
			"{" +
			"outTexCoord = vTexCoord;"  +
			"gl_Position = u_mvpMatrix * vPosition;" +
			"}"
		);
		
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		
		GLES32.glCompileShader(vertexShaderObject);
		
		int[] iInfoLogLength = new int[1];
		int[] status = new int[1];
		String szInfoLog = null;
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("HRH: Vertex shader compilation error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Vertex shader program compilation complete successfully");
		
		/*************Fragment shader***************************/
		
		final String fragmentShaderSourceCode = String.format
								(
										"#version 320 es\n"+
										"precision highp float;" +
										"in vec2 outTexCoord;" +
										"out vec4 FragColor; " +
										"uniform int u_isColor;" +
										"uniform highp sampler2D u_texture_sampler;" +
										"void main()"+
										"{" +
												"if(u_isColor == 1)" +
												"{" +
												"FragColor = vec4(1.0, 1.0, 1.0, 1.0) ;" +
												"}" +
												"else" +
												"{" +
												"FragColor = texture(u_texture_sampler,outTexCoord);" +
												"}" +
										"}"
								);
								
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
								
		GLES32.glCompileShader(fragmentShaderObject);
								
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("HRH: Fragment shader compilation error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Frgament shader program compilation complete successfully");
		
		shaderProgramObject = GLES32.glCreateProgram();
		
		// attach vertex shader to shader program
		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
	
		// attch fragment shader to shader program
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);
		
		// pri-link binding of shader object with vertex shader possition attribute
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.HRH_ATTRIBUTE_TEXTURE_0, "vTexCoord");
			
		// LINK PROGRAM
		GLES32.glLinkProgram(shaderProgramObject);
		
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("HRH: linking error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Shader program linking complete successfully");		
		
		mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvpMatrix");
		textureSamplerUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_texture_sampler");
		isColorUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_isColor");
		
		/***************************************RECTANGLE*************************************** */

		GLES32.glGenVertexArrays(1, vao_rectangle, 0);
		GLES32.glBindVertexArray(vao_rectangle[0]);

		// position
		final float cubeVertices[] = new float[]
		{
		 // FRONT
			1.0f,1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,		
		};

		ByteBuffer byteBufferCubeVertices = ByteBuffer.allocateDirect(cubeVertices.length * 4);
		byteBufferCubeVertices.order(ByteOrder.nativeOrder());
		FloatBuffer cubeVerticesBuffer = byteBufferCubeVertices.asFloatBuffer();
		cubeVerticesBuffer.put(cubeVertices);
		cubeVerticesBuffer.position(0);

		GLES32.glGenBuffers(1, vbo_rectangle_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_rectangle_position[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, (cubeVertices.length * 4), cubeVerticesBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.HRH_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.HRH_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

	
		GLES32.glGenBuffers(1, vbo_rectangle_texcoord, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_rectangle_texcoord[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, (currentTexcoord.length * 4), null, GLES32.GL_DYNAMIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.HRH_ATTRIBUTE_TEXTURE_0, 2, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.HRH_ATTRIBUTE_TEXTURE_0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0); // end vao for rectangle

		/************************************************************************************************** */

		//GLES32.glShadeModel(GLES32.GL_SMOOTH);
		//GLES32.glClearDepth(1.0f);
		//GLES32.glHint(GLES32.GL_PERSPECTIVE_CORRECTION_HINT, GLES32.GL_NICEST);

		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		// we will always cull face for better performance
		//GLES32.glEnable(GLES32.GL_CULL_FACE);
			
		tweaked_texture[0] = loadGLTexture(R.raw.smiley_512x512);
		GLES32.glEnable(GLES32.GL_TEXTURE_2D);
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);

	}

	private int loadGLTexture(int texture_resource_id)
	{
		//VARIABLE DECLARATION
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inScaled = false;
		int[] texture = new int[1];

		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), texture_resource_id);
	
		//CODE	
		// from here start OpenGL Texture code
		GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 1); //IN FFP 4
		GLES32.glGenTextures(1, texture, 0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
	
		//setting texture parameter
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);
	
		//glTexImage2D(GLES32.GL_TEXTURE_2D, 0, 3, bmp.bmWidth, bmp.bmHeight, 0, GLES32.GL_BGR, GLES32.GL_UNSIGNED_BYTE, bmp.bmBits);		
		GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap, 0);
		GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);
	
		return texture[0];
	}


	private void resize(int width, int hight)
	{
		GLES32.glViewport(0,0,width, hight);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix,0, 45.0f, ((float) width / (float) hight), ((float) hight / (float) width), 100.0f);
		
	}
	
	private void draw()
	{
		// Draw background color
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);		

		// START USING OpenGL program object
		GLES32.glUseProgram(shaderProgramObject);
	
		//OpenGL Drawing
		// set modelview & modelviewProjection matrices to indentity
		float[] modelViewMatrix = new float[16];
		float[] translateMatrix = new float[16];
		float[] modelViewProjectionMatrix = new float[16];
		
		/***************************Rectangle***************************************************** */

		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, 0.0f, 0.0f, -6.0f);

		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.multiplyMM(modelViewMatrix, 0, modelViewMatrix, 0, translateMatrix, 0);

		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);
	
		// pass above modelviewprojection matrix to vertex shader 'u_mvpMatrix' shader variable
		// whose value we already have calculated in inititialization() by uisng glGetUniformLocation()
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
		GLES32.glUniform1i(isColorUniform, isColor);
	
		if(isColor != 1)
		{
			GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
			GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, tweaked_texture[0]);
			GLES32.glUniform1i(textureSamplerUniform, 0);

			ByteBuffer byteBufferCubeTexcoord = ByteBuffer.allocateDirect(currentTexcoord.length * 4);
			byteBufferCubeTexcoord.order(ByteOrder.nativeOrder());
			FloatBuffer cubeTexcoordBuffer = byteBufferCubeTexcoord.asFloatBuffer();
			cubeTexcoordBuffer.put(currentTexcoord);
			cubeTexcoordBuffer.position(0);

			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_rectangle_texcoord[0]);
			GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, (currentTexcoord.length * 4), cubeTexcoordBuffer, GLES32.GL_DYNAMIC_DRAW);
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		}

		// *** bind vao **
		GLES32.glBindVertexArray(vao_rectangle[0]);	
		// ** draw , either by glDrawTriangles() or glDrawArrays() or glDrawElements();
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);	
		// ** unbind vao **
		GLES32.glBindVertexArray(0);

		// stop using OpenGL program object
		GLES32.glUseProgram(0);		
		requestRender();
	}
	
	private void uninitialize()
	{
		
	// destroy vao
	if (vao_rectangle[0] != 0)
	{
		GLES32.glDeleteVertexArrays(1, vao_rectangle, 0);
		vao_rectangle[0] = 0;
	}

	// destroy vbo
	if (vbo_rectangle_position[0] != 0)
	{
		GLES32.glDeleteBuffers(1, vbo_rectangle_position, 0);
		vbo_rectangle_position[0] = 0;
	}

	if (vbo_rectangle_texcoord[0] != 0)
	{
		GLES32.glDeleteBuffers(1, vbo_rectangle_texcoord, 0);
		vbo_rectangle_texcoord[0] = 0;
	}			

	if (tweaked_texture[0] != 0)
	{
		GLES32.glDeleteTextures(1, tweaked_texture, 0);
		tweaked_texture[0] = 0;
	}

	// deatch vertex shader from shader program object
	GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);

	// detach fragment shader from shader program obejct
	GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);

	// delete vertex shader object
	if(vertexShaderObject != 0)
	{
		GLES32.glDeleteShader(vertexShaderObject);
		vertexShaderObject = 0;
	}

	// delete fragment shader object
	if(fragmentShaderObject != 0)
	{
		GLES32.glDeleteShader(fragmentShaderObject);
		fragmentShaderObject = 0;
	}

	// delete shader program object
	if(shaderProgramObject != 0)
	{
		GLES32.glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;
	}

	// unlink shader program
	GLES32.glUseProgram(0);
	}
}
