package com.example.bdaydemo;

import androidx.appcompat.widget.AppCompatTextView;

import android.view.MotionEvent;

import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;

import android.content.Context;
import android.graphics.Color;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.PorterDuff.Mode;
import android.opengl.GLUtils;
import android.view.Gravity;

import android.opengl.GLSurfaceView;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32;
import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.Arrays;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private GestureDetector gestureDetector;
	
	private final Context context;

	private SceneCredit sceneCredit;

	private int sceneNumber = 0;

	private int theViewportWidth, theViewportHeight;

	private float perspectiveProjectionMatrix[] = new float[16];
	
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		
		context = drawingContext;
		
		// according to EGLContext to current supported version of OpenGL ES
		setEGLContextClientVersion(3);
		
		//set Render for drawing on the GLSurfaceView
		setRenderer(this);
		
		// Render the vuew only when there is change in the drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
				
		gestureDetector = new GestureDetector(context, this, null, false);
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// OpenGL-ES version check
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("HRH: "+ version);
		
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("HRH:  GLSL Version = " + glslVersion);
		
		initialize(gl);
	}
	
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int hight)
	{
		resize(width, hight);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
		draw();
		Update();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventaction = event.getAction();
		/*if (event.getAction() == MotionEvent.ACTION_DOWN) {
            prevX = event.getRawX();
            prevY = event.getRawY();
            touchDown(prevX, prevY);
        }
        else if (event.getAction() == MotionEvent.ACTION_UP) {
            touchUp(event.getRawX(), event.getRawY());
        }
		else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            float x = event.getRawX();
            float y = event.getRawY();
            touchMove(x, y, prevX, prevY);
            prevX = x;
            prevY = y;
        }*/
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		}

		return(true);
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		System.out.println("HRH: Double Tap");
		//setText("Double Tap");
		// count = 0;
		return (true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return (true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		//setText("Single Tap");
		System.out.println("HRH: Single Tap");
		return (true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return (true);
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return (true);
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
		//setText("Long Press");
		System.out.println("HRH: Long Press");
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll");
		System.out.println("HRH: Scroll");
		uninitialize();
		System.exit(0);
		return (true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
			
	}
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return (true);
	}

	public void touchUp(float aX, float aY) {
        //mustRebuildText = true;
    }

	// Touch UP event occured
    public void touchDown(float aX, float aY) {
    }

	public void touchMove(float aX, float aY,
                          float aPrevX, float aPrevY) {
       	sceneCredit.setRelPos(0.0f, 0.0f, (aPrevY - aY)/(float)(theViewportHeight / 4));
    }
	
	private void initialize(GL10 gl)
	{
		/************************************************************************************************** */

		//GLES32.glShadeModel(GLES32.GL_SMOOTH);
		//GLES32.glClearDepth(1.0f);
		//GLES32.glHint(GLES32.GL_PERSPECTIVE_CORRECTION_HINT, GLES32.GL_NICEST);

		sceneCredit = new SceneCredit();

		System.out.println("HRH: Calling sceneCredit.initializeSceneCredit(gl)");

		if(sceneCredit.initializeSceneCredit(gl, context) != GLESMacros.SUCCESS)
		{
			System.out.println("HRH: sceneCredit.initializeSceneCredit(gl) failed");
			uninitialize();
			System.exit(0);
		}

		System.out.println("HRH: sceneCredit.initializeSceneCredit(gl) success");

		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		
		GLES32.glEnable(GLES32.GL_TEXTURE_2D);
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
	}

	public int loadGLTexture(int texture_resource_id)
	{
		//VARIABLE DECLARATION
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inScaled = false;
		int[] texture = new int[1];

		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), texture_resource_id);
	
		//CODE	
		// from here start OpenGL Texture code
		GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 1); //IN FFP 4
		GLES32.glGenTextures(1, texture, 0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
	
		//setting texture parameter
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);
	
		//glTexImage2D(GLES32.GL_TEXTURE_2D, 0, 3, bmp.bmWidth, bmp.bmHeight, 0, GLES32.GL_BGR, GLES32.GL_UNSIGNED_BYTE, bmp.bmBits);		
		GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap, 0);
		GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);
	
		return texture[0];
	}


	private void resize(int width, int hight)
	{
		GLES32.glViewport(0,0,width, hight);
		
		theViewportWidth = width;
		theViewportHeight = hight;

		sceneCredit.setViewportWidthHeight(theViewportWidth,theViewportHeight);

		Matrix.perspectiveM(perspectiveProjectionMatrix,0, 45.0f, ((float) width / (float) hight), ((float) hight / (float) width), 100.0f);
		
	}

	private void draw()
	{
		// Draw background color 
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);		

		// call scene from here 
		if(sceneNumber == 0)
		{
			// Scene_Credit_Display() is temprory , after adding scene 0 please replace with Scene_0_Display() 
			// and move Scene_Credit_Display() to credit scene number 
			sceneCredit.Scene_Credit_Display(perspectiveProjectionMatrix); 
		}
		/*else if(sceneNumber == 1) 
		{
			Scene_1_Display(); 
		}
		else if(sceneNumber == 2) 
		{
			Scene_2_Display(); 
		}*/
		requestRender();
	}

	// you can call scene wise update() from here also or end of scene_No_Display() 
	private void Update()
	{
		if(sceneNumber == 0)
		{
			// Scene_Credit_Update();() is temprory , after adding scene 0 please replace with Scene_0_Update() 
			// and move Scene_Credit_Update(); to credit scene number 
			sceneCredit.Scene_Credit_Update();
		}
		
	}

	private void Scene_0_Display()
	{
		// code

		// update
		Scene_0_Update();

	}

	private void Scene_0_Update()
	{

	}

	private void Scene_1_Display()
	{
		// code

		// update
		Scene_1_Update();

	}

	private void Scene_1_Update()
	{
		
	}

	private void Scene_2_Display()
	{
		// code

		// update
		Scene_2_Update();

	}

	private void Scene_2_Update()
	{
		
	}

	private void uninitialize()
	{
		sceneCredit.uninitialize();	
	}
}
