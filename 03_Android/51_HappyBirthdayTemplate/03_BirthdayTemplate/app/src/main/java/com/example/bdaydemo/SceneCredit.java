package com.example.bdaydemo;


import androidx.appcompat.widget.AppCompatTextView;

import android.view.MotionEvent;

import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;

import android.content.Context;
import android.graphics.Color;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.PorterDuff.Mode;
import android.opengl.GLUtils;
import android.view.Gravity;

import android.opengl.GLSurfaceView;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32;
import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.Arrays;

public class SceneCredit
{
    private int vertexShaderObjectFont;
	private int fragmentShaderObjectFont;
	private int shaderProgramObjectFont;
	
	private int[] vao_quad = new int[1];
	private int[] vbo_quad_position = new int[1];
	private int[] vbo_quad_texcoord = new int[1];

	// d for diwali
	private int[] vao_quad_d = new int[1];
	private int[] vbo_quad_position_d = new int[1];
	private int[] vbo_quad_texcoord_d = new int[1];

	private int mvpUniform = 0;
	private int textureSamplerUniform = 0;
	
	private int[] stone_texture = new int[1];
	private int[] hero_bk_texture = new int[1];
	private int[] text_texture_happy = new int[1];
	private int[] text_texture_birthday = new int[1];
	private int[] text_texture_hero = new int[1];

	private static float rotateByAngle = 0.0f;

	int width, hight;

	private String theTextHappy = new String("H");
	private String theTextBirthday = new String("");
	private String theTextHero = new String("");
	private float textSize = 20;
    private int bitmapWidth = 1;
    private int bitmapHeight = 1;

	private int theViewportWidth, theViewportHeight;

	private float[] modelViewMatrix = new float[16];

	private boolean mustRebuildText = true;
	
	private Vector4f thePosition = new Vector4f();

	private boolean isupdate_FontNeeded = true;

	private float prevX = 0.0f;
    private float prevY = 0.0f;

	private int count = 0;
	private int temp = 10;

    public int initializeSceneCredit(GL10 gl,Context context)
    {

		System.out.println("HRH: In sceneCredit.initializeSceneCredit(gl)");

        final String vertexShaderSourceCode = String.format
		(
			"#version 320 es \n" +
			"in vec4 vPosition;" +
			"in vec2 vTexCoord;" +
			"out vec2 outTexCoord; " +
			"uniform mat4 u_mvpMatrix;" +
			"void main()" +
			"{" +
			"outTexCoord = vTexCoord;"  +
			"gl_Position = u_mvpMatrix * vPosition;" +
			"}"
		);
		
		vertexShaderObjectFont = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		GLES32.glShaderSource(vertexShaderObjectFont, vertexShaderSourceCode);
		
		GLES32.glCompileShader(vertexShaderObjectFont);
		
		int[] iInfoLogLength = new int[1];
		int[] status = new int[1];
		String szInfoLog = null;
		GLES32.glGetShaderiv(vertexShaderObjectFont, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObjectFont, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(vertexShaderObjectFont);
				System.out.println("HRH: Vertex shader compilation error : "+ szInfoLog + " ");
                uninitialize();				
				return GLESMacros.ERROR;
                //uninitialize();
				//System.exit(0);
			}
		}
		
		System.out.println("HRH: Vertex shader program compilation complete successfully");
		
		/*************Fragment shader***************************/
		
		final String fragmentShaderSourceCode = String.format
								(
										"#version 320 es\n"+
										"precision highp float;" +
										"in vec2 outTexCoord;" +
										"out vec4 FragColor; " +
										"uniform highp sampler2D u_texture_sampler;" +
										"void main()"+
										"{" +
										"FragColor = texture(u_texture_sampler,outTexCoord) ;" +
										"}"
								);
								
		fragmentShaderObjectFont = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		GLES32.glShaderSource(fragmentShaderObjectFont, fragmentShaderSourceCode);
								
		GLES32.glCompileShader(fragmentShaderObjectFont);
								
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetShaderiv(fragmentShaderObjectFont, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObjectFont, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(fragmentShaderObjectFont);
				System.out.println("HRH: Fragment shader compilation error : "+ szInfoLog + " ");				
				uninitialize();
				return GLESMacros.ERROR;
                //uninitialize();
				//System.exit(0);				
			}
		}
		
		System.out.println("HRH: Frgament shader program compilation complete successfully");
		
		shaderProgramObjectFont = GLES32.glCreateProgram();
		
		// attach vertex shader to shader program
		GLES32.glAttachShader(shaderProgramObjectFont, vertexShaderObjectFont);
	
		// attch fragment shader to shader program
		GLES32.glAttachShader(shaderProgramObjectFont, fragmentShaderObjectFont);
		
		// pri-link binding of shader object with vertex shader possition attribute
		GLES32.glBindAttribLocation(shaderProgramObjectFont, GLESMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
		GLES32.glBindAttribLocation(shaderProgramObjectFont, GLESMacros.HRH_ATTRIBUTE_TEXTURE_0, "vTexCoord");
	
		// LINK PROGRAM
		GLES32.glLinkProgram(shaderProgramObjectFont);
		
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetProgramiv(shaderProgramObjectFont, GLES32.GL_LINK_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObjectFont, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetProgramInfoLog(shaderProgramObjectFont);
				System.out.println("HRH: linking error : "+ szInfoLog + " ");				
                uninitialize();
				return GLESMacros.ERROR;
                //uninitialize();
				//System.exit(0);					
			}
		}
		
		System.out.println("HRH: Shader program linking complete successfully");		
		
		mvpUniform = GLES32.glGetUniformLocation(shaderProgramObjectFont, "u_mvpMatrix");
		textureSamplerUniform = GLES32.glGetUniformLocation(shaderProgramObjectFont, "u_texture_sampler");
		
		/********************************** Quad ***************************************************** */

		final float quadVerts[] = new float[]
		{
				// X, Y, Z
                -0.5f, -0.5f, 0,
                -0.5f,  0.5f, 0,
                 0.5f, -0.5f, 0,
                 0.5f,  0.5f, 0
		};

		GLES32.glGenVertexArrays(1, vao_quad, 0);
		GLES32.glBindVertexArray(vao_quad[0]);
		
		System.out.println("HRH: vao bind");
		
		// position
		GLES32.glGenBuffers(1, vbo_quad_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_quad_position[0]);
		
		System.out.println("HRH: vbo bind");
		
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect( quadVerts.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(quadVerts);
		verticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, quadVerts.length * 4, verticesBuffer, GLES32.GL_STATIC_DRAW);
		
		GLES32.glVertexAttribPointer(GLESMacros.HRH_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);

		GLES32.glEnableVertexAttribArray(GLESMacros.HRH_ATTRIBUTE_POSITION);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0); // glEnd() for vbo
		
		final float quadCoords[] = new float[]
		{   
				// X, Y, Z
                0.0f, 1.0f,
                0.0f, 0.0f,
                1.0f, 1.0f,
                1.0f, 0.0f,			
		};


		ByteBuffer byteBufferPyramidColor = ByteBuffer.allocateDirect(quadCoords.length * 4); // 4 - size of float
		byteBufferPyramidColor.order(ByteOrder.nativeOrder());
		FloatBuffer quadCoordsBuffer = byteBufferPyramidColor.asFloatBuffer();
		quadCoordsBuffer.put(quadCoords);
		quadCoordsBuffer.position(0);

		//color
		GLES32.glGenBuffers(1, vbo_quad_texcoord, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_quad_texcoord[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, quadCoords.length * 4, quadCoordsBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.HRH_ATTRIBUTE_TEXTURE_0, 2, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.HRH_ATTRIBUTE_TEXTURE_0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		GLES32.glBindVertexArray(0); // end for Vao

		/********************************** Quad for diwali background ******************************************************/
		final float quadVerts_d[] = new float[]
		{
				// X, Y, Z
                -1.15f, -0.55f, 0,
                -1.15f,  0.55f, 0,
                 1.15f, -0.55f, 0,
                 1.15f,  0.55f, 0
		};

		GLES32.glGenVertexArrays(1, vao_quad_d, 0);
		GLES32.glBindVertexArray(vao_quad_d[0]);
		
		System.out.println("HRH: vao bind");
		
		// position
		GLES32.glGenBuffers(1, vbo_quad_position_d, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_quad_position_d[0]);
		
		System.out.println("HRH: vbo bind");
		
		byteBuffer = ByteBuffer.allocateDirect( quadVerts_d.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(quadVerts_d);
		verticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, quadVerts.length * 4, verticesBuffer, GLES32.GL_STATIC_DRAW);
		
		GLES32.glVertexAttribPointer(GLESMacros.HRH_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);

		GLES32.glEnableVertexAttribArray(GLESMacros.HRH_ATTRIBUTE_POSITION);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0); // glEnd() for vbo
		
		final float quadCoords_d[] = new float[]
		{   
				// X, Y, Z
                0.0f, 1.0f,
                0.0f, 0.0f,
                1.0f, 1.0f,
                1.0f, 0.0f,			
		};


		byteBufferPyramidColor = ByteBuffer.allocateDirect(quadCoords_d.length * 4); // 4 - size of float
		byteBufferPyramidColor.order(ByteOrder.nativeOrder());
		quadCoordsBuffer = byteBufferPyramidColor.asFloatBuffer();
		quadCoordsBuffer.put(quadCoords_d);
		quadCoordsBuffer.position(0);

		//color
		GLES32.glGenBuffers(1, vbo_quad_texcoord_d, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_quad_texcoord_d[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, quadCoords.length * 4, quadCoordsBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.HRH_ATTRIBUTE_TEXTURE_0, 2, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.HRH_ATTRIBUTE_TEXTURE_0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		GLES32.glBindVertexArray(0); // end for Vao

        // we will always cull face for better performance
		stone_texture[0] = loadGLTexture(R.raw.stone, context);
		hero_bk_texture[0] = loadGLTexture(R.raw.om, context);

        loadTextTexture(text_texture_happy);
		loadTextTexture(text_texture_birthday);
		loadTextTexture(text_texture_hero);

        setPosition(0.0f, 0.0f, -1.0f);

        return GLESMacros.SUCCESS;
    }

	private int loadGLTexture(int texture_resource_id, Context context)
	{
		//VARIABLE DECLARATION
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inScaled = false;
		int[] texture = new int[1];

		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), texture_resource_id);
	
		//CODE	
		// from here start OpenGL Texture code
		GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 1); //IN FFP 4
		GLES32.glGenTextures(1, texture, 0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
	
		//setting texture parameter
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);
	
		//glTexImage2D(GLES32.GL_TEXTURE_2D, 0, 3, bmp.bmWidth, bmp.bmHeight, 0, GLES32.GL_BGR, GLES32.GL_UNSIGNED_BYTE, bmp.bmBits);		
		GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap, 0);
		GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);
	
		return texture[0];
	}

    public void setViewportWidthHeight(int w,int h)
    {
        theViewportWidth = w;
		theViewportHeight = h;
    }

    private void loadTextTexture(int[] texture)
	{
		GLES32.glGenTextures(1, texture, 0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
		
		GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 1);
		GLES32.glTexParameterf(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
		GLES32.glTexParameterf(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);
		GLES32.glTexParameterf(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_WRAP_S, GLES32.GL_CLAMP_TO_EDGE);
		GLES32.glTexParameterf(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_WRAP_T, GLES32.GL_CLAMP_TO_EDGE);		
	}

    public void setRelPos(float aX, float aY, float aZ) {
        setPosition(thePosition.x + aX,
                    thePosition.y + aY,
                    thePosition.z + aZ);

        // Preventing going too far-from or close-to the screen
        if (thePosition.z > 0.9f)
            thePosition.z = 0.9f;
        if (thePosition.z < -4.0f)
            thePosition.z = -4.0f;
    }

	private void setPosition(float aX, float aY, float aZ) {
        thePosition.set(aX, aY, aZ, 1.0f);
        isupdate_FontNeeded = true;
    }

    public void Scene_Credit_Display(float[] perspectiveProjectionMatrix)
	{
		// OpenGL Drawing 
		// set modelview & modelviewProjection matrices to indentity 

		float[] modelViewMatrix_d = new float[16];
		float[] translateMatrix = new float[16];
		float[] rotateMatrix = new float[16];
		float[] scaleMatrix = new float[16];
		float[] modelViewProjectionMatrix = new float[16];

		// diwali background 
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, 0.0f, 0.0f, -1.5f);
		
		Matrix.setIdentityM(modelViewMatrix_d, 0);
		Matrix.multiplyMM(modelViewMatrix_d, 0, modelViewMatrix_d, 0, translateMatrix, 0); 

		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix_d, 0);

		// pass above modelviewprojection matrix to vertex shader 'u_mvpMatrix' shader variable 
		// whose value we already have calculated in inititialization() by uisng glGetUniformLocation() 
		GLES32.glUseProgram(shaderProgramObjectFont);
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, hero_bk_texture[0]);
		GLES32.glUniform1i(textureSamplerUniform, 0);

		GLES32.glDisable(GLES32.GL_CULL_FACE);
		GLES32.glEnable(GLES32.GL_BLEND);
		GLES32.glBlendFunc(GLES32.GL_ONE, GLES32.GL_ONE_MINUS_SRC_ALPHA);

		GLES32.glBindVertexArray(vao_quad_d[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_STRIP, 0, 4);
		GLES32.glBindVertexArray(0);

		// Display happy  text		
		DisplayText(-0.6f, 0.1f, -1.4f, 0.8f, 0.4f, 1.0f, text_texture_happy, perspectiveProjectionMatrix);
		// display diwali text
		DisplayText(-0.3f, -0.3f, -1.4f, 0.8f, 0.4f, 1.0f, text_texture_birthday, perspectiveProjectionMatrix);
		DisplayText(0.5f, -0.3f, -1.4f, 0.7f, 0.3f, 1.0f, text_texture_hero, perspectiveProjectionMatrix);
	}

    public void Scene_Credit_Update()
	{
		count =  count + 1;
		if(count == 20)
		{
			theTextHappy = "Ha   ";
			mustRebuildText = true;
		}
		else if(count == 40)
		{
			theTextHappy = "Hap  ";
			mustRebuildText = true;
		}
		else if(count == 60)
		{
			theTextHappy = "Happ ";
			 mustRebuildText = true;
		}
		else if(count == 80)
		{
			theTextHappy = "Happy";
			mustRebuildText = true;
		}
		else if(count == 100)
		{
			theTextBirthday = "B     ";
			mustRebuildText = true;
		}
		else if(count == 120)
		{
			theTextBirthday = "Bi      ";
			mustRebuildText = true;
		}
		else if(count == 140)
		{
			theTextBirthday = "Bir     ";
			mustRebuildText = true;
		}
		else if(count == 160)
		{
			theTextBirthday = "Birt    ";
			mustRebuildText = true;
		}
		else if(count == 180)
		{
			theTextBirthday = "Birth   ";
			mustRebuildText = true;
		}
		else if(count == 200)
		{
			theTextBirthday = "Birthd  ";
			mustRebuildText = true;
		}
		else if(count == 220)
		{
			theTextBirthday = "Birthda ";
			mustRebuildText = true;
		}
		else if(count == 240)
		{
			theTextBirthday = "Birthday";
			mustRebuildText = true;
		}
		else if(count == 260)
		{
			theTextBirthday = "Birthday";
			mustRebuildText = true;
		}
		else if(count == 280)
		{
			theTextHero = "H    ";
			mustRebuildText = true;
		}
		else if(count == 300)
		{
			theTextHero = "He  ";
			mustRebuildText = true;
		}
		else if(count == 320)
		{
			theTextHero = "Her ";
			mustRebuildText = true;
		}
		else if(count == 340)
		{
			theTextHero = "Hero";
			mustRebuildText = true;
		}

		System.out.println("HRH: count " + count + " temp " + temp );
		
		//update_Font();
		if (mustRebuildText) {
            update_Font();
            mustRebuildText = false;
        }
	}

    private void DisplayText(float translateX, float translateY, float translateZ, float scaleX,float scaleY, float scaleZ, int[] texture, float[] perspectiveProjectionMatrix)
	{
		float[] translateMatrix = new float[16];
		float[] rotateMatrix = new float[16];
		float[] scaleMatrix = new float[16];
		float[] modelViewProjectionMatrix = new float[16];

		GLES32.glUseProgram(shaderProgramObjectFont);

		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, translateX, translateY, translateZ);

		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, scaleX, scaleY, scaleZ);
		
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.multiplyMM(modelViewMatrix, 0, modelViewMatrix, 0, translateMatrix, 0); 
		Matrix.multiplyMM(modelViewMatrix, 0, modelViewMatrix, 0, scaleMatrix, 0); 


		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);

		// pass above modelviewprojection matrix to vertex shader 'u_mvpMatrix' shader variable
		// whose value we already have calculated in inititialization() by uisng glGetUniformLocation()
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		// GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, stone_texture[0]);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
		GLES32.glUniform1i(textureSamplerUniform, 0);

		GLES32.glDisable(GLES32.GL_CULL_FACE);
		GLES32.glEnable(GLES32.GL_BLEND);
		GLES32.glBlendFunc(GLES32.GL_ONE, GLES32.GL_ONE_MINUS_SRC_ALPHA);

		GLES32.glBindVertexArray(vao_quad[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_STRIP, 0, 4);
		GLES32.glBindVertexArray(0);
	}

    private void update_Font()
	{
		/* [update_Font Text Size] */
        // 1. Calculate bounding box in screen coordinates with current matrices
        Vector4f cLT = new Vector4f(-0.5f,-0.5f, 0.0f, 1.0f);
        Vector4f cLB = new Vector4f(-0.5f, 0.5f, 0.0f, 1.0f);
        Vector4f cRT = new Vector4f( 0.5f,-0.5f, 0.0f, 1.0f);
        Vector4f cRB = new Vector4f( 0.5f, 0.5f, 0.0f, 1.0f);

        // Instead of calculating matrices again lets reuse ones which were already calculated
        // for rendering purpose. One important thing is the update_Font() method must be called
        // after render() method
        cLT.makePixelCoords(modelViewMatrix, theViewportWidth, theViewportHeight);
        cLB.makePixelCoords(modelViewMatrix, theViewportWidth, theViewportHeight);
        cRT.makePixelCoords(modelViewMatrix, theViewportWidth, theViewportHeight);
        cRB.makePixelCoords(modelViewMatrix, theViewportWidth, theViewportHeight);

        // 2. Evaluate font size based on the height of bounding box corners
        Vector4f vl = Vector4f.sub(cLB, cLT);
        Vector4f vr = Vector4f.sub(cRB, cRT);
        textSize = (vl.length3() + vr.length3()) / 2.0f;
        /* [update_Font Text Size] */

		drawCanvasToTextureFont(theTextHappy, textSize, text_texture_happy, 255, 0, 0);
		drawCanvasToTextureFont(theTextBirthday, textSize, text_texture_birthday, 0, 255, 0);
		drawCanvasToTextureFont(theTextHero, textSize, text_texture_hero, 0, 0, 255);
	}

    private void drawCanvasToTextureFont(
                String aText,
                float aFontSize, int[] texture, int r, int g, int b) 
	{

            if (aFontSize < 8.0f)
                aFontSize = 8.0f;

            if (aFontSize > 500.0f)
                aFontSize = 500.0f;

            Paint textPaint = new Paint();
            textPaint.setTextSize(aFontSize);
            textPaint.setFakeBoldText(false);
            textPaint.setAntiAlias(true);
            textPaint.setARGB(255, r, g, b);
			// If a hinting is available on the platform you are developing, you should enable it (uncomment the line below).
            //textPaint.setHinting(Paint.HINTING_ON);
            textPaint.setSubpixelText(true);
            textPaint.setXfermode(new PorterDuffXfermode(Mode.SCREEN));

            float realTextWidth = textPaint.measureText(aText);

            // Creates a new mutable bitmap, with 128px of width and height
            bitmapWidth = (int)(realTextWidth + 2.0f);
            bitmapHeight = (int)aFontSize + 2;

            Bitmap textBitmap = Bitmap.createBitmap(bitmapWidth, bitmapHeight, Bitmap.Config.ARGB_8888);
            textBitmap.eraseColor(Color.argb(0, 255, 255, 255));
            // Creates a new canvas that will draw into a bitmap instead of rendering into the screen
            Canvas bitmapCanvas = new Canvas(textBitmap);
            // Set start drawing position to [1, base_line_position]
            // The base_line_position may vary from one font to another but it usually is equal to 75% of font size (height).
            bitmapCanvas.drawText(aText, 1, 1.0f + aFontSize * 0.75f, textPaint);

            GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
            // Assigns the OpenGL texture with the Bitmap
            GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, GLES32.GL_RGBA, textBitmap, 0);
            // Free memory resources associated with this texture
            textBitmap.recycle();

            // After the image has been subloaded to texture, regenerate mipmaps
            GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);
	}

    public void uninitialize()
	{
    
    // destroy vao
	if (vao_quad[0] != 0)
	{
		GLES32.glDeleteVertexArrays(1, vao_quad, 0);
		vao_quad[0] = 0;
	}

	// destroy vbo
	if (vbo_quad_position[0] != 0)
	{
		GLES32.glDeleteBuffers(1, vbo_quad_position, 0);
		vbo_quad_position[0] = 0;
	}

	if (vbo_quad_texcoord[0] != 0)
	{
		GLES32.glDeleteBuffers(1, vbo_quad_texcoord, 0);
		vbo_quad_texcoord[0] = 0;
	}
	
	if (stone_texture[0] != 0)
	{
		GLES32.glDeleteTextures(1, stone_texture, 0);
		stone_texture[0] = 0;
	}

	// deatch vertex shader from shader program object
	GLES32.glDetachShader(shaderProgramObjectFont, vertexShaderObjectFont);

	// detach fragment shader from shader program obejct
	GLES32.glDetachShader(shaderProgramObjectFont, fragmentShaderObjectFont);

	// delete vertex shader object
	if(vertexShaderObjectFont != 0)
	{
		GLES32.glDeleteShader(vertexShaderObjectFont);
		vertexShaderObjectFont = 0;
	}

	// delete fragment shader object
	if(fragmentShaderObjectFont != 0)
	{
		GLES32.glDeleteShader(fragmentShaderObjectFont);
		fragmentShaderObjectFont = 0;
	}

	// delete shader program object
	if(shaderProgramObjectFont != 0)
	{
		GLES32.glDeleteProgram(shaderProgramObjectFont);
		shaderProgramObjectFont = 0;
	}

    // unlink shader program
	GLES32.glUseProgram(0);

    }

}
