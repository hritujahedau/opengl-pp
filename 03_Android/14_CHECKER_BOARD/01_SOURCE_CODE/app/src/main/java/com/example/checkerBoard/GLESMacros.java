package com.example.checkerBoard;

public class GLESMacros
{
	// attribute index
	public static final int HRH_ATTRIBUTE_POSITION = 0 ;
	public static final int HRH_ATTRIBUTE_COLOR = 1 ;
	public static final int HRH_ATTRIBUTE_NORMAL = 2;
	public static final int HRH_ATTRIBUTE_TEXTURE_0 = 3 ;
	public static final int CHECK_IMAGE_HEIGHT = 64;
	public static final int CHECK_IMAGE_WIDTH = 64;
}
