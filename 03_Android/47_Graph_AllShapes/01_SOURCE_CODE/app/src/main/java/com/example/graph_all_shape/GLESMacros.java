package com.example.graph_all_shape;

public class GLESMacros
{
	// attribute index
	public static final int HRH_ATTRIBUTE_POSITION = 0 ;
	public static final int HRH_ATTRIBUTE_COLOR = 1 ;
	public static final int HRH_ATTRIBUTE_NORMAL = 2;
	public static final int HRH_ATTRIBUTE_TEXTURE_0 = 3 ;
}
