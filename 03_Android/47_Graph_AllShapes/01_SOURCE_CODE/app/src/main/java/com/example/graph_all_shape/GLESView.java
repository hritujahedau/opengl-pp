package com.example.graph_all_shape;

import androidx.appcompat.widget.AppCompatTextView;

import android.view.MotionEvent;

import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;

import android.opengl.GLSurfaceView;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32;
import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.Arrays;
import java.lang.Math;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private GestureDetector gestureDetector;
	
	private final Context context;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
	private int[] vao = new int[1];
	private int[] vbo_position= new int[1];
	private int[] vbo_color = new int[1];
	private int mvpUniform = 0;
	
	private float graphColor[] = new float[] { 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f };
	private float graphVertices[] = new float[] { 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f };

	private float triangleColor[] = new float[] { 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f };
	private float triangleVertices[] = new float[] { 0.0f, 0.2f, 0.0f, -0.2f, -0.2f, 0.0f, 0.2f, -0.2f, 0.0f,  0.0f, 0.2f, 0.0f };
	
	private float perspectiveProjectionMatrix[] = new float[16];

	int width, hight;

	class POINTF
	{
		float x, y;
	};

	POINTF a = new POINTF();
	POINTF b = new POINTF(); 
	POINTF c = new POINTF(); 
	POINTF d = new POINTF(); 
	POINTF e = new POINTF();

	float radius = 1.0f;

	private float rectangleColor[] = new float[] 
	{ 
		1.0f, 1.0f, 0.0f, 
		1.0f, 1.0f, 0.0f, 
		1.0f, 1.0f, 0.0f, 
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f 
	};

	private float circleVertices[] = { 
		0.0f, 0.0f, 0.0f, 
		0.0f, 0.0f, 0.0f
	};
	private float circleColor[] = { 
		1.0f, 1.0f, 0.0f, 
		1.0f, 1.0f, 0.0f
	};
	
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		
		context = drawingContext;
		
		// according to EGLContext to current supported version of OpenGL ES
		setEGLContextClientVersion(3);
		
		//set Render for drawing on the GLSurfaceView
		setRenderer(this);
		
		// Render the vuew only when there is change in the drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
				
		gestureDetector = new GestureDetector(context, this, null, false);
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// OpenGL-ES version check
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("HRH: "+ version);
		
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("HRH:  GLSL Version = " + glslVersion);
		
		initialize(gl);
	}
	
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int hight)
	{
		resize(width, hight);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
		draw();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		System.out.println("HRH: Double Tap");
		//setText("Double Tap");
		return (true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return (true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		//setText("Single Tap");
		System.out.println("HRH: Single Tap");
		return (true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return (true);
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return (true);
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
		//setText("Long Press");
		System.out.println("HRH: Long Press");
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll");
		System.out.println("HRH: Scroll");
		uninitialize();
		System.exit(0);
		return (true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
			
	}
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return (true);
	}
	
	private void initialize(GL10 gl)
	{
		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es \n" +
			"in vec4 vPosition;" +
			"in vec4 vColor;" +
			"out vec4 out_color; " +
			"uniform mat4 u_mvpMatrix;" +
			"void main()" +
			"{" +
			"out_color = vColor;" +
			"gl_Position = u_mvpMatrix * vPosition;" +
			"}"
		);
		
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		
		GLES32.glCompileShader(vertexShaderObject);
		
		int[] iInfoLogLength = new int[1];
		int[] status = new int[1];
		String szInfoLog = null;
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("HRH: Vertex shader compilation error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Vertex shader program compilation complete successfully");
		
		/*************Fragment shader***************************/
		
		final String fragmentShaderSourceCode = String.format
								(
										"#version 320 es\n"+
										"precision highp float;" +
										"out vec4 FragColor; " +
										"in vec4 out_color;" +
										"void main()"+
										"{" +
										"FragColor = out_color;" +
										"}"
								);
								
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
								
		GLES32.glCompileShader(fragmentShaderObject);
								
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("HRH: Fragment shader compilation error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Frgament shader program compilation complete successfully");
		
		shaderProgramObject = GLES32.glCreateProgram();
		
		// attach vertex shader to shader program
		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
	
		// attch fragment shader to shader program
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);
		
		// pri-link binding of shader object with vertex shader possition attribute
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.HRH_ATTRIBUTE_COLOR, "vColor");
	
		// LINK PROGRAM
		GLES32.glLinkProgram(shaderProgramObject);
		
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("HRH: linking error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Shader program linking complete successfully");		
		
		mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvpMatrix");
		
		// Bind Vertex Array Object
		GLES32.glGenVertexArrays(1, vao, 0);
		GLES32.glBindVertexArray(vao[0]);
		
		System.out.println("HRH: vao bind");
		
		// Bind vertex buffer object for position
		
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect( graphVertices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer floatBuffer = byteBuffer.asFloatBuffer();
		floatBuffer.put(graphVertices);
		floatBuffer.position(0);
		
		GLES32.glGenBuffers(1, vbo_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, graphVertices.length * 4, floatBuffer, GLES32.GL_DYNAMIC_DRAW);		
		GLES32.glVertexAttribPointer(GLESMacros.HRH_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.HRH_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0); // glEnd() for vbo	

		System.out.println("HRH: vbo position bind");

		// Bind Vertex buffer for color
		byteBuffer = ByteBuffer.allocateDirect(graphColor.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		floatBuffer = byteBuffer.asFloatBuffer();
		floatBuffer.put(graphColor);
		floatBuffer.position(0);

		GLES32.glGenBuffers(1, vbo_color, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, graphColor.length * 4, floatBuffer,  GLES32.GL_DYNAMIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.HRH_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.HRH_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		System.out.println("HRH: vbo color bind");

		GLES32.glBindVertexArray(0); // end for Vao
		
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		// we will always cull face for better performance
		GLES32.glEnable(GLES32.GL_CULL_FACE);
			
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);

	}
	
	private void resize(int width, int hight)
	{
		GLES32.glViewport(0,0,width, hight);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix,0, 45.0f, ((float) width / (float) hight), ((float) hight / (float) width), 100.0f);
		
	}
	
	private void draw()
	{
	GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);	

	GLES32.glUseProgram(shaderProgramObject);

	float[] modelViewMatrix = new float[16];
	float[] modelViewProjectionMatrix = new float[16];
	
	
	Matrix.setIdentityM(modelViewMatrix, 0);
	Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -3.0f);
	Matrix.setIdentityM(modelViewProjectionMatrix, 0);
	
	Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);

	GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

	System.out.println("HRH: after passing matrix");

	graph();
	hallowBigCircle();
	hollowTriangle();
	hallowRectangle();
	hallowSmallCircle();

	// stop using OpenGL program object
	GLES32.glUseProgram(0);
		
	requestRender();
	}

	private void hallowSmallCircle()
	{
	
	float circle_points = 360, angle;

	ByteBuffer byteBuffer = ByteBuffer.allocateDirect(circleColor.length * 4);
	byteBuffer.order(ByteOrder.nativeOrder());
	FloatBuffer floatBuffer = byteBuffer.asFloatBuffer();
	floatBuffer.put(circleColor);
	floatBuffer.position(0);
	
	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color[0]);
	GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, circleColor.length * 4, floatBuffer, GLES32.GL_DYNAMIC_DRAW);
	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

	System.out.println("HRH: after circle color array bind");
	
	GLES32.glLineWidth(5.0f);

	float s = 0.0f, length_ec = 0.0f, length_cd = 0.0f, length_de = 0.0f;
	float hallowSmallCircleRadius = 0.0f;
	length_ec = getLengthFromVertises(e.x, e.y, c.x, c.y);
	length_cd = getLengthFromVertises(c.x, c.y, d.x, d.y);
	length_de = getLengthFromVertises(d.x, d.y, e.x, e.y);

	s = (length_ec + length_cd + length_de) / 2;
	hallowSmallCircleRadius = (float)Math.sqrt(s * (s - length_ec) * (s - length_cd) * (s - length_de)) / s;
	
	for (int i = 1; i < circle_points; i++) 
	{
		angle = (i - 1) * 0.01745329f;
		circleVertices[0] = hallowSmallCircleRadius * (float)Math.cos(angle);
		circleVertices[1] = -0.15f + hallowSmallCircleRadius * (float)Math.sin(angle);
		circleVertices[2] = 0.0f;
		angle = i * 0.01745329f;
		circleVertices[3] = hallowSmallCircleRadius * (float)Math.cos(angle);
		circleVertices[4] = -0.15f + hallowSmallCircleRadius * (float)Math.sin(angle);
		circleVertices[5] = 0.0f;

		byteBuffer = ByteBuffer.allocateDirect( circleVertices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		floatBuffer = byteBuffer.asFloatBuffer();
		floatBuffer.put(circleVertices);
		floatBuffer.position(0);
	
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, circleVertices.length * 4, floatBuffer, GLES32.GL_DYNAMIC_DRAW);		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(vao[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);
	}
	
	}

	private float getLengthFromVertises(float x1, float y1, float x2, float y2)
	{
		return (float)Math.sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
	}

	private void hallowBigCircle()
	{
	
	float circle_points = 360, angle;

	ByteBuffer byteBuffer = ByteBuffer.allocateDirect(circleColor.length * 4);
	byteBuffer.order(ByteOrder.nativeOrder());
	FloatBuffer floatBuffer = byteBuffer.asFloatBuffer();
	floatBuffer.put(circleColor);
	floatBuffer.position(0);
	
	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color[0]);
	GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, circleColor.length * 4, floatBuffer, GLES32.GL_DYNAMIC_DRAW);
	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

	System.out.println("HRH: after circle color array bind");
	
	GLES32.glLineWidth(5.0f);
	
	for (int i = 1; i < circle_points; i++) 
	{
		angle = (i - 1) * 0.01745329f;
		circleVertices[0] = radius * (float)Math.cos(angle);
		circleVertices[1] = radius * (float)Math.sin(angle);
		circleVertices[2] = 0.0f;
		angle = i * 0.01745329f;
		circleVertices[3] = radius * (float)Math.cos(angle);
		circleVertices[4] = radius * (float)Math.sin(angle);
		circleVertices[5] = 0.0f;

		byteBuffer = ByteBuffer.allocateDirect( circleVertices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		floatBuffer = byteBuffer.asFloatBuffer();
		floatBuffer.put(circleVertices);
		floatBuffer.position(0);
	
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, circleVertices.length * 4, floatBuffer, GLES32.GL_DYNAMIC_DRAW);		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(vao[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);
	}

	}

	private void hallowRectangle()
	{
	
	a.x = radius * ((float)Math.cos(35.0f * 0.01745329));
	a.y = radius * ((float)Math.sin(35.0f * 0.01745329));

	b.x = radius * ((float)Math.cos(145.0f * 0.01745329));
	b.y = radius * ((float)Math.sin(145.0f * 0.01745329));

	c.x = radius * ((float)Math.cos(215.0f * 0.01745329));
	c.y = radius * ((float)Math.sin(215.0f * 0.01745329));

	d.x = radius * ((float)Math.cos(325.0f * 0.01745329));
	d.y = radius * ((float)Math.sin(325.0f * 0.01745329));

	float hallowRectangle[] = 
	{ 
		a.x, a.y, 0.0f,
		b.x, b.y, 0.0f,
		c.x, c.y, 0.0f,
		d.x, d.y, 0.0f,
		a.x, a.y, 0.0f,
	};

	ByteBuffer byteBuffer = ByteBuffer.allocateDirect(rectangleColor.length * 4);
	byteBuffer.order(ByteOrder.nativeOrder());
	FloatBuffer floatBuffer = byteBuffer.asFloatBuffer();
	floatBuffer.put(rectangleColor);
	floatBuffer.position(0);
	
	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color[0]);
	GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, rectangleColor.length * 4, floatBuffer, GLES32.GL_DYNAMIC_DRAW);
	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

	System.out.println("HRH: after rectangle color array bind");

	byteBuffer = ByteBuffer.allocateDirect( hallowRectangle.length * 4);
	byteBuffer.order(ByteOrder.nativeOrder());
	floatBuffer = byteBuffer.asFloatBuffer();
	floatBuffer.put(hallowRectangle);
	floatBuffer.position(0);

	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position[0]);
	GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, hallowRectangle.length * 4, floatBuffer, GLES32.GL_DYNAMIC_DRAW);		
	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

	System.out.println("HRH: after rectangle vertices array bind");

	GLES32.glLineWidth(5.0f);
	GLES32.glBindVertexArray(vao[0]);
	GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
	GLES32.glDrawArrays(GLES32.GL_LINES, 1, 2);
	GLES32.glDrawArrays(GLES32.GL_LINES, 2, 2);
	GLES32.glDrawArrays(GLES32.GL_LINES, 3, 2);
	GLES32.glBindVertexArray(0);
	
	}

	private void hollowTriangle()
	{
		/***************************************HOLLOW TRAINGLE***********************************************/
		System.out.println("HRH: In Hollow Triangle");

		a.x = radius * ((float)Math.cos(35.0f * 0.01745329));
		a.y = radius * ((float)Math.sin(35.0f * 0.01745329));

		b.x = radius * ((float)Math.cos(145.0f * 0.01745329));
		b.y = radius * ((float)Math.sin(145.0f * 0.01745329));

		e.x = (a.x + b.x) / 2.0f;
		e.y = (a.y + b.y) / 2.0f;

		c.x = radius * ((float)Math.cos(215.0f * 0.01745329));
		c.y = radius * ((float)Math.sin(215.0f * 0.01745329));

		d.x = radius * ((float)Math.cos(325.0f * 0.01745329));
		d.y = radius * ((float)Math.sin(325.0f * 0.01745329));

		System.out.println("HRH: In Hollow Triangle create array");

		float hallowTriangle[] =
		{
			e.x, e.y, 0.0f,
			c.x, c.y, 0.0f,
			d.x, d.y, 0.0f,
			e.x, e.y, 0.0f,
		};		

		System.out.println("HRH: In Hollow Triangle after create array");

		ByteBuffer byteBuffer = ByteBuffer.allocateDirect( triangleColor.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer floatBuffer = byteBuffer.asFloatBuffer();
		floatBuffer.put(triangleColor);
		floatBuffer.position(0);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, triangleColor.length * 4, floatBuffer, GLES32.GL_DYNAMIC_DRAW);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		byteBuffer = ByteBuffer.allocateDirect( hallowTriangle.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		floatBuffer = byteBuffer.asFloatBuffer();
		floatBuffer.put(hallowTriangle);
		floatBuffer.position(0);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, hallowTriangle.length * 4, floatBuffer, GLES32.GL_DYNAMIC_DRAW);		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(vao[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glDrawArrays(GLES32.GL_LINES, 1, 2);
		GLES32.glDrawArrays(GLES32.GL_LINES, 2, 2);
		GLES32.glBindVertexArray(0);
	}

	private void graph()
	{
		/*************************************GRAPH************************************************/
		float distanceForX = 0, distanceForY = 0;
		float x = 3.0f, y = 3.0f;

		graphColor[0] = 0.0f;
		graphColor[1] = 0.0f;
		graphColor[2] = 1.0f;
		graphColor[3] = 0.0f;
		graphColor[4] = 0.0f;
		graphColor[5] = 1.0f;
	
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect( graphColor.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer floatBuffer = byteBuffer.asFloatBuffer();
		floatBuffer.put(graphColor);
		floatBuffer.position(0);
		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, graphColor.length * 4, floatBuffer, GLES32.GL_DYNAMIC_DRAW);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glLineWidth(1.0f);

		System.out.println("HRH: after vbo color bind");
		for (int i = 0; i < 60; i++) 
		{

		// verticle up
		graphVertices[0] = x;
		graphVertices[1] = distanceForX;
		graphVertices[2] = 0.0f;

		graphVertices[3] = -x;
		graphVertices[4] = distanceForX;
		graphVertices[5] = 0.0f;

		byteBuffer = ByteBuffer.allocateDirect( graphVertices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		floatBuffer = byteBuffer.asFloatBuffer();
		floatBuffer.put(graphVertices);
		floatBuffer.position(0);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, graphVertices.length * 4, floatBuffer, GLES32.GL_DYNAMIC_DRAW);		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(vao[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);

		System.out.println("HRH: after vertex up array bind");

		// verticle down
		graphVertices[0] = x;
		graphVertices[1] = -distanceForX;
		graphVertices[2] = 0.0f;

		graphVertices[3] = -x;
		graphVertices[4] = -distanceForX;
		graphVertices[5] = 0.0f;

		byteBuffer = ByteBuffer.allocateDirect( graphVertices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		floatBuffer = byteBuffer.asFloatBuffer();
		floatBuffer.put(graphVertices);
		floatBuffer.position(0);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, graphVertices.length * 4, floatBuffer, GLES32.GL_DYNAMIC_DRAW);		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(vao[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);

		System.out.println("HRH: after vertex down array bind");

		// horizontal right
		graphVertices[0] = -distanceForY;
		graphVertices[1] = y;
		graphVertices[2] = 0.0f;

		graphVertices[3] = -distanceForY;
		graphVertices[4] = -y;
		graphVertices[5] = 0.0f;

		byteBuffer = ByteBuffer.allocateDirect( graphVertices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		floatBuffer = byteBuffer.asFloatBuffer();
		floatBuffer.put(graphVertices);
		floatBuffer.position(0);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, graphVertices.length * 4, floatBuffer, GLES32.GL_DYNAMIC_DRAW);		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(vao[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);

		System.out.println("HRH: after vertex right array bind");

		// horizontal left
		graphVertices[0] = distanceForY;
		graphVertices[1] = y;
		graphVertices[2] = 0.0f;

		graphVertices[3] = distanceForY;
		graphVertices[4] = -y;
		graphVertices[5] = 0.0f;

		byteBuffer = ByteBuffer.allocateDirect( graphVertices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		floatBuffer = byteBuffer.asFloatBuffer();
		floatBuffer.put(graphVertices);
		floatBuffer.position(0);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, graphVertices.length * 4, floatBuffer, GLES32.GL_DYNAMIC_DRAW);		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(vao[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);

		System.out.println("HRH: after vertex left array bind");

		distanceForX += 1.5f / 15.0f;
		distanceForY += 1.5f / 10.0f;
	}

	graphColor[0] = 0.0f;
	graphColor[1] = 1.0f;
	graphColor[2] = 0.0f;
	graphColor[3] = 0.0f;
	graphColor[4] = 1.0f;
	graphColor[5] = 0.0f;
	
	byteBuffer = ByteBuffer.allocateDirect( graphColor.length * 4);
	byteBuffer.order(ByteOrder.nativeOrder());
	floatBuffer = byteBuffer.asFloatBuffer();
	floatBuffer.put(graphColor);
	floatBuffer.position(0);
	
	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color[0]);
	GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, graphColor.length * 4, floatBuffer, GLES32.GL_DYNAMIC_DRAW);
	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

	GLES32.glLineWidth(5.0f);
	// horizontal line
	graphVertices[0] = x;
	graphVertices[1] = 0;
	graphVertices[2] = 0.0f;

	graphVertices[3] = -x;
	graphVertices[4] = 0;
	graphVertices[5] = 0.0f;

	byteBuffer = ByteBuffer.allocateDirect( graphVertices.length * 4);
	byteBuffer.order(ByteOrder.nativeOrder());
	floatBuffer = byteBuffer.asFloatBuffer();
	floatBuffer.put(graphVertices);
	floatBuffer.position(0);

	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position[0]);
	GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, graphVertices.length * 4, floatBuffer, GLES32.GL_DYNAMIC_DRAW);		
	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

	GLES32.glBindVertexArray(vao[0]);
	GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
	GLES32.glBindVertexArray(0);

	GLES32.glLineWidth(5.0f);

	// verticle line
	graphVertices[0] = 0.0f;
	graphVertices[1] = y;
	graphVertices[2] = 0.0f;

	graphVertices[3] = 0.0f;
	graphVertices[4] = -y;
	graphVertices[5] = 0.0f;

	byteBuffer = ByteBuffer.allocateDirect( graphVertices.length * 4);
	byteBuffer.order(ByteOrder.nativeOrder());
	floatBuffer = byteBuffer.asFloatBuffer();
	floatBuffer.put(graphVertices);
	floatBuffer.position(0);

	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position[0]);
	GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, graphVertices.length * 4, floatBuffer, GLES32.GL_DYNAMIC_DRAW);		
	GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

	GLES32.glBindVertexArray(vao[0]);
	GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
	GLES32.glBindVertexArray(0);

	}
		
	private void uninitialize()
	{
		// destroy vao
	if (vao[0] != 0)
	{
		GLES32.glDeleteVertexArrays(1, vao, 0);
		vao[0] = 0;
	}

	// destroy vbo
	if (vbo_position[0] != 0)
	{
		GLES32.glDeleteBuffers(1, vbo_position, 0);
		vbo_position[0] = 0;
	}

	if (vbo_color[0] != 0)
	{
		GLES32.glDeleteBuffers(1, vbo_color, 0);
		vbo_color[0] = 0;
	}

	// deatch vertex shader from shader program object
	GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);

	// detach fragment shader from shader program obejct
	GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);

	// delete vertex shader object
	if(vertexShaderObject != 0)
	{
		GLES32.glDeleteShader(vertexShaderObject);
		vertexShaderObject = 0;
	}

	// delete fragment shader object
	if(fragmentShaderObject != 0)
	{
		GLES32.glDeleteShader(fragmentShaderObject);
		fragmentShaderObject = 0;
	}

	// delete shader program object
	if(shaderProgramObject != 0)
	{
		GLES32.glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;
	}

	// unlink shader program
	GLES32.glUseProgram(0);
	}
}
