package com.example.three_rotating_light;

public class GLESMacros
{
	// attribute index
	public static final int HRH_ATTRIBUTE_POSITION = 0 ;
	public static final int HRH_ATTRIBUTE_COLOR = 1 ;
	public static final int HRH_ATTRIBUTE_NORMAL = 2;
	public static final int HRH_ATTRIBUTE_TEXTURE_0 = 3 ;
	public static final float RADIAN_VALUE = 3.14159f/180.0f;
}
