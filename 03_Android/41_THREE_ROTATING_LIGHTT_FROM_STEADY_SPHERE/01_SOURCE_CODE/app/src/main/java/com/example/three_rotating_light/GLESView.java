package com.example.three_rotating_light;

import androidx.appcompat.widget.AppCompatTextView;

import android.view.MotionEvent;

import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;

import android.opengl.GLSurfaceView;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32;
import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.Arrays;
import java.lang.Math;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private GestureDetector gestureDetector;
	
	private final Context context;
	
	private int gVertexShaderObjectPerFragment;
	private int gFragmentShaderObjectPerFragment;
	private int gShaderProgramObjectPerFragment;

	private int gVertexShaderObjectPerVertex;
	private int gFragmentShaderObjectPerVertex;
	private int gShaderProgramObjectPerVertex;

	private int gCurrentShaderProgram;
	
	private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position = new int[1];
    private int[] vbo_sphere_normal = new int[1];
    private int[] vbo_sphere_element = new int[1];

	private int gModelUniform;
	private int gViewUniform;
	private int gPerspectiveUniform;
	
	private int gLightAmbientUniform;
	private int gLightSpecularUniform;
	private int gLightDiffusedUniform;
	private int gLightPossitionUniform;
	
	private int gMaterialSpecularUniform;
	private int gMaterialAmbientUniform;
	private int gMaterialDiffuseUniform;
	private int gMaterialShininessUniform;
	
	private int gIsLightOnUniform;
	
	private int isLightPerVertexShader = 0, isLightPerFragmentShader = 0, isLightOn = 0;

	private int gIsPerVertexShaderUniform;
	private int gIsPerFragmentShaderUniform;
		
	private float lightPosition[] = { 0.0f, 0.0f, 0.0f, 1.0f, 
						0.0f, 0.0f, 0.0f, 1.0f,
						0.0f, 0.0f, 0.0f, 1.0f
					};
					
	private float lightAmbient[] = {  0.0f, 0.0f, 0.0f,
						0.0f, 0.0f, 0.0f,
						0.0f, 0.0f, 0.0f
					};
					
	private float lightDiffuse[] = {  1.0f, 0.0f, 0.0f,
						0.0f, 1.0f, 0.0f,
						0.0f, 0.0f, 1.0f
					};
						
	private float lightSpecular[] = { 1.0f, 0.0f, 0.0f,
						0.0f, 1.0f, 0.0f,
						0.0f, 0.0f, 1.0f
					};

	float angle_red = 360.0f,angle_blue = 0.0f, angle_green = 360.0f;	

	private boolean bAnimate = false;
	private float materialAmbient[] = { 0.0f, 0.0f, 0.0f };
	private float materialDefuse[] = { 1.0f, 1.0f, 1.0f  };
	private float materialSpecular[] = { 1.0f, 1.0f, 1.0f};
	private float materialShinyness = 128.0f;

	private int numVertices ;
	private int numElements;
	
	private float gPerspectiveProjectionMatrix[] = new float[16];

	private static float rotateByAngle = 0.0f;

	float sin_angle = 0.0f, cos_angle = 0.0f, radius = 2;

	private int width, height;
	
	
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		
		context = drawingContext;
		
		// according to EGLContext to current supported version of OpenGL ES
		setEGLContextClientVersion(3);
		
		//set Render for drawing on the GLSurfaceView
		setRenderer(this);
		
		// Render the vuew only when there is change in the drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
				
		gestureDetector = new GestureDetector(context, this, null, false);
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// OpenGL-ES version check
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("HRH: "+ version);
		
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("HRH:  GLSL Version = " + glslVersion);
		
		initialize(gl);
	}
	
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
		draw();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		System.out.println("HRH: Double Tap");
		//setText("Double Tap");
		if (isLightPerVertexShader == 1)
			{
				isLightPerVertexShader = 0;
				isLightOn = 0;
			}
			else
			{
				isLightPerVertexShader = 1;
				isLightPerFragmentShader = 0;
				isLightOn = 1;
			}
		return (true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return (true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		//setText("Single Tap");
		System.out.println("HRH: Single Tap");
		if (bAnimate == false)
				{
					bAnimate = true;
				}
				else
				{
					bAnimate = false;
				}
		
		return (true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return (true);
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return (true);
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
		//setText("Long Press");
		System.out.println("HRH: Long Press");
		if (isLightPerFragmentShader == 0)
			{
				isLightPerFragmentShader = 1;
				isLightPerVertexShader = 0;
				isLightOn = 1;
			}
			else
			{
				isLightPerFragmentShader = 0;
				isLightOn = 0;
			}
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll");
		System.out.println("HRH: Scroll");
		uninitialize();
		System.exit(0);
		return (true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
			
	}
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return (true);
	}
	
	private void initialize(GL10 gl)
	{
		/***************************************PER VERTEX SHADER*******************************************************************/
		// vertex shader
		final String vertexShaderSourceCodePerVertex = String.format
		(
			"#version 320 es \n" +
			"in vec4 v_position;" +
			"in vec3 v_normals; " +
			"uniform mat4 u_modelMatrix;" +
			"uniform mat4 u_viewMatrix;" +
			"uniform mat4 u_projectionMatrix;" +
			"uniform int u_isLightOn;" +
			"uniform vec3 u_lightAmbient[3];" +
			"uniform vec3 u_lightSpecular[3];" +
			"uniform vec3 u_lightDiffuse[3];" +
			"uniform vec4 u_lightposition[3]; " +
			"uniform vec3 u_materialAmbient; " +
			"uniform vec3 u_materialSpecular; " +
			"uniform vec3 u_materialDiffuse; " +
			"uniform float u_materialshininess; " +
			"out vec3 phoung_ads_lighting;" +
			"vec4 eye_coordinates;" +
			"vec3 transformed_normal, light_direction, reflection_vector, view_vector;" +
			"vec3 ambient, diffuse_light, specular;" +
			"void main()" +
			"{" +
			"if (u_isLightOn == 1) " +
			"{" +
				"eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" +
				"transformed_normal = normalize( mat3( u_viewMatrix * u_modelMatrix ) * v_normals); " +
				"for(int i=0; i< 3;i++) " +
				"{" +
					"light_direction = normalize( vec3 ( ( u_lightposition[i]) - eye_coordinates) ); " +
					"reflection_vector = reflect (-light_direction, transformed_normal); " +
					"view_vector = normalize ( vec3 ( -eye_coordinates)); " +
					"ambient = u_lightAmbient[i] * u_materialAmbient; " +
					"diffuse_light = u_lightDiffuse[i] * u_materialDiffuse * max ( dot (light_direction , transformed_normal), 0.0); " +
					"specular = u_lightSpecular[i] * u_materialSpecular * pow( max ( dot ( reflection_vector , view_vector), 0.0) , u_materialshininess);" +
					"phoung_ads_lighting = phoung_ads_lighting + ambient + diffuse_light + specular;" +
				"}" +
			"}" +
			"else" +
			"{" +
			"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" +
			"}"+
			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" +
			"}"
		);
		
		gVertexShaderObjectPerVertex = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		GLES32.glShaderSource(gVertexShaderObjectPerVertex, vertexShaderSourceCodePerVertex);
		
		GLES32.glCompileShader(gVertexShaderObjectPerVertex);
		
		int[] iInfoLogLength = new int[1];
		int[] status = new int[1];
		String szInfoLog = null;
		GLES32.glGetShaderiv(gVertexShaderObjectPerVertex, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gVertexShaderObjectPerVertex, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(gVertexShaderObjectPerVertex);
				System.out.println("HRH: Vertex shader compilation error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Vertex shader program compilation complete successfully");
		
		//Fragment shader		
		final String fragmentShaderSourceCodePerVertex = String.format
								(
										"#version 320 es \n"+
										"precision highp float;" +
										"in vec3 phoung_ads_lighting;" +
										"out vec4 FragColor; " +
										"void main()"+
										"{" +
										"FragColor = vec4(phoung_ads_lighting, 1.0); " +
										"}"
								);
								
		gFragmentShaderObjectPerVertex = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		GLES32.glShaderSource(gFragmentShaderObjectPerVertex, fragmentShaderSourceCodePerVertex);								
		GLES32.glCompileShader(gFragmentShaderObjectPerVertex);
								
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetShaderiv(gFragmentShaderObjectPerVertex, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gFragmentShaderObjectPerVertex, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(gFragmentShaderObjectPerVertex);
				System.out.println("HRH: Fragment shader compilation error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Frgament shader program compilation complete successfully");
		
		gShaderProgramObjectPerVertex = GLES32.glCreateProgram();
		
		// attach vertex shader to shader program
		GLES32.glAttachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);
	
		// attch fragment shader to shader program
		GLES32.glAttachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);
		
		// pri-link binding of shader object with vertex shader possition attribute
		GLES32.glBindAttribLocation(gShaderProgramObjectPerVertex, GLESMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
		GLES32.glBindAttribLocation(gShaderProgramObjectPerVertex, GLESMacros.HRH_ATTRIBUTE_NORMAL, "v_normals");
	
		// LINK PROGRAM
		GLES32.glLinkProgram(gShaderProgramObjectPerVertex);
		
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetProgramiv(gShaderProgramObjectPerVertex, GLES32.GL_LINK_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(gShaderProgramObjectPerVertex, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetProgramInfoLog(gShaderProgramObjectPerVertex);
				System.out.println("HRH: linking error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: per vertex Shader program linking complete successfully");		
	
		/***************************************PER FRAGMENT SHADER*******************************************************************/
		// vertex shader
		final String vertexShaderSourceCodePerFragment = String.format
		(
			"#version 320 es \n" +
			"in vec4 v_position;" +
			"in vec3 v_normals; " +
			"uniform mat4 u_modelMatrix;" +
			"uniform mat4 u_viewMatrix;" +
			"uniform mat4 u_projectionMatrix;" +
			"uniform int u_isLightOn;" +
			"uniform highp vec4 u_lightposition[3]; " +
			"out vec3 light_direction[3]; " +
			"out vec3 transformed_normal;" +
			"out vec3 view_vector;" +
			"void main()" +
			"{" +
			"if (u_isLightOn == 1) " +
			"{" +
				"vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" +
				"transformed_normal =  mat3( u_viewMatrix * u_modelMatrix ) * v_normals;" +
				"for(int i = 0; i< 3; i++)" +
				"{" +
					"light_direction[i] = vec3 ( u_lightposition[i]  - eye_coordinates);" +
				"}" +
				"view_vector =  vec3 ( -eye_coordinates);" +
			"}" +
			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" +
			"}"
		);
		
		gVertexShaderObjectPerFragment = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		GLES32.glShaderSource(gVertexShaderObjectPerFragment, vertexShaderSourceCodePerFragment);
		
		GLES32.glCompileShader(gVertexShaderObjectPerFragment);
		
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetShaderiv(gVertexShaderObjectPerFragment, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gVertexShaderObjectPerFragment, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(gVertexShaderObjectPerFragment);
				System.out.println("HRH: Vertex shader compilation error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Vertex shader program compilation complete successfully");
		
		//Fragment shader		
		final String fragmentShaderSourceCodePerFragment = String.format
								(
										"#version 320 es\n"+
										"precision highp float;" +
										"vec3 phoung_ads_lighting;" +
										"out vec4 FragColor; " +
										"in vec3 light_direction[3]; " +
										"in vec3 transformed_normal;"+
										"in vec3 view_vector;"+
										"vec3 normalized_light_direction; " +
										"vec3 normalized_transformed_normal;"+
										"vec3 normalized_view_vector;"+
										"uniform highp vec3 u_lightAmbient[3];" +
										"uniform highp vec3 u_lightSpecular[3];" +
										"uniform highp vec3 u_lightDiffuse[3];" +
										"uniform highp vec3 u_materialAmbient; " +
										"uniform highp vec3 u_materialSpecular; " +
										"uniform highp vec3 u_materialDiffuse; " +
										"uniform highp float u_materialshininess; " +
										"uniform highp int u_isLightOn;" +
										"vec3 reflection_vector, ambient, diffuse_light, specular;"+
										"void main()" +
										"{"+
										"if (u_isLightOn == 1) " +
										"{" +
											"normalized_view_vector = normalize(view_vector);"+
											"normalized_transformed_normal = normalize(transformed_normal);"+
											"for(int i = 0; i < 3 ; i++)"+
											"{" +
												"normalized_light_direction = normalize(light_direction[i]);"+
												"reflection_vector = reflect (-normalized_light_direction, normalized_transformed_normal); " +
												"ambient = u_lightAmbient[i]  * u_materialAmbient; " +
												"diffuse_light = u_lightDiffuse[i] * u_materialDiffuse * max ( dot (normalized_light_direction , normalized_transformed_normal), 0.0); " +
												"specular = u_lightSpecular[i]  * u_materialSpecular * pow( max ( dot ( reflection_vector , normalized_view_vector), 0.0) , u_materialshininess);" +
												"phoung_ads_lighting = phoung_ads_lighting + ambient + diffuse_light + specular;" +
											"}"+
										"}"+
										"else" +
										"{"+
											"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" +
										"}"+
										"FragColor = vec4(phoung_ads_lighting, 1.0); " +
										"}"
								);
								
		gFragmentShaderObjectPerFragment = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		GLES32.glShaderSource(gFragmentShaderObjectPerFragment, fragmentShaderSourceCodePerFragment);								
		GLES32.glCompileShader(gFragmentShaderObjectPerFragment);
								
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetShaderiv(gFragmentShaderObjectPerFragment, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gFragmentShaderObjectPerFragment, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(gFragmentShaderObjectPerFragment);
				System.out.println("HRH: Fragment shader compilation error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Frgament shader program compilation complete successfully");
		
		gShaderProgramObjectPerFragment = GLES32.glCreateProgram();
		
		// attach vertex shader to shader program
		GLES32.glAttachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);
	
		// attch fragment shader to shader program
		GLES32.glAttachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);
		
		// pri-link binding of shader object with vertex shader possition attribute
		GLES32.glBindAttribLocation(gShaderProgramObjectPerFragment, GLESMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
		GLES32.glBindAttribLocation(gShaderProgramObjectPerFragment, GLESMacros.HRH_ATTRIBUTE_NORMAL, "v_normals");
	
		// LINK PROGRAM
		GLES32.glLinkProgram(gShaderProgramObjectPerFragment);
		
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetProgramiv(gShaderProgramObjectPerFragment, GLES32.GL_LINK_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(gShaderProgramObjectPerFragment, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetProgramInfoLog(gShaderProgramObjectPerFragment);
				System.out.println("HRH: linking error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: per vertex Shader program linking complete successfully");	


		/******************************************* SPHERE **************************************/
		Sphere sphere=new Sphere();
        float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];
        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();

		GLES32.glGenVertexArrays(1, vao_sphere, 0);
		GLES32.glBindVertexArray(vao_sphere[0]);

		// position vbo
		ByteBuffer sphereByteBuffer = ByteBuffer.allocateDirect(sphere_vertices.length * 4);
		sphereByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer sphereFloatBuffer = sphereByteBuffer.asFloatBuffer();
		sphereFloatBuffer.put(sphere_vertices);
		sphereFloatBuffer.position(0);

		GLES32.glGenBuffers(1, vbo_sphere_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_sphere_position[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_vertices.length * 4, sphereFloatBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.HRH_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.HRH_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// normals vbo
		ByteBuffer sphereNormalsByteBuffer = ByteBuffer.allocateDirect(sphere_normals.length * 4);
		sphereNormalsByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer sphereNormalsFloatBuffer = sphereNormalsByteBuffer.asFloatBuffer();
		sphereNormalsFloatBuffer.put(sphere_normals);
		sphereNormalsFloatBuffer.position(0);

		GLES32.glGenBuffers(1, vbo_sphere_normal, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_sphere_normal[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_normals.length * 4, sphereNormalsFloatBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.HRH_ATTRIBUTE_NORMAL, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.HRH_ATTRIBUTE_NORMAL);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		// element vbo
		ByteBuffer sphereElementByteBuffer = ByteBuffer.allocateDirect(sphere_elements.length * 2);
		sphereElementByteBuffer.order(ByteOrder.nativeOrder());
		ShortBuffer sphereElementShortByteBuffer = sphereElementByteBuffer.asShortBuffer();
		sphereElementShortByteBuffer.put(sphere_elements);
		sphereElementShortByteBuffer.position(0);

		GLES32.glGenBuffers(1, vbo_sphere_element, 0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER, sphere_elements.length * 2, sphereElementShortByteBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0);
		
		/************************************************************************************************** */

		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		// we will always cull face for better performance
		GLES32.glEnable(GLES32.GL_CULL_FACE);
			
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		Matrix.setIdentityM(gPerspectiveProjectionMatrix, 0);

	}

	private void resize(int width, int height)
	{
		GLES32.glViewport(0,0,width, height);
		
		Matrix.perspectiveM(gPerspectiveProjectionMatrix,0, 45.0f, ((float) width / (float) height), ((float) height / (float) width), 100.0f);
		
	}
	
	private void draw()
	{
		// Draw background color
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

		gCurrentShaderProgram = (isLightPerVertexShader == 1) ? gShaderProgramObjectPerVertex : gShaderProgramObjectPerFragment ;		

		// START USING OpenGL program object
		GLES32.glUseProgram(gCurrentShaderProgram);
	
		//OpenGL Drawing
		// set modelview & modelviewProjection matrices to indentity
		float[] viewMatrix = new float[16];
		float[] modelMatrix = new float[16];
		float[] translateMatrix = new float[16];
		float[] rotateMatrix = new float[16];
		float[] scaleMatrix = new float[16];
		
		Matrix.setIdentityM(viewMatrix, 0);

		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, 0.0f, 0.0f, -1.55f);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		// get MVP uniform location
		gModelUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_modelMatrix");
		gViewUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_viewMatrix");
		gPerspectiveUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_projectionMatrix");
	//	gIsPerFragmentShaderUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_isPerFragmentLightOn");
	//	gIsPerVertexShaderUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_isPerVertexLightOn");
		gIsLightOnUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_isLightOn");
		
		gLightAmbientUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_lightAmbient");
		gLightSpecularUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_lightSpecular");
		gLightDiffusedUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_lightDiffuse");
		gLightPossitionUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_lightposition");
	
		gMaterialDiffuseUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_materialDiffuse");
		gMaterialAmbientUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_materialAmbient");
		gMaterialSpecularUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_materialSpecular");
		gMaterialShininessUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_materialshininess");
	
		if (isLightOn == 1)
		{
			sin_angle = radius * (float)Math.sin(angle_red * GLESMacros.RADIAN_VALUE);
			cos_angle = radius * (float)Math.cos(angle_red * GLESMacros.RADIAN_VALUE);
			
			lightPosition[1] = sin_angle;
			lightPosition[2] = cos_angle;
			
			sin_angle = radius * (float)Math.sin(angle_blue * GLESMacros.RADIAN_VALUE);
			cos_angle = radius * (float)Math.cos(angle_blue * GLESMacros.RADIAN_VALUE);
			
			lightPosition[4] = sin_angle;
			lightPosition[6] = cos_angle;
			
			sin_angle = radius * (float)Math.sin(angle_green * GLESMacros.RADIAN_VALUE);
			cos_angle = radius * (float)Math.cos(angle_green * GLESMacros.RADIAN_VALUE);
			
			lightPosition[8] = sin_angle;
			lightPosition[9] = cos_angle;
			GLES32.glUniform4fv(gLightPossitionUniform, 3, lightPosition, 0);
			GLES32.glUniform3fv(gLightDiffusedUniform, 3, lightDiffuse, 0);
			GLES32.glUniform3fv(gLightAmbientUniform,  3 ,lightAmbient, 0);
			GLES32.glUniform3fv(gLightSpecularUniform, 3, lightSpecular, 0);
	
			GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient, 0);
			GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness);
			GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular, 0);
			GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse, 0);
		}
	
	//	GLES32.glUniform1i(gIsPerFragmentShaderUniform, isLightPerFragmentShader);
	//	GLES32.glUniform1i(gIsPerVertexShaderUniform, isLightPerVertexShader);
		GLES32.glUniform1i(gIsLightOnUniform, isLightOn);
		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(gViewUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(gPerspectiveUniform, 1, false, gPerspectiveProjectionMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		// stop using OpenGL program object
		GLES32.glUseProgram(0);
		
			if (bAnimate == true)
	{
		float speed = 1.0f;
		angle_red = angle_red - speed;
		angle_blue = angle_blue + speed;
		angle_green = angle_green - speed;
		if (angle_red <= 0)
		{
			angle_red = 360;
		}
		if (angle_blue >= 360)
		{
			angle_blue = 0.0f;
		}
		if (angle_green <= 0)
		{
			angle_green = 360;
		}
	}


		requestRender();
	}
	
	private void uninitialize()
	{
	
	// destroy vao
	if (vao_sphere[0] != 0)
	{
		GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
		vao_sphere[0] = 0;
	}

	// destroy vbo
	if (vbo_sphere_position[0] != 0)
	{
		GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
		vbo_sphere_position[0] = 0;
	}

	// destroy vbo
	if (vbo_sphere_element[0] != 0)
	{
		GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
		vbo_sphere_element[0] = 0;
	}

	if (vbo_sphere_normal[0] != 0)
	{
		GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
		vbo_sphere_normal[0] = 0;
	}

	// detach vertex shader from per vertex shader program object
	GLES32.glDetachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);

	// detach fragment shader 
	GLES32.glDetachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);

	// delete vertex shader object
	if(gVertexShaderObjectPerVertex != 0)
	{
		GLES32.glDeleteShader(gVertexShaderObjectPerVertex);
		gVertexShaderObjectPerVertex = 0;
	}

	// delete fragment shader object
	if(gFragmentShaderObjectPerVertex != 0)
	{
		GLES32.glDeleteShader(gFragmentShaderObjectPerVertex);
		gFragmentShaderObjectPerVertex = 0;
	}

	// delete vertex shader program object
	if(gShaderProgramObjectPerVertex != 0)
	{
		GLES32.glDeleteProgram(gShaderProgramObjectPerVertex);
		gShaderProgramObjectPerVertex = 0;
	}

	/********************** detach vertex shader from shader program object ***********************/
	GLES32.glDetachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);

	// detach fragment shader from shader program obejct
	GLES32.glDetachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);

	// delete vertex shader object
	if(gVertexShaderObjectPerFragment != 0)
	{
		GLES32.glDeleteShader(gVertexShaderObjectPerFragment);
		gVertexShaderObjectPerFragment = 0;
	}

	// delete fragment shader object
	if(gFragmentShaderObjectPerFragment != 0)
	{
		GLES32.glDeleteShader(gFragmentShaderObjectPerFragment);
		gFragmentShaderObjectPerFragment = 0;
	}

	// delete shader program object
	if(gShaderProgramObjectPerFragment != 0)
	{
		GLES32.glDeleteProgram(gShaderProgramObjectPerFragment);
		gShaderProgramObjectPerFragment = 0;
	}

	// unlink shader program
	GLES32.glUseProgram(0);
	}
}
