package com.example.sphere_per_vertex;

import androidx.appcompat.widget.AppCompatTextView;

import android.view.MotionEvent;

import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;

import android.opengl.GLSurfaceView;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32;
import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.Arrays;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private GestureDetector gestureDetector;
	
	private final Context context;
	
	private int gVertexShaderObject;
	private int gFragmentShaderObject;
	private int gShaderProgramObject;
	
	private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position = new int[1];
    private int[] vbo_sphere_normal = new int[1];
    private int[] vbo_sphere_element = new int[1];

	int gModelUniform;
	int gViewUniform;
	int gPerspectiveUniform;
	
	int gLightAmbientUniform;
	int gLightSpecularUniform;
	int gLightDiffusedUniform;
	int gLightPossitionUniform;
	
	int gMaterialSpecularUniform;
	int gMaterialAmbientUniform;
	int gMaterialDiffuseUniform;
	int gMaterialShininessUniform;
	
	int gIsLightOnUniform;
	
	int isLightOn = 0;
		
	float lightPossition[] = { 100.0f, 100.0f, 100.0f, 1.0f };
	float lightAmbient[] = { 0.0f, 0.0f, 0.0f };
	float lightDefuse[] = { 0.5f, 0.2f, 0.7f };
	float lightSpecular[] = { 0.7f, 0.7f, 0.7f }; 
	
	float materialAmbient[] = { 0.0f, 0.0f, 0.0f };
	float materialDefuse[] = { 1.0f, 1.0f, 1.0f  };
	float materialSpecular[] = { 1.0f, 1.0f, 1.0f};
	float materialShinyness = 58.0f;

	int numVertices ;
    int numElements;
	
	private float gPerspectiveProjectionMatrix[] = new float[16];

	private static float rotateByAngle = 0.0f;

	int width, height;
	
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		
		context = drawingContext;
		
		// according to EGLContext to current supported version of OpenGL ES
		setEGLContextClientVersion(3);
		
		//set Render for drawing on the GLSurfaceView
		setRenderer(this);
		
		// Render the vuew only when there is change in the drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
				
		gestureDetector = new GestureDetector(context, this, null, false);
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// OpenGL-ES version check
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("HRH: "+ version);
		
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("HRH:  GLSL Version = " + glslVersion);
		
		initialize(gl);
	}
	
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
		draw();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		System.out.println("HRH: Double Tap");
		//setText("Double Tap");
		return (true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return (true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		//setText("Single Tap");
		System.out.println("HRH: Single Tap");
		if (isLightOn == 0)
		{
			isLightOn = 1;
		}
		else
		{
			isLightOn = 0;
		}
		return (true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return (true);
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return (true);
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
		//setText("Long Press");
		System.out.println("HRH: Long Press");
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll");
		System.out.println("HRH: Scroll");
		uninitialize();
		System.exit(0);
		return (true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
			
	}
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return (true);
	}
	
	private void initialize(GL10 gl)
	{
		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es \n" +
			"in vec4 v_position;" +
			"in vec3 v_normals; " +
			"uniform mat4 u_modelMatrix;" +
			"uniform mat4 u_viewMatrix;" +
			"uniform mat4 u_projectionMatrix;" +
			"uniform int u_isLightOn;" +
			"uniform vec3 u_lightAmbient;" +
			"uniform vec3 u_lightSpecular;" +
			"uniform vec3 u_lightDiffuse;" +
			"uniform vec4 u_light_position; " +
			"uniform vec3 u_materialAmbient; " +
			"uniform vec3 u_materialSpecular; " +
			"uniform vec3 u_materialDiffuse; " +
			"uniform float u_materialshininess; " +
			"out vec3 phoung_ads_lighting;" +
			"void main()" +
			"{" +
			"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" +
			"if (u_isLightOn == 1) " +
			"{" +
				"vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" +
				"vec3 transformed_normal = normalize( mat3( u_viewMatrix * u_modelMatrix ) * v_normals); " +
				"vec3 light_direction = normalize( vec3 (u_light_position - eye_coordinates) ); " +
				"vec3 reflection_vector = reflect (-light_direction, transformed_normal); " +
				"vec3 view_vector = normalize ( vec3 ( -eye_coordinates)); " +
				"vec3 ambient = u_lightAmbient * u_materialAmbient; " +
				"vec3 diffuse_light = u_lightDiffuse * u_materialDiffuse * max ( dot (light_direction , transformed_normal), 0.0); " +
				"vec3 specular = u_lightSpecular * u_materialSpecular * pow( max ( dot ( reflection_vector , view_vector), 0.0) , u_materialshininess);" +
				"phoung_ads_lighting = ambient + diffuse_light + specular;" +
			"}" +
			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" +
			"}"
		);
		
		gVertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		GLES32.glShaderSource(gVertexShaderObject, vertexShaderSourceCode);
		
		GLES32.glCompileShader(gVertexShaderObject);
		
		int[] iInfoLogLength = new int[1];
		int[] status = new int[1];
		String szInfoLog = null;
		GLES32.glGetShaderiv(gVertexShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gVertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(gVertexShaderObject);
				System.out.println("HRH: Vertex shader compilation error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Vertex shader program compilation complete successfully");
		
		/*************Fragment shader***************************/
		
		final String fragmentShaderSourceCode = String.format
								(
										"#version 320 es\n"+
										"precision highp float;" +
										"in vec3 phoung_ads_lighting;" +
										"out vec4 FragColor; " +
										"void main()"+
										"{" +
										"FragColor = vec4(phoung_ads_lighting, 1.0); " +
										"}"
								);
								
		gFragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		GLES32.glShaderSource(gFragmentShaderObject, fragmentShaderSourceCode);
								
		GLES32.glCompileShader(gFragmentShaderObject);
								
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetShaderiv(gFragmentShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gFragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(gFragmentShaderObject);
				System.out.println("HRH: Fragment shader compilation error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Frgament shader program compilation complete successfully");
		
		gShaderProgramObject = GLES32.glCreateProgram();
		
		// attach vertex shader to shader program
		GLES32.glAttachShader(gShaderProgramObject, gVertexShaderObject);
	
		// attch fragment shader to shader program
		GLES32.glAttachShader(gShaderProgramObject, gFragmentShaderObject);
		
		// pri-link binding of shader object with vertex shader possition attribute
		GLES32.glBindAttribLocation(gShaderProgramObject, GLESMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
		GLES32.glBindAttribLocation(gShaderProgramObject, GLESMacros.HRH_ATTRIBUTE_NORMAL, "v_normals");
	
		// LINK PROGRAM
		GLES32.glLinkProgram(gShaderProgramObject);
		
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetProgramiv(gShaderProgramObject, GLES32.GL_LINK_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(gShaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetProgramInfoLog(gShaderProgramObject);
				System.out.println("HRH: linking error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Shader program linking complete successfully");		
		
		// get MVP uniform location
		gModelUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
		gViewUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
		gPerspectiveUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");
		gIsLightOnUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_isLightOn");
		
		gLightAmbientUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_lightAmbient");
		gLightSpecularUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_lightSpecular");
		gLightDiffusedUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_lightDiffuse");
		gLightPossitionUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_light_position");
	
		gMaterialDiffuseUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_materialDiffuse");
		gMaterialAmbientUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_materialAmbient");
		gMaterialSpecularUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_materialSpecular");
		gMaterialShininessUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_materialshininess");

		// sphere
		Sphere sphere=new Sphere();
        float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];
        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();

		GLES32.glGenVertexArrays(1, vao_sphere, 0);
		GLES32.glBindVertexArray(vao_sphere[0]);

		// position vbo
		ByteBuffer sphereByteBuffer = ByteBuffer.allocateDirect(sphere_vertices.length * 4);
		sphereByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer sphereFloatBuffer = sphereByteBuffer.asFloatBuffer();
		sphereFloatBuffer.put(sphere_vertices);
		sphereFloatBuffer.position(0);

		GLES32.glGenBuffers(1, vbo_sphere_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_sphere_position[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_vertices.length * 4, sphereFloatBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.HRH_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.HRH_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// normals vbo
		ByteBuffer sphereNormalsByteBuffer = ByteBuffer.allocateDirect(sphere_normals.length * 4);
		sphereNormalsByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer sphereNormalsFloatBuffer = sphereNormalsByteBuffer.asFloatBuffer();
		sphereNormalsFloatBuffer.put(sphere_normals);
		sphereNormalsFloatBuffer.position(0);

		GLES32.glGenBuffers(1, vbo_sphere_normal, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_sphere_normal[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_normals.length * 4, sphereNormalsFloatBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.HRH_ATTRIBUTE_NORMAL, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.HRH_ATTRIBUTE_NORMAL);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		// element vbo
		ByteBuffer sphereElementByteBuffer = ByteBuffer.allocateDirect(sphere_elements.length * 2);
		sphereElementByteBuffer.order(ByteOrder.nativeOrder());
		ShortBuffer sphereElementShortByteBuffer = sphereElementByteBuffer.asShortBuffer();
		sphereElementShortByteBuffer.put(sphere_elements);
		sphereElementShortByteBuffer.position(0);

		GLES32.glGenBuffers(1, vbo_sphere_element, 0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER, sphere_elements.length * 2, sphereElementShortByteBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0);
		
		/************************************************************************************************** */

		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		// we will always cull face for better performance
		GLES32.glEnable(GLES32.GL_CULL_FACE);
			
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		Matrix.setIdentityM(gPerspectiveProjectionMatrix, 0);

	}

	private void resize(int width, int height)
	{
		GLES32.glViewport(0,0,width, height);
		
		Matrix.perspectiveM(gPerspectiveProjectionMatrix,0, 45.0f, ((float) width / (float) height), ((float) height / (float) width), 100.0f);
		
	}
	
	private void draw()
	{
		// Draw background color
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);		

		// START USING OpenGL program object
		GLES32.glUseProgram(gShaderProgramObject);
	
		//OpenGL Drawing
		// set modelview & modelviewProjection matrices to indentity
		float[] viewMatrix = new float[16];
		float[] modelMatrix = new float[16];
		float[] translateMatrix = new float[16];
		float[] rotateMatrix = new float[16];
		float[] scaleMatrix = new float[16];
		
		Matrix.setIdentityM(viewMatrix, 0);

		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, 0.0f, 0.0f, -1.55f);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);
	
		if (isLightOn == 1)
		{
			GLES32.glUniform4fv(gLightPossitionUniform, 1, lightPossition, 0);
			GLES32.glUniform3fv(gLightDiffusedUniform, 1, lightDefuse, 0);
			GLES32.glUniform3fv(gLightAmbientUniform,  1 ,lightAmbient, 0);
			GLES32.glUniform3fv(gLightSpecularUniform, 1 ,lightSpecular, 0);
	
			GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient, 0);
			GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness);
			GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular, 0);
			GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse, 0);
		}
	
		GLES32.glUniform1i(gIsLightOnUniform, isLightOn);
		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(gViewUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(gPerspectiveUniform, 1, false, gPerspectiveProjectionMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		// stop using OpenGL program object
		GLES32.glUseProgram(0);

		rotateByAngle = rotateByAngle + 0.5f;
		
		requestRender();
	}
	
	private void uninitialize()
	{
	
	// destroy vao
	if (vao_sphere[0] != 0)
	{
		GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
		vao_sphere[0] = 0;
	}

	// destroy vbo
	if (vbo_sphere_position[0] != 0)
	{
		GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
		vbo_sphere_position[0] = 0;
	}

	// destroy vbo
	if (vbo_sphere_element[0] != 0)
	{
		GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
		vbo_sphere_element[0] = 0;
	}

	if (vbo_sphere_normal[0] != 0)
	{
		GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
		vbo_sphere_normal[0] = 0;
	}

	// deatch vertex shader from shader program object
	GLES32.glDetachShader(gShaderProgramObject, gVertexShaderObject);

	// detach fragment shader from shader program obejct
	GLES32.glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	if(gVertexShaderObject != 0)
	{
		GLES32.glDeleteShader(gVertexShaderObject);
		gVertexShaderObject = 0;
	}

	// delete fragment shader object
	if(gFragmentShaderObject != 0)
	{
		GLES32.glDeleteShader(gFragmentShaderObject);
		gFragmentShaderObject = 0;
	}

	// delete shader program object
	if(gShaderProgramObject != 0)
	{
		GLES32.glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
	}

	// unlink shader program
	GLES32.glUseProgram(0);
	}
}
