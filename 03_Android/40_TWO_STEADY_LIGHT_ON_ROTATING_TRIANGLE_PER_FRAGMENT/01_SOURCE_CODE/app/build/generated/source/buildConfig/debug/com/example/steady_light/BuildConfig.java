/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.example.steady_light;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.example.steady_light";
  public static final String BUILD_TYPE = "debug";
}
