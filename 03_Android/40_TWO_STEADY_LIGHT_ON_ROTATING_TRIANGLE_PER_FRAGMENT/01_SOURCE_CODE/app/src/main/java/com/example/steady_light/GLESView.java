package com.example.steady_light;

import androidx.appcompat.widget.AppCompatTextView;

import android.view.MotionEvent;

import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;

import android.opengl.GLSurfaceView;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32;
import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.Arrays;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private GestureDetector gestureDetector;
	
	private final Context context;
	
	private int gVertexShaderObject;
	private int gFragmentShaderObject;
	private int gShaderProgramObject;
	
	private int[] gVao_pyramid = new int[1];
	private int[] gVbo_pyramid_position =  new int[1];
	private int[] gVbo_pyramid_normals = new int[1];
	
	private int gModelUniform;
	private int gViewUniform;
	private int gPerspectiveUniform;
	
	private int gLight_0_AmbientUniform;
	private int gLight_0_SpecularUniform;
	private int gLight_0_DiffusedUniform;
	private int gLight_0_PossitionUniform;
	
	
	private int gLight_1_AmbientUniform;
	private int gLight_1_SpecularUniform;
	private int gLight_1_DiffusedUniform;
	private int gLight_1_PossitionUniform;
	
	private int gMaterialSpecularUniform;
	private int gMaterialAmbientUniform;
	private int gMaterialDiffuseUniform;
	private int gMaterialShininessUniform;
	
	private int gIsLightOnUniform;
	
	private int isLightOn = 0;
	
	private float gPerspectiveProjectionMatrix[] = new float[16];

	private static float rotateByAngle = 0.0f;

	int width, hight;

	private float lightPosition_0[] = { 100.0f, 100.0f, 100.0f, 1.0f };
	private float lightAmbient_0[] = { 1.0f, 0.0f, 0.0f};
	private float lightDiffuse_0[] = { 1.0f, 0.0f, 0.0f};
	private float lightSpecular_0[] = { 0.0f, 0.0f, 1.0f};
	
	private float lightPosition_1[] = { -100.0f, 100.0f, 100.0f, 1.0f };
	private float lightAmbient_1[] = { 0.0f, 0.0f, 1.0f};
	private float lightDiffuse_1[] = { 0.0f, 0.0f, 1.0f};
	private float lightSpecular_1[] = { 0.0f, 0.0f, 1.0F };
	
	private float materialAmbient[] = { 0.0f, 0.0f, 0.0f};
	private float materialDiffuse[] = { 1.0f, 1.0f, 1.0f};
	private float materialSpecular[] = { 1.0f, 1.0f, 1.0F };
	private float materialShininess = 128.0f;
	
	private boolean bAnimate = false;
	
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		
		context = drawingContext;
		
		// according to EGLContext to current supported version of OpenGL ES
		setEGLContextClientVersion(3);
		
		//set Render for drawing on the GLSurfaceView
		setRenderer(this);
		
		// Render the vuew only when there is change in the drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
				
		gestureDetector = new GestureDetector(context, this, null, false);
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// OpenGL-ES version check
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("HRH: "+ version);
		
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("HRH:  GLSL Version = " + glslVersion);
		
		initialize(gl);
	}
	
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int hight)
	{
		resize(width, hight);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
		draw();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		System.out.println("HRH: Double Tap");
		//setText("Double Tap");
		return (true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return (true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		//setText("Single Tap");
		System.out.println("HRH: Single Tap");
		if (bAnimate == false)
		{
			bAnimate = true;
		}
		else
		{
			bAnimate = false;
		}
		return (true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return (true);
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return (true);
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
		//setText("Long Press");
		System.out.println("HRH: Long Press");
		if (isLightOn == 0)
		{
			isLightOn = 1;
		}
		else
		{
			isLightOn = 0;
		}
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll");
		System.out.println("HRH: Scroll");
		uninitialize();
		System.exit(0);
		return (true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
			
	}
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return (true);
	}
	
	private void initialize(GL10 gl)
	{
		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es \n" +
			"in vec4 v_position;" +
			"in vec3 v_normals; " +
			"uniform mat4 u_modelMatrix;" +
			"uniform mat4 u_viewMatrix;" +
			"uniform mat4 u_projectionMatrix;" +
			"uniform int u_isLightOn;" +
			"uniform vec4 u_light_0_position; " +
			"uniform vec4 u_light_1_position; " +
			"out vec3 light_direction_0; " +
			"out vec3 light_direction_1; " +
			"out vec3 transformed_normal;" +
			"out vec3 view_vector;" +
			"void main()" +
			"{" +
			"if (u_isLightOn == 1) " +
			"{" +
				"vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" +
				"transformed_normal =  mat3( u_viewMatrix * u_modelMatrix ) * v_normals;" +
				"light_direction_0 = vec3 ( u_light_0_position  - eye_coordinates);" +
				"light_direction_1 = vec3 ( u_light_1_position  - eye_coordinates);" +
				"view_vector =  vec3 ( -eye_coordinates);" +
			"}" +
			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" +
			"}"
		);
		
		gVertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		GLES32.glShaderSource(gVertexShaderObject, vertexShaderSourceCode);
		
		GLES32.glCompileShader(gVertexShaderObject);
		
		int[] iInfoLogLength = new int[1];
		int[] status = new int[1];
		String szInfoLog = null;
		GLES32.glGetShaderiv(gVertexShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gVertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(gVertexShaderObject);
				System.out.println("HRH: Vertex shader compilation error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Vertex shader program compilation complete successfully");
		
		/*************Fragment shader***************************/
		
		final String fragmentShaderSourceCode = String.format(
			"#version 320 es\n"+
			"precision highp float;" +
			"vec3 phoung_ads_lighting;" +
			"out vec4 FragColor; " +
			"in vec3 light_direction_0; " +
			"in vec3 light_direction_1; " +
			"in vec3 transformed_normal;" +
			"in vec3 view_vector;" +
			"vec3 normalized_light_direction_0; " +
			"vec3 normalized_light_direction_1; " +
			"vec3 normalized_transformed_normal;" +
			"vec3 normalized_view_vector;" +
			"uniform highp vec3 u_light_0_Ambient;" +
			"uniform highp vec3 u_light_0_Specular;" +
			"uniform highp vec3 u_light_0_Diffuse;" +
			"uniform highp vec3 u_light_1_Ambient;" +
			"uniform highp vec3 u_light_1_Specular;" +
			"uniform highp vec3 u_light_1_Diffuse;" +
			"uniform highp vec3 u_materialAmbient; " +
			"uniform highp vec3 u_materialSpecular; " +
			"uniform highp vec3 u_materialDiffuse; " +
			"uniform highp float u_materialshininess; " +
			"uniform highp int u_isLightOn;" +
			"void main()" +
			"{" +
			"if (u_isLightOn == 1) " +
			"{" +
				"normalized_light_direction_0 = normalize(light_direction_0);" +
				"normalized_light_direction_1 = normalize(light_direction_1);" +
				"normalized_transformed_normal = normalize(transformed_normal);" +
				"normalized_view_vector = normalize(view_vector);" +
				"vec3 reflection_vector = reflect (-normalized_light_direction_0, normalized_transformed_normal); " +
				"vec3 ambient = u_light_0_Ambient  * u_materialAmbient; " +
				"vec3 diffuse_light = u_light_0_Diffuse * u_materialDiffuse * max ( dot (normalized_light_direction_0 , normalized_transformed_normal), 0.0); " +
				"vec3 specular = u_light_0_Specular  * u_materialSpecular * pow( max ( dot ( reflection_vector , normalized_view_vector), 0.0) , u_materialshininess);" +
				"phoung_ads_lighting = ambient + diffuse_light + specular;" +
				"reflection_vector = reflect (-normalized_light_direction_1, normalized_transformed_normal); " +
				"ambient = u_light_1_Ambient  * u_materialAmbient; " +
				"diffuse_light = u_light_1_Diffuse * u_materialDiffuse * max ( dot (normalized_light_direction_1 , normalized_transformed_normal), 0.0); " +
				"specular = u_light_1_Specular  * u_materialSpecular * pow( max ( dot ( reflection_vector , normalized_view_vector), 0.0) , u_materialshininess);" +
				"phoung_ads_lighting = phoung_ads_lighting + ambient + diffuse_light + specular;" +
			"}" +
			"else" +
			"{" +
				"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" +
			"}" +
			"FragColor = vec4(phoung_ads_lighting, 1.0); " +
			"}"
			);
								
		gFragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		GLES32.glShaderSource(gFragmentShaderObject, fragmentShaderSourceCode);
								
		GLES32.glCompileShader(gFragmentShaderObject);
								
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetShaderiv(gFragmentShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gFragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(gFragmentShaderObject);
				System.out.println("HRH: Fragment shader compilation error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Frgament shader program compilation complete successfully");
		
		gShaderProgramObject = GLES32.glCreateProgram();
		
		// attach vertex shader to shader program
		GLES32.glAttachShader(gShaderProgramObject, gVertexShaderObject);
	
		// attch fragment shader to shader program
		GLES32.glAttachShader(gShaderProgramObject, gFragmentShaderObject);
		
		// pri-link binding of shader object with vertex shader possition attribute
		GLES32.glBindAttribLocation(gShaderProgramObject, GLESMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
		GLES32.glBindAttribLocation(gShaderProgramObject, GLESMacros.HRH_ATTRIBUTE_NORMAL, "v_normals");
	
		// LINK PROGRAM
		GLES32.glLinkProgram(gShaderProgramObject);
		
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetProgramiv(gShaderProgramObject, GLES32.GL_LINK_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(gShaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetProgramInfoLog(gShaderProgramObject);
				System.out.println("HRH: linking error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Shader program linking complete successfully");		
		
		// get MVP uniform location
		gModelUniform =       GLES32.glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
		gViewUniform =        GLES32.glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
		gPerspectiveUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");
		gIsLightOnUniform =   GLES32.glGetUniformLocation(gShaderProgramObject, "u_isLightOn");
		
		gLight_0_AmbientUniform =   GLES32.glGetUniformLocation(gShaderProgramObject, "u_light_0_Ambient");
		gLight_0_SpecularUniform =  GLES32.glGetUniformLocation(gShaderProgramObject, "u_light_0_Specular");
		gLight_0_DiffusedUniform =  GLES32.glGetUniformLocation(gShaderProgramObject, "u_light_0_Diffuse");
		gLight_0_PossitionUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_light_0_position");
	
		gLight_1_AmbientUniform =   GLES32.glGetUniformLocation(gShaderProgramObject, "u_light_1_Ambient");
		gLight_1_SpecularUniform =  GLES32.glGetUniformLocation(gShaderProgramObject, "u_light_1_Specular");
		gLight_1_DiffusedUniform =  GLES32.glGetUniformLocation(gShaderProgramObject, "u_light_1_Diffuse");
		gLight_1_PossitionUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_light_1_position");
	
		gMaterialDiffuseUniform =   GLES32.glGetUniformLocation(gShaderProgramObject, "u_materialDiffuse");
		gMaterialAmbientUniform =   GLES32.glGetUniformLocation(gShaderProgramObject, "u_materialAmbient");
		gMaterialSpecularUniform =  GLES32.glGetUniformLocation(gShaderProgramObject, "u_materialSpecular");
		gMaterialShininessUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_materialshininess");
		
	/**********************************TRIANGLE***************************************************** */

		final float pyramidVertices[] = new float[]
		{
					// FRONT FACE
					0.0f, 1.0f, 0.0f,
					-1.0f,-1.0f, 1.0f,
					1.0f, -1.0f,1.0f,
					
					// RIGHT FACE
					0.0f, 1.0f, 0.0f,
					1.0f, -1.0f, 1.0f,
					1.0f, -1.0f, -1.0f,

					// BACK FACE
					0.0f, 1.0f, 0.0f,
					1.0f, -1.0f,-1.0f,
					-1.0f, -1.0f,-1.0f,

					// LEFT FACE
					0.0f, 1.0f, 0.0f,
					-1.0f, -1.0f,-1.0f,
					-1.0f, -1.0f,1.0f,
				};

		GLES32.glGenVertexArrays(1, gVao_pyramid, 0);
		GLES32.glBindVertexArray(gVao_pyramid[0]);
		
		System.out.println("HRH: vao bind");
		
		// position
		GLES32.glGenBuffers(1, gVbo_pyramid_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, gVbo_pyramid_position[0]);
		
		System.out.println("HRH: vbo bind");
		
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect( pyramidVertices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(pyramidVertices);
		verticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, pyramidVertices.length * 4, verticesBuffer, GLES32.GL_STATIC_DRAW);
		
		GLES32.glVertexAttribPointer(GLESMacros.HRH_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);

		GLES32.glEnableVertexAttribArray(GLESMacros.HRH_ATTRIBUTE_POSITION);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0); // glEnd() for vbo	

		// NORMALS
		float pyramid_normals[] = {
		0.0f, 0.447214f, 0.894427f, //FRONT	
		0.0f, 0.447214f, 0.894427f, //FRONT	
		0.0f, 0.447214f, 0.894427f, //FRONT			
		
		0.894427f, 0.447214f, 0.0f, // RIGHT
		0.894427f, 0.447214f, 0.0f, // RIGHT
		0.894427f, 0.447214f, 0.0f, // RIGHT
		
		0.0f, 0.447214f, -0.894427f, // BACK
		0.0f, 0.447214f, -0.894427f, // BACK
		0.0f, 0.447214f, -0.894427f, // BACK
		
		-0.894427f, 0.447214f, 0.0f, // LEFT
		-0.894427f, 0.447214f, 0.0f, // LEFT
		-0.894427f, 0.447214f, 0.0f // LEFT
		};

		GLES32.glGenBuffers(1, gVbo_pyramid_normals, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, gVbo_pyramid_normals[0]);

		byteBuffer = ByteBuffer.allocateDirect( pyramid_normals.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(pyramid_normals);
		verticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, pyramid_normals.length * 4, verticesBuffer, GLES32.GL_STATIC_DRAW);
		
		GLES32.glVertexAttribPointer(GLESMacros.HRH_ATTRIBUTE_NORMAL, 3, GLES32.GL_FLOAT, false, 0, 0);

		GLES32.glEnableVertexAttribArray(GLESMacros.HRH_ATTRIBUTE_NORMAL);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0); // glEnd() for vbo

		GLES32.glBindVertexArray(0);

		/************************************************************************************************** */

		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		// we will always cull face for better performance
		GLES32.glEnable(GLES32.GL_CULL_FACE);
			
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		Matrix.setIdentityM(gPerspectiveProjectionMatrix, 0);

	}

	private void resize(int width, int hight)
	{
		GLES32.glViewport(0,0,width, hight);
		
		Matrix.perspectiveM(gPerspectiveProjectionMatrix,0, 45.0f, ((float) width / (float) hight), ((float) hight / (float) width), 100.0f);
		
	}
	
	private void draw()
	{
		// Draw background color
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);		

		// START USING OpenGL program object
		GLES32.glUseProgram(gShaderProgramObject);
	
		//OpenGL Drawing
		// set modelview & modelviewProjection matrices to indentity
		float[] modelMatrix = new float[16];
		float[] viewMatrix = new float[16];
		float[] translateMatrix = new float[16];
		float[] rotateMatrix = new float[16];
		float[] scaleMatrix = new float[16];

		Matrix.setIdentityM(viewMatrix, 0);
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, 0.0f, 0.0f, -6.0f);
		
		Matrix.setIdentityM(rotateMatrix, 0);
		Matrix.rotateM(rotateMatrix, 0, rotateByAngle, 0.0f, 1.0f, 0.0f);

		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, translateMatrix, 0, rotateMatrix, 0); 
	
		if (isLightOn == 1)
		{
			GLES32.glUniform4fv(gLight_0_PossitionUniform, 1, lightPosition_0, 0);
			GLES32.glUniform3fv(gLight_0_DiffusedUniform, 1, lightDiffuse_0, 0);
			GLES32.glUniform3fv(gLight_0_AmbientUniform,  1 , lightAmbient_0, 0);
			GLES32.glUniform3fv(gLight_0_SpecularUniform, 1 , lightSpecular_0, 0);
	
			GLES32.glUniform4fv(gLight_1_PossitionUniform, 1, lightPosition_1, 0);
			GLES32.glUniform3fv(gLight_1_DiffusedUniform, 1, lightDiffuse_1, 0);
			GLES32.glUniform3fv(gLight_1_AmbientUniform, 1, lightAmbient_1, 0);
			GLES32.glUniform3fv(gLight_1_SpecularUniform, 1, lightSpecular_1, 0);
	
			GLES32.glUniform3fv(gMaterialAmbientUniform, 1,  materialAmbient, 0);
			GLES32.glUniform1f(gMaterialShininessUniform, materialShininess);
			GLES32.glUniform3fv(gMaterialSpecularUniform, 1,  materialSpecular, 0);
			GLES32.glUniform3fv(gMaterialDiffuseUniform, 1,  materialDiffuse, 0);
		}
	
		GLES32.glUniform1i(gIsLightOnUniform, isLightOn);
		GLES32.glUniformMatrix4fv(gModelUniform, 1, false,  modelMatrix, 0);
		GLES32.glUniformMatrix4fv(gViewUniform, 1, false,  viewMatrix, 0);
		GLES32.glUniformMatrix4fv(gPerspectiveUniform, 1, false,  gPerspectiveProjectionMatrix, 0);

		
		GLES32.glBindVertexArray(gVao_pyramid[0]); // bind vao 
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 12); 
		GLES32.glBindVertexArray(0); // unbind vao 

		// stop using OpenGL program object
		GLES32.glUseProgram(0);

		if(bAnimate == true)
		{
			rotateByAngle = rotateByAngle + 0.5f;
		}

		requestRender();
	}
	
	private void uninitialize()
	{
	
	// destroy vao
	if (gVao_pyramid[0] != 0)
	{
		GLES32.glDeleteVertexArrays(1, gVao_pyramid, 0);
		gVao_pyramid[0] = 0;
	}

	// destroy vbo
	if (gVbo_pyramid_position[0] != 0)
	{
		GLES32.glDeleteBuffers(1, gVbo_pyramid_position, 0);
		gVbo_pyramid_position[0] = 0;
	}

	if (gVbo_pyramid_normals[0] != 0)
	{
		GLES32.glDeleteBuffers(1, gVbo_pyramid_normals, 0);
		gVbo_pyramid_normals[0] = 0;
	}

	// deatch vertex shader from shader program object
	GLES32.glDetachShader(gShaderProgramObject, gVertexShaderObject);

	// detach fragment shader from shader program obejct
	GLES32.glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	if(gVertexShaderObject != 0)
	{
		GLES32.glDeleteShader(gVertexShaderObject);
		gVertexShaderObject = 0;
	}

	// delete fragment shader object
	if(gFragmentShaderObject != 0)
	{
		GLES32.glDeleteShader(gFragmentShaderObject);
		gFragmentShaderObject = 0;
	}

	// delete shader program object
	if(gShaderProgramObject != 0)
	{
		GLES32.glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
	}

	// unlink shader program
	GLES32.glUseProgram(0);
	}
}
