/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.example.texturedPyramidCube;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.example.texturedPyramidCube";
  public static final String BUILD_TYPE = "debug";
}
