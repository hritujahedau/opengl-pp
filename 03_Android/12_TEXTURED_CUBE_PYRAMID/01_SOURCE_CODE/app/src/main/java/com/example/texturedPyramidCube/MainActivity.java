package com.example.texturedPyramidCube;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import androidx.appcompat.widget.AppCompatTextView; // widgets - group of ui elements
import android.graphics.Color;
import android.view.Gravity;
import android.content.Context;
import android.view.View;
import android.content.pm.ActivityInfo;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
       // super.onCreate(savedInstanceState);

	//getWindow().getDecorView().setBackgroundColor(Color.rgb(0,0,0));

	getWindow().getDecorView().setSystemUiVisibility
	(
		View.SYSTEM_UI_FLAG_IMMERSIVE |
		View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
		View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
		View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
		View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
		View.SYSTEM_UI_FLAG_FULLSCREEN
	);
	
	super.onCreate(savedInstanceState);
	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

	GLESView glesView = new GLESView(this);
	
	setContentView(glesView);
    }
}