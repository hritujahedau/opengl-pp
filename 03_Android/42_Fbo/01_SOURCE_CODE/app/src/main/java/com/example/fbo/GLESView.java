package com.example.fbo;

import androidx.appcompat.widget.AppCompatTextView;

import android.view.MotionEvent;

import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;

import android.opengl.GLSurfaceView;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32;
import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.Arrays;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private GestureDetector gestureDetector;
	
	private final Context context;
	
	private int gVertexShaderObject_1;
	private int gVertexShaderObject_2;
	
	private int gFragmentShaderObject_1;
	private int gFragmentShaderObject_2;
	
	private int gShaderProgramObject_1;
	private int gShaderProgramObject_2;
	
	private int[] gVao = new int[1];
	private int[] gVbo_position = new int[1];
	private int[] gVbo_index = new int[1];
	
	private int[] fbo = new int[1];
	
	private int gMVUniform_1;
	private int gProjectionUniform_1;
	
	private int gMVUniform_2;
	private int gProjectionUniform_2;
	
	private int gTexCoordUniform;
	
	private int[] color_texture = new int[1];
	private int[] depth_texture = new int[1];
	
	private float perspectiveProjectionMatrix[] = new float[16];

	float rotateByAngle = 0.0f;

///	enum draw_buffers { GLES32.GL_COLOR_ATTACHMENT0 ; }

	int width, height;
	
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		
		context = drawingContext;
		
		// according to EGLContext to current supported version of OpenGL ES
		setEGLContextClientVersion(3);
		
		//set Render for drawing on the GLSurfaceView
		setRenderer(this);
		
		// Render the vuew only when there is change in the drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
				
		gestureDetector = new GestureDetector(context, this, null, false);
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// OpenGL-ES version check
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("HRH: "+ version);
		
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("HRH:  GLSL Version = " + glslVersion);
		
		initialize(gl);
	}
	
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int hight)
	{
		resize(width, hight);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
		draw();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		System.out.println("HRH: Double Tap");
		//setText("Double Tap");
		return (true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return (true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		//setText("Single Tap");
		System.out.println("HRH: Single Tap");
		return (true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return (true);
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return (true);
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
		//setText("Long Press");
		System.out.println("HRH: Long Press");
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll");
		System.out.println("HRH: Scroll");
		uninitialize();
		System.exit(0);
		return (true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
			
	}
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return (true);
	}
	
	private void initialize(GL10 gl)
	{
		final String vertexShaderSourceCode_1 = String.format
		(
			"#version 320 es													\n" +
			"layout (location = 0 ) in vec4 vPosition;							\n" +
			"layout (location = 1 ) in vec2 texcoord;							\n" +
			"uniform mat4 u_mvMatrix;											\n" +
			"uniform mat4 u_projectionMatrix;									\n" +
			"out vec4 color_out;												\n" +
			"out vec2 texcoord_out;												\n" +
			"void main()														\n" +
			"																	\n" +
			"{																	\n" +
			"gl_Position = u_projectionMatrix * u_mvMatrix * vPosition;			\n" +
			"color_out = vPosition * 2.0 + vec4(0.5, 0.5, 0.5, 0.0);			\n" +
			"texcoord_out = texcoord;											\n" +
			"}"
		);
		
		gVertexShaderObject_1 = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		GLES32.glShaderSource(gVertexShaderObject_1, vertexShaderSourceCode_1);
		
		GLES32.glCompileShader(gVertexShaderObject_1);
		
		int[] iInfoLogLength = new int[1];
		int[] status = new int[1];
		String szInfoLog = null;
		GLES32.glGetShaderiv(gVertexShaderObject_1, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gVertexShaderObject_1, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(gVertexShaderObject_1);
				System.out.println("HRH: 1:Vertex shader compilation error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: 1:Vertex shader program compilation complete successfully");
		
		/*************Fragment shader***************************/
		
		final String fragmentShaderSourceCode_1 = String.format
		(
			"#version 320 es																	\n" +
			"precision highp float;																\n" +
			"out vec4 color; 																	\n" +
			"in vec4 color_out;																	\n" +
			"in vec2 texcoord_out;																\n" +
			"void main()																		\n" +
			"{																					\n "+
			"color = sin(color_out * vec4(40.0, 20.0, 30.0, 1.0)) * 0.5 + vec4(0.5);			\n" +
			"}"
		);
								
		gFragmentShaderObject_1 = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		GLES32.glShaderSource(gFragmentShaderObject_1, fragmentShaderSourceCode_1);
								
		GLES32.glCompileShader(gFragmentShaderObject_1);
								
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetShaderiv(gFragmentShaderObject_1, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gFragmentShaderObject_1, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(gFragmentShaderObject_1);
				System.out.println("HRH: 1: Fragment shader compilation error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: 1: Frgament shader program compilation complete successfully");
		
		gShaderProgramObject_1 = GLES32.glCreateProgram();
		
		// attach vertex shader to shader program
		GLES32.glAttachShader(gShaderProgramObject_1, gVertexShaderObject_1);
	
		// attch fragment shader to shader program
		GLES32.glAttachShader(gShaderProgramObject_1, gFragmentShaderObject_1);
		
		// LINK PROGRAM
		GLES32.glLinkProgram(gShaderProgramObject_1);
		
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetProgramiv(gShaderProgramObject_1, GLES32.GL_LINK_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(gShaderProgramObject_1, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetProgramInfoLog(gShaderProgramObject_1);
				System.out.println("HRH: 1: linking error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: 1: First Shader program linking complete successfully");		

		// second program starts here
		gVertexShaderObject_2 = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		GLES32.glShaderSource(gVertexShaderObject_2, vertexShaderSourceCode_1);
		
		GLES32.glCompileShader(gVertexShaderObject_2);
		
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetShaderiv(gVertexShaderObject_2, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gVertexShaderObject_2, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(gVertexShaderObject_2);
				System.out.println("HRH: 2: Vertex shader compilation error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: 2: Vertex shader program compilation complete successfully");
		
		/*************Fragment shader***************************/
		
		final String fragmentShaderSourceCode_2 = String.format
		(
			"#version 320 es																	\n" +
			"precision highp float;																\n" +
			"uniform highp sampler2D tex;														\n" +
			"out vec4 color;																	\n" +
			"in vec4 color_out;																	\n" +
			"in vec2 texcoord_out;																\n" +
			"void main()																		\n" +
			"{																					\n" +
			"	color = mix(color_out, texture ( tex, texcoord_out), 0.7);						\n" +
			"}																					\n"
		);
								
		gFragmentShaderObject_2 = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		GLES32.glShaderSource(gFragmentShaderObject_2, fragmentShaderSourceCode_2);
								
		GLES32.glCompileShader(gFragmentShaderObject_2);
								
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetShaderiv(gFragmentShaderObject_2, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gFragmentShaderObject_2, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(gFragmentShaderObject_2);
				System.out.println("HRH: 2: Fragment shader compilation error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: 2: Frgament shader program compilation complete successfully");
		
		gShaderProgramObject_2 = GLES32.glCreateProgram();
		
		// attach vertex shader to shader program
		GLES32.glAttachShader(gShaderProgramObject_2, gVertexShaderObject_2);
	
		// attch fragment shader to shader program
		GLES32.glAttachShader(gShaderProgramObject_2, gFragmentShaderObject_2);
		
		// LINK PROGRAM
		GLES32.glLinkProgram(gShaderProgramObject_2);
		
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetProgramiv(gShaderProgramObject_2, GLES32.GL_LINK_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(gShaderProgramObject_2, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetProgramInfoLog(gShaderProgramObject_2);
				System.out.println("HRH: linking error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: 2: Second Shader program linking complete successfully");		

		// get MVP uniform location
		gMVUniform_1 = GLES32.glGetUniformLocation(gShaderProgramObject_1, "u_mvMatrix");
		gProjectionUniform_1 = GLES32.glGetUniformLocation(gShaderProgramObject_1, "u_projectionMatrix");
	
		gMVUniform_2 = GLES32.glGetUniformLocation(gShaderProgramObject_2, "u_mvMatrix");
		gProjectionUniform_2 = GLES32.glGetUniformLocation(gShaderProgramObject_2, "u_projectionMatrix");
	
		final short vertex_indices[] = new short[]
		{
			0, 1, 2,
			2, 1, 3,
			2, 3, 4,
			4, 3, 5,
			4, 5, 6,
			6, 5, 7,
			6, 7, 0,
			0, 7, 1,
			6, 0, 2,
			2, 4, 6,
			7, 5, 3,
			7, 3, 1
		};
	
		final float vertex_data[] = new float[]
		{
			// Position                 Tex Coord
			-0.25f, -0.25f,  0.25f,      0.0f, 1.0f,
			-0.25f, -0.25f, -0.25f,      0.0f, 0.0f,
			0.25f, -0.25f, -0.25f,      1.0f, 0.0f,
	
			0.25f, -0.25f, -0.25f,      1.0f, 0.0f,
			0.25f, -0.25f,  0.25f,      1.0f, 1.0f,
			-0.25f, -0.25f,  0.25f,      0.0f, 1.0f,
	
			0.25f, -0.25f, -0.25f,      0.0f, 0.0f,
			0.25f,  0.25f, -0.25f,      1.0f, 0.0f,
			0.25f, -0.25f,  0.25f,      0.0f, 1.0f,
	
			0.25f,  0.25f, -0.25f,      1.0f, 0.0f,
			0.25f,  0.25f,  0.25f,      1.0f, 1.0f,
			0.25f, -0.25f,  0.25f,      0.0f, 1.0f,
	
			0.25f,  0.25f, -0.25f,      1.0f, 0.0f,
			-0.25f,  0.25f, -0.25f,      0.0f, 0.0f,
			0.25f,  0.25f,  0.25f,      1.0f, 1.0f,
	
			-0.25f,  0.25f, -0.25f,      0.0f, 0.0f,
			-0.25f,  0.25f,  0.25f,      0.0f, 1.0f,
			0.25f,  0.25f,  0.25f,      1.0f, 1.0f,
	
			-0.25f,  0.25f, -0.25f,      1.0f, 0.0f,
			-0.25f, -0.25f, -0.25f,      0.0f, 0.0f,
			-0.25f,  0.25f,  0.25f,      1.0f, 1.0f,
	
			-0.25f, -0.25f, -0.25f,      0.0f, 0.0f,
			-0.25f, -0.25f,  0.25f,      0.0f, 1.0f,
			-0.25f,  0.25f,  0.25f,      1.0f, 1.0f,
	
			-0.25f,  0.25f, -0.25f,      0.0f, 1.0f,
			0.25f,  0.25f, -0.25f,      1.0f, 1.0f,
			0.25f, -0.25f, -0.25f,      1.0f, 0.0f,
	
			0.25f, -0.25f, -0.25f,      1.0f, 0.0f,
			-0.25f, -0.25f, -0.25f,      0.0f, 0.0f,
			-0.25f,  0.25f, -0.25f,      0.0f, 1.0f,
	
			-0.25f, -0.25f,  0.25f,      0.0f, 0.0f,
			0.25f, -0.25f,  0.25f,      1.0f, 0.0f,
			0.25f,  0.25f,  0.25f,      1.0f, 1.0f,
	
			0.25f,  0.25f,  0.25f,      1.0f, 1.0f,
			-0.25f,  0.25f,  0.25f,      0.0f, 1.0f,
			-0.25f, -0.25f,  0.25f,      0.0f, 0.0f,
		};
	
		System.out.println("HRH: after arrays alocation");

		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(vertex_data.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
	
		FloatBuffer floatBuffer = byteBuffer.asFloatBuffer();
		floatBuffer.put(vertex_data);
		floatBuffer.position(0);
	
		byteBuffer = ByteBuffer.allocateDirect(vertex_indices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
	
		ShortBuffer shortBuffer = byteBuffer.asShortBuffer();
		shortBuffer.put(vertex_indices);
		shortBuffer.position(0);

		System.out.println("HRH: after buffer creation");

		GLES32.glGenVertexArrays(1, gVao, 0);
		GLES32.glBindVertexArray(gVao[0]);

		System.out.println("HRH: after vao");
	
		GLES32.glGenBuffers(1, gVbo_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, gVbo_position[0]);

		System.out.println("HRH: after vbo ");
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, vertex_data.length * 4, floatBuffer, GLES32.GL_STATIC_DRAW);
		
		System.out.println("HRH: after buffer data");

		GLES32.glVertexAttribPointer(0, 3, GLES32.GL_FLOAT, false, (5 * 4), 0);

		System.out.println("HRH: after vertex attrib pointer");

		GLES32.glEnableVertexAttribArray(0);

		System.out.println("HRH: after position vertex attrib pointer");
	
		GLES32.glVertexAttribPointer(1, 2, GLES32.GL_FLOAT, false, 5 * 4, (3 * 4));
		GLES32.glEnableVertexAttribArray(1);

		System.out.println("HRH: after texcoord attrib pointer");

		System.out.println("HRH: after vao");

		GLES32.glGenBuffers(1, gVbo_index, 0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, gVbo_index[0]);
		GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER, vertex_indices.length * 4, shortBuffer, GLES32.GL_STATIC_DRAW);

		GLES32.glEnable(GLES32.GL_CULL_FACE);
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		GLES32.glGenFramebuffers(1, fbo, 0);
		GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, fbo[0]);
	
		System.out.println("HRH: frame buffer creation color_texture");
	
		GLES32.glGenTextures(1, color_texture, 0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, color_texture[0]);
	
		GLES32.glTexStorage2D(GLES32.GL_TEXTURE_2D, 9, GLES32.GL_RGBA8, 512, 512);
	
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
	
		System.out.println("HRH: frame buffer creation depth_texture");
	
		GLES32.glGenTextures(1, depth_texture, 0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, depth_texture[0]);
	
		GLES32.glTexStorage2D(GLES32.GL_TEXTURE_2D, 9, GLES32.GL_DEPTH_COMPONENT32F, 512, 512);
	
		GLES32.glFramebufferTexture(GLES32.GL_FRAMEBUFFER, GLES32.GL_COLOR_ATTACHMENT0, color_texture[0], 0);
		GLES32.glFramebufferTexture(GLES32.GL_FRAMEBUFFER, GLES32.GL_DEPTH_ATTACHMENT, depth_texture[0], 0);
	
		
	//	GLES32.glDrawBuffers(1, draw_buffers);

		GLES32.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
		
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);

	}
	
	private void resize(int width, int height)
	{
		this.width = width;
		this.height = height;
		GLES32.glViewport(0,0,width, height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix,0, 45.0f, ((float) width / (float) height), ((float) height / (float) width), 100.0f);
		
	}
	
	private void draw()
	{
		// Draw background color
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);		
	
		//OpenGL Drawing
 		float blue[] = new float[] { 0.0f, 0.0f, 0.3f, 1.0f };
		float[] one = new float[] { 1.0f };
		
		
		float[] modelViewMatrix = new float[16];
		float[] translateMatrix = new float[16];
		float[] rotateMatrix = new float[16];
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, 0.0f, 0.0f, -2.0f);

		Matrix.setIdentityM(rotateMatrix, 0);
		Matrix.rotateM(rotateMatrix, 0, rotateByAngle, 1.0f, 1.0f, 1.0f);

		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.multiplyMM(modelViewMatrix, 0, translateMatrix, 0, rotateMatrix, 0);
	
		GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, fbo[0]);

		GLES32.glViewport(0, 0, 512, 512);
		float[] green = new float [] {0.0f, 1.0f, 0.0f};
		GLES32.glClearBufferfv(GLES32.GL_COLOR, 0, green, 0);
		GLES32.glClearBufferfi(GLES32.GL_DEPTH_STENCIL, 0, 1.0f, 0);

		GLES32.glUseProgram(gShaderProgramObject_1);

		GLES32.glUniformMatrix4fv(gMVUniform_1, 1, false, modelViewMatrix, 0);
		GLES32.glUniformMatrix4fv(gProjectionUniform_1, 1, false, perspectiveProjectionMatrix, 0);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 36);

		GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, 0);

		GLES32.glViewport(0, 0, width, height);
		GLES32.glClearBufferfv(GLES32.GL_COLOR, 0, blue, 0);
		GLES32.glClearBufferfv(GLES32.GL_DEPTH, 0, one, 0);

		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, color_texture[0]);

		GLES32.glUseProgram(gShaderProgramObject_2);

		GLES32.glUniformMatrix4fv(gMVUniform_1, 1, false, modelViewMatrix, 0);
		GLES32.glUniformMatrix4fv(gProjectionUniform_1, 1, false, perspectiveProjectionMatrix, 0);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 36);

		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);

		GLES32.glUseProgram(0);
		
		rotateByAngle = rotateByAngle + 1.0f;
		if(rotateByAngle > 360.0f)
		{
			rotateByAngle = 0.0f;
		}

		requestRender();
	}
	
	private void uninitialize()
	{
	
	// destroy vao
	if (gVao[0] != 0)
	{
		GLES32.glDeleteVertexArrays(1, gVao, 0);
		gVao[0] = 0;
	}

	// destroy vbo
	if (gVbo_position[0] != 0)
	{
		GLES32.glDeleteBuffers(1, gVbo_position, 0);
		gVbo_position[0] = 0;
	}

	// destroy vbo
	if (gVbo_index[0] != 0)
	{
		GLES32.glDeleteBuffers(1, gVbo_index, 0);
		gVbo_index[0] = 0;
	}

	// detach vertex shader from per vertex shader program object
	GLES32.glDetachShader(gShaderProgramObject_1, gVertexShaderObject_1);

	// detach fragment shader 
	GLES32.glDetachShader(gShaderProgramObject_1, gFragmentShaderObject_1);

	// delete vertex shader object
	if(gVertexShaderObject_1 != 0)
	{
		GLES32.glDeleteShader(gVertexShaderObject_1);
		gVertexShaderObject_1 = 0;
	}

	// delete fragment shader object
	if(gFragmentShaderObject_1 != 0)
	{
		GLES32.glDeleteShader(gFragmentShaderObject_1);
		gFragmentShaderObject_1 = 0;
	}

	// delete vertex shader program object
	if(gShaderProgramObject_1 != 0)
	{
		GLES32.glDeleteProgram(gShaderProgramObject_1);
		gShaderProgramObject_1 = 0;
	}

	// detach vertex shader from per vertex shader program object
	GLES32.glDetachShader(gShaderProgramObject_2, gVertexShaderObject_2);

	// detach fragment shader 
	GLES32.glDetachShader(gShaderProgramObject_2, gFragmentShaderObject_2);

	// delete vertex shader object
	if(gVertexShaderObject_2 != 0)
	{
		GLES32.glDeleteShader(gVertexShaderObject_2);
		gVertexShaderObject_2 = 0;
	}

	// delete fragment shader object
	if(gFragmentShaderObject_2 != 0)
	{
		GLES32.glDeleteShader(gFragmentShaderObject_2);
		gFragmentShaderObject_2 = 0;
	}

	// delete vertex shader program object
	if(gShaderProgramObject_2 != 0)
	{
		GLES32.glDeleteProgram(gShaderProgramObject_2);
		gShaderProgramObject_2 = 0;
	}

	// unlink shader program
	GLES32.glUseProgram(0);
			
	}
}
