package com.example.twenty_four_sphere;

import androidx.appcompat.widget.AppCompatTextView;

import android.view.MotionEvent;

import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;

import android.opengl.GLSurfaceView;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32;
import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.Arrays;
import java.lang.Math;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private GestureDetector gestureDetector;
	
	private final Context context;
	
	private int gVertexShaderObjectPerFragment;
	private int gFragmentShaderObjectPerFragment;
	private int gShaderProgramObjectPerFragment;

	private int gVertexShaderObjectPerVertex;
	private int gFragmentShaderObjectPerVertex;
	private int gShaderProgramObjectPerVertex;

	private int gCurrentShaderProgram;
	
	private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position = new int[1];
    private int[] vbo_sphere_normal = new int[1];
    private int[] vbo_sphere_element = new int[1];

	private int gModelUniform;
	private int gViewUniform;
	private int gPerspectiveUniform;
	
	private int gLightAmbientUniform;
	private int gLightSpecularUniform;
	private int gLightDiffusedUniform;
	private int gLightPossitionUniform;
	
	private int gMaterialSpecularUniform;
	private int gMaterialAmbientUniform;
	private int gMaterialDiffuseUniform;
	private int gMaterialShininessUniform;
	
	private int gIsLightOnUniform;
	
	private int isPerFragmentLight = 0, isPerVertexLight = 0, isLightOn = 0;

	private int gIsPerVertexShaderUniform;
	private int gIsPerFragmentShaderUniform;
		
	private float lightPosition[] = { 0.0f, 0.0f, 0.0f, 1.0f};						
	private float lightAmbient[] = {  0.0f, 0.0f, 0.0f};							
	private float lightDiffuse[] = {  1.0f, 1.0f, 1.0f};								
	private float lightSpecular[] = { 1.0f, 1.0f, 1.0f};

	private int numVertices ;
	private int numElements;
	
	private float gPerspectiveProjectionMatrix[] = new float[16];

	private static float rotateByAngle = 0.0f;
	private float angle_red = 360.0f, angle_blue = 0.0f, angle_green = 360.0f;
	private int key_x = 0, key_y = 0, key_z = 0;
	private float angle_for_light = 0.0f;

	private int width, height;

	private boolean bAnimate = false;

	private int countForPerVertex = 0, countForPerFragment = 0;
	// first column
	// 1
	private float materialAmbient_emerald[] = { 0.0215f, 0.1745f, 0.0215f, 1.0f };
	private float materialDefuse_emerald[] = { 0.07568f, 0.61424f, 0.07568f, 1.0f };
	private float materialSpecular_emerald[] = { 0.633f, 0.727811f, 0.633f, 1.0f };
	private float materialShinyness_emerald = 0.6f * 128.0f;
	
	// 2
	private float materialAmbient_jade[] = { 0.135f, 0.2225f, 0.1575f, 1.0f };
	private float materialDefuse_jade[] = { 0.54f, 0.89f, 0.63f, 1.0f };
	private float materialSpecular_jade[] = { 0.316228f, 0.316228f, 0.316228f, 1.0f };
	private float materialShinyness_jade = 0.1f * 128.0f;
	
	// 3
	private float materialAmbient_obsidian[] = { 0.05375f, 0.05f, 0.06625f, 1.0f };
	private float materialDefuse_obsidian[] = { 0.18275f, 0.17f, 0.22525f, 1.0f };
	private float materialSpecular_obsidian[] = { 0.332741f, 0.328634f, 0.346435f, 1.0f };
	private float materialShinyness_obsidian = 0.3f * 128.0f;
	
	//4 pearl
	private float materialAmbient_pearl[] = { 0.25f, 0.20725f, 0.20725f, 1.0f };
	private float materialDefuse_pearl[] = { 1.0f, 0.829f, 0.829f, 1.0f };
	private float materialSpecular_pearl[] = { 0.296648f, 0.296648f, 0.296648f, 1.0f };
	private float materialShinyness_pearl = 0.088f * 128.0f;
	
	//5 ruby 
	private float materialAmbient_ruby[] = { 0.1745f, 0.01175f, 0.01175f, 1.0f };
	private float materialDefuse_ruby[] = { 0.61424f, 0.04136f, 0.04136f, 1.0f };
	private float materialSpecular_ruby[] = { 0.727811f, 0.626959f, 0.626959f, 1.0f };
	private float materialShinyness_ruby = 0.6f * 128.0f;

	//6 turquoise
	private float materialAmbient_turquoise[] = { 0.1f, 0.18725f, 0.1745f, 1.0f };
	private float materialDefuse_turquoise[] = { 0.396f, 0.74151f, 0.69102f, 1.0f };
	private float materialSpecular_turquoise[] = { 0.297254f, 0.30829f, 0.306678f, 1.0f };
	private float materialShinyness_turquoise = 0.1f * 128.0f;

	// second column
	// 7 brass 
	private float materialAmbient_brass[] = { 0.329412f, 0.223529f, 0.027451f, 1.0f };
	private float materialDefuse_brass[] = { 0.780392f, 0.568627f, 0.113725f, 1.0f };
	private float materialSpecular_brass[] = { 0.992157f, 0.941176f, 0.807843f, 1.0f };
	private float materialShinyness_brass = 0.21794872f * 128.0f;

	// 8 bronze
	private float materialAmbient_bronze[] = { 0.2125f, 0.1275f, 0.054f, 1.0f };
	private float materialDefuse_bronze[] = { 0.714f, 0.4284f, 0.18144f, 1.0f };
	private float materialSpecular_bronze[] = { 0.393548f, 0.271906f, 0.166721f, 1.0f };
	private float materialShinyness_bronze = 0.2f * 128.0f;

	// 9 chrome
	private float materialAmbient_chrome[] = { 0.25f, 0.25f, 0.25f, 1.0f };
	private float materialDefuse_chrome[] = { 0.4f, 0.4f, 0.4f, 1.0f };
	private float materialSpecular_chrome[] = { 0.774597f, 0.774597f, 0.774597f, 1.0f };
	private float materialShinyness_chrome = 0.2f * 128.0f;

	// 10 4th sphere on 2nd column, copper  
	private float materialAmbient_copper[] = { 0.19125f, 0.19125f, 0.0225f, 1.0f };
	private float materialDefuse_copper[] = { 0.7038f, 0.27048f, 0.0828f, 1.0f };
	private float materialSpecular_copper[] = { 0.256777f, 0.137622f, 0.086014f, 1.0f };
	private float materialShinyness_copper = 0.1f * 128.0f;

	// 11 5th sphere on 2nd column, gold
	private float materialAmbient_gold[] = { 0.24725f, 0.1995f, 0.0745f, 1.0f };
	private float materialDefuse_gold[] = { 0.75164f, 0.60648f, 0.22648f, 1.0f };
	private float materialSpecular_gold[] = { 0.628281f, 0.555802f, 0.366065f, 1.0f };
	private float materialShinyness_gold = 0.4f * 128.0f;

	// 12 6th sphere on 2nd column, silver
	private float materialAmbient_silver[] = { 0.19225f, 0.1995f, 0.19225f, 1.0f };
	private float materialDefuse_silver[] = { 0.50754f, 0.50754f, 0.50754f, 1.0f };
	private float materialSpecular_silver[] = { 0.508273f, 0.508273f, 0.508273f, 1.0f };
	private float materialShinyness_silver = 0.4f * 128.0f;
	
	// 13 1st sphere on 3rd column, black 
	private float materialAmbient_black[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	private float materialDefuse_black[] = { 0.01f, 0.01f, 0.01f, 1.0f };
	private float materialSpecular_black[] = { 0.5f, 0.5f, 0.5f, 1.0f };
	private float materialShinyness_black = 0.25f * 128.0f;
	
	// 14 2nd sphere on 3rd column, cyan
	private float materialAmbient_cyan[] = { 0.0f, 0.1f, 0.06f, 1.0f };
	private float materialDefuse_cyan[] = { 0.0f, 0.50980392f, 0.50980392f, 1.0f };
	private float materialSpecular_cyan[] = { 0.50196078f, 0.50196078f, 0.50196078f, 1.0f };
	private float materialShinyness_cyan = 0.25f * 128.0f;
	
	// 15 3rd sphere on 2nd column, green
	private float materialAmbient_green[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	private float materialDefuse_green[] = { 0.1f, 0.35f, 0.1f, 1.0f };
	private float materialSpecular_green[] = { 0.45f, 0.55f, 0.45f, 1.0f };
	private float materialShinyness_green = 0.25f * 128.0f;
	
	// 16 4th sphere on 3rd column, red
	private float materialAmbient_red[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	private float materialDefuse_red[] = { 0.5f, 0.0f, 0.0f, 1.0f };
	private float materialSpecular_red[] = { 0.7f, 0.6f, 0.6f, 1.0f };
	private float materialShinyness_red = 0.25f * 128.0f;
	
	// 17 5th sphere on 3rd column, white
	private float materialAmbient_white[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	private float materialDefuse_white[] = { 0.55f, 0.55f, 0.55f, 1.0f };
	private float materialSpecular_white[] = { 0.7f, 0.7f, 0.7f, 1.0f };
	private float materialShinyness_white = 0.25f * 128.0f;

	// 18 6th sphere on 3rd column, yellow plastic
	private float materialAmbient_plastic[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	private float materialDefuse_plastic[] = { 0.5f, 0.5f, 0.0f, 1.0f };
	private float materialSpecular_plastic[] = { 0.60f, 0.60f, 0.50f, 1.0f };
	private float materialShinyness_plastic = 0.25f * 128.0f;
	
	// 19  1st sphere on 4th column, black
	private float materialAmbient_black_2[] = { 0.02f, 0.02f, 0.02f, 1.0f };
	private float materialDefuse_black_2[] = { 0.01f, 0.01f, 0.01f, 1.0f };
	private float materialSpecular_black_2[] = { 0.4f, 0.4f, 0.4f, 1.0f };
	private float materialShinyness_black_2 = 0.078125f * 128.0f;
	
	// 20  2nd sphere on 4th column, cyan
	private float materialAmbient_cyan_2[] = { 0.0f, 0.05f, 0.05f, 1.0f };
	private float materialDefuse_cyan_2[] = { 0.4f, 0.5f, 0.5f, 1.0f };
	private float materialSpecular_cyan_2[] = { 0.04f, 0.7f, 0.7f, 1.0f };
	private float materialShinyness_cyan_2 = 0.078125f * 128.0f;
	
	// 21  3rd sphere on 4th column, green
	private float materialAmbient_green_2[] = { 0.0f, 0.05f, 0.05f, 1.0f };
	private float materialDefuse_green_2[] = { 0.4f, 0.5f, 0.4f, 1.0f };
	private float materialSpecular_green_2[] = { 0.04f, 0.7f, 0.04f, 1.0f };
	private float materialShinyness_green_2 = 0.078125f * 128.0f;
	
	// 22   4th sphere on 4th column, red 
	private float materialAmbient_red_2[] = { 0.05f, 0.0f, 0.0f, 1.0f };
	private float materialDefuse_red_2[] = { 0.5f, 0.4f, 0.4f, 1.0f };
	private float materialSpecular_red_2[] = { 0.7f, 0.04f, 0.04f, 1.0f };
	private float materialShinyness_red_2 = 0.078125f * 128.0f;
	
	// 23   5th sphere on 4th column, white
	private float materialAmbient_white_2[] = { 0.05f, 0.05f, 0.05f, 1.0f };
	private float materialDefuse_white_2[] = { 0.5f, 0.5f, 0.5f, 1.0f };
	private float materialSpecular_white_2[] = { 0.7f, 0.7f, 0.7f, 1.0f };
	private float materialShinyness_white_2 = 0.078125f * 128.0f;
	
	// 24   6th sphere on 4th column, yellow rubber
	private float materialAmbient_rubber[] = { 0.05f, 0.05f, 0.0f, 1.0f };
	private float materialDefuse_rubber[] = { 0.5f, 0.5f, 0.4f, 1.0f };
	private float materialSpecular_rubber[] = { 0.7f, 0.7f, 0.04f, 1.0f };
	private float materialShinyness_rubber = 0.078125f * 128.0f;
	
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		
		context = drawingContext;
		
		// according to EGLContext to current supported version of OpenGL ES
		setEGLContextClientVersion(3);
		
		//set Render for drawing on the GLSurfaceView
		setRenderer(this);
		
		// Render the vuew only when there is change in the drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
				
		gestureDetector = new GestureDetector(context, this, null, false);
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// OpenGL-ES version check
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("HRH: "+ version);
		
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("HRH:  GLSL Version = " + glslVersion);
		
		initialize(gl);
	}
	
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
		draw();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		System.out.println("HRH: Double Tap");
		//setText("Double Tap");
		gCurrentShaderProgram = gShaderProgramObjectPerFragment;
		isPerFragmentLight = 1;
		isPerVertexLight = 0;
		if(countForPerFragment == 0)
		{
			key_x = 1;
			key_y = 0;
			key_z = 0;
		} else if(countForPerFragment == 1)
		{
			key_x = 0;
			key_y = 1;
			key_z = 0;
		} else if(countForPerFragment == 2)
		{
			key_x = 0;
			key_y = 0;
			key_z = 1;
		}
		countForPerFragment = countForPerFragment + 1;
		if(countForPerFragment > 2)
		{
			countForPerFragment = 0;
		}

		return (true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return (true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		//setText("Single Tap");
		System.out.println("HRH: Single Tap");
		gCurrentShaderProgram = gShaderProgramObjectPerVertex;
		isPerVertexLight = 1;
		isPerFragmentLight = 0;
		if(countForPerVertex == 0)
		{
			key_x = 1;
			key_y = 0;
			key_z = 0;
		} else if(countForPerVertex == 1)
		{
			key_x = 0;
			key_y = 1;
			key_z = 0;
		} else if(countForPerVertex == 2)
		{
			key_x = 0;
			key_y = 0;
			key_z = 1;
		}
		countForPerVertex = countForPerVertex + 1;
		if(countForPerVertex > 2)
		{
			countForPerVertex = 0;
		}
		return (true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return (true);
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return (true);
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
		//setText("Long Press");
		System.out.println("HRH: Long Press");
		if (isLightOn == 1)
			{
				isLightOn = 0;
				isPerVertexLight = 0;
				isPerFragmentLight = 0;
				angle_for_light = 0.0f;
			}
			else
			{
				key_x = 0;
				key_y = 0;
				key_z = 0;
				isLightOn = 1;
			}
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll");
		System.out.println("HRH: Scroll");
		uninitialize();
		System.exit(0);
		return (true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
			
	}
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return (true);
	}
	
	private void initialize(GL10 gl)
	{
		/***************************************PER VERTEX SHADER*******************************************************************/
		// vertex shader
		final String vertexShaderSourceCodePerVertex = String.format
		(
			"#version 320 es \n" +
			"in vec4 v_position;" +
			"in vec3 v_normals; " +
			"uniform mat4 u_modelMatrix;" +
			"uniform mat4 u_viewMatrix;" +
			"uniform mat4 u_projectionMatrix;" +
			"uniform int u_isLightOn;" +
			"uniform vec3 u_lightAmbient[3];" +
			"uniform vec3 u_lightSpecular[3];" +
			"uniform vec3 u_lightDiffuse[3];" +
			"uniform vec4 u_lightposition[3]; " +
			"uniform vec3 u_materialAmbient; " +
			"uniform vec3 u_materialSpecular; " +
			"uniform vec3 u_materialDiffuse; " +
			"uniform float u_materialshininess; " +
			"out vec3 phoung_ads_lighting;" +
			"vec4 eye_coordinates;" +
			"vec3 transformed_normal, light_direction, reflection_vector, view_vector;" +
			"vec3 ambient, diffuse_light, specular;" +
			"void main()" +
			"{" +
			"if (u_isLightOn == 1) " +
			"{" +
				"eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" +
				"transformed_normal = normalize( mat3( u_viewMatrix * u_modelMatrix ) * v_normals); " +
				"for(int i=0; i< 3;i++) " +
				"{" +
					"light_direction = normalize( vec3 ( ( u_lightposition[i]) - eye_coordinates) ); " +
					"reflection_vector = reflect (-light_direction, transformed_normal); " +
					"view_vector = normalize ( vec3 ( -eye_coordinates)); " +
					"ambient = u_lightAmbient[i] * u_materialAmbient; " +
					"diffuse_light = u_lightDiffuse[i] * u_materialDiffuse * max ( dot (light_direction , transformed_normal), 0.0); " +
					"specular = u_lightSpecular[i] * u_materialSpecular * pow( max ( dot ( reflection_vector , view_vector), 0.0) , u_materialshininess);" +
					"phoung_ads_lighting = phoung_ads_lighting + ambient + diffuse_light + specular;" +
				"}" +
			"}" +
			"else" +
			"{" +
			"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" +
			"}" +
			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" +
			"}"
		);
		
		gVertexShaderObjectPerVertex = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		GLES32.glShaderSource(gVertexShaderObjectPerVertex, vertexShaderSourceCodePerVertex);
		
		GLES32.glCompileShader(gVertexShaderObjectPerVertex);
		
		int[] iInfoLogLength = new int[1];
		int[] status = new int[1];
		String szInfoLog = null;
		GLES32.glGetShaderiv(gVertexShaderObjectPerVertex, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gVertexShaderObjectPerVertex, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(gVertexShaderObjectPerVertex);
				System.out.println("HRH: Vertex shader compilation error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Vertex shader program compilation complete successfully");
		
		//Fragment shader		
		final String fragmentShaderSourceCodePerVertex = String.format
								(
										"#version 320 es\n"+
										"precision highp float;" +
										"in vec3 phoung_ads_lighting;" +
										"out vec4 FragColor; " +
										"void main()"+
										"{" +
										"FragColor = vec4(phoung_ads_lighting, 1.0); " +
										"}"
								);
								
		gFragmentShaderObjectPerVertex = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		GLES32.glShaderSource(gFragmentShaderObjectPerVertex, fragmentShaderSourceCodePerVertex);								
		GLES32.glCompileShader(gFragmentShaderObjectPerVertex);
								
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetShaderiv(gFragmentShaderObjectPerVertex, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gFragmentShaderObjectPerVertex, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(gFragmentShaderObjectPerVertex);
				System.out.println("HRH: Fragment shader compilation error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Frgament shader program compilation complete successfully");
		
		gShaderProgramObjectPerVertex = GLES32.glCreateProgram();
		
		// attach vertex shader to shader program
		GLES32.glAttachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);
	
		// attch fragment shader to shader program
		GLES32.glAttachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);
		
		// pri-link binding of shader object with vertex shader possition attribute
		GLES32.glBindAttribLocation(gShaderProgramObjectPerVertex, GLESMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
		GLES32.glBindAttribLocation(gShaderProgramObjectPerVertex, GLESMacros.HRH_ATTRIBUTE_NORMAL, "v_normals");
	
		// LINK PROGRAM
		GLES32.glLinkProgram(gShaderProgramObjectPerVertex);
		
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetProgramiv(gShaderProgramObjectPerVertex, GLES32.GL_LINK_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(gShaderProgramObjectPerVertex, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetProgramInfoLog(gShaderProgramObjectPerVertex);
				System.out.println("HRH: linking error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: per vertex Shader program linking complete successfully");		
	
		/***************************************PER FRAGMENT SHADER*******************************************************************/
		// vertex shader
		final String vertexShaderSourceCodePerFragment = String.format
		(
			"#version 320 es \n" +
			"in vec4 v_position;" +
			"in vec3 v_normals; " +
			"uniform mat4 u_modelMatrix;" +
			"uniform mat4 u_viewMatrix;" +
			"uniform mat4 u_projectionMatrix;" +
			"uniform int u_isLightOn;" +
			"uniform vec4 u_lightposition[3]; " +
			"out vec3 light_direction[3]; " +
			"out vec3 transformed_normal;" +
			"out vec3 view_vector;" +
			"void main()" +
			"{" +
			"if (u_isLightOn == 1) " +
			"{" +
				"vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" +
				"transformed_normal =  mat3( u_viewMatrix * u_modelMatrix ) * v_normals;" +
				"for(int i = 0; i< 3; i++)" +
				"{" +
					"light_direction[i] = vec3 ( u_lightposition[i]  - eye_coordinates);" +
				"}" +
				"view_vector =  vec3 ( -eye_coordinates);" +
			"}" +
			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" +
			"}"
		);
		
		gVertexShaderObjectPerFragment = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		GLES32.glShaderSource(gVertexShaderObjectPerFragment, vertexShaderSourceCodePerFragment);
		
		GLES32.glCompileShader(gVertexShaderObjectPerFragment);
		
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetShaderiv(gVertexShaderObjectPerFragment, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gVertexShaderObjectPerFragment, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(gVertexShaderObjectPerFragment);
				System.out.println("HRH: Vertex shader compilation error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Vertex shader program compilation complete successfully");
		
		//Fragment shader		
		final String fragmentShaderSourceCodePerFragment = String.format
								(
										"#version 320 es \n"+
										"precision highp float;" +
										"vec3 phoung_ads_lighting;" +
										"out vec4 FragColor; " +
										"in vec3 light_direction[3]; " +
										"in vec3 transformed_normal;" +
										"in vec3 view_vector;" +
										"vec3 normalized_light_direction; " +
										"vec3 normalized_transformed_normal;" +
										"vec3 normalized_view_vector;" +
										"uniform highp vec3 u_lightAmbient[3];" +
										"uniform highp vec3 u_lightSpecular[3];" +
										"uniform highp vec3 u_lightDiffuse[3];" +
										"uniform highp vec3 u_materialAmbient; " +
										"uniform highp vec3 u_materialSpecular; " +
										"uniform highp vec3 u_materialDiffuse; " +
										"uniform highp float u_materialshininess; " +
										"uniform highp int u_isLightOn;" +
										"vec3 reflection_vector, ambient, diffuse_light, specular;" +
										"void main()" +
										"{" +
										"if (u_isLightOn == 1) " +
										"{" +
											"normalized_view_vector = normalize(view_vector);" +
											"normalized_transformed_normal = normalize(transformed_normal);" +
											"for(int i = 0; i < 3 ; i++)" +
											"{" +
												"normalized_light_direction = normalize(light_direction[i]);" +
												"reflection_vector = reflect (-normalized_light_direction, normalized_transformed_normal); " +
												"ambient = u_lightAmbient[i]  * u_materialAmbient; " +
												"diffuse_light = u_lightDiffuse[i] * u_materialDiffuse * max ( dot (normalized_light_direction , normalized_transformed_normal), 0.0); " +
												"specular = u_lightSpecular[i]  * u_materialSpecular * pow( max ( dot ( reflection_vector , normalized_view_vector), 0.0) , u_materialshininess);" +
												"phoung_ads_lighting = phoung_ads_lighting + ambient + diffuse_light + specular;" +
											"}" +
										"}" +
										"else" +
										"{" +
											"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" +
										"}" +
										"FragColor = vec4(phoung_ads_lighting, 1.0); " +
										"}"
								);
								
		gFragmentShaderObjectPerFragment = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		GLES32.glShaderSource(gFragmentShaderObjectPerFragment, fragmentShaderSourceCodePerFragment);								
		GLES32.glCompileShader(gFragmentShaderObjectPerFragment);
								
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetShaderiv(gFragmentShaderObjectPerFragment, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gFragmentShaderObjectPerFragment, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(gFragmentShaderObjectPerFragment);
				System.out.println("HRH: Fragment shader compilation error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Frgament shader program compilation complete successfully");
		
		gShaderProgramObjectPerFragment = GLES32.glCreateProgram();
		
		// attach vertex shader to shader program
		GLES32.glAttachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);
	
		// attch fragment shader to shader program
		GLES32.glAttachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);
		
		// pri-link binding of shader object with vertex shader possition attribute
		GLES32.glBindAttribLocation(gShaderProgramObjectPerFragment, GLESMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
		GLES32.glBindAttribLocation(gShaderProgramObjectPerFragment, GLESMacros.HRH_ATTRIBUTE_NORMAL, "v_normals");
	
		// LINK PROGRAM
		GLES32.glLinkProgram(gShaderProgramObjectPerFragment);
		
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetProgramiv(gShaderProgramObjectPerFragment, GLES32.GL_LINK_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(gShaderProgramObjectPerFragment, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetProgramInfoLog(gShaderProgramObjectPerFragment);
				System.out.println("HRH: linking error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: per vertex Shader program linking complete successfully");	


		/******************************************* SPHERE **************************************/
		Sphere sphere=new Sphere();
        float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];
        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();

		GLES32.glGenVertexArrays(1, vao_sphere, 0);
		GLES32.glBindVertexArray(vao_sphere[0]);

		// position vbo
		ByteBuffer sphereByteBuffer = ByteBuffer.allocateDirect(sphere_vertices.length * 4);
		sphereByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer sphereFloatBuffer = sphereByteBuffer.asFloatBuffer();
		sphereFloatBuffer.put(sphere_vertices);
		sphereFloatBuffer.position(0);

		GLES32.glGenBuffers(1, vbo_sphere_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_sphere_position[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_vertices.length * 4, sphereFloatBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.HRH_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.HRH_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// normals vbo
		ByteBuffer sphereNormalsByteBuffer = ByteBuffer.allocateDirect(sphere_normals.length * 4);
		sphereNormalsByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer sphereNormalsFloatBuffer = sphereNormalsByteBuffer.asFloatBuffer();
		sphereNormalsFloatBuffer.put(sphere_normals);
		sphereNormalsFloatBuffer.position(0);

		GLES32.glGenBuffers(1, vbo_sphere_normal, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_sphere_normal[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_normals.length * 4, sphereNormalsFloatBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.HRH_ATTRIBUTE_NORMAL, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.HRH_ATTRIBUTE_NORMAL);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		// element vbo
		ByteBuffer sphereElementByteBuffer = ByteBuffer.allocateDirect(sphere_elements.length * 2);
		sphereElementByteBuffer.order(ByteOrder.nativeOrder());
		ShortBuffer sphereElementShortByteBuffer = sphereElementByteBuffer.asShortBuffer();
		sphereElementShortByteBuffer.put(sphere_elements);
		sphereElementShortByteBuffer.position(0);

		GLES32.glGenBuffers(1, vbo_sphere_element, 0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER, sphere_elements.length * 2, sphereElementShortByteBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0);
		
		/************************************************************************************************** */

		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		// we will always cull face for better performance
		GLES32.glEnable(GLES32.GL_CULL_FACE);
			
		GLES32.glClearColor(0.20f, 0.20f, 0.20f, 1.0f);
		
		Matrix.setIdentityM(gPerspectiveProjectionMatrix, 0);

	}

	private void resize(int width, int height)
	{
		GLES32.glViewport(0,0,width, height);
		
		Matrix.perspectiveM(gPerspectiveProjectionMatrix,0, 45.0f, ((float) width / (float) height), ((float) height / (float) width), 100.0f);
		
	}
	
	private void draw()
	{
		float sin_angle = 0.0f, cos_angle = 0.0f, radius = 10.0f;
		float scaleSphere = 0.5f, sphere_translate_x = 0.0f, sphere_translate_y = 2.5f, sphere_translate_z = -8.0f , translate_z = -9.0f, difference_y = 1.0f;
		float x_difference_corner = 1.2f, x_difference_middle = 0.4f, verticle_difference = 1.f;

		// Draw background color
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

		gCurrentShaderProgram = (isPerFragmentLight == 1) ? gShaderProgramObjectPerFragment : gShaderProgramObjectPerVertex ;		

		// START USING OpenGL program object
		GLES32.glUseProgram(gCurrentShaderProgram);
	
		//OpenGL Drawing
		// set modelview & modelviewProjection matrices to indentity
		float[] viewMatrix = new float[16];
		float[] modelMatrix = new float[16];
		float[] scaleMatrix = new float[16];
		float[] translateMatrix = new float[16];
		float[] rotateMatrix = new float[16];
		
		Matrix.setIdentityM(viewMatrix, 0);

		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, 0.0f, 1.0f, 0.0f);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		// get MVP uniform location
		gModelUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_modelMatrix");
		gViewUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_viewMatrix");
		gPerspectiveUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_projectionMatrix");

		gIsLightOnUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_isLightOn");
		
		gLightAmbientUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_lightAmbient");
		gLightSpecularUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_lightSpecular");
		gLightDiffusedUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_lightDiffuse");																						
		gLightPossitionUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_lightposition");
	
		gMaterialDiffuseUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_materialDiffuse");
		gMaterialAmbientUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_materialAmbient");
		gMaterialSpecularUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_materialSpecular");
		gMaterialShininessUniform = GLES32.glGetUniformLocation(gCurrentShaderProgram, "u_materialshininess");
	
		System.out.println("HRH: In Draw isLightOn = " + isLightOn);

		sin_angle = radius * (float)Math.sin(angle_for_light * GLESMacros.RADIAN_VALUE);
	cos_angle = radius * (float)Math.cos(angle_for_light * GLESMacros.RADIAN_VALUE);

	if (key_x == 1)
	{
		lightPosition[0] = 0.0f;
		lightPosition[1] = sin_angle;
		lightPosition[2] = translate_z + cos_angle;
		lightPosition[3] = 1.0f;
	} else if (key_y == 1)
	{
		lightPosition[0] = sin_angle;
		lightPosition[1] = 0.0f;
		lightPosition[2] = translate_z + cos_angle;
		lightPosition[3] = 1.0f;
	} else if (key_z == 1)
	{
		lightPosition[0] = sin_angle;
		lightPosition[1] = cos_angle;
		lightPosition[2] = translate_z;
		lightPosition[3] = 1.0f;
	}

	if (key_x == 1 || key_y == 1 || key_z == 1)
	{
		if (angle_for_light >= 360.0f)
		{
			angle_for_light = 0.0f;
		}
		angle_for_light = angle_for_light + 0.5f;
	}

		if (isLightOn == 1)
		{

		System.out.println("HRH: in draw angle_red = " + angle_red + " sin_angle = " + sin_angle + " cos_angle = "+ cos_angle);
		System.out.println("HRH: lightPosition = " + Arrays.toString(lightPosition));
		GLES32.glUniform4fv(gLightPossitionUniform, 1, lightPosition, 0);
		GLES32.glUniform3fv(gLightDiffusedUniform, 1, lightDiffuse, 0);
		GLES32.glUniform3fv(gLightAmbientUniform,  1 ,lightAmbient, 0);
		GLES32.glUniform3fv(gLightSpecularUniform, 1, lightSpecular, 0);
		}

		GLES32.glUniform1i(gIsLightOnUniform, isLightOn);
		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(gViewUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(gPerspectiveUniform, 1, false, gPerspectiveProjectionMatrix, 0);
	
		// set materials
		/********************************************First Row**************************************************************/
		// 1 sphere
		if (isLightOn == 1)
		{
		GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient_emerald, 0);
		GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness_emerald);
		GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular_emerald, 0);
		GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse_emerald, 0);
		}
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, sphere_translate_x - x_difference_corner, sphere_translate_y, sphere_translate_z);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, scaleSphere, scaleSphere, scaleSphere);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		// second sphere
		if (isLightOn == 1)
		{
		GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient_jade, 0);
		GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness_jade);
		GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular_jade, 0);
		GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse_jade, 0);
		}
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, sphere_translate_x - x_difference_middle, sphere_translate_y, sphere_translate_z);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, scaleSphere, scaleSphere, scaleSphere);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		// third sphere
		if (isLightOn == 1)
		{
		GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient_obsidian, 0);
		GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness_obsidian);
		GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular_obsidian, 0);
		GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse_obsidian, 0);
		}
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, sphere_translate_x + x_difference_middle, sphere_translate_y, sphere_translate_z);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, scaleSphere, scaleSphere, scaleSphere);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		// forth sphere
		if (isLightOn == 1)
		{
		GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient_pearl, 0);
		GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness_pearl);
		GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular_pearl, 0);
		GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse_pearl, 0);
		}
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, sphere_translate_x + x_difference_corner, sphere_translate_y, sphere_translate_z);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, scaleSphere, scaleSphere, scaleSphere);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		/********************************************Second Row**************************************************************/
		// first sphere
		if (isLightOn == 1)
		{
		GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient_ruby, 0);
		GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness_ruby);
		GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular_ruby, 0);
		GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse_ruby, 0);
		}
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, sphere_translate_x - x_difference_corner, sphere_translate_y -difference_y, sphere_translate_z);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, scaleSphere, scaleSphere, scaleSphere);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		// second sphere
		if (isLightOn == 1)
		{
		GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient_turquoise, 0);
		GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness_turquoise);
		GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular_turquoise, 0);
		GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse_turquoise, 0);
		}
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, sphere_translate_x - x_difference_middle, sphere_translate_y -difference_y, sphere_translate_z);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, scaleSphere, scaleSphere, scaleSphere);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		// third sphere
		if (isLightOn == 1)
		{
		GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient_brass, 0);
		GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness_brass);
		GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular_brass, 0);
		GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse_brass, 0);
		}
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, sphere_translate_x + x_difference_middle, sphere_translate_y -difference_y, sphere_translate_z);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, scaleSphere, scaleSphere, scaleSphere);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		// forth sphere
		if (isLightOn == 1)
		{
		GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient_bronze, 0);
		GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness_bronze);
		GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular_bronze, 0);
		GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse_bronze, 0);
		}
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, sphere_translate_x + x_difference_corner, sphere_translate_y -difference_y, sphere_translate_z);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, scaleSphere, scaleSphere, scaleSphere);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		/********************************************Third Row**************************************************************/
		// first sphere
		difference_y = difference_y + verticle_difference;
		if (isLightOn == 1)
		{
		GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient_chrome, 0);
		GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness_chrome);
		GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular_chrome, 0);
		GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse_chrome, 0);
		}
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, sphere_translate_x - x_difference_corner, sphere_translate_y -difference_y, sphere_translate_z);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, scaleSphere, scaleSphere, scaleSphere);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		// second sphere
		if (isLightOn == 1)
		{
		GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient_copper, 0);
		GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness_copper);
		GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular_copper, 0);
		GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse_copper, 0);
		}
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, sphere_translate_x - x_difference_middle, sphere_translate_y -difference_y, sphere_translate_z);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, scaleSphere, scaleSphere, scaleSphere);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		// third sphere
		if (isLightOn == 1)
		{
		GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient_gold, 0);
		GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness_gold);
		GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular_gold, 0);
		GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse_gold, 0);
		}
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, sphere_translate_x + x_difference_middle, sphere_translate_y -difference_y, sphere_translate_z);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, scaleSphere, scaleSphere, scaleSphere);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		// forth sphere
		if (isLightOn == 1)
		{
		GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient_silver, 0);
		GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness_silver);
		GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular_silver, 0);
		GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse_silver, 0);
		}
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, sphere_translate_x + x_difference_corner, sphere_translate_y -difference_y, sphere_translate_z);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, scaleSphere, scaleSphere, scaleSphere);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		/********************************************Forth Row**************************************************************/
		// first sphere
		difference_y = difference_y + verticle_difference;
		if (isLightOn == 1)
		{
		GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient_black, 0);
		GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness_black);
		GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular_black, 0);
		GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse_black, 0);
		}
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, sphere_translate_x - x_difference_corner, sphere_translate_y -difference_y, sphere_translate_z);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, scaleSphere, scaleSphere, scaleSphere);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		// second sphere
		if (isLightOn == 1)
		{
		GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient_cyan, 0);
		GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness_cyan);
		GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular_cyan, 0);
		GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse_cyan, 0);
		}
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, sphere_translate_x - x_difference_middle, sphere_translate_y -difference_y, sphere_translate_z);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, scaleSphere, scaleSphere, scaleSphere);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		// third sphere
		if (isLightOn == 1)
		{
		GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient_green, 0);
		GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness_green);
		GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular_green, 0);
		GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse_green, 0);
		}
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, sphere_translate_x + x_difference_middle, sphere_translate_y -difference_y, sphere_translate_z);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, scaleSphere, scaleSphere, scaleSphere);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		// forth sphere
		if (isLightOn == 1)
		{
		GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient_red, 0);
		GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness_red);
		GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular_red, 0);
		GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse_red, 0);
		}
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, sphere_translate_x + x_difference_corner, sphere_translate_y -difference_y, sphere_translate_z);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, scaleSphere, scaleSphere, scaleSphere);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		/********************************************Fifth Row**************************************************************/
		// first sphere
		difference_y = difference_y + verticle_difference;
		if (isLightOn == 1)
		{
		GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient_white, 0);
		GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness_white);
		GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular_white, 0);
		GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse_white, 0);
		}
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, sphere_translate_x - x_difference_corner, sphere_translate_y -difference_y, sphere_translate_z);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, scaleSphere, scaleSphere, scaleSphere);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		// second sphere
		if (isLightOn == 1)
		{
		GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient_plastic, 0);
		GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness_plastic);
		GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular_plastic, 0);
		GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse_plastic, 0);
		}
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, sphere_translate_x - x_difference_middle, sphere_translate_y -difference_y, sphere_translate_z);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, scaleSphere, scaleSphere, scaleSphere);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		// third sphere
		if (isLightOn == 1)
		{
		GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient_black_2, 0);
		GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness_black_2);
		GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular_black_2, 0);
		GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse_black_2, 0);
		}
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, sphere_translate_x + x_difference_middle, sphere_translate_y -difference_y, sphere_translate_z);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, scaleSphere, scaleSphere, scaleSphere);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		// forth sphere
		if (isLightOn == 1)
		{
		GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient_cyan_2, 0);
		GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness_cyan_2);
		GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular_cyan_2, 0);
		GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse_cyan_2, 0);
		}
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, sphere_translate_x + x_difference_corner, sphere_translate_y -difference_y, sphere_translate_z);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, scaleSphere, scaleSphere, scaleSphere);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		/********************************************Sixth Row**************************************************************/
		// first sphere
		difference_y = difference_y + verticle_difference;
		if (isLightOn == 1)
		{
		GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient_green_2, 0);
		GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness_green_2);
		GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular_green_2, 0);
		GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse_green_2, 0);
		}
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, sphere_translate_x - x_difference_corner, sphere_translate_y -difference_y, sphere_translate_z);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, scaleSphere, scaleSphere, scaleSphere);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		// second sphere
		if (isLightOn == 1)
		{
		GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient_red_2, 0);
		GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness_red_2);
		GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular_red_2, 0);
		GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse_red_2, 0);
		}
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, sphere_translate_x - x_difference_middle, sphere_translate_y -difference_y, sphere_translate_z);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, scaleSphere, scaleSphere, scaleSphere);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		// third sphere
		if (isLightOn == 1)
		{
		GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient_white_2, 0);
		GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness_white_2);
		GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular_white_2, 0);
		GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse_white_2, 0);
		}
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, sphere_translate_x + x_difference_middle, sphere_translate_y -difference_y, sphere_translate_z);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, scaleSphere, scaleSphere, scaleSphere);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		// forth sphere
		if (isLightOn == 1)
		{
		GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient_rubber, 0);
		GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness_rubber);
		GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular_rubber, 0);
		GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse_rubber, 0);
		}
		
		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, sphere_translate_x + x_difference_corner, sphere_translate_y -difference_y, sphere_translate_z);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, translateMatrix, 0);

		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.scaleM(scaleMatrix, 0, scaleSphere, scaleSphere, scaleSphere);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);

		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		
		GLES32.glBindVertexArray(vao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 


		// enf of metrial
		// stop using OpenGL program object
		GLES32.glUseProgram(0);
		
		update();

		requestRender();
	}
	
	private void update()
	{
		float speed = 1.0f;
		if (bAnimate == true)
		{
			System.out.println("HRH: in update");
			angle_red = angle_red - speed;
			angle_blue = angle_blue + speed;
			angle_green = angle_green - speed;
			if (angle_red <= 0.0f)
			{
				angle_red = 360.0f;
			}
			if (angle_blue >= 360)
			{
				angle_blue = 1.0f;
			}
			if (angle_green <= 0.0f)
			{
				angle_green = 360.0f;
			}
		}
		//System.out.println("HRH: angle_red = " + angle_red + " angle_blue = " + angle_blue + " angle_green = "+angle_green);
	}

	private void uninitialize()
	{
	
	// destroy vao
	if (vao_sphere[0] != 0)
	{
		GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
		vao_sphere[0] = 0;
	}

	// destroy vbo
	if (vbo_sphere_position[0] != 0)
	{
		GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
		vbo_sphere_position[0] = 0;
	}

	// destroy vbo
	if (vbo_sphere_element[0] != 0)
	{
		GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
		vbo_sphere_element[0] = 0;
	}

	if (vbo_sphere_normal[0] != 0)
	{
		GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
		vbo_sphere_normal[0] = 0;
	}

	// detach vertex shader from per vertex shader program object
	GLES32.glDetachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);

	// detach fragment shader 
	GLES32.glDetachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);

	// delete vertex shader object
	if(gVertexShaderObjectPerVertex != 0)
	{
		GLES32.glDeleteShader(gVertexShaderObjectPerVertex);
		gVertexShaderObjectPerVertex = 0;
	}

	// delete fragment shader object
	if(gFragmentShaderObjectPerVertex != 0)
	{
		GLES32.glDeleteShader(gFragmentShaderObjectPerVertex);
		gFragmentShaderObjectPerVertex = 0;
	}

	// delete vertex shader program object
	if(gShaderProgramObjectPerVertex != 0)
	{
		GLES32.glDeleteProgram(gShaderProgramObjectPerVertex);
		gShaderProgramObjectPerVertex = 0;
	}

	/********************** detach vertex shader from shader program object ***********************/
	GLES32.glDetachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);

	// detach fragment shader from shader program obejct
	GLES32.glDetachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);

	// delete vertex shader object
	if(gVertexShaderObjectPerFragment != 0)
	{
		GLES32.glDeleteShader(gVertexShaderObjectPerFragment);
		gVertexShaderObjectPerFragment = 0;
	}

	// delete fragment shader object
	if(gFragmentShaderObjectPerFragment != 0)
	{
		GLES32.glDeleteShader(gFragmentShaderObjectPerFragment);
		gFragmentShaderObjectPerFragment = 0;
	}

	// delete shader program object
	if(gShaderProgramObjectPerFragment != 0)
	{
		GLES32.glDeleteProgram(gShaderProgramObjectPerFragment);
		gShaderProgramObjectPerFragment = 0;
	}

	// unlink shader program
	GLES32.glUseProgram(0);
	}
}
