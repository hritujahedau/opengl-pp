/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.example.twenty_four_sphere;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.example.twenty_four_sphere";
  public static final String BUILD_TYPE = "debug";
}
