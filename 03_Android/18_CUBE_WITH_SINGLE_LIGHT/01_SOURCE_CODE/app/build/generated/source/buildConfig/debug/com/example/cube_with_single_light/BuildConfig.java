/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.example.cube_with_single_light;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.example.cube_with_single_light";
  public static final String BUILD_TYPE = "debug";
}
