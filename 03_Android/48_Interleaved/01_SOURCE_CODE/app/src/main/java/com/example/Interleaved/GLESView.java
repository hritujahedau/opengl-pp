package com.example.Interleaved;

import androidx.appcompat.widget.AppCompatTextView;

import android.view.MotionEvent;

import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;

import android.content.Context;
import android.graphics.Color;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils;
import android.view.Gravity;

import android.opengl.GLSurfaceView;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32;
import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.Arrays;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private GestureDetector gestureDetector;
	
	private final Context context;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
	private int[] vao_cube = new int[1];
	private int[] vbo_cube_position = new int[1];
	private int[] vbo_cube_color = new int[1];
	private int[] vbo_cube_texcoord = new int[1];
	private int[] vbo_cube_normals = new int[1];

	private int gModelUniform;
	private int gViewUniform;
	private int gPerspectiveUniform;
	
	private float perspectiveProjectionMatrix[] = new float[16];

	private static float rotateByAngle = 0.0f;

	private int countForUpdateUniform = 0;
	private static int countForUpdate = 0;
	private int textureSamplerUniform = 0;

	private int[] texture = new int[1];

	int width, hight;

	int gLightAmbientUniform;
	int gLightSpecularUniform;
	int gLightDiffusedUniform;
	int gLightPossitionUniform;
	
	int gMaterialSpecularUniform;
	int gMaterialAmbientUniform;
	int gMaterialDiffuseUniform;
	int gMaterialShininessUniform;

	float lightPossition[] = { 100.0f, 100.0f, 100.0f, 1.0f };
	float lightAmbient[] = { 0.0f, 0.0f, 0.0f };
	float lightDefuse[] = { 1.0f, 1.0f, 1.0f };
	float lightSpecular[] = { 1.0f, 1.0f, 1.0f }; 
	
	float materialAmbient[] = { 0.0f, 0.0f, 0.0f };
	float materialDefuse[] = { 1.0f, 1.0f, 1.0f  };
	float materialSpecular[] = { 1.0f, 1.0f, 1.0f};
	float materialShinyness = 58.0f;
	
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		
		context = drawingContext;
		
		// according to EGLContext to current supported version of OpenGL ES
		setEGLContextClientVersion(3);
		
		//set Render for drawing on the GLSurfaceView
		setRenderer(this);
		
		// Render the vuew only when there is change in the drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
				
		gestureDetector = new GestureDetector(context, this, null, false);
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// OpenGL-ES version check
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("HRH: "+ version);
		
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("HRH:  GLSL Version = " + glslVersion);
		
		initialize(gl);
	}
	
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int hight)
	{
		resize(width, hight);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
		draw();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		System.out.println("HRH: Double Tap");
		//setText("Double Tap");
		if(countForUpdate > 0)
		{
			countForUpdate = countForUpdate - 1;
		}
		return (true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return (true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		//setText("Single Tap");
		System.out.println("HRH: Single Tap");
		if(countForUpdate < 3)
		{
			countForUpdate = countForUpdate + 1;
		}
		return (true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return (true);
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return (true);
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
		//setText("Long Press");
		System.out.println("HRH: Long Press");
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll");
		System.out.println("HRH: Scroll");
		uninitialize();
		System.exit(0);
		return (true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
			
	}
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return (true);
	}
	
	private void initialize(GL10 gl)
	{
		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es \n" +
			"in vec4 vPosition;" +
			"in vec4 vColor;" +
			"out vec4 out_color; " +
			"in vec3 vNormals; " +
			"in vec2 vTexCoord;" +
			"out vec2 outTexCoord; " +
			"uniform mat4 u_modelMatrix;" +
			"uniform mat4 u_viewMatrix;" +
			"uniform mat4 u_projectionMatrix;" +
			"uniform vec3 u_lightAmbient;" +
			"uniform vec3 u_lightSpecular;" +
			"uniform vec3 u_lightDiffuse;" +
			"uniform vec4 u_light_position; " +
			"uniform vec3 u_materialAmbient; " +
			"uniform vec3 u_materialSpecular; " +
			"uniform vec3 u_materialDiffuse; " +
			"uniform float u_materialshininess; " +
			"out vec3 phoung_ads_lighting; " +
			"uniform highp int u_countForUpdate; " +
			"void main()" +
			"{" +
			"out_color = vColor;" +
			"outTexCoord = vTexCoord;"  +
			"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" +
			"if (u_countForUpdate == 3) " +
			"{" +
				"vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * vPosition;" +
				"vec3 transformed_normal = normalize( mat3( u_viewMatrix * u_modelMatrix ) * vNormals); " +
				"vec3 light_direction = normalize( vec3 (u_light_position - eye_coordinates) ); " +
				"vec3 reflection_vector = reflect (-light_direction, transformed_normal); " +
				"vec3 view_vector = normalize ( vec3 ( -eye_coordinates)); " +
				"vec3 ambient = u_lightAmbient * u_materialAmbient; " +
				"vec3 diffuse_light = u_lightDiffuse * u_materialDiffuse * max ( dot (light_direction , transformed_normal), 0.0); " +
				"vec3 specular = u_lightSpecular * u_materialSpecular * pow( max ( dot ( reflection_vector , view_vector), 0.0) , u_materialshininess);" +
				"phoung_ads_lighting = ambient + diffuse_light + specular;" +
			"}" +
			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * vPosition;" +
			"}"
		);
		
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		
		GLES32.glCompileShader(vertexShaderObject);
		
		int[] iInfoLogLength = new int[1];
		int[] status = new int[1];
		String szInfoLog = null;
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("HRH: Vertex shader compilation error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Vertex shader program compilation complete successfully");
		
		/*************Fragment shader***************************/
		
		final String fragmentShaderSourceCode = String.format
				(
						" #version 320 es	\n																			" +
						" precision highp float;																		" +
						" out vec4 FragColor; 																			" +
						" in vec4 out_color;																			" +
						" in vec2 outTexCoord;																			" +
						" uniform highp int u_countForUpdate;  															" +
						" uniform highp sampler2D u_texture_sampler;													" +
						" in vec3 phoung_ads_lighting;																	" +
						" void main()																					" +
						" {																								" +
						" 	FragColor = vec4(1.0, 1.0, 1.0, 1.0); 														" +
						" 	if(u_countForUpdate >= 1) 																	" +
						" 	{																							" +
						" 		FragColor = out_color;																	" +
						" 	} 																						 	" +
						" 	if(u_countForUpdate >= 2) 																	" +
						" 	{																							" +
						"		FragColor = FragColor * texture(u_texture_sampler,outTexCoord);							" +
						" 	} 																						 	" +
						" 	if(u_countForUpdate == 3) 																	" +
						" 	{																							" +
						"		FragColor = FragColor * vec4(phoung_ads_lighting, 1.0);									" +
						" 	} 																						 	" +										
						" } 																							"
				);
								
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
								
		GLES32.glCompileShader(fragmentShaderObject);
								
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("HRH: Fragment shader compilation error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Frgament shader program compilation complete successfully");
		
		shaderProgramObject = GLES32.glCreateProgram();
		
		// attach vertex shader to shader program
		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
	
		// attch fragment shader to shader program
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);
		
		// pri-link binding of shader object with vertex shader possition attribute
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.HRH_ATTRIBUTE_COLOR, "vColor");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.HRH_ATTRIBUTE_TEXTURE_0, "vTexCoord");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.HRH_ATTRIBUTE_NORMAL, "vNormals");
	
		// LINK PROGRAM
		GLES32.glLinkProgram(shaderProgramObject);
		
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("HRH: linking error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Shader program linking complete successfully");		
		
		gModelUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_modelMatrix");
		gViewUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
		gPerspectiveUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projectionMatrix");
		
		countForUpdateUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_countForUpdate");
		textureSamplerUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_texture_sampler");

		gLightAmbientUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_lightAmbient");
		gLightSpecularUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_lightSpecular");
		gLightDiffusedUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_lightDiffuse");
		gLightPossitionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position");
	
		gMaterialDiffuseUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_materialDiffuse");
		gMaterialAmbientUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_materialAmbient");
		gMaterialSpecularUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_materialSpecular");
		gMaterialShininessUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_materialshininess");

		
		/***************************************CUBE*************************************** */

		GLES32.glGenVertexArrays(1, vao_cube, 0);
		GLES32.glBindVertexArray(vao_cube[0]);

		// position
		final float cubeVertices[] = new float[]
		{
		 // FRONT
		1.0f,1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,

		// RIGHT
		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f,-1.0f,

		//BACK
		1.0f, -1.0f,-1.0f,
		-1.0f, -1.0f,-1.0f,
		-1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,
		
		// LEFT
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,

		// TOP
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		// BOTTOM
		1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		
		};

		ByteBuffer byteBufferCubeVertices = ByteBuffer.allocateDirect(cubeVertices.length * 4);
		byteBufferCubeVertices.order(ByteOrder.nativeOrder());
		FloatBuffer cubeVerticesBuffer = byteBufferCubeVertices.asFloatBuffer();
		cubeVerticesBuffer.put(cubeVertices);
		cubeVerticesBuffer.position(0);

		GLES32.glGenBuffers(1, vbo_cube_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube_position[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, (cubeVertices.length * 4), cubeVerticesBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.HRH_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.HRH_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		// color
		final float cubeColor[] = new float[]
		{   
							// 1
							1.0f, 0.0f, 0.0f,
							1.0f, 0.0f, 0.0f,
							1.0f, 0.0f, 0.0f,
							1.0f, 0.0f, 0.0f,

							//2
							0.0f, 1.0f, 0.0f,
							0.0f, 1.0f, 0.0f,
							0.0f, 1.0f, 0.0f,
							0.0f, 1.0f, 0.0f,
							
							//3
							0.0f, 0.0f, 1.0f,
							0.0f, 0.0f, 1.0f,
							0.0f, 0.0f, 1.0f,
							0.0f, 0.0f, 1.0f,

							//4
							1.0f, 1.0f, 0.0f,
							1.0f, 1.0f, 0.0f,
							1.0f, 1.0f, 0.0f,
							1.0f, 1.0f, 0.0f,

							//5
							0.0f, 1.0f, 1.0f,
							0.0f, 1.0f, 1.0f,
							0.0f, 1.0f, 1.0f,
							0.0f, 1.0f, 1.0f,

							//6
							1.0f, 0.0f, 1.0f,
							1.0f, 0.0f, 1.0f,
							1.0f, 0.0f, 1.0f,
							1.0f, 0.0f, 1.0f,
							 
						};

		ByteBuffer byteBufferCubeColor = ByteBuffer.allocateDirect(cubeColor.length * 4);
		byteBufferCubeColor.order(ByteOrder.nativeOrder());
		FloatBuffer cubeColorBuffer = byteBufferCubeColor.asFloatBuffer();
		cubeColorBuffer.put(cubeColor);
		cubeColorBuffer.position(0);

		GLES32.glGenBuffers(1, vbo_cube_color, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube_color[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, (cubeColor.length * 4), cubeColorBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.HRH_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.HRH_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		// color
		final float cubeTexcoord[] = new float[]
		{   
							// 1
							0.0f, 0.0f,
							1.0f, 0.0f,
							1.0f,1.0f,
							0.0f,1.0f,

							//2
							1.0f, 0.0f,
							1.0f,1.0f,
							0.0f,1.0f,
							0.0f, 0.0f,

							//3
							1.0f, 0.0f,
							1.0f, 1.0f,
							0.0f, 1.0f,
							0.0f, 0.0f,

							//4
							0.0f, 0.0f,
							1.0f, 0.0f,
							1.0f, 1.0f,
							0.0f, 1.0f,

							//5
							0.0f,1.0f,
							0.0f, 0.0f,
							1.0f, 0.0f,
							1.0f, 1.0f,

							//6
							1.0f, 1.0f,
							0.0f, 1.0f,
							0.0f, 0.0f,
							1.0f, 0.0f,							 
						};

		ByteBuffer byteBufferCubeTexcoord = ByteBuffer.allocateDirect(cubeTexcoord.length * 4);
		byteBufferCubeTexcoord.order(ByteOrder.nativeOrder());
		FloatBuffer cubeTexcoordBuffer = byteBufferCubeTexcoord.asFloatBuffer();
		cubeTexcoordBuffer.put(cubeTexcoord);
		cubeTexcoordBuffer.position(0);

		GLES32.glGenBuffers(1, vbo_cube_texcoord, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube_texcoord[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, (cubeTexcoord.length * 4), cubeTexcoordBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.HRH_ATTRIBUTE_TEXTURE_0, 2, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.HRH_ATTRIBUTE_TEXTURE_0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		//normals
		final float cubeNormals[] = new float[]
		{
				0.0f,  0.0f,  1.0f , //front
				0.0f,  0.0f,  1.0f , //front
				0.0f,  0.0f,  1.0f , //front
				0.0f,  0.0f,  1.0f , //front

				1.0f,  0.0f,  0.0f , // right
				1.0f,  0.0f,  0.0f , // right
				1.0f,  0.0f,  0.0f , // right
				1.0f,  0.0f,  0.0f , // right
				
				0.0f, 0.0f,  -1.0f , //back
				0.0f, 0.0f,  -1.0f , //back
				0.0f, 0.0f,  -1.0f , //back
				0.0f, 0.0f,  -1.0f , //back

				-1.0f,  0.0f, 0.0f , //left
				-1.0f,  0.0f, 0.0f , //left
				-1.0f,  0.0f, 0.0f , //left
				-1.0f,  0.0f, 0.0f , //left
				

				0.0f,  1.0f,  0.0f , // top
				0.0f,  1.0f,  0.0f , // top
				0.0f,  1.0f,  0.0f , // top
				0.0f,  1.0f,  0.0f , // top

				0.0f, -1.0f,  0.0f , // bottom
				0.0f, -1.0f,  0.0f , // bottom
				0.0f, -1.0f,  0.0f , // bottom
				0.0f, -1.0f,  0.0f , // bottom

		};

		ByteBuffer cubeNormalsByteBuffer = ByteBuffer.allocateDirect(cubeNormals.length * 4);
		cubeNormalsByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer cubeNormalsFloatBuffer = cubeNormalsByteBuffer.asFloatBuffer();
		cubeNormalsFloatBuffer.put(cubeNormals);
		cubeNormalsFloatBuffer.position(0);

		GLES32.glGenBuffers(1, vbo_cube_normals, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube_normals[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, (cubeNormals.length * 4), cubeNormalsFloatBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.HRH_ATTRIBUTE_NORMAL, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.HRH_ATTRIBUTE_NORMAL);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0); // end vao for rectangle

		/************************************************************************************************** */

		texture[0] = loadGLTexture(R.raw.marble);

		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		// we will always cull face for better performance
		//GLES32.glEnable(GLES32.GL_CULL_FACE);
			
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);

	}


	private int loadGLTexture(int texture_resource_id)
	{
		//VARIABLE DECLARATION
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inScaled = false;
		int[] texture = new int[1];

		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), texture_resource_id);
	
		//CODE	
		// from here start OpenGL Texture code
		GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 1); //IN FFP 4
		GLES32.glGenTextures(1, texture, 0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
	
		//setting texture parameter
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);
	
		//glTexImage2D(GLES32.GL_TEXTURE_2D, 0, 3, bmp.bmWidth, bmp.bmHeight, 0, GLES32.GL_BGR, GLES32.GL_UNSIGNED_BYTE, bmp.bmBits);		
		GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap, 0);
		GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);
	
		return texture[0];
	}

	private void resize(int width, int hight)
	{
		GLES32.glViewport(0,0,width, hight);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix,0, 45.0f, ((float) width / (float) hight), ((float) hight / (float) width), 100.0f);
		
	}
	
	private void draw()
	{
		// Draw background color
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);		

		// START USING OpenGL program object
		GLES32.glUseProgram(shaderProgramObject);
	
		//OpenGL Drawing
		// set modelview & modelviewProjection matrices to indentity
		float[] modelMatrix = new float[16];
		float[] viewMatrix = new float[16];
		float[] translateMatrix = new float[16];
		float[] rotateMatrix = new float[16];
		float[] scaleMatrix = new float[16];
		float[] modelViewProjectionMatrix = new float[16];
		
		/***************************CUBE***************************************************** */

		Matrix.setIdentityM(translateMatrix, 0);
		Matrix.translateM(translateMatrix, 0, 0.0f, 0.0f, -6.0f);
		
		Matrix.setIdentityM(viewMatrix, 0);

		Matrix.setIdentityM(rotateMatrix, 0);
		Matrix.rotateM(rotateMatrix, 0, rotateByAngle, 1.0f, 1.0f, 1.0f);

		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, translateMatrix, 0, rotateMatrix, 0);

	
		// pass above modelviewprojection matrix to vertex shader 'u_mvpMatrix' shader variable
		// whose value we already have calculated in inititialization() by uisng glGetUniformLocation()
		GLES32.glUniformMatrix4fv(gModelUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(gViewUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(gPerspectiveUniform, 1, false, perspectiveProjectionMatrix, 0);
		
		GLES32.glUniform1i(countForUpdateUniform, countForUpdate);

		if (countForUpdate == 3)
		{
			GLES32.glUniform4fv(gLightPossitionUniform, 1, lightPossition, 0);
			GLES32.glUniform3fv(gLightDiffusedUniform, 1, lightDefuse, 0);
			GLES32.glUniform3fv(gLightAmbientUniform,  1 ,lightAmbient, 0);
			GLES32.glUniform3fv(gLightSpecularUniform, 1 ,lightSpecular, 0);
	
			GLES32.glUniform3fv(gMaterialAmbientUniform, 1, materialAmbient, 0);
			GLES32.glUniform1f(gMaterialShininessUniform, materialShinyness);
			GLES32.glUniform3fv(gMaterialSpecularUniform, 1, materialSpecular, 0);
			GLES32.glUniform3fv(gMaterialDiffuseUniform, 1, materialDefuse, 0);
		}
	
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
		GLES32.glUniform1i(textureSamplerUniform, 0);

		// *** bind vao **
		GLES32.glBindVertexArray(vao_cube[0]);
	
		// ** draw , either by glDrawTriangles() or glDrawArrays() or glDrawElements();
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4); 
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4); 
	
		// ** unbind vao **
		GLES32.glBindVertexArray(0);

		// stop using OpenGL program object
		GLES32.glUseProgram(0);

		rotateByAngle = rotateByAngle + 0.5f;
		
		requestRender();
	}
	
	private void uninitialize()
	{
	
	// destroy vao
	if (vao_cube[0] != 0)
	{
		GLES32.glDeleteVertexArrays(1, vao_cube, 0);
		vao_cube[0] = 0;
	}

	// destroy vbo
	if (vbo_cube_position[0] != 0)
	{
		GLES32.glDeleteBuffers(1, vbo_cube_position, 0);
		vbo_cube_position[0] = 0;
	}

	// deatch vertex shader from shader program object
	GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);

	// detach fragment shader from shader program obejct
	GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);

	// delete vertex shader object
	if(vertexShaderObject != 0)
	{
		GLES32.glDeleteShader(vertexShaderObject);
		vertexShaderObject = 0;
	}

	// delete fragment shader object
	if(fragmentShaderObject != 0)
	{
		GLES32.glDeleteShader(fragmentShaderObject);
		fragmentShaderObject = 0;
	}

	// delete shader program object
	if(shaderProgramObject != 0)
	{
		GLES32.glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;
	}

	// unlink shader program
	GLES32.glUseProgram(0);
	}
}
