/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.example.robotic_arm;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.example.robotic_arm";
  public static final String BUILD_TYPE = "release";
}
