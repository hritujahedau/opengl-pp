package com.example.robotic_arm;

public class GLESMacros
{
	// attribute index
	public static final int HRH_ATTRIBUTE_POSITION = 0 ;
	public static final int HRH_ATTRIBUTE_COLOR = 1 ;
	public static final int HRH_ATTRIBUTE_NORMAL = 2;
	public static final int HRH_ATTRIBUTE_TEXTURE_0 = 3 ;
}
