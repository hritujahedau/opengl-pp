package com.example.robotic_arm;

import androidx.appcompat.widget.AppCompatTextView;

import android.view.MotionEvent;

import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;

import android.opengl.GLSurfaceView;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32;
import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.Arrays;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private GestureDetector gestureDetector;
	
	private final Context context;
	
	private int gVertexShaderObject;
	private int gFragmentShaderObject;
	private int gShaderProgramObject;
	
	private int[] gVao_sphere = new int[1];
    private int[] gVbo_sphere_position = new int[1];
    private int[] gVbo_sphere_normal = new int[1];
    private int[] gVbo_sphere_element = new int[1];
	private int[] gVbo_sphere_color = new int[1];

	private int gModelMatrixUniform;
	private int gViewMatrixUniform;
	private int gPerspectiveMatrixUniform;

	private int numVertices ;
    private int numElements;
	private int mvpUniform = 0;
	
	private float gPerspectiveProjectionMatrix[] = new float[16];

	private int width, height;

	private int sholder = 0, elbow = 0;

	class Node
	{
    float[] data =new float[16];
    Node prev;
	}

	Node top;
	
	void pushMatrix(float setMatrix[])
	{
		Node node = new Node();
		for(int i=0; i< 16; i++)
		{
			node.data[i] = setMatrix[i];
		}		

		if(top == null)
		{
			top = node;
			node.prev = null;
		}
		else
		{
			node.prev = top;
			top = node;
		}
		System.out.println("HRH: Push() array data = " + Arrays.toString(node.data));
	}

	float[] popMatrix()
	{
		Node node = new Node();
		if(top == null)
		{
			System.out.println("HRH: Stack Underflow");
		}
		for(int i=0; i< 16; i++)
		{
			node.data[i] = top.data[i];
		}
		top = top.prev;
		System.out.println("HRH: Pop() array data = " + Arrays.toString(node.data));
		return(node.data);
	}

	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		
		context = drawingContext;
		
		// according to EGLContext to current supported version of OpenGL ES
		setEGLContextClientVersion(3);
		
		//set Render for drawing on the GLSurfaceView
		setRenderer(this);
		
		// Render the vuew only when there is change in the drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
				
		gestureDetector = new GestureDetector(context, this, null, false);
		gestureDetector.setOnDoubleTapListener(this);

		this.top = null;
	}
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// OpenGL-ES version check
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("HRH: "+ version);
		
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("HRH:  GLSL Version = " + glslVersion);
		
		initialize(gl);
	}
	
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
		draw();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		System.out.println("HRH: Double Tap");
		//setText("Double Tap");
		return (true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return (true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		//setText("Single Tap");
		System.out.println("HRH: Single Tap");
		return (true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return (true);
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return (true);
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
		//setText("Long Press");
		System.out.println("HRH: Long Press");
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll");
		System.out.println("HRH: Scroll");
		uninitialize();
		System.exit(0);
		return (true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{

	}
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return (true);
	}
	
	private void initialize(GL10 gl)
	{
		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es \n" +
											"in vec4 vPosition;" +
											"in vec3 color;"+
											"out vec3 color_out;"+
											"uniform mat4 u_modelMatrix;" +
											"uniform mat4 u_viewMatrix;" +
											"uniform mat4 u_projectionMatrix;" +
											"void main()"+
											"{" +
											"color_out = color;"+
											"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;"+
											"}"
		);
		
		gVertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		GLES32.glShaderSource(gVertexShaderObject, vertexShaderSourceCode);
		
		GLES32.glCompileShader(gVertexShaderObject);
		
		int[] iInfoLogLength = new int[1];
		int[] status = new int[1];
		String szInfoLog = null;
		GLES32.glGetShaderiv(gVertexShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gVertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(gVertexShaderObject);
				System.out.println("HRH: Vertex shader compilation error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Vertex shader program compilation complete successfully");
		
		/*************Fragment shader***************************/
		
		final String fragmentShaderSourceCode = String.format
								(
										"#version 320 es\n"+
										"precision highp float;" +
										"out vec4 FragColor; " +
										"in vec3 color_out;" +
										"void main()"+
										"{" +
										"FragColor = vec4(color_out, 1.0);" +
										"}"
								);
								
		gFragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		GLES32.glShaderSource(gFragmentShaderObject, fragmentShaderSourceCode);
								
		GLES32.glCompileShader(gFragmentShaderObject);
								
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetShaderiv(gFragmentShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gFragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetShaderInfoLog(gFragmentShaderObject);
				System.out.println("HRH: Fragment shader compilation error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Frgament shader program compilation complete successfully");
		
		gShaderProgramObject = GLES32.glCreateProgram();
		
		// attach vertex shader to shader program
		GLES32.glAttachShader(gShaderProgramObject, gVertexShaderObject);
	
		// attch fragment shader to shader program
		GLES32.glAttachShader(gShaderProgramObject, gFragmentShaderObject);
		
		// pri-link binding of shader object with vertex shader possition attribute
		GLES32.glBindAttribLocation(gShaderProgramObject, GLESMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
	
		// LINK PROGRAM
		GLES32.glLinkProgram(gShaderProgramObject);
		
		iInfoLogLength = new int[1];
		status = new int[1];
		szInfoLog = null;
		GLES32.glGetProgramiv(gShaderProgramObject, GLES32.GL_LINK_STATUS, status, 0);
		if (status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(gShaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog =GLES32.glGetProgramInfoLog(gShaderProgramObject);
				System.out.println("HRH: linking error : "+ szInfoLog + " ");				
				uninitialize();
				System.exit(0);				
			}
		}
		
		System.out.println("HRH: Shader program linking complete successfully");		
		
	// get MVP uniform location
	gModelMatrixUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
	gViewMatrixUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
	gPerspectiveMatrixUniform = GLES32.glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");

		// sphere
		Sphere sphere=new Sphere();
        float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];
        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();

		GLES32.glGenVertexArrays(1, gVao_sphere, 0);
		GLES32.glBindVertexArray(gVao_sphere[0]);

		// position vbo
		ByteBuffer sphereByteBuffer = ByteBuffer.allocateDirect(sphere_vertices.length * 4);
		sphereByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer sphereFloatBuffer = sphereByteBuffer.asFloatBuffer();
		sphereFloatBuffer.put(sphere_vertices);
		sphereFloatBuffer.position(0);

		GLES32.glGenBuffers(1, gVbo_sphere_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, gVbo_sphere_position[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_vertices.length * 4, sphereFloatBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.HRH_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.HRH_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// normals vbo
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(sphere_normals.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer floatBuffer = byteBuffer.asFloatBuffer();
		floatBuffer.put(sphere_normals);
		floatBuffer.position(0);

		GLES32.glGenBuffers(1, gVbo_sphere_normal, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, gVbo_sphere_normal[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_normals.length * 4, floatBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.HRH_ATTRIBUTE_NORMAL, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.HRH_ATTRIBUTE_NORMAL);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		// element vbo
		ByteBuffer sphereElementByteBuffer = ByteBuffer.allocateDirect(sphere_elements.length * 2);
		sphereElementByteBuffer.order(ByteOrder.nativeOrder());
		ShortBuffer sphereElementShortByteBuffer = sphereElementByteBuffer.asShortBuffer();
		sphereElementShortByteBuffer.put(sphere_elements);
		sphereElementShortByteBuffer.position(0);

		GLES32.glGenBuffers(1, gVbo_sphere_element, 0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element[0]);
		GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER, sphere_elements.length * 2, sphereElementShortByteBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0);
		
		/************************************************************************************************** */

		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		// we will always cull face for better performance
		GLES32.glEnable(GLES32.GL_CULL_FACE);
			
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		Matrix.setIdentityM(gPerspectiveProjectionMatrix, 0);

	}

	private void resize(int width, int height)
	{
		GLES32.glViewport(0,0,width, height);
		
		Matrix.perspectiveM(gPerspectiveProjectionMatrix,0, 45.0f, ((float) width / (float) height), ((float) height / (float) width), 100.0f);
		
	}
	
	private void draw()
	{
		// Draw background color
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);	

		float scale = 0.3f;	

		// START USING OpenGL program object
		GLES32.glUseProgram(gShaderProgramObject);
	
		//OpenGL Drawing
		// set modelview & modelviewProjection matrices to indentity
		float[] modelMatrix = new float[16];
		float[] viewMatrix = new float[16];

		float[] sholderTranslateMatrix = new float[16];
		float[] sholderRotateMatrix = new float[16];
		float[] sholderScaleMatrix = new float[16];
		float[] ctm = new float[16];

		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setLookAtM(viewMatrix, 0, 0.0f, 0.0f, 5.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
		pushMatrix(viewMatrix);
		
		System.out.println("HRH: Draw() array data = " + Arrays.toString(viewMatrix));
		
		Matrix.setIdentityM(sholderTranslateMatrix, 0);
		Matrix.translateM(sholderTranslateMatrix, 0, 1.0f, 0.0f, 0.0f);

		Matrix.setIdentityM(sholderRotateMatrix, 0);
		Matrix.rotateM(sholderRotateMatrix, 0, sholder, 0.0f, 0.0f, 1.0f);

		Matrix.setIdentityM(sholderScaleMatrix, 0);
		Matrix.scaleM(sholderScaleMatrix, 0, 2.0f, 0.5f, 0.1f);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, sholderRotateMatrix, 0, sholderTranslateMatrix, 0);
		// push model matrix
		pushMatrix(modelMatrix);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, sholderScaleMatrix, 0);		

		GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(gPerspectiveMatrixUniform, 1, false, gPerspectiveProjectionMatrix, 0);
		
		GLES32.glVertexAttrib3f(GLESMacros.HRH_ATTRIBUTE_COLOR, 0.5f, 0.35f, 0.05f);

		GLES32.glBindVertexArray(gVao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		// elbow
		float[] elbowRotateMatrix = new float[16];
		float[] elbowTranslateMatrix = new float[16];
		float[] elbowScaleMatrix =new float[16];

		Matrix.setIdentityM(elbowTranslateMatrix, 0);
		Matrix.translateM(elbowTranslateMatrix, 0, 1.0f, 0.0f, 0.0f);

		Matrix.setIdentityM(elbowScaleMatrix, 0);
		Matrix.scaleM(elbowScaleMatrix, 0, 2.0f, 0.5f, 0.1f);

		Matrix.setIdentityM(elbowRotateMatrix, 0);
		Matrix.rotateM(elbowRotateMatrix, 0, elbow, 0.0f, 0.0f, 1.0f);
		
		Matrix.setIdentityM(modelMatrix, 0);
		modelMatrix = popMatrix();
		System.out.println("HRH: after pop model matrix = "+ Arrays.toString(modelMatrix));
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, elbowTranslateMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, elbowRotateMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, elbowTranslateMatrix, 0);
		Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, elbowScaleMatrix, 0);

		GLES32.glUniformMatrix4fv(gModelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(gViewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(gPerspectiveMatrixUniform, 1, false, gPerspectiveProjectionMatrix, 0);
		
	//	GLES32.glVertexAttrib3f(GLESMacros.HRH_ATTRIBUTE_COLOR, 0.5f, 0.35f, 0.05f);
		GLES32.glVertexAttrib3f(GLESMacros.HRH_ATTRIBUTE_COLOR, 1.0f, 1.0f, 1.0f);

		GLES32.glBindVertexArray(gVao_sphere[0]); // bind vao 
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0); 
		GLES32.glBindVertexArray(0); // unbind vao 

		viewMatrix = popMatrix();

		// stop using OpenGL program object
		GLES32.glUseProgram(0);

		sholder = (sholder + 1) % 360;
		elbow = (elbow + 1) % 360;

		System.out.println("HRH: sholder = " + sholder + " elbow  = " + elbow);
		
		if(sholder > 360)
		{
			sholder = 0;
		}

		if(elbow > 360)
		{
			elbow = 0;
		}

		requestRender();
	}
	
	private void uninitialize()
	{
	
	// destroy vao
	if (gVao_sphere[0] != 0)
	{
		GLES32.glDeleteVertexArrays(1, gVao_sphere, 0);
		gVao_sphere[0] = 0;
	}

	// destroy vbo
	if (gVbo_sphere_position[0] != 0)
	{
		GLES32.glDeleteBuffers(1, gVbo_sphere_position, 0);
		gVbo_sphere_position[0] = 0;
	}

	// destroy vbo
	if (gVbo_sphere_element[0] != 0)
	{
		GLES32.glDeleteBuffers(1, gVbo_sphere_element, 0);
		gVbo_sphere_element[0] = 0;
	}

	if (gVbo_sphere_normal[0] != 0)
	{
		GLES32.glDeleteBuffers(1, gVbo_sphere_normal, 0);
		gVbo_sphere_normal[0] = 0;
	}

	// deatch vertex shader from shader program object
	GLES32.glDetachShader(gShaderProgramObject, gVertexShaderObject);

	// detach fragment shader from shader program obejct
	GLES32.glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	if(gVertexShaderObject != 0)
	{
		GLES32.glDeleteShader(gVertexShaderObject);
		gVertexShaderObject = 0;
	}

	// delete fragment shader object
	if(gFragmentShaderObject != 0)
	{
		GLES32.glDeleteShader(gFragmentShaderObject);
		gFragmentShaderObject = 0;
	}

	// delete shader program object
	if(gShaderProgramObject != 0)
	{
		GLES32.glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
	}

	// unlink shader program
	GLES32.glUseProgram(0);
	}
}
