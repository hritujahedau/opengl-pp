#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>


#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include "vmath.h"

enum
{
	HRH_ATTRIBUTE_POSITION = 0 ,
	HRH_ATTRIBUTE_COLOR ,
	HRH_ATTRIBUTE_NORMAL ,
	HRH_ATTRIBUTE_TEXTURE_0
};

//namespace
using namespace std;

//global variable declarations
bool bFullScreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;
GLXContext gGLXContext; // HGLRC

using namespace vmath;

typedef GLXContext (*glxCreateContextAttribsARBProc)( Display *, GLXFBConfig, GLXContext, Bool, const int *);

glxCreateContextAttribsARBProc  glxCreateContextAttribsARB = NULL;

GLXFBConfig gGLXFBConfig;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao_graph;
GLuint gVbo_graph_position;
GLuint gVbo_graph_color;

GLuint gMVPUniform;

GLfloat graphColor[] = { 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat vertices[] = { 1.0f, 0.0, 0.0f, -1.0f, 0.0f, 0.0f };

mat4 gPerspectiveProjectionMatrix;

//entry point function
int main(void)
{
	//function prototype
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void Initialize();
	void Resize(int,int);
	void Render();
	void Uninitialize();
	
	printf("\n In main()");	

	//variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	//code
	printf("\n Entering in createWindow()");
	CreateWindow();
	printf("\n Entering in Initialize()");
	Initialize();

	//Message Loop
	XEvent event;
	KeySym keysym;
	bool bDone=false;

	while(bDone==false)
	{
	    while(XPending(gpDisplay))
	    {
		XNextEvent(gpDisplay, &event);
		switch(event.type)
		{
			case MapNotify:
				break;
			case KeyPress:
				keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0,0);
				switch(keysym)
				{
					case XK_Escape:
						bDone=true;
						Uninitialize();
						//exit(0); no need as we are checking in while condition
					case XK_F:
					case XK_f:
						if(bFullScreen == false)
						{
							ToggleFullScreen();
							bFullScreen=true;
						}
						else
						{
							ToggleFullScreen();
							bFullScreen=false;
						}
						break;
					default:
						break;
	
				}
				break;
			case ButtonPress:
				switch(event.xbutton.button)
				{
					case 1:
						break;
					case 2:
						break;
					case 3:
						break;
					default:
						break;
				}
				break;
			case MotionNotify:
				break;
			case ConfigureNotify:
				winWidth=event.xconfigure.width;
				winHeight=event.xconfigure.height;
				Resize(winWidth, winHeight);
				break;
			case Expose:
				break;
			case DestroyNotify:
				break;
			case 33:
				bDone = true;
				Uninitialize();
				//exit(0); no need as we are checking in while condition
			default:
				break;
			
		} // switch
	    } // XPending Loop
	    Render();
	}
	Uninitialize();
	return 0;
}

void CreateWindow(void)
{

	//function declaration
	void Uninitialize();

	//variable declaration
	XSetWindowAttributes winAttribs;
	GLXFBConfig *pGLXFBConfig = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int numFbConfigs;
	int i;
	int styleMask;

	static int framebufferAttribes[] = 
				{
					GLX_X_RENDERABLE, True, 
					GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT, 
					GLX_RENDER_TYPE, GLX_RGBA_BIT, 
					GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR, 				
					GLX_RED_SIZE, 8, 
					GLX_GREEN_SIZE, 8, 
					GLX_BLUE_SIZE, 8, 
					GLX_ALPHA_SIZE, 8, 
					GLX_DEPTH_SIZE, 24, 
					GLX_STENCIL_SIZE, 8, 
					GLX_DOUBLEBUFFER, True, 
					None 
				};

	//code
	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("Error: Uable To Open X Display.\nExiting Now...\n ");
		Uninitialize();
		exit(1);
	}
	printf("\ngot gp display\n");
	pGLXFBConfig = glXChooseFBConfig(gpDisplay, XDefaultScreen(gpDisplay), framebufferAttribes, &numFbConfigs);

	if(pGLXFBConfig == NULL)
	{
		printf("\nError: Not get pGLXFBConfig\n");
		Uninitialize();
		exit(1);
		
	}

	int bestFrameBufferConfig = -1;
	int worstFrameBufferConfig = -1;
	int bestNumberOfSample = -1;
	int worstNumberOfSample = 99;

	printf("\nNumber of FB Config = %d " , numFbConfigs);

	for(int i =0; i< numFbConfigs; i++ )
	{
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfig[i]);
		if(pTempXVisualInfo != NULL)
		{
			int sampleBuffers, samples;
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLES, &samples);

			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNumberOfSample)
			{
				bestFrameBufferConfig = i;
				bestNumberOfSample = samples;
			} 
		
			if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < 99)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSample = samples;
			}
			
			printf("i = %d , sample = %d, sampleBuffers = %d", i, samples, sampleBuffers);
		} // If block
		XFree(pTempXVisualInfo);
	} // loop

	bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];
	gGLXFBConfig = bestGLXFBConfig;

	XFree(pGLXFBConfig);

	// get best vusualInfo now
	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, gGLXFBConfig);

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			gpXVisualInfo->visual,
			AllocNone);
	gColormap = winAttribs.colormap;
	winAttribs.background_pixel=BlackPixel(gpDisplay, XDefaultScreen(gpDisplay));
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask
				| KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	gWindow=XCreateWindow(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		0,
		0,
		giWindowWidth,
		giWindowHeight,
		0,
		gpXVisualInfo->depth,
		InputOutput,
		gpXVisualInfo->visual,
		styleMask,
		&winAttribs);
	if(!gWindow)
	{
		printf("Error: Failed To Create Main Window.\nExiting Now...\n");
		Uninitialize();	
		exit(1);
	}
	XStoreName(gpDisplay, gWindow, "Hrituja's First XWidow");
	Atom windowManagerDelete=XInternAtom(gpDisplay, "WM_DELETE_WINDOW", true);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	
	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen(void)
{
	//variable declaration
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	//code
	wm_state=XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(XEvent));
	
	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullScreen ? 0 : 1;
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN", false);
	xev.xclient.data.l[1]=fullscreen;
	
	XSendEvent(gpDisplay, 
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False,
		StructureNotifyMask,
		&xev);
}

void Initialize()
{
	// Function Declaration
	void Resize(int, int);
	void Uninitialize();
	
	//gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);

	glxCreateContextAttribsARB = (glxCreateContextAttribsARBProc) glXGetProcAddressARB( (GLubyte*) "glXCreateContextAttribsARB");	
	
	const int attribes[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
			GLX_CONTEXT_MINOR_VERSION_ARB, 6,
			GLX_CONTEXT_PROFILE_MASK_ARB,
			GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
			None
		};

	gGLXContext = glxCreateContextAttribsARB( gpDisplay, gGLXFBConfig, 0, True, attribes); // richest context
	if(!gGLXContext)
	{
		printf("\nDidn't get gGLXContext\n");
		const int attribes_1[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
			GLX_CONTEXT_MINOR_VERSION_ARB, 0,
			GLX_CONTEXT_PROFILE_MASK_ARB,
			GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
			None
		};

		gGLXContext = glxCreateContextAttribsARB( gpDisplay, gGLXFBConfig, 0, True, attribes_1); // richest context
	}
	printf("\nGot gGLXContext\n");

	Bool isDirectContext = glXIsDirect(gpDisplay, gGLXContext);
	if(isDirectContext == True)
	{
		printf("\n H/w Rendering support");
	}


	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		Uninitialize();
		exit(1);
	}

	//OpenGL Logs
	printf("OpenGL Logs");
	printf("OpenGL Vendor: %s \n ", glGetString(GL_VENDOR));
	printf("OpenGL Renderer: %s \n", glGetString(GL_RENDERER));
	printf("OpenGL Version : %s \n", glGetString(GL_VERSION));
	printf("OpenGL Shading Language : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	printf("Open Enabled extension\n");
	GLint numExtension = 0;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	printf("Open Enabled total extension %d \n", numExtension);

	for (int i = 0; i < numExtension; i++)
	{		
		//printf("%s\t", glGetStringi(GL_EXTENSIONS,i));
	}

	/********** VERTEX SHADER ***********/
	// CREATE SHADER

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	
	//provide souce code to shader
	const GLchar* vertexShaderSourceCode =  "#version 450 core\n"\
											"in vec4 vPosition;" \
											"in vec4 vColor;" \
											"out vec4 out_color; " \
											"uniform mat4 u_mvpMatrix;" \
											"void main()"\
											"{" \
											"out_color = vColor;" \
											"gl_Position = u_mvpMatrix * vPosition;"\
											"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				printf("Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				Uninitialize();
				exit(1);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCode = "#version 450 core\n"\
											"in vec4 out_color;" \
											"out vec4 FragColor; " \
											"void main()"\
											"{"\
											"FragColor = out_color;" \
											"}";
	

	glShaderSource(gFragmentShaderObject, 1, (const char**)&fragmentShaderSourceCode, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				printf("Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				Uninitialize();
				exit(1);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_COLOR, "vColor");

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObject);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				printf("Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				Uninitialize();
				exit(1);
			}
		}
	}

	// get MVP uniform location
	gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvpMatrix");

	// ***** vertex, colors, shadersatribs , vbo, vao initialization *********
	const GLfloat triangleVertices[] =
	{
		0.0f, 1.0f, 0.0f, //apex
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f
	};

	glGenVertexArrays(1, &gVao_graph);
	glBindVertexArray(gVao_graph);

	/****
	If we don't use gVao need to write #1, #2, #3 function in display again.
	****/

	/***********************************gVao*********************************************************/

	// position
	glGenBuffers(1, &gVbo_graph_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position); //Bind gVbo
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), NULL, GL_DYNAMIC_DRAW); // feed the data #1	
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);	// kontya variable sathi, kiti elements, type #2	
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION); // #3
	glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo

	// color
	glGenBuffers(1, &gVbo_graph_color); // Bind gVbo_color 
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(graphColor), graphColor, GL_DYNAMIC_DRAW); // #1
	glVertexAttribPointer(HRH_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL); // #2
	glEnableVertexAttribArray(HRH_ATTRIBUTE_COLOR); // #3
	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind Bind gVbo_color
	
	// unbind gVao
	glBindVertexArray(0); // unbind for gVao

	// set-up depth buffers
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // don't work

	// we will always cull face for better performance
	glEnable(GL_CULL_FACE);

	gPerspectiveProjectionMatrix = mat4::identity();

	Resize(giWindowWidth, giWindowHeight);
}

void Resize(int width, int height) {
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) height);

	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); // no 0.1f

}

void Render()
{
	// FUNCTION DECLARATION
	void graph();
	void hallowRectangle();

	// CODE
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// START USING OpenGL program object
	
	glUseProgram(gShaderProgramObject);

	//OpenGL Drawing For Trianlge
	// set modelview & modelviewProjection matrices to indentity
	mat4 translateMatrix = vmath::translate(0.0f, 0.0f, -1.0f);
	mat4 modelViewMatrix = translateMatrix;
	mat4 modelViewProjectionMatrix = mat4::identity();
	

	// multiply the modelview and orthographic matrix to get modelviewProjectyion matrix
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix; // ORDER IS IMPORTANT

	// pass above modelviewprojection matrix to vertex shader 'u_mvpMatrix' shader variable
	// whose value we already have calculated in inititialization() by uisng glGetUniformLocation()
	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	graph();

	translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelViewMatrix = translateMatrix;
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	hallowRectangle();


	// stop using OpenGL program object
	glUseProgram(0);

	glXSwapBuffers(gpDisplay, gWindow);
}

void hallowRectangle()
{
	// VARIABLE DECLARATION
	int i = 0;
	GLfloat colorArray[] = {
		1.0f,0.0f,0.0f,
		0.0f,1.0f,0.0f,
		0.0f,0.0f,1.0f,
		1.0f,10.f,0.0f,
		0.0f,1.0f,1.0f,
		1.0f,0.0f,1.0f,
		0.0f,0.5f,0.5f,
		0.5f,0.5f,0.0f,
		0.5f,0.0f,0.5f,
		0.5f,1.0f,0.0f
	};

	// CODE
	for (GLfloat points = 0.1; points <= 0.1 + 1.0; points += 0.1)
	{
		GLfloat rectangle[] = {
			points, points, 0.0f,
			-points, points, 0.0f,
			-points, -points, 0.0f,
			points, -points, 0.0f ,
			points, points, 0.0f,
		};

		GLfloat color[] = {
			colorArray[i], colorArray[i + 1], colorArray[i + 2],
			colorArray[i], colorArray[i + 1], colorArray[i + 2],
			colorArray[i], colorArray[i + 1], colorArray[i + 2],
			colorArray[i], colorArray[i + 1], colorArray[i + 2],
			colorArray[i], colorArray[i + 1], colorArray[i + 2]
		};
		i = i + 3;

		glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_color);
		glBufferData(GL_ARRAY_BUFFER, sizeof(color), color, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
		glBufferData(GL_ARRAY_BUFFER, sizeof(rectangle), rectangle, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(gVao_graph);
		glDrawArrays(GL_LINES, 0, 2);
		glDrawArrays(GL_LINES, 1, 2);
		glDrawArrays(GL_LINES, 2, 2);
		glDrawArrays(GL_LINES, 3, 2);
		glBindVertexArray(0);
	}

}

void graph()
{
	GLfloat distanceForX = 0, distanceForY = 0;

	graphColor[0] = 0.0f;
	graphColor[1] = 0.0f;
	graphColor[2] = 1.0f;
	graphColor[3] = 0.0f;
	graphColor[4] = 0.0f;
	graphColor[5] = 1.0f;

	glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(graphColor), graphColor, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	for (int i = 0; i < 20; i++) {

		// verticle up
		vertices[0] = 1.5f;
		vertices[1] = distanceForX;
		vertices[2] = 0.0f;

		vertices[3] = -1.5f;
		vertices[4] = distanceForX;
		vertices[5] = 0.0f;

		glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(gVao_graph);
		glDrawArrays(GL_LINES, 0, 2);
		glBindVertexArray(0);

		// verticle down
		vertices[0] = 1.5f;
		vertices[1] = -distanceForX;
		vertices[2] = 0.0f;

		vertices[3] = -1.5f;
		vertices[4] = -distanceForX;
		vertices[5] = 0.0f;

		glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(gVao_graph);
		glDrawArrays(GL_LINES, 0, 2);
		glBindVertexArray(0);

		// horizontal right
		vertices[0] = -distanceForY;
		vertices[1] = 1.5;
		vertices[2] = 0.0f;

		vertices[3] = -distanceForY;
		vertices[4] = -1.5f;
		vertices[5] = 0.0f;

		glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(gVao_graph);
		glDrawArrays(GL_LINES, 0, 2);
		glBindVertexArray(0);

		// horizontal left
		vertices[0] = distanceForY;
		vertices[1] = 1.5;
		vertices[2] = 0.0f;

		vertices[3] = distanceForY;
		vertices[4] = -1.5f;
		vertices[5] = 0.0f;

		glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(gVao_graph);
		glDrawArrays(GL_LINES, 0, 2);
		glBindVertexArray(0);


		distanceForX += (GLfloat)1 / 46;
		distanceForY += (GLfloat)1 / 25;
	}
	graphColor[0] = 0.0f;
	graphColor[1] = 1.0f;
	graphColor[2] = 0.0f;
	graphColor[3] = 0.0f;
	graphColor[4] = 1.0f;
	graphColor[5] = 0.0f;

	glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(graphColor), graphColor, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// horizontal green line
	vertices[0] = 1.5f;
	vertices[1] = 0.0f;
	vertices[2] = 0.0f;

	vertices[3] = -1.5f;
	vertices[4] = 0.0f;
	vertices[5] = 0.0f;

	glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(gVao_graph);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	// verticle green line
	vertices[0] = 0.0f;
	vertices[1] = 1.5f;
	vertices[2] = 0.0f;

	vertices[3] = 0.0f;
	vertices[4] = -1.5f;
	vertices[5] = 0.0f;

	glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(gVao_graph);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

}

void Uninitialize()
{
	// destroy gVao_triangle
	if (gVao_graph)
	{
		glDeleteVertexArrays(1, &gVao_graph);
		gVao_graph = 0;
	}

	if (gVbo_graph_color)
	{
		glDeleteBuffers(1, &gVbo_graph_color);
		gVbo_graph_color = 0;
	}

	if (gVbo_graph_position)
	{
		glDeleteBuffers(1, &gVbo_graph_position);
		gVbo_graph_position = 0;
	}

	// deatch vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	// detach fragment shader from shader program obejct
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject);

	gVertexShaderObject = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	if(currentGLXContext = gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}
