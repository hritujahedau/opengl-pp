#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>


#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include <SOIL/SOIL.h>
#include "vmath.h"

//namespace
using namespace std;

//global variable declarations
bool bFullScreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;
GLXContext gGLXContext; // HGLRC

FILE *gpFile;

using namespace vmath;

typedef GLXContext (*glxCreateContextAttribsARBProc)( Display *, GLXFBConfig, GLXContext, Bool, const int *);

glxCreateContextAttribsARBProc  glxCreateContextAttribsARB = NULL;

GLXFBConfig gGLXFBConfig;

enum
{
	HRH_ATTRIBUTE_POSITION = 0 ,
	HRH_ATTRIBUTE_COLOR ,
	HRH_ATTRIBUTE_NORMAL ,
	HRH_ATTRIBUTE_TEXTURE_0
};

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao_cube;
GLuint gVbo_cube_position;
GLuint gVbo_cube_texcoord;
GLuint gVbo_cube_color;
GLuint gvbo_cube_normals;

GLuint gModelUniform;
GLuint gViewUniform;
GLuint gPerspectiveUniform;

GLuint gTextureSamplerUniform;
GLuint texture = 0;

GLuint gLightAmbientUniform;
GLuint gLightSpecularUniform;
GLuint gLightDiffuseUniform;
GLuint gLightPositionUniform;

GLuint gMaterialSpecularUniform;
GLuint gMaterialAmbientUniform;
GLuint gMaterialDiffuseUniform;
GLuint gMaterialShininessUniform;

GLuint gIsLightOnUniform;
GLuint gIsColorOnUniform;
GLuint gIsTextureOnUniform;

int isLightOn = 0, isColorOn = 0, isTextureOn = 0;

mat4 gPerspectiveProjectionMatrix;

GLfloat lightPosition[] = { 100.0f, 100.0f, 100.0f, 1.0f };
GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f };
GLfloat lightDiffuse[] = { 1.0f, 1.0f, 1.0f };
GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f };

GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f };
GLfloat materialDiffuse[] = { 1.0f, 1.0f, 1.0f };
GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0f };
GLfloat materialShinyness = 128.0f;

//entry point function
int main(void)
{
	//function prototype
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void Initialize();
	void Resize(int,int);
	void Render();
	void Uninitialize();
	
	printf("\n In main()");	

	//variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	//code
	printf("\n Entering in createWindow()");
	CreateWindow();
	printf("\n Entering in Initialize()");
	Initialize();

	//Message Loop
	XEvent event;
	KeySym keysym;
	bool bDone=false;

	while(bDone==false)
	{
	    while(XPending(gpDisplay))
	    {
		XNextEvent(gpDisplay, &event);
		switch(event.type)
		{
			case MapNotify:
				break;
			case KeyPress:
				keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0,0);
				switch(keysym)
				{
					case XK_Escape:
						bDone=true;
						Uninitialize();
						//exit(0); no need as we are checking in while condition
					case XK_F:
					case XK_f:
						if(bFullScreen == false)
						{
							ToggleFullScreen();
							bFullScreen=true;
						}
						else
						{
							ToggleFullScreen();
							bFullScreen=false;
						}
						break;
					default:
						break;
	
				}
				break;
			case ButtonPress:
				switch(event.xbutton.button)
				{
					case 1:
						break;
					case 2:
						break;
					case 3:
						break;
					default:
						break;
				}
				break;
			case MotionNotify:
				break;
			case ConfigureNotify:
				winWidth=event.xconfigure.width;
				winHeight=event.xconfigure.height;
				Resize(winWidth, winHeight);
				break;
			case Expose:
				break;
			case DestroyNotify:
				break;
			case 33:
				bDone = true;
				Uninitialize();
				//exit(0); no need as we are checking in while condition
			default:
				break;
			
		} // switch
	    } // XPending Loop
	    Render();
	}
	Uninitialize();
	return 0;
}

void CreateWindow(void)
{

	//function declaration
	void Uninitialize();

	//variable declaration
	XSetWindowAttributes winAttribs;
	GLXFBConfig *pGLXFBConfig = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int numFbConfigs;
	int i;
	int styleMask;

	static int framebufferAttribes[] = 
				{
					GLX_X_RENDERABLE, True, 
					GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT, 
					GLX_RENDER_TYPE, GLX_RGBA_BIT, 
					GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR, 				
					GLX_RED_SIZE, 8, 
					GLX_GREEN_SIZE, 8, 
					GLX_BLUE_SIZE, 8, 
					GLX_ALPHA_SIZE, 8, 
					GLX_DEPTH_SIZE, 24, 
					GLX_STENCIL_SIZE, 8, 
					GLX_DOUBLEBUFFER, True, 
					None 
				};

	//code
	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("Error: Uable To Open X Display.\nExiting Now...\n ");
		Uninitialize();
		exit(1);
	}
	printf("\ngot gp display\n");
	pGLXFBConfig = glXChooseFBConfig(gpDisplay, XDefaultScreen(gpDisplay), framebufferAttribes, &numFbConfigs);

	if(pGLXFBConfig == NULL)
	{
		printf("\nError: Not get pGLXFBConfig\n");
		Uninitialize();
		exit(1);
		
	}

	int bestFrameBufferConfig = -1;
	int worstFrameBufferConfig = -1;
	int bestNumberOfSample = -1;
	int worstNumberOfSample = 99;

	printf("\nNumber of FB Config = %d " , numFbConfigs);

	for(int i =0; i< numFbConfigs; i++ )
	{
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfig[i]);
		if(pTempXVisualInfo != NULL)
		{
			int sampleBuffers, samples;
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLES, &samples);

			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNumberOfSample)
			{
				bestFrameBufferConfig = i;
				bestNumberOfSample = samples;
			} 
		
			if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < 99)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSample = samples;
			}
			
			printf("\ni = %d , sample = %d, sampleBuffers = %d", i, samples, sampleBuffers);
		} // If block
		XFree(pTempXVisualInfo);
	} // loop

	bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];
	gGLXFBConfig = bestGLXFBConfig;

	XFree(pGLXFBConfig);

	// get best vusualInfo now
	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, gGLXFBConfig);

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			gpXVisualInfo->visual,
			AllocNone);
	gColormap = winAttribs.colormap;
	winAttribs.background_pixel=BlackPixel(gpDisplay, XDefaultScreen(gpDisplay));
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask
				| KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	gWindow=XCreateWindow(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		0,
		0,
		giWindowWidth,
		giWindowHeight,
		0,
		gpXVisualInfo->depth,
		InputOutput,
		gpXVisualInfo->visual,
		styleMask,
		&winAttribs);
	if(!gWindow)
	{
		printf("Error: Failed To Create Main Window.\nExiting Now...\n");
		Uninitialize();	
		exit(1);
	}
	XStoreName(gpDisplay, gWindow, "Hrituja's First XWidow");
	Atom windowManagerDelete=XInternAtom(gpDisplay, "WM_DELETE_WINDOW", true);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	
	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen(void)
{
	//variable declaration
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	//code
	wm_state=XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(XEvent));
	
	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullScreen ? 0 : 1;
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN", false);
	xev.xclient.data.l[1]=fullscreen;
	
	XSendEvent(gpDisplay, 
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False,
		StructureNotifyMask,
		&xev);
}

void Initialize()
{
	// Function Declaration
	void Resize(int, int);
	GLuint LoadBitmapAsTexture(const char *);
	void Uninitialize();
	
	//gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);

	glxCreateContextAttribsARB = (glxCreateContextAttribsARBProc) glXGetProcAddressARB( (GLubyte*) "glXCreateContextAttribsARB");	
	
	const int attribes[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
			GLX_CONTEXT_MINOR_VERSION_ARB, 6,
			GLX_CONTEXT_PROFILE_MASK_ARB,
			GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
			None
		};

	gGLXContext = glxCreateContextAttribsARB( gpDisplay, gGLXFBConfig, 0, True, attribes); // richest context
	if(!gGLXContext)
	{
		printf("\nDidn't get gGLXContext\n");
		const int attribes_1[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
			GLX_CONTEXT_MINOR_VERSION_ARB, 0,
			GLX_CONTEXT_PROFILE_MASK_ARB,
			GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
			None
		};

		gGLXContext = glxCreateContextAttribsARB( gpDisplay, gGLXFBConfig, 0, True, attribes_1); // richest context
	}
	printf("\nGot gGLXContext\n");

	Bool isDirectContext = glXIsDirect(gpDisplay, gGLXContext);
	if(isDirectContext == True)
	{
		printf("\nH/w Rendering support\n");
	}


	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		Uninitialize();
		exit(1);
	}

	//OpenGL Logs
	printf("OpenGL Logs");
	printf("OpenGL Vendor: %s \n", glGetString(GL_VENDOR));
	printf("OpenGL Renderer: %s \n", glGetString(GL_RENDERER));
	printf("OpenGL Version : %s \n", glGetString(GL_VERSION));
	printf("OpenGL Shading Language : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	printf("Open Enabled extension\n");
	GLint numExtension = 0;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	printf("Open Enabled total extension %d \n", numExtension);

	/*for (int i = 0; i < numExtension; i++)
	{		
		printf("%s\t", glGetStringi(GL_EXTENSIONS,i));
	}*/

	
	/********** VERTEX SHADER ***********/
// CREATE SHADER

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//provide souce code to shader
	const GLchar* vertexShaderSourceCode =
		" #version 450 core\n																		" \
		" in vec4 vPosition;																		" \
		" in vec3 vNormals;																			" \
		" in vec2 vTexCoord;																		" \
		" in vec3 vColor;																			" \
		" out vec3 outColor;																		" \
		" out vec2 outTexCoord;																		" \
		" uniform mat4 u_projectionMatrix;															" \
		" uniform mat4 u_modelMatrix;																" \
		" uniform mat4 u_viewMatrix;																" \
		" uniform int u_isLightOn;																	" \
		" uniform vec4 u_light_position;															" \
		" out vec3 light_direction;																	" \
		" out vec3 transformed_normal;																" \
		" out vec3 view_vector;																		" \
		" void main()																				" \
		" {																							" \
		"	if(u_isLightOn == 1)																	" \
		"	{																						" \
		"		vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * vPosition;					" \
		"		transformed_normal = mat3(u_viewMatrix * u_modelMatrix) * vNormals ;				" \
		"		light_direction = vec3(u_light_position - eye_coordinates );						" \
		"		view_vector = vec3 (-eye_coordinates);												" \
		"	}																						" \
		"	outTexCoord = vTexCoord;																" \
		"	gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;			" \
		"	outColor = vColor;																		" \
		" }																							";

	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				XDestroyWindow(gpDisplay, gWindow);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCode =
		" #version 450 core\n																																		" \
		" vec3 phoung_ads_lighting;																																	" \
		" in vec3 light_direction;																																	" \
		" in vec3 transformed_normal;																																" \
		" in vec3 view_vector;																																		" \
		" vec3 normalized_light_direction;																															" \
		" vec3 normalized_transformed_normal;																														" \
		" vec3 normalized_view_vector;																																" \
		" uniform vec3 u_lightAmbient;																																" \
		" uniform vec3 u_lightSpecular;																																" \
		" uniform vec3 u_lightDiffuse;																																" \
		" uniform vec3 u_materialAmbient;																															" \
		" uniform vec3 u_materialSpecular;																															" \
		" uniform vec3 u_materialDiffuse;																															" \
		" uniform float u_materialShininess;																														" \
		" uniform int u_isLightOn;																																	" \
		" uniform int u_isColor;																																	" \
		" uniform int u_isTexture;																																	" \
		" in vec2 outTexCoord;		 																																" \
		" out vec4 FragColor;																																		" \
		" in vec3 outColor;																																			" \
		" uniform sampler2D u_texture_sampler;																														" \
		" void main()																																				" \
		" {																																							" \
		"	phoung_ads_lighting = vec3(1.0, 1.0, 1.0);																												" \
		"	if(u_isLightOn == 1)																																	" \
		"	{																																						" \
		"		normalized_light_direction = normalize(light_direction);																							" \
		"		normalized_transformed_normal = normalize(transformed_normal);																						" \
		"		normalized_view_vector = normalize(view_vector);																									" \
		"		vec3 reflection_vector = reflect (-normalized_light_direction, normalized_transformed_normal);														" \
		"		vec3 ambient = u_lightAmbient * u_materialAmbient;																									" \
		"		vec3 diffuse = u_lightDiffuse * u_materialDiffuse * max( dot ( normalized_light_direction, normalized_transformed_normal), 0.0);					" \
		"		vec3 specular = u_lightSpecular * u_materialSpecular * pow( max ( dot (reflection_vector , normalized_view_vector), 0.0) , u_materialShininess);	" \
		"		phoung_ads_lighting = ambient + diffuse + specular;																									" \
		"	}																																						" \
		"	FragColor = vec4(1.0, 1.0, 1.0, 1.0);																													" \
		"	if(u_isColor == 1)																																		" \
		"	{																																						" \
		"		FragColor = vec4(outColor, 1.0);																													" \
		"	}																																						" \
		"	if(u_isTexture == 1)																																	" \
		"	{																																						" \
		"		FragColor = FragColor * texture(u_texture_sampler, outTexCoord);																					" \
		"	}																																						" \
		"	if(u_isLightOn == 1)																																	" \
		"	{																																						" \
		"		FragColor = FragColor * vec4(phoung_ads_lighting, 1.0);																								" \
		"	}																																						" \
		" }																																							";

	glShaderSource(gFragmentShaderObject, 1, (const char**)&fragmentShaderSourceCode, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				XDestroyWindow(gpDisplay, gWindow);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_TEXTURE_0, "vTexCoord");
	glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_COLOR, "vColor");
	glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_NORMAL, "vNormals");

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObject);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				XDestroyWindow(gpDisplay, gWindow);
			}
		}
	}

	// get MVP uniform location

	gModelUniform = glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
	gViewUniform = glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
	gPerspectiveUniform = glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");

	gTextureSamplerUniform = glGetUniformLocation(gShaderProgramObject, "u_texture_sampler");
	gIsLightOnUniform = glGetUniformLocation(gShaderProgramObject, "u_isLightOn");
	gIsColorOnUniform = glGetUniformLocation(gShaderProgramObject, "u_isColor");
	gIsTextureOnUniform = glGetUniformLocation(gShaderProgramObject, "u_isTexture");

	gLightAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_lightAmbient");
	gLightSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_lightSpecular");
	gLightDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_lightDiffuse");
	gLightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");

	gMaterialDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_materialDiffuse");
	gMaterialAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_materialAmbient");
	gMaterialSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_materialSpecular");
	gMaterialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_materialShininess");

	/**********************************gVao_square*******************************************************/
	const GLfloat cubeVertices[] =
	{
		// FRONT
		1.0f,1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,


		// RIGHT
		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f,-1.0f,

		//BACK
		1.0f, -1.0f,-1.0f,
		-1.0f, -1.0f,-1.0f,
		-1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,

		// LEFT
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,

		// TOP
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		// BOTTOM
		1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f

	};


	const GLfloat cubeTexCoordMatrix[] =
	{
		// 1
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f,1.0f,
		0.0f,1.0f,

		//2
		1.0f, 0.0f,
		1.0f,1.0f,
		0.0f,1.0f,
		0.0f, 0.0f,

		//3
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,

		//4
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,

		//5
		0.0f,1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,

		//6
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,

	};

	const GLfloat cubeColorMatrix[] =
	{
		// 1
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,

		//2
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,

		//3
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,

		//4
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,

		//5
		0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f,

		//6
		1.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 1.0f,

	};

	GLfloat glNormal_coord[] =
	{

		0.0f,  0.0f,  1.0f , //front
		0.0f,  0.0f,  1.0f , //front
		0.0f,  0.0f,  1.0f , //front
		0.0f,  0.0f,  1.0f , //front

		1.0f,  0.0f,  0.0f , // right
		1.0f,  0.0f,  0.0f , // right
		1.0f,  0.0f,  0.0f , // right
		1.0f,  0.0f,  0.0f , // right

		0.0f, 0.0f,  -1.0f , //back
		0.0f, 0.0f,  -1.0f , //back
		0.0f, 0.0f,  -1.0f , //back
		0.0f, 0.0f,  -1.0f , //back

		-1.0f,  0.0f, 0.0f , //left
		-1.0f,  0.0f, 0.0f , //left
		-1.0f,  0.0f, 0.0f , //left
		-1.0f,  0.0f, 0.0f , //left

		0.0f,  1.0f,  0.0f , // top
		0.0f,  1.0f,  0.0f , // top
		0.0f,  1.0f,  0.0f , // top
		0.0f,  1.0f,  0.0f , // top

		0.0f, -1.0f,  0.0f , // bottom
		0.0f, -1.0f,  0.0f , // bottom
		0.0f, -1.0f,  0.0f , // bottom
		0.0f, -1.0f,  0.0f , // bottom

	};

	glGenVertexArrays(1, &gVao_cube);
	glBindVertexArray(gVao_cube);

	// now give vertex for square geometry
	glGenBuffers(1, &gVbo_cube_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_cube_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &gVbo_cube_texcoord);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_cube_texcoord);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeTexCoordMatrix), cubeTexCoordMatrix, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_TEXTURE_0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_TEXTURE_0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &gVbo_cube_color);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_cube_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeColorMatrix), cubeColorMatrix, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &gvbo_cube_normals);
	glBindBuffer(GL_ARRAY_BUFFER, gvbo_cube_normals);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glNormal_coord), glNormal_coord, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// unbind to gVao_square
	glBindVertexArray(0);

	texture = LoadBitmapAsTexture("marble.bmp");

	/***************************************************************************************************************/

	// set-up depth buffers
	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); 
	glEnable(GL_TEXTURE_2D);

	// we will always cull face for better performance
	glEnable(GL_CULL_FACE);

	gPerspectiveProjectionMatrix = mat4::identity();

	Resize(giWindowWidth, giWindowHeight);
}

GLuint LoadBitmapAsTexture(const char *path)
{
		int w, h;
		unsigned char *ImageData = NULL;
		GLuint texture = 0;

		ImageData = SOIL_load_image(path, &w, &h, NULL, SOIL_LOAD_RGB);
	
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // IN FFP 4
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		
		//gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, ImageData);
		// glTexImage2D(GL_TEXTURE_2D, 0, 3, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, bmp.bmBits);		
		glTexImage2D(GL_TEXTURE_2D, 0, 3, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, ImageData);		
		glGenerateMipmap(GL_TEXTURE_2D);
		
		SOIL_free_image_data(ImageData);
		
		return texture;		
}

void Resize(int width, int height) {
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) height);

	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); // no 0.1f

}

void Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	static GLfloat angle = 0.0f;

	// START USING OpenGL program object
	glUseProgram(gShaderProgramObject);

	//OpenGL Drawing For Trianlge
	// set modelview & modelviewProjection matrices to indentity
	mat4 translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);

	mat4 scaleMatrix = mat4::identity();
	mat4 rotateMatrix = vmath::rotate(angle, 1.0f, 0.0f, 0.0f) *
		vmath::rotate(angle, 0.0f, 1.0f, 0.0f) *
		vmath::rotate(angle, 0.0f, 0.0f, 1.0f);

	mat4 viewMatrix = mat4::identity();
	mat4 modelMatrix = translateMatrix * rotateMatrix;

	isLightOn = 1;
	isColorOn = 1;
	isTextureOn = 0;

	if (isLightOn == 1)
	{
		glUniform4fv(gLightPositionUniform, 1, (const GLfloat*)lightPosition);
		glUniform3fv(gLightDiffuseUniform, 1, (const GLfloat*)lightDiffuse);
		glUniform3fv(gLightAmbientUniform, 1, (const GLfloat*)lightAmbient);
		glUniform3fv(gLightSpecularUniform, 1, (const GLfloat*)lightSpecular);

		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient);
		glUniform1f(gMaterialShininessUniform, materialShinyness);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDiffuse);
	}

	glUniform1i(gIsLightOnUniform, isLightOn);
	glUniform1i(gIsColorOnUniform, isColorOn);
	glUniform1i(gIsTextureOnUniform, isTextureOn);

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);
	glUniformMatrix4fv(gViewUniform, 1, GL_FALSE, (const float*)viewMatrix);
	glUniformMatrix4fv(gPerspectiveUniform, 1, GL_FALSE, (const GLfloat*)gPerspectiveProjectionMatrix);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	glUniform1i(gTextureSamplerUniform, 0);

	glBindVertexArray(gVao_cube);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
	glBindVertexArray(0);

	// stop using OpenGL program object
	glUseProgram(0);

	angle = angle + 0.1f;

	glXSwapBuffers(gpDisplay, gWindow);
}

void Uninitialize()
{
	if (gVao_cube)
	{
		glDeleteVertexArrays(1, &gVao_cube);
		gVao_cube = 0;
	}

	if (gVbo_cube_position)
	{
		glDeleteBuffers(1, &gVbo_cube_position);
		gVbo_cube_position = 0;
	}

	// deatch vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	// detach fragment shader from shader program obejct
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject);

	gVertexShaderObject = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	if(currentGLXContext = gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}
