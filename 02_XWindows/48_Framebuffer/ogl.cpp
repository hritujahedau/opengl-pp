#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>


#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include <SOIL/SOIL.h>
#include "vmath.h"

//namespace
using namespace std;

//global variable declarations
bool bFullScreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;
GLXContext gGLXContext; // HGLRC
FILE *gpFile;

using namespace vmath;

typedef GLXContext (*glxCreateContextAttribsARBProc)( Display *, GLXFBConfig, GLXContext, Bool, const int *);

glxCreateContextAttribsARBProc  glxCreateContextAttribsARB = NULL;

GLXFBConfig gGLXFBConfig;

int w, h;

enum
{
	HRH_ATTRIBUTE_POSITION = 0 ,
	HRH_ATTRIBUTE_COLOR ,
	HRH_ATTRIBUTE_NORMAL ,
	HRH_ATTRIBUTE_TEXTURE_0
};

GLuint gVertexShaderObject_1;
GLuint gVertexShaderObject_2;

GLuint gFragmentShaderObject_1;
GLuint gFragmentShaderObject_2;

GLuint gShaderProgramObject_1;
GLuint gShaderProgramObject_2;

GLuint gVao;
GLuint gVbo_position;
GLuint gVbo_index;

GLuint fbo;

GLuint gMVUniform_1;
GLuint gProjectionUniform_1;

GLuint gMVUniform_2;
GLuint gProjectionUniform_2;

GLuint gTexCoordUniform;

mat4 gPerspectiveProjectionMatrix;

GLuint color_texture = 0, depth_texture = 0;

//entry point function
int main(void)
{
	//function prototype
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void Initialize();
	void Resize(int,int);
	void Render();
	void Uninitialize();
	
	printf("\n In main()");	

	//variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	//code
	printf("\n Entering in createWindow()");
	CreateWindow();
	printf("\n Entering in Initialize()");
	Initialize();

	//Message Loop
	XEvent event;
	KeySym keysym;
	bool bDone=false;

	while(bDone==false)
	{
	    while(XPending(gpDisplay))
	    {
		XNextEvent(gpDisplay, &event);
		switch(event.type)
		{
			case MapNotify:
				break;
			case KeyPress:
				keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0,0);
				switch(keysym)
				{
					case XK_Escape:
						bDone=true;
						Uninitialize();
						//exit(0); no need as we are checking in while condition
					case XK_F:
					case XK_f:
						if(bFullScreen == false)
						{
							ToggleFullScreen();
							bFullScreen=true;
						}
						else
						{
							ToggleFullScreen();
							bFullScreen=false;
						}
						break;
					default:
						break;
	
				}
				break;
			case ButtonPress:
				switch(event.xbutton.button)
				{
					case 1:
						break;
					case 2:
						break;
					case 3:
						break;
					default:
						break;
				}
				break;
			case MotionNotify:
				break;
			case ConfigureNotify:
				winWidth=event.xconfigure.width;
				winHeight=event.xconfigure.height;
				Resize(winWidth, winHeight);
				break;
			case Expose:
				break;
			case DestroyNotify:
				break;
			case 33:
				bDone = true;
				Uninitialize();
				//exit(0); no need as we are checking in while condition
			default:
				break;
			
		} // switch
	    } // XPending Loop
	    Render();
	}
	Uninitialize();
	return 0;
}

void CreateWindow(void)
{

	//function declaration
	void Uninitialize();

	//variable declaration
	XSetWindowAttributes winAttribs;
	GLXFBConfig *pGLXFBConfig = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int numFbConfigs;
	int i;
	int styleMask;

	static int framebufferAttribes[] = 
				{
					GLX_X_RENDERABLE, True, 
					GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT, 
					GLX_RENDER_TYPE, GLX_RGBA_BIT, 
					GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR, 				
					GLX_RED_SIZE, 8, 
					GLX_GREEN_SIZE, 8, 
					GLX_BLUE_SIZE, 8, 
					GLX_ALPHA_SIZE, 8, 
					GLX_DEPTH_SIZE, 24, 
					GLX_STENCIL_SIZE, 8, 
					GLX_DOUBLEBUFFER, True, 
					None 
				};

	//code
	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("Error: Uable To Open X Display.\nExiting Now...\n ");
		Uninitialize();
		exit(1);
	}
	printf("\ngot gp display\n");
	pGLXFBConfig = glXChooseFBConfig(gpDisplay, XDefaultScreen(gpDisplay), framebufferAttribes, &numFbConfigs);

	if(pGLXFBConfig == NULL)
	{
		printf("\nError: Not get pGLXFBConfig\n");
		Uninitialize();
		exit(1);
		
	}

	int bestFrameBufferConfig = -1;
	int worstFrameBufferConfig = -1;
	int bestNumberOfSample = -1;
	int worstNumberOfSample = 99;

	printf("\nNumber of FB Config = %d " , numFbConfigs);

	for(int i =0; i< numFbConfigs; i++ )
	{
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfig[i]);
		if(pTempXVisualInfo != NULL)
		{
			int sampleBuffers, samples;
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLES, &samples);

			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNumberOfSample)
			{
				bestFrameBufferConfig = i;
				bestNumberOfSample = samples;
			} 
		
			if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < 99)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSample = samples;
			}
			
			printf("\ni = %d , sample = %d, sampleBuffers = %d", i, samples, sampleBuffers);
		} // If block
		XFree(pTempXVisualInfo);
	} // loop

	bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];
	gGLXFBConfig = bestGLXFBConfig;

	XFree(pGLXFBConfig);

	// get best vusualInfo now
	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, gGLXFBConfig);

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			gpXVisualInfo->visual,
			AllocNone);
	gColormap = winAttribs.colormap;
	winAttribs.background_pixel=BlackPixel(gpDisplay, XDefaultScreen(gpDisplay));
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask
				| KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	gWindow=XCreateWindow(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		0,
		0,
		giWindowWidth,
		giWindowHeight,
		0,
		gpXVisualInfo->depth,
		InputOutput,
		gpXVisualInfo->visual,
		styleMask,
		&winAttribs);
	if(!gWindow)
	{
		printf("Error: Failed To Create Main Window.\nExiting Now...\n");
		Uninitialize();	
		exit(1);
	}
	XStoreName(gpDisplay, gWindow, "Hrituja's First XWidow");
	Atom windowManagerDelete=XInternAtom(gpDisplay, "WM_DELETE_WINDOW", true);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	
	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen(void)
{
	//variable declaration
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	//code
	wm_state=XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(XEvent));
	
	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullScreen ? 0 : 1;
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN", false);
	xev.xclient.data.l[1]=fullscreen;
	
	XSendEvent(gpDisplay, 
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False,
		StructureNotifyMask,
		&xev);
}

void Initialize()
{
	// Function Declaration
	void Resize(int, int);
	GLuint LoadBitmapAsTexture(const char *);
	void Uninitialize();
	
	//gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);

	

	glxCreateContextAttribsARB = (glxCreateContextAttribsARBProc) glXGetProcAddressARB( (GLubyte*) "glXCreateContextAttribsARB");	
	
	const int attribes[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
			GLX_CONTEXT_MINOR_VERSION_ARB, 6,
			GLX_CONTEXT_PROFILE_MASK_ARB,
			GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
			None
		};

	gGLXContext = glxCreateContextAttribsARB( gpDisplay, gGLXFBConfig, 0, True, attribes); // richest context
	if(!gGLXContext)
	{
		printf("\nDidn't get gGLXContext\n");
		const int attribes_1[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
			GLX_CONTEXT_MINOR_VERSION_ARB, 0,
			GLX_CONTEXT_PROFILE_MASK_ARB,
			GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
			None
		};

		gGLXContext = glxCreateContextAttribsARB( gpDisplay, gGLXFBConfig, 0, True, attribes_1); // richest context
	}
	printf("\nGot gGLXContext\n");

	Bool isDirectContext = glXIsDirect(gpDisplay, gGLXContext);
	if(isDirectContext == True)
	{
		printf("\nH/w Rendering support\n");
	}


	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		Uninitialize();
		exit(1);
	}

	//OpenGL Logs
	printf("OpenGL Logs");
	printf("OpenGL Vendor: %s \n", glGetString(GL_VENDOR));
	printf("OpenGL Renderer: %s \n", glGetString(GL_RENDERER));
	printf("OpenGL Version : %s \n", glGetString(GL_VERSION));
	printf("OpenGL Shading Language : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	printf("Open Enabled extension\n");
	GLint numExtension = 0;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	printf("Open Enabled total extension %d \n", numExtension);

	/*for (int i = 0; i < numExtension; i++)
	{		
		printf("%s\t", glGetStringi(GL_EXTENSIONS,i));
	}*/

	/********** VERTEX SHADER ***********/
	// CREATE SHADER

	gVertexShaderObject_1 = glCreateShader(GL_VERTEX_SHADER);

	//provide souce code to shader

		//provide souce code to shader
	const GLchar* vertexShaderSourceCode = "#version 450 core													\n" \
		"layout (location = 0 ) in vec4 vPosition;							\n" \
		"layout (location = 1 ) in vec2 texcoord;							\n" \
		"uniform mat4 u_mvMatrix;											\n" \
		"uniform mat4 u_projectionMatrix;									\n" \
		"out vec4 color_out;												\n" \
		"out vec2 texcoord_out;												\n" \
		"void main()														\n" \
		"																	\n" \
		"{																	\n" \
		"gl_Position = u_projectionMatrix * u_mvMatrix * vPosition;			\n" \
		"color_out = vPosition * 2.0 + vec4(0.5, 0.5, 0.5, 0.0);			\n" \
		"texcoord_out = texcoord;											\n" \
		"}";

	gVertexShaderObject_1 = glCreateShader(GL_VERTEX_SHADER);

	glShaderSource(gVertexShaderObject_1, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject_1);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject_1, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_1, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_1, iInfoLogLength, &written, szInfoLog);
				printf("1: Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				XDestroyWindow(gpDisplay, gWindow);
			}
		}
	}

	printf("After Vertex Shader\n");

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObject_1 = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCode_1 = "#version 450 core																	\n"\
		"out vec4 color; 																	\n"\
		"in vec4 color_out;																\n"\
		"in vec2 texcoord_out;																\n" \
		"void main()																		\n"\
		"{																					\n"\
		"color = sin(color_out * vec4(40.0, 20.0, 30.0, 1.0)) * 0.5 + vec4(0.5);			\n" \
		"}";


	glShaderSource(gFragmentShaderObject_1, 1, (const char**)&fragmentShaderSourceCode_1, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject_1);

	// compilation error checking
	iInfoLogLength = 0;
	iShaderCompileStatus = 0;
	szInfoLog = NULL;

	glGetShaderiv(gFragmentShaderObject_1, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_1, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_1, iInfoLogLength, &written, szInfoLog);
				printf("Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				XDestroyWindow(gpDisplay, gWindow);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObject_1 = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject_1, gVertexShaderObject_1);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObject_1, gFragmentShaderObject_1);

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObject_1);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	iInfoLogLength = 0;
	iShaderCompileStatus = 0;
	szInfoLog = NULL;
	glGetProgramiv(gShaderProgramObject_1, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_1, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_1, iInfoLogLength, &written, szInfoLog);
				printf("Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				XDestroyWindow(gpDisplay, gWindow);
			}
		}
	}

	// create second vertext shader
	gVertexShaderObject_2 = glCreateShader(GL_VERTEX_SHADER);

	glShaderSource(gVertexShaderObject_2, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject_2);

	// check compilation error
	iInfoLogLength = 0;
	iShaderCompileStatus = 0;
	szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject_2, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_2, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_2, iInfoLogLength, &written, szInfoLog);
				printf("2: Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				XDestroyWindow(gpDisplay, gWindow);
			}
		}
	}

	// create second frame buffer
	gFragmentShaderObject_2 = glCreateShader(GL_FRAGMENT_SHADER);

	const char* fragmentShaderSourceCode_2 = "#version 450 core								\n" \
		"uniform sampler2D tex;												\n" \
		"out vec4 color;																	\n" \
		"in vec4 color_out;																	\n" \
		"in vec2 texcoord_out;																\n" \
		"void main()																		\n" \
		"{																					\n" \
		"	color = mix(color_out, texture ( tex, texcoord_out), 0.7);		\n" \
		"}																					\n";



	glShaderSource(gFragmentShaderObject_2, 1, (const char**)&fragmentShaderSourceCode_2, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject_2);

	// compilation error checking
	iInfoLogLength = 0;
	iShaderCompileStatus = 0;
	szInfoLog = NULL;

	glGetShaderiv(gFragmentShaderObject_2, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_2, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_2, iInfoLogLength, &written, szInfoLog);
				printf("2: Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				XDestroyWindow(gpDisplay, gWindow);
			}
		}
	}

	gShaderProgramObject_2 = glCreateProgram();

	glAttachShader(gShaderProgramObject_2, gVertexShaderObject_2);
	glAttachShader(gShaderProgramObject_2, gFragmentShaderObject_2);

	glLinkProgram(gShaderProgramObject_2);
	// CHECK LINKING ERROR
	iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject_2, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_2, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_2, iInfoLogLength, &written, szInfoLog);
				printf("2: Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				XDestroyWindow(gpDisplay, gWindow);
			}
		}
	}

	printf("After linking Shader\n");


	// get MVP uniform location
	gMVUniform_1 = glGetUniformLocation(gShaderProgramObject_1, "u_mvMatrix");
	gProjectionUniform_1 = glGetUniformLocation(gShaderProgramObject_1, "u_projectionMatrix");

	gMVUniform_2 = glGetUniformLocation(gShaderProgramObject_2, "u_mvMatrix");
	gProjectionUniform_2 = glGetUniformLocation(gShaderProgramObject_2, "u_projectionMatrix");

	printf("\nafter get uniform location");

	static const GLushort vertex_indices[] =
	{
		0, 1, 2,
		2, 1, 3,
		2, 3, 4,
		4, 3, 5,
		4, 5, 6,
		6, 5, 7,
		6, 7, 0,
		0, 7, 1,
		6, 0, 2,
		2, 4, 6,
		7, 5, 3,
		7, 3, 1
	};

	static const GLfloat vertex_data[] =
	{
		// Position                 Tex Coord
	   -0.25f, -0.25f,  0.25f,      0.0f, 1.0f,
	   -0.25f, -0.25f, -0.25f,      0.0f, 0.0f,
		0.25f, -0.25f, -0.25f,      1.0f, 0.0f,

		0.25f, -0.25f, -0.25f,      1.0f, 0.0f,
		0.25f, -0.25f,  0.25f,      1.0f, 1.0f,
	   -0.25f, -0.25f,  0.25f,      0.0f, 1.0f,

		0.25f, -0.25f, -0.25f,      0.0f, 0.0f,
		0.25f,  0.25f, -0.25f,      1.0f, 0.0f,
		0.25f, -0.25f,  0.25f,      0.0f, 1.0f,

		0.25f,  0.25f, -0.25f,      1.0f, 0.0f,
		0.25f,  0.25f,  0.25f,      1.0f, 1.0f,
		0.25f, -0.25f,  0.25f,      0.0f, 1.0f,

		0.25f,  0.25f, -0.25f,      1.0f, 0.0f,
	   -0.25f,  0.25f, -0.25f,      0.0f, 0.0f,
		0.25f,  0.25f,  0.25f,      1.0f, 1.0f,

	   -0.25f,  0.25f, -0.25f,      0.0f, 0.0f,
	   -0.25f,  0.25f,  0.25f,      0.0f, 1.0f,
		0.25f,  0.25f,  0.25f,      1.0f, 1.0f,

	   -0.25f,  0.25f, -0.25f,      1.0f, 0.0f,
	   -0.25f, -0.25f, -0.25f,      0.0f, 0.0f,
	   -0.25f,  0.25f,  0.25f,      1.0f, 1.0f,

	   -0.25f, -0.25f, -0.25f,      0.0f, 0.0f,
	   -0.25f, -0.25f,  0.25f,      0.0f, 1.0f,
	   -0.25f,  0.25f,  0.25f,      1.0f, 1.0f,

	   -0.25f,  0.25f, -0.25f,      0.0f, 1.0f,
		0.25f,  0.25f, -0.25f,      1.0f, 1.0f,
		0.25f, -0.25f, -0.25f,      1.0f, 0.0f,

		0.25f, -0.25f, -0.25f,      1.0f, 0.0f,
	   -0.25f, -0.25f, -0.25f,      0.0f, 0.0f,
	   -0.25f,  0.25f, -0.25f,      0.0f, 1.0f,

	   -0.25f, -0.25f,  0.25f,      0.0f, 0.0f,
		0.25f, -0.25f,  0.25f,      1.0f, 0.0f,
		0.25f,  0.25f,  0.25f,      1.0f, 1.0f,

		0.25f,  0.25f,  0.25f,      1.0f, 1.0f,
	   -0.25f,  0.25f,  0.25f,      0.0f, 1.0f,
	   -0.25f, -0.25f,  0.25f,      0.0f, 0.0f,
	};

	printf("After Vertex Shader\n");

	glGenVertexArrays(1, &gVao);
	glBindVertexArray(gVao);

	glGenBuffers(1, &gVbo_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_position);

	glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_data), vertex_data, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), NULL);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);

	printf("\nvbo position");

	glGenBuffers(1, &gVbo_index);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_index);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(vertex_indices), vertex_indices, GL_STATIC_DRAW);

	printf("\nvbo index");
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// create frame buffer object (fbo)
	printf("\n frame buffer creation ");

	glGenFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	printf("\n frame buffer creation color_texture");

	glGenTextures(1, &color_texture);
	glBindTexture(GL_TEXTURE_2D, color_texture);

	glTexStorage2D(GL_TEXTURE_2D, 9, GL_RGBA8, 512, 512);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	printf("\n frame buffer creation depth_texture");

	glGenTextures(1, &depth_texture);
	glBindTexture(GL_TEXTURE_2D, depth_texture);

	glTexStorage2D(GL_TEXTURE_2D, 9, GL_DEPTH_COMPONENT32F, 512, 512);

	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, color_texture, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depth_texture, 0);

	static const GLenum draw_buffers[] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, draw_buffers);

	gPerspectiveProjectionMatrix = mat4::identity();

	Resize(giWindowWidth, giWindowHeight);
}
/*
GLuint LoadBitmapAsTexture(const char *path)
{
		int w, h;
		unsigned char *ImageData = NULL;
		GLuint texture = 0;

		ImageData = SOIL_load_image(path, &w, &h, NULL, SOIL_LOAD_RGB);
	
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // IN FFP 4
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		
		//gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, ImageData);
		// glTexImage2D(GL_TEXTURE_2D, 0, 3, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, bmp.bmBits);		
		glTexImage2D(GL_TEXTURE_2D, 0, 3, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, ImageData);		
		glGenerateMipmap(GL_TEXTURE_2D);
		
		SOIL_free_image_data(ImageData);
		
		return texture;		
}*/

void Resize(int width, int height) {
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) height);

	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); // no 0.1f

	w = width;
	h = height;
}

void Render()
{
	// CODE
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// OpenGL Drawing
	// set modelview & modelviewProjection matrices to indentity
	static const GLfloat blue[] = { 0.0f, 0.0f, 0.3f, 1.0f };
	static const GLfloat one = 1.0f;
	static GLfloat angle = 1.0f;
	mat4 modelViewMatrix = vmath::translate(0.0f, 0.0f, -3.0f) *
		vmath::rotate(angle, 1.0f, 0.0f, 0.0f) *
		vmath::rotate(angle, 0.0f, 1.0f, 0.0f) *
		vmath::rotate(angle, 0.0f, 0.0f, 1.0f);

	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	//	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glViewport(0, 0, 512, 512);
	float green_color[] = { 0.0f, 1.0f, 0.0f };
	glClearBufferfv(GL_COLOR, 0, green_color);
	glClearBufferfi(GL_DEPTH_STENCIL, 0, 1.0f, 0);

	glUseProgram(gShaderProgramObject_1);

	glUniformMatrix4fv(gMVUniform_1, 1, GL_FALSE, modelViewMatrix);
	glUniformMatrix4fv(gProjectionUniform_1, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glDrawArrays(GL_TRIANGLES, 0, 36);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glViewport(0, 0, w, h);
	glClearBufferfv(GL_COLOR, 0, blue);
	glClearBufferfv(GL_DEPTH, 0, &one);

	glBindTexture(GL_TEXTURE_2D, color_texture);

	glUseProgram(gShaderProgramObject_2);

	glUniformMatrix4fv(gMVUniform_2, 1, GL_FALSE, modelViewMatrix);
	glUniformMatrix4fv(gProjectionUniform_2, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glDrawArrays(GL_TRIANGLES, 0, 36);

	glBindTexture(GL_TEXTURE_2D, 0);

	// stop using OpenGL program object	

	angle = angle + 1.0f;
	if (angle > 360.0)
	{
		angle = 1.0f;
	}

	glXSwapBuffers(gpDisplay, gWindow);
}

void Uninitialize()
{
	// destroy vao
	if (gVao)
	{
		glDeleteVertexArrays(1, &gVao);
		gVao = 0;
	}

	// destroy vbo
	if (gVbo_index)
	{
		glDeleteBuffers(1, &gVbo_index);
		gVbo_index = 0;
	}

	if (gVbo_position)
	{
		glDeleteBuffers(1, &gVbo_position);
		gVbo_position = 0;
	}

	// deatch vertex shader from shader program object
	glDetachShader(gShaderProgramObject_1, gVertexShaderObject_1);

	// detach fragment shader from shader program obejct
	glDetachShader(gShaderProgramObject_1, gFragmentShaderObject_1);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject_1);

	gVertexShaderObject_1 = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject_1);
	gFragmentShaderObject_1 = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject_1);
	gShaderProgramObject_1 = 0;

	// unlink shader program
	glUseProgram(0);

	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	if(currentGLXContext = gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}
