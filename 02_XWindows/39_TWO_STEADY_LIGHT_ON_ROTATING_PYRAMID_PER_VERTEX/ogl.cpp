#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>


#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include <SOIL/SOIL.h>
#include "vmath.h"

//namespace
using namespace std;

//global variable declarations
bool bFullScreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;
GLXContext gGLXContext; // HGLRC

using namespace vmath;

typedef GLXContext (*glxCreateContextAttribsARBProc)( Display *, GLXFBConfig, GLXContext, Bool, const int *);

glxCreateContextAttribsARBProc  glxCreateContextAttribsARB = NULL;

GLXFBConfig gGLXFBConfig;

enum
{
	HRH_ATTRIBUTE_POSITION = 0 ,
	HRH_ATTRIBUTE_COLOR ,
	HRH_ATTRIBUTE_NORMAL ,
	HRH_ATTRIBUTE_TEXTURE_0
};

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao_pyramid;
GLuint gVbo_pyramid_position;
GLuint gVbo_pyramid_normals;

GLuint gModelUniform;
GLuint gViewUniform;
GLuint gPerspectiveUniform;

GLuint gMaterialSpecularUniform;
GLuint gMaterialAmbientUniform;
GLuint gMaterialDiffuseUniform;
GLuint gMaterialShininessUniform;

GLuint gIsLightOnUniform;

int isLightOn = 0;

mat4 gPerspectiveProjectionMatrix;

GLfloat lightPosition[] = { 100.0f, 100.0f, 100.0f, 1.0f,  -100.0f, 100.0f, 100.0f, 1.0f  };
GLfloat lightAmbient[] = { 1.0f, 0.0f, 0.0f , 0.0f, 0.0f, 1.0f};
GLfloat lightDiffuse[] = { 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightSpecular[] = { 0.0f, 0.0f, 1.0f , 0.0f, 0.0f, 1.0 };

GLuint gLightAmbientUniform;
GLuint gLightSpecularUniform;
GLuint gLightDiffusedUniform;
GLuint gLightPossitionUniform;

GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f };
GLfloat materialDiffuse[] = { 1.0f, 1.0f, 1.0f };
GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0 };
GLfloat materialShininess = 128.0f;

bool bAnimate = false;

//entry point function
int main(void)
{
	//function prototype
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void Initialize();
	void Resize(int,int);
	void Render();
	void Uninitialize();
	
	printf("\n In main()");	

	//variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	//code
	printf("\n Entering in createWindow()");
	CreateWindow();
	printf("\n Entering in Initialize()");
	Initialize();

	//Message Loop
	XEvent event;
	KeySym keysym;
	XKeyEvent xKeyEvent;
	char keys[26];

	while(1)
	{
	    while(XPending(gpDisplay))
	    {
		XNextEvent(gpDisplay, &event);
		switch(event.type)
		{
			case MapNotify:
				break;
			case KeyPress:
				keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0,0);
				switch(keysym)
				{
					case XK_Escape:
						Uninitialize();
						exit(0); //no need as we are checking in while condition
					case XK_F:
					case XK_f:
						if(bFullScreen == false)
						{
							ToggleFullScreen();
							bFullScreen=true;
						}
						else
						{
							ToggleFullScreen();
							bFullScreen=false;
						}
						break;
					default:
						break;
	
				}
				xKeyEvent.display = gpDisplay;
				xKeyEvent.window = gWindow;
				XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);

				switch(keys[0])
				{
					case 'a':
					case 'A':
						if (bAnimate == false)
						{
							bAnimate = true;
						}
						else
						{
							bAnimate = false;
						}
						break;
					case 'l':
					case 'L':
						if (isLightOn == 0)
						{
							isLightOn = 1;
						}
						else
						{
							isLightOn = 0;
						}
						break;
				}
				break;
			case ButtonPress:
				switch(event.xbutton.button)
				{
					case 1:
						break;
					case 2:
						break;
					case 3:
						break;
					default:
						break;
				}
				break;
			case MotionNotify:
				break;
			case ConfigureNotify:
				winWidth=event.xconfigure.width;
				winHeight=event.xconfigure.height;
				Resize(winWidth, winHeight);
				break;
			case Expose:
				break;
			case DestroyNotify:
				break;
			case 33:
				Uninitialize();
				exit(0); //no need as we are checking in while condition
			default:
				break;
			
		} // switch
	    } // XPending Loop
	    Render();
	}
	Uninitialize();
	return 0;
}

void CreateWindow(void)
{

	//function declaration
	void Uninitialize();

	//variable declaration
	XSetWindowAttributes winAttribs;
	GLXFBConfig *pGLXFBConfig = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int numFbConfigs;
	int i;
	int styleMask;

	static int framebufferAttribes[] = 
				{
					GLX_X_RENDERABLE, True, 
					GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT, 
					GLX_RENDER_TYPE, GLX_RGBA_BIT, 
					GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR, 				
					GLX_RED_SIZE, 8, 
					GLX_GREEN_SIZE, 8, 
					GLX_BLUE_SIZE, 8, 
					GLX_ALPHA_SIZE, 8, 
					GLX_DEPTH_SIZE, 24, 
					GLX_STENCIL_SIZE, 8, 
					GLX_DOUBLEBUFFER, True, 
					None 
				};

	//code
	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("Error: Uable To Open X Display.\nExiting Now...\n ");
		Uninitialize();
		exit(1);
	}
	printf("\ngot gp display\n");
	pGLXFBConfig = glXChooseFBConfig(gpDisplay, XDefaultScreen(gpDisplay), framebufferAttribes, &numFbConfigs);

	if(pGLXFBConfig == NULL)
	{
		printf("\nError: Not get pGLXFBConfig\n");
		Uninitialize();
		exit(1);
		
	}

	int bestFrameBufferConfig = -1;
	int worstFrameBufferConfig = -1;
	int bestNumberOfSample = -1;
	int worstNumberOfSample = 99;

	printf("\nNumber of FB Config = %d " , numFbConfigs);

	for(int i =0; i< numFbConfigs; i++ )
	{
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfig[i]);
		if(pTempXVisualInfo != NULL)
		{
			int sampleBuffers, samples;
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLES, &samples);

			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNumberOfSample)
			{
				bestFrameBufferConfig = i;
				bestNumberOfSample = samples;
			} 
		
			if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < 99)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSample = samples;
			}
			
			printf("\ni = %d , sample = %d, sampleBuffers = %d", i, samples, sampleBuffers);
		} // If block
		XFree(pTempXVisualInfo);
	} // loop

	bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];
	gGLXFBConfig = bestGLXFBConfig;

	XFree(pGLXFBConfig);

	// get best vusualInfo now
	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, gGLXFBConfig);

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			gpXVisualInfo->visual,
			AllocNone);
	gColormap = winAttribs.colormap;
	winAttribs.background_pixel=BlackPixel(gpDisplay, XDefaultScreen(gpDisplay));
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask
				| KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	gWindow=XCreateWindow(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		0,
		0,
		giWindowWidth,
		giWindowHeight,
		0,
		gpXVisualInfo->depth,
		InputOutput,
		gpXVisualInfo->visual,
		styleMask,
		&winAttribs);
	if(!gWindow)
	{
		printf("Error: Failed To Create Main Window.\nExiting Now...\n");
		Uninitialize();	
		exit(1);
	}
	XStoreName(gpDisplay, gWindow, "Hrituja's First XWidow");
	Atom windowManagerDelete=XInternAtom(gpDisplay, "WM_DELETE_WINDOW", true);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	
	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen(void)
{
	//variable declaration
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	//code
	wm_state=XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(XEvent));
	
	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullScreen ? 0 : 1;
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN", false);
	xev.xclient.data.l[1]=fullscreen;
	
	XSendEvent(gpDisplay, 
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False,
		StructureNotifyMask,
		&xev);
}

void Initialize()
{
	// Function Declaration
	void Resize(int, int);
	GLuint LoadBitmapAsTexture(const char *);
	void Uninitialize();
	
	//gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);

	glxCreateContextAttribsARB = (glxCreateContextAttribsARBProc) glXGetProcAddressARB( (GLubyte*) "glXCreateContextAttribsARB");	
	
	const int attribes[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
			GLX_CONTEXT_MINOR_VERSION_ARB, 6,
			GLX_CONTEXT_PROFILE_MASK_ARB,
			GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
			None
		};

	gGLXContext = glxCreateContextAttribsARB( gpDisplay, gGLXFBConfig, 0, True, attribes); // richest context
	if(!gGLXContext)
	{
		printf("\nDidn't get gGLXContext\n");
		const int attribes_1[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
			GLX_CONTEXT_MINOR_VERSION_ARB, 0,
			GLX_CONTEXT_PROFILE_MASK_ARB,
			GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
			None
		};

		gGLXContext = glxCreateContextAttribsARB( gpDisplay, gGLXFBConfig, 0, True, attribes_1); // richest context
	}
	printf("\nGot gGLXContext\n");

	Bool isDirectContext = glXIsDirect(gpDisplay, gGLXContext);
	if(isDirectContext == True)
	{
		printf("\nH/w Rendering support\n");
	}


	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		Uninitialize();
		exit(1);
	}

	//OpenGL Logs
	printf("OpenGL Logs");
	printf("OpenGL Vendor: %s \n", glGetString(GL_VENDOR));
	printf("OpenGL Renderer: %s \n", glGetString(GL_RENDERER));
	printf("OpenGL Version : %s \n", glGetString(GL_VERSION));
	printf("OpenGL Shading Language : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	printf("Open Enabled extension\n");
	GLint numExtension = 0;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	printf("Open Enabled total extension %d \n", numExtension);

	/*for (int i = 0; i < numExtension; i++)
	{		
		printf("%s\t", glGetStringi(GL_EXTENSIONS,i));
	}*/

	/********** VERTEX SHADER ***********/
	// CREATE SHADER

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	
	//provide souce code to shader
	const GLchar* vertexShaderSourceCode = "#version 450 core\n"  \
		"in vec4 v_position;" \
		"in vec3 v_normals; " \
		"uniform mat4 u_modelMatrix;" \
		"uniform mat4 u_viewMatrix;" \
		"uniform mat4 u_projectionMatrix;" \
		"uniform int u_isLightOn;" \
		"uniform vec3 u_lightAmbient[2];" \
		"uniform vec3 u_lightSpecular[2];" \
		"uniform vec3 u_lightDiffuse[2];" \
		"uniform vec4 u_lightposition[2]; " \
		"uniform vec3 u_materialAmbient; " \
		"uniform vec3 u_materialSpecular; "\
		"uniform vec3 u_materialDiffuse; "\
		"uniform float u_materialshininess; "\
		"out vec3 phoung_ads_lighting;" \
		"vec4 eye_coordinates;"\
		"vec3 transformed_normal, light_direction, reflection_vector, view_vector;"\
		"vec3 ambient, diffuse_light, specular;"\
		"void main()" \
		"{" \
		"if (u_isLightOn == 1) " \
		"{" \
			"eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" \
			"transformed_normal = normalize( mat3( u_viewMatrix * u_modelMatrix ) * v_normals); " \
			"for(int i=0; i< 2;i++) "\
			"{" \
				"light_direction = normalize( vec3 ( ( u_lightposition[i]) - eye_coordinates) ); " \
				"reflection_vector = reflect (-light_direction, transformed_normal); " \
				"view_vector = normalize ( vec3 ( -eye_coordinates)); " \
				"ambient = u_lightAmbient[i] * u_materialAmbient; " \
				"diffuse_light = u_lightDiffuse[i] * u_materialDiffuse * max ( dot (light_direction , transformed_normal), 0.0); "\
				"specular = u_lightSpecular[i] * u_materialSpecular * pow( max ( dot ( reflection_vector , view_vector), 0.0) , u_materialshininess);" \
				"phoung_ads_lighting = phoung_ads_lighting + ambient + diffuse_light + specular;" \
			"}"\
			
		"}" \
		"else"\
		"{"\
		"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" \
		"}"\
		"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				printf("Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				Uninitialize();
				exit(1);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCode = "#version 450 core\n" \
		"in vec3 phoung_ads_lighting;"\
		"out vec4 FragColor; " \
		"void main()" \
		"{"\
		"FragColor = vec4(phoung_ads_lighting, 1.0); " \
		"}";	

	glShaderSource(gFragmentShaderObject, 1, (const char**)&fragmentShaderSourceCode, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				printf("Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				Uninitialize();
				exit(1);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_POSITION, "v_position");
	glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_NORMAL, "v_normals");

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObject);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				printf("Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				Uninitialize();
				exit(1);
			}
		}
	}

	// get MVP uniform location
	gModelUniform = glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
	gViewUniform = glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
	gPerspectiveUniform = glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");
	gIsLightOnUniform = glGetUniformLocation(gShaderProgramObject, "u_isLightOn");
		
	gLightAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_lightAmbient");
	gLightSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_lightSpecular");
	gLightDiffusedUniform = glGetUniformLocation(gShaderProgramObject, "u_lightDiffuse");
	gLightPossitionUniform = glGetUniformLocation(gShaderProgramObject, "u_lightposition");

	gMaterialDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_materialDiffuse");
	gMaterialAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_materialAmbient");
	gMaterialSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_materialSpecular");
	gMaterialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_materialshininess");

	
	// ***** vertex, colors, shadersatribs , vbo, vao initialization *********
	const GLfloat pyramidVertices[] =
				{
					// FRONT FACE
					0.0f, 1.0f, 0.0f,
					-1.0f,-1.0f, 1.0f,
					1.0f, -1.0f,1.0f,
					
					// RIGHT FACE
					0.0f, 1.0f, 0.0f,
					1.0f, -1.0f, 1.0f,
					1.0f, -1.0f, -1.0f,

					// BACK FACE
					0.0f, 1.0f, 0.0f,
					1.0f, -1.0f,-1.0f,
					-1.0f, -1.0f,-1.0f,

					// LEFT FACE
					0.0f, 1.0f, 0.0f,
					-1.0f, -1.0f,-1.0f,
					-1.0f, -1.0f,1.0f,
				};

	GLfloat pyramid_normals[] = {
		0.0f, 0.447214f, 0.894427f, //FRONT
		0.0f, 0.447214f, 0.894427f, //FRONT
		0.0f, 0.447214f, 0.894427f, //FRONT
		
		0.894427f, 0.447214f, 0.0f, // RIGHT
		0.894427f, 0.447214f, 0.0f, // RIGHT
		0.894427f, 0.447214f, 0.0f, // RIGHT
		
		0.0f, 0.447214f, -0.894427f, // BACK
		0.0f, 0.447214f, -0.894427f, // BACK
		0.0f, 0.447214f, -0.894427f, // BACK
		
		-0.894427f, 0.447214f, 0.0f, // LEFT
		-0.894427f, 0.447214f, 0.0f, // LEFT
		-0.894427f, 0.447214f, 0.0f // LEFT
	};

	glGenVertexArrays(1, &gVao_pyramid);
	glBindVertexArray(gVao_pyramid);

	/****
	If we don't use gVao need to write #1, #2, #3 function in display again.
	****/

	/***********************************gVao*********************************************************/

	//gVbo_triangle_position
	glGenBuffers(1, &gVbo_pyramid_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_pyramid_position); //Bind gVbo
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW); // feed the data #1	
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);	// kontya variable sathi, kiti elements, type #2	
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION); // #3
	glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo
	
	glGenBuffers(1, &gVbo_pyramid_normals);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_pyramid_normals);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramid_normals), pyramid_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// unbind gVao_triangle 
	glBindVertexArray(0); // unbind for gVao

	/***************************************************************************************************************/

	// set-up depth buffers
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); 
	glEnable(GL_TEXTURE_2D);

	// we will always cull face for better performance
	glEnable(GL_CULL_FACE);

	gPerspectiveProjectionMatrix = mat4::identity();

	Resize(giWindowWidth, giWindowHeight);
}

void Resize(int width, int height) {
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) height);

	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); // no 0.1f

}

void Render()
{
	// VARIABLE DECLARATION
	static GLfloat angle = 0.0f;

	// CODE
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// START USING OpenGL program object
	glUseProgram(gShaderProgramObject);

	// OpenGL Drawing
	// set modelview & modelviewProjection matrices to indentity
	mat4 translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
	mat4 rotateMatrix = vmath::rotate((GLfloat)angle, 0.0f, 1.0f, 0.0f);
	mat4 viewMatrix = mat4::identity();
	/*	vmath::lookat(
		vmath::vec3(0.0f, 0.0f, 6.0f),
		vmath::vec3(0.0f, 0.0f, 0.0f),
		vmath::vec3(0.0f, 1.0f, 0.0f)
	); */
	mat4 modelMatrix = translateMatrix * rotateMatrix;

	if (isLightOn == 1)
	{
		glUniform4fv(gLightPossitionUniform, 2, (const GLfloat*) lightPosition);
		glUniform3fv(gLightDiffusedUniform, 2, (const GLfloat*) lightDiffuse);
		glUniform3fv(gLightAmbientUniform,  2 , (const GLfloat*) lightAmbient);
		glUniform3fv(gLightSpecularUniform, 2 , (const GLfloat*) lightSpecular);
		
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*) materialAmbient);
		glUniform1f(gMaterialShininessUniform, materialShininess);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*) materialSpecular);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*) materialDiffuse);
	}

	glUniform1i(gIsLightOnUniform, isLightOn);
	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*) modelMatrix);
	glUniformMatrix4fv(gViewUniform, 1, GL_FALSE, (const float*) viewMatrix);
	glUniformMatrix4fv(gPerspectiveUniform, 1, GL_FALSE, (const GLfloat *) gPerspectiveProjectionMatrix);

	// *** bind vao **
	glBindVertexArray(gVao_pyramid);
	glDrawArrays(GL_TRIANGLES, 0, 12);
	glBindVertexArray(0);

	// stop using OpenGL program object
	glUseProgram(0);

	if (bAnimate == true)
	{
		angle = angle + 0.25f;
		if (angle >= 360)
		{
			angle = 0.0f;
		}
	}

	glXSwapBuffers(gpDisplay, gWindow);
}

void Uninitialize()
{
	
	// destroy gVao_triangle
	if (gVao_pyramid)
	{
		glDeleteVertexArrays(1, &gVao_pyramid);
		gVao_pyramid = 0;
	}

	if (gVbo_pyramid_position)
	{
		glDeleteBuffers(1, &gVbo_pyramid_position);
		gVbo_pyramid_position = 0;
	}

	// deatch vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	// detach fragment shader from shader program obejct
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject);

	gVertexShaderObject = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	if(currentGLXContext = gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}
