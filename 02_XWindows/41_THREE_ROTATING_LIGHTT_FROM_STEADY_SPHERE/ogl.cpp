#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>


#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include <SOIL/SOIL.h>
#include "vmath.h"
#include "Sphere.h"
#define RADIAN_VALUE 3.14159/180

//namespace
using namespace std;

//global variable declarations
bool bFullScreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;
GLXContext gGLXContext; // HGLRC

using namespace vmath;

typedef GLXContext (*glxCreateContextAttribsARBProc)( Display *, GLXFBConfig, GLXContext, Bool, const int *);

glxCreateContextAttribsARBProc  glxCreateContextAttribsARB = NULL;

GLXFBConfig gGLXFBConfig;

enum
{
	HRH_ATTRIBUTE_POSITION = 0 ,
	HRH_ATTRIBUTE_COLOR ,
	HRH_ATTRIBUTE_NORMAL ,
	HRH_ATTRIBUTE_TEXTURE_0
};

GLuint gVertexShaderObjectPerVertex;
GLuint gFragmentShaderObjectPerVertex;

GLuint gVertexShaderObjectPerFragment;
GLuint gFragmentShaderObjectPerFragment;

GLuint gShaderProgramObjectPerVertex;
GLuint gShaderProgramObjectPerFragment;
GLuint gShaderProgramObject;

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint gModelUniform;
GLuint gViewUniform;
GLuint gPerspectiveUniform;

GLuint gMaterialSpecularUniform;
GLuint gMaterialAmbientUniform;
GLuint gMaterialDiffuseUniform;
GLuint gMaterialShininessUniform;

GLuint gIsLightOnUniform;

int isLightOn = 0;
int isPerFragmentLight = 0;
int isPerVertexLight = 0;

mat4 gPerspectiveProjectionMatrix;

GLfloat lightPosition[] = { 0.0f, 0.0f, 0.0f, 1.0f, 
							0.0f, 0.0f, 0.0f, 1.0f,
							0.0f, 0.0f, 0.0f, 1.0f
						};
						
GLfloat lightAmbient[] = {  0.0f, 0.0f, 0.0f,
							0.0f, 0.0f, 0.0f,
							0.0f, 0.0f, 0.0f
						};
						
GLfloat lightDiffuse[] = {  1.0f, 0.0f, 0.0f,
							0.0f, 1.0f, 0.0f,
							0.0f, 0.0f, 1.0f
						};
							
GLfloat lightSpecular[] = { 1.0f, 0.0f, 0.0f,
							0.0f, 1.0f, 0.0f,
							0.0f, 0.0f, 1.0
						};

GLuint gLightAmbientUniform;
GLuint gLightSpecularUniform;
GLuint gLightDiffusedUniform;
GLuint gLightPossitionUniform;

GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f };
GLfloat materialDiffuse[] = { 1.0f, 1.0f, 1.0f };
GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0 };
GLfloat materialShininess = 128.0f;

bool bAnimate = false;

GLfloat sphere_vertices[1146];
GLfloat Sphere_normals[1146];
GLfloat Sphere_textrure[764];
unsigned short sphere_elements[2280]; 
int gNumVertices;
int gNumElements;

GLfloat sin_angle = 0.0f, cos_angle = 0.0f, radius = 2;


//entry point function
int main(void)
{
	//function prototype
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void Initialize();
	void Resize(int,int);
	void Render();
	void Uninitialize();
	
	printf("\n In main()");	

	//variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	//code
	printf("\n Entering in createWindow()");
	CreateWindow();
	printf("\n Entering in Initialize()");
	Initialize();

	//Message Loop
	XEvent event;
	KeySym keysym;
	XKeyEvent xKeyEvent;
	char keys[26];

	while(1)
	{
	    while(XPending(gpDisplay))
	    {
		XNextEvent(gpDisplay, &event);
		switch(event.type)
		{
			case MapNotify:
				break;
			case KeyPress:
				keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0,0);
				switch(keysym)
				{
					case XK_Escape:
						Uninitialize();
						exit(0); //no need as we are checking in while condition
					default:
						break;
	
				}

				xKeyEvent.display = gpDisplay;
				xKeyEvent.window = gWindow;
				XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);

				switch(keys[0])
				{
					case 'a':
					case 'A':
						if (bAnimate == false)
						{
							bAnimate = true;
						}
						else
						{
							bAnimate = false;
						}
				break;
					case 'l':
					case 'L':
						if (isLightOn == 0)
						{
							isLightOn = 1;
						}
						else
						{
							isLightOn = 0;
						}
						break;
					case 'q':
					case 'Q':
						ToggleFullScreen();
						break;
					case 'f':
					case 'F':
						if(isPerFragmentLight == 0)
						{
							isPerFragmentLight = 1;
							isPerVertexLight = 0;
							isLightOn = 1;
							bAnimate = 1;
							gShaderProgramObject = gShaderProgramObjectPerFragment;
						}
						else
						{
							isPerFragmentLight = 0;
						}
						break;
					case 'v':
					case 'V':
						if(isPerVertexLight == 0)
						{
							isPerVertexLight = 1;
							isPerFragmentLight =0;
							isLightOn = 1;
							bAnimate = 1;
							gShaderProgramObject = gShaderProgramObjectPerVertex;
						}
						else
						{
							isPerVertexLight = 0;
						}
						break;
				}
				break;
			case ButtonPress:
				switch(event.xbutton.button)
				{
					case 1:
						break;
					case 2:
						break;
					case 3:
						break;
					default:
						break;
				}
				break;
			case MotionNotify:
				break;
			case ConfigureNotify:
				winWidth=event.xconfigure.width;
				winHeight=event.xconfigure.height;
				Resize(winWidth, winHeight);
				break;
			case Expose:
				break;
			case DestroyNotify:
				break;
			case 33:
				Uninitialize();
				exit(0); //no need as we are checking in while condition
			default:
				break;
			
		} // switch
	    } // XPending Loop
	    Render();
	}
	Uninitialize();
	return 0;
}

void CreateWindow(void)
{

	//function declaration
	void Uninitialize();

	//variable declaration
	XSetWindowAttributes winAttribs;
	GLXFBConfig *pGLXFBConfig = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int numFbConfigs;
	int i;
	int styleMask;

	static int framebufferAttribes[] = 
				{
					GLX_X_RENDERABLE, True, 
					GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT, 
					GLX_RENDER_TYPE, GLX_RGBA_BIT, 
					GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR, 				
					GLX_RED_SIZE, 8, 
					GLX_GREEN_SIZE, 8, 
					GLX_BLUE_SIZE, 8, 
					GLX_ALPHA_SIZE, 8, 
					GLX_DEPTH_SIZE, 24, 
					GLX_STENCIL_SIZE, 8, 
					GLX_DOUBLEBUFFER, True, 
					None 
				};

	//code
	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("Error: Uable To Open X Display.\nExiting Now...\n ");
		Uninitialize();
		exit(1);
	}
	printf("\ngot gp display\n");
	pGLXFBConfig = glXChooseFBConfig(gpDisplay, XDefaultScreen(gpDisplay), framebufferAttribes, &numFbConfigs);

	if(pGLXFBConfig == NULL)
	{
		printf("\nError: Not get pGLXFBConfig\n");
		Uninitialize();
		exit(1);
		
	}

	int bestFrameBufferConfig = -1;
	int worstFrameBufferConfig = -1;
	int bestNumberOfSample = -1;
	int worstNumberOfSample = 99;

	printf("\nNumber of FB Config = %d " , numFbConfigs);

	for(int i =0; i< numFbConfigs; i++ )
	{
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfig[i]);
		if(pTempXVisualInfo != NULL)
		{
			int sampleBuffers, samples;
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLES, &samples);

			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNumberOfSample)
			{
				bestFrameBufferConfig = i;
				bestNumberOfSample = samples;
			} 
		
			if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < 99)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSample = samples;
			}
			
			printf("\ni = %d , sample = %d, sampleBuffers = %d", i, samples, sampleBuffers);
		} // If block
		XFree(pTempXVisualInfo);
	} // loop

	bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];
	gGLXFBConfig = bestGLXFBConfig;

	XFree(pGLXFBConfig);

	// get best vusualInfo now
	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, gGLXFBConfig);

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			gpXVisualInfo->visual,
			AllocNone);
	gColormap = winAttribs.colormap;
	winAttribs.background_pixel=BlackPixel(gpDisplay, XDefaultScreen(gpDisplay));
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask
				| KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	gWindow=XCreateWindow(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		0,
		0,
		giWindowWidth,
		giWindowHeight,
		0,
		gpXVisualInfo->depth,
		InputOutput,
		gpXVisualInfo->visual,
		styleMask,
		&winAttribs);
	if(!gWindow)
	{
		printf("Error: Failed To Create Main Window.\nExiting Now...\n");
		Uninitialize();	
		exit(1);
	}
	XStoreName(gpDisplay, gWindow, "Hrituja's First XWidow");
	Atom windowManagerDelete=XInternAtom(gpDisplay, "WM_DELETE_WINDOW", true);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	
	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen(void)
{
	//variable declaration
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	//code
	wm_state=XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(XEvent));
	
	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullScreen ? 0 : 1;
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN", false);
	xev.xclient.data.l[1]=fullscreen;
	
	XSendEvent(gpDisplay, 
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False,
		StructureNotifyMask,
		&xev);
}

void Initialize()
{
	// Function Declaration
	void Resize(int, int);
	GLuint LoadBitmapAsTexture(const char *);
	void getSphereVertexData(float spherePositionCoords[1146], float sphereNormalCoords[1146], float sphereTexCoords[764], unsigned short sphereElements[2280]);
	unsigned int getNumberOfSphereVertices(void);
	unsigned int getNumberOfSphereElements(void);
	void Uninitialize();
	
	//gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);

	glxCreateContextAttribsARB = (glxCreateContextAttribsARBProc) glXGetProcAddressARB( (GLubyte*) "glXCreateContextAttribsARB");	
	
	const int attribes[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
			GLX_CONTEXT_MINOR_VERSION_ARB, 6,
			GLX_CONTEXT_PROFILE_MASK_ARB,
			GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
			None
		};

	gGLXContext = glxCreateContextAttribsARB( gpDisplay, gGLXFBConfig, 0, True, attribes); // richest context
	if(!gGLXContext)
	{
		printf("\nDidn't get gGLXContext\n");
		const int attribes_1[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
			GLX_CONTEXT_MINOR_VERSION_ARB, 0,
			GLX_CONTEXT_PROFILE_MASK_ARB,
			GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
			None
		};

		gGLXContext = glxCreateContextAttribsARB( gpDisplay, gGLXFBConfig, 0, True, attribes_1); // richest context
	}
	printf("\nGot gGLXContext\n");

	Bool isDirectContext = glXIsDirect(gpDisplay, gGLXContext);
	if(isDirectContext == True)
	{
		printf("\nH/w Rendering support\n");
	}


	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		Uninitialize();
		exit(1);
	}

	//OpenGL Logs
	printf("OpenGL Logs");
	printf("OpenGL Vendor: %s \n", glGetString(GL_VENDOR));
	printf("OpenGL Renderer: %s \n", glGetString(GL_RENDERER));
	printf("OpenGL Version : %s \n", glGetString(GL_VERSION));
	printf("OpenGL Shading Language : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	printf("Open Enabled extension\n");
	GLint numExtension = 0;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	printf("Open Enabled total extension %d \n", numExtension);

	/*for (int i = 0; i < numExtension; i++)
	{		
		printf("%s\t", glGetStringi(GL_EXTENSIONS,i));
	}*/

	/*************************************************LIGHT PER VERTEX********************************************************/
										/************** VERTEX SHADER ***********/
	
	// CREATE SHADER
	gVertexShaderObjectPerVertex = glCreateShader(GL_VERTEX_SHADER);
	
	//provide souce code to shader
	const GLchar* vertexShaderSourceCodePerVertex = "#version 450 core\n"  \
		"in vec4 v_position;" \
		"in vec3 v_normals; " \
		"uniform mat4 u_modelMatrix;" \
		"uniform mat4 u_viewMatrix;" \
		"uniform mat4 u_projectionMatrix;" \
		"uniform int u_isLightOn;" \
		"uniform vec3 u_lightAmbient[3];" \
		"uniform vec3 u_lightSpecular[3];" \
		"uniform vec3 u_lightDiffuse[3];" \
		"uniform vec4 u_lightposition[3]; " \
		"uniform vec3 u_materialAmbient; " \
		"uniform vec3 u_materialSpecular; "\
		"uniform vec3 u_materialDiffuse; "\
		"uniform float u_materialshininess; "\
		"out vec3 phoung_ads_lighting;" \
		"vec4 eye_coordinates;"\
		"vec3 transformed_normal, light_direction, reflection_vector, view_vector;"\
		"vec3 ambient, diffuse_light, specular;"\
		"void main()" \
		"{" \
		"if (u_isLightOn == 1) " \
		"{" \
			"eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" \
			"transformed_normal = normalize( mat3( u_viewMatrix * u_modelMatrix ) * v_normals); " \
			"for(int i=0; i< 3;i++) "\
			"{" \
				"light_direction = normalize( vec3 ( ( u_lightposition[i]) - eye_coordinates) ); " \
				"reflection_vector = reflect (-light_direction, transformed_normal); " \
				"view_vector = normalize ( vec3 ( -eye_coordinates)); " \
				"ambient = u_lightAmbient[i] * u_materialAmbient; " \
				"diffuse_light = u_lightDiffuse[i] * u_materialDiffuse * max ( dot (light_direction , transformed_normal), 0.0); "\
				"specular = u_lightSpecular[i] * u_materialSpecular * pow( max ( dot ( reflection_vector , view_vector), 0.0) , u_materialshininess);" \
				"phoung_ads_lighting = phoung_ads_lighting + ambient + diffuse_light + specular;" \
			"}"\
		"}" \
		"else"\
		"{"\
		"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" \
		"}"\
		"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" \
		"}";

	glShaderSource(gVertexShaderObjectPerVertex, 1, (const GLchar**)&vertexShaderSourceCodePerVertex, NULL);

	// compile shader
	glCompileShader(gVertexShaderObjectPerVertex);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
				printf("PEr Vertex Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				Uninitialize();
				exit(1);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObjectPerVertex = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCodePerVertex = "#version 450 core\n" \
		"in vec3 phoung_ads_lighting;"\
		"out vec4 FragColor; " \
		"void main()" \
		"{"\
		"FragColor = vec4(phoung_ads_lighting, 1.0); " \
		"}";

	glShaderSource(gFragmentShaderObjectPerVertex, 1, (const char**)&fragmentShaderSourceCodePerVertex, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObjectPerVertex);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
				printf("Per Vertex Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				Uninitialize();
				exit(1);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObjectPerVertex = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);

	// pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(gShaderProgramObjectPerVertex, HRH_ATTRIBUTE_POSITION, "v_position");
	glBindAttribLocation(gShaderProgramObjectPerVertex, HRH_ATTRIBUTE_NORMAL, "v_normals");

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObjectPerVertex);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObjectPerVertex, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectPerVertex, iInfoLogLength, &written, szInfoLog);
				printf("Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				Uninitialize();
				exit(1);
			}
		}
	}
	

	/*************************************************LIGHT PER FRGAMENT********************************************************/
										/************** VERTEX SHADER ***********/
	
	
	gVertexShaderObjectPerFragment = glCreateShader(GL_VERTEX_SHADER);
	
	//provide souce code to shader
	const GLchar* vertexShaderSourceCodePerFragment =  "#version 450 core\n"  \
	"in vec4 v_position;" \
	"in vec3 v_normals; " \
	"uniform mat4 u_modelMatrix;" \
	"uniform mat4 u_viewMatrix;" \
	"uniform mat4 u_projectionMatrix;" \
	"uniform int u_isLightOn;" \
	"uniform vec4 u_lightposition[3]; " \
	"out vec3 light_direction[3]; "\
	"out vec3 transformed_normal;"\
	"out vec3 view_vector;"\
	"void main()" \
	"{" \
	"if (u_isLightOn == 1) " \
	"{" \
		"vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" \
		"transformed_normal =  mat3( u_viewMatrix * u_modelMatrix ) * v_normals;" \
		"for(int i = 0; i< 3; i++)"\
		"{"\
			"light_direction[i] = vec3 ( u_lightposition[i]  - eye_coordinates);" \
		"}"\
		"view_vector =  vec3 ( -eye_coordinates);" \
	"}" \
	"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" \
	"}";

	glShaderSource(gVertexShaderObjectPerFragment, 1, (const GLchar**)&vertexShaderSourceCodePerFragment, NULL);

	// compile shader
	glCompileShader(gVertexShaderObjectPerFragment);

	// check compilation error
	iInfoLogLength = 0;
	iShaderCompileStatus = 0;
	szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
				printf("Per Frgament Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				Uninitialize();
				exit(1);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObjectPerFragment = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCodePerFragment = "#version 450 core\n" \
		"vec3 phoung_ads_lighting;" \
		"out vec4 FragColor; " \
		"in vec3 light_direction[3]; "\
		"in vec3 transformed_normal;"\
		"in vec3 view_vector;"\
		"vec3 normalized_light_direction; "\
		"vec3 normalized_transformed_normal;"\
		"vec3 normalized_view_vector;"\
		"uniform vec3 u_lightAmbient[3];" \
		"uniform vec3 u_lightSpecular[3];" \
		"uniform vec3 u_lightDiffuse[3];" \
		"uniform vec3 u_materialAmbient; " \
		"uniform vec3 u_materialSpecular; "\
		"uniform vec3 u_materialDiffuse; "\
		"uniform float u_materialshininess; "\
		"uniform int u_isLightOn;" \
		"vec3 reflection_vector, ambient, diffuse_light, specular;"\
		"void main()" \
		"{"\
		"if (u_isLightOn == 1) " \
		"{" \
			"normalized_view_vector = normalize(view_vector);"\
			"normalized_transformed_normal = normalize(transformed_normal);"\
			"for(int i = 0; i < 3 ; i++)"\
			"{"\
				"normalized_light_direction = normalize(light_direction[i]);"\
				"reflection_vector = reflect (-normalized_light_direction, normalized_transformed_normal); " \
				"ambient = u_lightAmbient[i]  * u_materialAmbient; " \
				"diffuse_light = u_lightDiffuse[i] * u_materialDiffuse * max ( dot (normalized_light_direction , normalized_transformed_normal), 0.0); "\
				"specular = u_lightSpecular[i]  * u_materialSpecular * pow( max ( dot ( reflection_vector , normalized_view_vector), 0.0) , u_materialshininess);" \
				"phoung_ads_lighting = phoung_ads_lighting + ambient + diffuse_light + specular;" \
			"}"\
		"}"\
		"else"\
		"{"\
			"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" \
		"}"\
		"FragColor = vec4(phoung_ads_lighting, 1.0); " \
		"}";

	glShaderSource(gFragmentShaderObjectPerFragment, 1, (const char**)&fragmentShaderSourceCodePerFragment, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObjectPerFragment);

	// compilation error checking
	glGetShaderiv(gFragmentShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
				printf("Per Frgament Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				Uninitialize();
				exit(1);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM
	gShaderProgramObjectPerFragment = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);

	// pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(gShaderProgramObjectPerFragment, HRH_ATTRIBUTE_POSITION, "v_position");
	glBindAttribLocation(gShaderProgramObjectPerFragment, HRH_ATTRIBUTE_NORMAL, "v_normals");

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObjectPerFragment);

	// CHECK LINKING ERROR
	iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObjectPerFragment, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectPerFragment, iInfoLogLength, &written, szInfoLog);
				printf("Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				Uninitialize();
				exit(1);
			}
		}
	}
			

	/************************************************* vertex, colors, shadersatribs , vbo, vao initialization ************************************/
	
	getSphereVertexData(sphere_vertices, Sphere_normals, Sphere_textrure, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// ***** vertex, colors, shadersatribs , vbo, vao initialization *********
	
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo

	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Sphere_normals), Sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0); // end for Vao

	/***************************************************************************************************************/

	// set-up depth buffers
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); 

	// we will always cull face for better performance
	glEnable(GL_CULL_FACE);

	gPerspectiveProjectionMatrix = mat4::identity();

	Resize(giWindowWidth, giWindowHeight);
}


void Resize(int width, int height) {
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) height);

	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); // no 0.1f

}

void Render()
{
		// VARIABLE DECLARATION
	static GLfloat angle_red = 360.0f,angle_blue = 0.0f, angle_green = 360.0f;

	gShaderProgramObject = (isPerVertexLight == 1) ? gShaderProgramObjectPerVertex : gShaderProgramObjectPerFragment  ;
	
	// CODE
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// START USING OpenGL program object
	glUseProgram(gShaderProgramObject);

	// OpenGL Drawing
	// set modelview & modelviewProjection matrices to indentity
	mat4 translateMatrix = vmath::translate(0.0f, 0.0f, -1.55f);
	mat4 viewMatrix = mat4::identity();
	mat4 modelMatrix = translateMatrix;
	
	// get MVP uniform location
	gModelUniform = glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
	gViewUniform = glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
	gPerspectiveUniform = glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");
	gIsLightOnUniform = glGetUniformLocation(gShaderProgramObject, "u_isLightOn");
		
	gLightAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_lightAmbient");
	gLightSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_lightSpecular");
	gLightDiffusedUniform = glGetUniformLocation(gShaderProgramObject, "u_lightDiffuse");
	gLightPossitionUniform = glGetUniformLocation(gShaderProgramObject, "u_lightposition");

	gMaterialDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_materialDiffuse");
	gMaterialAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_materialAmbient");
	gMaterialSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_materialSpecular");
	gMaterialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_materialshininess");

	if (isLightOn == 1)
	{
		sin_angle = radius * sin(angle_red * RADIAN_VALUE);
		cos_angle = radius * cos(angle_red * RADIAN_VALUE);
		
		lightPosition[1] = sin_angle;
		lightPosition[2] = cos_angle;
		
		sin_angle = radius * sin(angle_blue * RADIAN_VALUE);
		cos_angle = radius * cos(angle_blue * RADIAN_VALUE);
		
		lightPosition[4] = sin_angle;
		lightPosition[6] = cos_angle;
		
		sin_angle = radius * sin(angle_green * RADIAN_VALUE);
		cos_angle = radius * cos(angle_green * RADIAN_VALUE);
		
		lightPosition[8] = sin_angle;
		lightPosition[9] = cos_angle;
		
		glUniform4fv(gLightPossitionUniform, 3, (const GLfloat*) lightPosition);
		glUniform3fv(gLightDiffusedUniform,  3 , (const GLfloat*) lightDiffuse);
		glUniform3fv(gLightAmbientUniform,   3, (const GLfloat*) lightAmbient);
		glUniform3fv(gLightSpecularUniform,  3, (const GLfloat*) lightSpecular);
		
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*) materialAmbient);
		glUniform1f(gMaterialShininessUniform, materialShininess);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*) materialSpecular);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*) materialDiffuse);
	}

	glUniform1i(gIsLightOnUniform, isLightOn);
	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*) modelMatrix);
	glUniformMatrix4fv(gViewUniform, 1, GL_FALSE, (const float*) viewMatrix);
	glUniformMatrix4fv(gPerspectiveUniform, 1, GL_FALSE, (const GLfloat *) gPerspectiveProjectionMatrix);

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	// stop using OpenGL program object
	glUseProgram(0);

	if (bAnimate == true)
	{
		angle_red = angle_red - 0.3;
		angle_blue = angle_blue + 0.3;
		angle_green = angle_green - 0.3;
		if (angle_red <= 0)
		{
			angle_red = 360;
		}
		if (angle_blue >= 360)
		{
			angle_blue = 1.5;
		}
		if (angle_green <= 0)
		{
			angle_green = 360;
		}
	}


	glXSwapBuffers(gpDisplay, gWindow);
}

void Uninitialize()
{
	// destroy vao
	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}

	// destroy vbo
	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}

	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}

	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}

	
	// deatch vertex shader from shader program object
	glDetachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);

	// detach fragment shader from shader program obejct
	glDetachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObjectPerVertex);

	gVertexShaderObjectPerVertex = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObjectPerVertex);
	gFragmentShaderObjectPerVertex = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObjectPerVertex);
	gShaderProgramObjectPerVertex = 0;

	// unlink shader program
	glUseProgram(0);

	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	if(currentGLXContext = gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}
