
typedef struct node
{
	vmath::mat4 matrix;
	struct node* prev;
} node;

node* head = NULL;