#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>


#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include <SOIL/SOIL.h>
#include "vmath.h"
#include "Sphere.h"
#define RADIAN_VALUE 3.14159/180

//namespace
using namespace std;

//global variable declarations
bool bFullScreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;
GLXContext gGLXContext; // HGLRC

using namespace vmath;

typedef GLXContext (*glxCreateContextAttribsARBProc)( Display *, GLXFBConfig, GLXContext, Bool, const int *);

glxCreateContextAttribsARBProc  glxCreateContextAttribsARB = NULL;

GLXFBConfig gGLXFBConfig;

enum
{
	HRH_ATTRIBUTE_POSITION = 0 ,
	HRH_ATTRIBUTE_COLOR ,
	HRH_ATTRIBUTE_NORMAL ,
	HRH_ATTRIBUTE_TEXTURE_0
};

GLuint gVertexShaderObjectPerFragment;
GLuint gFragmentShaderObjectPerFragment;
GLuint gShaderProgramObjectPerFragment;

GLuint gVertexShaderObjectPerVertex;
GLuint gFragmentShaderObjectPerVertex;
GLuint gShaderProgramObjectPerVertex;

GLuint gCurrentShaderProgram;

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint gModelUniform;
GLuint gViewUniform;
GLuint gPerspectiveUniform;

GLuint gLightAmbientUniform;
GLuint gLightSpecularUniform;
GLuint gLightDiffusedUniform;
GLuint gLightPossitionUniform;

GLuint gMaterialSpecularUniform;
GLuint gMaterialAmbientUniform;
GLuint gMaterialDiffuseUniform;
GLuint gMaterialShininessUniform;

GLuint gIsPerVertexShaderUniform;
GLuint gIsPerFragmentShaderUniform;

GLfloat sphere_vertices[1146];
GLfloat Sphere_normals[1146];
GLfloat Sphere_textrure[764];
unsigned short sphere_elements[2280];

int isLightPerVertexShader = 0, isLightPerFragmentShader = 0, isLightOn = 0;
int gNumVertices;
int gNumElements;

mat4 gOrthographicProjectionMatrix;
mat4 gPerspectiveProjectionMatrix;

GLfloat lightPosition[] = { 0.0f, 0.0f, 0.0f, 1.0f }; 
GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f }; 
GLfloat lightDefuse[] = { 1.0f, 1.0f, 1.0f  } ; 
GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f }; 

GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f };
GLfloat materialDefuse[] = { 1.0f, 1.0f, 1.0f  }; 
GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0f};
GLfloat materialShinyness = 128.0f;

int key_x = 0, key_y = 0, key_z = 0;
GLfloat angle_for_light = 0.0f;
GLfloat sin_angle = 0.0f, cos_angle = 0.0f, radius = 10;
// first column
// 1
GLfloat materialAmbient_emerald[] = { 0.0215f, 0.1745f, 0.0215f, 1.0f };
GLfloat materialDefuse_emerald[] = { 0.07568f, 0.61424f, 0.07568f, 1.0f };
GLfloat materialSpecular_emerald[] = { 0.633f, 0.727811f, 0.633f, 1.0f };
GLfloat materialShinyness_emerald = 0.6 * 128;

// 2
GLfloat materialAmbient_jade[] = { 0.135f, 0.2225f, 0.1575f, 1.0f };
GLfloat materialDefuse_jade[] = { 0.54f, 0.89f, 0.63f, 1.0f };
GLfloat materialSpecular_jade[] = { 0.316228f, 0.316228f, 0.316228f, 1.0f };
GLfloat materialShinyness_jade = 0.1 * 128;

// 3
GLfloat materialAmbient_obsidian[] = { 0.05375f, 0.05f, 0.06625f, 1.0f };
GLfloat materialDefuse_obsidian[] = { 0.18275f, 0.17f, 0.22525f, 1.0f };
GLfloat materialSpecular_obsidian[] = { 0.332741f, 0.328634f, 0.346435f, 1.0f };
GLfloat materialShinyness_obsidian = 0.3 * 128;

// 4 pearl
GLfloat materialAmbient_pearl[] = { 0.25f, 0.20725f, 0.20725f, 1.0f };
GLfloat materialDefuse_pearl[] = { 1.0f, 0.829f, 0.829f, 1.0f };
GLfloat materialSpecular_pearl[] = { 0.296648f, 0.296648f, 0.296648f, 1.0f };
GLfloat materialShinyness_pearl = 0.088 * 128;

// 5 ruby 
GLfloat materialAmbient_ruby[] = { 0.1745f, 0.01175f, 0.01175f, 1.0f };
GLfloat materialDefuse_ruby[] = { 0.61424f, 0.04136f, 0.04136f, 1.0f };
GLfloat materialSpecular_ruby[] = { 0.727811f, 0.626959f, 0.626959f, 1.0f };
GLfloat materialShinyness_ruby = 0.6 * 128;

// 6 turquoise
GLfloat materialAmbient_turquoise[] = { 0.1f, 0.18725f, 0.1745f, 1.0f };
GLfloat materialDefuse_turquoise[] = { 0.396f, 0.74151f, 0.69102f, 1.0f };
GLfloat materialSpecular_turquoise[] = { 0.297254f, 0.30829f, 0.306678f, 1.0f };
GLfloat materialShinyness_turquoise = 0.1 * 128;

// second column
// 7 brass 
GLfloat materialAmbient_brass[] = { 0.329412f, 0.223529f, 0.027451f, 1.0f };
GLfloat materialDefuse_brass[] = { 0.780392f, 0.568627f, 0.113725f, 1.0f };
GLfloat materialSpecular_brass[] = { 0.992157f, 0.941176f, 0.807843f, 1.0f };
GLfloat materialShinyness_brass = 0.21794872 * 128;

// 8 bronze
GLfloat materialAmbient_bronze[] = { 0.2125f, 0.1275f, 0.054f, 1.0f };
GLfloat materialDefuse_bronze[] = { 0.714f, 0.4284f, 0.18144f, 1.0f };
GLfloat materialSpecular_bronze[] = { 0.393548f, 0.271906f, 0.166721f, 1.0f };
GLfloat materialShinyness_bronze = 0.2 * 128;

// 9 chrome
GLfloat materialAmbient_chrome[] = { 0.25f, 0.25f, 0.25f, 1.0f };
GLfloat materialDefuse_chrome[] = { 0.4f, 0.4f, 0.4f, 1.0f };
GLfloat materialSpecular_chrome[] = { 0.774597f, 0.774597f, 0.774597f, 1.0f };
GLfloat materialShinyness_chrome = 0.2 * 128;

// 10 4th sphere on 2nd column, copper  
GLfloat materialAmbient_copper[] = { 0.19125f, 0.19125f, 0.0225f, 1.0f };
GLfloat materialDefuse_copper[] = { 0.7038f, 0.27048f, 0.0828f, 1.0f };
GLfloat materialSpecular_copper[] = { 0.256777f, 0.137622f, 0.086014f, 1.0f };
GLfloat materialShinyness_copper = 0.1 * 128;

// 11 5th sphere on 2nd column, gold
GLfloat materialAmbient_gold[] = { 0.24725f, 0.1995f, 0.0745f, 1.0f };
GLfloat materialDefuse_gold[] = { 0.75164f, 0.60648f, 0.22648f, 1.0f };
GLfloat materialSpecular_gold[] = { 0.628281f, 0.555802f, 0.366065f, 1.0f };
GLfloat materialShinyness_gold = 0.4 * 128;

// 12 6th sphere on 2nd column, silver
GLfloat materialAmbient_silver[] = { 0.19225, 0.1995f, 0.19225f, 1.0f };
GLfloat materialDefuse_silver[] = { 0.50754f, 0.50754f, 0.50754f, 1.0f };
GLfloat materialSpecular_silver[] = { 0.508273f, 0.508273f, 0.508273f, 1.0f };
GLfloat materialShinyness_silver = 0.4 * 128;

// 13 1st sphere on 3rd column, black 
GLfloat materialAmbient_black[] = { 0.0, 0.0f, 0.0f, 1.0f };
GLfloat materialDefuse_black[] = { 0.01f, 0.01f, 0.01f, 1.0f };
GLfloat materialSpecular_black[] = { 0.5f, 0.5f, 0.5f, 1.0f };
GLfloat materialShinyness_black = 0.25 * 128;

// 14 2nd sphere on 3rd column, cyan
GLfloat materialAmbient_cyan[] = { 0.0, 0.1f, 0.06f, 1.0f };
GLfloat materialDefuse_cyan[] = { 0.0f, 0.50980392f, 0.50980392f, 1.0f };
GLfloat materialSpecular_cyan[] = { 0.50196078f, 0.50196078f, 0.50196078f, 1.0f };
GLfloat materialShinyness_cyan = 0.25 * 128;

// 15 3rd sphere on 2nd column, green
GLfloat materialAmbient_green[] = { 0.0, 0.0f, 0.0f, 1.0f };
GLfloat materialDefuse_green[] = { 0.1f, 0.35f, 0.1f, 1.0f };
GLfloat materialSpecular_green[] = { 0.45f, 0.55f, 0.45f, 1.0f };
GLfloat materialShinyness_green = 0.25 * 128;

// 16 4th sphere on 3rd column, red
GLfloat materialAmbient_red[] = { 0.0, 0.0f, 0.0f, 1.0f };
GLfloat materialDefuse_red[] = { 0.5, 0.0f, 0.0f, 1.0f };
GLfloat materialSpecular_red[] = { 0.7, 0.6f, 0.6f, 1.0f };
GLfloat materialShinyness_red = 0.25 * 128;

// 17 5th sphere on 3rd column, white
GLfloat materialAmbient_white[] = { 0.0, 0.0f, 0.0f, 1.0f };
GLfloat materialDefuse_white[] = { 0.55, 0.55f, 0.55f, 1.0f };
GLfloat materialSpecular_white[] = { 0.7, 0.7f, 0.7f, 1.0f };
GLfloat materialShinyness_white = 0.25 * 128;

// 18 6th sphere on 3rd column, yellow plastic
GLfloat materialAmbient_plastic[] = { 0.0, 0.0f, 0.0f, 1.0f };
GLfloat materialDefuse_plastic[] = { 0.5, 0.5f, 0.0f, 1.0f };
GLfloat materialSpecular_plastic[] = { 0.60, 0.60f, 0.50f, 1.0f };
GLfloat materialShinyness_plastic = 0.25 * 128;

// 19  1st sphere on 4th column, black
GLfloat materialAmbient_black_2[] = { 0.02, 0.02f, 0.02f, 1.0f };
GLfloat materialDefuse_black_2[] = { 0.01, 0.01f, 0.01f, 1.0f };
GLfloat materialSpecular_black_2[] = { 0.4f, 0.4f, 0.4f, 1.0f };
GLfloat materialShinyness_black_2 = 0.078125 * 128;

// 20  2nd sphere on 4th column, cyan
GLfloat materialAmbient_cyan_2[] = { 0.0, 0.05f, 0.05f, 1.0f };
GLfloat materialDefuse_cyan_2[] = { 0.4, 0.5f, 0.5f, 1.0f };
GLfloat materialSpecular_cyan_2[] = { 0.04f, 0.7f, 0.7f, 1.0f };
GLfloat materialShinyness_cyan_2 = 0.078125 * 128;

// 21  3rd sphere on 4th column, green
GLfloat materialAmbient_green_2[] = { 0.0, 0.05f, 0.05f, 1.0f };
GLfloat materialDefuse_green_2[] = { 0.4, 0.5f, 0.4f, 1.0f };
GLfloat materialSpecular_green_2[] = { 0.04f, 0.7f, 0.04f, 1.0f };
GLfloat materialShinyness_green_2 = 0.078125 * 128;

// 22   4th sphere on 4th column, red 
GLfloat materialAmbient_red_2[] = { 0.05, 0.0f, 0.0f, 1.0f };
GLfloat materialDefuse_red_2[] = { 0.5, 0.4f, 0.4f, 1.0f };
GLfloat materialSpecular_red_2[] = { 0.7f, 0.04f, 0.04f, 1.0f };
GLfloat materialShinyness_red_2 = 0.078125 * 128;

// 23   5th sphere on 4th column, white
GLfloat materialAmbient_white_2[] = { 0.05, 0.05f, 0.05f, 1.0f };
GLfloat materialDefuse_white_2[] = { 0.5, 0.5f, 0.5f, 1.0f };
GLfloat materialSpecular_white_2[] = { 0.7f, 0.7f, 0.7f, 1.0f };
GLfloat materialShinyness_white_2 = 0.078125 * 128;

// 24   6th sphere on 4th column, yellow rubber
GLfloat materialAmbient_rubber[] = { 0.05, 0.05f, 0.0f, 1.0f };
GLfloat materialDefuse_rubber[] = { 0.5, 0.5f, 0.4f, 1.0f };
GLfloat materialSpecular_rubber[] = { 0.7f, 0.7f, 0.04f, 1.0f };
GLfloat materialShinyness_rubber = 0.078125 * 128;

GLfloat translate_x = 0.0f, translate_y = 2.0f, translate_z = 0.0f, diiference_y = 0.7;

//entry point function
int main(void)
{
	//function prototype
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void Initialize();
	void Resize(int,int);
	void Render();
	void Uninitialize();
	
	printf("\n In main()");	

	//variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	//code
	printf("\n Entering in createWindow()");
	CreateWindow();
	printf("\n Entering in Initialize()");
	Initialize();

	//Message Loop
	XEvent event;
	KeySym keysym;
	XKeyEvent xKeyEvent;
	char keys[26];

	while(1)
	{
	    while(XPending(gpDisplay))
	    {
		XNextEvent(gpDisplay, &event);
		switch(event.type)
		{
			case MapNotify:
				break;
			case KeyPress:
				keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0,0);
				switch(keysym)
				{
					case XK_Escape:
						Uninitialize();
						exit(0); //no need as we are checking in while condition
					default:
						break;
	
				}

				xKeyEvent.display = gpDisplay;
				xKeyEvent.window = gWindow;
				XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);

				switch(keys[0])
				{
					case 'l':
					case 'L':
						if (isLightOn == 1)
						{
							isLightOn = 0;
							isLightPerVertexShader = 0;
							isLightPerFragmentShader = 0;
							angle_for_light = 0.0f;
						}
						else
						{
							key_x = 0;
							key_y = 0;
							key_z = 0;
							isLightOn = 1;
						}
						break;
					case 'v':
					case 'V':
						if (isLightPerVertexShader == 1)
						{
							isLightPerVertexShader = 0;
							isLightOn = 0;
						}
						else
						{
							isLightPerVertexShader = 1;
							isLightPerFragmentShader = 0;
							isLightOn = 1;
							angle_for_light = 0.0f;
						}
						break;
					case 'f':
					case 'F':
						if (isLightPerFragmentShader == 0)
						{
							isLightPerFragmentShader = 1;
							isLightPerVertexShader = 0;
							isLightOn = 1;
							angle_for_light = 0.0f; 
						}
						else
						{
							isLightPerFragmentShader = 0;
							isLightOn = 0;
						}
						break;
					case 'q':
					case 'Q':
						ToggleFullScreen();
						break;
					case 'x':
					case 'X':
						key_x = 1;
						key_y = 0;
						key_z = 0;
						break;
					case 'y':
					case 'Y':
						key_x = 0;
						key_y = 1;
						key_z = 0;
						break;
					case 'z':
					case 'Z':
						key_x = 0;
						key_y = 0;
						key_z = 1;
						break;
				}
				break;
			case ButtonPress:
				switch(event.xbutton.button)
				{
					case 1:
						break;
					case 2:
						break;
					case 3:
						break;
					default:
						break;
				}
				break;
			case MotionNotify:
				break;
			case ConfigureNotify:
				winWidth=event.xconfigure.width;
				winHeight=event.xconfigure.height;
				Resize(winWidth, winHeight);
				break;
			case Expose:
				break;
			case DestroyNotify:
				break;
			case 33:
				Uninitialize();
				exit(0); //no need as we are checking in while condition
			default:
				break;
			
		} // switch
	    } // XPending Loop
	    Render();
	}
	Uninitialize();
	return 0;
}

void CreateWindow(void)
{

	//function declaration
	void Uninitialize();

	//variable declaration
	XSetWindowAttributes winAttribs;
	GLXFBConfig *pGLXFBConfig = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int numFbConfigs;
	int i;
	int styleMask;

	static int framebufferAttribes[] = 
				{
					GLX_X_RENDERABLE, True, 
					GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT, 
					GLX_RENDER_TYPE, GLX_RGBA_BIT, 
					GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR, 				
					GLX_RED_SIZE, 8, 
					GLX_GREEN_SIZE, 8, 
					GLX_BLUE_SIZE, 8, 
					GLX_ALPHA_SIZE, 8, 
					GLX_DEPTH_SIZE, 24, 
					GLX_STENCIL_SIZE, 8, 
					GLX_DOUBLEBUFFER, True, 
					None 
				};

	//code
	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("Error: Uable To Open X Display.\nExiting Now...\n ");
		Uninitialize();
		exit(1);
	}
	printf("\ngot gp display\n");
	pGLXFBConfig = glXChooseFBConfig(gpDisplay, XDefaultScreen(gpDisplay), framebufferAttribes, &numFbConfigs);

	if(pGLXFBConfig == NULL)
	{
		printf("\nError: Not get pGLXFBConfig\n");
		Uninitialize();
		exit(1);
		
	}

	int bestFrameBufferConfig = -1;
	int worstFrameBufferConfig = -1;
	int bestNumberOfSample = -1;
	int worstNumberOfSample = 99;

	printf("\nNumber of FB Config = %d " , numFbConfigs);

	for(int i =0; i< numFbConfigs; i++ )
	{
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfig[i]);
		if(pTempXVisualInfo != NULL)
		{
			int sampleBuffers, samples;
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLES, &samples);

			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNumberOfSample)
			{
				bestFrameBufferConfig = i;
				bestNumberOfSample = samples;
			} 
		
			if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < 99)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSample = samples;
			}
			
			printf("\ni = %d , sample = %d, sampleBuffers = %d", i, samples, sampleBuffers);
		} // If block
		XFree(pTempXVisualInfo);
	} // loop

	bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];
	gGLXFBConfig = bestGLXFBConfig;

	XFree(pGLXFBConfig);

	// get best vusualInfo now
	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, gGLXFBConfig);

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			gpXVisualInfo->visual,
			AllocNone);
	gColormap = winAttribs.colormap;
	winAttribs.background_pixel=BlackPixel(gpDisplay, XDefaultScreen(gpDisplay));
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask
				| KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	gWindow=XCreateWindow(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		0,
		0,
		giWindowWidth,
		giWindowHeight,
		0,
		gpXVisualInfo->depth,
		InputOutput,
		gpXVisualInfo->visual,
		styleMask,
		&winAttribs);
	if(!gWindow)
	{
		printf("Error: Failed To Create Main Window.\nExiting Now...\n");
		Uninitialize();	
		exit(1);
	}
	XStoreName(gpDisplay, gWindow, "Hrituja's First XWidow");
	Atom windowManagerDelete=XInternAtom(gpDisplay, "WM_DELETE_WINDOW", true);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	
	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen(void)
{
	//variable declaration
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	//code
	wm_state=XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(XEvent));
	
	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullScreen ? 0 : 1;
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN", false);
	xev.xclient.data.l[1]=fullscreen;
	
	XSendEvent(gpDisplay, 
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False,
		StructureNotifyMask,
		&xev);
}

void Initialize()
{
	// Function Declaration
	void Resize(int, int);
	GLuint LoadBitmapAsTexture(const char *);
	void getSphereVertexData(float spherePositionCoords[1146], float sphereNormalCoords[1146], float sphereTexCoords[764], unsigned short sphereElements[2280]);
	unsigned int getNumberOfSphereVertices(void);
	unsigned int getNumberOfSphereElements(void);
	void Uninitialize();
	
	//gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);

	glxCreateContextAttribsARB = (glxCreateContextAttribsARBProc) glXGetProcAddressARB( (GLubyte*) "glXCreateContextAttribsARB");	
	
	const int attribes[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
			GLX_CONTEXT_MINOR_VERSION_ARB, 6,
			GLX_CONTEXT_PROFILE_MASK_ARB,
			GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
			None
		};

	gGLXContext = glxCreateContextAttribsARB( gpDisplay, gGLXFBConfig, 0, True, attribes); // richest context
	if(!gGLXContext)
	{
		printf("\nDidn't get gGLXContext\n");
		const int attribes_1[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
			GLX_CONTEXT_MINOR_VERSION_ARB, 0,
			GLX_CONTEXT_PROFILE_MASK_ARB,
			GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
			None
		};

		gGLXContext = glxCreateContextAttribsARB( gpDisplay, gGLXFBConfig, 0, True, attribes_1); // richest context
	}
	printf("\nGot gGLXContext\n");

	Bool isDirectContext = glXIsDirect(gpDisplay, gGLXContext);
	if(isDirectContext == True)
	{
		printf("\nH/w Rendering support\n");
	}


	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		Uninitialize();
		exit(1);
	}

	//OpenGL Logs
	printf("OpenGL Logs");
	printf("OpenGL Vendor: %s \n", glGetString(GL_VENDOR));
	printf("OpenGL Renderer: %s \n", glGetString(GL_RENDERER));
	printf("OpenGL Version : %s \n", glGetString(GL_VERSION));
	printf("OpenGL Shading Language : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	printf("Open Enabled extension\n");
	GLint numExtension = 0;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	printf("Open Enabled total extension %d \n", numExtension);

	/*for (int i = 0; i < numExtension; i++)
	{		
		printf("%s\t", glGetStringi(GL_EXTENSIONS,i));
	}*/

		/***************************************PER VERTEX SHADER*******************************************************************/
	gVertexShaderObjectPerVertex = glCreateShader(GL_VERTEX_SHADER);

	//provide souce code to shader
	const GLchar* vertexShaderSourceCodePerVertex = "#version 450 core\n"  \
		"in vec4 v_position;" \
		"in vec3 v_normals; " \
		"uniform mat4 u_modelMatrix;" \
		"uniform mat4 u_viewMatrix;" \
		"uniform mat4 u_projectionMatrix;" \
		"uniform int u_isPerVertexLightOn;" \
		"uniform vec3 u_lightAmbient;" \
		"uniform vec3 u_lightSpecular;" \
		"uniform vec3 u_lightDiffuse;" \
		"uniform vec4 u_light_position; " \
		"uniform vec3 u_materialAmbient; " \
		"uniform vec3 u_materialSpecular; "\
		"uniform vec3 u_materialDiffuse; "\
		"uniform float u_materialshininess; "\
		"out vec3 phoung_ads_lighting;" \
		"void main()" \
		"{" \
		"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" \
		"if (u_isPerVertexLightOn == 1) " \
		"{" \
		"vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" \
		"vec3 transformed_normal = normalize( mat3( u_viewMatrix * u_modelMatrix ) * v_normals); " \
		"vec3 light_direction = normalize( vec3 (u_light_position - eye_coordinates) ); " \
		"vec3 reflection_vector = reflect (-light_direction, transformed_normal); " \
		"vec3 view_vector = normalize ( vec3 ( -eye_coordinates)); " \
		"vec3 ambient = u_lightAmbient * u_materialAmbient; " \
		"vec3 diffuse_light = u_lightDiffuse * u_materialDiffuse * max ( dot (light_direction , transformed_normal), 0.0); "\
		"vec3 specular = u_lightSpecular * u_materialSpecular * pow( max ( dot ( reflection_vector , view_vector), 0.0) , u_materialshininess);" \
		"phoung_ads_lighting = ambient + diffuse_light + specular;" \
		"}" \
		"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" \
		"}";

	glShaderSource(gVertexShaderObjectPerVertex, 1, (const GLchar**)&vertexShaderSourceCodePerVertex, NULL);

	// compile shader
	glCompileShader(gVertexShaderObjectPerVertex);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
				printf("Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				Uninitialize();
				exit(1);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObjectPerVertex = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCodePerVertex = "#version 450 core\n" \
		"in vec3 phoung_ads_lighting;" \
		"out vec4 FragColor; " \
		"void main()" \
		"{"\
		"FragColor = vec4(phoung_ads_lighting, 1.0); " \
		"}";



	glShaderSource(gFragmentShaderObjectPerVertex, 1, (const char**)&fragmentShaderSourceCodePerVertex, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObjectPerVertex);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
				printf("Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				Uninitialize();
				exit(1);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObjectPerVertex = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);

	// pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(gShaderProgramObjectPerVertex, HRH_ATTRIBUTE_POSITION, "v_position");
	glBindAttribLocation(gShaderProgramObjectPerVertex, HRH_ATTRIBUTE_NORMAL, "v_normals");

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObjectPerVertex);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObjectPerVertex, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectPerVertex, iInfoLogLength, &written, szInfoLog);
				printf("Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				Uninitialize();
				exit(1);
			}
		}
	}

	/**********************************************************************************************************************************************************/

	/**************************************PER FRAGMENT SHADER******************************************************************/
	/********** VERTEX SHADER ***********/
	// CREATE SHADER

	gVertexShaderObjectPerFragment = glCreateShader(GL_VERTEX_SHADER);
	
	//provide souce code to shader
	const GLchar* vertexShaderSourceCodePerFragment =  "#version 450 core\n"  \
	"in vec4 v_position;" \
	"in vec3 v_normals; " \
	"uniform mat4 u_modelMatrix;" \
	"uniform mat4 u_viewMatrix;" \
	"uniform mat4 u_projectionMatrix;" \
	"uniform int u_isPerFragmentLightOn;" \
	"uniform vec4 u_light_position; " \
	"out vec3 light_direction; "\
	"out vec3 transformed_normal;"\
	"out vec3 view_vector;"\
	"void main()" \
	"{" \
	"if (u_isPerFragmentLightOn == 1) " \
	"{" \
	"vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" \
	"transformed_normal =  mat3( u_viewMatrix * u_modelMatrix ) * v_normals;" \
	"light_direction = vec3 (u_light_position - eye_coordinates);" \
	"view_vector =  vec3 ( -eye_coordinates);" \
	"}" \
	"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" \
	"}";

	glShaderSource(gVertexShaderObjectPerFragment, 1, (const GLchar**)&vertexShaderSourceCodePerFragment, NULL);

	// compile shader
	glCompileShader(gVertexShaderObjectPerFragment);

	// check compilation error
	iInfoLogLength = 0;
	iShaderCompileStatus = 0;
	szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
				printf("Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				Uninitialize();
				exit(1);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObjectPerFragment = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCodePerFragment = "#version 450 core\n" \
		"vec3 phoung_ads_lighting;" \
		"out vec4 FragColor; " \
		"in vec3 light_direction; "\
		"in vec3 transformed_normal;"\
		"in vec3 view_vector;"\
		"vec3 normalized_light_direction; "\
		"vec3 normalized_transformed_normal;"\
		"vec3 normalized_view_vector;"\
		"uniform vec3 u_lightAmbient;" \
		"uniform vec3 u_lightSpecular;" \
		"uniform vec3 u_lightDiffuse;" \
		"uniform vec3 u_materialAmbient; " \
		"uniform vec3 u_materialSpecular; "\
		"uniform vec3 u_materialDiffuse; "\
		"uniform float u_materialshininess; "\
		"uniform int u_isPerFragmentLightOn;" \
		"void main()" \
		"{"\
		"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" \
		"if (u_isPerFragmentLightOn == 1) " \
		"{" \
		"normalized_light_direction = normalize(light_direction);"\
		"normalized_transformed_normal = normalize(transformed_normal);"\
		"normalized_view_vector = normalize(view_vector);"\
		"vec3 reflection_vector = reflect (-normalized_light_direction, normalized_transformed_normal); " \
		"vec3 ambient = u_lightAmbient * u_materialAmbient; " \
		"vec3 diffuse_light = u_lightDiffuse * u_materialDiffuse * max ( dot (normalized_light_direction , normalized_transformed_normal), 0.0); "\
		"vec3 specular = u_lightSpecular * u_materialSpecular * pow( max ( dot ( reflection_vector , normalized_view_vector), 0.0) , u_materialshininess);" \
		"phoung_ads_lighting = ambient + diffuse_light + specular;" \
		"}"\
		"FragColor = vec4(phoung_ads_lighting, 1.0); " \
		"}";

	glShaderSource(gFragmentShaderObjectPerFragment, 1, (const char**)&fragmentShaderSourceCodePerFragment, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObjectPerFragment);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
				printf("Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				Uninitialize();
				exit(1);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObjectPerFragment = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);

	// pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(gShaderProgramObjectPerFragment, HRH_ATTRIBUTE_POSITION, "v_position");
	glBindAttribLocation(gShaderProgramObjectPerFragment, HRH_ATTRIBUTE_NORMAL, "v_normals");

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObjectPerFragment);

	// CHECK LINKING ERROR
	iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObjectPerFragment, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectPerFragment, iInfoLogLength, &written, szInfoLog);
				printf("Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				Uninitialize();
				exit(1);
			}
		}
	}
	
	/**********************************************************************************************************************************************************/

	getSphereVertexData(sphere_vertices, Sphere_normals, Sphere_textrure, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// ***** vertex, colors, shadersatribs , vbo, vao initialization *********
	
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo

	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Sphere_normals), Sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0); // end for Vao

	/***************************************************************************************************************/

	// set-up depth buffers
	glClearColor(0.20f, 0.20f, 0.20f, 1.0f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); 

	// we will always cull face for better performance
	glEnable(GL_CULL_FACE);

	gPerspectiveProjectionMatrix = mat4::identity();

	Resize(giWindowWidth, giWindowHeight);
}


void Resize(int width, int height) {
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) height);

	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); // no 0.1f

}

void Render()
{
	// CODE
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	GLfloat scaleSphere = 0.5f, sphere_translate_x = 0.0f, sphere_translate_y = 2.0f, sphere_translate_z = 0.0f , translate_z = 8.0f, diiference_y = 0.7;

	// START USING OpenGL program object
	gCurrentShaderProgram = (isLightPerVertexShader == 1) ? gShaderProgramObjectPerVertex : gShaderProgramObjectPerFragment;

	glUseProgram(gCurrentShaderProgram);

	mat4 temp_pos;

	mat4 viewMatrix = vmath::lookat(
		vmath::vec3(0.0f, 0.0f, translate_z),
		vmath::vec3(0.0f, 0.0f, 0.0f),
		vmath::vec3(0.0f, 1.0f, 0.0f)
	);

	// OpenGL Drawing
	// set modelview & modelviewProjection matrices to indentity
	mat4 translateMatrix = vmath::translate(sphere_translate_x, sphere_translate_y, sphere_translate_z);
	mat4 scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
	mat4 modelMatrix = translateMatrix * scaleMatrix;

	gModelUniform = glGetUniformLocation(gCurrentShaderProgram, "u_modelMatrix");
	gViewUniform = glGetUniformLocation(gCurrentShaderProgram, "u_viewMatrix");
	gPerspectiveUniform = glGetUniformLocation(gCurrentShaderProgram, "u_projectionMatrix");
	gIsPerFragmentShaderUniform = glGetUniformLocation(gCurrentShaderProgram, "u_isPerFragmentLightOn");
	gIsPerVertexShaderUniform = glGetUniformLocation(gCurrentShaderProgram, "u_isPerVertexLightOn");

	gLightAmbientUniform = glGetUniformLocation(gCurrentShaderProgram, "u_lightAmbient");
	gLightSpecularUniform = glGetUniformLocation(gCurrentShaderProgram, "u_lightSpecular");
	gLightDiffusedUniform = glGetUniformLocation(gCurrentShaderProgram, "u_lightDiffuse");
	gLightPossitionUniform = glGetUniformLocation(gCurrentShaderProgram, "u_light_position");

	gMaterialDiffuseUniform = glGetUniformLocation(gCurrentShaderProgram, "u_materialDiffuse");
	gMaterialAmbientUniform = glGetUniformLocation(gCurrentShaderProgram, "u_materialAmbient");
	gMaterialSpecularUniform = glGetUniformLocation(gCurrentShaderProgram, "u_materialSpecular");
	gMaterialShininessUniform = glGetUniformLocation(gCurrentShaderProgram, "u_materialshininess");

	sin_angle = radius * sin(angle_for_light * RADIAN_VALUE);
	cos_angle = radius * cos(angle_for_light * RADIAN_VALUE);

	if (key_x == 1)
	{
		lightPosition[0] = 0.0f;
		lightPosition[1] = sin_angle;
		lightPosition[2] = -translate_z + cos_angle;
		lightPosition[3] = 1.0f;
	} else if (key_y == 1)
	{
		lightPosition[0] = sin_angle;
		lightPosition[1] = 0.0f;
		lightPosition[2] = -translate_z + cos_angle;
		lightPosition[3] = 1.0f;
	} else if (key_z == 1)
	{
		lightPosition[0] = sin_angle;
		lightPosition[1] = cos_angle;
		lightPosition[2] = -translate_z;
		lightPosition[3] = 1.0f;
	}

	if (key_x == 1 || key_y == 1 || key_z == 1)
	{
		if (angle_for_light >= 360.0f)
		{
			angle_for_light = 0.0f;
		}
		angle_for_light = angle_for_light + 0.5f;
	}

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform4fv(gLightPossitionUniform, 1, (const GLfloat*)lightPosition);
		glUniform3fv(gLightDiffusedUniform, 1, (const GLfloat*)lightDefuse);
		glUniform3fv(gLightAmbientUniform, 1, (const GLfloat*)lightAmbient);
		glUniform3fv(gLightSpecularUniform, 1, (const GLfloat*)lightSpecular);
	}
	glUniform1i(gIsPerFragmentShaderUniform, isLightPerFragmentShader);
	glUniform1i(gIsPerVertexShaderUniform, isLightPerVertexShader);
	glUniformMatrix4fv(gViewUniform, 1, GL_FALSE, (const float*)viewMatrix);
	//glUniformMatrix4fv(gPerspectiveUniform, 1, GL_FALSE, (const GLfloat*)gOrthographicProjectionMatrix);
	glUniformMatrix4fv(gPerspectiveUniform, 1, GL_FALSE, (const GLfloat*)gPerspectiveProjectionMatrix);
	
	/********************************************First Row**************************************************************/
	// first sphere
	translateMatrix = vmath::translate(sphere_translate_x - 1.5f, sphere_translate_y, sphere_translate_z);
	scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
	modelMatrix = translateMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient_emerald);
		glUniform1f(gMaterialShininessUniform, materialShinyness_emerald);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular_emerald);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDefuse_emerald);
	}

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0); 
	// ** unbind vao **
	glBindVertexArray(0);

	// second sphere
	translateMatrix = vmath::translate(sphere_translate_x - 0.5f, sphere_translate_y, sphere_translate_z);
	scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
	modelMatrix = translateMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient_jade);
		glUniform1f(gMaterialShininessUniform, materialShinyness_jade);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular_jade);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDefuse_jade);
	}

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	// ** unbind vao **
	glBindVertexArray(0);

	// third sphere
	translateMatrix = vmath::translate(sphere_translate_x + 0.5f, sphere_translate_y, sphere_translate_z);
	scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
	modelMatrix = translateMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient_obsidian);
		glUniform1f(gMaterialShininessUniform, materialShinyness_obsidian);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular_obsidian);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDefuse_obsidian);
	}

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	// ** unbind vao **
	glBindVertexArray(0);


	// forth sphere
	translateMatrix = vmath::translate(sphere_translate_x + 1.5f, sphere_translate_y, sphere_translate_z);
	scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
	modelMatrix = translateMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient_pearl);
		glUniform1f(gMaterialShininessUniform, materialShinyness_pearl);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular_pearl);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDefuse_pearl);
	}

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	// ** unbind vao **
	glBindVertexArray(0);

	/********************************************Second Row**************************************************************/

	// first sphere
	translateMatrix = vmath::translate(sphere_translate_x - 1.5f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
	modelMatrix = translateMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient_ruby);
		glUniform1f(gMaterialShininessUniform, materialShinyness_ruby);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular_ruby);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDefuse_ruby);
	}

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	// ** unbind vao **
	glBindVertexArray(0);

	// second sphere
	translateMatrix = vmath::translate(sphere_translate_x - 0.5f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
	modelMatrix = translateMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient_turquoise);
		glUniform1f(gMaterialShininessUniform, materialShinyness_turquoise);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular_turquoise);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDefuse_turquoise);
	}

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	// ** unbind vao **
	glBindVertexArray(0);

	// third sphere
	translateMatrix = vmath::translate(sphere_translate_x + 0.5f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
	modelMatrix = translateMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient_brass);
		glUniform1f(gMaterialShininessUniform, materialShinyness_brass);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular_brass);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDefuse_brass);
	}

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	// ** unbind vao **
	glBindVertexArray(0);


	// forth sphere
	translateMatrix = vmath::translate(sphere_translate_x + 1.5f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
	modelMatrix = translateMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient_bronze);
		glUniform1f(gMaterialShininessUniform, materialShinyness_bronze);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular_bronze);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDefuse_bronze);
	}

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	// ** unbind vao **
	glBindVertexArray(0);


	/********************************************THIRD ROW**************************************************************/
	diiference_y = diiference_y + 0.7f;
	// first sphere
	translateMatrix = vmath::translate(sphere_translate_x - 1.5f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
	modelMatrix = translateMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient_chrome);
		glUniform1f(gMaterialShininessUniform, materialShinyness_chrome);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular_chrome);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDefuse_chrome);
	}

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	// ** unbind vao **
	glBindVertexArray(0);

	// second sphere
	translateMatrix = vmath::translate(sphere_translate_x - 0.5f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
	modelMatrix = translateMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient_copper);
		glUniform1f(gMaterialShininessUniform, materialShinyness_copper);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular_copper);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDefuse_copper);
	}

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	// ** unbind vao **
	glBindVertexArray(0);

	// third sphere
	translateMatrix = vmath::translate(sphere_translate_x + 0.5f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
	modelMatrix = translateMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient_gold);
		glUniform1f(gMaterialShininessUniform, materialShinyness_gold);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular_gold);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDefuse_gold);
	}

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	// ** unbind vao **
	glBindVertexArray(0);


	// forth sphere
	translateMatrix = vmath::translate(sphere_translate_x + 1.5f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
	modelMatrix = translateMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient_silver);
		glUniform1f(gMaterialShininessUniform, materialShinyness_silver);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular_silver);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDefuse_silver);
	}

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	// ** unbind vao **
	glBindVertexArray(0);

	/********************************************FORTH ROW**************************************************************/
	diiference_y = diiference_y + 0.7f;
	// first sphere
	translateMatrix = vmath::translate(sphere_translate_x - 1.5f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
	modelMatrix = translateMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient_black);
		glUniform1f(gMaterialShininessUniform, materialShinyness_black);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular_black);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDefuse_black);
	}

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	// ** unbind vao **
	glBindVertexArray(0);

	// second sphere
	translateMatrix = vmath::translate(sphere_translate_x - 0.5f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
	modelMatrix = translateMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient_cyan);
		glUniform1f(gMaterialShininessUniform, materialShinyness_cyan);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular_cyan);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDefuse_cyan);
	}

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	// ** unbind vao **
	glBindVertexArray(0);

	// third sphere
	translateMatrix = vmath::translate(sphere_translate_x + 0.5f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
	modelMatrix = translateMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient_green);
		glUniform1f(gMaterialShininessUniform, materialShinyness_green);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular_green);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDefuse_green);
	}

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	// ** unbind vao **
	glBindVertexArray(0);


	// forth sphere
	translateMatrix = vmath::translate(sphere_translate_x + 1.5f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
	modelMatrix = translateMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient_red);
		glUniform1f(gMaterialShininessUniform, materialShinyness_red);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular_red);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDefuse_red);
	}

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	// ** unbind vao **
	glBindVertexArray(0);


	/********************************************FIFTH ROW**************************************************************/
	diiference_y = diiference_y + 0.7f;
	// first sphere
	translateMatrix = vmath::translate(sphere_translate_x - 1.5f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
	modelMatrix = translateMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient_white);
		glUniform1f(gMaterialShininessUniform, materialShinyness_white);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular_white);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDefuse_white);
	}

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	// ** unbind vao **
	glBindVertexArray(0);

	// second sphere
	translateMatrix = vmath::translate(sphere_translate_x - 0.5f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
	modelMatrix = translateMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient_plastic);
		glUniform1f(gMaterialShininessUniform, materialShinyness_plastic);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular_plastic);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDefuse_plastic);
	}

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	// ** unbind vao **
	glBindVertexArray(0);

	// third sphere
	translateMatrix = vmath::translate(sphere_translate_x + 0.5f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
	modelMatrix = translateMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient_black_2);
		glUniform1f(gMaterialShininessUniform, materialShinyness_black_2);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular_black_2);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDefuse_black_2);
	}

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	// ** unbind vao **
	glBindVertexArray(0);


	// forth sphere
	translateMatrix = vmath::translate(sphere_translate_x + 1.5f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
	modelMatrix = translateMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient_cyan_2);
		glUniform1f(gMaterialShininessUniform, materialShinyness_cyan_2);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular_cyan_2);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDefuse_cyan_2);
	}

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	// ** unbind vao **
	glBindVertexArray(0);

	/********************************************SIXTH ROW**************************************************************/
	diiference_y = diiference_y + 0.7f;
	// first sphere
	translateMatrix = vmath::translate(sphere_translate_x - 1.5f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
	modelMatrix = translateMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient_green_2);
		glUniform1f(gMaterialShininessUniform, materialShinyness_green_2);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular_green_2);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDefuse_green_2);
	}

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	// ** unbind vao **
	glBindVertexArray(0);

	// second sphere
	translateMatrix = vmath::translate(sphere_translate_x - 0.5f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
	modelMatrix = translateMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient_red_2);
		glUniform1f(gMaterialShininessUniform, materialShinyness_red_2);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular_red_2);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDefuse_red_2);
	}

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	// ** unbind vao **
	glBindVertexArray(0);

	// third sphere
	translateMatrix = vmath::translate(sphere_translate_x + 0.5f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
	modelMatrix = translateMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient_white_2);
		glUniform1f(gMaterialShininessUniform, materialShinyness_white_2);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular_white_2);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDefuse_white_2);
	}

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	// ** unbind vao **
	glBindVertexArray(0);


	// forth sphere
	translateMatrix = vmath::translate(sphere_translate_x + 1.5f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
	modelMatrix = translateMatrix * scaleMatrix;

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient_rubber);
		glUniform1f(gMaterialShininessUniform, materialShinyness_rubber);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular_rubber);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDefuse_rubber);
	}

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	// ** unbind vao **
	glBindVertexArray(0);

	// stop using OpenGL program object
	glUseProgram(0);

	glXSwapBuffers(gpDisplay, gWindow);
}

void Uninitialize()
{
	// destroy vao
	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}

	// destroy vbo
	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}

	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}

	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}

	// detach fragment shader from shader program obejct
	glDetachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObjectPerFragment);

	gVertexShaderObjectPerFragment = 0;	

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObjectPerFragment);
	gFragmentShaderObjectPerFragment = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObjectPerFragment);
	gShaderProgramObjectPerFragment = 0;


	glDetachShader(gShaderProgramObjectPerVertex, gShaderProgramObjectPerVertex);
	glDetachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);

	glDeleteShader(gVertexShaderObjectPerVertex);
	gVertexShaderObjectPerVertex = 0;

	glDeleteShader(gFragmentShaderObjectPerVertex);
	gFragmentShaderObjectPerVertex = 0;

	// unlink shader program
	glUseProgram(0);

	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	if(currentGLXContext = gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}
