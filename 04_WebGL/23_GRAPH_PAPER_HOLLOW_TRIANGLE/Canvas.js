// JavaScript source code
var canvas = null;
var gl;
var canvas_original_width = 0, canvas_original_height = 0;
var bFullScreen = false;
var requestAnimationFrame = window.requestAnimationFrame    || 
							window.webkitAnimationFrame     ||
							window.mozRequestAnimationFrame ||
							window.opRequestAnimationFrame  ||
							window.msRequestAnimationFrame	 ;

const webGLMacros =
{
	HRH_ATTRIBUTE_POSITION : 0 ,
    HRH_ATTRIBUTE_COLOR : 1 ,
    HRH_ATTRIBUTE_NORMAL : 2 ,
	HRH_ATTRIBUTE_TEXTURE_0 : 3 ,
}

var gVertexShaderObject;
var gFragmentShaderObject;
var gShaderProgramObject;

var gVao;
var gVbo_position;
var gVbo_color;
var gMVPUniform;

var gPerspectiveProjectionMatrix;

var color = new Float32Array([ 0.0, 0.0, 1.0, 0.0, 0.0, 1.0 ]);
var vertices = new Float32Array([ 1.0, 0.0, 0.0, -1.0, 0.0, 0.0 ]);
var tringleColor = new Float32Array ([ 1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0, 0.0 ]);

function main()
{
	// get canvas from dom
    canvas = document.getElementById("hrh"); // type inferance

    if (!canvas) 
	{
        console.log("canvas failed");
    }
    else
    {
        console.log("Got canvas");
    }

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
    
	// window in buil variable , window is inherited from DOM object
	// 3rd parameter false means do bubbling
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);	
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize(); // explicit warm up resize because webgl don't call resize
	
	draw();
	
}

function keyDown(event)
{
	switch(event.keyCode)
	{
		// for 'f' or 'F'
		case 70:
			toggleFullScreen();
			break;
		case 27:
			uninitialize();
			window.close(); // may not work in firefox but will work in chrome and safari
			break;
	}
}

function mouseDown()
{
	
}

function toggleFullScreen()
{
	var fullscreen_element = 	document.fullscreenElement || 
								document.webkitFulllscreenElement || 
								document.mozFullScreen || 
								document.msFullscreenElement ||
								null;
								
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
		{
			canvas.requestFullscreen();			
		} 
		else if(canvas.webkitRequestFulllscreen())
		{
			canvas.webkitRequestFulllscreen();
		}
		else if(canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		} 
		else if(canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		} 
		else if ( document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		} 
		else if (document.mozCancelFullScreen)
		{
			document.mozCancelFullScreen();
		} 
		else if (document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		bFullScreen = false;
	}	
}

function init()
{
	
    // get drawing context from the canvas
    gl = canvas.getContext("webgl2");

    if (!gl) 
	{
        console.log("gl failed");
    }
    else 
	{
        console.log("Got gl");
    }
	
	gVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	
	var vertexShaderSourceCode = 
								"#version 300 es \n" +
								"in vec4 vPosition;" +
								"in vec4 vColor;" +
								"out vec4 outColor;"+
								"uniform mat4 u_mvpMatrix;" +
								"void main()" +
								"{" +
								"gl_Position = u_mvpMatrix * vPosition;" +
								"outColor = vColor;" +
								"}";

	gl.shaderSource(gVertexShaderObject, vertexShaderSourceCode);
	
	gl.compileShader(gVertexShaderObject);
	
	if(gl.getShaderParameter(gVertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
	var fragmentShaderSourceCode = 	"#version 300 es\n"+
									"precision highp float;" +
									"in vec4 outColor;" +
									"out vec4 FragColor; " +
									"void main()"+
									"{" +
									"FragColor = outColor;" +
									"}";
	
	gl.shaderSource(gFragmentShaderObject, fragmentShaderSourceCode);
	
	gl.compileShader(gFragmentShaderObject);
	
	if(gl.getShaderParameter(gFragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gShaderProgramObject = gl.createProgram();
	
	// attach vertex shader to shader program
	gl.attachShader(gShaderProgramObject, gVertexShaderObject);
	
	// attch fragment shader to shader program
	gl.attachShader(gShaderProgramObject, gFragmentShaderObject);
	
	// pri-link binding of shader object with vertex shader possition attribute
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_COLOR, "vColor");

	// LINK PROGRAM
	gl.linkProgram(gShaderProgramObject);
	
	if(!gl.getProgramParameter(gShaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gMVPUniform = gl.getUniformLocation(gShaderProgramObject, "u_mvpMatrix");
	
	gVao = gl.createVertexArray();
	gl.bindVertexArray(gVao);
	
	gVbo_position = gl.createBuffer()
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_position);
	gl.bufferData(gl.ARRAY_BUFFER, null, gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_POSITION);
		
	gVbo_color = gl.createBuffer()
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_color);
	gl.bufferData(gl.ARRAY_BUFFER, null, gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_COLOR);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, null);	
	gl.bindVertexArray(null);	
	
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	gPerspectiveProjectionMatrix = mat4.create();
	
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	// ser the viewport to match	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(gPerspectiveProjectionMatrix, 
						45.0, 
						parseFloat(canvas.width) /parseFloat(canvas.height), 
						parseFloat(canvas.height) / parseFloat(canvas.width), 
						100.0); // no 0.1f
	
}

function draw()
{
	// code
	gl.clear(gl.COLOR_BUFFER_BIT);
	
	gl.useProgram(gShaderProgramObject);
	
	var modelViewMatrix = mat4.create();
	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -1.0]);
	var modelViewProjectionMatrix = mat4.create();
	
	mat4.multiply(modelViewProjectionMatrix, gPerspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMVPUniform, false, modelViewProjectionMatrix);
	
	graph();

	hallowTriangle();

	gl.useProgram(null);
	
	requestAnimationFrame(draw, canvas);
}

function hallowTriangle()
{
	var triangle = new Float32Array ([ 0.0, 0.2, 0.0, -0.2, -0.2, 0.0, 0.2, -0.2, 0.0, 0.0, 0.2, 0.0 ]);

	// CODE
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_color);
	gl.bufferData(gl.ARRAY_BUFFER, tringleColor, gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_position);
	gl.bufferData(gl.ARRAY_BUFFER, triangle, gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(gVao);
	gl.drawArrays(gl.LINES, 0, 2);
	gl.drawArrays(gl.LINES, 1, 2);
	gl.drawArrays(gl.LINES, 2, 2);
	gl.bindVertexArray(null);
}

function graph()
{
	// variabl declaration
	var distanceForX = 0, distanceForY = 0;
	
	// code
	
	color[0] = 0.0;
	color[1] = 0.0;
	color[2] = 1.0;
	color[3] = 0.0;
	color[4] = 0.0;
	color[5] = 1.0;
	
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_color);
	gl.bufferData(gl.ARRAY_BUFFER, color, gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	for (var i = 0; i < 30; i++) {

		// verticle up
		vertices[0] = 1.5;
		vertices[1] = distanceForX;
		vertices[2] = 0.0;

		vertices[3] = -1.5;
		vertices[4] = distanceForX;
		vertices[5] = 0.0;

		gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_position);
		gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.DYNAMIC_DRAW);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

		gl.bindVertexArray(gVao);
		gl.drawArrays(gl.LINES, 0, 2);
		gl.bindVertexArray(null);

		// verticle down
		vertices[0] = 1.5;
		vertices[1] = -distanceForX;
		vertices[2] = 0.0;

		vertices[3] = -1.5;
		vertices[4] = -distanceForX;
		vertices[5] = 0.0;

		gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_position);
		gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.DYNAMIC_DRAW);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

		gl.bindVertexArray(gVao);
		gl.drawArrays(gl.LINES, 0, 2);
		gl.bindVertexArray(null);

		// horizontal right
		vertices[0] = -distanceForY;
		vertices[1] = 1.5;
		vertices[2] = 0.0;

		vertices[3] = -distanceForY;
		vertices[4] = -1.5;
		vertices[5] = 0.0;

		gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_position);
		gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.DYNAMIC_DRAW);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

		gl.bindVertexArray(gVao);
		gl.drawArrays(gl.LINES, 0, 2);
		gl.bindVertexArray(null);

		// horizontal left
		vertices[0] = distanceForY;
		vertices[1] = 1.5;
		vertices[2] = 0.0;

		vertices[3] = distanceForY;
		vertices[4] = -1.5;
		vertices[5] = 0.0;

		gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_position);
		gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.DYNAMIC_DRAW);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

		gl.bindVertexArray(gVao);
		gl.drawArrays(gl.LINES, 0, 2);
		gl.bindVertexArray(null);


		distanceForX += 1.0 / 46;
		distanceForY += 1.0 / 25;
	}

	color[0] = 0.0;
	color[1] = 1.0;
	color[2] = 0.0;
	color[3] = 0.0;
	color[4] = 1.0;
	color[5] = 0.0;

	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_color);
	gl.bufferData(gl.ARRAY_BUFFER, color, gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	// horizontal green line
	vertices[0] = 1.5;
	vertices[1] = 0.0;
	vertices[2] = 0.0;

	vertices[3] = -1.5;
	vertices[4] =  0.0;
	vertices[5] =  0.0;

	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_position);
	gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(gVao);
	gl.drawArrays(gl.LINES, 0, 2);
	gl.bindVertexArray(null);

	// verticle green line
	vertices[0] = 0.0;
	vertices[1] = 1.5;
	vertices[2] = 0.0;

	vertices[3] =  0.0;
	vertices[4] = -1.5;
	vertices[5] =  0.0;

	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_position);
	gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(gVao);
	gl.drawArrays(gl.LINES, 0, 2);
	gl.bindVertexArray(null);

	gl.bindVertexArray(null);
	
}

function uninitialize()
{
	if(gVao)
	{
		gl.deleteVertexArray(gVao);
		gVao = null;
	}
	
	if(gVbo_position)
	{
		gl.deleteBuffer(gVbo_position);
		gVbo_position = null;
	}
	
	if(gShaderProgramObject)
	{
		if(gFragmentShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gFragmentShaderObject);
			gl.deleteShader(gFragmentShaderObject);
			gFragmentShaderObject = null;
		}
		
		if(gVertexShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gVertexShaderObject);
			gl.deleteShader(gVertexShaderObject);
			gVertexShaderObject = null;
		}
		
		gl.deleteProgram(gShaderProgramObject);
		gShaderProgramObject = null;
	}
}
