// JavaScript source code
var canvas = null;
var gl;
var canvas_original_width = 0, canvas_original_height = 0;
var bFullScreen = false;
var requestAnimationFrame = window.requestAnimationFrame    || 
							window.webkitAnimationFrame     ||
							window.mozRequestAnimationFrame ||
							window.opRequestAnimationFrame  ||
							window.msRequestAnimationFrame	 ;

const webGLMacros =
{
	HRH_ATTRIBUTE_POSITION : 0 ,
    HRH_ATTRIBUTE_COLOR : 1 ,
    HRH_ATTRIBUTE_NORMAL : 2 ,
	HRH_ATTRIBUTE_TEXTURE_0 : 3 ,
}

var gVertexShaderObject;
var gFragmentShaderObject;
var gShaderProgramObject;

var gVao_cube;
var gVbo_cube_position;
var gVbo_cube_color;
var gVbo_cube_normals;
var gVbo_pyramid_texcoord;

var gModelUniform;
var gViewUniform;
var gPerspectiveUniform;

var gLightAmbientUniform;
var gLightSpecularUniform;
var gLightDiffusedUniform;
var gLightPossitionUniform;

var gMaterialSpecularUniform;
var gMaterialAmbientUniform;
var gMaterialDiffuseUniform;
var gMaterialShininessUniform;

var gIsLightOnUniform;
var gIsColorOnUniform;
var gIsTextureOnUniform;

var isLightOn = 0, isColorOn = 0, isTextureOn = 0;

var gPerspectiveProjectionMatrix;

var lightPossition = new Float32Array ([ 100.0, 100.0, 100.0, 1.0 ]);
var lightAmbient   = new Float32Array ([ 0.0, 0.0, 0.0 ]);
var lightDefuse    = new Float32Array ([ 1.0, 1.0, 1.0 ]);
var lightSpecular  = new Float32Array ([ 1.0, 1.0, 1.0 ]);

var materialAmbient   = new Float32Array ([ 0.0, 0.0, 0.0 ]);
var materialDefuse    = new Float32Array ([ 1.0, 1.0, 1.0 ]);
var materialSpecular  = new Float32Array ([ 1.0, 1.0, 1.0 ]);
var materialShinyness = 58.0;

var rotateAngle = 0.0;

function main()
{
	// get canvas from dom
    canvas = document.getElementById("hrh"); // type inferance

    if (!canvas) 
	{
        console.log("canvas failed");
    }
    else
    {
        console.log("Got canvas");
    }

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
    
	// window in buil variable , window is inherited from DOM object
	// 3rd parameter false means do bubbling
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);	
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize(); // explicit warm up resize because webgl don't call resize
	
	draw();
	
}

function keyDown(event)
{
	switch(event.keyCode)
	{
		// for 'f' or 'F'
		case 70:
			toggleFullScreen();
			break;
		case 27:
			uninitialize();
			window.close(); // may not work in firefox but will work in chrome and safari
			break;
	}
	
	switch(event.key)
	{
		case 'l':
		case 'L':
			isLightOn = !isLightOn;
			break;
		case 't':
		case 'T':
			isTextureOn = !isTextureOn;
			break;
		case 'c':
		case 'C':
			isColorOn = !isColorOn;
			break;
	}
}

function mouseDown()
{
	
}

function toggleFullScreen()
{
	var fullscreen_element = 	document.fullscreenElement || 
								document.webkitFulllscreenElement || 
								document.mozFullScreen || 
								document.msFullscreenElement ||
								null;
								
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
		{
			canvas.requestFullscreen();			
		} 
		else if(canvas.webkitRequestFulllscreen())
		{
			canvas.webkitRequestFulllscreen();
		}
		else if(canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		} 
		else if(canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		} 
		else if ( document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		} 
		else if (document.mozCancelFullScreen)
		{
			document.mozCancelFullScreen();
		} 
		else if (document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		bFullScreen = false;
	}	
}

function init()
{
	
    // get drawing context from the canvas
    gl = canvas.getContext("webgl2");

    if (!gl) 
	{
        console.log("gl failed");
    }
    else 
	{
        console.log("Got gl");
    }
	
	gVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	
	var vertexShaderSourceCode = 
			"#version 300 es \n" +
			"in vec4 v_position;" +
			"in vec3 v_normals; " +
			"in vec3 v_color; " +
			"in vec2 vTexCoord;" +
			"uniform mat4 u_modelMatrix;" +
			"uniform mat4 u_viewMatrix;" +
			"uniform mat4 u_projectionMatrix;" +
			"uniform int u_isLightOn;" +
			"uniform vec4 u_light_position; " +
			"out vec3 light_direction; " +
			"out vec3 transformed_normal;" +
			"out vec3 view_vector;" +
			"out vec3 outColor; " +
			"out vec2 outTexCoord; " +
			"void main()" +
			"{" +
			"if (u_isLightOn == 1) " +
			"{" +
			"vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" +
			"transformed_normal =  mat3( u_viewMatrix * u_modelMatrix ) * v_normals;" +
			"light_direction = vec3 (u_light_position - eye_coordinates);" +
			"view_vector =  vec3 ( -eye_coordinates);" +
			"}" +
			"outColor = v_color; " +
			"outTexCoord = vTexCoord;"  +
			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" +
			"}" ;

	gl.shaderSource(gVertexShaderObject, vertexShaderSourceCode);
	
	gl.compileShader(gVertexShaderObject);
	
	if(gl.getShaderParameter(gVertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
	var fragmentShaderSourceCode = 	
										"#version 300 es\n"+
										"precision highp float;" +
										"vec3 phoung_ads_lighting;" +
										"out vec4 FragColor; " +
										"in vec3 light_direction; " +
										"in vec3 transformed_normal;" +
										"in vec3 outColor; " +
										"in vec2 outTexCoord;" +
										"in vec3 view_vector;" +
										"vec3 normalized_light_direction; " +
										"vec3 normalized_transformed_normal;" +
										"vec3 normalized_view_vector;" +
										"uniform highp vec3 u_lightAmbient;" +
										"uniform highp vec3 u_lightSpecular;" +
										"uniform highp vec3 u_lightDiffuse;" +
										"uniform highp vec3 u_materialAmbient; " +
										"uniform highp vec3 u_materialSpecular; " +
										"uniform highp vec3 u_materialDiffuse; " +
										"uniform highp float u_materialshininess; " +
										"uniform highp int u_isLightOn;" +
										"uniform highp int u_isColorOn;" +
										"uniform highp int u_isTextureOn;" +
										"uniform highp sampler2D u_texture_sampler;" +
										"void main()" +
										"{" +
										"FragColor = vec4(1.0f, 1.0f, 1.0f, 1.0); " +
										"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" +
										"if (u_isLightOn == 1) " +
										"{" +
										"normalized_light_direction = normalize(light_direction);" +
										"normalized_transformed_normal = normalize(transformed_normal);" +
										"normalized_view_vector = normalize(view_vector);" +
										"vec3 reflection_vector = reflect (-normalized_light_direction, normalized_transformed_normal); " +
										"vec3 ambient = u_lightAmbient * u_materialAmbient; " +
										"vec3 diffuse_light = u_lightDiffuse * u_materialDiffuse * max ( dot (normalized_light_direction , normalized_transformed_normal), 0.0); " +
										"vec3 specular = u_lightSpecular * u_materialSpecular * pow( max ( dot ( reflection_vector , normalized_view_vector), 0.0) , u_materialshininess);" +
										"phoung_ads_lighting = ambient + diffuse_light + specular;" +
										"FragColor = vec4(phoung_ads_lighting, 1.0) ; " +
										"}" +
										"if(u_isColorOn == 1)" +
										"{" +
										"FragColor = FragColor * vec4(outColor, 1.0);" +
										"}" +
										"if(u_isTextureOn == 1)" +
										"{" +
										"FragColor = FragColor * texture(u_texture_sampler,outTexCoord) ;" +
										"}" +
										"}" ;

	gl.shaderSource(gFragmentShaderObject, fragmentShaderSourceCode);
	
	gl.compileShader(gFragmentShaderObject);
	
	if(gl.getShaderParameter(gFragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gShaderProgramObject = gl.createProgram();
	
	// attach vertex shader to shader program
	gl.attachShader(gShaderProgramObject, gVertexShaderObject);
	
	// attch fragment shader to shader program
	gl.attachShader(gShaderProgramObject, gFragmentShaderObject);
	
	// pri-link binding of shader object with vertex shader possition attribute
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_POSITION, "v_position");
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_NORMAL, "v_normals");
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_COLOR, "v_color");
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_TEXTURE_0, "vTexCoord");

	// LINK PROGRAM
	gl.linkProgram(gShaderProgramObject);
	
	if(!gl.getProgramParameter(gShaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	// get MVP uniform location
	gModelUniform = gl.getUniformLocation(gShaderProgramObject, "u_modelMatrix");
	gViewUniform = gl.getUniformLocation(gShaderProgramObject, "u_viewMatrix");
	gPerspectiveUniform = gl.getUniformLocation(gShaderProgramObject, "u_projectionMatrix");
	
	gIsLightOnUniform = gl.getUniformLocation(gShaderProgramObject, "u_isLightOn");
	gIsColorOnUniform = gl.getUniformLocation(gShaderProgramObject, "u_isColorOn");
	gIsTextureOnUniform = gl.getUniformLocation(gShaderProgramObject, "u_isTextureOn");
	
	gLightAmbientUniform =   gl.getUniformLocation(gShaderProgramObject, "u_lightAmbient");
	gLightSpecularUniform =  gl.getUniformLocation(gShaderProgramObject, "u_lightSpecular");
	gLightDiffusedUniform =  gl.getUniformLocation(gShaderProgramObject, "u_lightDiffuse");
	gLightPossitionUniform = gl.getUniformLocation(gShaderProgramObject, "u_light_position");

	gMaterialDiffuseUniform   =   gl.getUniformLocation(gShaderProgramObject, "u_materialDiffuse");
	gMaterialAmbientUniform   =   gl.getUniformLocation(gShaderProgramObject, "u_materialAmbient");
	gMaterialSpecularUniform  =   gl.getUniformLocation(gShaderProgramObject, "u_materialSpecular");
	gMaterialShininessUniform =   gl.getUniformLocation(gShaderProgramObject, "u_materialshininess");
	gTextureSamplerUniform = gl.getUniformLocation(gShaderProgramObject, "u_texture_sampler");

	// rectangle
	var cubeVertices = new Float32Array
		([
		 // FRONT
	  	 1.0,  1.0, 1.0,
		-1.0,  1.0, 1.0,
		-1.0, -1.0, 1.0,
		 1.0, -1.0, 1.0,

		// RIGHT
		1.0,  1.0, -1.0,
		1.0 , 1.0,  1.0,
		1.0, -1.0,  1.0,
		1.0, -1.0, -1.0,

		//BACK
		 1.0, -1.0, -1.0,
		-1.0, -1.0, -1.0,
		-1.0,  1.0, -1.0,
		 1.0,  1.0, -1.0,
		
		// LEFT
		-1.0,  1.0,  1.0,
		-1.0,  1.0, -1.0,
		-1.0, -1.0, -1.0,
		-1.0, -1.0,  1.0,

		// TOP
		 1.0, 1.0, -1.0,
		-1.0, 1.0, -1.0,
		-1.0, 1.0,  1.0,
		 1.0, 1.0,  1.0,

		// BOTTOM
	 	 1.0, -1.0,  1.0,
		-1.0, -1.0,  1.0,
		-1.0, -1.0, -1.0,
		 1.0, -1.0, -1.0,
		]);
	
	var cubeColor = new Float32Array
	([
		// 1
		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0,

		//2
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
					 
		//3          
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
					 
		//4          
		1.0, 1.0, 0.0,
		1.0, 1.0, 0.0,
		1.0, 1.0, 0.0,
		1.0, 1.0, 0.0,
					 
		//5          
		0.0, 1.0, 1.0,
		0.0, 1.0, 1.0,
		0.0, 1.0, 1.0,
		0.0, 1.0, 1.0,
					 
		//6          
		1.0, 0.0, 1.0,
		1.0, 0.0, 1.0,
		1.0, 0.0, 1.0,
		1.0, 0.0, 1.0,

	]);

	gVao_cube = gl.createVertexArray();
	gl.bindVertexArray(gVao_cube);
	
	gVbo_cube_position = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_cube_position);
	gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gVbo_cube_color = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_cube_color);
	gl.bufferData(gl.ARRAY_BUFFER, cubeColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	var cubeTexcoord = new Float32Array
	([
		// 1
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
				
		//2     
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
				
		//3     
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
				
		//4     
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
				
		//5     
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
				
		//6     
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0,

	]);

	gVbo_cube_texcoord = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_cube_texcoord);
	gl.bufferData(gl.ARRAY_BUFFER, cubeTexcoord, gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_TEXTURE_0, 2, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_TEXTURE_0);	
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	var glNormal_coord = new Float32Array
				([
				 0.0,  0.0,  1.0 , //front
				 0.0,  0.0,  1.0 , //front
				 0.0,  0.0,  1.0 , //front
				 0.0,  0.0,  1.0 , //front
								
				 1.0,  0.0,  0.0 , // right
				 1.0,  0.0,  0.0 , // right
				 1.0,  0.0,  0.0 , // right
				 1.0,  0.0,  0.0 , // right
								
				 0.0, 0.0,  -1.0 , //back
				 0.0, 0.0,  -1.0 , //back
				 0.0, 0.0,  -1.0 , //back
				 0.0, 0.0,  -1.0 , //back

				-1.0,  0.0, 0.0 , //left
				-1.0,  0.0, 0.0 , //left
				-1.0,  0.0, 0.0 , //left
				-1.0,  0.0, 0.0 , //left
							   
							   
				0.0,  1.0,  0.0 , // top
				0.0,  1.0,  0.0 , // top
				0.0,  1.0,  0.0 , // top
				0.0,  1.0,  0.0 , // top
							   
				0.0, -1.0,  0.0 , // bottom
				0.0, -1.0,  0.0 , // bottom
				0.0, -1.0,  0.0 , // bottom
				0.0, -1.0,  0.0 , // bottom
				]);

	gVbo_cube_normals = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_cube_normals);
	gl.bufferData(gl.ARRAY_BUFFER, glNormal_coord, gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_NORMAL);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	texture = gl.createTexture();
	texture.image = new Image();
	texture.image.src = "marble.png";
	texture.image.onload = function (){
					gl.bindTexture(gl.TEXTURE_2D, texture);
					gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
					gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
					gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
					// 0 - MIPMAP LEVEL 0
					gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image);
					gl.bindTexture(gl.TEXTURE_2D, null);
				};

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	gPerspectiveProjectionMatrix = mat4.create();	
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	// ser the viewport to match	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(gPerspectiveProjectionMatrix, 
						45.0, 
						parseFloat(canvas.width) /parseFloat(canvas.height), 
						parseFloat(canvas.height) / parseFloat(canvas.width), 
						100.0); // no 0.1f
	
}

function draw()
{
	// code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(gShaderProgramObject);
	
	var viewMatrix = mat4.create();
	var modelMatrix = mat4.create();
	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);
	var modelViewProjectionMatrix = mat4.create();

	var rotateMatrix = mat4.create();
	
	mat4.rotateX(rotateMatrix, rotateMatrix, degToRad(rotateAngle));
	mat4.rotateY(rotateMatrix, rotateMatrix, degToRad(rotateAngle));
	mat4.rotateZ(rotateMatrix, rotateMatrix, degToRad(rotateAngle));

	mat4.multiply(modelMatrix, modelMatrix, rotateMatrix);

	if (isLightOn == 1)
	{
		gl.uniform4fv(gLightPossitionUniform, lightPossition);
		gl.uniform3fv(gLightDiffusedUniform, lightDefuse);
		gl.uniform3fv(gLightAmbientUniform,  lightAmbient);
		gl.uniform3fv(gLightSpecularUniform, lightSpecular);

		gl.uniform3fv(gMaterialAmbientUniform,  materialAmbient);
		gl.uniform3fv(gMaterialSpecularUniform, materialSpecular);
		gl.uniform3fv(gMaterialDiffuseUniform,  materialDefuse);
		gl.uniform1f (gMaterialShininessUniform, materialShinyness);
	}

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniform1i(gIsTextureOnUniform, isTextureOn);
	gl.uniform1i(gIsColorOnUniform, isColorOn);

	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false,  viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, texture);
	gl.uniform1i(gTextureSamplerUniform, 0);

	gl.bindVertexArray(gVao_cube);
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
	gl.bindVertexArray(null);

	if(rotateAngle > 360.0)
	{
		rotateAngle = 0.0;
	}
	rotateAngle = rotateAngle + 0.1;
	
	requestAnimationFrame(draw, canvas);
}

function degToRad(degree)
{
	return ( degree * Math.PI / 180.0 );
}

function uninitialize()
{	
	if(gShaderProgramObject)
	{
		if(gFragmentShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gFragmentShaderObject);
			gl.deleteShader(gFragmentShaderObject);
			gFragmentShaderObject = null;
		}
		
		if(gVertexShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gVertexShaderObject);
			gl.deleteShader(gVertexShaderObject);
			gVertexShaderObject = null;
		}
		
		gl.deleteProgram(gShaderProgramObject);
		gShaderProgramObject = null;
	}
}
