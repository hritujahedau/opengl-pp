// JavaScript source code
var canvas = null;
var gl;
var canvas_original_width = 0, canvas_original_height = 0;
var bFullScreen = false;
var requestAnimationFrame = window.requestAnimationFrame    || 
							window.webkitAnimationFrame     ||
							window.mozRequestAnimationFrame ||
							window.opRequestAnimationFrame  ||
							window.msRequestAnimationFrame	 ;

const webGLMacros =
{
	HRH_ATTRIBUTE_POSITION : 0 ,
    HRH_ATTRIBUTE_COLOR : 1 ,
    HRH_ATTRIBUTE_NORMAL : 2 ,
	HRH_ATTRIBUTE_TEXTURE_0 : 3 ,
}


var gVertexShaderObjectPerVertex;
var gFragmentShaderObjectPerVertex;

var gVertexShaderObjectPerFragment;
var gFragmentShaderObjectPerFragment;

var gShaderProgramObjectPerVertex;
var gShaderProgramObjectPerFragment;
var gShaderProgramObject;

var gVao;
var gVbo;
var gMVPUniform;

var sphere = null;

var gModelUniform;
var gViewUniform;
var gPerspectiveUniform;

var gMaterialSpecularUniform;
var gMaterialAmbientUniform;
var gMaterialDiffuseUniform;
var gMaterialShininessUniform;

var gIsLightOnUniform;

var isLightOn = 0;
var isPerFragmentLight = 0;
var isPerVertexLight = 0;

var lightPosition = new Float32Array ([ 0.0, 0.0, 0.0, 1.0, 
							0.0, 0.0, 0.0, 1.0,
							0.0, 0.0, 0.0, 1.0
						]);
						
var lightAmbient = new Float32Array ([  0.0, 0.0, 0.0,
							0.0, 0.0, 0.0,
							0.0, 0.0, 0.0
						]);
						
var lightDiffuse = new Float32Array ([  1.0, 0.0, 0.0,
							0.0, 1.0, 0.0,
							0.0, 0.0, 1.0
						]);
							
var lightSpecular = new Float32Array ([ 1.0, 0.0, 0.0,
							0.0, 1.0, 0.0,
							0.0, 0.0, 1.0
						]);

var gLightAmbientUniform;
var gLightSpecularUniform;
var gLightDiffusedUniform;
var gLightPossitionUniform;

var materialAmbient =  new Float32Array ([ 0.0, 0.0, 0.0 ]);
var materialDiffuse =  new Float32Array ([ 1.0, 1.0, 1.0 ]);
var materialSpecular = new Float32Array ([ 1.0, 1.0, 1.0  ]);
var materialShininess = 128.0;

var bAnimate = false;

var angle_red = 360.0,angle_blue = 0.0, angle_green = 360.0;

var sin_angle = 0.0, cos_angle = 0.0, radius = 5.0;

var gPerspectiveProjectionMatrix;

function main()
{
	// get canvas from dom
    canvas = document.getElementById("hrh"); // type inferance

    if (!canvas) 
	{
        console.log("canvas failed");
    }
    else
    {
        console.log("Got canvas");
    }

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
    
	// window in buil variable , window is inherited from DOM object
	// 3rd parameter false means do bubbling
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);	
	window.addEventListener("resize", resize, false);
	
	init();
	resize(); // explicit warm up resize because webgl don't call resize
	draw();
	
}

function keyDown(event)
{
	switch(event.keyCode)
	{
		case 27:
			uninitialize();
			window.close(); // may not work in firefox but will work in chrome and safari
			break;
	}
	switch(event.key)
	{
			case 'a':
			case 'A':
				if (bAnimate == false)
				{
					bAnimate = true;
				}
				else
				{
					bAnimate = false;
				}
				break;
			case 'l':
			case 'L':
				if (isLightOn == 0)
				{
					isLightOn = 1;
				}
				else
				{
					isLightOn = 0;
				}
				break;
			case 'q':
			case 'Q':
				toggleFullScreen();
				break;
			case 'f':
			case 'F':
				if(isPerFragmentLight == 0)
				{
					isPerFragmentLight = 1;
					isPerVertexLight = 0;
					isLightOn = 1;
					bAnimate = 1;
					gShaderProgramObject = gShaderProgramObjectPerFragment;
				}
				else
				{
					isPerFragmentLight = 0;
					isLightOn = 0;
				}
				break;
			case 'v':
			case 'V':
				if(isPerVertexLight == 0)
				{
					isPerVertexLight = 1;
					isPerFragmentLight =0;
					isLightOn = 1;
					bAnimate = 1;
					gShaderProgramObject = gShaderProgramObjectPerVertex;
				}
				else
				{
					isPerVertexLight = 0;
					isLightOn = 0;
				}
				break;
		}
}

function mouseDown()
{
	
}

function toggleFullScreen()
{
	var fullscreen_element = 	document.fullscreenElement || 
								document.webkitFulllscreenElement || 
								document.mozFullScreen || 
								document.msFullscreenElement ||
								null;
								
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
		{
			canvas.requestFullscreen();			
		} 
		else if(canvas.webkitRequestFulllscreen())
		{
			canvas.webkitRequestFulllscreen();
		}
		else if(canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		} 
		else if(canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		} 
		else if ( document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		} 
		else if (document.mozCancelFullScreen)
		{
			document.mozCancelFullScreen();
		} 
		else if (document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		bFullScreen = false;
	}	
}

function init()
{
	
    // get drawing context from the canvas
    gl = canvas.getContext("webgl2");

    if (!gl) 
	{
        console.log("gl failed");
    }
    else 
	{
        console.log("Got gl");
    }
	
	gVertexShaderObjectPerVertex = gl.createShader(gl.VERTEX_SHADER);
	
	var vertexShaderSourceCodePerVertex = 
			"#version 300 es \n" +
			"in vec4 v_position;" +
			"in vec3 v_normals; " +
			"uniform mat4 u_modelMatrix;" +
			"uniform mat4 u_viewMatrix;" +
			"uniform mat4 u_projectionMatrix;" +
			"uniform int u_isLightOn;" +
			"uniform vec3 u_lightAmbient[3];" +
			"uniform vec3 u_lightSpecular[3];" +
			"uniform vec3 u_lightDiffuse[3];" +
			"uniform vec4 u_lightposition[3]; " +
			"uniform vec3 u_materialAmbient; " +
			"uniform vec3 u_materialSpecular; " +
			"uniform vec3 u_materialDiffuse; " +
			"uniform float u_materialshininess; " +
			"out vec3 phoung_ads_lighting;" +
			"vec4 eye_coordinates;" +
			"vec3 transformed_normal, light_direction, reflection_vector, view_vector;" +
			"vec3 ambient, diffuse_light, specular;" +
			"void main()" +
			"{" +
			"if (u_isLightOn == 1) " +
			"{" +
				"eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" +
				"transformed_normal = normalize( mat3( u_viewMatrix * u_modelMatrix ) * v_normals); " +
				"for(int i=0; i< 3;i++) " +
				"{" +
					"light_direction = normalize( vec3 ( ( u_lightposition[i]) - eye_coordinates) ); " +
					"reflection_vector = reflect (-light_direction, transformed_normal); " +
					"view_vector = normalize ( vec3 ( -eye_coordinates)); " +
					"ambient = u_lightAmbient[i] * u_materialAmbient; " +
					"diffuse_light = u_lightDiffuse[i] * u_materialDiffuse * max ( dot (light_direction , transformed_normal), 0.0); " +
					"specular = u_lightSpecular[i] * u_materialSpecular * pow( max ( dot ( reflection_vector , view_vector), 0.0) , u_materialshininess);" +
					"phoung_ads_lighting = phoung_ads_lighting + ambient + diffuse_light + specular;" +
				"}" +
			"}" +
			"else" +
			"{" +
			"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" +
			"}"+
			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" +
			"}";

	gl.shaderSource(gVertexShaderObjectPerVertex, vertexShaderSourceCodePerVertex);
	
	gl.compileShader(gVertexShaderObjectPerVertex);
	
	if(gl.getShaderParameter(gVertexShaderObjectPerVertex, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObjectPerVertex);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gFragmentShaderObjectPerVertex = gl.createShader(gl.FRAGMENT_SHADER);
	
	var fragmentShaderSourceCodePerVertex = 	
									"#version 300 es \n"+
									"precision highp float;" +
									"in vec3 phoung_ads_lighting;" +
									"out vec4 FragColor; " +
									"void main()"+
									"{" +
									"FragColor = vec4(phoung_ads_lighting, 1.0); " +
									"}" ;
	
	gl.shaderSource(gFragmentShaderObjectPerVertex, fragmentShaderSourceCodePerVertex);
	
	gl.compileShader(gFragmentShaderObjectPerVertex);
	
	if(gl.getShaderParameter(gFragmentShaderObjectPerVertex, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObjectPerVertex);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gShaderProgramObjectPerVertex = gl.createProgram();
	
	// attach vertex shader to shader program
	gl.attachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);
	
	// attch fragment shader to shader program
	gl.attachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);
	
	// pri-link binding of shader object with vertex shader possition attribute
	gl.bindAttribLocation(gShaderProgramObjectPerVertex, webGLMacros.HRH_ATTRIBUTE_POSITION, "v_position");
	gl.bindAttribLocation(gShaderProgramObjectPerVertex, webGLMacros.HRH_ATTRIBUTE_NORMAL, "v_normals");

	// LINK PROGRAM
	gl.linkProgram(gShaderProgramObjectPerVertex);
	
	if(!gl.getProgramParameter(gShaderProgramObjectPerVertex, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObjectPerVertex);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	/*********************************** Per Fragment ****************************************/
	
	gVertexShaderObjectPerFragment = gl.createShader(gl.VERTEX_SHADER);
	
	var vertexShaderSourceCodePerFragment = 
									"#version 300 es\n"  +
									"in vec4 v_position;" +
									"in vec3 v_normals; " +
									"uniform mat4 u_modelMatrix;" +
									"uniform mat4 u_viewMatrix;" +
									"uniform mat4 u_projectionMatrix;" +
									"uniform int u_isLightOn;" +
									"uniform vec4 u_lightposition[3]; " +
									"out vec3 light_direction[3]; "+
									"out vec3 transformed_normal;"+
									"out vec3 view_vector;"+
									"void main()" +
									"{" +
									"if (u_isLightOn == 1) " +
									"{" +
										"vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" +
										"transformed_normal =  mat3( u_viewMatrix * u_modelMatrix ) * v_normals;" +
										"for(int i = 0; i< 3; i++)"+
										"{"+
											"light_direction[i] = vec3 ( u_lightposition[i]  - eye_coordinates);" +
										"}"+
										"view_vector =  vec3 ( -eye_coordinates);" +
									"}" +
									"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" +
									"}";

	gl.shaderSource(gVertexShaderObjectPerFragment, vertexShaderSourceCodePerFragment);
	
	gl.compileShader(gVertexShaderObjectPerFragment);
	
	if(gl.getShaderParameter(gVertexShaderObjectPerFragment, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObjectPerFragment);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gFragmentShaderObjectPerFragment = gl.createShader(gl.FRAGMENT_SHADER);
	
	var fragmentShaderSourceCodePerFragment = 	
											"#version 300 es\n" +
											"precision highp float;" +
											"vec3 phoung_ads_lighting;" +
											"out vec4 FragColor; " +
											"in vec3 light_direction[3]; "+
											"in vec3 transformed_normal;"+
											"in vec3 view_vector;"+
											"vec3 normalized_light_direction; "+
											"vec3 normalized_transformed_normal;"+
											"vec3 normalized_view_vector;"+
											"uniform highp vec3 u_lightAmbient[3];" +
											"uniform highp vec3 u_lightSpecular[3];" +
											"uniform highp vec3 u_lightDiffuse[3];" +
											"uniform highp vec3 u_materialAmbient; " +
											"uniform highp vec3 u_materialSpecular; "+
											"uniform highp vec3 u_materialDiffuse; "+
											"uniform highp float u_materialshininess; "+
											"uniform highp int u_isLightOn;" +
											"vec3 reflection_vector, ambient, diffuse_light, specular;"+
											"void main()" +
											"{"+
											"if (u_isLightOn == 1) " +
											"{" +
												"normalized_view_vector = normalize(view_vector);"+
												"normalized_transformed_normal = normalize(transformed_normal);"+
												"for(int i = 0; i < 3 ; i++)"+
												"{"+
													"normalized_light_direction = normalize(light_direction[i]);"+
													"reflection_vector = reflect (-normalized_light_direction, normalized_transformed_normal); " +
													"ambient = u_lightAmbient[i]  * u_materialAmbient; " +
													"diffuse_light = u_lightDiffuse[i] * u_materialDiffuse * max ( dot (normalized_light_direction , normalized_transformed_normal), 0.0); "+
													"specular = u_lightSpecular[i]  * u_materialSpecular * pow( max ( dot ( reflection_vector , normalized_view_vector), 0.0) , u_materialshininess);" +
													"phoung_ads_lighting = phoung_ads_lighting + ambient + diffuse_light + specular;" +
												"}"+
											"}"+
											"else"+
											"{"+
												"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" +
											"}"+
											"FragColor = vec4(phoung_ads_lighting, 1.0); " +
											"}";
	
	gl.shaderSource(gFragmentShaderObjectPerFragment, fragmentShaderSourceCodePerFragment);
	
	gl.compileShader(gFragmentShaderObjectPerFragment);
	
	if(gl.getShaderParameter(gFragmentShaderObjectPerFragment, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObjectPerFragment);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gShaderProgramObjectPerFragment = gl.createProgram();
	
	// attach vertex shader to shader program
	gl.attachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);
	
	// attch fragment shader to shader program
	gl.attachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);
	
	// pri-link binding of shader object with vertex shader possition attribute
	gl.bindAttribLocation(gShaderProgramObjectPerFragment, webGLMacros.HRH_ATTRIBUTE_POSITION, "v_position");
	gl.bindAttribLocation(gShaderProgramObjectPerFragment, webGLMacros.HRH_ATTRIBUTE_NORMAL, "v_normals");

	// LINK PROGRAM
	gl.linkProgram(gShaderProgramObjectPerFragment);
	
	if(!gl.getProgramParameter(gShaderProgramObjectPerFragment, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObjectPerFragment);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//gShaderProgramObject = gShaderProgramObjectPerVertex;
	
	sphere = new Mesh();
		
	makeSphere(sphere, 2.0, 30, 30);
	
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);

	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	gPerspectiveProjectionMatrix = mat4.create();
	
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	// ser the viewport to match	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(gPerspectiveProjectionMatrix, 
						45.0, 
						parseFloat(canvas.width) /parseFloat(canvas.height), 
						parseFloat(canvas.height) / parseFloat(canvas.width), 
						100.0); // no 0.1f
	
}

function draw()
{
	// code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gShaderProgramObject = (isPerVertexLight == 1) ? gShaderProgramObjectPerVertex : gShaderProgramObjectPerFragment  ;
	
	gl.useProgram(gShaderProgramObject);
	
	// VARIABLE DECLARATION
	var modelMatrix = mat4.create();
	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -4.0]);
	
	var viewMatrix = mat4.create();	
	// get MVP uniform location
	gModelUniform       = gl.getUniformLocation(gShaderProgramObject, "u_modelMatrix");
	gViewUniform        = gl.getUniformLocation(gShaderProgramObject, "u_viewMatrix");
	gPerspectiveUniform = gl.getUniformLocation(gShaderProgramObject, "u_projectionMatrix");
	gIsLightOnUniform   = gl.getUniformLocation(gShaderProgramObject, "u_isLightOn");
		
	gLightAmbientUniform   = gl.getUniformLocation(gShaderProgramObject, "u_lightAmbient");
	gLightSpecularUniform  = gl.getUniformLocation(gShaderProgramObject, "u_lightSpecular");
	gLightDiffusedUniform  = gl.getUniformLocation(gShaderProgramObject, "u_lightDiffuse");
	gLightPossitionUniform = gl.getUniformLocation(gShaderProgramObject, "u_lightposition");

	gMaterialDiffuseUniform   = gl.getUniformLocation(gShaderProgramObject, "u_materialDiffuse");
	gMaterialAmbientUniform   = gl.getUniformLocation(gShaderProgramObject, "u_materialAmbient");
	gMaterialSpecularUniform  = gl.getUniformLocation(gShaderProgramObject, "u_materialSpecular");
	gMaterialShininessUniform = gl.getUniformLocation(gShaderProgramObject, "u_materialshininess");
	
	if (isLightOn == 1)
	{
		sin_angle = radius * Math.sin(degToRad(angle_red));
		cos_angle = radius * Math.cos(degToRad(angle_red));
		
		lightPosition[1] = sin_angle;
		lightPosition[2] = cos_angle;
		
		sin_angle = radius * Math.sin(degToRad(angle_blue));
		cos_angle = radius * Math.cos(degToRad(angle_blue));
		
		lightPosition[4] = sin_angle;
		lightPosition[6] = cos_angle;
		
		sin_angle = radius * Math.sin(degToRad(angle_green));
		cos_angle = radius * Math.cos(degToRad(angle_green));
		
		lightPosition[8] = sin_angle;
		lightPosition[9] = cos_angle;
		
		gl.uniform4fv(gLightPossitionUniform, lightPosition);
		gl.uniform3fv(gLightDiffusedUniform, lightDiffuse);
		gl.uniform3fv(gLightAmbientUniform, lightAmbient);
		gl.uniform3fv(gLightSpecularUniform, lightSpecular);
		
		gl.uniform3fv(gMaterialAmbientUniform, materialAmbient);
		gl.uniform1f(gMaterialShininessUniform, materialShininess);
		gl.uniform3fv(gMaterialSpecularUniform, materialSpecular);
		gl.uniform3fv(gMaterialDiffuseUniform,  materialDiffuse);
	}

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();
	
	if (bAnimate == true)
	{
		angle_red = angle_red - 0.3;
		angle_blue = angle_blue + 0.3;
		angle_green = angle_green - 0.3;
		if (angle_red <= 0)
		{
			angle_red = 360;
		}
		if (angle_blue >= 360)
		{
			angle_blue = 1.5;
		}
		if (angle_green <= 0)
		{
			angle_green = 360;
		}
	}
		
	
	
	requestAnimationFrame(draw, canvas);
}

function degToRad(degree)
{
	return ( degree * Math.PI / 180.0 );
}

function uninitialize()
{
	if(sphere)
	{
		sphere.deallocate();
	}
	
	if(gVao)
	{
		gl.deleteVertexArray(gVao);
		gVao = null;
	}
	
	if(gVbo)
	{
		gl.deleteBuffer(gVbo);
		gVbo = null;
	}
	
	if(gShaderProgramObjectPerVertex)
	{
		if(gFragmentShaderObjectPerVertex)
		{
			gl.detachShader(gShaderProgramObject, gFragmentShaderObjectPerVertex);
			gl.deleteShader(gFragmentShaderObjectPerVertex);
			gFragmentShaderObjectPerVertex = null;
		}
		
		if(gVertexShaderObjectPerVertex)
		{
			gl.detachShader(gShaderProgramObject, gVertexShaderObjectPerVertex);
			gl.deleteShader(gVertexShaderObjectPerVertex);
			gVertexShaderObjectPerVertex = null;
		}
		
		gl.deleteProgram(gShaderProgramObject);
		gShaderProgramObject = null;
	}
}
