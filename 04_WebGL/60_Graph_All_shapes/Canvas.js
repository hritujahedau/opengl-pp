// JavaScript source code
var canvas = null;
var gl;
var canvas_original_width = 0, canvas_original_height = 0;
var bFullScreen = false;
var requestAnimationFrame = window.requestAnimationFrame    || 
							window.webkitAnimationFrame     ||
							window.mozRequestAnimationFrame ||
							window.opRequestAnimationFrame  ||
							window.msRequestAnimationFrame	 ;

const webGLMacros =
{
	HRH_ATTRIBUTE_POSITION : 0 ,
    HRH_ATTRIBUTE_COLOR : 1 ,
    HRH_ATTRIBUTE_NORMAL : 2 ,
	HRH_ATTRIBUTE_TEXTURE_0 : 3 ,
}

var gVertexShaderObject;
var gFragmentShaderObject;
var gShaderProgramObject;

var gVao;
var gVbo_position;
var gVbo_color;
var gMVPUniform;

var gPerspectiveProjectionMatrix;

var color = new Float32Array([ 0.0, 0.0, 1.0, 0.0, 0.0, 1.0 ]);
var vertices = new Float32Array([ 1.0, 0.0, 0.0, -1.0, 0.0, 0.0 ]);

var a = 
{
	x: 0,
	y: 0,
};

var b = 
{
	x: 0,
	y: 0,
};

var c = 
{
	x: 0,
	y: 0,
};

var d = 
{
	x: 0,
	y: 0,
};

var radius = 1.0;

function main()
{
	// get canvas from dom
    canvas = document.getElementById("hrh"); // type inferance

    if (!canvas) 
	{
        console.log("canvas failed");
    }
    else
    {
        console.log("Got canvas");
    }

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
    
	// window in buil variable , window is inherited from DOM object
	// 3rd parameter false means do bubbling
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);	
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize(); // explicit warm up resize because webgl don't call resize
	
	draw();
	
}

function keyDown(event)
{
	switch(event.keyCode)
	{
		// for 'f' or 'F'
		case 70:
			toggleFullScreen();
			break;
		case 27:
			uninitialize();
			window.close(); // may not work in firefox but will work in chrome and safari
			break;
	}
}

function mouseDown()
{
	
}

function toggleFullScreen()
{
	var fullscreen_element = 	document.fullscreenElement || 
								document.webkitFulllscreenElement || 
								document.mozFullScreen || 
								document.msFullscreenElement ||
								null;
								
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
		{
			canvas.requestFullscreen();			
		} 
		else if(canvas.webkitRequestFulllscreen())
		{
			canvas.webkitRequestFulllscreen();
		}
		else if(canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		} 
		else if(canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		} 
		else if ( document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		} 
		else if (document.mozCancelFullScreen)
		{
			document.mozCancelFullScreen();
		} 
		else if (document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		bFullScreen = false;
	}	
}

function init()
{
	
    // get drawing context from the canvas
    gl = canvas.getContext("webgl2");

    if (!gl) 
	{
        console.log("gl failed");
    }
    else 
	{
        console.log("Got gl");
    }
	
	gVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	
	var vertexShaderSourceCode = 
								"#version 300 es \n" +
								"in vec4 vPosition;" +
								"in vec4 vColor;" +
								"out vec4 outColor;"+
								"uniform mat4 u_mvpMatrix;" +
								"void main()" +
								"{" +
								"gl_Position = u_mvpMatrix * vPosition;" +
								"outColor = vColor;" +
								"}";

	gl.shaderSource(gVertexShaderObject, vertexShaderSourceCode);
	
	gl.compileShader(gVertexShaderObject);
	
	if(gl.getShaderParameter(gVertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
	var fragmentShaderSourceCode = 	"#version 300 es\n"+
									"precision highp float;" +
									"in vec4 outColor;" +
									"out vec4 FragColor; " +
									"void main()"+
									"{" +
									"FragColor = outColor;" +
									"}";
	
	gl.shaderSource(gFragmentShaderObject, fragmentShaderSourceCode);
	
	gl.compileShader(gFragmentShaderObject);
	
	if(gl.getShaderParameter(gFragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gShaderProgramObject = gl.createProgram();
	
	// attach vertex shader to shader program
	gl.attachShader(gShaderProgramObject, gVertexShaderObject);
	
	// attch fragment shader to shader program
	gl.attachShader(gShaderProgramObject, gFragmentShaderObject);
	
	// pri-link binding of shader object with vertex shader possition attribute
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_COLOR, "vColor");

	// LINK PROGRAM
	gl.linkProgram(gShaderProgramObject);
	
	if(!gl.getProgramParameter(gShaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gMVPUniform = gl.getUniformLocation(gShaderProgramObject, "u_mvpMatrix");
	
	gVao = gl.createVertexArray();
	gl.bindVertexArray(gVao);
	
	gVbo_position = gl.createBuffer()
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_position);
	gl.bufferData(gl.ARRAY_BUFFER, null, gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_POSITION);
		
	gVbo_color = gl.createBuffer()
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_color);
	gl.bufferData(gl.ARRAY_BUFFER, null, gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_COLOR);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, null);	
	gl.bindVertexArray(null);	
	
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	gPerspectiveProjectionMatrix = mat4.create();
	
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	// ser the viewport to match	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(gPerspectiveProjectionMatrix, 
						45.0, 
						parseFloat(canvas.width) /parseFloat(canvas.height), 
						parseFloat(canvas.height) / parseFloat(canvas.width), 
						100.0); // no 0.1f
	
}

function draw()
{
	// code
	gl.clear(gl.COLOR_BUFFER_BIT);
	
	gl.useProgram(gShaderProgramObject);
	
	var modelViewMatrix = mat4.create();
	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -3.0]);
	var modelViewProjectionMatrix = mat4.create();
	
	mat4.multiply(modelViewProjectionMatrix, gPerspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMVPUniform, false, modelViewProjectionMatrix);
	
	graph();
	hallowBigCircle();
	hallowRectangle();
	hallowTriangle();
	hallowSmallCircle();

	gl.useProgram(null);
	
	requestAnimationFrame(draw, canvas);
}

function hallowBigCircle()
{
	var circleLines = new Float32Array ([ 
		0.0, 0.0, 0.0, 
		0.0, 0.0, 0.0
	]);
	
	var rectnagleColor = new Float32Array ([ 
		1.0, 1.0, 0.0, 
		1.0, 1.0, 0.0
	]);

	// CODE
	var circle_points = 360;
	var angle = 0;
	
	for (var i = 1; i < circle_points; i++) {
		angle = (i - 1) * 0.01745329;
		circleLines[0] = radius * Math.cos(angle);
		circleLines[1] = radius * Math.sin(angle);
		circleLines[2] = 0.0;
		angle = i * 0.01745329;
		circleLines[3] = radius * Math.cos(angle);
		circleLines[4] = radius * Math.sin(angle);
		circleLines[5] = 0.0;
		
	    gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_color);
	    gl.bufferData(gl.ARRAY_BUFFER, rectnagleColor, gl.DYNAMIC_DRAW);
	    gl.bindBuffer(gl.ARRAY_BUFFER, null);
	    
	    gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_position);
	    gl.bufferData(gl.ARRAY_BUFFER, circleLines, gl.DYNAMIC_DRAW);
	    gl.bindBuffer(gl.ARRAY_BUFFER, null);

		gl.bindVertexArray(gVao);
		gl.drawArrays(gl.LINES, 0, 2);
		gl.bindVertexArray(null);

	}
}

function hallowTriangle()
{
	var ax = radius * (Math.cos(35.0 * 0.01745329));
	var ay = radius * (Math.sin(35.0 * 0.01745329));

	var bx = radius * (Math.cos(145.0 * 0.01745329));
	var by = radius * (Math.sin(145.0 * 0.01745329));

	var cx = radius * (Math.cos(215.0 * 0.01745329));
	var cy = radius * (Math.sin(215.0 * 0.01745329));

	var dx = radius * (Math.cos(325.0 * 0.01745329));
	var dy = radius * (Math.sin(325.0 * 0.01745329));

	var ex = (ax + bx) / 2;
	var ey = (ay + by) / 2;
	
	var triangle = new Float32Array ([ 
		ex, ey, 0.0,
		cx, cy, 0.0,
		dx, dy, 0.0,
		ex, ey, 0.0,
		]);

	var tringleColor = new Float32Array ([ 1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0, 0.0 ]);

	// CODE
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_color);
	gl.bufferData(gl.ARRAY_BUFFER, tringleColor, gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_position);
	gl.bufferData(gl.ARRAY_BUFFER, triangle, gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(gVao);
	gl.drawArrays(gl.LINES, 0, 2);
	gl.drawArrays(gl.LINES, 1, 2);
	gl.drawArrays(gl.LINES, 2, 2);
	gl.bindVertexArray(null);
}

function getLengthFromVertises( x1, y1, x2, y2 )
{
	// eqaution for calculating legth between 2 points
	// x1, y1, x2, y2
	//x1 = arguments[0];
	//y1 = arguments[1];
	//x2 = arguments[2];
	//y2 = arguments[3];

	return Math.sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
}

function hallowSmallCircle()
{
	var circleLines = new Float32Array ([ 
		0.0, 0.0, 0.0, 
		0.0, 0.0, 0.0
	]);
	
	var rectnagleColor = new Float32Array ([ 
		1.0, 1.0, 0.0, 
		1.0, 1.0, 0.0
	]);

	// CODE
	var circle_points = 360, angle = 0.0;
	var s = 0.0, length_ec = 0.0, length_cd = 0.0, length_de = 0.0;
	var hallowSmallCircleRadius = 0.0;

	var ax = radius * (Math.cos(35.0 * 0.01745329));
	var ay = radius * (Math.sin(35.0 * 0.01745329));

	var bx = radius * (Math.cos(145.0 * 0.01745329));
	var by = radius * (Math.sin(145.0 * 0.01745329));

	var cx = radius * (Math.cos(215.0 * 0.01745329));
	var cy = radius * (Math.sin(215.0 * 0.01745329));

	var dx = radius * (Math.cos(325.0 * 0.01745329));
	var dy = radius * (Math.sin(325.0 * 0.01745329));

	var ex = (ax + bx) / 2;
	var ey = (ay + by) / 2;

	length_ec = getLengthFromVertises(ex, ey, cx, cy);
	length_cd = getLengthFromVertises(cx, cy, dx, dy);
	length_de = getLengthFromVertises(dx, dy, ex, ey);

	s = (length_ec + length_cd + length_de) / 2;
	hallowSmallCircleRadius = Math.sqrt(s * (s - length_ec) * (s - length_cd) * (s - length_de)) / s;

	
	for (var i = 1; i < circle_points; i++) {
		angle = (i - 1) * 0.01745329;
		circleLines[0] = hallowSmallCircleRadius * Math.cos(angle);
		circleLines[1] = -0.15 + hallowSmallCircleRadius * Math.sin(angle);
		circleLines[2] = 0.0;
		angle = i * 0.01745329;
		circleLines[3] = hallowSmallCircleRadius * Math.cos(angle);
		circleLines[4] = -0.15 + hallowSmallCircleRadius * Math.sin(angle);
		circleLines[5] = 0.0;
		
	    gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_color);
	    gl.bufferData(gl.ARRAY_BUFFER, rectnagleColor, gl.DYNAMIC_DRAW);
	    gl.bindBuffer(gl.ARRAY_BUFFER, null);
	    
	    gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_position);
	    gl.bufferData(gl.ARRAY_BUFFER, circleLines, gl.DYNAMIC_DRAW);
	    gl.bindBuffer(gl.ARRAY_BUFFER, null);

		gl.bindVertexArray(gVao);
		gl.drawArrays(gl.LINES, 0, 2);
		gl.bindVertexArray(null);

	}

}

function hallowRectangle()
{
	ax = radius * (Math.cos(35.0 * 0.01745329));
	ay = radius * (Math.sin(35.0 * 0.01745329));

	bx = radius * (Math.cos(145.0 * 0.01745329));
	by = radius * (Math.sin(145.0 * 0.01745329));

	cx = radius * (Math.cos(215.0 * 0.01745329));
	cy = radius * (Math.sin(215.0 * 0.01745329));

	dx = radius * (Math.cos(325.0 * 0.01745329));
	dy = radius * (Math.sin(325.0 * 0.01745329));

	var rectangle = new Float32Array ([ 
		 ax, ay, 0.0,
		 bx, by, 0.0,
		 cx, cy, 0.0,
		 dx, dy, 0.0,
		 ax, ay, 0.0,
	]);
	
	var rectnagleColor = new Float32Array ([
		1.0, 1.0, 0.0, 
		1.0, 1.0, 0.0, 
		1.0, 1.0, 0.0, 
		1.0, 1.0, 0.0,
		1.0, 1.0, 0.0 
	]);

	// CODE
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_color);
	gl.bufferData(gl.ARRAY_BUFFER, rectnagleColor, gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_position);
	gl.bufferData(gl.ARRAY_BUFFER, rectangle, gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(gVao);
	gl.drawArrays(gl.LINES, 0, 2);
	gl.drawArrays(gl.LINES, 1, 2);
	gl.drawArrays(gl.LINES, 2, 2);
	gl.drawArrays(gl.LINES, 3, 2);
	gl.bindVertexArray(null);
}

function graph()
{
	// variabl declaration
	var distanceForX = 0, distanceForY = 0;
	var x = 4.0, y = 3.0;
	// code
	
	color[0] = 0.0;
	color[1] = 0.0;
	color[2] = 1.0;
	color[3] = 0.0;
	color[4] = 0.0;
	color[5] = 1.0;
	
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_color);
	gl.bufferData(gl.ARRAY_BUFFER, color, gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	for (var i = 0; i < 80; i++) {

		// verticle up
		vertices[0] = x;
		vertices[1] = distanceForX;
		vertices[2] = 0.0;

		vertices[3] = -x;
		vertices[4] = distanceForX;
		vertices[5] = 0.0;

		gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_position);
		gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.DYNAMIC_DRAW);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

		gl.bindVertexArray(gVao);
		gl.drawArrays(gl.LINES, 0, 2);
		gl.bindVertexArray(null);

		// verticle down
		vertices[0] = x;
		vertices[1] = -distanceForX;
		vertices[2] = 0.0;

		vertices[3] = -x;
		vertices[4] = -distanceForX;
		vertices[5] = 0.0;

		gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_position);
		gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.DYNAMIC_DRAW);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

		gl.bindVertexArray(gVao);
		gl.drawArrays(gl.LINES, 0, 2);
		gl.bindVertexArray(null);

		// horizontal right
		vertices[0] = -distanceForY;
		vertices[1] = y;
		vertices[2] = 0.0;

		vertices[3] = -distanceForY;
		vertices[4] = -y;
		vertices[5] = 0.0;

		gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_position);
		gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.DYNAMIC_DRAW);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

		gl.bindVertexArray(gVao);
		gl.drawArrays(gl.LINES, 0, 2);
		gl.bindVertexArray(null);

		// horizontal left
		vertices[0] = distanceForY;
		vertices[1] = y;
		vertices[2] = 0.0;

		vertices[3] = distanceForY;
		vertices[4] = -y;
		vertices[5] = 0.0;

		gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_position);
		gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.DYNAMIC_DRAW);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

		gl.bindVertexArray(gVao);
		gl.drawArrays(gl.LINES, 0, 2);
		gl.bindVertexArray(null);


		distanceForX += 1.0 / 15;
		distanceForY += 1.0 / 10;
	}

	color[0] = 0.0;
	color[1] = 1.0;
	color[2] = 0.0;
	color[3] = 0.0;
	color[4] = 1.0;
	color[5] = 0.0;

	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_color);
	gl.bufferData(gl.ARRAY_BUFFER, color, gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	// horizontal green line
	vertices[0] = x;
	vertices[1] = 0.0;
	vertices[2] = 0.0;

	vertices[3] = -x;
	vertices[4] =  0.0;
	vertices[5] =  0.0;

	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_position);
	gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(gVao);
	gl.drawArrays(gl.LINES, 0, 2);
	gl.bindVertexArray(null);

	// verticle green line
	vertices[0] = 0.0;
	vertices[1] = y;
	vertices[2] = 0.0;

	vertices[3] =  0.0;
	vertices[4] = -y;
	vertices[5] =  0.0;

	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_position);
	gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(gVao);
	gl.drawArrays(gl.LINES, 0, 2);
	gl.bindVertexArray(null);

	gl.bindVertexArray(null);
	
}

function uninitialize()
{
	if(gVao)
	{
		gl.deleteVertexArray(gVao);
		gVao = null;
	}
	
	if(gVbo_position)
	{
		gl.deleteBuffer(gVbo_position);
		gVbo_position = null;
	}
	
	if(gShaderProgramObject)
	{
		if(gFragmentShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gFragmentShaderObject);
			gl.deleteShader(gFragmentShaderObject);
			gFragmentShaderObject = null;
		}
		
		if(gVertexShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gVertexShaderObject);
			gl.deleteShader(gVertexShaderObject);
			gVertexShaderObject = null;
		}
		
		gl.deleteProgram(gShaderProgramObject);
		gShaderProgramObject = null;
	}
}
