// JavaScript source code
var canvas = null;
var gl;
var canvas_original_width = 0, canvas_original_height = 0;
var bFullScreen = false;
var requestAnimationFrame = window.requestAnimationFrame    || 
							window.webkitAnimationFrame     ||
							window.mozRequestAnimationFrame ||
							window.opRequestAnimationFrame  ||
							window.msRequestAnimationFrame	 ;

const webGLMacros =
{
	HRH_ATTRIBUTE_POSITION : 0 ,
    HRH_ATTRIBUTE_COLOR : 1 ,
    HRH_ATTRIBUTE_NORMAL : 2 ,
	HRH_ATTRIBUTE_TEXTURE_0 : 3 ,
}

var gVertexShaderObject;
var gFragmentShaderObject;
var gShaderProgramObject;

var gVao_cube;
var gVbo_cube_position;
var gVbo_cube_normals;

var gModelViewUniform;
var perspectiveProjectionUniform;
var lKeyPressedUniform;
var ldUniform;
var kdUniform;
var LightPossitionUniform;

var gbAnimate = false;
var gbLight = false;

var gPerspectiveProjectionMatrix;

var rotateAngle = 0.0;

function main()
{
	// get canvas from dom
    canvas = document.getElementById("hrh"); // type inferance

    if (!canvas) 
	{
        console.log("canvas failed");
    }
    else
    {
        console.log("Got canvas");
    }

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
    
	// window in buil variable , window is inherited from DOM object
	// 3rd parameter false means do bubbling
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);	
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize(); // explicit warm up resize because webgl don't call resize
	
	draw();
	
}

function keyDown(event)
{
	switch(event.keyCode)
	{
		// for 'f' or 'F'
		case 70:
			toggleFullScreen();
			break;
		case 27:
			uninitialize();
			window.close(); // may not work in firefox but will work in chrome and safari
			break;
	}
	
	switch(event.key)
	{
		case 'a':
		case 'A':
			if(gbAnimate == true)
			{
				gbAnimate = false;
			}
			else
			{
				gbAnimate = true;
			}
			break;
		case 'l':
		case 'L':
			if(gbLight == true)
			{
				gbLight = false;
			}
			else
			{
				gbLight = true;
			}
			break;
	}
}

function mouseDown()
{
	
}

function toggleFullScreen()
{
	var fullscreen_element = 	document.fullscreenElement || 
								document.webkitFulllscreenElement || 
								document.mozFullScreen || 
								document.msFullscreenElement ||
								null;
								
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
		{
			canvas.requestFullscreen();			
		} 
		else if(canvas.webkitRequestFulllscreen())
		{
			canvas.webkitRequestFulllscreen();
		}
		else if(canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		} 
		else if(canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		} 
		else if ( document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		} 
		else if (document.mozCancelFullScreen)
		{
			document.mozCancelFullScreen();
		} 
		else if (document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		bFullScreen = false;
	}	
}

function init()
{
	
    // get drawing context from the canvas
    gl = canvas.getContext("webgl2");

    if (!gl) 
	{
        console.log("gl failed");
    }
    else 
	{
        console.log("Got gl");
    }
	
	gVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	
	var vertexShaderSourceCode = 
			"#version 300 es \n" +
			"in vec4 v_position; " +
			"in vec3 v_normals; " +
			"uniform mat4 u_projection_matrix_uniform; " +
			"uniform mat4 u_model_view_matrix; " +
			"uniform int u_lKeyPressed;" +
			"uniform vec3 u_kd; " +
			"uniform vec3 u_ld; " +
			"uniform vec4 u_light_possition; " +
			"out vec3 diffuse_light; " +
			"void main() " +
			"{ " +
			"if(u_lKeyPressed == 1) " +
			"{ " +
				"vec4 eye_coordinates = u_model_view_matrix  * v_position;" +
				"mat3 normal_matrix = mat3( transpose (inverse ( u_model_view_matrix ) ) );" +
				"vec3 transformed_normal = normalize ( normal_matrix * v_normals) ;" +
				"vec3 source = normalize ( vec3  ( u_light_possition - eye_coordinates)) ;  " +
				"diffuse_light = u_ld * u_kd * max ( dot (source, transformed_normal), 0.0);  " +
			"}" +
			"gl_Position = u_projection_matrix_uniform * u_model_view_matrix * v_position;" +
			"}";

	gl.shaderSource(gVertexShaderObject, vertexShaderSourceCode);
	
	gl.compileShader(gVertexShaderObject);
	
	if(gl.getShaderParameter(gVertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
	var fragmentShaderSourceCode = 	
										"#version 300 es\n"+
										"precision highp float;" +
										"in vec3 diffuse_light; " +
										"uniform highp int u_lKeyPressed; "  +
										"out vec4 FragColor; " +
										"void main()" +
										"{" +
										"vec4 color; " +
										"if( u_lKeyPressed == 1) " +
										"{ " +
										"color = vec4(diffuse_light , 1.0); " +
										"} " +
										"else " +
										"{ " +
										"color = vec4(1.0, 1.0, 1.0, 1.0); " +
										"} " +
										"FragColor = color; " +
										"} " ;
	
	gl.shaderSource(gFragmentShaderObject, fragmentShaderSourceCode);
	
	gl.compileShader(gFragmentShaderObject);
	
	if(gl.getShaderParameter(gFragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gShaderProgramObject = gl.createProgram();
	
	// attach vertex shader to shader program
	gl.attachShader(gShaderProgramObject, gVertexShaderObject);
	
	// attch fragment shader to shader program
	gl.attachShader(gShaderProgramObject, gFragmentShaderObject);
	
	// pri-link binding of shader object with vertex shader possition attribute
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_NORMAL, "v_normals");

	// LINK PROGRAM
	gl.linkProgram(gShaderProgramObject);
	
	if(!gl.getProgramParameter(gShaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gModelViewUniform = gl.getUniformLocation(gShaderProgramObject, "u_model_view_matrix");
	perspectiveProjectionUniform = gl.getUniformLocation(gShaderProgramObject, "u_projection_matrix_uniform");
	lKeyPressedUniform = gl.getUniformLocation(gShaderProgramObject, "u_lKeyPressed");
	ldUniform = gl.getUniformLocation(gShaderProgramObject, "u_ld");
	kdUniform = gl.getUniformLocation(gShaderProgramObject, "u_kd");
	LightPossitionUniform = gl.getUniformLocation(gShaderProgramObject, "u_light_possition");
	
	// rectangle
	var cubeVertices = new Float32Array
		([
		 // FRONT
	  	 1.0,  1.0, 1.0,
		-1.0,  1.0, 1.0,
		-1.0, -1.0, 1.0,
		 1.0, -1.0, 1.0,

		// RIGHT
		1.0,  1.0, -1.0,
		1.0 , 1.0,  1.0,
		1.0, -1.0,  1.0,
		1.0, -1.0, -1.0,

		//BACK
		 1.0, -1.0, -1.0,
		-1.0, -1.0, -1.0,
		-1.0,  1.0, -1.0,
		 1.0,  1.0, -1.0,
		
		// LEFT
		-1.0,  1.0,  1.0,
		-1.0,  1.0, -1.0,
		-1.0, -1.0, -1.0,
		-1.0, -1.0,  1.0,

		// TOP
		 1.0, 1.0, -1.0,
		-1.0, 1.0, -1.0,
		-1.0, 1.0,  1.0,
		 1.0, 1.0,  1.0,

		// BOTTOM
	 	 1.0, -1.0,  1.0,
		-1.0, -1.0,  1.0,
		-1.0, -1.0, -1.0,
		 1.0, -1.0, -1.0,
		]);
	
	gVao_cube = gl.createVertexArray();
	gl.bindVertexArray(gVao_cube);
	
	gVbo_cube_position = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_cube_position);
	gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	var glNormal_coord = new Float32Array 
				([
				 0.0,  0.0,  1.0 , //front
				 0.0,  0.0,  1.0 , //front
				 0.0,  0.0,  1.0 , //front
				 0.0,  0.0,  1.0 , //front
								
				 1.0,  0.0,  0.0 , // right
				 1.0,  0.0,  0.0 , // right
				 1.0,  0.0,  0.0 , // right
				 1.0,  0.0,  0.0 , // right
								
				 0.0, 0.0,  -1.0 , //back
				 0.0, 0.0,  -1.0 , //back
				 0.0, 0.0,  -1.0 , //back
				 0.0, 0.0,  -1.0 , //back

				-1.0,  0.0, 0.0 , //left
				-1.0,  0.0, 0.0 , //left
				-1.0,  0.0, 0.0 , //left
				-1.0,  0.0, 0.0 , //left
							   
							   
				0.0,  1.0,  0.0 , // top
				0.0,  1.0,  0.0 , // top
				0.0,  1.0,  0.0 , // top
				0.0,  1.0,  0.0 , // top
							   
				0.0, -1.0,  0.0 , // bottom
				0.0, -1.0,  0.0 , // bottom
				0.0, -1.0,  0.0 , // bottom
				0.0, -1.0,  0.0 , // bottom

				]);

	gVbo_cube_normals = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_cube_normals);
	gl.bufferData(gl.ARRAY_BUFFER, glNormal_coord, gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_NORMAL);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);
	
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
//	gl.shadeModel(gl.SMOOTH);
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
//	gl.hint(gl.PERSPECTIVE_CORRECTION_HINT, gl.NICEST);
	
	
	gPerspectiveProjectionMatrix = mat4.create();
	
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	// ser the viewport to match	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(gPerspectiveProjectionMatrix, 
						45.0, 
						parseFloat(canvas.width) /parseFloat(canvas.height), 
						parseFloat(canvas.height) / parseFloat(canvas.width), 
						100.0); 
	
}

function draw()
{
	// variable declaration	
	
	
	// code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(gShaderProgramObject);
	
	// rectangle	
	var modelViewMatrix = mat4.create();
	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -6.0]);
	
	var rotateMatrix = mat4.create();
	
	mat4.rotateX(rotateMatrix, rotateMatrix, degToRad(rotateAngle));
	mat4.rotateY(rotateMatrix, rotateMatrix, degToRad(rotateAngle));
	mat4.rotateZ(rotateMatrix, rotateMatrix, degToRad(rotateAngle));
	// same function for scale
	
	mat4.multiply(modelViewMatrix, modelViewMatrix, rotateMatrix);
	
	var scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [0.9, 0.9, 0.9]);
	
	mat4.multiply(modelViewMatrix, modelViewMatrix, scaleMatrix);
		
	if (gbLight == true)
	{
		gl.uniform1i(lKeyPressedUniform, 1);
		gl.uniform3f(ldUniform, 1.0, 1.0, 1.0);
		gl.uniform3f(kdUniform, 0.5, 0.5, 0.5);
		var light_possition = new Float32Array ([0.0, 0.0, 2.0, 1.0]);
		gl.uniform4fv( LightPossitionUniform, light_possition );		
	}
	else
	{
		gl.uniform1i(lKeyPressedUniform, 0);
	}
	gl.uniformMatrix4fv(gModelViewUniform, false, modelViewMatrix);
	gl.uniformMatrix4fv(perspectiveProjectionUniform, false, gPerspectiveProjectionMatrix);
	
	gl.bindVertexArray(gVao_cube);
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
	gl.bindVertexArray(null);
	
	gl.useProgram(null);
	
	if(rotateAngle > 360.0)
	{
		rotateAngle = 0.0;
	}
	rotateAngle = rotateAngle + 0.1;
	
	requestAnimationFrame(draw, canvas);
}

function degToRad(degree)
{
	return ( degree * Math.PI / 180.0 );
}

function uninitialize()
{	
	// rectangle
	if(gVao_cube)
	{
		gl.deleteVertexArray(gVao_cube);
		gVao_cube = null;
	}
	
	if(gVbo_cube_position)
	{
		gl.deleteBuffer(gVbo_cube_position);
		gVbo_cube_position= null;
	}

	if(gVbo_cube_normals)
	{
		gl.deleteBuffer(gVbo_cube_normals);
		gVbo_cube_normals = null;
	}
	
	if(gShaderProgramObject)
	{
		if(gFragmentShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gFragmentShaderObject);
			gl.deleteShader(gFragmentShaderObject);
			gFragmentShaderObject = null;
		}
		
		if(gVertexShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gVertexShaderObject);
			gl.deleteShader(gVertexShaderObject);
			gVertexShaderObject = null;
		}
		
		gl.deleteProgram(gShaderProgramObject);
		gShaderProgramObject = null;
	}
}
