// JavaScript source code
var canvas = null;
var gl;
var canvas_original_width = 0, canvas_original_height = 0;
var bFullScreen = false;
var requestAnimationFrame = window.requestAnimationFrame    || 
							window.webkitAnimationFrame     ||
							window.mozRequestAnimationFrame ||
							window.opRequestAnimationFrame  ||
							window.msRequestAnimationFrame	 ;

const webGLMacros =
{
	HRH_ATTRIBUTE_POSITION : 0 ,
    HRH_ATTRIBUTE_COLOR : 1 ,
    HRH_ATTRIBUTE_NORMAL : 2 ,
	HRH_ATTRIBUTE_TEXTURE_0 : 3 ,
}

var gVertexShaderObject_1;
var gVertexShaderObject_2;

var gFragmentShaderObject_1;
var gFragmentShaderObject_2;

var gShaderProgramObject_1;
var gShaderProgramObject_2;

var gVao_cube;
var gVbo_cube_position;
var gVbo_cube_color;
var gVbo_cube_texcoord;
var gVbo_index;

var fbo;

var gMVUniform_1;
var gProjectionUniform_1;

var gMVUniform_2;
var gProjectionUniform_2;

var gTexCoordUniform;

var gPerspectiveProjectionMatrix;

var color_texture = 0, depth_texture = 0;

var rotateAngle = 0;

var w, h;

function main()
{
	// get canvas from dom
    canvas = document.getElementById("hrh"); // type inferance

    if (!canvas) 
	{
        console.log("canvas failed");
    }
    else
    {
        console.log("Got canvas");
    }

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
    
	// window in buil variable , window is inherited from DOM object
	// 3rd parameter false means do bubbling
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);	
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize(); // explicit warm up resize because webgl don't call resize
	
	draw();
	
}

function keyDown(event)
{
	switch(event.keyCode)
	{
		// for 'f' or 'F'
		case 70:
			toggleFullScreen();
			break;
		case 27:
			uninitialize();
			window.close(); // may not work in firefox but will work in chrome and safari
			break;
	}
}

function mouseDown()
{
	
}

function toggleFullScreen()
{
	var fullscreen_element = 	document.fullscreenElement || 
								document.webkitFulllscreenElement || 
								document.mozFullScreen || 
								document.msFullscreenElement ||
								null;
								
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
		{
			canvas.requestFullscreen();			
		} 
		else if(canvas.webkitRequestFulllscreen())
		{
			canvas.webkitRequestFulllscreen();
		}
		else if(canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		} 
		else if(canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		} 
		else if ( document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		} 
		else if (document.mozCancelFullScreen)
		{
			document.mozCancelFullScreen();
		} 
		else if (document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		bFullScreen = false;
	}	
}

function init()
{
	
    // get drawing context from the canvas
    gl = canvas.getContext("webgl2");

    if (!gl) 
	{
        console.log("gl failed");
    }
    else 
	{
        console.log("Got gl");
    }
	
	gVertexShaderObject_1 = gl.createShader(gl.VERTEX_SHADER);
	
	var vertexShaderSourceCode = 
		" #version 300 es													\n" +
		" precision highp float; 											\n" +
		" in vec4 vPosition;												\n" +
		" in vec2 texcoord;													\n" +
		" uniform mat4 u_mvMatrix;											\n" +
		" uniform mat4 u_projectionMatrix;									\n" +
		" out vec4 color_out;												\n" +
		" out vec2 texcoord_out;											\n" +
		" void main()														\n" +
		" 																	\n" +
		" {																	\n" +
		" gl_Position = u_projectionMatrix * u_mvMatrix * vPosition;		\n" +
		" color_out = vPosition * 2.0 + vec4(0.5, 0.5, 0.5, 0.0);			\n" +
		" texcoord_out = texcoord;											\n" +
		" }";

	gl.shaderSource(gVertexShaderObject_1, vertexShaderSourceCode);
	
	gl.compileShader(gVertexShaderObject_1);
	
	if(gl.getShaderParameter(gVertexShaderObject_1, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObject_1);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	console.log("Vertex Shader passed");
	
	gFragmentShaderObject_1 = gl.createShader(gl.FRAGMENT_SHADER);
	
	var fragmentShaderSourceCode_1 = 
	" #version 300 es	\n														  " +
	" precision highp float; 													\n" +
	" out vec4 color; 															\n" +
	" in vec4 color_out;														\n" +
	" in vec2 texcoord_out;														\n" +
	" void main()																\n" +
	" {																			\n" +
	" color = sin(color_out * vec4(40.0, 20.0, 30.0, 1.0)) * 0.5 + vec4(0.5);	\n" +
	" }";
	
	gl.shaderSource(gFragmentShaderObject_1, fragmentShaderSourceCode_1);
	
	gl.compileShader(gFragmentShaderObject_1);
	
	if(gl.getShaderParameter(gFragmentShaderObject_1, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObject_1);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	console.log("Fragment Shader 1 passed");
	
	gShaderProgramObject_1 = gl.createProgram();
	
	// attach vertex shader to shader program
	gl.attachShader(gShaderProgramObject_1, gVertexShaderObject_1);
	
	// attch fragment shader to shader program
	gl.attachShader(gShaderProgramObject_1, gFragmentShaderObject_1);
	
	// pri-link binding of shader object with vertex shader possition attribute
	gl.bindAttribLocation(gShaderProgramObject_1, webGLMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(gShaderProgramObject_1, webGLMacros.HRH_ATTRIBUTE_TEXTURE_0, "texcoord");

	// LINK PROGRAM
	gl.linkProgram(gShaderProgramObject_1);
	
	if(!gl.getProgramParameter(gShaderProgramObject_1, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObject_1);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
		
	gFragmentShaderObject_2 = gl.createShader(gl.FRAGMENT_SHADER);
	
	var fragmentShaderSourceCode_2 = 
	" #version 300 es	\n														  " +
	" precision highp float; 													\n" +
	" uniform sampler2D tex;													\n" +
	" out vec4 color;															\n" +
	" in vec4 color_out;														\n" +
	" in vec2 texcoord_out;														\n" +
	" void main()																\n" +
	" {																			\n" +
	" 	color = mix(color_out, texture ( tex, texcoord_out), 0.7);				\n" +
	" }																			\n" ;
	
	gl.shaderSource(gFragmentShaderObject_2, fragmentShaderSourceCode_2);
	
	gl.compileShader(gFragmentShaderObject_2);
	
	if(gl.getShaderParameter(gFragmentShaderObject_2, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObject_2);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	console.log("Fragment Shader 2 passed");
	
	gShaderProgramObject_2 = gl.createProgram();
	
	// attach vertex shader to shader program
	gl.attachShader(gShaderProgramObject_2, gVertexShaderObject_1);
	
	// attch fragment shader to shader program
	gl.attachShader(gShaderProgramObject_2, gFragmentShaderObject_2);
	
	// pri-link binding of shader object with vertex shader possition attribute
	gl.bindAttribLocation(gShaderProgramObject_2, webGLMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(gShaderProgramObject_2, webGLMacros.HRH_ATTRIBUTE_TEXTURE_0, "texcoord");

	// LINK PROGRAM
	gl.linkProgram(gShaderProgramObject_2);
	
	if(!gl.getProgramParameter(gShaderProgramObject_2, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObject_2);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	gMVUniform_1 = gl.getUniformLocation(gShaderProgramObject_1, "u_mvMatrix");
	gProjectionUniform_1 = gl.getUniformLocation(gShaderProgramObject_1, "u_projectionMatrix");

	gMVUniform_2 = gl.getUniformLocation(gShaderProgramObject_2, "u_mvMatrix");
	gProjectionUniform_2 = gl.getUniformLocation(gShaderProgramObject_2, "u_projectionMatrix");

	// rectangle
	var cubeVertices = new Float32Array
		([
		 	1.0,  1.0, 1.0,
		-1.0,  1.0, 1.0,
		-1.0, -1.0, 1.0,
		 1.0, -1.0, 1.0,

		// RIGHT
		1.0,  1.0, -1.0,
		1.0 , 1.0,  1.0,
		1.0, -1.0,  1.0,
		1.0, -1.0, -1.0,

		//BACK
		 1.0, -1.0, -1.0,
		-1.0, -1.0, -1.0,
		-1.0,  1.0, -1.0,
		 1.0,  1.0, -1.0,
		
		// LEFT
		-1.0,  1.0,  1.0,
		-1.0,  1.0, -1.0,
		-1.0, -1.0, -1.0,
		-1.0, -1.0,  1.0,

		// TOP
		 1.0, 1.0, -1.0,
		-1.0, 1.0, -1.0,
		-1.0, 1.0,  1.0,
		 1.0, 1.0,  1.0,

		// BOTTOM
	 	 1.0, -1.0,  1.0,
		-1.0, -1.0,  1.0,
		-1.0, -1.0, -1.0,
		 1.0, -1.0, -1.0,
		]);

	var cubeTexcoord = new Float32Array
	([
		// 1
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
				
		//2     
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
				
		//3     
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
				
		//4     
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
				
		//5     
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
				
		//6     
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0,

	]);
	
	gVao_cube = gl.createVertexArray();
	gl.bindVertexArray(gVao_cube);
	
	gVbo_cube_position = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_cube_position);
	gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gVbo_cube_texcoord = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_cube_texcoord);
	gl.bufferData(gl.ARRAY_BUFFER, cubeTexcoord, gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_TEXTURE_0, 2, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_TEXTURE_0);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	//gl.bindVertexArray(null);
	
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
//	gl.shadeModel(gl.SMOOTH);
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

	fbo = gl.createFramebuffer();
	gl.bindFramebuffer(gl.FRAMEBUFFER, fbo);	

	color_texture = gl.createTexture();
	gl.bindTexture(gl.TEXTURE_2D, color_texture);

	gl.texStorage2D(gl.TEXTURE_2D, 1, gl.RGBA8, 1024, 1024);

	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);

	gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, color_texture, 0);

	depth_texture = gl.createRenderbuffer();
	gl.bindRenderbuffer(gl.RENDERBUFFER, depth_texture);

	gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT24, 1024, 1024);
	
	gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT,  gl.RENDERBUFFER, depth_texture);

	var draw_buffers = [ gl.COLOR_ATTACHMENT0 ];
	gl.drawBuffers( draw_buffers);

	gl.bindFramebuffer(gl.FRAMEBUFFER, null);

	gPerspectiveProjectionMatrix = mat4.create();
	
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	// ser the viewport to match	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(gPerspectiveProjectionMatrix, 
						45.0, 
						parseFloat(canvas.width) /parseFloat(canvas.height), 
						parseFloat(canvas.height) / parseFloat(canvas.width), 
						100.0); 
	w = canvas.width;
	h = canvas.height;
	
}

function draw()
{
	// code	
	var modelViewMatrix = mat4.create();
	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -6.0]);

	gl.bindFramebuffer(gl.FRAMEBUFFER, fbo);

	gl.viewport(0, 0, canvas.width, canvas.height);

	gl.clearColor(0.0, 1.0, 0.0, 1.0);
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.clearBufferfi(gl.DEPTH_STENCIL, 0, 1.0, 0);

	gl.useProgram(gShaderProgramObject_1);

	var rotateMatrix = mat4.create();

	mat4.rotateX(rotateMatrix, rotateMatrix, degToRad(rotateAngle));
	mat4.rotateY(rotateMatrix, rotateMatrix, degToRad(rotateAngle));
	mat4.rotateZ(rotateMatrix, rotateMatrix, degToRad(rotateAngle));
	// same function for scale

	mat4.multiply(modelViewMatrix, modelViewMatrix, rotateMatrix);

	modelViewProjectionMatrix = mat4.create();
	mat4.multiply(modelViewProjectionMatrix, gPerspectiveProjectionMatrix, modelViewMatrix);

	gl.uniformMatrix4fv(gMVUniform_1, false, modelViewMatrix);
	gl.uniformMatrix4fv(gProjectionUniform_1, false, gPerspectiveProjectionMatrix);

	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);

	gl.bindFramebuffer(gl.FRAMEBUFFER, null);

	gl.viewport(0, 0, w, h);

	gl.clearColor(0.0, 0.0, 0.3, 1.0);
	gl.clearDepth(1.0);

	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	gl.bindTexture(gl.TEXTURE_2D, color_texture);

	gl.useProgram(gShaderProgramObject_2);

	gl.uniformMatrix4fv(gMVUniform_2, false, modelViewMatrix);
	gl.uniformMatrix4fv(gProjectionUniform_2, false, gPerspectiveProjectionMatrix);

	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);

	gl.useProgram(null);
	
	rotateAngle = rotateAngle + 0.1;

	if(rotateAngle > 360.0)
	{
		rotateAngle = 0.0;
	}

	requestAnimationFrame(draw, canvas);
}

function degToRad(degree)
{
	return ( degree * Math.PI / 180.0 );
}

function uninitialize()
{	
	// rectangle
	if(gVao_cube)
	{
		gl.deleteVertexArray(gVao_cube);
		gVao_cube = null;
	}
	
	if(gVbo_cube_position)
	{
		gl.deleteBuffer(gVbo_cube_position);
		gVbo_cube_position= null;
	}

	if(gVbo_cube_color)
	{
		gl.deleteBuffer(gVbo_cube_color);
		gVbo_cube_color = null;
	}
	
	if(gShaderProgramObject_1)
	{
		if(gFragmentShaderObject_1)
		{
			gl.detachShader(gShaderProgramObject_1, gFragmentShaderObject_1);
			gl.deleteShader(gFragmentShaderObject_1);
			gFragmentShaderObject_1 = null;
		}
		
		if(gVertexShaderObject_1)
		{
			gl.detachShader(gShaderProgramObject_1, gVertexShaderObject_1);
			gl.deleteShader(gVertexShaderObject_1);
			gVertexShaderObject_1 = null;
		}
		
		gl.deleteProgram(gShaderProgramObject_1);
		gShaderProgramObject_1 = null;
	}

	if(gShaderProgramObject_2)
	{
		if(gFragmentShaderObject_2)
		{
			gl.detachShader(gShaderProgramObject_2, gFragmentShaderObject_2);
			gl.deleteShader(gFragmentShaderObject_2);
			gFragmentShaderObject_2 = null;
		}
				
		gl.deleteProgram(gShaderProgramObject_2);
		gShaderProgramObject_2 = null;
	}
}
