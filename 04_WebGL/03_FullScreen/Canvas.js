// JavaScript source code
var canvas = null;
var context

function main(){
    // get canvas from dom
    canvas = document.getElementById("hrh"); // type inferance

    if (!canvas) {
        console.log("canvas failed");
    }
    else
    {
        console.log("Got canvas");
    }

    // retrieve the hight and width of canvas
    // positional arg
    console.log("Canvas width = " + canvas.width + " height = " + canvas.height + "\n");

    // get drawing context from the canvas
    context = canvas.getContext("2d");

    if (!context) {
        console.log("context failed");
    }
    else {
        console.log("Got context");
    }

    // paint background by black color

    context.fillStyle = "black";

    context.fillRect(0, 0, canvas.width, canvas.height);
	
	drawText();
	
	// window in buil variable , window is inherited from DOM object
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);	
}

function drawText()
{
	// center the text
    context.textAlign = "center"; // horizontal center
    context.textBaseline = "middle"; // verticle center
    context.font = "48px Arial";

    // declare string to be displayed
    var str = "Hello Hrituja Hedau";

    // color the text foreground
    context.fillStyle = "white";

    // display the text
    context.fillText(str, canvas.width / 2, canvas.height / 2);
}

function keyDown(event)
{
	switch(event.key)
	{
		case 'f':
		case 'F':
			toggleFullScreen();
			drawText();
			break;
	}
}

function mouseDown()
{
	
}

function toggleFullScreen()
{
	var fullscreen_element = 	document.fullscreenElement || 
								document.webkitFulllscreenElement || 
								document.mozFullScreen || 
								document.msFullscreenElement ||
								null;
								
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
		{
			canvas.requestFullscreen();			
		} else if(canvas.webkitRequestFulllscreen())
		{
			canvas.webkitRequestFulllscreen();
		}
		else if(canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		} else if(canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		} else if ( document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		} else if (document.mozCancelFullScreen)
		{
			document.mozCancelFullScreen();
		} else if (document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
	}
	
}