// JavaScript source code
var canvas = null;
var gl;
var canvas_original_width = 0, canvas_original_height = 0;
var bFullScreen = false;
var requestAnimationFrame = window.requestAnimationFrame    || 
							window.webkitAnimationFrame     ||
							window.mozRequestAnimationFrame ||
							window.opRequestAnimationFrame  ||
							window.msRequestAnimationFrame	 ;

const webGLMacros =
{
	HRH_ATTRIBUTE_POSITION : 0 ,
    HRH_ATTRIBUTE_COLOR : 1 ,
    HRH_ATTRIBUTE_NORMAL : 2 ,
	HRH_ATTRIBUTE_TEXTURE_0 : 3 ,
}

var gVertexShaderObject;
var gFragmentShaderObject;
var gShaderProgramObject;

var gVao_pyramid;
var gVbo_pyramid_position;
var gVbo_pyramid_texcoord;

var gVao_cube;
var gVbo_cube_position;
var gVbo_cube_texcoord;

var gMVPUniform;
var gTextureSamplerUniform = 0;

var gPerspectiveProjectionMatrix;

var rotateAngle = 0.0;

var stone_texture = 0, kundali_texture = 0;

function main()
{
	// get canvas from dom
    canvas = document.getElementById("hrh"); // type inferance

    if (!canvas) 
	{
        console.log("canvas failed");
    }
    else
    {
        console.log("Got canvas");
    }

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
    
	// window in buil variable , window is inherited from DOM object
	// 3rd parameter false means do bubbling
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);	
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize(); // explicit warm up resize because webgl don't call resize
	
	draw();
	
}

function keyDown(event)
{
	switch(event.keyCode)
	{
		// for 'f' or 'F'
		case 70:
			toggleFullScreen();
			break;
		case 27:
			uninitialize();
			window.close(); // may not work in firefox but will work in chrome and safari
			break;
	}
}

function mouseDown()
{
	
}

function toggleFullScreen()
{
	var fullscreen_element = 	document.fullscreenElement || 
								document.webkitFulllscreenElement || 
								document.mozFullScreen || 
								document.msFullscreenElement ||
								null;
								
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
		{
			canvas.requestFullscreen();			
		} 
		else if(canvas.webkitRequestFulllscreen())
		{
			canvas.webkitRequestFulllscreen();
		}
		else if(canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		} 
		else if(canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		} 
		else if ( document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		} 
		else if (document.mozCancelFullScreen)
		{
			document.mozCancelFullScreen();
		} 
		else if (document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		bFullScreen = false;
	}	
}

function init()
{
	
    // get drawing context from the canvas
    gl = canvas.getContext("webgl2");

    if (!gl) 
	{
        console.log("gl failed");
    }
    else 
	{
        console.log("Got gl");
    }
	
	gVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	
	var vertexShaderSourceCode = 
								"#version 300 es \n" +
								"in vec4 vPosition;" +
								"in  vec2  vTexcoord;" +
								"out vec2  outTexcoord;"+
								"uniform mat4 u_mvpMatrix;" +
								"void main()" +
								"{" +
								"gl_Position = u_mvpMatrix * vPosition;" +
								"outTexcoord = vTexcoord;" +
								"}";

	gl.shaderSource(gVertexShaderObject, vertexShaderSourceCode);
	
	gl.compileShader(gVertexShaderObject);
	
	if(gl.getShaderParameter(gVertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
	var fragmentShaderSourceCode = 	"#version 300 es\n"+
									"precision highp float;" +
									"in vec2 outTexcoord;" +
									"uniform highp sampler2D u_texture;" +
									"out vec4 FragColor; " +
									"void main()"+
									"{" +
									"FragColor = texture(u_texture, outTexcoord);" +
									"}";
	
	gl.shaderSource(gFragmentShaderObject, fragmentShaderSourceCode);
	
	gl.compileShader(gFragmentShaderObject);
	
	if(gl.getShaderParameter(gFragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gShaderProgramObject = gl.createProgram();
	
	// attach vertex shader to shader program
	gl.attachShader(gShaderProgramObject, gVertexShaderObject);
	
	// attch fragment shader to shader program
	gl.attachShader(gShaderProgramObject, gFragmentShaderObject);
	
	// pri-link binding of shader object with vertex shader possition attribute
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_TEXTURE_0, "vTexcoord");

	// LINK PROGRAM
	gl.linkProgram(gShaderProgramObject);
	
	if(!gl.getProgramParameter(gShaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gMVPUniform = gl.getUniformLocation(gShaderProgramObject, "u_mvpMatrix");
	gTextureSamplerUniform = gl.getUniformLocation(gShaderProgramObject, "u_texture");
	
	// triangle 
	var pyramidVertices = new Float32Array
		([
			 0.0, 1.0, 0.0,
			-1.0,-1.0, 1.0,
			 1.0, -1.0, 1.0,
			
			0.0,  1.0,  0.0,
			1.0, -1.0,  1.0,
			1.0, -1.0, -1.0,
			
			 0.0,  1.0, 0.0,
			 1.0, -1.0,-1.0,
			-1.0, -1.0,-1.0,
			
			 0.0,  1.0,  0.0,
			-1.0, -1.0, -1.0,
			-1.0, -1.0,  1.0,
		]);
	
	gVao_pyramid = gl.createVertexArray();
	gl.bindVertexArray(gVao_pyramid);
	
	gVbo_pyramid_position = gl.createBuffer()
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_pyramid_position);
	gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_POSITION);

	var pyramidTexcoord = new Float32Array
		([
			//1
			0.5, 1.0,
			0.0, 0.0,
			1.0, 0.0,
				 
			//2     
			0.5, 1.0,
			1.0, 0.0,
			0.0, 0.0,
				 
				 
			//3     
			0.5, 1.0,
			0.0, 0.0,
			1.0, 0.0,
				 
				 
			//4     
			0.5, 1.0,
			1.0, 0.0,
			0.0, 0.0,

		]);
		
	gVbo_pyramid_color = gl.createBuffer()
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_pyramid_color);
	gl.bufferData(gl.ARRAY_BUFFER, pyramidTexcoord, gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_TEXTURE_0, 2, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_TEXTURE_0);

	
	gl.bindBuffer(gl.ARRAY_BUFFER, null);	
	gl.bindVertexArray(null);	

	// rectangle
	var cubeVertices = new Float32Array
		([
		 // FRONT
	  	 1.0,  1.0, 1.0,
		-1.0,  1.0, 1.0,
		-1.0, -1.0, 1.0,
		 1.0, -1.0, 1.0,

		// RIGHT
		1.0,  1.0, -1.0,
		1.0 , 1.0,  1.0,
		1.0, -1.0,  1.0,
		1.0, -1.0, -1.0,

		//BACK
		 1.0, -1.0, -1.0,
		-1.0, -1.0, -1.0,
		-1.0,  1.0, -1.0,
		 1.0,  1.0, -1.0,
		
		// LEFT
		-1.0,  1.0,  1.0,
		-1.0,  1.0, -1.0,
		-1.0, -1.0, -1.0,
		-1.0, -1.0,  1.0,

		// TOP
		 1.0, 1.0, -1.0,
		-1.0, 1.0, -1.0,
		-1.0, 1.0,  1.0,
		 1.0, 1.0,  1.0,

		// BOTTOM
	 	 1.0, -1.0,  1.0,
		-1.0, -1.0,  1.0,
		-1.0, -1.0, -1.0,
		 1.0, -1.0, -1.0,
		]);
	
	gVao_cube = gl.createVertexArray();
	gl.bindVertexArray(gVao_cube);
	
	gVbo_cube_position = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_cube_position);
	gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	var cubeTexcoord = new Float32Array
	([
		// 1
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
				
		//2     
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
				
		//3     
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
				
		//4     
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
				
		//5     
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
				
		//6     
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0,

	]);

	gVbo_cube_texcoord = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_cube_texcoord);
	gl.bufferData(gl.ARRAY_BUFFER, cubeTexcoord, gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_TEXTURE_0, 2, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_TEXTURE_0);	
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.bindVertexArray(null);
	
	stone_texture = gl.createTexture();
	stone_texture.image = new Image();
	stone_texture.image.scr = "stone.png";
	
	stone_texture.image.onload = function (){
					gl.bindTexture(gl.TEXTURE_2D, stone_texture);
					gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
					gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
					gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST_MIPMAP_NEAREST);
					// 0 - MIPMAP LEVEL 0
					gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.UNSINGED_BYTE, stone_texture.image);
					gl.generateMipMap(gl.TEXTURE_2D);
					gl.bindTexture(gl.TEXTURE_2D, null);					
				};
	
	//gl.enable(gl.TEXTURE_2D);
	
	kundali_texture = gl.createTexture();
	kundali_texture.image = new Image();
	kundali_texture.image.scr = "Vijay_Kundali.png";
	
	kundali_texture.image.onload = function (){
					gl.bindTexture(gl.TEXTURE_2D, kundali_texture);
					gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
					//gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
					//gl.textureParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST_MIPMAP_NEAREST);
					gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
					gl.textureParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LEANER);
					// 0 - MIPMAP LEVEL 0
					gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.UNSINGED_BYTE, kundali_texture.image);
					gl.bindTexture(gl.TEXTURE_2D, null);
				};
	
	//gl.enable(gl.TEXTURE_2D);
	
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	gl.clearColor(0.0, 0.0, 1.0, 1.0);
	
//	gl.shadeModel(gl.SMOOTH);
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
//	gl.hint(gl.PERSPECTIVE_CORRECTION_HINT, gl.NICEST);	
	
	gPerspectiveProjectionMatrix = mat4.create();
	
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	// ser the viewport to match	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(gPerspectiveProjectionMatrix, 
						45.0, 
						parseFloat(canvas.width) /parseFloat(canvas.height), 
						parseFloat(canvas.height) / parseFloat(canvas.width), 
						100.0); 
	
}

function draw()
{
	// variable declaration	
	
	// code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT );
	
	gl.useProgram(gShaderProgramObject);
	
	// triangle	
	var modelViewMatrix = mat4.create();
	mat4.translate(modelViewMatrix, modelViewMatrix, [-1.5, 0.0, -6.0]);
	
	
	var rotateMatrix = mat4.create();
	//mat4.rotate(rotateMatrix, rotateMatrix, rotateAngle , [0.0, 1.0, 0.0]);
	mat4.rotateY(rotateMatrix, rotateMatrix, degToRad(rotateAngle));
	
	mat4.multiply(modelViewMatrix, modelViewMatrix, rotateMatrix);
	
	var modelViewProjectionMatrix = mat4.create();	
	mat4.multiply(modelViewProjectionMatrix, gPerspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMVPUniform, false, modelViewProjectionMatrix);
	
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, stone_texture);
	gl.uniform1i(gTextureSamplerUniform, 0);
	
	gl.bindVertexArray(gVao_pyramid);
	gl.drawArrays(gl.TRIANGLES, 0, 12); 
	gl.bindVertexArray(null);
	
	// rectangle
	modelViewMatrix = mat4.create();
	mat4.translate(modelViewMatrix, modelViewMatrix, [1.5, 0.0, -6.0]);
	
	rotateMatrix = mat4.create();
	//mat4.rotate(rotateMatrix, rotateMatrix, rotateAngle , [1.0, 0.0, 0.0]);
	
	mat4.rotateX(rotateMatrix, rotateMatrix, degToRad(rotateAngle));
	mat4.rotateY(rotateMatrix, rotateMatrix, degToRad(rotateAngle));
	mat4.rotateZ(rotateMatrix, rotateMatrix, degToRad(rotateAngle));
	// same function for scale
	
	mat4.multiply(modelViewMatrix, modelViewMatrix, rotateMatrix);
	
	var scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [0.9, 0.9, 0.9]);
	
	mat4.multiply(modelViewMatrix, modelViewMatrix, scaleMatrix);
	
	modelViewProjectionMatrix = mat4.create();	
	mat4.multiply(modelViewProjectionMatrix, gPerspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMVPUniform, false, modelViewProjectionMatrix);
	
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, kundali_texture);
	gl.uniform1i(gTextureSamplerUniform, 0);
	
	gl.bindVertexArray(gVao_cube);
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
	gl.bindVertexArray(null);
	
	gl.useProgram(null);
	
	if(rotateAngle > 360.0)
	{
		rotateAngle = 0.0;
	}
	rotateAngle = rotateAngle + 0.1;
	
	requestAnimationFrame(draw, canvas);
}

function degToRad(degree)
{
	return ( degree * Math.PI / 180.0 );
}

function uninitialize()
{
	if(stone_texture)
	{
		gl.deleteTexture(stone_texture);
		stone_texture = 0;
	}
	
	if(kundali_texture)
	{
		gl.deleteTexture(kundali_texture);
		kundali_texture = 0;
	}
	
	// triangle
	if(gVao_pyramid)
	{
		gl.deleteVertexArray(gVao_triangle);
		gVao_triangle = null;
	}
	
	if(gVbo_pyramid_position)
	{
		gl.deleteBuffer(gVbo_pyramid_position);
		gVbo_pyramid_position = null;
	}

	if(gVbo_pyramid_texcoord)
	{
		gl.deleteBuffer(gVbo_pyramid_texcoord);
		gVbo_pyramid_texcoord = null;
	}
	
	// rectangle
	if(gVao_cube)
	{
		gl.deleteVertexArray(gVao_cube);
		gVao_cube = null;
	}
	
	if(gVbo_cube_position)
	{
		gl.deleteBuffer(gVbo_cube_position);
		gVbo_cube_position= null;
	}

	if(gVbo_cube_texcoord)
	{
		gl.deleteBuffer(gVbo_cube_texcoord);
		gVbo_cube_texcoord = null;
	}
	
	if(gShaderProgramObject)
	{
		if(gFragmentShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gFragmentShaderObject);
			gl.deleteShader(gFragmentShaderObject);
			gFragmentShaderObject = null;
		}
		
		if(gVertexShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gVertexShaderObject);
			gl.deleteShader(gVertexShaderObject);
			gVertexShaderObject = null;
		}
		
		gl.deleteProgram(gShaderProgramObject);
		gShaderProgramObject = null;
	}
}
