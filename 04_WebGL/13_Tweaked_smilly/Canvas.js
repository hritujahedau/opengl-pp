// JavaScript source code
var canvas = null;
var gl;
var canvas_original_width = 0, canvas_original_height = 0;
var bFullScreen = false;
var requestAnimationFrame = window.requestAnimationFrame    || 
							window.webkitAnimationFrame     ||
							window.mozRequestAnimationFrame ||
							window.opRequestAnimationFrame  ||
							window.msRequestAnimationFrame	 ;

const webGLMacros =
{
	HRH_ATTRIBUTE_POSITION : 0 ,
    HRH_ATTRIBUTE_COLOR : 1 ,
    HRH_ATTRIBUTE_NORMAL : 2 ,
	HRH_ATTRIBUTE_TEXTURE_0 : 3 ,
}

var gVertexShaderObject;
var gFragmentShaderObject;
var gShaderProgramObject;

var gVao_rectangle;
var gVbo_rectangle_position;
var gVbo_rectangle_texcoord;

var gMVPUniform;
var gTextureSamplerUniform = 0;
var isColorUniform;

var gPerspectiveProjectionMatrix;

var rotateAngle = 0.0;

var smilley_texture;

var fSqaureTexcord_0 = new Float32Array([ 1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0 ]);
var fSqaureTexcord_1  = new Float32Array([ 0.5, 0.5, 0.0, 0.5, 0.0, 0.0, 0.5, 0.0 ]);
var fSqaureTexcord_2  = new Float32Array([ 2.0, 2.0, 0.0, 2.0, 0.0, 0.0, 2.0, 0.0 ]);
var fSqaureTexcord_3  = new Float32Array([ 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5 ]);

var isColor = 1;

rectangleTexcoord = fSqaureTexcord_0;

function main()
{
	// get canvas from dom
    canvas = document.getElementById("hrh"); // type inferance

    if (!canvas) 
	{
        console.log("canvas failed");
    }
    else
    {
        console.log("Got canvas");
    }

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
    
	// window in buil variable , window is inherited from DOM object
	// 3rd parameter false means do bubbling
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);	
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize(); // explicit warm up resize because webgl don't call resize
	
	draw();
	
}

function keyDown(event)
{
	switch(event.keyCode)
	{
		// for 'f' or 'F'
		case 70:
			toggleFullScreen();
			break;
		case 27:
			uninitialize();
			window.close(); // may not work in firefox but will work in chrome and safari
			break;
	}
	switch(event.key)
	{
		case '0':
			rectangleTexcoord = fSqaureTexcord_0;
			isColor = 0;
			break;
		case '1':
			rectangleTexcoord = fSqaureTexcord_1;
			isColor = 0;
			break;
		case '2':
			rectangleTexcoord = fSqaureTexcord_2;
			isColor = 0;
			break;
		case '3':
			rectangleTexcoord = fSqaureTexcord_3;
			isColor = 0;
			break;
		case '4':
			isColor = 1;
			break;
	}
}

function mouseDown()
{
	
}

function toggleFullScreen()
{
	var fullscreen_element = 	document.fullscreenElement || 
								document.webkitFulllscreenElement || 
								document.mozFullScreen || 
								document.msFullscreenElement ||
								null;
								
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
		{
			canvas.requestFullscreen();			
		} 
		else if(canvas.webkitRequestFulllscreen())
		{
			canvas.webkitRequestFulllscreen();
		}
		else if(canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		} 
		else if(canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		} 
		else if ( document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		} 
		else if (document.mozCancelFullScreen)
		{
			document.mozCancelFullScreen();
		} 
		else if (document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		bFullScreen = false;
	}
}

function init()
{
	
    // get drawing context from the canvas
    gl = canvas.getContext("webgl2");

    if (!gl) 
	{
        console.log("gl failed");
    }
    else 
	{
        console.log("Got gl");
    }
	
	gVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	
	var vertexShaderSourceCode = 
									"#version 300 es \n" +
									"in vec4 vPosition;" +
									"in vec2 vTexCoord;" +
									"out vec2 outTexCoord; " +
									"uniform mat4 u_mvpMatrix;" +
									"void main()" +
									"{" +
									"outTexCoord = vTexCoord;"  +
									"gl_Position = u_mvpMatrix * vPosition;" +
									"}";

	gl.shaderSource(gVertexShaderObject, vertexShaderSourceCode);
	
	gl.compileShader(gVertexShaderObject);
	
	if(gl.getShaderParameter(gVertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
	var fragmentShaderSourceCode = 	
									" #version 300 es 											 \n	" +
									" precision highp float;										" +
									" in vec2 outTexCoord;											" +
									" out vec4 FragColor; 											" +
									" uniform int isColor;											" +
									" uniform highp sampler2D u_texture_sampler;					" +
									" void main()													" +
									" {																" +
									" 	if(isColor == 1) 											" +
									" 	{															" +
									" 		FragColor = vec4(1.0, 1.0, 1.0, 1.0);					" +
									" 	}															" +
									" 	else 														" +
									" 	{ 															" +
									" 		FragColor = texture(u_texture_sampler,outTexCoord) ;	" +
									" 	} 															" +
									" }																" ;
	
	gl.shaderSource(gFragmentShaderObject, fragmentShaderSourceCode);
	
	gl.compileShader(gFragmentShaderObject);
	
	if(gl.getShaderParameter(gFragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gShaderProgramObject = gl.createProgram();
	
	// attach vertex shader to shader program
	gl.attachShader(gShaderProgramObject, gVertexShaderObject);
	
	// attch fragment shader to shader program
	gl.attachShader(gShaderProgramObject, gFragmentShaderObject);
	
	// pri-link binding of shader object with vertex shader possition attribute
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_TEXTURE_0, "vTexCoord");

	// LINK PROGRAM
	gl.linkProgram(gShaderProgramObject);
	
	if(!gl.getProgramParameter(gShaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gMVPUniform = gl.getUniformLocation(gShaderProgramObject, "u_mvpMatrix");
	gTextureSamplerUniform = gl.getUniformLocation(gShaderProgramObject, "u_texture_sampler");
	isColorUniform = gl.getUniformLocation(gShaderProgramObject, "isColor");
	
	// rectangle
	var rectangleVertices = new Float32Array
		([
		 // FRONT
	  	 1.0,  1.0, 1.0,
		-1.0,  1.0, 1.0,
		-1.0, -1.0, 1.0,
		 1.0, -1.0, 1.0,
		]);
	
	gVao_rectangle = gl.createVertexArray();
	gl.bindVertexArray(gVao_rectangle);
	
	gVbo_rectangle_position = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_rectangle_position);
	gl.bufferData(gl.ARRAY_BUFFER, rectangleVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	
	gVbo_rectangle_texcoord = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_rectangle_texcoord);
	gl.bufferData(gl.ARRAY_BUFFER, null, gl.DYNAMIC_DRAW);	
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.bindVertexArray(null);
	
	smilley_texture = gl.createTexture();
	smilley_texture.image = new Image();
	smilley_texture.image.src = "Smiley.png";
	smilley_texture.image.onload = function (){
					gl.bindTexture(gl.TEXTURE_2D, smilley_texture);
					gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
					gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
					gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
					gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, smilley_texture.image);
					gl.bindTexture(gl.TEXTURE_2D, null);
				};
	
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);	
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	gPerspectiveProjectionMatrix = mat4.create();
	
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	// ser the viewport to match	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(gPerspectiveProjectionMatrix, 
						45.0, 
						parseFloat(canvas.width) /parseFloat(canvas.height), 
						parseFloat(canvas.height) / parseFloat(canvas.width), 
						100.0); 
	
}

function draw()
{
	// variable declaration	
	
	// code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT );
	
	gl.useProgram(gShaderProgramObject);
	
	// rectangle
	var modelViewMatrix = mat4.create();
	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -6.0]);
			
	var modelViewProjectionMatrix = mat4.create();	
	mat4.multiply(modelViewProjectionMatrix, gPerspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMVPUniform, false, modelViewProjectionMatrix);
	
	gl.uniform1i(isColorUniform, isColor)

	if(isColor == 1)
	{
		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D, smilley_texture);
		gl.uniform1i(gTextureSamplerUniform, 0);
	}
	gl.bindVertexArray(gVao_rectangle);

	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_rectangle_texcoord);
	gl.bufferData(gl.ARRAY_BUFFER, rectangleTexcoord, gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_TEXTURE_0, 2, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_TEXTURE_0);

	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.bindVertexArray(null);
	
	gl.useProgram(null);
		
	requestAnimationFrame(draw, canvas);
}

function degToRad(degree)
{
	return ( degree * Math.PI / 180.0 );
}

function uninitialize()
{
	if(smilley_texture)
	{
		gl.deleteTexture(smilley_texture);
		smilley_texture = 0;
	}
	
	// rectangle
	if(gVao_rectangle)
	{
		gl.deleteVertexArray(gVao_rectangle);
		gVao_rectangle = null;
	}
	
	if(gVbo_rectangle_position)
	{
		gl.deleteBuffer(gVbo_rectangle_position);
		gVbo_rectangle_position = null;
	}

	if(gVbo_rectangle_texcoord)
	{
		gl.deleteBuffer(gVbo_rectangle_texcoord);
		gVbo_rectangle_texcoord = null;
	}
	
	if(gShaderProgramObject)
	{
		if(gFragmentShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gFragmentShaderObject);
			gl.deleteShader(gFragmentShaderObject);
			gFragmentShaderObject = null;
		}
		
		if(gVertexShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gVertexShaderObject);
			gl.deleteShader(gVertexShaderObject);
			gVertexShaderObject = null;
		}
		
		gl.deleteProgram(gShaderProgramObject);
		gShaderProgramObject = null;
	}
}
