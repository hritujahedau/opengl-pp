

// JavaScript source code
var canvas = null;
var gl;
var canvas_original_width = 0, canvas_original_height = 0;
var bFullScreen = false;
var requestAnimationFrame = window.requestAnimationFrame    || 
							window.webkitAnimationFrame     ||
							window.mozRequestAnimationFrame ||
							window.opRequestAnimationFrame  ||
							window.msRequestAnimationFrame	 ;

const webGLMacros =
{
	HRH_ATTRIBUTE_POSITION : 0 ,
    HRH_ATTRIBUTE_COLOR : 1 ,
    HRH_ATTRIBUTE_NORMAL : 2 ,
	HRH_ATTRIBUTE_TEXTURE_0 : 3 ,
}

var gVertexShaderObject;
var gFragmentShaderObject;
var gShaderProgramObject;

var gVao;
var gVbo;
var gMVPUniform;

var sphere = null;

var gPerspectiveProjectionMatrix;

var sholder = 0, elbow = 0;

var stack_head = [];

function main()
{
	// get canvas from dom
    canvas = document.getElementById("hrh"); // type inferance

    if (!canvas) 
	{
        console.log("canvas failed");
    }
    else
    {
        console.log("Got canvas");
    }

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
    
	// window in buil variable , window is inherited from DOM object
	// 3rd parameter false means do bubbling
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);	
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize(); // explicit warm up resize because webgl don't call resize
	
	draw();
	
}

function keyDown(event)
{
	switch(event.keyCode)
	{
		// for 'f' or 'F'
		case 70:
			toggleFullScreen();
			break;
		case 27:
			uninitialize();
			window.close(); // may not work in firefox but will work in chrome and safari
			break;
	}
	switch(event.key)
	{
		case 's':
			sholder = (sholder + 3) % 360;
			break;
		case 'S':
			sholder = (sholder - 3) % 360;
			break;
		case 'e':
			elbow = (elbow + 3) % 360;
			break;
		case 'E':
			elbow = (elbow - 3) % 360;
			break;
	}
}

function mouseDown()
{
	
}

function toggleFullScreen()
{
	var fullscreen_element = 	document.fullscreenElement || 
								document.webkitFulllscreenElement || 
								document.mozFullScreen || 
								document.msFullscreenElement ||
								null;
								
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
		{
			canvas.requestFullscreen();			
		} 
		else if(canvas.webkitRequestFulllscreen())
		{
			canvas.webkitRequestFulllscreen();
		}
		else if(canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		} 
		else if(canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		} 
		else if ( document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		} 
		else if (document.mozCancelFullScreen)
		{
			document.mozCancelFullScreen();
		} 
		else if (document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		bFullScreen = false;
	}	
}

function init()
{
	
    // get drawing context from the canvas
    gl = canvas.getContext("webgl2");

    if (!gl) 
	{
        console.log("gl failed");
    }
    else 
	{
        console.log("Got gl");
    }
	
	gVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	
	var vertexShaderSourceCode = 
								" #version 300 es \n 										" +
								" in vec4 vPosition;										" +
								" in vec3 inColor; 											" +
								" out vec3 outColor; 										" +
								" uniform mat4 u_mvpMatrix;									" +
								" void main()												" +
								" {															" +
								" 	gl_Position = u_mvpMatrix * vPosition; 					" +
								" 	outColor = inColor;										" +
								" }															" ;

	gl.shaderSource(gVertexShaderObject, vertexShaderSourceCode);
	
	gl.compileShader(gVertexShaderObject);
	
	if(gl.getShaderParameter(gVertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
	var fragmentShaderSourceCode = 	" #version 300 es\n 						" +
									" precision highp float; 					" +
									" out vec4 FragColor; 						" +
									" in vec3 outColor; 						" +
									" void main()								" +
									" {											" +
									" 	FragColor = vec4(outColor, 1.0);		" +
									" }											" ;
	
	gl.shaderSource(gFragmentShaderObject, fragmentShaderSourceCode);
	
	gl.compileShader(gFragmentShaderObject);
	
	if(gl.getShaderParameter(gFragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gShaderProgramObject = gl.createProgram();
	
	// attach vertex shader to shader program
	gl.attachShader(gShaderProgramObject, gVertexShaderObject);
	
	// attch fragment shader to shader program
	gl.attachShader(gShaderProgramObject, gFragmentShaderObject);
	
	// pri-link binding of shader object with vertex shader possition attribute
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_COLOR, "inColor");

	// LINK PROGRAM
	gl.linkProgram(gShaderProgramObject);
	
	if(!gl.getProgramParameter(gShaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gMVPUniform = gl.getUniformLocation(gShaderProgramObject, "u_mvpMatrix");
	
	sphere = new Mesh();
		
	makeSphere(sphere, 2.0, 30, 30);
	
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);

	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	gPerspectiveProjectionMatrix = mat4.create();
	
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	// ser the viewport to match	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(gPerspectiveProjectionMatrix, 
						45.0, 
						parseFloat(canvas.width) /parseFloat(canvas.height), 
						parseFloat(canvas.height) / parseFloat(canvas.width), 
						100.0); 
	
}

function draw()
{
	// code
	gl.clear(gl.COLOR_BUFFER_BIT);
	
	gl.useProgram(gShaderProgramObject);
	
	var currentMatrix = mat4.create();

	var cameraPosition = [0.0, 0.0, 20.0];
	var cameraFocusPoints = [0.0, 0.0, 0.0];
	var cameraUpAxis = [0.0, 1.0, 0.0];

	var cameraMatrix = mat4.create();
	mat4.lookAt(cameraMatrix, cameraPosition, cameraFocusPoints, cameraUpAxis);

	stack_head.push(cameraMatrix);

	var translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [1.0, 0.0, 0.0]);

	var sholderRotateMatrix = mat4.create();
	mat4.rotateZ(sholderRotateMatrix, sholderRotateMatrix, degToRad(sholder));

	var sholderScaleMatrix = mat4.create();
	mat4.scale(sholderScaleMatrix, sholderScaleMatrix, [2.0, 0.5, 1.0]);

	var modelViewMatrix = mat4.create();
	mat4.multiply(modelViewMatrix, modelViewMatrix, cameraMatrix);
	mat4.multiply(modelViewMatrix, modelViewMatrix, sholderRotateMatrix);
	mat4.multiply(modelViewMatrix, modelViewMatrix, translateMatrix);
	mat4.multiply(modelViewMatrix, modelViewMatrix, sholderScaleMatrix);

	currentMatrix = mat4.create();
	mat4.multiply(currentMatrix, sholderRotateMatrix, translateMatrix);
	
	// push only sholder rotate and translate matrix
	stack_head.push(currentMatrix);

	var modelViewProjectionMatrix = mat4.create();
	
	mat4.multiply(modelViewProjectionMatrix, gPerspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMVPUniform, false, modelViewProjectionMatrix);

	gl.vertexAttrib3f(webGLMacros.HRH_ATTRIBUTE_COLOR, 0.5, 0.35, 0.05);

	gl.bindVertexArray(gVao);
	
	sphere.draw();

	// ******************************************* second sphere *******************************
	currentMatrix = stack_head.pop();
	
	var elbowRotateMatrix = mat4.create();
	mat4.rotateZ(elbowRotateMatrix, elbowRotateMatrix, degToRad(elbow));
	
	elbowTranslateMatrix = mat4.create();
	mat4.translate(elbowTranslateMatrix, elbowTranslateMatrix, [3.0, 0.0, 0.0]);

	var elbowScaleMatrix = mat4.create();
	mat4.scale(elbowScaleMatrix, elbowScaleMatrix, [2.0, 0.5, 0.1]);

	modelViewMatrix = mat4.create();

	mat4.multiply(modelViewMatrix, modelViewMatrix, currentMatrix);
	mat4.multiply(modelViewMatrix, modelViewMatrix, elbowTranslateMatrix);
	mat4.multiply(modelViewMatrix, modelViewMatrix, elbowRotateMatrix);
	mat4.multiply(modelViewMatrix, modelViewMatrix, elbowTranslateMatrix);
	mat4.multiply(modelViewMatrix, modelViewMatrix, elbowScaleMatrix);

	mat4.multiply(modelViewMatrix, cameraMatrix, modelViewMatrix);

	mat4.multiply(modelViewProjectionMatrix, gPerspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMVPUniform, false, modelViewProjectionMatrix);

	gl.vertexAttrib3f(webGLMacros.HRH_ATTRIBUTE_COLOR, 1.0, 1.0, 1.0);

	gl.bindVertexArray(gVao);
	
	sphere.draw();
	
	stack_head.pop();

	requestAnimationFrame(draw, canvas);
}

function degToRad(degree)
{
	return ( degree * Math.PI / 180.0 );
}

function uninitialize()
{
	if(sphere)
	{
		sphere.deallocate();
	}
	
	if(gVao)
	{
		gl.deleteVertexArray(gVao);
		gVao = null;
	}
	
	if(gVbo)
	{
		gl.deleteBuffer(gVbo);
		gVbo = null;
	}
	
	if(gShaderProgramObject)
	{
		if(gFragmentShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gFragmentShaderObject);
			gl.deleteShader(gFragmentShaderObject);
			gFragmentShaderObject = null;
		}
		
		if(gVertexShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gVertexShaderObject);
			gl.deleteShader(gVertexShaderObject);
			gVertexShaderObject = null;
		}
		
		gl.deleteProgram(gShaderProgramObject);
		gShaderProgramObject = null;
	}
}
