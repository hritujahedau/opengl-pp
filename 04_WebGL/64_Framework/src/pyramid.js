
var gVertexShaderObject;
var gFragmentShaderObject;
var gShaderProgramObject;

var gMVPUniform;
var gTextureSamplerUniform = 0;

var rotateAngle = 0.0;

function initPyramidShader()
{
    gVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	
	var vertexShaderFile = document.getElementById("hvs-pyramid");

	var vertexShaderSourceCode = "";

	// fc - firstChild
	var fc = vertexShaderFile.firstChild;
	while(fc)
	{
		if(fc.nodeType == 3)
		{
			vertexShaderSourceCode = vertexShaderSourceCode + fc.textContent;
		}	
		fc = fc.nextSibling;
	}

	gl.shaderSource(gVertexShaderObject, vertexShaderSourceCode);
	
	gl.compileShader(gVertexShaderObject);
	
	if(gl.getShaderParameter(gVertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
	var fragmentShaderFile = document.getElementById("hfs-pyramid");

	var fragmentShaderSourceCode = "";

	// fc - firstChild
	fc = fragmentShaderFile.firstChild;
	while(fc)
	{
		if(fc.nodeType == 3)
		{
			fragmentShaderSourceCode = fragmentShaderSourceCode + fc.textContent;
		}	
		fc = fc.nextSibling;
	}
	
	gl.shaderSource(gFragmentShaderObject, fragmentShaderSourceCode);
	
	gl.compileShader(gFragmentShaderObject);
	
	if(gl.getShaderParameter(gFragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gShaderProgramObject = gl.createProgram();
	
	// attach vertex shader to shader program
	gl.attachShader(gShaderProgramObject, gVertexShaderObject);
	
	// attch fragment shader to shader program
	gl.attachShader(gShaderProgramObject, gFragmentShaderObject);
	
	// pri-link binding of shader object with vertex shader possition attribute
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_TEXTURE0, "vTexCoord");

	// LINK PROGRAM
	gl.linkProgram(gShaderProgramObject);
	
	if(!gl.getProgramParameter(gShaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gMVPUniform = gl.getUniformLocation(gShaderProgramObject, "u_mvpMatrix");
	gTextureSamplerUniform = gl.getUniformLocation(gShaderProgramObject, "u_texture_sampler");

}

function drawPyramid()
{
    gl.useProgram(gShaderProgramObject);
	
	var cameraMatrix = mat4.create();
	var modelViewMatrix = mat4.create();

	var cameraPosition = [0.0, 0.0, 1.0];
	var cameraFocusPoints = [0.0, 0.0, 0.0];
	var cameraUpAxis = [0.0, 1.0, 0.0];
	
	mat4.lookAt(cameraMatrix, cameraPosition, cameraFocusPoints, cameraUpAxis);

	mat4.multiply(modelViewMatrix, modelViewMatrix, cameraMatrix);
	
	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -3.0]);
	var modelViewProjectionMatrix = mat4.create();

	var rotateMatrix = mat4.create();
	mat4.rotateY(rotateMatrix, rotateMatrix, degToRad(rotateAngle));

	mat4.multiply(modelViewMatrix, modelViewMatrix, rotateMatrix);
	
	mat4.multiply(modelViewProjectionMatrix, gPerspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMVPUniform, false, modelViewProjectionMatrix);
	
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, stone_texture);
	gl.uniform1i(gTextureSamplerUniform, 0);
	
	renderSphere();

	gl.useProgram(null);
	
}

function updatePyramid()
{
	if(rotateAngle > 360.0)
	{
		rotateAngle = 0.0;
	}
	rotateAngle = rotateAngle + 0.1;
}

function uninitializePyramidShader()
{
    if(gShaderProgramObject)
	{
		if(gFragmentShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gFragmentShaderObject);
			gl.deleteShader(gFragmentShaderObject);
			gFragmentShaderObject = null;
		}
		
		if(gVertexShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gVertexShaderObject);
			gl.deleteShader(gVertexShaderObject);
			gVertexShaderObject = null;
		}
		
		gl.deleteProgram(gShaderProgramObject);
		gShaderProgramObject = null;
	}
}

function uninitializePyramidVao()
{
    if(gVao)
	{
		gl.deleteVertexArray(gVao);
		gVao = null;
	}
	
	if(gVbo)
	{
		gl.deleteBuffer(gVbo);
		gVbo = null;
	}
}
