// JavaScript source code
var canvas = null;
var gl;
var canvas_original_width = 0, canvas_original_height = 0;
var bFullScreen = false;
var requestAnimationFrame = window.requestAnimationFrame    || 
							window.webkitAnimationFrame     ||
							window.mozRequestAnimationFrame ||
							window.opRequestAnimationFrame  ||
							window.msRequestAnimationFrame	 ;

const webGLMacros =
{
	HRH_ATTRIBUTE_POSITION : 0 ,
    HRH_ATTRIBUTE_COLOR : 1 ,
    HRH_ATTRIBUTE_NORMAL : 2 ,
	HRH_ATTRIBUTE_TEXTURE_0 : 3 ,
}

var gVertexShaderObject;
var gFragmentShaderObject;
var gShaderProgramObject;

var gVao;
var gVbo;
var gMVPUniform;

var gPerspectiveProjectionMatrix;

var x, y, height, width;

function main()
{
	// get canvas from dom
    canvas = document.getElementById("hrh"); // type inferance

    if (!canvas) 
	{
        console.log("canvas failed");
    }
    else
    {
        console.log("Got canvas");
    }

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
    
	// window in buil variable , window is inherited from DOM object
	// 3rd parameter false means do bubbling
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);	
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize(); // explicit warm up resize because webgl don't call resize
	
	draw();
	
}

function keyDown(event)
{	
	var heightByTwo = canvas.height / 2, widthByTwo = canvas.width / 2;
	
	switch(event.keyCode)
	{
		case 70:
			toggleFullScreen();
			break;
		case 27:
			uninitialize();
			window.close(); // may not work in firefox but will work in chrome and safari
			break;
	}
	
	switch(event.key)
	{	
		case '0':
			x = 0;
			y = 0;
			width = canvas.width;
			height = canvas.height;
			break;
		case '1':
			x = 0;
			y = heightByTwo;
			width = widthByTwo;
			height = heightByTwo;
			break;
		case '2':
			x = widthByTwo;
			y = heightByTwo;
			width = widthByTwo;
			height = heightByTwo;
			break;
		case '3':
			x = widthByTwo;
			y = 0;
			width = widthByTwo;
			height = heightByTwo;
			break;
		case '4':
			x = 0 ;
			y = 0;
			width = widthByTwo;
			height = heightByTwo;
			break;
		case '5':
			x = 0;
			y = 0;
			width = widthByTwo;
			height = heightByTwo * 2;
			break;
		case '6':
			x = widthByTwo;
			y = 0;
			width = widthByTwo;
			height = heightByTwo * 2;
			break;
		case '7':
			x = 0;
			y = heightByTwo;
			width = widthByTwo * 2;
			height = heightByTwo;
			break;
		case '8':
			x = 0;
			y = 0;
			width = widthByTwo * 2;
			height = heightByTwo;
			break;
		case '9':
			x = widthByTwo / 2;
			y = heightByTwo / 2;
			width = widthByTwo;
			height = heightByTwo;
			break;
		default:
			break;
	}	
}

function mouseDown()
{
	
}

function toggleFullScreen()
{
	var fullscreen_element = 	document.fullscreenElement || 
								document.webkitFulllscreenElement || 
								document.mozFullScreen || 
								document.msFullscreenElement ||
								null;
								
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
		{
			canvas.requestFullscreen();			
		} 
		else if(canvas.webkitRequestFulllscreen())
		{
			canvas.webkitRequestFulllscreen();
		}
		else if(canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		} 
		else if(canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		} 
		else if ( document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		} 
		else if (document.mozCancelFullScreen)
		{
			document.mozCancelFullScreen();
		} 
		else if (document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		bFullScreen = false;
	}	
}

function init()
{
	
    // get drawing context from the canvas
    gl = canvas.getContext("webgl2");

    if (!gl) 
	{
        console.log("gl failed");
    }
    else 
	{
        console.log("Got gl");
    }
	
	gVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	
	var vertexShaderSourceCode = 
								"#version 300 es \n" +
								"in vec4 vPosition;" +
								"uniform mat4 u_mvpMatrix;" +
								"void main()" +
								"{" +
								"gl_Position = u_mvpMatrix * vPosition;" +
								"}";

	gl.shaderSource(gVertexShaderObject, vertexShaderSourceCode);
	
	gl.compileShader(gVertexShaderObject);
	
	if(gl.getShaderParameter(gVertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
	var fragmentShaderSourceCode = 	"#version 300 es\n"+
									"precision highp float;" +
									"out vec4 FragColor; " +
									"void main()"+
									"{" +
									"FragColor = vec4(1.0, 1.0, 1.0, 1.0);" +
									"}";
	
	gl.shaderSource(gFragmentShaderObject, fragmentShaderSourceCode);
	
	gl.compileShader(gFragmentShaderObject);
	
	if(gl.getShaderParameter(gFragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gShaderProgramObject = gl.createProgram();
	
	// attach vertex shader to shader program
	gl.attachShader(gShaderProgramObject, gVertexShaderObject);
	
	// attch fragment shader to shader program
	gl.attachShader(gShaderProgramObject, gFragmentShaderObject);
	
	// pri-link binding of shader object with vertex shader possition attribute
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_POSITION, "vPosition");

	// LINK PROGRAM
	gl.linkProgram(gShaderProgramObject);
	
	if(!gl.getProgramParameter(gShaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gMVPUniform = gl.getUniformLocation(gShaderProgramObject, "u_mvpMatrix");
	
	var triangleVertices = new Float32Array
		([
			0.0, 1.0, 0.0,
			-1.0, -1.0, 0.0,
			1.0, -1.0, 0.0
		]);
	
	gVao = gl.createVertexArray();
	gl.bindVertexArray(gVao);
	
	gVbo = gl.createBuffer()
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo);
	gl.bufferData(gl.ARRAY_BUFFER, triangleVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_POSITION);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, null);	
	gl.bindVertexArray(null);	
	
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	x = 0;
	y = 0;
	width = canvas.width;
	height = canvas.height;
	
	gl.clearColor(0.0, 0.0, 1.0, 1.0);	
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	x = 0;
	y = 0;
	width = canvas.width;
	height = canvas.height;
	// set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);	
}

function draw()
{
	// code
	gl.clear(gl.COLOR_BUFFER_BIT);
		
	gl.viewport(x, y, width, height);
	
	gl.useProgram(gShaderProgramObject);
	
	var modelViewMatrix = mat4.create();
	gl.uniformMatrix4fv(gMVPUniform, false, modelViewMatrix);
	
	gl.bindVertexArray(gVao);	
	gl.drawArrays(gl.TRIANGLES, 0, 3);	
	gl.bindVertexArray(null);
	
	gl.useProgram(null);
	
	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	if(gVao)
	{
		gl.deleteVertexArray(gVao);
		gVao = null;
	}
	
	if(gVbo)
	{
		gl.deleteBuffer(gVbo);
		gVbo = null;
	}
	
	if(gShaderProgramObject)
	{
		if(gFragmentShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gFragmentShaderObject);
			gl.deleteShader(gFragmentShaderObject);
			gFragmentShaderObject = null;
		}
		
		if(gVertexShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gVertexShaderObject);
			gl.deleteShader(gVertexShaderObject);
			gVertexShaderObject = null;
		}
		
		gl.deleteProgram(gShaderProgramObject);
		gShaderProgramObject = null;
	}
}
