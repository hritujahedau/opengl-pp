// JavaScript source code
var canvas = null;
var gl;
var canvas_original_width = 0, canvas_original_height = 0;
var bFullScreen = false;
var requestAnimationFrame = window.requestAnimationFrame    || 
							window.webkitAnimationFrame     ||
							window.mozRequestAnimationFrame ||
							window.opRequestAnimationFrame  ||
							window.msRequestAnimationFrame	 ;

const webGLMacros =
{
	HRH_ATTRIBUTE_POSITION : 0 ,
    HRH_ATTRIBUTE_COLOR : 1 ,
    HRH_ATTRIBUTE_NORMAL : 2 ,
	HRH_ATTRIBUTE_TEXTURE_0 : 3 ,
}

var gVertexShaderObject;
var gFragmentShaderObject;
var gShaderProgramObject;

var gModelUniform;
var gViewUniform;
var gPerspectiveUniform;

var gLightAmbientUniform;
var gLightSpecularUniform;
var gLightDiffusedUniform;
var gLightPossitionUniform;

var gMaterialSpecularUniform;
var gMaterialAmbientUniform;
var gMaterialDiffuseUniform;
var gMaterialShininessUniform;

var gIsLightOnUniform;

var isLightOn = 0;

var sphere = null;

var gPerspectiveProjectionMatrix;

var lightPossition = new Float32Array ([ 100.0, 100.0, 100.0, 1.0 ]);
var lightAmbient   = new Float32Array ([ 0.0, 0.0, 0.0 ]);
var lightDefuse    = new Float32Array ([ 0.5, 0.2, 0.7 ]);
var lightSpecular  = new Float32Array ([ 0.7, 0.7, 0.7 ]);

var materialAmbient   = new Float32Array ([ 0.0, 0.0, 0.0 ]);
var materialDefuse    = new Float32Array ([ 1.0, 1.0, 1.0 ]);
var materialSpecular  = new Float32Array ([ 1.0, 1.0, 1.0 ]);
var materialShinyness = 58.0;

function main()
{
	// get canvas from dom
    canvas = document.getElementById("hrh"); // type inferance

    if (!canvas) 
	{
        console.log("canvas failed");
    }
    else
    {
        console.log("Got canvas");
    }

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
    
	// window in buil variable , window is inherited from DOM object
	// 3rd parameter false means do bubbling
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);	
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize(); // explicit warm up resize because webgl don't call resize
	
	draw();
	
}

function keyDown(event)
{
	switch(event.keyCode)
	{
		// for 'f' or 'F'
		case 70:
			toggleFullScreen();
			break;
		case 27:
			uninitialize();
			window.close(); // may not work in firefox but will work in chrome and safari
			break;
	}
	
	switch(event.key)
	{
		case 'l':
		case 'L':
			if (isLightOn == 0)
			{
				isLightOn = 1;
			}
			else
			{
				isLightOn = 0;
			}
			break;
	}
}

function mouseDown()
{
	
}

function toggleFullScreen()
{
	var fullscreen_element = 	document.fullscreenElement || 
								document.webkitFulllscreenElement || 
								document.mozFullScreen || 
								document.msFullscreenElement ||
								null;
								
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
		{
			canvas.requestFullscreen();			
		} 
		else if(canvas.webkitRequestFulllscreen())
		{
			canvas.webkitRequestFulllscreen();
		}
		else if(canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		} 
		else if(canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		} 
		else if ( document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		} 
		else if (document.mozCancelFullScreen)
		{
			document.mozCancelFullScreen();
		} 
		else if (document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		bFullScreen = false;
	}	
}

function init()
{
	
    // get drawing context from the canvas
    gl = canvas.getContext("webgl2");

    if (!gl) 
	{
        console.log("gl failed");
    }
    else 
	{
        console.log("Got gl");
    }
	
	gVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	
	var vertexShaderSourceCode = 
			"#version 300 es \n" +
			"in vec4 v_position;" +
			"in vec3 v_normals; " +
			"uniform mat4 u_modelMatrix;" +
			"uniform mat4 u_viewMatrix;" +
			"uniform mat4 u_projectionMatrix;" +
			"uniform int u_isLightOn;" +
			"uniform vec3 u_lightAmbient;" +
			"uniform vec3 u_lightSpecular;" +
			"uniform vec3 u_lightDiffuse;" +
			"uniform vec4 u_light_position; " +
			"uniform vec3 u_materialAmbient; " +
			"uniform vec3 u_materialSpecular; " +
			"uniform vec3 u_materialDiffuse; " +
			"uniform float u_materialshininess; " +
			"out vec3 phoung_ads_lighting;" +
			"void main()" +
			"{" +
			"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" +
			"if (u_isLightOn == 1) " +
			"{" +
				"vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" +
				"vec3 transformed_normal = normalize( mat3( u_viewMatrix * u_modelMatrix ) * v_normals); " +
				"vec3 light_direction = normalize( vec3 (u_light_position - eye_coordinates) ); " +
				"vec3 reflection_vector = reflect (-light_direction, transformed_normal); " +
				"vec3 view_vector = normalize ( vec3 ( -eye_coordinates)); " +
				"vec3 ambient = u_lightAmbient * u_materialAmbient; " +
				"vec3 diffuse_light = u_lightDiffuse * u_materialDiffuse * max ( dot (light_direction , transformed_normal), 0.0); " +
				"vec3 specular = u_lightSpecular * u_materialSpecular * pow( max ( dot ( reflection_vector , view_vector), 0.0) , u_materialshininess);" +
				"phoung_ads_lighting = ambient + diffuse_light + specular;" +
			"}" +
			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" +
			"}";

	gl.shaderSource(gVertexShaderObject, vertexShaderSourceCode);
	
	gl.compileShader(gVertexShaderObject);
	
	if(gl.getShaderParameter(gVertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
	var fragmentShaderSourceCode = 	"#version 300 es\n"+
									"precision highp float;" +
									"in vec3 phoung_ads_lighting;" +
									"out vec4 FragColor; " +
									"void main()"+
									"{" +
									"FragColor = vec4(phoung_ads_lighting, 1.0); " +
									"}";

	gl.shaderSource(gFragmentShaderObject, fragmentShaderSourceCode);
	
	gl.compileShader(gFragmentShaderObject);
	
	if(gl.getShaderParameter(gFragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gShaderProgramObject = gl.createProgram();
	
	// attach vertex shader to shader program
	gl.attachShader(gShaderProgramObject, gVertexShaderObject);
	
	// attch fragment shader to shader program
	gl.attachShader(gShaderProgramObject, gFragmentShaderObject);
	
	// pri-link binding of shader object with vertex shader possition attribute
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_NORMAL, "v_normals");

	// LINK PROGRAM
	gl.linkProgram(gShaderProgramObject);
	
	if(!gl.getProgramParameter(gShaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	// get MVP uniform location
	gModelUniform = gl.getUniformLocation(gShaderProgramObject, "u_modelMatrix");
	gViewUniform = gl.getUniformLocation(gShaderProgramObject, "u_viewMatrix");
	gPerspectiveUniform = gl.getUniformLocation(gShaderProgramObject, "u_projectionMatrix");
	gIsLightOnUniform = gl.getUniformLocation(gShaderProgramObject, "u_isLightOn");
	
	gLightAmbientUniform =   gl.getUniformLocation(gShaderProgramObject, "u_lightAmbient");
	gLightSpecularUniform =  gl.getUniformLocation(gShaderProgramObject, "u_lightSpecular");
	gLightDiffusedUniform =  gl.getUniformLocation(gShaderProgramObject, "u_lightDiffuse");
	gLightPossitionUniform = gl.getUniformLocation(gShaderProgramObject, "u_light_position");

	gMaterialDiffuseUniform   =   gl.getUniformLocation(gShaderProgramObject, "u_materialDiffuse");
	gMaterialAmbientUniform   =   gl.getUniformLocation(gShaderProgramObject, "u_materialAmbient");
	gMaterialSpecularUniform  =   gl.getUniformLocation(gShaderProgramObject, "u_materialSpecular");
	gMaterialShininessUniform =   gl.getUniformLocation(gShaderProgramObject, "u_materialshininess");
	
	sphere = new Mesh();
		
	makeSphere(sphere, 2.0, 30, 30);
	
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	gPerspectiveProjectionMatrix = mat4.create();
	
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	// ser the viewport to match	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(gPerspectiveProjectionMatrix, 
						45.0, 
						parseFloat(canvas.width) /parseFloat(canvas.height), 
						parseFloat(canvas.height) / parseFloat(canvas.width), 
						100.0); // no 0.1f
	
}

function draw()
{
	// code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(gShaderProgramObject);
	
	var viewMatrix = mat4.create();
	var modelMatrix = mat4.create();
	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -4.55]);
	var modelViewProjectionMatrix = mat4.create();

	if (isLightOn == 1)
	{
		gl.uniform4fv(gLightPossitionUniform, lightPossition);
		gl.uniform3fv(gLightDiffusedUniform, lightDefuse);
		gl.uniform3fv(gLightAmbientUniform,  lightAmbient);
		gl.uniform3fv(gLightSpecularUniform, lightSpecular);

		gl.uniform3fv(gMaterialAmbientUniform,  materialAmbient);
		gl.uniform3fv(gMaterialSpecularUniform, materialSpecular);
		gl.uniform3fv(gMaterialDiffuseUniform,  materialDefuse);
		gl.uniform1f (gMaterialShininessUniform, materialShinyness);
	}

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false,  viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();
	
	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	if(sphere)
	{
		sphere.deallocate();
	}
	
	if(gVao)
	{
		gl.deleteVertexArray(gVao);
		gVao = null;
	}
	
	if(gVbo)
	{
		gl.deleteBuffer(gVbo);
		gVbo = null;
	}
	
	if(gShaderProgramObject)
	{
		if(gFragmentShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gFragmentShaderObject);
			gl.deleteShader(gFragmentShaderObject);
			gFragmentShaderObject = null;
		}
		
		if(gVertexShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gVertexShaderObject);
			gl.deleteShader(gVertexShaderObject);
			gVertexShaderObject = null;
		}
		
		gl.deleteProgram(gShaderProgramObject);
		gShaderProgramObject = null;
	}
}
