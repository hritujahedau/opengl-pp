// JavaScript source code
var canvas = null;
var gl;
var canvas_original_width = 0, canvas_original_height = 0;
var bFullScreen = false;
var requestAnimationFrame = window.requestAnimationFrame    || 
							window.webkitAnimationFrame     ||
							window.mozRequestAnimationFrame ||
							window.opRequestAnimationFrame  ||
							window.msRequestAnimationFrame	 ;

const webGLMacros =
{
	HRH_ATTRIBUTE_POSITION : 0 ,
    HRH_ATTRIBUTE_COLOR : 1 ,
    HRH_ATTRIBUTE_NORMAL : 2 ,
	HRH_ATTRIBUTE_TEXTURE_0 : 3 ,
}

var gVertexShaderObjectPerVertex;
var gFragmentShaderObjectPerVertex;
var gShaderProgramObjectPerVertexPerVertex;

var gVertexShaderObjectPerFragment;
var gFragmentShaderObjectPerFragment;
var gShaderProgramObjectPerFragment;

var gCurrentShaderProgram;

var gModelUniform;
var gViewUniform;
var gPerspectiveUniform;

var gLightAmbientUniform;
var gLightSpecularUniform;
var gLightDiffusedUniform;
var gLightPossitionUniform;

var gMaterialSpecularUniform;
var gMaterialAmbientUniform;
var gMaterialDiffuseUniform;
var gMaterialShininessUniform;

var gIsLightOnUniform;

var isLightPerVertexShader = 0, isLightPerFragmentShader = 0, isLightOn = 0;

var sphere = null;

var gPerspectiveProjectionMatrix;

var lightPossition = new Float32Array ([ 100.0, 100.0, 100.0, 1.0 ]);
var lightAmbient   = new Float32Array ([ 0.0, 0.0, 0.0 ]);
var lightDefuse    = new Float32Array ([ 0.5, 0.2, 0.7 ]);
var lightSpecular  = new Float32Array ([ 0.7, 0.7, 0.7 ]);

var materialAmbient   = new Float32Array ([ 0.0, 0.0, 0.0 ]);
var materialDefuse    = new Float32Array ([ 1.0, 1.0, 1.0 ]);
var materialSpecular  = new Float32Array ([ 1.0, 1.0, 1.0 ]);
var materialShinyness = 58.0;

function main()
{
	// get canvas from dom
    canvas = document.getElementById("hrh"); // type inferance

    if (!canvas) 
	{
        console.log("canvas failed");
    }
    else
    {
        console.log("Got canvas");
    }

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
    
	// window in buil variable , window is inherited from DOM object
	// 3rd parameter false means do bubbling
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);	
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize(); // explicit warm up resize because webgl don't call resize
	
	draw();
	
}

function keyDown(event)
{
	switch(event.keyCode)
	{
		case 27:
			uninitialize();
			window.close();
			break;
	}
	
	switch(event.key)
	{
		case 'v':
		case 'V':
			if (isLightPerVertexShader == 1)
			{
				isLightPerVertexShader = 0;
				isLightOn = 0;
			}
			else
			{
				isLightPerVertexShader = 1;
				isLightPerFragmentShader = 0;
				isLightOn = 1;
			}
			break;
		case 'f':
		case 'F':
			if (isLightPerFragmentShader == 0)
			{
				isLightPerFragmentShader = 1;
				isLightPerVertexShader = 0;
				isLightOn = 1;
			}
			else
			{
				isLightPerFragmentShader = 0;
				isLightOn = 0;
			}
			break;
		case 'q':
		case 'Q':
			toggleFullScreen();
			break;
	}
}

function mouseDown()
{
	
}

function toggleFullScreen()
{
	var fullscreen_element = 	document.fullscreenElement || 
								document.webkitFulllscreenElement || 
								document.mozFullScreen || 
								document.msFullscreenElement ||
								null;
								
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
		{
			canvas.requestFullscreen();			
		} 
		else if(canvas.webkitRequestFulllscreen())
		{
			canvas.webkitRequestFulllscreen();
		}
		else if(canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		} 
		else if(canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		} 
		else if ( document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		} 
		else if (document.mozCancelFullScreen)
		{
			document.mozCancelFullScreen();
		} 
		else if (document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		bFullScreen = false;
	}	
}

function init()
{
	
    // get drawing context from the canvas
    gl = canvas.getContext("webgl2");

    if (!gl) 
	{
        console.log("gl failed");
    }
    else 
	{
        console.log("Got gl");
    }
	
	/************************************ Per Vertex *********************************************************************/
	
	gVertexShaderObjectPerVertex = gl.createShader(gl.VERTEX_SHADER);
	
	var vertexShaderSourceCodePerVertex = 
			"#version 300 es \n" +
			"in vec4 v_position;" +
			"in vec3 v_normals; " +
			"uniform mat4 u_modelMatrix;" +
			"uniform mat4 u_viewMatrix;" +
			"uniform mat4 u_projectionMatrix;" +
			"uniform int u_isLightOn;" +
			"uniform vec3 u_lightAmbient;" +
			"uniform vec3 u_lightSpecular;" +
			"uniform vec3 u_lightDiffuse;" +
			"uniform vec4 u_light_position; " +
			"uniform vec3 u_materialAmbient; " +
			"uniform vec3 u_materialSpecular; " +
			"uniform vec3 u_materialDiffuse; " +
			"uniform float u_materialshininess; " +
			"out vec3 phoung_ads_lighting;" +
			"void main()" +
			"{" +
			"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" +
			"if (u_isLightOn == 1) " +
			"{" +
				"vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" +
				"vec3 transformed_normal = normalize( mat3( u_viewMatrix * u_modelMatrix ) * v_normals); " +
				"vec3 light_direction = normalize( vec3 (u_light_position - eye_coordinates) ); " +
				"vec3 reflection_vector = reflect (-light_direction, transformed_normal); " +
				"vec3 view_vector = normalize ( vec3 ( -eye_coordinates)); " +
				"vec3 ambient = u_lightAmbient * u_materialAmbient; " +
				"vec3 diffuse_light = u_lightDiffuse * u_materialDiffuse * max ( dot (light_direction , transformed_normal), 0.0); " +
				"vec3 specular = u_lightSpecular * u_materialSpecular * pow( max ( dot ( reflection_vector , view_vector), 0.0) , u_materialshininess);" +
				"phoung_ads_lighting = ambient + diffuse_light + specular;" +
			"}" +
			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" +
			"}";

	gl.shaderSource(gVertexShaderObjectPerVertex, vertexShaderSourceCodePerVertex);
	
	gl.compileShader(gVertexShaderObjectPerVertex);
	
	if(gl.getShaderParameter(gVertexShaderObjectPerVertex, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObjectPerVertex);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gFragmentShaderObjectPerVertex = gl.createShader(gl.FRAGMENT_SHADER);
	
	var fragmentShaderSourceCodePerVertex = 	"#version 300 es\n"+
									"precision highp float;" +
									"in vec3 phoung_ads_lighting;" +
									"out vec4 FragColor; " +
									"void main()"+
									"{" +
									"FragColor = vec4(phoung_ads_lighting, 1.0); " +
									"}";

	gl.shaderSource(gFragmentShaderObjectPerVertex, fragmentShaderSourceCodePerVertex);
	
	gl.compileShader(gFragmentShaderObjectPerVertex);
	
	if(gl.getShaderParameter(gFragmentShaderObjectPerVertex, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObjectPerVertex);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gShaderProgramObjectPerVertex = gl.createProgram();
	
	// attach vertex shader to shader program
	gl.attachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);
	
	// attch fragment shader to shader program
	gl.attachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);
	
	// pri-link binding of shader object with vertex shader possition attribute
	gl.bindAttribLocation(gShaderProgramObjectPerVertex, webGLMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(gShaderProgramObjectPerVertex, webGLMacros.HRH_ATTRIBUTE_NORMAL, "v_normals");

	// LINK PROGRAM
	gl.linkProgram(gShaderProgramObjectPerVertex);
	
	if(!gl.getProgramParameter(gShaderProgramObjectPerVertex, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObjectPerVertex);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	/********************************************************** Per Fragment **************************************************************/
	
	
	gVertexShaderObjectPerFragment = gl.createShader(gl.VERTEX_SHADER);
	
	var vertexShaderSourceCode = 
			"#version 300 es \n" +
			"in vec4 v_position;" +
			"in vec3 v_normals; " +
			"uniform mat4 u_modelMatrix;" +
			"uniform mat4 u_viewMatrix;" +
			"uniform mat4 u_projectionMatrix;" +
			"uniform int u_isLightOn;" +
			"uniform vec4 u_light_position; " +
			"out vec3 light_direction; " +
			"out vec3 transformed_normal;" +
			"out vec3 view_vector;" +
			"void main()" +
			"{" +
			"if (u_isLightOn == 1) " +
			"{" +
			"vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" +
			"transformed_normal =  mat3( u_viewMatrix * u_modelMatrix ) * v_normals;" +
			"light_direction = vec3 (u_light_position - eye_coordinates);" +
			"view_vector =  vec3 ( -eye_coordinates);" +
			"}" +
			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" +
			"}" ;

	gl.shaderSource(gVertexShaderObjectPerFragment, vertexShaderSourceCode);
	
	gl.compileShader(gVertexShaderObjectPerFragment);
	
	if(gl.getShaderParameter(gVertexShaderObjectPerFragment, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObjectPerFragment);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gFragmentShaderObjectPerFragment = gl.createShader(gl.FRAGMENT_SHADER);
	
	var fragmentShaderSourceCode = 	
										"#version 300 es\n"+
										"precision highp float;" +
										"vec3 phoung_ads_lighting;" +
										"out vec4 FragColor; " +
										"in vec3 light_direction; " +
										"in vec3 transformed_normal;" +
										"in vec3 view_vector;" +
										"vec3 normalized_light_direction; " +
										"vec3 normalized_transformed_normal;" +
										"vec3 normalized_view_vector;" +
										"uniform highp vec3 u_lightAmbient;" +
										"uniform highp vec3 u_lightSpecular;" +
										"uniform highp vec3 u_lightDiffuse;" +
										"uniform highp vec3 u_materialAmbient; " +
										"uniform highp vec3 u_materialSpecular; " +
										"uniform highp vec3 u_materialDiffuse; " +
										"uniform highp float u_materialshininess; " +
										"uniform highp int u_isLightOn;" +
										"void main()" +
										"{" +
										"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" +
										"if (u_isLightOn == 1) " +
										"{" +
										"normalized_light_direction = normalize(light_direction);" +
										"normalized_transformed_normal = normalize(transformed_normal);" +
										"normalized_view_vector = normalize(view_vector);" +
										"vec3 reflection_vector = reflect (-normalized_light_direction, normalized_transformed_normal); " +
										"vec3 ambient = u_lightAmbient * u_materialAmbient; " +
										"vec3 diffuse_light = u_lightDiffuse * u_materialDiffuse * max ( dot (normalized_light_direction , normalized_transformed_normal), 0.0); " +
										"vec3 specular = u_lightSpecular * u_materialSpecular * pow( max ( dot ( reflection_vector , normalized_view_vector), 0.0) , u_materialshininess);" +
										"phoung_ads_lighting = ambient + diffuse_light + specular;" +
										"}" +
										"FragColor = vec4(phoung_ads_lighting, 1.0); " +
										"}" ;

	gl.shaderSource(gFragmentShaderObjectPerFragment, fragmentShaderSourceCode);
	
	gl.compileShader(gFragmentShaderObjectPerFragment);
	
	if(gl.getShaderParameter(gFragmentShaderObjectPerFragment, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObjectPerFragment);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gShaderProgramObjectPerFragment = gl.createProgram();
	
	// attach vertex shader to shader program
	gl.attachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);
	
	// attch fragment shader to shader program
	gl.attachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);
	
	// pri-link binding of shader object with vertex shader possition attribute
	gl.bindAttribLocation(gShaderProgramObjectPerFragment, webGLMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(gShaderProgramObjectPerFragment, webGLMacros.HRH_ATTRIBUTE_NORMAL, "v_normals");

	// LINK PROGRAM
	gl.linkProgram(gShaderProgramObjectPerFragment);
	
	if(!gl.getProgramParameter(gShaderProgramObjectPerFragment, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObjectPerFragment);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	
	// sphere
	sphere = new Mesh();
		
	makeSphere(sphere, 2.0, 30, 30);
	
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	gPerspectiveProjectionMatrix = mat4.create();
	
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	// ser the viewport to match	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(gPerspectiveProjectionMatrix, 
						45.0, 
						parseFloat(canvas.width) /parseFloat(canvas.height), 
						parseFloat(canvas.height) / parseFloat(canvas.width), 
						100.0); // no 0.1f
	
}

function draw()
{
	// code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gCurrentShaderProgram = (isLightPerFragmentShader == 1) ? gShaderProgramObjectPerFragment : gShaderProgramObjectPerVertex ;
	
	gl.useProgram(gCurrentShaderProgram);
	
	var viewMatrix = mat4.create();
	var modelMatrix = mat4.create();
	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -4.55]);
	var modelViewProjectionMatrix = mat4.create();
	
	// get MVP uniform location
	gModelUniform = gl.getUniformLocation(gCurrentShaderProgram, "u_modelMatrix");
	gViewUniform = gl.getUniformLocation(gCurrentShaderProgram, "u_viewMatrix");
	gPerspectiveUniform = gl.getUniformLocation(gCurrentShaderProgram, "u_projectionMatrix");
	gIsLightOnUniform = gl.getUniformLocation(gCurrentShaderProgram, "u_isLightOn");
	
	gLightAmbientUniform =   gl.getUniformLocation(gCurrentShaderProgram, "u_lightAmbient");
	gLightSpecularUniform =  gl.getUniformLocation(gCurrentShaderProgram, "u_lightSpecular");
	gLightDiffusedUniform =  gl.getUniformLocation(gCurrentShaderProgram, "u_lightDiffuse");
	gLightPossitionUniform = gl.getUniformLocation(gCurrentShaderProgram, "u_light_position");

	gMaterialDiffuseUniform   =   gl.getUniformLocation(gCurrentShaderProgram, "u_materialDiffuse");
	gMaterialAmbientUniform   =   gl.getUniformLocation(gCurrentShaderProgram, "u_materialAmbient");
	gMaterialSpecularUniform  =   gl.getUniformLocation(gCurrentShaderProgram, "u_materialSpecular");
	gMaterialShininessUniform =   gl.getUniformLocation(gCurrentShaderProgram, "u_materialshininess");

	if (isLightOn == 1)
	{
		gl.uniform4fv(gLightPossitionUniform, lightPossition);
		gl.uniform3fv(gLightDiffusedUniform, lightDefuse);
		gl.uniform3fv(gLightAmbientUniform,  lightAmbient);
		gl.uniform3fv(gLightSpecularUniform, lightSpecular);

		gl.uniform3fv(gMaterialAmbientUniform,  materialAmbient);
		gl.uniform3fv(gMaterialSpecularUniform, materialSpecular);
		gl.uniform3fv(gMaterialDiffuseUniform,  materialDefuse);
		gl.uniform1f (gMaterialShininessUniform, materialShinyness);
	}

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false,  viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();
	
	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	if(sphere)
	{
		sphere.deallocate();
	}
	
	if(gVao)
	{
		gl.deleteVertexArray(gVao);
		gVao = null;
	}
	
	if(gVbo)
	{
		gl.deleteBuffer(gVbo);
		gVbo = null;
	}
	
	if(gShaderProgramObjectPerVertex)
	{
		if(gFragmentShaderObjectPerVertex)
		{
			gl.detachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);
			gl.deleteShader(gFragmentShaderObjectPerVertex);
			gFragmentShaderObjectPerVertex = null;
		}
		
		if(gVertexShaderObjectPerVertex)
		{
			gl.detachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);
			gl.deleteShader(gVertexShaderObjectPerVertex);
			gVertexShaderObjectPerVertex = null;
		}
		
		gl.deleteProgram(gShaderProgramObjectPerVertex);
		gShaderProgramObjectPerVertex = null;
	}
	
	if(gShaderProgramObjectPerFragment)
	{
		if(gFragmentShaderObjectPerFragment)
		{
			gl.detachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);
			gl.deleteShader(gFragmentShaderObjectPerFragment);
			gFragmentShaderObjectPerFragment = null;
		}
		
		if(gVertexShaderObjectPerFragment)
		{
			gl.detachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);
			gl.deleteShader(gVertexShaderObjectPerFragment);
			gVertexShaderObjectPerFragment = null;
		}
		
		gl.deleteProgram(gShaderProgramObjectPerFragment);
		gShaderProgramObjectPerFragment = null;
	}
}
