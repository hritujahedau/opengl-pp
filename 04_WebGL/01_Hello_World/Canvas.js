// JavaScript source code

function main(){
    // get canvas from dom
    var canvas = document.getElementById("hrh"); // type inferance

    if (!canvas) {
        console.log("canvas failed");
    }
    else
    {
        console.log("Got canvas");
    }

    // retrieve the hight and width of canvas
    // positional arg
    console.log("Canvas width = " + canvas.width + " height = " + canvas.height + "\n");

    // get drawing context from the canvas
    var context = canvas.getContext("2d");

    if (!context) {
        console.log("context failed");
    }
    else {
        console.log("Got context");
    }

    // paint background by black color

    context.fillStyle = "black";

    context.fillRect(0, 0, canvas.width, canvas.height);

    // center the text
    context.textAlign = "center"; // horizontal center
    context.textBaseline = "middle"; // verticle center
    context.font = "48px Arial";

    // declare string to be displayed
    var str = "Hello Hrituja Hedau";

    // color the text foreground
    context.fillStyle = "white";

    // display the text
    context.fillText(str, canvas.width / 2, canvas.height / 2);
}
