

// JavaScript source code
var canvas = null;
var gl;
var canvas_original_width = 0, canvas_original_height = 0;
var bFullScreen = false;
var requestAnimationFrame = window.requestAnimationFrame    || 
							window.webkitAnimationFrame     ||
							window.mozRequestAnimationFrame ||
							window.opRequestAnimationFrame  ||
							window.msRequestAnimationFrame	 ;

const webGLMacros =
{
	HRH_ATTRIBUTE_POSITION : 0 ,
    HRH_ATTRIBUTE_COLOR : 1 ,
    HRH_ATTRIBUTE_NORMAL : 2 ,
	HRH_ATTRIBUTE_TEXTURE_0 : 3 ,
}

var gVertexShaderObject;
var gFragmentShaderObject;
var gShaderProgramObject;

var gVao;
var gVbo;
var gMVPUniform;

var sphere = null;

var gPerspectiveProjectionMatrix;

var year = 0, day = 0, moon = 0, sun = 0;

var stack_head = [];

var earth_texture, sun_texture, moon_texture;

function main()
{
	// get canvas from dom
    canvas = document.getElementById("hrh"); // type inferance

    if (!canvas) 
	{
        console.log("canvas failed");
    }
    else
    {
        console.log("Got canvas");
    }

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
    
	// window in buil variable , window is inherited from DOM object
	// 3rd parameter false means do bubbling
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);	
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize(); // explicit warm up resize because webgl don't call resize
	
	draw();
	
}

function keyDown(event)
{
	switch(event.keyCode)
	{
		// for 'f' or 'F'
		case 70:
			toggleFullScreen();
			break;
		case 27:
			uninitialize();
			window.close(); // may not work in firefox but will work in chrome and safari
			break;
	}
	switch(event.key)
	{
		case 'y':
			year = (year + 3) % 360;
			day = (day + 6) % 360;
			moon = (moon + 6) % 360;
			break;
		case 'Y':
			year = (year - 3) % 360;
			day = (day - 6) % 360;
			moon = (moon - 6) % 360;
			break;
		case 'd':
			day = (day + 6) % 360;
			moon = (moon + 6) % 360;
			break;
		case 'D':
			day = (day - 6) % 360;
			moon = (moon - 6) % 360;
			break;
		case 'm':
			moon = (moon + 6) % 360;
			break;
		case 'M':
			moon = (moon - 6) % 360;
			break;
		case 's':
			sun = (sun + 3) % 360;
			day = (day + 6) % 360;
			moon = (moon + 6) % 360;
			break;
		case 'S':
			sun = (sun -3) % 360;
			day = (day - 6) % 360;
			moon = (moon - 6) % 360;
			break;
	}
}

function mouseDown()
{
	
}

function toggleFullScreen()
{
	var fullscreen_element = 	document.fullscreenElement || 
								document.webkitFulllscreenElement || 
								document.mozFullScreen || 
								document.msFullscreenElement ||
								null;
								
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
		{
			canvas.requestFullscreen();			
		} 
		else if(canvas.webkitRequestFulllscreen())
		{
			canvas.webkitRequestFulllscreen();
		}
		else if(canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		} 
		else if(canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		} 
		else if ( document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		} 
		else if (document.mozCancelFullScreen)
		{
			document.mozCancelFullScreen();
		} 
		else if (document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		bFullScreen = false;
	}	
}

function init()
{
	
    // get drawing context from the canvas
    gl = canvas.getContext("webgl2");

    if (!gl) 
	{
        console.log("gl failed");
    }
    else 
	{
        console.log("Got gl");
    }
	
	gVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	
	var vertexShaderSourceCode = 
									"#version 300 es \n 								" +
									"in vec4 vPosition;									" +
									"in vec2 vTexCoord;									" +
									"out vec2 outTexCoord; 								" +
									"uniform mat4 u_mvpMatrix;							" +
									"void main()										" +
									"{													" +
									"outTexCoord = vTexCoord;							" +
									"gl_Position = u_mvpMatrix * vPosition;				" +
									"}													" ;

	gl.shaderSource(gVertexShaderObject, vertexShaderSourceCode);
	
	gl.compileShader(gVertexShaderObject);
	
	if(gl.getShaderParameter(gVertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
	var fragmentShaderSourceCode = 	"#version 300 es 		\n  										" +
									"precision highp float;												" +
									"in vec2 outTexCoord;												" +
									"out vec4 FragColor; 												" +
									"uniform highp sampler2D u_texture_sampler;							" +
									"void main()														" +
									"{																	" +
									"	FragColor = texture(u_texture_sampler,outTexCoord) ;			" +
									"}																	" ;
	
	gl.shaderSource(gFragmentShaderObject, fragmentShaderSourceCode);
	
	gl.compileShader(gFragmentShaderObject);
	
	if(gl.getShaderParameter(gFragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gShaderProgramObject = gl.createProgram();
	
	// attach vertex shader to shader program
	gl.attachShader(gShaderProgramObject, gVertexShaderObject);
	
	// attch fragment shader to shader program
	gl.attachShader(gShaderProgramObject, gFragmentShaderObject);
	
	// pri-link binding of shader object with vertex shader possition attribute
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_TEXTURE_0, "vTexCoord");

	// LINK PROGRAM
	gl.linkProgram(gShaderProgramObject);
	
	if(!gl.getProgramParameter(gShaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gMVPUniform = gl.getUniformLocation(gShaderProgramObject, "u_mvpMatrix");
	gTextureSamplerUniform = gl.getUniformLocation(gShaderProgramObject, "u_texture_sampler");

	
	sphere = new Mesh();
		
	makeSphere(sphere, 2.0, 30, 30);

	earth_texture = gl.createTexture();
	earth_texture.image = new Image();
	earth_texture.image.src = "2k_earth_daymap.png";
	earth_texture.image.onload = function (){
					gl.bindTexture(gl.TEXTURE_2D, earth_texture);
					gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
					gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
					gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
					gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, earth_texture.image);
					gl.bindTexture(gl.TEXTURE_2D, null);
				};

	sun_texture = gl.createTexture();
	sun_texture.image = new Image();
	sun_texture.image.src = "2k_sun.png";
	sun_texture.image.onload = function (){
					gl.bindTexture(gl.TEXTURE_2D, sun_texture);
					gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
					gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
					gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
					gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, sun_texture.image);
					gl.bindTexture(gl.TEXTURE_2D, null);
				};

	moon_texture = gl.createTexture();
	moon_texture.image = new Image();
	moon_texture.image.src = "2k_moon.png";
	moon_texture.image.onload = function (){
					gl.bindTexture(gl.TEXTURE_2D, moon_texture);
					gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
					gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
					gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
					gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, moon_texture.image);
					gl.bindTexture(gl.TEXTURE_2D, null);
				};
	
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);

	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	gPerspectiveProjectionMatrix = mat4.create();
	
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	// ser the viewport to match	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(gPerspectiveProjectionMatrix, 
						45.0, 
						parseFloat(canvas.width) /parseFloat(canvas.height), 
						parseFloat(canvas.height) / parseFloat(canvas.width), 
						100.0); 
	
}

function draw()
{
	// code
	gl.clear(gl.COLOR_BUFFER_BIT);
	
	gl.useProgram(gShaderProgramObject);
	
	var currentMatrix = mat4.create();

	var cameraPosition = [0.0, 0.0, 20.0];
	var cameraFocusPoints = [0.0, 0.0, 0.0];
	var cameraUpAxis = [0.0, 1.0, 0.0];

	var cameraMatrix = mat4.create();
	mat4.lookAt(cameraMatrix, cameraPosition, cameraFocusPoints, cameraUpAxis);

	stack_head.push(cameraMatrix);

	console.log("push cameraMatrix");

	// ***************************************** first sphere ***************************************** 
	var translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [0.0, 0.0, 5.0]);

	var rotateMatrix_sun = mat4.create();
	mat4.rotateY(rotateMatrix_sun, rotateMatrix_sun, degToRad(sun));

	var rotateMatrix_1 = mat4.create();
	mat4.rotateX(rotateMatrix_1, rotateMatrix_1, degToRad(90));

	var modelViewMatrix = mat4.create();
	mat4.multiply(modelViewMatrix, modelViewMatrix, cameraMatrix);
	mat4.multiply(modelViewMatrix, modelViewMatrix, translateMatrix);
	mat4.multiply(modelViewMatrix, modelViewMatrix, rotateMatrix_sun);
	mat4.multiply(modelViewMatrix, modelViewMatrix, rotateMatrix_1);

	// calculate current matrix
	mat4.multiply(currentMatrix, translateMatrix, rotateMatrix_sun);

	stack_head.push(currentMatrix);

	var modelViewProjectionMatrix = mat4.create();

	mat4.multiply(modelViewProjectionMatrix, gPerspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMVPUniform, false, modelViewProjectionMatrix);

	gl.vertexAttrib3f(webGLMacros.HRH_ATTRIBUTE_COLOR, 1.0, 1.0, 0.0);

	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, sun_texture);
	gl.uniform1i(gTextureSamplerUniform, 0);

	gl.bindVertexArray(gVao);

	sphere.draw();

	console.log("first sphere");

	// ***************************************** second sphere ***************************************** 
	
	currentMatrix = stack_head.pop();
	cameraMatrix = stack_head.pop();
	
	stack_head.push(cameraMatrix);

	var rotateMatrix_year = mat4.create();
	mat4.rotateY(rotateMatrix_year, rotateMatrix_year, degToRad(year));
	
	translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [3.0, 0.0, 7.0]);

	var scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [0.3, 0.3, 0.3]);

	var rotateMatrix_day = mat4.create();
	mat4.rotateY(rotateMatrix_day, rotateMatrix_day, degToRad(day));


	var rotateMatrix_2 = mat4.create();
	mat4.rotateX(rotateMatrix_2, rotateMatrix_2, degToRad(300));
	mat4.rotateZ(rotateMatrix_2, rotateMatrix_2, degToRad(280));
	mat4.rotateY(rotateMatrix_2, rotateMatrix_2, degToRad(350));
	

	modelViewMatrix = mat4.create();

	mat4.multiply(modelViewMatrix, modelViewMatrix, cameraMatrix);
	mat4.multiply(modelViewMatrix, modelViewMatrix, currentMatrix);
	mat4.multiply(modelViewMatrix, modelViewMatrix, rotateMatrix_year);
	mat4.multiply(modelViewMatrix, modelViewMatrix, translateMatrix);
	mat4.multiply(modelViewMatrix, modelViewMatrix, rotateMatrix_day);
	mat4.multiply(modelViewMatrix, modelViewMatrix, scaleMatrix);
	mat4.multiply(modelViewMatrix, modelViewMatrix, rotateMatrix_2);

	//currentMatrix = mat4.create();
	mat4.multiply(currentMatrix, currentMatrix, rotateMatrix_year);
	mat4.multiply(currentMatrix, currentMatrix , translateMatrix);
	stack_head.push(currentMatrix);

	mat4.multiply(modelViewProjectionMatrix, gPerspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMVPUniform, false, modelViewProjectionMatrix);

	gl.vertexAttrib3f(webGLMacros.HRH_ATTRIBUTE_COLOR, 0.0, 1.0, 1.0);

	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, earth_texture);
	gl.uniform1i(gTextureSamplerUniform, 0);

	gl.bindVertexArray(gVao);
	
	sphere.draw();

	console.log("second sphere");

	// ***************************************** third sphere ***************************************** 

	currentMatrix = stack_head.pop();

	//cameraMatrix = stack_head.pop();

	var rotateMatrix_moon = mat4.create();
	mat4.rotateY(rotateMatrix_moon, rotateMatrix_moon, degToRad(moon));

	translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [2.0, 0.2, 0.0]);

	var scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [0.2, 0.2, 0.2]);

	modelViewMatrix = mat4.create();

	mat4.multiply(modelViewMatrix, modelViewMatrix, cameraMatrix);
	mat4.multiply(modelViewMatrix, modelViewMatrix, currentMatrix);
	mat4.multiply(modelViewMatrix, modelViewMatrix, rotateMatrix_moon);
	mat4.multiply(modelViewMatrix, modelViewMatrix, translateMatrix);
	mat4.multiply(modelViewMatrix, modelViewMatrix, scaleMatrix);

	mat4.multiply(modelViewProjectionMatrix, gPerspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMVPUniform, false, modelViewProjectionMatrix);

	gl.vertexAttrib3f(webGLMacros.HRH_ATTRIBUTE_COLOR, 1.0, 1.0, 1.0);

	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, moon_texture);
	gl.uniform1i(gTextureSamplerUniform, 0);

	gl.bindVertexArray(gVao);
	
	sphere.draw();

	stack_head.pop();
	
	requestAnimationFrame(draw, canvas);
}

function degToRad(degree)
{
	return ( degree * Math.PI / 180.0 );
}

function uninitialize()
{
	if(sphere)
	{
		sphere.deallocate();
	}
	
	if(sun_texture)
	{
		gl.deleteTexture(sun_texture);
		sun_texture = 0;
	}

	if(moon_texture)
	{
		gl.deleteTexture(moon_texture);
		moon_texture = 0;
	}

	if(earth_texture)
	{
		gl.deleteTexture(earth_texture);
		earth_texture = 0;
	}

	if(gVao)
	{
		gl.deleteVertexArray(gVao);
		gVao = null;
	}
	
	if(gVbo)
	{
		gl.deleteBuffer(gVbo);
		gVbo = null;
	}
	
	if(gShaderProgramObject)
	{
		if(gFragmentShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gFragmentShaderObject);
			gl.deleteShader(gFragmentShaderObject);
			gFragmentShaderObject = null;
		}
		
		if(gVertexShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gVertexShaderObject);
			gl.deleteShader(gVertexShaderObject);
			gVertexShaderObject = null;
		}
		
		gl.deleteProgram(gShaderProgramObject);
		gShaderProgramObject = null;
	}
}
