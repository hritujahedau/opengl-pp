// JavaScript source code
var canvas = null;
var gl;
var canvas_original_width = 0, canvas_original_height = 0;
var bFullScreen = false;
var requestAnimationFrame = window.requestAnimationFrame    || 
							window.webkitAnimationFrame     ||
							window.mozRequestAnimationFrame ||
							window.opRequestAnimationFrame  ||
							window.msRequestAnimationFrame	 ;

const webGLMacros =
{
	HRH_ATTRIBUTE_POSITION : 0 ,
    HRH_ATTRIBUTE_COLOR : 1 ,
    HRH_ATTRIBUTE_NORMAL : 2 ,
	HRH_ATTRIBUTE_TEXTURE_0 : 3 ,
};

var gVertexShaderObjectPerVertex;
var gFragmentShaderObjectPerVertex;

var gVertexShaderObjectPerFragment;
var gFragmentShaderObjectPerFragment;

var gShaderProgramObjectPerVertex;
var gShaderProgramObjectPerFragment;
var gShaderProgramObject;

var gVao;
var gVbo;
var gMVPUniform;

var sphere = null;

var gModelUniform;
var gViewUniform;
var gPerspectiveUniform;

var gMaterialSpecularUniform;
var gMaterialAmbientUniform;
var gMaterialDiffuseUniform;
var gMaterialShininessUniform;

var gIsLightOnUniform;

var isLightOn = 0;
var isPerFragmentLight = 0;
var isPerVertexLight = 0;

var lightPosition = new Float32Array ([ 0.0, 0.0, 0.0, 1.0 ]);						
var lightAmbient = new Float32Array ([  0.0, 0.0, 0.0 ]);						
var lightDiffuse = new Float32Array ([  1.0, 1.0, 1.0 ]);							
var lightSpecular = new Float32Array ([ 1.0, 1.0, 1.0 ]);

var gLightAmbientUniform;
var gLightSpecularUniform;
var gLightDiffusedUniform;
var gLightPossitionUniform;

var materialAmbient =  new Float32Array ([ 0.0, 0.0, 0.0 ]);
var materialDiffuse =  new Float32Array ([ 1.0, 1.0, 1.0 ]);
var materialSpecular = new Float32Array ([ 1.0, 1.0, 1.0  ]);
var materialShininess = 128.0;

var bAnimate = false;

var angle_red = 360.0,angle_blue = 0.0, angle_green = 360.0;
var sin_angle = 0.0, cos_angle = 0.0, radius = 20.0;
var gPerspectiveProjectionMatrix;
var key_x = 0, key_y = 0, key_z = 0;
var angle_for_light = 0.0;

var scaleSphere = 0.5, sphere_translate_x = 0.0, sphere_translate_y = 2.0, sphere_translate_z = 0.0 , translate_z = 8.0, diiference_y = 0.7;

/**********************************************************************************************/
// first column
// 1
var materialAmbient_emerald    = new Float32Array ([0.0215, 0.1745, 0.0215, 1.0 ]);
var materialDefuse_emerald     = new Float32Array ([ 0.07568, 0.61424, 0.07568 ]);
var materialSpecular_emerald   = new Float32Array ([ 0.633, 0.727811, 0.633 ]);
var materialShinyness_emerald  = 0.6 * 128;

// 2
var materialAmbient_jade     =  new Float32Array ([ 0.135, 0.2225, 0.1575, 1.0 ]);
var materialDefuse_jade      =  new Float32Array ([ 0.54, 0.89, 0.63 ]);
var materialSpecular_jade    =  new Float32Array ([ 0.316228, 0.316228, 0.316228 ]);
var materialShinyness_jade   = 0.1 * 128;

// 3
var materialAmbient_obsidian    = new Float32Array ([ 0.05375, 0.05, 0.06625, 1.0 ]);
var materialDefuse_obsidian     = new Float32Array ([ 0.18275, 0.17, 0.22525 ]);
var materialSpecular_obsidian   = new Float32Array ([ 0.332741, 0.328634, 0.346435 ]);
var materialShinyness_obsidian  = 0.3 * 128;

// 4 pearl
var materialAmbient_pearl    = new Float32Array ([ 0.25, 0.20725, 0.20725, 1.0 ]);
var materialDefuse_pearl     = new Float32Array ([ 1.0, 0.829, 0.829 ]);
var materialSpecular_pearl   = new Float32Array ([ 0.296648, 0.296648, 0.296648 ]);
var materialShinyness_pearl  = 0.088 * 128;

// 5 ruby 
var materialAmbient_ruby    =  new Float32Array ([ 0.1745, 0.01175, 0.01175, 1.0 ]);
var materialDefuse_ruby     =  new Float32Array ([ 0.61424, 0.04136, 0.04136 ]);
var materialSpecular_ruby   =  new Float32Array ([ 0.727811, 0.626959, 0.626959 ]);
var materialShinyness_ruby  = 0.6 * 128;

// 6 turquoise
var materialAmbient_turquoise   =  new Float32Array ([ 0.1, 0.18725, 0.1745, 1.0 ]);
var materialDefuse_turquoise    =  new Float32Array ([ 0.396, 0.74151, 0.69102 ]);
var materialSpecular_turquoise  =  new Float32Array ([ 0.297254, 0.30829, 0.306678]);
var materialShinyness_turquoise = 0.1 * 128;

// second column
// 7 brass 
var materialAmbient_brass   = new Float32Array ([ 0.329412, 0.223529, 0.027451, 1.0 ]);
var materialDefuse_brass    = new Float32Array ([ 0.780392, 0.568627, 0.113725 ]);
var materialSpecular_brass  = new Float32Array ([ 0.992157, 0.941176, 0.807843 ]);
var materialShinyness_brass = 0.21794872 * 128;

// 8 bronze
var materialAmbient_bronze  = new Float32Array ([ 0.2125, 0.1275, 0.054, 1.0 ]);
var materialDefuse_bronze  =   new Float32Array ([ 0.714, 0.4284, 0.18144  ]);
var materialSpecular_bronze  = new Float32Array ([ 0.393548, 0.271906, 0.166721 ]);
var materialShinyness_bronze = 0.2 * 128;

// 9 chrome
var materialAmbient_chrome  =  new Float32Array ([ 0.25, 0.25, 0.25, 1.0 ]);
var materialDefuse_chrome  =   new Float32Array ([ 0.4, 0.4, 0.4]);
var materialSpecular_chrome  = new Float32Array ([ 0.774597, 0.774597, 0.774597 ]);
var materialShinyness_chrome = 0.2 * 128;

// 10 4th sphere on 2nd column, copper  
var materialAmbient_copper  =  new Float32Array ([ 0.19125, 0.19125, 0.0225, 1.0 ]);
var materialDefuse_copper  =   new Float32Array ([ 0.7038, 0.27048, 0.0828 ]);
var materialSpecular_copper  = new Float32Array ([ 0.256777, 0.137622, 0.086014 ]);
var materialShinyness_copper = 0.1 * 128;

// 11 5th sphere on 2nd column, gold
var materialAmbient_gold   = new Float32Array ([ 0.24725, 0.1995, 0.0745, 1.0      ]);
var materialDefuse_gold    = new Float32Array ([ 0.75164, 0.60648, 0.22648  ]);
var materialSpecular_gold  = new Float32Array ([ 0.628281, 0.555802, 0.366065 ]);
var materialShinyness_gold  = 0.4 * 128;

// 12 6th sphere on 2nd column, silver
var materialAmbient_silver   = new Float32Array ([ 0.19225, 0.1995, 0.19225, 1.0     ]);
var materialDefuse_silver    = new Float32Array ([ 0.50754, 0.50754, 0.50754  ]);
var materialSpecular_silver  = new Float32Array ([ 0.508273, 0.508273, 0.508273 ]);
var materialShinyness_silver = 0.4 * 128;

// 13 1st sphere on 3rd column, black 
var materialAmbient_black   = new Float32Array ([ 0.0, 0.0, 0.0, 1.0    ]);
var materialDefuse_black    = new Float32Array ([ 0.01, 0.01, 0.01 ]);
var materialSpecular_black  = new Float32Array ([ 0.5, 0.5, 0.5 ]);
var materialShinyness_black = 0.25 * 128;

// 14 2nd sphere on 3rd column, cyan
var materialAmbient_cyan   = new Float32Array ([ 0.0, 0.1, 0.06, 1.0 ]);
var materialDefuse_cyan    = new Float32Array ([ 0.0, 0.50980392, 0.50980392 ]);
var materialSpecular_cyan  = new Float32Array ([ 0.50196078, 0.50196078, 0.50196078]);
var materialShinyness_cyan = 0.25 * 128;

// 15 3rd sphere on 2nd column, green
var materialAmbient_green   = new Float32Array ([ 0.0, 0.0, 0.0, 1.0    ]);
var materialDefuse_green    = new Float32Array ([ 0.1, 0.35, 0.1 ]);
var materialSpecular_green  = new Float32Array ([ 0.45, 0.55, 0.45 ]);
var materialShinyness_green = 0.25 * 128;

// 16 4th sphere on 3rd column, red
var materialAmbient_red   = new Float32Array ([ 0.0, 0.0, 0.0, 1.0 ]);
var materialDefuse_red    = new Float32Array ([ 0.5, 0.0, 0.0]);
var materialSpecular_red  = new Float32Array ([ 0.7, 0.6, 0.6]);
var materialShinyness_red = 0.25 * 128;

// 17 5th sphere on 3rd column, white
var materialAmbient_white  =  new Float32Array ([ 0.0, 0.0, 0.0, 1.0    ]);
var materialDefuse_white  =   new Float32Array ([ 0.55, 0.55, 0.55 ]);
var materialSpecular_white  = new Float32Array ([ 0.7, 0.7, 0.7    ]);
var materialShinyness_white = 0.25 * 128;

// 18 6th sphere on 3rd column, yellow plastic
var materialAmbient_plastic   = new Float32Array ([ 0.0, 0.0, 0.0, 1.0    ]);
var materialDefuse_plastic    = new Float32Array ([ 0.5, 0.5, 0.0 ]);
var materialSpecular_plastic  = new Float32Array ([ 0.60, 0.60, 0.50 ]);
var materialShinyness_plastic = 0.25 * 128;

// 19  1st sphere on 4th column, black
var materialAmbient_black_2   = new Float32Array ([ 0.02, 0.02, 0.02, 1.0 ]);
var materialDefuse_black_2    = new Float32Array ([ 0.01, 0.01, 0.01 ]);
var materialSpecular_black_2  = new Float32Array ([ 0.4, 0.4, 0.4    ]);
var materialShinyness_black_2 = 0.078125 * 128;

// 20  2nd sphere on 4th column, cyan
var materialAmbient_cyan_2   = new Float32Array ([ 0.0, 0.05, 0.05, 1.0 ]);
var materialDefuse_cyan_2    = new Float32Array ([ 0.4, 0.5, 0.5 ]);
var materialSpecular_cyan_2  = new Float32Array ([ 0.04, 0.7, 0.7 ]);
var materialShinyness_cyan_2 = 0.078125 * 128;

// 21  3rd sphere on 4th column, green
var materialAmbient_green_2   = new Float32Array ([ 0.0, 0.05, 0.05, 1.0 ]);
var materialDefuse_green_2    = new Float32Array ([ 0.4, 0.5, 0.4  ]);
var materialSpecular_green_2  = new Float32Array ([ 0.04, 0.7, 0.04 ]);
var materialShinyness_green_2 = 0.078125 * 128;

// 22   4th sphere on 4th column, red 
var materialAmbient_red_2   = new Float32Array ([ 0.05, 0.0, 0.0, 1.0  ]);
var materialDefuse_red_2    = new Float32Array ([ 0.5, 0.4, 0.4 ]);
var materialSpecular_red_2  = new Float32Array ([ 0.7, 0.04, 0.04]);
var materialShinyness_red_2 = 0.078125 * 128;

// 23   5th sphere on 4th column, white
var materialAmbient_white_2   = new Float32Array ([ 0.05, 0.05, 0.05, 1.0 ]);
var materialDefuse_white_2    = new Float32Array ([ 0.5, 0.5, 0.5]);
var materialSpecular_white_2  = new Float32Array ([ 0.7, 0.7, 0.7]);
var materialShinyness_white_2 = 0.078125 * 128;

// 24   6th sphere on 4th column, yellow rubber
var materialAmbient_rubber   = new Float32Array ([ 0.05, 0.05, 0.0, 1.0 ]);
var materialDefuse_rubber    = new Float32Array ([ 0.5, 0.5, 0.4  ]);
var materialSpecular_rubber  = new Float32Array ([ 0.7, 0.7, 0.04 ]);
var materialShinyness_rubber = 0.078125 * 128;


function main()
{
	// get canvas from dom
    canvas = document.getElementById("hrh"); // type inferance

    if (!canvas) 
	{
        console.log("canvas failed");
    }
    else
    {
        console.log("Got canvas");
    }

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
    
	// window in buil variable , window is inherited from DOM object
	// 3rd parameter false means do bubbling
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);	
	window.addEventListener("resize", resize, false);
	
	init();
	resize(); // explicit warm up resize because webgl don't call resize
	draw();
	
}

function keyDown(event)
{
	switch(event.keyCode)
	{
		case 27:
			uninitialize();
			window.close(); // may not work in firefox but will work in chrome and safari
			break;
	}
	switch(event.key)
	{
			case 'a':
			case 'A':
				if (bAnimate == false)
				{
					bAnimate = true;
				}
				else
				{
					bAnimate = false;
				}
				break;
			case 'l':
			case 'L':
				if (isLightOn == 1)
				{
					isLightOn = 0;
					angle_for_light = 0.0;
				}
				else
				{
					key_x = 0;
					key_y = 0;
					key_z = 0;
					isLightOn = 1;
				}
				break;
			case 'q':
			case 'Q':
				toggleFullScreen();
				break;
			case 'f':
			case 'F':
				if(isPerFragmentLight == 1)
				{
					isPerFragmentLight = 0;
					isLightOn = 0;
					bAnimate = 0;
					
				}
				else
				{
					isPerFragmentLight = 1;
					isPerVertexLight = 0;
					isLightOn = 1;
					bAnimate = 1;
				}
				break;
			case 'v':
			case 'V':
				if(isPerVertexLight == 1)
				{
					isPerVertexLight = 0;					
					isLightOn = 0;				
				}
				else
				{
					isPerVertexLight = 1;
					isPerFragmentLight =0;					
					isLightOn = 1;
					bAnimate = 1;
				}
				break;
			case 'x':
			case 'X':
				key_x = 1;
				key_y = 0;
				key_z = 0;
				break;
			case 'y':
			case 'Y':
				key_x = 0;
				key_y = 1;
				key_z = 0;
				break;
			case 'z':
			case 'Z':
				key_x = 0;
				key_y = 0;
				key_z = 1;
				break;

		}
}

function mouseDown()
{
	
}

function toggleFullScreen()
{
	var fullscreen_element = 	document.fullscreenElement || 
								document.webkitFulllscreenElement || 
								document.mozFullScreen || 
								document.msFullscreenElement ||
								null;
								
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
		{
			canvas.requestFullscreen();			
		} 
		else if(canvas.webkitRequestFulllscreen())
		{
			canvas.webkitRequestFulllscreen();
		}
		else if(canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		} 
		else if(canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		} 
		else if ( document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		} 
		else if (document.mozCancelFullScreen)
		{
			document.mozCancelFullScreen();
		} 
		else if (document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		bFullScreen = false;
	}	
}

function init()
{
	
    // get drawing context from the canvas
    gl = canvas.getContext("webgl2");

    if (!gl) 
	{
        console.log("gl failed");
    }
    else 
	{
        console.log("Got gl");
    }
	
	gVertexShaderObjectPerVertex = gl.createShader(gl.VERTEX_SHADER);
	
	var vertexShaderSourceCodePerVertex = 
			"#version 300 es \n" +
			"in vec4 v_position;" +
			"in vec3 v_normals; " +
			"uniform mat4 u_modelMatrix;" +
			"uniform mat4 u_viewMatrix;" +
			"uniform mat4 u_projectionMatrix;" +
			"uniform int u_isLightOn;" +
			"uniform vec3 u_lightAmbient;" +
			"uniform vec3 u_lightSpecular;" +
			"uniform vec3 u_lightDiffuse;" +
			"uniform vec4 u_lightposition; " +
			"uniform vec3 u_materialAmbient; " +
			"uniform vec3 u_materialSpecular; " +
			"uniform vec3 u_materialDiffuse; " +
			"uniform float u_materialshininess; " +
			"out vec3 phoung_ads_lighting;" +
			"vec4 eye_coordinates;" +
			"vec3 transformed_normal, light_direction, reflection_vector, view_vector;" +
			"vec3 ambient, diffuse_light, specular;" +
			"void main()" +
			"{" +
			"if (u_isLightOn == 1) " +
			"{" +
				"eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" +
				"transformed_normal = normalize( mat3( u_viewMatrix * u_modelMatrix ) * v_normals); " +
				"light_direction = normalize( vec3 ( ( u_lightposition ) - eye_coordinates) ); " +
				"reflection_vector = reflect (-light_direction, transformed_normal); " +
				"view_vector = normalize ( vec3 ( -eye_coordinates)); " +
				"ambient = u_lightAmbient * u_materialAmbient; " +
				"diffuse_light = u_lightDiffuse * u_materialDiffuse * max ( dot (light_direction , transformed_normal), 0.0); " +
				"specular = u_lightSpecular * u_materialSpecular * pow( max ( dot ( reflection_vector , view_vector), 0.0) , u_materialshininess);" +
				"phoung_ads_lighting = ambient + diffuse_light + specular;" +
			"}" +
			"else" +
			"{" +
			"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" +
			"}"+
			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" +
			"}";

	gl.shaderSource(gVertexShaderObjectPerVertex, vertexShaderSourceCodePerVertex);
	
	gl.compileShader(gVertexShaderObjectPerVertex);
	
	if(gl.getShaderParameter(gVertexShaderObjectPerVertex, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObjectPerVertex);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gFragmentShaderObjectPerVertex = gl.createShader(gl.FRAGMENT_SHADER);
	
	var fragmentShaderSourceCodePerVertex = 	
									"#version 300 es \n"+
									"precision highp float;" +
									"in vec3 phoung_ads_lighting;" +
									"out vec4 FragColor; " +
									"void main()"+
									"{" +
									"FragColor = vec4(phoung_ads_lighting, 1.0); " +
									"}" ;
	
	gl.shaderSource(gFragmentShaderObjectPerVertex, fragmentShaderSourceCodePerVertex);
	
	gl.compileShader(gFragmentShaderObjectPerVertex);
	
	if(gl.getShaderParameter(gFragmentShaderObjectPerVertex, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObjectPerVertex);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gShaderProgramObjectPerVertex = gl.createProgram();
	
	// attach vertex shader to shader program
	gl.attachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);
	
	// attch fragment shader to shader program
	gl.attachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);
	
	// pri-link binding o shader object with vertex shader possition attribute
	gl.bindAttribLocation(gShaderProgramObjectPerVertex, webGLMacros.HRH_ATTRIBUTE_POSITION, "v_position");
	gl.bindAttribLocation(gShaderProgramObjectPerVertex, webGLMacros.HRH_ATTRIBUTE_NORMAL, "v_normals");

	// LINK PROGRAM
	gl.linkProgram(gShaderProgramObjectPerVertex);
	
	if(!gl.getProgramParameter(gShaderProgramObjectPerVertex, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObjectPerVertex);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	/*********************************** Per Fragment ****************************************/
	
	gVertexShaderObjectPerFragment = gl.createShader(gl.VERTEX_SHADER);
	
	var vertexShaderSourceCodePerFragment = 
									"#version 300 es\n"  +
									"in vec4 v_position;" +
									"in vec3 v_normals; " +
									"uniform mat4 u_modelMatrix;" +
									"uniform mat4 u_viewMatrix;" +
									"uniform mat4 u_projectionMatrix;" +
									"uniform int u_isLightOn;" +
									"uniform vec4 u_lightposition; " +
									"out vec3 light_direction; "+
									"out vec3 transformed_normal;"+
									"out vec3 view_vector;"+
									"void main()" +
									"{" +
									"if (u_isLightOn == 1) " +
									"{" +
										"vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" +
										"transformed_normal =  mat3( u_viewMatrix * u_modelMatrix ) * v_normals;" +
										"light_direction = vec3 ( u_lightposition  - eye_coordinates);" +
										"view_vector =  vec3 ( -eye_coordinates);" +
									"}" +
									"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" +
									"}";

	gl.shaderSource(gVertexShaderObjectPerFragment, vertexShaderSourceCodePerFragment);
	
	gl.compileShader(gVertexShaderObjectPerFragment);
	
	if(gl.getShaderParameter(gVertexShaderObjectPerFragment, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObjectPerFragment);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gFragmentShaderObjectPerFragment = gl.createShader(gl.FRAGMENT_SHADER);
	
	var fragmentShaderSourceCodePerFragment = 	
											"#version 300 es\n" +
											"precision highp float;" +
											"vec3 phoung_ads_lighting;" +
											"out vec4 FragColor; " +
											"in vec3 light_direction; "+
											"in vec3 transformed_normal;"+
											"in vec3 view_vector;"+
											"vec3 normalized_light_direction; "+
											"vec3 normalized_transformed_normal;"+
											"vec3 normalized_view_vector;"+
											"uniform highp vec3 u_lightAmbient;" +
											"uniform highp vec3 u_lightSpecular;" +
											"uniform highp vec3 u_lightDiffuse;" +
											"uniform highp vec3 u_materialAmbient; " +
											"uniform highp vec3 u_materialSpecular; "+
											"uniform highp vec3 u_materialDiffuse; "+
											"uniform highp float u_materialshininess; "+
											"uniform highp int u_isLightOn;" +
											"vec3 reflection_vector, ambient, diffuse_light, specular;"+
											"void main()" +
											"{"+
											"if (u_isLightOn == 1) " +
											"{" +
												"normalized_view_vector = normalize(view_vector);"+
												"normalized_transformed_normal = normalize(transformed_normal);"+
												"normalized_light_direction = normalize(light_direction);"+
												"reflection_vector = reflect (-normalized_light_direction, normalized_transformed_normal); " +
												"ambient = u_lightAmbient  * u_materialAmbient; " +
												"diffuse_light = u_lightDiffuse * u_materialDiffuse * max ( dot (normalized_light_direction , normalized_transformed_normal), 0.0); "+
												"specular = u_lightSpecular  * u_materialSpecular * pow( max ( dot ( reflection_vector , normalized_view_vector), 0.0) , u_materialshininess);" +
												"phoung_ads_lighting =  + ambient + diffuse_light + specular;" +
											"}"+
											"else"+
											"{"+
												"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" +
											"}"+
											"FragColor = vec4(phoung_ads_lighting, 1.0); " +
											"}";
	
	gl.shaderSource(gFragmentShaderObjectPerFragment, fragmentShaderSourceCodePerFragment);
	
	gl.compileShader(gFragmentShaderObjectPerFragment);
	
	if(gl.getShaderParameter(gFragmentShaderObjectPerFragment, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObjectPerFragment);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gShaderProgramObjectPerFragment = gl.createProgram();
	
	// attach vertex shader to shader program
	gl.attachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);
	
	// attch fragment shader to shader program
	gl.attachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);
	
	// pri-link binding o shader object with vertex shader possition attribute
	gl.bindAttribLocation(gShaderProgramObjectPerFragment, webGLMacros.HRH_ATTRIBUTE_POSITION, "v_position");
	gl.bindAttribLocation(gShaderProgramObjectPerFragment, webGLMacros.HRH_ATTRIBUTE_NORMAL, "v_normals");

	// LINK PROGRAM
	gl.linkProgram(gShaderProgramObjectPerFragment);
	
	if(!gl.getProgramParameter(gShaderProgramObjectPerFragment, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObjectPerFragment);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
		
	sphere = new Mesh();
		
	makeSphere(sphere, 2.0, 30, 30);
	
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	gl.clearColor(0.25, 0.25, 0.25, 1.0);

	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	gPerspectiveProjectionMatrix = mat4.create();
	
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	// ser the viewport to match	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(gPerspectiveProjectionMatrix, 
						45.0, 
						parseFloat(canvas.width) /parseFloat(canvas.height), 
						parseFloat(canvas.height) / parseFloat(canvas.width), 
						100.0); // no 0.1f
	
}

function draw()
{
	// code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gShaderProgramObject = (isPerVertexLight == 0) ? gShaderProgramObjectPerFragment : gShaderProgramObjectPerVertex  ;
	
	gl.useProgram(gShaderProgramObject);
	
	// VARIABLE DECLARATION
	var scaleSphere = 0.3, sphere_translate_x = 0.0, sphere_translate_y = 5.0, sphere_translate_z = -12.0, translate_z = 12.0, diference_y = 2.0;

	var translateMatrix = mat4.create();
	var scaleMatrix = mat4.create();
	var modelMatrix = mat4.create();

	var cameraPosition = [0.0, 0.0, translate_z];
	var target = [0.0, 0.0, 0.0];
	var up = [ 0.0, 1.0, 0.0 ];
	//var viewMatrix = mat4.lookAt( cameraPosition, target, up ); 
	
	var viewMatrix = mat4.create();
	//mat4.lookAt(viewMatrix, 0.0, 0.0, translate_z, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0 ); 

	// get MVP uniform location
	gModelUniform       = gl.getUniformLocation(gShaderProgramObject, "u_modelMatrix");
	gViewUniform        = gl.getUniformLocation(gShaderProgramObject, "u_viewMatrix");
	gPerspectiveUniform = gl.getUniformLocation(gShaderProgramObject, "u_projectionMatrix");
	gIsLightOnUniform   = gl.getUniformLocation(gShaderProgramObject, "u_isLightOn");
	
	gLightAmbientUniform   = gl.getUniformLocation(gShaderProgramObject, "u_lightAmbient");
	gLightSpecularUniform  = gl.getUniformLocation(gShaderProgramObject, "u_lightSpecular");
	gLightDiffusedUniform  = gl.getUniformLocation(gShaderProgramObject, "u_lightDiffuse");
	gLightPossitionUniform = gl.getUniformLocation(gShaderProgramObject, "u_lightposition");
	
	gMaterialDiffuseUniform   = gl.getUniformLocation(gShaderProgramObject, "u_materialDiffuse");
	gMaterialAmbientUniform   = gl.getUniformLocation(gShaderProgramObject, "u_materialAmbient");
	gMaterialSpecularUniform  = gl.getUniformLocation(gShaderProgramObject, "u_materialSpecular");
	gMaterialShininessUniform = gl.getUniformLocation(gShaderProgramObject, "u_materialshininess");
		
	sin_angle = radius * Math.sin(degToRad(angle_for_light));
	cos_angle = radius * Math.cos(degToRad(angle_for_light));

	var light_position = -8.0;
	if (key_x == 1)
	{
		lightPosition[0] = 0.0;
		lightPosition[1] = sin_angle;
		lightPosition[2] = light_position + cos_angle;
		lightPosition[3] = 1.0;
	} else if (key_y == 1)
	{
		lightPosition[0] = sin_angle;
		lightPosition[1] = 0.0;
		lightPosition[2] = light_position + cos_angle;
		lightPosition[3] = 1.0;
	} else if (key_z == 1)
	{
		lightPosition[0] = sin_angle;
		lightPosition[1] = cos_angle;
		lightPosition[2] = light_position;
		lightPosition[3] = 1.0;
	}
	
	if (key_x == 1 || key_y == 1 || key_z == 1)
	{
		if (angle_for_light >= 360.0)
		{
			angle_for_light = 0.0;
		}
		angle_for_light = angle_for_light + 0.5;
	}
	
	if (isLightOn == 1)
	{
		gl.uniform4fv(gLightPossitionUniform, lightPosition);
		gl.uniform3fv(gLightDiffusedUniform, lightDiffuse);
		gl.uniform3fv(gLightAmbientUniform, lightAmbient);
		gl.uniform3fv(gLightSpecularUniform, lightSpecular);
	}

	// set materials
	/********************************************First Row**************************************************************/
	// 1 sphere
	if (isLightOn == 1)
	{
		gl.uniform3fv( gMaterialAmbientUniform, materialAmbient_emerald );
		gl.uniform1f ( gMaterialShininessUniform, materialShinyness_emerald );
		gl.uniform3fv( gMaterialSpecularUniform, materialSpecular_emerald );
		gl.uniform3fv( gMaterialDiffuseUniform, materialDefuse_emerald );
	}
	
	mat4.translate(translateMatrix, translateMatrix, [sphere_translate_x - 4.0, sphere_translate_y, sphere_translate_z]);
	mat4.scale(scaleMatrix, scaleMatrix, [scaleSphere, scaleSphere, scaleSphere]);
	mat4.multiply(modelMatrix, translateMatrix, scaleMatrix);

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();
	
	// 2 sphere
	if (isLightOn == 1)
	{
		gl.uniform3fv( gMaterialAmbientUniform, materialAmbient_jade );
		gl.uniform1f ( gMaterialShininessUniform, materialShinyness_jade );
		gl.uniform3fv( gMaterialSpecularUniform, materialSpecular_jade );
		gl.uniform3fv( gMaterialDiffuseUniform, materialDefuse_jade );
	}
	
	translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [sphere_translate_x - 1.25, sphere_translate_y, sphere_translate_z]);
	
	scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [scaleSphere, scaleSphere, scaleSphere]);
	
	modelMatrix = mat4.create();
	mat4.multiply(modelMatrix, translateMatrix, scaleMatrix);

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();
	

	// 3 sphere
	if (isLightOn == 1)
	{
		gl.uniform3fv( gMaterialAmbientUniform, materialAmbient_obsidian );
		gl.uniform1f ( gMaterialShininessUniform, materialShinyness_obsidian );
		gl.uniform3fv( gMaterialSpecularUniform, materialSpecular_obsidian );
		gl.uniform3fv( gMaterialDiffuseUniform, materialDefuse_obsidian );
	}
	
	translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [sphere_translate_x + 1.25, sphere_translate_y, sphere_translate_z]);
	
	scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [scaleSphere, scaleSphere, scaleSphere]);
	
	modelMatrix = mat4.create();
	mat4.multiply(modelMatrix, translateMatrix, scaleMatrix);

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();

	// 4 sphere
	if (isLightOn == 1)
	{
		gl.uniform3fv( gMaterialAmbientUniform, materialAmbient_pearl );
		gl.uniform1f ( gMaterialShininessUniform, materialShinyness_pearl );
		gl.uniform3fv( gMaterialSpecularUniform, materialSpecular_pearl );
		gl.uniform3fv( gMaterialDiffuseUniform, materialDefuse_pearl );
	}
	
	translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [sphere_translate_x + 4, sphere_translate_y, sphere_translate_z]);
	
	scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [scaleSphere, scaleSphere, scaleSphere]);
	
	modelMatrix = mat4.create();
	mat4.multiply(modelMatrix, translateMatrix, scaleMatrix);

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();

	/********************************************Second Row**************************************************************/
	// first sphere
	if (isLightOn == 1)
	{
		gl.uniform3fv( gMaterialAmbientUniform, materialAmbient_ruby );
		gl.uniform1f ( gMaterialShininessUniform, materialShinyness_ruby );
		gl.uniform3fv( gMaterialSpecularUniform, materialSpecular_ruby );
		gl.uniform3fv( gMaterialDiffuseUniform, materialDefuse_ruby );
	}
	
	translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [sphere_translate_x - 4, sphere_translate_y - diference_y, sphere_translate_z]);
	
	scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [scaleSphere, scaleSphere, scaleSphere]);
	
	modelMatrix = mat4.create();
	mat4.multiply(modelMatrix, translateMatrix, scaleMatrix);

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();

	// second sphere
	if (isLightOn == 1)
	{
		gl.uniform3fv( gMaterialAmbientUniform, materialAmbient_turquoise );
		gl.uniform1f ( gMaterialShininessUniform, materialShinyness_turquoise );
		gl.uniform3fv( gMaterialSpecularUniform, materialSpecular_turquoise );
		gl.uniform3fv( gMaterialDiffuseUniform, materialDefuse_turquoise );
	}
	
	translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [sphere_translate_x - 1.25, sphere_translate_y - diference_y, sphere_translate_z]);
	
	scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [scaleSphere, scaleSphere, scaleSphere]);
	
	modelMatrix = mat4.create();
	mat4.multiply(modelMatrix, translateMatrix, scaleMatrix);

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();
	
	// third sphere
	if (isLightOn == 1)
	{
		gl.uniform3fv( gMaterialAmbientUniform, materialAmbient_brass );
		gl.uniform1f ( gMaterialShininessUniform, materialShinyness_brass );
		gl.uniform3fv( gMaterialSpecularUniform, materialSpecular_brass );
		gl.uniform3fv( gMaterialDiffuseUniform, materialDefuse_brass );
	}
	
	translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [sphere_translate_x + 1.25, sphere_translate_y - diference_y, sphere_translate_z]);
	
	scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [scaleSphere, scaleSphere, scaleSphere]);
	
	modelMatrix = mat4.create();
	mat4.multiply(modelMatrix, translateMatrix, scaleMatrix);

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();

	// forth sphere
	if (isLightOn == 1)
	{
		gl.uniform3fv( gMaterialAmbientUniform, materialAmbient_bronze );
		gl.uniform1f ( gMaterialShininessUniform, materialShinyness_bronze );
		gl.uniform3fv( gMaterialSpecularUniform, materialSpecular_bronze );
		gl.uniform3fv( gMaterialDiffuseUniform, materialDefuse_bronze );
	}
	
	translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [sphere_translate_x + 4, sphere_translate_y - diference_y, sphere_translate_z]);
	
	scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [scaleSphere, scaleSphere, scaleSphere]);
	
	modelMatrix = mat4.create();
	mat4.multiply(modelMatrix, translateMatrix, scaleMatrix);

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();
	
	/********************************************Third Row**************************************************************/
	diference_y = diference_y + diference_y;
	// first sphere
	if (isLightOn == 1)
	{
		gl.uniform3fv( gMaterialAmbientUniform, materialAmbient_chrome );
		gl.uniform1f ( gMaterialShininessUniform, materialShinyness_chrome );
		gl.uniform3fv( gMaterialSpecularUniform, materialSpecular_chrome );
		gl.uniform3fv( gMaterialDiffuseUniform, materialDefuse_chrome );
	}
	
	translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [sphere_translate_x - 4, sphere_translate_y - diference_y, sphere_translate_z]);
	
	scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [scaleSphere, scaleSphere, scaleSphere]);
	
	modelMatrix = mat4.create();
	mat4.multiply(modelMatrix, translateMatrix, scaleMatrix);

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();

	// second sphere
	if (isLightOn == 1)
	{
		gl.uniform3fv( gMaterialAmbientUniform, materialAmbient_copper );
		gl.uniform1f ( gMaterialShininessUniform, materialShinyness_copper );
		gl.uniform3fv( gMaterialSpecularUniform, materialSpecular_copper );
		gl.uniform3fv( gMaterialDiffuseUniform, materialDefuse_copper );
	}
	
	translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [sphere_translate_x - 1.25, sphere_translate_y - diference_y, sphere_translate_z]);
	
	scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [scaleSphere, scaleSphere, scaleSphere]);
	
	modelMatrix = mat4.create();
	mat4.multiply(modelMatrix, translateMatrix, scaleMatrix);

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();
	
	// third sphere
	if (isLightOn == 1)
	{
		gl.uniform3fv( gMaterialAmbientUniform, materialAmbient_gold );
		gl.uniform1f ( gMaterialShininessUniform, materialShinyness_gold );
		gl.uniform3fv( gMaterialSpecularUniform, materialSpecular_gold );
		gl.uniform3fv( gMaterialDiffuseUniform, materialDefuse_gold );
	}
	
	translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [sphere_translate_x + 1.25, sphere_translate_y - diference_y, sphere_translate_z]);
	
	scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [scaleSphere, scaleSphere, scaleSphere]);
	
	modelMatrix = mat4.create();
	mat4.multiply(modelMatrix, translateMatrix, scaleMatrix);

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();

	// forth sphere
	if (isLightOn == 1)
	{
		gl.uniform3fv( gMaterialAmbientUniform, materialAmbient_silver );
		gl.uniform1f ( gMaterialShininessUniform, materialShinyness_silver );
		gl.uniform3fv( gMaterialSpecularUniform, materialSpecular_silver );
		gl.uniform3fv( gMaterialDiffuseUniform, materialDefuse_silver );
	}
	
	translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [sphere_translate_x + 4, sphere_translate_y - diference_y, sphere_translate_z]);
	
	scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [scaleSphere, scaleSphere, scaleSphere]);
	
	modelMatrix = mat4.create();
	mat4.multiply(modelMatrix, translateMatrix, scaleMatrix);

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();

	/********************************************Forth Row**************************************************************/
	diference_y = diference_y + (diference_y / 2);
	// first sphere
	if (isLightOn == 1)
	{
		gl.uniform3fv( gMaterialAmbientUniform, materialAmbient_black );
		gl.uniform1f ( gMaterialShininessUniform, materialShinyness_black );
		gl.uniform3fv( gMaterialSpecularUniform, materialSpecular_black );
		gl.uniform3fv( gMaterialDiffuseUniform, materialDefuse_black );
	}
	
	translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [sphere_translate_x - 4, sphere_translate_y - diference_y, sphere_translate_z]);
	
	scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [scaleSphere, scaleSphere, scaleSphere]);
	
	modelMatrix = mat4.create();
	mat4.multiply(modelMatrix, translateMatrix, scaleMatrix);

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();

	// second sphere
	if (isLightOn == 1)
	{
		gl.uniform3fv( gMaterialAmbientUniform, materialAmbient_cyan );
		gl.uniform1f ( gMaterialShininessUniform, materialShinyness_cyan );
		gl.uniform3fv( gMaterialSpecularUniform, materialSpecular_cyan );
		gl.uniform3fv( gMaterialDiffuseUniform, materialDefuse_cyan );
	}
	
	translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [sphere_translate_x - 1.25, sphere_translate_y - diference_y, sphere_translate_z]);
	
	scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [scaleSphere, scaleSphere, scaleSphere]);
	
	modelMatrix = mat4.create();
	mat4.multiply(modelMatrix, translateMatrix, scaleMatrix);

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();
	
	// third sphere
	if (isLightOn == 1)
	{
		gl.uniform3fv( gMaterialAmbientUniform, materialAmbient_green );
		gl.uniform1f ( gMaterialShininessUniform, materialShinyness_green );
		gl.uniform3fv( gMaterialSpecularUniform, materialSpecular_green );
		gl.uniform3fv( gMaterialDiffuseUniform, materialDefuse_green );
	}
	
	translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [sphere_translate_x + 1.25, sphere_translate_y - diference_y, sphere_translate_z]);
	
	scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [scaleSphere, scaleSphere, scaleSphere]);
	
	modelMatrix = mat4.create();
	mat4.multiply(modelMatrix, translateMatrix, scaleMatrix);

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();

	// forth sphere
	if (isLightOn == 1)
	{
		gl.uniform3fv( gMaterialAmbientUniform, materialAmbient_red );
		gl.uniform1f ( gMaterialShininessUniform, materialShinyness_red );
		gl.uniform3fv( gMaterialSpecularUniform, materialSpecular_red );
		gl.uniform3fv( gMaterialDiffuseUniform, materialDefuse_red );
	}
	
	translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [sphere_translate_x + 4, sphere_translate_y - diference_y, sphere_translate_z]);
	
	scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [scaleSphere, scaleSphere, scaleSphere]);
	
	modelMatrix = mat4.create();
	mat4.multiply(modelMatrix, translateMatrix, scaleMatrix);

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();

	/********************************************Fifth Row**************************************************************/
	diference_y = diference_y + 2.2;
	// first sphere
	if (isLightOn == 1)
	{
		gl.uniform3fv( gMaterialAmbientUniform, materialAmbient_white );
		gl.uniform1f ( gMaterialShininessUniform, materialShinyness_white );
		gl.uniform3fv( gMaterialSpecularUniform, materialSpecular_white );
		gl.uniform3fv( gMaterialDiffuseUniform, materialDefuse_white );
	}
	
	translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [sphere_translate_x - 4, sphere_translate_y - diference_y, sphere_translate_z]);
	
	scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [scaleSphere, scaleSphere, scaleSphere]);
	
	modelMatrix = mat4.create();
	mat4.multiply(modelMatrix, translateMatrix, scaleMatrix);

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();

	// second sphere
	if (isLightOn == 1)
	{
		gl.uniform3fv( gMaterialAmbientUniform, materialAmbient_plastic );
		gl.uniform1f ( gMaterialShininessUniform, materialShinyness_plastic );
		gl.uniform3fv( gMaterialSpecularUniform, materialSpecular_plastic );
		gl.uniform3fv( gMaterialDiffuseUniform, materialDefuse_plastic );
	}
	
	translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [sphere_translate_x - 1.25, sphere_translate_y - diference_y, sphere_translate_z]);
	
	scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [scaleSphere, scaleSphere, scaleSphere]);
	
	modelMatrix = mat4.create();
	mat4.multiply(modelMatrix, translateMatrix, scaleMatrix);

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();
	
	// third sphere
	if (isLightOn == 1)
	{
		gl.uniform3fv( gMaterialAmbientUniform, materialAmbient_black_2 );
		gl.uniform1f ( gMaterialShininessUniform, materialShinyness_black_2 );
		gl.uniform3fv( gMaterialSpecularUniform, materialSpecular_black_2 );
		gl.uniform3fv( gMaterialDiffuseUniform, materialDefuse_black_2 );
	}
	
	translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [sphere_translate_x + 1.25, sphere_translate_y - diference_y, sphere_translate_z]);
	
	scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [scaleSphere, scaleSphere, scaleSphere]);
	
	modelMatrix = mat4.create();
	mat4.multiply(modelMatrix, translateMatrix, scaleMatrix);

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();

	// forth sphere
	if (isLightOn == 1)
	{
		gl.uniform3fv( gMaterialAmbientUniform, materialAmbient_cyan_2 );
		gl.uniform1f ( gMaterialShininessUniform, materialShinyness_cyan_2 );
		gl.uniform3fv( gMaterialSpecularUniform, materialSpecular_cyan_2);
		gl.uniform3fv( gMaterialDiffuseUniform, materialDefuse_cyan_2 );
	}
	
	translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [sphere_translate_x + 4, sphere_translate_y - diference_y, sphere_translate_z]);
	
	scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [scaleSphere, scaleSphere, scaleSphere]);
	
	modelMatrix = mat4.create();
	mat4.multiply(modelMatrix, translateMatrix, scaleMatrix);

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();
	
	/********************************************Sixth Row**************************************************************/
	diference_y = diference_y + 2.2;
	// first sphere
	if (isLightOn == 1)
	{
		gl.uniform3fv( gMaterialAmbientUniform, materialAmbient_green_2 );
		gl.uniform1f ( gMaterialShininessUniform, materialShinyness_green_2 );
		gl.uniform3fv( gMaterialSpecularUniform, materialSpecular_green_2 );
		gl.uniform3fv( gMaterialDiffuseUniform, materialDefuse_green_2 );
	}
	
	translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [sphere_translate_x - 4, sphere_translate_y - diference_y, sphere_translate_z]);
	
	scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [scaleSphere, scaleSphere, scaleSphere]);
	
	modelMatrix = mat4.create();
	mat4.multiply(modelMatrix, translateMatrix, scaleMatrix);

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();

	// second sphere
	if (isLightOn == 1)
	{
		gl.uniform3fv( gMaterialAmbientUniform, materialAmbient_red_2 );
		gl.uniform1f ( gMaterialShininessUniform, materialShinyness_red_2 );
		gl.uniform3fv( gMaterialSpecularUniform, materialSpecular_red_2 );
		gl.uniform3fv( gMaterialDiffuseUniform, materialDefuse_red_2 );
	}
	
	translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [sphere_translate_x - 1.25, sphere_translate_y - diference_y, sphere_translate_z]);
	
	scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [scaleSphere, scaleSphere, scaleSphere]);
	
	modelMatrix = mat4.create();
	mat4.multiply(modelMatrix, translateMatrix, scaleMatrix);

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();
	
	// third sphere
	if (isLightOn == 1)
	{
		gl.uniform3fv( gMaterialAmbientUniform, materialAmbient_white_2 );
		gl.uniform1f ( gMaterialShininessUniform, materialShinyness_white_2 );
		gl.uniform3fv( gMaterialSpecularUniform, materialSpecular_white_2 );
		gl.uniform3fv( gMaterialDiffuseUniform, materialDefuse_white_2 );
	}
	
	translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [sphere_translate_x + 1.25, sphere_translate_y - diference_y, sphere_translate_z]);
	
	scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [scaleSphere, scaleSphere, scaleSphere]);
	
	modelMatrix = mat4.create();
	mat4.multiply(modelMatrix, translateMatrix, scaleMatrix);

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();

	// forth sphere
	if (isLightOn == 1)
	{
		gl.uniform3fv( gMaterialAmbientUniform, materialAmbient_rubber );
		gl.uniform1f ( gMaterialShininessUniform, materialShinyness_rubber );
		gl.uniform3fv( gMaterialSpecularUniform, materialSpecular_rubber );
		gl.uniform3fv( gMaterialDiffuseUniform, materialDefuse_rubber );
	}
	
	translateMatrix = mat4.create();
	mat4.translate(translateMatrix, translateMatrix, [sphere_translate_x + 4, sphere_translate_y - diference_y, sphere_translate_z]);
	
	scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [scaleSphere, scaleSphere, scaleSphere]);
	
	modelMatrix = mat4.create();
	mat4.multiply(modelMatrix, translateMatrix, scaleMatrix);

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform, false, gPerspectiveProjectionMatrix);
	
	sphere.draw();

	gl.useProgram(null);
	
	requestAnimationFrame(draw, canvas);
}

function degToRad(degree)
{
	return ( degree * Math.PI / 180.0 );
}

function uninitialize()
{
	if(sphere)
	{
		sphere.deallocate();
	}
	
	if(gVao)
	{
		gl.deleteVertexArray(gVao);
		gVao = null;
	}
	
	if(gVbo)
	{
		gl.deleteBuffer(gVbo);
		gVbo = null;
	}
	
	if(gShaderProgramObjectPerVertex)
	{
		if(gFragmentShaderObjectPerVertex)
		{
			gl.detachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);
			gl.deleteShader(gFragmentShaderObjectPerVertex);
			gFragmentShaderObjectPerVertex = null;
		}
		
		if(gVertexShaderObjectPerVertex)
		{
			gl.detachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);
			gl.deleteShader(gVertexShaderObjectPerVertex);
			gVertexShaderObjectPerVertex = null;
		}
		
		gl.deleteProgram(gShaderProgramObjectPerVertex);
		gShaderProgramObjectPerVertex = null;
	}
	
	if(gShaderProgramObjectPerFragment)
	{
		if(gFragmentShaderObjectPerFragment)
		{
			gl.detachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);
			gl.deleteShader(gFragmentShaderObjectPerFragment);
			gFragmentShaderObjectPerFragment = null;
		}
		
		if(gVertexShaderObjectPerFragment)
		{
			gl.detachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);
			gl.deleteShader(gVertexShaderObjectPerFragment);
			gVertexShaderObjectPerFragment = null;
		}
		
		gl.deleteProgram(gShaderProgramObjectPerFragment);
		gShaderProgramObjectPerFragment = null;
	}
}
