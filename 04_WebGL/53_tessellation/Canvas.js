// JavaScript source code
var canvas = null;
var gl;
var canvas_original_width = 0, canvas_original_height = 0;
var bFullScreen = false;
var requestAnimationFrame = window.requestAnimationFrame    || 
							window.webkitAnimationFrame     ||
							window.mozRequestAnimationFrame ||
							window.opRequestAnimationFrame  ||
							window.msRequestAnimationFrame	 ;

const webGLMacros =
{
	HRH_ATTRIBUTE_POSITION : 0 ,
    HRH_ATTRIBUTE_COLOR : 1 ,
    HRH_ATTRIBUTE_NORMAL : 2 ,
	HRH_ATTRIBUTE_TEXTURE_0 : 3 ,
}

var gVertexShaderObject;
var gTessallatioControlShaderObject;
var gTessellationEvaluationShaderObject;
var gFragmentShaderObject;
var gShaderProgramObject;

var gVao;
var gVbo;
var gMVPUniform;
var gNumberOfSegmentUniform;
var gNumberOfStripsUniform;
var gLineColorUniform;

var gPerspectiveProjectionMatrix;

var uiNumberOfSegment;

function main()
{
	// get canvas from dom
    canvas = document.getElementById("hrh"); // type inferance

    if (!canvas) 
	{
        console.log("canvas failed");
    }
    else
    {
        console.log("Got canvas");
    }

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
    
	// window in buil variable , window is inherited from DOM object
	// 3rd parameter false means do bubbling
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);	
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize(); // explicit warm up resize because webgl don't call resize
	
	draw();
	
}

function keyDown(event)
{
	switch(event.keyCode)
	{
		// for 'f' or 'F'
		case 70:
			toggleFullScreen();
			break;
		case 27:
			uninitialize();
			window.close(); // may not work in firefox but will work in chrome and safari
			break;
	}
	
	switch(event.key)
	{
		case 'u':
		case 'U':
				if (uiNumberOfSegment < 30)
				{
					uiNumberOfSegment = uiNumberOfSegment + 1;
				}
				break;
		case 'd':
		case 'D':
				if (uiNumberOfSegment > 1)
				{
					uiNumberOfSegment = uiNumberOfSegment - 1;
				}
				break;
	}
}

function mouseDown()
{
	
}

function toggleFullScreen()
{
	var fullscreen_element = 	document.fullscreenElement || 
								document.webkitFulllscreenElement || 
								document.mozFullScreen || 
								document.msFullscreenElement ||
								null;
								
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
		{
			canvas.requestFullscreen();			
		} 
		else if(canvas.webkitRequestFulllscreen())
		{
			canvas.webkitRequestFulllscreen();
		}
		else if(canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		} 
		else if(canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		} 
		else if ( document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		} 
		else if (document.mozCancelFullScreen)
		{
			document.mozCancelFullScreen();
		} 
		else if (document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		bFullScreen = false;
	}	
}

function init()
{
	
    // get drawing context from the canvas
    gl = canvas.getContext("webgl2");

    if (!gl) 
	{
        console.log("gl failed");
    }
    else 
	{
        console.log("Got gl");
    }
	
	gVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	
	var vertexShaderSourceCode = 
								" #version 310 es \n 									" +
								" in vec2 vPosition;									" +
								" void main()											" +
								" {														" +
								" 	gl_Position = vec4(vPosition, 0.0, 1.0);			" +
								" }														" ;

	gl.shaderSource(gVertexShaderObject, vertexShaderSourceCode);
	
	gl.compileShader(gVertexShaderObject);
	
	if(gl.getShaderParameter(gVertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	// tessellation control shader
	gTessallatioControlShaderObject = gl.createShader(gl.TESS_CONTROL_SHADER);
	
	var tessellationShaderSourceCode = 	
									"	#version 310 es\n 																	" +
									"	precision highp float;																" +
									"	layout (vertices = 4) out ; 														" +
									"	uniform int numberOfSegments; 														" +
									"	uniform int numberOfStrips; 														" +
									"	void main()																			" +
									"	{																					" +
									"		gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;		" +
									"		gl_TessLevelOuter[0] = float(numberOfStrips); 									" +
									"		gl_TessLevelOuter[1] = float(numberOfSegments); 								" +
									"	}																					" ;
	
	gl.shaderSource(gTessallatioControlShaderObject, tessellationShaderSourceCode);
	
	gl.compileShader(gTessallatioControlShaderObject);
	
	if(gl.getShaderParameter(gTessallatioControlShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gTessallatioControlShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	// tessellation evaluation shader
	gTessellationEvaluationShaderObject = gl.createShader(gl.TESS_EVALUATION_SHADER);
	
	var tessellationEvalutionShaderSourceCode = 	
									" #version 310 es\n 																																																													" +
									" precision highp float;																																																												" +
									" layout (isolines) in ;																																																												" +
									" uniform mat4 u_mvpMatrix;																																																												" +
									" void main()																																																															" +
									" {									 																																																									" +
									" 	float tess_coord = gl_TessCoord.x;  																																																								" +
									" 	vec3 p0 = gl_in[0].gl_Position.xyz; 																																																								" +
									" 	vec3 p1 = gl_in[1].gl_Position.xyz; 																																																								" +
									" 	vec3 p2 = gl_in[2].gl_Position.xyz; 																																																								" +
									" 	vec3 p3 = gl_in[3].gl_Position.xyz; 																																																								" +
									" 	vec3 p = p0 * (1.0 - tess_coord ) * (1.0 - tess_coord) * ( 1.0 - tess_coord) + p1 * 3.0 * tess_coord * ( 1.0 - tess_coord) * ( 1.0 - tess_coord) + p2 * 3.0 * tess_coord * tess_coord * ( 1.0 - tess_coord) + p3 * tess_coord * tess_coord * tess_coord ; 			" +
									" 	gl_Position = u_mvpMatrix * vec4(p, 1.0); 																																																							" +
									" }																																																																		" ;
	
	gl.shaderSource(gTessellationEvaluationShaderObject, tessellationEvalutionShaderSourceCode);
	
	gl.compileShader(gTessellationEvaluationShaderObject);
	
	if(gl.getShaderParameter(gTessellationEvaluationShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gTessellationEvaluationShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	gFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
	var fragmentShaderSourceCode = 	" #version 310 es 										\n  " +
									" precision highp float;									" +
									" uniform vec4 lineColor; 									" +
									" out vec4 FragColor; 										" +
									" void main()												" +
									" {															" +
									" 	FragColor = lineColor;									" +
									" }															" ;
	
	gl.shaderSource(gFragmentShaderObject, fragmentShaderSourceCode);
	
	gl.compileShader(gFragmentShaderObject);
	
	if(gl.getShaderParameter(gFragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gShaderProgramObject = gl.createProgram();
	
	// attach vertex shader to shader program
	gl.attachShader(gShaderProgramObject, gVertexShaderObject);

	gl.attachShader(gShaderProgramObject, gTessallatioControlShaderObject);

	gl.attachShader(gShaderProgramObject, gTessellationEvaluationShaderObject);
	
	// attch fragment shader to shader program
	gl.attachShader(gShaderProgramObject, gFragmentShaderObject);
	
	// pri-link binding of shader object with vertex shader possition attribute
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_POSITION, "vPosition");

	// LINK PROGRAM
	gl.linkProgram(gShaderProgramObject);
	
	if(!gl.getProgramParameter(gShaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	gMVPUniform = gl.getUniformLocation(gShaderProgramObject, "u_mvpMatrix");
	gNumberOfSegmentUniform = gl.getUniformLocation(gShaderProgramObject, "numberOfSegments");
	gNumberOfStripsUniform = gl.getUniformLocation(gShaderProgramObject, "numberOfStrips");
	gLineColorUniform = gl.getUniformLocation(gShaderProgramObject, "lineColor");	
	
	var triangleVertices = new Float32Array
		([
			0.0, 1.0, 0.0,
			-1.0, -1.0, 0.0,
			1.0, -1.0, 0.0
		]);
	
	gVao = gl.createVertexArray();
	gl.bindVertexArray(gVao);
	
	gVbo = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo);
	gl.bufferData(gl.ARRAY_BUFFER, triangleVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_POSITION);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);
	
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	gl.clearColor(0.0, 0.0, 1.0, 1.0);
	
	gPerspectiveProjectionMatrix = mat4.create();
	
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	// ser the viewport to match	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(gPerspectiveProjectionMatrix, 
						45.0, 
						parseFloat(canvas.width) /parseFloat(canvas.height), 
						parseFloat(canvas.height) / parseFloat(canvas.width), 
						100.0); // no 0.1f
	
}

function draw()
{
	// code
	gl.clear(gl.COLOR_BUFFER_BIT);
	
	gl.useProgram(gShaderProgramObject);

	var modelViewMatrix = mat4.create();
	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -4.0]);
	var modelViewProjectionMatrix = mat4.create();
	
	mat4.multiply(modelViewProjectionMatrix, gPerspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMVPUniform, false, modelViewProjectionMatrix);
	
	gl.uniform1i(gNumberOfSegmentUniform, uiNumberOfSegment);
	gl.uniform1i(gNumberOfStripsUniform, 1);
	var color = new Float32Array([ 1.0, 1.0, 0.0, 1.0 ]);
	gl.uniform4fv(gLineColorUniform, 1, color);

	gl.bindVertexArray(gVao);
	glPatchParameteri(gl.PATCH_VERTICES, 4);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	
	gl.bindVertexArray(null);
	
	gl.useProgram(null);
	
	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	if(gVao)
	{
		gl.deleteVertexArray(gVao);
		gVao = null;
	}
	
	if(gVbo)
	{
		gl.deleteBuffer(gVbo);
		gVbo = null;
	}
	
	if(gShaderProgramObject)
	{
		if(gFragmentShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gFragmentShaderObject);
			gl.deleteShader(gFragmentShaderObject);
			gFragmentShaderObject = null;
		}
		
		if(gVertexShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gVertexShaderObject);
			gl.deleteShader(gVertexShaderObject);
			gVertexShaderObject = null;
		}
		
		gl.deleteProgram(gShaderProgramObject);
		gShaderProgramObject = null;
	}
}
