// JavaScript source code
var canvas = null;
var gl;
var canvas_original_width = 0, canvas_original_height = 0;
var bFullScreen = false;
var requestAnimationFrame = window.requestAnimationFrame    || 
							window.webkitAnimationFrame     ||
							window.mozRequestAnimationFrame ||
							window.opRequestAnimationFrame  ||
							window.msRequestAnimationFrame	 ;

const webGLMacros =
{
	HRH_ATTRIBUTE_POSITION : 0 ,
    HRH_ATTRIBUTE_COLOR : 1 ,
    HRH_ATTRIBUTE_NORMAL : 2 ,
	HRH_ATTRIBUTE_TEXTURE_0 : 3 ,
}

var gVertexShaderObject;
var gFragmentShaderObject;
var gShaderProgramObject;

var gVao_triangle;
var gVbo_triangle_position;
var gVbo_triangle_color;

var gVao_rectangle;
var gVbo_rectangle_position;
var gVbo_rectangle_color;

var gMVPUniform;

var gPerspectiveProjectionMatrix;

var rotateAngle = 0.0;

function main()
{
	// get canvas from dom
    canvas = document.getElementById("hrh"); // type inferance

    if (!canvas) 
	{
        console.log("canvas failed");
    }
    else
    {
        console.log("Got canvas");
    }

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
    
	// window in buil variable , window is inherited from DOM object
	// 3rd parameter false means do bubbling
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);	
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize(); // explicit warm up resize because webgl don't call resize
	
	draw();
	
}

function keyDown(event)
{
	switch(event.keyCode)
	{
		// for 'f' or 'F'
		case 70:
			toggleFullScreen();
			break;
		case 27:
			uninitialize();
			window.close(); // may not work in firefox but will work in chrome and safari
			break;
	}
}

function mouseDown()
{
	
}

function toggleFullScreen()
{
	var fullscreen_element = 	document.fullscreenElement || 
								document.webkitFulllscreenElement || 
								document.mozFullScreen || 
								document.msFullscreenElement ||
								null;
								
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
		{
			canvas.requestFullscreen();			
		} 
		else if(canvas.webkitRequestFulllscreen())
		{
			canvas.webkitRequestFulllscreen();
		}
		else if(canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		} 
		else if(canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		} 
		else if ( document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		} 
		else if (document.mozCancelFullScreen)
		{
			document.mozCancelFullScreen();
		} 
		else if (document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		bFullScreen = false;
	}	
}

function init()
{
	
    // get drawing context from the canvas
    gl = canvas.getContext("webgl2");

    if (!gl) 
	{
        console.log("gl failed");
    }
    else 
	{
        console.log("Got gl");
    }
	
	gVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	
	var vertexShaderSourceCode = 
								"#version 300 es \n" +
								"in vec4 vPosition;" +
								"in vec4 vColor;" +
								"out vec4 outColor;"+
								"uniform mat4 u_mvpMatrix;" +
								"void main()" +
								"{" +
								"gl_Position = u_mvpMatrix * vPosition;" +
								"outColor = vColor;" +
								"}";

	gl.shaderSource(gVertexShaderObject, vertexShaderSourceCode);
	
	gl.compileShader(gVertexShaderObject);
	
	if(gl.getShaderParameter(gVertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
	var fragmentShaderSourceCode = 	"#version 300 es\n"+
									"precision highp float;" +
									"in vec4 outColor;" +
									"out vec4 FragColor; " +
									"void main()"+
									"{" +
									"FragColor = outColor;" +
									"}";
	
	gl.shaderSource(gFragmentShaderObject, fragmentShaderSourceCode);
	
	gl.compileShader(gFragmentShaderObject);
	
	if(gl.getShaderParameter(gFragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gShaderProgramObject = gl.createProgram();
	
	// attach vertex shader to shader program
	gl.attachShader(gShaderProgramObject, gVertexShaderObject);
	
	// attch fragment shader to shader program
	gl.attachShader(gShaderProgramObject, gFragmentShaderObject);
	
	// pri-link binding of shader object with vertex shader possition attribute
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_COLOR, "vColor");

	// LINK PROGRAM
	gl.linkProgram(gShaderProgramObject);
	
	if(!gl.getProgramParameter(gShaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gMVPUniform = gl.getUniformLocation(gShaderProgramObject, "u_mvpMatrix");
	
	// triangle	
	var triangleVertices = new Float32Array
		([
			0.0, 1.0, 0.0,
			-1.0, -1.0, 0.0,
			1.0, -1.0, 0.0
		]);
	
	gVao_triangle = gl.createVertexArray();
	gl.bindVertexArray(gVao_triangle);
	
	gVbo_triangle_position = gl.createBuffer()
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_triangle_position);
	gl.bufferData(gl.ARRAY_BUFFER, triangleVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_POSITION);

	var triangleColor = new Float32Array
		([
			1.0, 0.0, 0.0,
			0.0, 1.0, 0.0,
			0.0, 0.0, 1.0
		]);
		
	gVbo_triangle_color = gl.createBuffer()
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_triangle_color);
	gl.bufferData(gl.ARRAY_BUFFER, triangleColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_COLOR);

	
	gl.bindBuffer(gl.ARRAY_BUFFER, null);	
	gl.bindVertexArray(null);	

	// rectangle
	var rectangleVertices = new Float32Array
		([
		1.0, 1.0, 0.0,
		-1.0, 1.0, 0.0,
		-1.0, -1.0, 0.0,
		1.0, -1.0, 0.0
		]);
	
	gVao_rectangle = gl.createVertexArray();
	gl.bindVertexArray(gVao_rectangle);
	
	gVbo_rectangle_position = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_rectangle_position);
	gl.bufferData(gl.ARRAY_BUFFER, rectangleVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_POSITION);

	gl.vertexAttrib3f(webGLMacros.HRH_ATTRIBUTE_COLOR,0.0, 0.0, 1.0);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, null);	
	gl.bindVertexArray(null);	

	
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	gPerspectiveProjectionMatrix = mat4.create();
	
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	// ser the viewport to match	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(gPerspectiveProjectionMatrix, 
						45.0, 
						parseFloat(canvas.width) /parseFloat(canvas.height), 
						parseFloat(canvas.height) / parseFloat(canvas.width), 
						100.0); 
	
}

function draw()
{
	// variable declaration	
	
	
	// code
	gl.clear(gl.COLOR_BUFFER_BIT);
	
	gl.useProgram(gShaderProgramObject);
	
	// triangle	
	var modelViewMatrix = mat4.create();
	mat4.translate(modelViewMatrix, modelViewMatrix, [-1.5, 0.0, -6.0]);
	
	
	var rotateMatrix = mat4.create();
	mat4.rotate(rotateMatrix, rotateMatrix, rotateAngle , [0.0, 1.0, 0.0]);
	
	mat4.multiply(modelViewMatrix, modelViewMatrix, rotateMatrix);
	
	var modelViewProjectionMatrix = mat4.create();	
	mat4.multiply(modelViewProjectionMatrix, gPerspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMVPUniform, false, modelViewProjectionMatrix);
	
	gl.bindVertexArray(gVao_triangle);
	gl.drawArrays(gl.TRIANGLES, 0, 3); 
	gl.bindVertexArray(null);
	
	// rectangle	
	modelViewMatrix = mat4.create();
	mat4.translate(modelViewMatrix, modelViewMatrix, [1.5, 0.0, -6.0]);
	
	rotateMatrix = mat4.create();
	mat4.rotate(rotateMatrix, rotateMatrix, rotateAngle , [1.0, 0.0, 0.0]);
	
	mat4.multiply(modelViewMatrix, modelViewMatrix, rotateMatrix);
	
	var scaleMatrix = mat4.create();
	mat4.scale(scaleMatrix, scaleMatrix, [0.9, 0.9, 0.9]);
	
	mat4.multiply(modelViewMatrix, modelViewMatrix, scaleMatrix);
	
	modelViewProjectionMatrix = mat4.create();	
	mat4.multiply(modelViewProjectionMatrix, gPerspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMVPUniform, false, modelViewProjectionMatrix);
	gl.bindVertexArray(gVao_rectangle);
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);	
	gl.bindVertexArray(null);
	
	gl.useProgram(null);
	
	if(rotateAngle > 360.0)
	{
		rotateAngle = 0.0;
	}
	rotateAngle = rotateAngle + 0.01;
	
	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	// triangle
	if(gVao_triangle)
	{
		gl.deleteVertexArray(gVao_triangle);
		gVao_triangle = null;
	}
	
	if(gVbo_triangle_position)
	{
		gl.deleteBuffer(gVbo_triangle_position);
		gVbo_triangle_position = null;
	}

	if(gVbo_triangle_color)
	{
		gl.deleteBuffer(gVbo_triangle_color);
		gVbo_triangle_color = null;
	}
	
	// rectangle
	if(gVao_rectangle)
	{
		gl.deleteVertexArray(gVao_rectangle);
		gVao_rectangle = null;
	}
	
	if(gVbo_rectangle_position)
	{
		gl.deleteBuffer(gVbo_rectangle_position);
		gVbo_rectangle_position = null;
	}

	if(gVbo_rectangle_color)
	{
		gl.deleteBuffer(gVbo_rectangle_color);
		gVbo_rectangle_color = null;
	}
	
	if(gShaderProgramObject)
	{
		if(gFragmentShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gFragmentShaderObject);
			gl.deleteShader(gFragmentShaderObject);
			gFragmentShaderObject = null;
		}
		
		if(gVertexShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gVertexShaderObject);
			gl.deleteShader(gVertexShaderObject);
			gVertexShaderObject = null;
		}
		
		gl.deleteProgram(gShaderProgramObject);
		gShaderProgramObject = null;
	}
}
