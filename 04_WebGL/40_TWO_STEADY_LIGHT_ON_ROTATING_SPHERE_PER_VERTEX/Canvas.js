// JavaScript source code
var canvas = null;
var gl;
var canvas_original_width = 0, canvas_original_height = 0;
var bFullScreen = false;
var requestAnimationFrame = window.requestAnimationFrame    || 
							window.webkitAnimationFrame     ||
							window.mozRequestAnimationFrame ||
							window.opRequestAnimationFrame  ||
							window.msRequestAnimationFrame	 ;

const webGLMacros =
{
	HRH_ATTRIBUTE_POSITION : 0 ,
    HRH_ATTRIBUTE_COLOR : 1 ,
    HRH_ATTRIBUTE_NORMAL : 2 ,
	HRH_ATTRIBUTE_TEXTURE_0 : 3 ,
}

var gVertexShaderObject;
var gFragmentShaderObject;
var gShaderProgramObject;

var gVao_pyramid;
var gVbo_pyramid_position;
var gVbo_pyramid_normal;

var gModelUniform;
var gRotateUniform;
var gViewUniform;
var gPerspectiveUniform;

var gMaterialSpecularUniform;
var gMaterialAmbientUniform;
var gMaterialDiffuseUniform;
var gMaterialShininessUniform;

var gIsLightOnUniform;

var isLightOn = 1;

var bAnimate = false;

var lightPosition = new Float32Array([ 5.0, 0.0, 0.0, 1.0,  -5.0, 0.0, 0.0, 1.0  ]);
var lightAmbient  = new Float32Array([  1.0, 0.0, 0.0, 0.0, 0.0, 1.0 ]);
var lightDiffuse  = new Float32Array([  1.0, 0.0, 0.0, 0.0, 0.0, 1.0 ]);
var lightSpecular = new Float32Array([  0.0, 0.0, 1.0, 0.0, 0.0, 1.0  ]);

var gLightAmbientUniform;
var gLightSpecularUniform;
var gLightDiffusedUniform;
var gLightPossitionUniform;

var materialAmbient  = new Float32Array([  0.0, 0.0, 0.0 ]);
var materialDiffuse  = new Float32Array([  1.0, 1.0, 1.0 ]);
var materialSpecular = new Float32Array([ 1.0, 1.0, 1.0  ]);
var materialShininess  = 128.0;

var gPerspectiveProjectionMatrix;

var rotateAngle = 0.0;

var sphere = null;

function main()
{
	// get canvas from dom
    canvas = document.getElementById("hrh"); // type inferance

    if (!canvas) 
	{
        console.log("canvas failed");
    }
    else
    {
        console.log("Got canvas");
    }

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
    
	// window in buil variable , window is inherited from DOM object
	// 3rd parameter false means do bubbling
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);	
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize(); // explicit warm up resize because webgl don't call resize
	
	draw();
	
}

function keyDown(event)
{
	switch(event.keyCode)
	{
		// for 'f' or 'F'
		case 70:
			toggleFullScreen();
			break;
		case 27:
			uninitialize();
			window.close(); // may not work in firefox but will work in chrome and safari
			break;
	}
	switch(event.key)
	{
		case 'a':
		case 'A':
			if (bAnimate == false)
			{
				bAnimate = true;
			}
			else
			{
				bAnimate = false;
			}
			break;
		case 'l':
		case 'L':
			if (isLightOn == 0)
			{
				isLightOn = 1;
			}
			else
			{
				isLightOn = 0;
			}
			break;

	}
}

function mouseDown()
{
	
}

function toggleFullScreen()
{
	var fullscreen_element = 	document.fullscreenElement || 
								document.webkitFulllscreenElement || 
								document.mozFullScreen || 
								document.msFullscreenElement ||
								null;
								
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
		{
			canvas.requestFullscreen();			
		} 
		else if(canvas.webkitRequestFulllscreen())
		{
			canvas.webkitRequestFulllscreen();
		}
		else if(canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		} 
		else if(canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		} 
		else if ( document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		} 
		else if (document.mozCancelFullScreen)
		{
			document.mozCancelFullScreen();
		} 
		else if (document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		bFullScreen = false;
	}	
}

function init()
{
	
    // get drawing context from the canvas
    gl = canvas.getContext("webgl2");

    if (!gl) 
	{
        console.log("gl failed");
    }
    else 
	{
        console.log("Got gl");
    }
	
	gVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	
	var vertexShaderSourceCode = 
		"#version 300 es \n"  +
		"in vec4 v_position;" +
		"in vec3 v_normals; " +
		"uniform mat4 u_modelMatrix;" +
		"uniform mat4 u_rotateMatrix; " +
		"uniform mat4 u_viewMatrix;" +
		"uniform mat4 u_projectionMatrix;" +
		"uniform int u_isLightOn;" +
		"uniform vec3 u_lightAmbient[2];" +
		"uniform vec3 u_lightSpecular[2];" +
		"uniform vec3 u_lightDiffuse[2];" +
		"uniform vec4 u_lightposition[2]; " +
		"uniform vec3 u_materialAmbient; " +
		"uniform vec3 u_materialSpecular; " +
		"uniform vec3 u_materialDiffuse; " +
		"uniform float u_materialshininess; " +
		"out vec3 phoung_ads_lighting;" +
		"vec4 eye_coordinates;" +
		"vec3 transformed_normal, light_direction, reflection_vector, view_vector;" +
		"vec3 ambient, diffuse_light, specular;"+
		"void main()" +
		"{" +
		"if (u_isLightOn == 1) " +
		"{" +
			"eye_coordinates = u_viewMatrix * u_modelMatrix * u_rotateMatrix * v_position; " +
			"transformed_normal = normalize( mat3( u_viewMatrix * u_modelMatrix * u_rotateMatrix ) * v_normals); " +
			"for(int i=0; i< 2;i++) " +
			"{" +
				"light_direction = normalize( vec3 ( ( u_lightposition[i]) - eye_coordinates) ); " +
				"reflection_vector = reflect (-light_direction, transformed_normal); " +
				"view_vector = normalize ( vec3 ( -eye_coordinates)); " +
				"ambient = u_lightAmbient[i] * u_materialAmbient; " +
				"diffuse_light = u_lightDiffuse[i] * u_materialDiffuse * max ( dot (light_direction , transformed_normal), 0.0); " +
				"specular = u_lightSpecular[i] * u_materialSpecular * pow( max ( dot ( reflection_vector , view_vector), 0.0) , u_materialshininess);" +
				"phoung_ads_lighting = phoung_ads_lighting + ambient + diffuse_light + specular;" +
			"}" +			
		"}" +
		"else" +
		"{" +
		"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" +
		"}"+
		"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * u_rotateMatrix * v_position;" +
		"}" ;

	gl.shaderSource(gVertexShaderObject, vertexShaderSourceCode);
	
	gl.compileShader(gVertexShaderObject);
	
	if(gl.getShaderParameter(gVertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
	var fragmentShaderSourceCode = 	
		"#version 300 es \n" +
		"precision highp float;" +
		"in vec3 phoung_ads_lighting;"+
		"out vec4 FragColor; " +
		"void main()" +
		"{"+
		"FragColor = vec4(phoung_ads_lighting, 1.0); " +
		"}";
	
	gl.shaderSource(gFragmentShaderObject, fragmentShaderSourceCode);
	
	gl.compileShader(gFragmentShaderObject);
	
	if(gl.getShaderParameter(gFragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	gShaderProgramObject = gl.createProgram();
	
	// attach vertex shader to shader program
	gl.attachShader(gShaderProgramObject, gVertexShaderObject);
	
	// attch fragment shader to shader program
	gl.attachShader(gShaderProgramObject, gFragmentShaderObject);
	
	// pri-link binding of shader object with vertex shader possition attribute
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(gShaderProgramObject, webGLMacros.HRH_ATTRIBUTE_NORMAL, "v_normals");

	// LINK PROGRAM
	gl.linkProgram(gShaderProgramObject);
	
	if(!gl.getProgramParameter(gShaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	// get MVP uniform location
	gModelUniform       = gl.getUniformLocation(gShaderProgramObject, "u_modelMatrix");
	gViewUniform        = gl.getUniformLocation(gShaderProgramObject, "u_viewMatrix");
	gPerspectiveUniform = gl.getUniformLocation(gShaderProgramObject, "u_projectionMatrix");
	gIsLightOnUniform   = gl.getUniformLocation(gShaderProgramObject, "u_isLightOn");
	gRotateUniform      = gl.getUniformLocation(gShaderProgramObject, "u_rotateMatrix");
		
	gLightAmbientUniform   = gl.getUniformLocation(gShaderProgramObject, "u_lightAmbient");
	gLightSpecularUniform  = gl.getUniformLocation(gShaderProgramObject, "u_lightSpecular");
	gLightDiffusedUniform  = gl.getUniformLocation(gShaderProgramObject, "u_lightDiffuse");
	gLightPossitionUniform = gl.getUniformLocation(gShaderProgramObject, "u_lightposition");

	gMaterialDiffuseUniform   = gl.getUniformLocation(gShaderProgramObject, "u_materialDiffuse");
	gMaterialAmbientUniform   = gl.getUniformLocation(gShaderProgramObject, "u_materialAmbient");
	gMaterialSpecularUniform  = gl.getUniformLocation(gShaderProgramObject, "u_materialSpecular");
	gMaterialShininessUniform = gl.getUniformLocation(gShaderProgramObject, "u_materialshininess");
	
	// triangle 
	var pyramidVertices = new Float32Array
		([
			 0.0, 1.0, 0.0,
			-1.0,-1.0, 1.0,
			 1.0, -1.0, 1.0,
			
			0.0,  1.0,  0.0,
			1.0, -1.0,  1.0,
			1.0, -1.0, -1.0,
			
			 0.0,  1.0, 0.0,
			 1.0, -1.0,-1.0,
			-1.0, -1.0,-1.0,
			
			 0.0,  1.0,  0.0,
			-1.0, -1.0, -1.0,
			-1.0, -1.0,  1.0,
		]);
	
	gVao_pyramid = gl.createVertexArray();
	gl.bindVertexArray(gVao_pyramid);
	
	gVbo_pyramid_position = gl.createBuffer()
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_pyramid_position);
	gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	var pyramid_normals = new Float32Array ([
		0.0, 0.447214, 0.894427, //FRONT	
		0.0, 0.447214, 0.894427, //FRONT	
		0.0, 0.447214, 0.894427, //FRONT			
		
		0.894427, 0.447214, 0.0, // RIGHT
		0.894427, 0.447214, 0.0, // RIGHT
		0.894427, 0.447214, 0.0, // RIGHT
		
		0.0, 0.447214, -0.894427, // BACK
		0.0, 0.447214, -0.894427, // BACK
		0.0, 0.447214, -0.894427, // BACK
		
		-0.894427, 0.447214, 0.0, // LEFT
		-0.894427, 0.447214, 0.0, // LEFT
		-0.894427, 0.447214, 0.0 // LEFT
	]);
	
	gVbo_pyramid_normal = gl.createBuffer()
	gl.bindBuffer(gl.ARRAY_BUFFER, gVbo_pyramid_normal);
	gl.bufferData(gl.ARRAY_BUFFER, pyramid_normals, gl.STATIC_DRAW);
	gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_NORMAL);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);
	
	sphere = new Mesh();
		
	makeSphere(sphere, 2.0, 30, 30);
	
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
//	gl.shadeModel(gl.SMOOTH);
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
//	gl.hint(gl.PERSPECTIVE_CORRECTION_HINT, gl.NICEST);
	
	gPerspectiveProjectionMatrix = mat4.create();
	
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	// ser the viewport to match	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(gPerspectiveProjectionMatrix, 
						45.0, 
						parseFloat(canvas.width) /parseFloat(canvas.height), 
						parseFloat(canvas.height) / parseFloat(canvas.width), 
						100.0); 
	
}

function draw()
{
	// variable declaration		
	// code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(gShaderProgramObject);
	
	// triangle	
	var modelMatrix = mat4.create();
	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);
	
	var viewMatrix = mat4.create();
	
	var rotateMatrix = mat4.create();
	mat4.rotateY(rotateMatrix, rotateMatrix, degToRad(rotateAngle));
	
	if (isLightOn == 1)
	{
		gl.uniform4fv(gLightPossitionUniform, lightPosition);
		gl.uniform3fv(gLightDiffusedUniform, lightDiffuse);
		gl.uniform3fv(gLightAmbientUniform,  lightAmbient);
		gl.uniform3fv(gLightSpecularUniform, lightSpecular);
		
		gl.uniform3fv(gMaterialAmbientUniform, materialAmbient);
		gl.uniform1f (gMaterialShininessUniform, materialShininess);
		gl.uniform3fv(gMaterialSpecularUniform, materialSpecular);
		gl.uniform3fv(gMaterialDiffuseUniform, materialDiffuse);
	}

	gl.uniform1i(gIsLightOnUniform, isLightOn);
	gl.uniformMatrix4fv(gModelUniform, false,modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, viewMatrix);
	gl.uniformMatrix4fv(gPerspectiveUniform,  false, gPerspectiveProjectionMatrix);
	gl.uniformMatrix4fv(gRotateUniform, false, rotateMatrix);

	sphere.draw();
	
	gl.useProgram(null);
	
	if(rotateAngle > 360.0)
	{
		rotateAngle = 0.0;
	}
	rotateAngle = rotateAngle + 0.2;
	
	requestAnimationFrame(draw, canvas);
}

function degToRad(degree)
{
	return ( degree * Math.PI / 180.0 );
}

function uninitialize()
{
	// triangle
	if(gVao_pyramid)
	{
		gl.deleteVertexArray(gVao_triangle);
		gVao_triangle = null;
	}
	
	if(gVbo_pyramid_position)
	{
		gl.deleteBuffer(gVbo_pyramid_position);
		gVbo_pyramid_position = null;
	}

	if(gVbo_pyramid_normal)
	{
		gl.deleteBuffer(gVbo_pyramid_normal);
		gVbo_pyramid_normal = null;
	}
	
	if(gShaderProgramObject)
	{
		if(gFragmentShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gFragmentShaderObject);
			gl.deleteShader(gFragmentShaderObject);
			gFragmentShaderObject = null;
		}
		
		if(gVertexShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gVertexShaderObject);
			gl.deleteShader(gVertexShaderObject);
			gVertexShaderObject = null;
		}
		
		gl.deleteProgram(gShaderProgramObject);
		gShaderProgramObject = null;
	}
}
