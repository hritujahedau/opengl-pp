var gProgramObject_render;
var gProgramObject_filter;
var gProgramObject_resolve;

var gVao;
var gVao_quad;
var data_buffer;
var ubo_transform;
var ubo_material;

var render_fbo;

var filter_fbo = [];
var tex_filter = [];

var gProjUniform;
var gViewUniform;
var gModelUniform;

var gDiffuseUniform;
var gSpecularUniform;
var gSpecualrPowerUniform;
var gAmbientUniform;

var gSampler2D;

var g_bloom_thresh_min_uniform;
var g_bloom_thresh_max_uniform;
var g_bloom_factor_uniform, g_scene_factor_uniform;
var g_exposure_uniform, g_light_pos_uniform;
var g_horizontal_uniform;

var gUniformExposure;

var tex_scene, tex_brightpass, tex_depth, tex_lut;

var show_bloom = false, show_scene = true, show_prefilter = false, paused = false;
var bloom_thresh_min = 0.8, bloom_thresh_max = 1.2;
var directional_light = new Float32Array([ 100.0, 100.0, 100.0 ]);

var exposure = 1.0;
var bloom_factor = 1.0;

var mode = 1;

var MAX_SCENE_WIDTH = 2048;
var MAX_SCENE_HEIGHT = 2048;

var w, h;

function init_Hrh_HDR_Bloom()
{
	createShaderProgramHdrBloom();
	
	createAndBindFrameBuffer();
}

function createShaderProgramHdrBloom()
{
    console.log("Compiling bloom-render");
    gProgramObject_render = compileAndLinkShader("hvs-hdr-bloom-render", "hfs-hdr-bloom-render");

    gProjUniform = gl.getUniformLocation(gProgramObject_render, "mat_proj");
    gViewUniform = gl.getUniformLocation(gProgramObject_render, "mat_view");
    gModelUniform = gl.getUniformLocation(gProgramObject_render, "mat_model");

    gDiffuseUniform = gl.getUniformLocation(gProgramObject_render, "diffuse_color");
    gSpecularUniform = gl.getUniformLocation(gProgramObject_render, "specular_color");
    gSpecualrPowerUniform = gl.getUniformLocation(gProgramObject_render, "specular_power");
    gAmbientUniform = gl.getUniformLocation(gProgramObject_render, "ambient_color");


    g_bloom_thresh_min_uniform = gl.getUniformLocation(gProgramObject_render, "bloom_thresh_min");
	g_bloom_thresh_max_uniform = gl.getUniformLocation(gProgramObject_render, "bloom_thresh_max");
	g_light_pos_uniform = gl.getUniformLocation(gProgramObject_render, "light_pos");

    console.log("Compiling bloom-filter");
    gProgramObject_filter = compileAndLinkShader("hvs-hdr-bloom-filter", "hfs-hdr-bloom-filter");

    g_horizontal_uniform = gl.getUniformLocation(gProgramObject_filter, "horizontal");

    console.log("Compiling bloom-resolve");
    gProgramObject_resolve = compileAndLinkShader("hvs-hdr-bloom-resolve", "hfs-hdr-bloom-resolve");

    g_exposure_uniform = gl.getUniformLocation(gProgramObject_resolve, "exposure");
	g_bloom_factor_uniform = gl.getUniformLocation(gProgramObject_resolve, "bloom_factor");
	g_scene_factor_uniform = gl.getUniformLocation(gProgramObject_resolve, "scene_factor");

    gVao_quad = gl.createVertexArray();
	gl.bindVertexArray(gVao_quad);
}

function createAndBindFrameBuffer()
{
    const ext = gl.getExtension('EXT_color_buffer_float');

    console.log("Entering in createAndBindFrameBuffer()");
    render_fbo = gl.createFramebuffer();
	gl.bindFramebuffer(gl.FRAMEBUFFER, render_fbo);

    console.log("In createAndBindFrameBuffer() after creating render_fbo framebuffer");

    tex_scene = gl.createTexture();
	gl.bindTexture(gl.TEXTURE_2D, tex_scene);
	gl.texStorage2D(gl.TEXTURE_2D, 1, gl.RGBA16F, MAX_SCENE_WIDTH, MAX_SCENE_HEIGHT);
	gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, tex_scene, 0);
    console.log("In createAndBindFrameBuffer() after creating tex_scene framebuffer");

    tex_brightpass = gl.createTexture();
	gl.bindTexture(gl.TEXTURE_2D, tex_brightpass);
	gl.texStorage2D(gl.TEXTURE_2D, 1, gl.RGBA16F, MAX_SCENE_WIDTH, MAX_SCENE_HEIGHT);
	gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT1, gl.TEXTURE_2D, tex_brightpass, 0);
    console.log("In createAndBindFrameBuffer() after creating tex_brightpass framebuffer");

    tex_depth = gl.createTexture();
	gl.bindTexture(gl.TEXTURE_2D, tex_depth);
	gl.texStorage2D(gl.TEXTURE_2D, 1, gl.DEPTH_COMPONENT32F, MAX_SCENE_WIDTH, MAX_SCENE_HEIGHT);
	gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.TEXTURE_2D, tex_depth, 0);
 
    console.log("In createAndBindFrameBuffer() after creating tex_depth texture");

    gl.drawBuffers([gl.COLOR_ATTACHMENT0, gl.COLOR_ATTACHMENT1]);
    
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);

    // filter_fbo
    filter_fbo[0] = gl.createFramebuffer();
    filter_fbo[1] = gl.createFramebuffer();

    tex_filter[0] = gl.createTexture();
    tex_filter[1] = gl.createTexture();

    for(var i = 0; i < 2; i++)
    {
        gl.bindFramebuffer(gl.FRAMEBUFFER, filter_fbo[i]);
        gl.bindTexture(gl.TEXTURE_2D, tex_filter[i]);
        gl.texStorage2D(gl.TEXTURE_2D, 1, gl.RGBA16F,  i ? MAX_SCENE_HEIGHT : MAX_SCENE_WIDTH, i ? MAX_SCENE_WIDTH : MAX_SCENE_HEIGHT);
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, tex_filter[i], 0);
        gl.drawBuffers([gl.COLOR_ATTACHMENT0]);
    }

    gl.bindFramebuffer(gl.FRAMEBUFFER, null);

    //var exposureLUT = new Float32Array([11.0, 6.0, 3.2, 2.8, 2.2, 1.90, 1.80, 1.80, 1.70, 1.70,  1.60, 1.60, 1.50, 1.50, 1.40, 1.40, 1.30, 1.20, 1.10, 1.00]);

    //tex_lut = gl.createTexture();
    //gl.bindTexture(gl.TEXTURE_1D, tex_lut);
    //gl.texStorage1D(gl.TEXTURE_1D, 1, gl.R32F, 20);
    //gl.texSubImage1D(gl.TEXTURE_1D, 0, 0, 20, gl.RED, float, exposureLUT);
    //gl.textureParameteri(gl.TEXTURE_1D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    //gl.textureParameteri(gl.TEXTURE_1D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    //gl.textureParameteri(gl.TEXTURE_1D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EGDE);

    console.log("Exiting in createAndBindFrameBuffer()");
}

function keyDownForhdrBloom(key)
{
    switch(key)
    {
        case '1':
			mode = 1;
			break;
		case '2':
			mode = 2;
			break;
		case '3':
			mode = 3;
			break;
		case 'h':
			exposure = exposure * 1.1;
			break;
		case 'H':
			exposure = exposure / 1.1;
			break;
		case 'b':
		case 'B':
			show_bloom = !show_bloom;
			break;
		case 'v':
		case 'V':
			show_scene = !show_scene;
		case 'A':
		case 'a':
			bloom_factor = bloom_factor + 0.1;
			break;
		case 'Z':
		case 'z':
			bloom_factor = bloom_factor - 0.1;
			break;
		case 's':
		case 'S':
			bloom_thresh_min = bloom_thresh_min + 0.1;
			break;
		case 'x':
		case 'X':
			bloom_thresh_min = bloom_thresh_min - 0.1;
			break;
		case 'D':
		case 'd':
			bloom_thresh_max = bloom_thresh_max + 0.1;
			break;
		case 'c':
		case 'C':
			bloom_thresh_max = bloom_thresh_max - 0.1;
			break;
		case 'N':
		case 'n':
			show_prefilter = !show_prefilter;
			break;
		case 'p':
		case 'P':
			paused = !paused;
			break;
    }
}

function renderScene()
{
    gl.useProgram(gProgramObject_render);
	
	var cameraMatrix = mat4.create();
	var modelMatrix = mat4.create();

	var cameraPosition = [0.0, 0.0, 1.0];
	var cameraFocusPoints = [0.0, 0.0, 0.0];
	var cameraUpAxis = [0.0, 1.0, 0.0];
	
	mat4.lookAt(cameraMatrix, cameraPosition, cameraFocusPoints, cameraUpAxis);
	
	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);

	var rotateMatrix = mat4.create();
	mat4.rotateZ(rotateMatrix, rotateMatrix, degToRad(rotateAngle));

	mat4.multiply(modelMatrix, modelMatrix, rotateMatrix);
	
    gl.uniformMatrix4fv(gProjUniform, false, gPerspectiveProjectionMatrix);
	gl.uniformMatrix4fv(gModelUniform, false, modelMatrix);
	gl.uniformMatrix4fv(gViewUniform, false, cameraMatrix);
	
    gl.uniform1f(g_bloom_thresh_min_uniform, bloom_thresh_min);
	gl.uniform1f(g_bloom_thresh_max_uniform, bloom_thresh_max);
	gl.uniform3fv(g_light_pos_uniform, directional_light);

    var ambient = 0.002 * 1.5;
	
	var fi = 3.14159267 * (32.0 / 0.8);
	var diffuse_color = new Float32Array ([Math.sin(fi) * 0.5 + 0.5, Math.sin(fi + 1.345) * 0.5 + 0.5, Math.sin(fi + 2.567) * 0.5 + 0.5]);
	var specular_color = new Float32Array ( [2.8, 2.8, 2.9] );
	var specular_power = 30.0;
	var ambient_color = new Float32Array ( [ambient * 0.025, ambient * 0.025, ambient * 0.025]);
    
    var diffuse_color_white = new Float32Array ([0.0,   5.0,  5.0]);
    var lightAmbient   = new Float32Array ([ 0.0, 0.0, 0.0 ]);
    var lightDefuse    = new Float32Array ([ 0.5, 0.2, 0.7 ]);
    var lightSpecular  = new Float32Array ([ 0.7, 0.7, 0.7 ]);
    
	gl.uniform3fv(gDiffuseUniform, diffuse_color_white);
	gl.uniform3fv(gSpecularUniform, specular_color);
	gl.uniform3fv(gAmbientUniform, ambient_color);
    gl.uniform1f(gSpecualrPowerUniform, specular_power);
	
	renderSphere();

	gl.useProgram(null);
}

function renderSceneInFramebuffer()
{
    var black = [0.2, 0.2, 0.2, 1.0];

    var one = [1.0, 1.0, 1.0, 1.0];

    gl.bindFramebuffer(gl.FRAMEBUFFER, render_fbo);

    gl.viewport(0, 0, w, h);

    gl.clearBufferfv(gl.COLOR, 0, black);
    gl.clearBufferfv(gl.COLOR, 1, black);
    gl.clearBufferfv(gl.DEPTH, 0, one);

    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LESS);

    renderScene();
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);

    gl.disable(gl.DEPTH_TEST);
}

function applyHdrBloom()
{
    gl.useProgram(gProgramObject_filter);

    // First Pass
	gl.uniform1i(g_horizontal_uniform, 0);
	gl.bindFramebuffer(gl.FRAMEBUFFER, filter_fbo[0]);
	gl.bindTexture(gl.TEXTURE_2D, tex_brightpass);
	gl.bindVertexArray(gVao_quad);
	gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);

    // Second pass
	gl.uniform1i(g_horizontal_uniform, 1);
	gl.bindFramebuffer(gl.FRAMEBUFFER, filter_fbo[1]);
	gl.bindTexture(gl.TEXTURE_2D, tex_filter[0]);
	gl.bindVertexArray(gVao_quad);
	gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);

    gl.useProgram(gProgramObject_resolve);

    gl.uniform1f(g_exposure_uniform, exposure);
	if (show_prefilter)
	{
		gl.uniform1f(g_bloom_factor_uniform, 0.0);
		gl.uniform1f(g_scene_factor_uniform, 1.0);
	}
	else
	{
		gl.uniform1f(g_bloom_factor_uniform, show_bloom ? bloom_factor : 0.0);
		gl.uniform1f(g_scene_factor_uniform, show_scene ? 1.0 : 0.0);
	}

	gl.bindFramebuffer(gl.FRAMEBUFFER, null);
	gl.activeTexture(gl.TEXTURE1);
	gl.bindTexture(gl.TEXTURE_2D, tex_filter[1]);
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, show_prefilter ? tex_brightpass : tex_scene);

	gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);

    gl.useProgram(null);

}

function DisplayHdrBloom()
{
    renderSceneInFramebuffer();

    applyHdrBloom();

}
