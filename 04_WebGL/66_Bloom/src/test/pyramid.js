
var gVertexShaderObject;
var gFragmentShaderObject;
var gShaderProgramObject;

var gMVPUniform;
var gTextureSamplerUniform = 0;

function init_Hrh_PyramidShaderTest()
{
    gShaderProgramObject = compileAndLinkShader("hvs-pyramid", "hfs-pyramid");

	gMVPUniform = gl.getUniformLocation(gShaderProgramObject, "u_mvpMatrix");
	gTextureSamplerUniform = gl.getUniformLocation(gShaderProgramObject, "u_texture_sampler");

}

function draw_Hrh_Pyramid()
{
    gl.useProgram(gShaderProgramObject);
	
	var cameraMatrix = mat4.create();
	var modelViewMatrix = mat4.create();

	var cameraPosition = [0.0, 0.0, 1.0];
	var cameraFocusPoints = [0.0, 0.0, 0.0];
	var cameraUpAxis = [0.0, 1.0, 0.0];
	
	mat4.lookAt(cameraMatrix, cameraPosition, cameraFocusPoints, cameraUpAxis);

	mat4.multiply(modelViewMatrix, modelViewMatrix, cameraMatrix);
	
	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -3.0]);
	var modelViewProjectionMatrix = mat4.create();

	var rotateMatrix = mat4.create();
	mat4.rotateY(rotateMatrix, rotateMatrix, degToRad(rotateAngle));

	mat4.multiply(modelViewMatrix, modelViewMatrix, rotateMatrix);
	
	mat4.multiply(modelViewProjectionMatrix, gPerspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMVPUniform, false, modelViewProjectionMatrix);
	
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, stone_texture);
	gl.uniform1i(gTextureSamplerUniform, 0);
	
	renderPyramid();

	gl.useProgram(null);
	
}

function update_Hrh_Pyramid()
{
	if(rotateAngle > 360.0)
	{
		rotateAngle = 0.0;
	}
	rotateAngle = rotateAngle + 0.1;
}

function uninitialize_Hrh_PyramidShaderTest()
{
    if(gShaderProgramObject)
	{
		if(gFragmentShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gFragmentShaderObject);
			gl.deleteShader(gFragmentShaderObject);
			gFragmentShaderObject = null;
		}
		
		if(gVertexShaderObject)
		{
			gl.detachShader(gShaderProgramObject, gVertexShaderObject);
			gl.deleteShader(gVertexShaderObject);
			gVertexShaderObject = null;
		}
		
		gl.deleteProgram(gShaderProgramObject);
		gShaderProgramObject = null;
	}
}

function uninitializePyramidVao()
{
	
}
