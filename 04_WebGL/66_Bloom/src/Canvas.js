// JavaScript source code
var canvas = null;
var gl;
var canvas_original_width = 0, canvas_original_height = 0;
var bFullScreen = false;
var requestAnimationFrame = window.requestAnimationFrame    || 
							window.webkitAnimationFrame     ||
							window.mozRequestAnimationFrame ||
							window.opRequestAnimationFrame  ||
							window.msRequestAnimationFrame	 ;

var gPerspectiveProjectionMatrix;

var gTextureSamplerUniform = 0;

var stone_texture;

var debugegGLShaderProgram;

var TEST_EFFETS = true;

var rotateAngle = 0.0;

function main()
{
	// get canvas from dom
    canvas = document.getElementById("hrh"); // type inferance

    if (!canvas)
	{
        console.log("canvas failed");
    }
    else
    {
        console.log("Got canvas");
    }

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
    
	// window in buil variable , window is inherited from DOM object
	// 3rd parameter false means do bubbling
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);	
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize(); // explicit warm up resize because webgl don't call resize
	
	draw();	
}

function readShaderFromHTMLTag(elementId)
{
	var shaderFile = document.getElementById(elementId);

	var shaderSourceCode = "";

	// fc - firstChild
	var fc = shaderFile.firstChild;
	while(fc)
	{
		if(fc.nodeType == 3)
		{
			shaderSourceCode = shaderSourceCode + fc.textContent;
		}	
		fc = fc.nextSibling;
	}
	return shaderSourceCode;
}

function compileAndLinkShader(elementIdVs, elementIdFs)
{
	var vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	
	var vertexShaderSourceCode = readShaderFromHTMLTag(elementIdVs);

	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	
	gl.compileShader(vertexShaderObject);
	
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert("Vertex Shader for " + elementIdVs + " errors: "  + error);
			uninitialize();
		}
	}
	
	var fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
	var fragmentShaderSourceCode = readShaderFromHTMLTag(elementIdFs);
	
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	
	gl.compileShader(fragmentShaderObject);
	
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert("Fragment Shader for " + elementIdFs + " errors: " + error );
			uninitialize();
		}
	}
	
	var shaderProgramObject = gl.createProgram();
	
	// attach vertex shader to shader program
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	
	// attch fragment shader to shader program
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	// pri-link binding of shader object with vertex shader possition attribute
	gl.bindAttribLocation(shaderProgramObject, webGLMacros.AMC_FRUSTUM_ATTRIBUTE_POSITION, "a_position");
	gl.bindAttribLocation(shaderProgramObject, webGLMacros.AMC_FRUSTUM_ATTRIBUTE_TEXTURE0, "a_texcoord");
	gl.bindAttribLocation(shaderProgramObject, webGLMacros.AMC_FRUSTUM_ATTRIBUTE_NORMAL, "a_normal");
	gl.bindAttribLocation(shaderProgramObject, webGLMacros.AMC_FRUSTUM_ATTRIBUTE_COLOR, "a_color");

	// LINK PROGRAM
	gl.linkProgram(shaderProgramObject);
	
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	return shaderProgramObject;
}

function keyDown(event)
{
	switch(event.keyCode)
	{
		// for 'f' or 'F'
		case 70:
			toggleFullScreen();
			resize();
			break;
		case 27:
			uninitialize();
			window.close(); // may not work in firefox but will work in chrome and safari
			break;
	}

	switch(event.key)
	{
		
	}
	keyDownForhdrBloom(event.key);
}

function mouseDown()
{
	
}

function toggleFullScreen()
{
	var fullscreen_element = 	document.fullscreenElement || 
								document.webkitFulllscreenElement || 
								document.mozFullScreen || 
								document.msFullscreenElement ||
								null;
								
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
		{
			canvas.requestFullscreen();			
		} 
		else if(canvas.webkitRequestFulllscreen())
		{
			canvas.webkitRequestFulllscreen();
		}
		else if(canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		} 
		else if(canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		} 
		else if ( document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		} 
		else if (document.mozCancelFullScreen)
		{
			document.mozCancelFullScreen();
		} 
		else if (document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		bFullScreen = false;
	}	
}

function init()
{
	
    // get drawing context from the canvas
    gl = canvas.getContext("webgl2");

    if (!gl) 
	{
        console.log("gl failed");
    }
    else 
	{
        console.log("Got gl");
    }
			
	
	if(TEST_EFFETS)
	{
		debugegGLShaderProgram = compileAndLinkShader("hvs-debug-quad", "hfs-debug-quad");

		// init_Hrh_PyramidShaderTest();		

		init_Hrh_HDR_Bloom();
	}	

	stone_texture = gl.createTexture();
	stone_texture.image = new Image();
	stone_texture.image.src = "res/texture/stone.png";
	stone_texture.image.onload = function (){
					gl.bindTexture(gl.TEXTURE_2D, stone_texture);
					gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
					gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
					gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
					// 0 - MIPMAP LEVEL 0
					gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, stone_texture.image);
					gl.bindTexture(gl.TEXTURE_2D, null);
				};
	
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	gl.clearColor(0.2, 0.2, 0.2, 1.0);

	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	gPerspectiveProjectionMatrix = mat4.create();
	
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	// ser the viewport to match	
	gl.viewport(0, 0, canvas.width, canvas.height);

	w  = canvas.width;
	h = canvas.height;
	mat4.perspective(gPerspectiveProjectionMatrix, 
						45.0, 
						parseFloat(canvas.width) /parseFloat(canvas.height), 
						parseFloat(canvas.height) / parseFloat(canvas.width), 
						100.0); // no 0.1f
	
}

function renderTextureOnQuad(texture)
{
	gl.useProgram(debugegGLShaderProgram);

    var gTextureSamplerUniform = gl.getUniformLocation(debugegGLShaderProgram, "debug_texture");
    gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, texture);
	gl.uniform1i(gTextureSamplerUniform, 0);

    renderQuad();
}

function draw()
{
	// code
	gl.clear(gl.COLOR_BUFFER_BIT);
	
	if(TEST_EFFETS)
	{
		// test special effects here
		// draw_Hrh_Pyramid();
	
		DisplayHdrBloom();		
	}
	else
	{
		// show scene
	}

	requestAnimationFrame(draw, canvas);

	update();
}

function update()
{
	update_Hrh_Pyramid();
}

function uninitialize()
{
	uninitializePyramidVao();
	uninitialize_Hrh_PyramidShaderTest();
}
