// JavaScript source code
var canvas = null;
var gl;
var canvas_original_width = 0, canvas_original_height = 0;
var bFullScreen = false;
var requestAnimationFrame = window.requestAnimationFrame    || 
							window.webkitAnimationFrame     ||
							window.mozRequestAnimationFrame ||
							window.opRequestAnimationFrame  ||
							window.msRequestAnimationFrame	 ;

var gPerspectiveProjectionMatrix;

var gTextureSamplerUniform = 0;

var stone_texture;

var modelJson;

class myMesh{
	constructor(vao, numOfElements)
	{
		this.vao = vao;
		this.numOfElements = numOfElements;
	}
}

class NodeData
{
	constructor(name, childrenCount)
	{
		this.name = name;
		this.childrenCount = childrenCount;
	}
}

class Animation
{
	constructor(name, childrenCount, tickspersecond, duration)
		{
			this.m_RooNode = new NodeData(name,  childrenCount);
			this.m_Duration = duration;
			this.m_TicksPerSecond = tickspersecond;
		}
}

var meshesVar = [];

var gModelUniform;
var gViewUniform;
var gPerspectiveUniform;

var gLightAmbientUniform;
var gLightSpecularUniform;
var gLightDiffusedUniform;
var gLightPossitionUniform;

var gMaterialSpecularUniform;
var gMaterialAmbientUniform;
var gMaterialDiffuseUniform;
var gMaterialShininessUniform;

var gIsLightOnUniform;

var isLightOn = 0;

var sphere = null;

var gPerspectiveProjectionMatrix;

var lightPossition = new Float32Array ([ 100.0, 100.0, 100.0, 1.0 ]);
var lightAmbient   = new Float32Array ([ 0.0, 0.0, 0.0 ]);
var lightDefuse    = new Float32Array ([ 0.5, 0.2, 0.7 ]);
var lightSpecular  = new Float32Array ([ 0.7, 0.7, 0.7 ]);

var materialAmbient   = new Float32Array ([ 0.0, 0.0, 0.0 ]);
var materialDefuse    = new Float32Array ([ 1.0, 1.0, 1.0 ]);
var materialSpecular  = new Float32Array ([ 1.0, 1.0, 1.0 ]);
var materialShinyness = 58.0;

fetch('res/vampire/dancing_vampire.json')
.then((response) => response.json())
.then((json) => {
	console.log(json);
	modelJson = json;
	LoadAnimation(json);
	main();
});

function LoadAnimation(json)
{
	if(json.animations == undefined)
	{
		console.log("No Animation");
		return;
	}

	var animation ;

	for(var i = 0 ; i < json.animations.length; i++)
	{
		console.log("Animation: " + json.animations[i].name, json.animations[i].channels.length, 
			json.animations[i].tickspersecond, json.animations[i].duration);

		animation = new Animation(json.animations[i].name, json.animations[i].channels.length, 
			json.animations[i].tickspersecond, json.animations[i].duration );
		
		console.log("Animation: " + animation.m_RooNode.name, animation.m_RooNode.childrenCount, 
		animation.m_TicksPerSecond, animation.m_Duration);
	}
	
}

function parseNodeTree(node)
{
	console.log(node.name);
	for(var i = 0; node.meshes != undefined && i < node.meshes.length; i++)
	{
		var currentMesh = modelJson.meshes[node.meshes[i]];
		//console.log(currentMesh);
		var vertexArray = new Float32Array(currentMesh.vertices);
		var normalArray = new Float32Array(currentMesh.normals);
		var texcoordArray = new Float32Array(currentMesh.texturecoords[0]);
		var elementArray = new Uint16Array(currentMesh.faces.length * 3);
		for(var j = 0; j < currentMesh.faces.length; j++)
		{
			elementArray[j * 3 + 0] = currentMesh.faces[j][0];
			elementArray[j * 3 + 1] = currentMesh.faces[j][1];
			elementArray[j * 3 + 2] = currentMesh.faces[j][2];
		}
		var vao, vboPosition, vboNormal, vboTexcoord, ebo;

		vao = gl.createVertexArray();
		gl.bindVertexArray(vao);

		vboPosition = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER ,vboPosition);

		gl.bufferData(gl.ARRAY_BUFFER, vertexArray, gl.STATIC_DRAW);

		gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
		gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_POSITION);

		vboNormal = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER ,vboNormal);

		gl.bufferData(gl.ARRAY_BUFFER, normalArray, gl.STATIC_DRAW);

		gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
		gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_NORMAL);

		vboTexcoord = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER ,vboTexcoord);

		gl.bufferData(gl.ARRAY_BUFFER, texcoordArray, gl.STATIC_DRAW);

		gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
		gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_TEXTURE0);

		ebo = gl.createBuffer();
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ebo);

		gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, elementArray, gl.STATIC_DRAW);

		gl.bindVertexArray(null);

		meshesVar.push(new myMesh(vao, elementArray.length));
	}

	for(var i = 0; node.children != undefined && i < node.children.length; i++)
	{
		parseNodeTree(node.children[i]);
	}
}

function main()
{
	// get canvas from dom
    canvas = document.getElementById("hrh"); // type inferance

    if (!canvas)
	{
        console.log("canvas failed");
    }
    else
    {
        console.log("Got canvas");
    }

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
    
	// window in buil variable , window is inherited from DOM object
	// 3rd parameter false means do bubbling
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);	
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize(); // explicit warm up resize because webgl don't call resize
	
	draw();	
}

function keyDown(event)
{
	switch(event.keyCode)
	{
		// for 'f' or 'F'
		case 70:
			toggleFullScreen();
			break;
		case 27:
			uninitialize();
			window.close(); // may not work in firefox but will work in chrome and safari
			break;
	}
}

function mouseDown()
{
	
}

function toggleFullScreen()
{
	var fullscreen_element = 	document.fullscreenElement || 
								document.webkitFulllscreenElement || 
								document.mozFullScreen || 
								document.msFullscreenElement ||
								null;
								
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
		{
			canvas.requestFullscreen();			
		} 
		else if(canvas.webkitRequestFulllscreen())
		{
			canvas.webkitRequestFulllscreen();
		}
		else if(canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		} 
		else if(canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		} 
		else if ( document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		} 
		else if (document.mozCancelFullScreen)
		{
			document.mozCancelFullScreen();
		} 
		else if (document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		bFullScreen = false;
	}	
}

function init()
{	
    // get drawing context from the canvas
    gl = canvas.getContext("webgl2");

    if (!gl)
	{
        console.log("gl failed");
    }
    else
	{
        console.log("Got gl");
    }

	parseNodeTree(modelJson.rootnode);

	initPyramidShader();

	stone_texture = gl.createTexture();
	stone_texture.image = new Image();
	stone_texture.image.src = "res/vampire/Vampire_diffuse.png";
	stone_texture.image.onload = function (){
					gl.bindTexture(gl.TEXTURE_2D, stone_texture);
					gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
					gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
					gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
					// 0 - MIPMAP LEVEL 0
					gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, stone_texture.image);
					gl.bindTexture(gl.TEXTURE_2D, null);
				};
	
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	gl.clearColor(0.2, 0.2, 0.2, 1.0);

	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	gPerspectiveProjectionMatrix = mat4.create();
	
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	// ser the viewport to match	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(gPerspectiveProjectionMatrix, 
						45.0, 
						parseFloat(canvas.width) /parseFloat(canvas.height), 
						parseFloat(canvas.height) / parseFloat(canvas.width), 
						100.0); // no 0.1f
	
}

function renderMesh(meshesVar)
{
	for(var i = 0; i < meshesVar.length; i++)
	{
			gl.bindVertexArray(meshesVar[i].vao);
			gl.drawElements(gl.TRIANGLES, meshesVar[i].numOfElements, gl.UNSIGNED_SHORT, 0);
	}
}

function draw()
{
	// code
	gl.clear(gl.COLOR_BUFFER_BIT);
	
	// drawPyramid();

	gl.useProgram(gShaderProgramObject);
	
	var cameraMatrix = mat4.create();
	var modelViewMatrix = mat4.create();
	var scaleMatrix = mat4.create();

	var cameraPosition = [0.0, 0.0, 1.0];
	var cameraFocusPoints = [0.0, 0.0, 0.0];
	var cameraUpAxis = [0.0, 1.0, 0.0];
	
	mat4.lookAt(cameraMatrix, cameraPosition, cameraFocusPoints, cameraUpAxis);

	mat4.multiply(modelViewMatrix, modelViewMatrix, cameraMatrix);
	
	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, -2.0, -5.0]);
	var modelViewProjectionMatrix = mat4.create();


	mat4.scale(scaleMatrix, scaleMatrix, [0.02, 0.02, 0.02]);

	mat4.multiply(modelViewMatrix, modelViewMatrix, scaleMatrix);

	var rotateMatrix = mat4.create();
	mat4.rotateY(rotateMatrix, rotateMatrix, degToRad(rotateAngle));

	mat4.multiply(modelViewMatrix, modelViewMatrix, rotateMatrix);

	
	
	mat4.multiply(modelViewProjectionMatrix, gPerspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(gMVPUniform, false, modelViewProjectionMatrix);
	
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, stone_texture);
	gl.uniform1i(gTextureSamplerUniform, 0);
	
	renderMesh(meshesVar);

	gl.useProgram(null);
	
	requestAnimationFrame(draw, canvas);

	update();
}

function update()
{
	updatePyramid();
}

function uninitialize()
{
	uninitializePyramidVao();
	uninitializePyramidShader();
}
