var vao;
var vbo;
var ebo;

function setUpMeshVertexData(vertices)
{
    // vao and vbo related code

    vao = gl.createVertexArray();
    gl.bindVertexArray(vao);

    vbo = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo);

    glBufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

    // ebo = glCreateBuffer();
    // gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ebo);

    // glBufferData(gl.ELEMENT_ARRAY_BUFFER, indices, gl.STATIC_DRAW);

    // vertex position
    glEnableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_POSITION);
    glVertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, 0);

    // // vertex normals
    // glEnableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_NORMAL);
    // glVertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, offsetof(Vertex, Normal));
// 
    // // vertex texcoord
    // glEnableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_TEXTURE0);
    // glVertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, offsetof(Vertex, TexCoords));
// 
    // // vertex tangent
    // glEnableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_TANGENT);
    // glVertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_TANGENT, 3, GL_FLOAT, GL_FALSE, offsetof(Vertex, Tangent));
// 
    // // vertex bitangent
    // glEnableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_BITANGENT);
    // glVertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_BITANGENT, 3, GL_FLOAT, GL_FALSE, offsetof(Vertex, Bitangent));
// 
    // // ids
    // glEnableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_ID);
    // glVertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_ID, 4, GL_INT, GL_FALSE, offsetof(Vertex, m_BoneIDs));
// 
    // // weights
    // glEnableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_WIGHTS);
    // glVertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_WIGHTS, 4, GL_FLOAT, GL_FALSE, offsetof(Vertex, m_Weights));
// 
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
}
