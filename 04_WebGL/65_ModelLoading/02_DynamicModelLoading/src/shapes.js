var gVaoPyramid = 0;

var sphere = null;

function renderPyramid()
{
    if(gVaoPyramid == 0)
    {
        var gVboPosition;
        var gVboTexcoord;

        var pyramidVertices = new Float32Array
		    ([
		    	 0.0, 1.0, 0.0,
		    	-1.0,-1.0, 1.0,
		    	 1.0, -1.0, 1.0,
            
		    	0.0,  1.0,  0.0,
		    	1.0, -1.0,  1.0,
		    	1.0, -1.0, -1.0,
            
		    	 0.0,  1.0, 0.0,
		    	 1.0, -1.0,-1.0,
		    	-1.0, -1.0,-1.0,
            
		    	 0.0,  1.0,  0.0,
		    	-1.0, -1.0, -1.0,
		    	-1.0, -1.0,  1.0,
		    ]);
	
	    var pyramidTexcoord = new Float32Array
		    ([
		    	//1
		    	0.5, 1.0,
		    	0.0, 0.0,
		    	1.0, 0.0,
            
            
		    	//2     
		    	0.5, 1.0,
		    	1.0, 0.0,
		    	0.0, 0.0,
            
            
		    	//3     
		    	0.5, 1.0,
		    	0.0, 0.0,
		    	1.0, 0.0,
            
            
		    	//4     
		    	0.5, 1.0,
		    	1.0, 0.0,
		    	0.0, 0.0,

	    	]);

	    gVaoPyramid = gl.createVertexArray();
	    gl.bindVertexArray(gVaoPyramid);
        
	    gVboPosition = gl.createBuffer();
	    gl.bindBuffer(gl.ARRAY_BUFFER, gVboPosition);
	    gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
	    gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	    gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_POSITION);
	    gl.bindBuffer(gl.ARRAY_BUFFER, null);
        
	    gVboTexcoord = gl.createBuffer();
	    gl.bindBuffer(gl.ARRAY_BUFFER, gVboTexcoord);
	    gl.bufferData(gl.ARRAY_BUFFER, pyramidTexcoord, gl.STATIC_DRAW);
	    gl.vertexAttribPointer(webGLMacros.HRH_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
	    gl.enableVertexAttribArray(webGLMacros.HRH_ATTRIBUTE_TEXTURE0);
	    gl.bindBuffer(gl.ARRAY_BUFFER, null);
        
	    gl.bindVertexArray(null);
    }

    gl.bindVertexArray(gVaoPyramid);
	gl.drawArrays(gl.TRIANGLES, 0, 12);	
	gl.bindVertexArray(null);

}

function renderSphere()
{
    if(sphere == null)
    {
        sphere = new Mesh();
	    makeSphere(sphere, 2.0, 30, 30);
    }
    sphere.draw();
}
