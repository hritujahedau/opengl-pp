//
//  MyView.m
//  com.myioswindow
//
//  Created by user166446 on 7/3/21.
//  Copyright © 2021 user166446. All rights reserved.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"
#include "Sphere.h"

using namespace vmath;

enum
{
    HRH_ATTRIBUTE_POSITION = 0 ,
    HRH_ATTRIBUTE_COLOR ,
    HRH_ATTRIBUTE_NORMAL ,
    HRH_ATTRIBUTE_TEXTURE_0
};

const float RADIAN_VALUE = 3.14159/180.0f;

int isLightOn = 0;
int isPerFragmentLight = 0;
int isPerVertexLight = 0;

GLfloat lightPosition[] = { 0.0f, 0.0f, 0.0f, 1.0f, 
                            0.0f, 0.0f, 0.0f, 1.0f,
                            0.0f, 0.0f, 0.0f, 1.0f
                        };
                        
GLfloat lightAmbient[] = {  0.0f, 0.0f, 0.0f,
                            0.0f, 0.0f, 0.0f,
                            0.0f, 0.0f, 0.0f
                        };
                        
GLfloat lightDiffuse[] = {  1.0f, 0.0f, 0.0f,
                            0.0f, 1.0f, 0.0f,
                            0.0f, 0.0f, 1.0f
                        };
                            
GLfloat lightSpecular[] = { 1.0f, 0.0f, 0.0f,
                            0.0f, 1.0f, 0.0f,
                            0.0f, 0.0f, 1.0
                        };

GLuint lightAmbientUniform;
GLuint lightSpecularUniform;
GLuint lightDiffusedUniform;
GLuint lightPossitionUniform;

GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f };
GLfloat materialDiffuse[] = { 1.0f, 1.0f, 1.0f };
GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0 };
GLfloat materialShininess = 128.0f;

bool bAnimate = true;

GLfloat sin_angle = 0.0f, cos_angle = 0.0f, radius = 2;

@implementation GLESView
{
    @private
    EAGLContext *eaglContext;
    
    GLuint defaultFrameBuffer;
    GLuint colorRenderBuffer;
    GLuint depthRenderBuffer;
    
    //CADisplayLink *displayLink;
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint vertexShaderObjectPerVertex;
    GLuint fragmentShaderObjectPerVertex;
    
    GLuint vertexShaderObjectPerFragment;
    GLuint fragmentShaderObjectPerFragment;
    
    GLuint shaderProgramObjectPerVertex;
    GLuint shaderProgramObjectPerFragment;
    GLuint shaderProgramObject;
        
    GLuint modelUniform;
    GLuint viewUniform;
    GLuint perspectiveUniform;
    
    GLuint materialSpecularUniform;
    GLuint materialAmbientUniform;
    GLuint materialDiffuseUniform;
    GLuint materialShininessUniform;
    
    GLuint isLightOnUniform;

    GLuint vao_sphere;
    GLuint vbo_sphere_position;
    GLuint vbo_sphere_normal;
    GLuint vbo_sphere_element;
    
    GLfloat sphere_vertices[1146];
    GLfloat Sphere_normals[1146];
    GLfloat Sphere_textrure[764];
    unsigned short sphere_elements[2280];
    
    int gNumVertices;
    int gNumElements;

    mat4 perspectiveProjectionMatrix;
    
}

+(Class)layerClass
{
    //code
    return ([CAEAGLLayer class]);
}

-(id)initWithFrame:(CGRect)frame
{
    //code
    self = [super initWithFrame:frame];
    if(self)
    {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)[super layer];
        [eaglLayer setOpaque:YES];
        [eaglLayer setDrawableProperties: [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil]
         ];
        
        eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            printf("OpenGL-ES Context creation failed\n");
            return nil;
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFrameBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        glGenRenderbuffers(1, &colorRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Framebuffer is not complete\n");
            [self unintialize];
            return nil;
        }
        
        animationFrameInterval = 60;
        isAnimating = NO;

        if( [self loadShaders] == NO)
        {
           [self unintialize];
           return nil;
        }
        [self loadObjects];
        
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        
        //glShadeModel(GL_SMOOTH);
        //glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        //glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

        // we will always cull face for better performance
        glEnable(GL_CULL_FACE);
        perspectiveProjectionMatrix = mat4::identity();
        
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [swipeGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        UILongPressGestureRecognizer *longPressesGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [longPressesGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:longPressesGestureRecognizer];
        
    }
    return self;
}

-(BOOL)loadShaders
{
    vertexShaderObjectPerVertex = glCreateShader(GL_VERTEX_SHADER);
    
    //provide souce code to shader
    const GLchar* vertexShaderSourceCodePerVertex =
                                            " #version 300 es\n"\
                                            " precision highp float;" \
                                            " in vec4 v_position;" \
                                            " in vec3 v_normals; " \
                                            " uniform highp mat4 u_modelMatrix;" \
                                            " uniform highp mat4 u_viewMatrix;" \
                                            " uniform highp mat4 u_projectionMatrix;" \
                                            " uniform highp int u_isLightOn;" \
                                            " uniform highp vec3 u_lightAmbient[3];" \
                                            " uniform highp vec3 u_lightSpecular[3];" \
                                            " uniform highp vec3 u_lightDiffuse[3];" \
                                            " uniform highp vec4 u_lightposition[3]; " \
                                            " uniform highp vec3 u_materialAmbient; " \
                                            " uniform highp vec3 u_materialSpecular; "\
                                            " uniform highp vec3 u_materialDiffuse; "\
                                            " uniform highp float u_materialshininess; "\
                                            " out vec3 phoung_ads_lighting;" \
                                            " vec4 eye_coordinates;"\
                                            " vec3 transformed_normal, light_direction, reflection_vector, view_vector;"\
                                            " vec3 ambient, diffuse_light, specular;"\
                                            " void main()" \
                                            " {" \
                                            " phoung_ads_lighting = vec3(0.0, 0.0, 0.0);" \
                                            " if (u_isLightOn == 1) " \
                                            " {" \
                                                 "eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" \
                                                 "transformed_normal = normalize( mat3( u_viewMatrix * u_modelMatrix ) * v_normals); " \
                                                 "for(int i=0; i< 3;i++) "\
                                                 "{" \
                                                     " light_direction = normalize( vec3 ( ( u_lightposition[i]) - eye_coordinates) ); " \
                                                     " reflection_vector = reflect (-light_direction, transformed_normal); " \
                                                     " view_vector = normalize ( vec3 ( -eye_coordinates)); " \
                                                     " ambient = u_lightAmbient[i] * u_materialAmbient; " \
                                                     " diffuse_light = u_lightDiffuse[i] * u_materialDiffuse * max ( dot (light_direction , transformed_normal), 0.0); "\
                                                     " specular = u_lightSpecular[i] * u_materialSpecular * pow( max ( dot ( reflection_vector , view_vector), 0.0) , u_materialshininess);" \
                                                     " phoung_ads_lighting = phoung_ads_lighting + ambient + diffuse_light + specular;" \
                                                 "}"\
                                            " }" \
                                            " else"\
                                            " {"\
                                            " phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" \
                                            " }"\
                                            " gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" \
                                            " }";

    glShaderSource(vertexShaderObjectPerVertex, 1, (const GLchar**)&vertexShaderSourceCodePerVertex, NULL);

    // compile shader
    glCompileShader(vertexShaderObjectPerVertex);

    // check compilation error
    GLint iInfoLogLength = 0;
    GLint iShaderCompileStatus = 0;
    char* szInfoLog = NULL;
    glGetShaderiv(vertexShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
                printf("Vertex shader compilation error : %s \n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    /***************** FRAGMENT SHADER **********************/

    // CREATE SHADER
    fragmentShaderObjectPerVertex = glCreateShader(GL_FRAGMENT_SHADER);

    //PROVIDE SOURCE CODE

    const char* fragmentShaderSourceCodePerVertex =
                                            " #version 300 es\n"\
                                            " precision highp float;" \
                                            " in vec3 phoung_ads_lighting;"\
                                            " out vec4 FragColor; " \
                                            " void main()" \
                                            " {"\
                                            " FragColor = vec4(phoung_ads_lighting, 1.0); " \
                                            " }";
    
    glShaderSource(fragmentShaderObjectPerVertex, 1, (const char**)&fragmentShaderSourceCodePerVertex, NULL);

    // COMPILE SHADER
    glCompileShader(fragmentShaderObjectPerVertex);

    // compilation error checking

    glGetShaderiv(fragmentShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
                printf("Fragment Shader Compilation Error : %s\n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    // SHADER PROGRAM
    //CREATE SHADER PROGRAM

    shaderProgramObjectPerVertex = glCreateProgram();

    // attach vertex shader to shader program
    glAttachShader(shaderProgramObjectPerVertex, vertexShaderObjectPerVertex);

    // attch fragment shader to shader program
    glAttachShader(shaderProgramObjectPerVertex, fragmentShaderObjectPerVertex);

    // pri-link binding of shader object with vertex shader possition attribute
    glBindAttribLocation(shaderProgramObjectPerVertex, HRH_ATTRIBUTE_POSITION, "v_position");
    glBindAttribLocation(shaderProgramObjectPerVertex, HRH_ATTRIBUTE_NORMAL, "v_normals");

    // LINK PROGRAM
    glLinkProgram(shaderProgramObjectPerVertex);

    // CHECK LINKING ERROR
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(shaderProgramObjectPerVertex, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(shaderProgramObjectPerVertex, iInfoLogLength, &written, szInfoLog);
                printf("Shader Program Link Log : %s", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }
    
    /*************************************************LIGHT PER FRGAMENT********************************************************/

    vertexShaderObjectPerFragment = glCreateShader(GL_VERTEX_SHADER);
    
    //provide souce code to shader
    const GLchar* vertexShaderSourceCodePerFragment =
                                            " #version 300 es\n"\
                                            " precision highp float;" \
                                            " in vec4 v_position;" \
                                            " in vec3 v_normals; " \
                                            " uniform highp mat4 u_modelMatrix;" \
                                            " uniform highp mat4 u_viewMatrix;" \
                                            " uniform highp mat4 u_projectionMatrix;" \
                                            " uniform highp int u_isLightOn;" \
                                            " uniform highp vec4 u_lightposition[3]; " \
                                            " out vec3 light_direction[3]; "\
                                            " out vec3 transformed_normal;"\
                                            " out vec3 view_vector;"\
                                            " void main()" \
                                            " {" \
                                            " if (u_isLightOn == 1) " \
                                            " {" \
                                                 "vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" \
                                                 "transformed_normal =  mat3( u_viewMatrix * u_modelMatrix ) * v_normals;" \
                                                 "for(int i = 0; i< 3; i++)"\
                                                 "{"\
                                                     "light_direction[i] = vec3 ( u_lightposition[i]  - eye_coordinates);" \
                                                 "}"\
                                                 "view_vector =  vec3 ( -eye_coordinates);" \
                                            " }" \
                                            " gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" \
                                            " }";

    glShaderSource(vertexShaderObjectPerFragment, 1, (const GLchar**)&vertexShaderSourceCodePerFragment, NULL);

    // compile shader
    glCompileShader(vertexShaderObjectPerFragment);

    // check compilation error
    iInfoLogLength = 0;
    iShaderCompileStatus = 0;
    szInfoLog = NULL;
    glGetShaderiv(vertexShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
                printf("Vertex shader compilation error : %s \n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    /***************** FRAGMENT SHADER **********************/

    // CREATE SHADER
    fragmentShaderObjectPerFragment = glCreateShader(GL_FRAGMENT_SHADER);

    //PROVIDE SOURCE CODE

    const char* fragmentShaderSourceCodePerFragment =
                                            " #version 300 es\n"\
                                            " precision highp float;" \
                                            " vec3 phoung_ads_lighting;" \
                                            " out vec4 FragColor; " \
                                            " in vec3 light_direction[3]; "\
                                            " in vec3 transformed_normal;"\
                                            " in vec3 view_vector;"\
                                            " vec3 normalized_light_direction; "\
                                            " vec3 normalized_transformed_normal;"\
                                            " vec3 normalized_view_vector;"\
                                            " uniform highp vec3 u_lightAmbient[3];" \
                                            " uniform highp vec3 u_lightSpecular[3];" \
                                            " uniform highp vec3 u_lightDiffuse[3];" \
                                            " uniform highp vec3 u_materialAmbient; " \
                                            " uniform highp vec3 u_materialSpecular; "\
                                            " uniform highp vec3 u_materialDiffuse; "\
                                            " uniform highp float u_materialshininess; "\
                                            " uniform highp int u_isLightOn;" \
                                            " vec3 reflection_vector, ambient, diffuse_light, specular;"\
                                            " void main()" \
                                            " {"\
                                            "phoung_ads_lighting = vec3(0.0, 0.0, 0.0);" \
                                            " if (u_isLightOn == 1) " \
                                            " {" \
                                                 "normalized_view_vector = normalize(view_vector);"\
                                                 "normalized_transformed_normal = normalize(transformed_normal);"\
                                                 "for(int i = 0; i < 3 ; i++)"\
                                                 "{"\
                                                     "normalized_light_direction = normalize(light_direction[i]);"\
                                                     "reflection_vector = reflect (-normalized_light_direction, normalized_transformed_normal); " \
                                                     "ambient = u_lightAmbient[i]  * u_materialAmbient; " \
                                                     "diffuse_light = u_lightDiffuse[i] * u_materialDiffuse * max ( dot (normalized_light_direction , normalized_transformed_normal), 0.0); "\
                                                     "specular = u_lightSpecular[i]  * u_materialSpecular * pow( max ( dot ( reflection_vector , normalized_view_vector), 0.0) , u_materialshininess);" \
                                                     "phoung_ads_lighting = phoung_ads_lighting + ambient + diffuse_light + specular;" \
                                                 "}"\
                                            " }"\
                                            " else"\
                                            " {"\
                                                 "phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" \
                                            " }"\
                                            " FragColor = vec4(phoung_ads_lighting, 1.0); " \
                                            " }";

    
    glShaderSource(fragmentShaderObjectPerFragment, 1, (const char**)&fragmentShaderSourceCodePerFragment, NULL);

    // COMPILE SHADER
    glCompileShader(fragmentShaderObjectPerFragment);

    // compilation error checking

    glGetShaderiv(fragmentShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
                printf("Fragment Shader Compilation Error : %s\n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    // SHADER PROGRAM
    //CREATE SHADER PROGRAM

    shaderProgramObjectPerFragment = glCreateProgram();

    // attach vertex shader to shader program
    glAttachShader(shaderProgramObjectPerFragment, vertexShaderObjectPerFragment);

    // attch fragment shader to shader program
    glAttachShader(shaderProgramObjectPerFragment, fragmentShaderObjectPerFragment);

    // pri-link binding of shader object with vertex shader possition attribute
    glBindAttribLocation(shaderProgramObjectPerFragment, HRH_ATTRIBUTE_POSITION, "v_position");
    glBindAttribLocation(shaderProgramObjectPerFragment, HRH_ATTRIBUTE_NORMAL, "v_normals");

    // LINK PROGRAM
    glLinkProgram(shaderProgramObjectPerFragment);

    // CHECK LINKING ERROR
    iShaderProgramLinkStatus = 0;
    glGetProgramiv(shaderProgramObjectPerFragment, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(shaderProgramObjectPerFragment, iInfoLogLength, &written, szInfoLog);
                printf("Shader Program Link Log : %s", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    return YES;
}

-(void)loadObjects
{
    getSphereVertexData(sphere_vertices, Sphere_normals, Sphere_textrure, sphere_elements);
    gNumVertices = getNumberOfSphereVertices();
    gNumElements = getNumberOfSphereElements();

    glGenVertexArrays(1, &vao_sphere);
    glBindVertexArray(vao_sphere);

    glGenBuffers(1, &vbo_sphere_position);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo

    glGenBuffers(1, &vbo_sphere_normal);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Sphere_normals), Sphere_normals, GL_STATIC_DRAW);
    glVertexAttribPointer(HRH_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(HRH_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // element vbo
    glGenBuffers(1, &vbo_sphere_element);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glBindVertexArray(0); // end for Vao

}

-(void)layoutSubviews
{
    //code
 //     glGenRenderbuffers(1, &colorRenderBuffer);
      glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
      
      [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *) [self layer]];
  //    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
      
      GLint width;
      GLint height;
      
      glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
      glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
      
      glGenRenderbuffers(1, &depthRenderBuffer);
      glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
      glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
      glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
      
      if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
      {
          printf("Framebuffer is not complete\n");
          [self unintialize];
      }
    if(height < 0)
    {
        height = 1;
    }
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    // glORtho(left, right, bottom, top, near, far);
    perspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); 
    
    [self drawView];
}

-(void)drawView
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);

    static GLfloat angle_red = 360.0f,angle_blue = 0.0f, angle_green = 360.0f;
    
    // glUseProgram

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // start from here
    shaderProgramObject = (isPerVertexLight == 1) ? shaderProgramObjectPerVertex : shaderProgramObjectPerFragment;

    glUseProgram(shaderProgramObject);

    // OpenGL Drawing
    // set modelview & modelviewProjection matrices to indentity
    mat4 translateMatrix = vmath::translate(0.0f, 0.0f, -1.55f);
    mat4 viewMatrix = mat4::identity();
    mat4 modelMatrix = translateMatrix;

    // get MVP uniform location
    modelUniform = glGetUniformLocation      (shaderProgramObject, "u_modelMatrix");
    viewUniform = glGetUniformLocation       (shaderProgramObject, "u_viewMatrix");
    perspectiveUniform = glGetUniformLocation(shaderProgramObject, "u_projectionMatrix");
    isLightOnUniform = glGetUniformLocation  (shaderProgramObject, "u_isLightOn");

    lightAmbientUniform = glGetUniformLocation  (shaderProgramObject, "u_lightAmbient");
    lightSpecularUniform = glGetUniformLocation (shaderProgramObject, "u_lightSpecular");
    lightDiffusedUniform = glGetUniformLocation (shaderProgramObject, "u_lightDiffuse");
    lightPossitionUniform = glGetUniformLocation(shaderProgramObject, "u_lightposition");

    materialDiffuseUniform = glGetUniformLocation  (shaderProgramObject, "u_materialDiffuse");
    materialAmbientUniform = glGetUniformLocation  (shaderProgramObject, "u_materialAmbient");
    materialSpecularUniform = glGetUniformLocation (shaderProgramObject, "u_materialSpecular");
    materialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_materialshininess");

    if (isLightOn == 1)
    {
        sin_angle = radius * sin(angle_red * RADIAN_VALUE);
        cos_angle = radius * cos(angle_red * RADIAN_VALUE);
        
        lightPosition[1] = sin_angle;
        lightPosition[2] = cos_angle;
        
        sin_angle = radius * sin(angle_blue * RADIAN_VALUE);
        cos_angle = radius * cos(angle_blue * RADIAN_VALUE);
        
        lightPosition[4] = sin_angle;
        lightPosition[6] = cos_angle;
        
        sin_angle = radius * sin(angle_green * RADIAN_VALUE);
        cos_angle = radius * cos(angle_green * RADIAN_VALUE);
        
        lightPosition[8] = sin_angle;
        lightPosition[9] = cos_angle;
        
        glUniform4fv(lightPossitionUniform, 3, (const GLfloat*) lightPosition);
        glUniform3fv(lightDiffusedUniform,  3 , (const GLfloat*) lightDiffuse);
        glUniform3fv(lightAmbientUniform,   3, (const GLfloat*) lightAmbient);
        glUniform3fv(lightSpecularUniform,  3, (const GLfloat*) lightSpecular);
        
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*) materialAmbient);
        glUniform1f (materialShininessUniform, materialShininess);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*) materialSpecular);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*) materialDiffuse);
    }

    glUniform1i(isLightOnUniform, isLightOn);
    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*) modelMatrix);
    glUniformMatrix4fv(viewUniform, 1, GL_FALSE, (const float*) viewMatrix);
    glUniformMatrix4fv(perspectiveUniform, 1, GL_FALSE, (const GLfloat *) perspectiveProjectionMatrix);

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);

    // stop using OpenGL program object
    glUseProgram(0);

    if (bAnimate == true)
    {
        angle_red = angle_red - 0.5;
        angle_blue = angle_blue + 0.5;
        angle_green = angle_green - 0.5;
        if (angle_red <= 0)
        {
            angle_red = 360;
        }
        if (angle_blue >= 360)
        {
            angle_blue = 1.5;
        }
        if (angle_green <= 0)
        {
            angle_green = 360;
        }
    }


    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
}

-(void)startAnimation
{
    //code
    if(isAnimating == NO)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    //code
    if(isAnimating == YES)
    {
        [displayLink invalidate];
        displayLink = nil;
        isAnimating = NO;
    }
}
-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    //code
    if (isLightOn == 0)
    {
        isLightOn = 1;
    }
    else
    {
        isLightOn = 0;
    }
    
}

-(void) onDoubleTap:(UITapGestureRecognizer *)gr
{
    //code
    if(isPerVertexLight == 0)
    {
        isPerVertexLight = 1;
        isPerFragmentLight =0;
        isLightOn = 1;
        bAnimate = 1;
        shaderProgramObject = shaderProgramObjectPerVertex;
    }
    else
    {
        isPerVertexLight = 0;
    }
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
        //code
    //centralText = @"swipe";
   //[self setNeedsDisplay];
    [self unintialize];
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    //code
    if(isPerFragmentLight == 0)
    {
        isPerFragmentLight = 1;
        isPerVertexLight = 0;
        isLightOn = 1;
        bAnimate = 1;
        shaderProgramObject = shaderProgramObjectPerFragment;
    }
    else
    {
        isPerFragmentLight = 0;
    }
}

-(void)unintialize
{
    //code
        if (vao_sphere)
    {
        glDeleteVertexArrays(1, &vao_sphere);
        vao_sphere = 0;
    }

    // destroy vbo
    if (vbo_sphere_position)
    {
        glDeleteBuffers(1, &vbo_sphere_position);
        vbo_sphere_position = 0;
    }

    if (vbo_sphere_element)
    {
        glDeleteBuffers(1, &vbo_sphere_element);
        vbo_sphere_element = 0;
    }

    if (vbo_sphere_normal)
    {
        glDeleteBuffers(1, &vbo_sphere_normal);
        vbo_sphere_normal = 0;
    }

    // deatch vertex shader from shader program object
    glDetachShader(shaderProgramObjectPerVertex, vertexShaderObjectPerVertex);

    // detach fragment shader from shader program obejct
    glDetachShader(shaderProgramObjectPerVertex, fragmentShaderObjectPerVertex);

    // delete vertex shader object
    glDeleteShader(vertexShaderObjectPerVertex);

    vertexShaderObjectPerVertex = 0;

    // delete fragment shader object
    glDeleteShader(fragmentShaderObjectPerVertex);
    fragmentShaderObjectPerVertex = 0;

    // delete shader program object
    glDeleteProgram(shaderProgramObjectPerVertex);
    shaderProgramObjectPerVertex = 0;

    // fragment
    // deatch vertex shader from shader program object
    glDetachShader(shaderProgramObjectPerFragment, vertexShaderObjectPerFragment);

    // detach fragment shader from shader program obejct
    glDetachShader(shaderProgramObjectPerFragment, fragmentShaderObjectPerFragment);

    // delete vertex shader object
    glDeleteShader(vertexShaderObjectPerFragment);

    vertexShaderObjectPerFragment = 0;

    // delete fragment shader object
    glDeleteShader(fragmentShaderObjectPerFragment);
    fragmentShaderObjectPerFragment = 0;

    // delete shader program object
    glDeleteProgram(shaderProgramObjectPerFragment);
    shaderProgramObjectPerFragment = 0;

    // unlink shader program
    glUseProgram(0);

    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer = 0;
    }
    
    if(colorRenderBuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderBuffer);
        colorRenderBuffer = 0;
    }
    
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer = 0;
    }
    
    if(eaglContext)
    {
        if([EAGLContext currentContext] == eaglContext )
        {
            [EAGLContext setCurrentContext:nil];
            [eaglContext release];
            eaglContext = nil;
        }
    }
}


-(void)dealloc
{
    //code
    [self unintialize];
    [super dealloc];
}

@end
