//
//  MyView.m
//  com.myioswindow
//
//  Created by user166446 on 7/3/21.
//  Copyright © 2021 user166446. All rights reserved.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"

using namespace vmath;

enum
{
    HRH_ATTRIBUTE_POSITION = 0 ,
    HRH_ATTRIBUTE_COLOR ,
    HRH_ATTRIBUTE_NORMAL ,
    HRH_ATTRIBUTE_TEXTURE_0
};

int isLightOn = 0;

GLfloat lightPosition_0[] = { 100.0f, 100.0f, 100.0f, 1.0f };
GLfloat lightAmbient_0[] = { 1.0f, 0.0f, 0.0f};
GLfloat lightDiffuse_0[] = { 1.0f, 0.0f, 0.0f};
GLfloat lightSpecular_0[] = { 0.0f, 0.0f, 1.0f};

GLfloat lightPosition_1[] = { -100.0f, 100.0f, 100.0f, 1.0f };
GLfloat lightAmbient_1[] = { 0.0f, 0.0f, 1.0f};
GLfloat lightDiffuse_1[] = { 0.0f, 0.0f, 1.0f};
GLfloat lightSpecular_1[] = { 0.0f, 0.0f, 1.0 };

GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f};
GLfloat materialDiffuse[] = { 1.0f, 1.0f, 1.0f};
GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0 };
GLfloat materialShininess = 128.0f;

bool bAnimate = false;

@implementation GLESView
{
    @private
    EAGLContext *eaglContext;
    
    GLuint defaultFrameBuffer;
    GLuint colorRenderBuffer;
    GLuint depthRenderBuffer;
    
    //CADisplayLink *displayLink;
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;

    GLuint vao_pyramid;
    GLuint vbo_pyramid_position;
    GLuint vbo_pyramid_normals;

    GLuint modelUniform;
    GLuint viewUniform;
    GLuint perspectiveUniform;
    
    GLuint light_0_AmbientUniform;
    GLuint light_0_SpecularUniform;
    GLuint light_0_DiffusedUniform;
    GLuint light_0_PossitionUniform;
    
    GLuint light_1_AmbientUniform;
    GLuint light_1_SpecularUniform;
    GLuint light_1_DiffusedUniform;
    GLuint light_1_PossitionUniform;
    
    GLuint materialSpecularUniform;
    GLuint materialAmbientUniform;
    GLuint materialDiffuseUniform;
    GLuint materialShininessUniform;
    
    GLuint isLightOnUniform;    

    mat4 perspectiveProjectionMatrix;
    
}

+(Class)layerClass
{
    //code
    return ([CAEAGLLayer class]);
}

-(id)initWithFrame:(CGRect)frame
{
    //code
    self = [super initWithFrame:frame];
    if(self)
    {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)[super layer];
        [eaglLayer setOpaque:YES];
        [eaglLayer setDrawableProperties: [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil]
         ];
        
        eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            printf("OpenGL-ES Context creation failed\n");
            return nil;
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFrameBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        glGenRenderbuffers(1, &colorRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Framebuffer is not complete\n");
            [self unintialize];
            return nil;
        }
        
        animationFrameInterval = 60;
        isAnimating = NO;

        if( [self loadShaders] == NO)
        {
           [self unintialize];
           return nil;
        }
        [self loadObjects];
        
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        
        //glShadeModel(GL_SMOOTH);
        //glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        //glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

        // we will always cull face for better performance
        glEnable(GL_CULL_FACE);
        perspectiveProjectionMatrix = mat4::identity();
        
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [swipeGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        UILongPressGestureRecognizer *longPressesGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [longPressesGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:longPressesGestureRecognizer];
        
    }
    return self;
}

-(BOOL)loadShaders
{
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    //provide souce code to shader
    const GLchar* vertexShaderSourceCode =
                                            "#version 300 es\n"\
                                            " in vec4 v_position;" \
                                            " in vec3 v_normals; " \
                                            " uniform mat4 u_modelMatrix;" \
                                            " uniform mat4 u_viewMatrix;" \
                                            " uniform mat4 u_projectionMatrix;" \
                                            " uniform int u_isLightOn;" \
                                            " uniform vec4 u_light_0_position; " \
                                            " uniform vec4 u_light_1_position; " \
                                            " out vec3 light_direction_0; "\
                                            " out vec3 light_direction_1; "\
                                            " out vec3 transformed_normal;"\
                                            " out vec3 view_vector;"\
                                            " void main()" \
                                            " {" \
                                            "       if (u_isLightOn == 1) " \
                                            "       {" \
                                            "       vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" \
                                            "       transformed_normal =  mat3( u_viewMatrix * u_modelMatrix ) * v_normals;" \
                                            "       light_direction_0 = vec3 ( u_light_0_position  - eye_coordinates);" \
                                            "       light_direction_1 = vec3 ( u_light_1_position  - eye_coordinates);" \
                                            "       view_vector =  vec3 ( -eye_coordinates);" \
                                            "       }" \
                                            "       gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" \
                                            " }";

    glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

    // compile shader
    glCompileShader(vertexShaderObject);

    // check compilation error
    GLint iInfoLogLength = 0;
    GLint iShaderCompileStatus = 0;
    char* szInfoLog = NULL;
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("Vertex shader compilation error : %s \n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    /***************** FRAGMENT SHADER **********************/

    // CREATE SHADER
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    //PROVIDE SOURCE CODE

    const char* fragmentShaderSourceCode =
                                            " #version 300 es\n"\
                                            " precision highp float;" \
                                            " vec3 phoung_ads_lighting;" \
                                            " out vec4 FragColor; " \
                                            " in vec3 light_direction_0; "\
                                            " in vec3 light_direction_1; "\
                                            " in vec3 transformed_normal;"\
                                            " in vec3 view_vector;"\
                                            " vec3 normalized_light_direction_0; "\
                                            " vec3 normalized_light_direction_1; "\
                                            " vec3 normalized_transformed_normal;"\
                                            " vec3 normalized_view_vector;"\
                                            " uniform highp vec3 u_light_0_Ambient;" \
                                            " uniform highp vec3 u_light_0_Specular;" \
                                            " uniform highp vec3 u_light_0_Diffuse;" \
                                            " uniform highp vec3 u_light_1_Ambient;" \
                                            " uniform highp vec3 u_light_1_Specular;" \
                                            " uniform highp vec3 u_light_1_Diffuse;" \
                                            " uniform highp vec3 u_materialAmbient; " \
                                            " uniform highp vec3 u_materialSpecular; "\
                                            " uniform highp vec3 u_materialDiffuse; "\
                                            " uniform highp float u_materialshininess; "\
                                            " uniform highp int u_isLightOn;" \
                                            " void main()" \
                                            " {"\
                                            "      if (u_isLightOn == 1) " \
                                            "      {" \
                                            "           normalized_light_direction_0 = normalize(light_direction_0);"\
                                            "           normalized_light_direction_1 = normalize(light_direction_1);"\
                                            "           normalized_transformed_normal = normalize(transformed_normal);"\
                                            "           normalized_view_vector = normalize(view_vector);"\
                                            "           vec3 reflection_vector = reflect (-normalized_light_direction_0, normalized_transformed_normal); " \
                                            "           vec3 ambient = u_light_0_Ambient  * u_materialAmbient; " \
                                            "           vec3 diffuse_light = u_light_0_Diffuse * u_materialDiffuse * max ( dot (normalized_light_direction_0 , normalized_transformed_normal), 0.0); "\
                                            "           vec3 specular = u_light_0_Specular  * u_materialSpecular * pow( max ( dot ( reflection_vector , normalized_view_vector), 0.0) , u_materialshininess);" \
                                            "           phoung_ads_lighting = ambient + diffuse_light + specular;" \
                                            "           reflection_vector = reflect (-normalized_light_direction_1, normalized_transformed_normal); " \
                                            "           ambient = u_light_1_Ambient  * u_materialAmbient; " \
                                            "           diffuse_light = u_light_1_Diffuse * u_materialDiffuse * max ( dot (normalized_light_direction_1 , normalized_transformed_normal), 0.0); "\
                                            "           specular = u_light_1_Specular  * u_materialSpecular * pow( max ( dot ( reflection_vector , normalized_view_vector), 0.0) , u_materialshininess);" \
                                            "           phoung_ads_lighting = phoung_ads_lighting + ambient + diffuse_light + specular;" \
                                            "      }"\
                                            "      else"\
                                            "      {"\
                                                      "phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" \
                                            "      }"\
                                            "      FragColor = vec4(phoung_ads_lighting, 1.0); " \
                                            " }";
    

    glShaderSource(fragmentShaderObject, 1, (const char**)&fragmentShaderSourceCode, NULL);

    // COMPILE SHADER
    glCompileShader(fragmentShaderObject);

    // compilation error checking

    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("Fragment Shader Compilation Error : %s\n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    // SHADER PROGRAM
    //CREATE SHADER PROGRAM

    shaderProgramObject = glCreateProgram();

    // attach vertex shader to shader program
    glAttachShader(shaderProgramObject, vertexShaderObject);

    // attch fragment shader to shader program
    glAttachShader(shaderProgramObject, fragmentShaderObject);

    // pri-link binding of shader object with vertex shader possition attribute
    glBindAttribLocation(shaderProgramObject, HRH_ATTRIBUTE_POSITION, "v_position");
    glBindAttribLocation(shaderProgramObject, HRH_ATTRIBUTE_NORMAL, "v_normals");


    // LINK PROGRAM
    glLinkProgram(shaderProgramObject);

    // CHECK LINKING ERROR
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                printf("Shader Program Link Log : %s", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }
    
    // get MVP uniform location
    modelUniform = glGetUniformLocation(shaderProgramObject, "u_modelMatrix");
    viewUniform = glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
    perspectiveUniform = glGetUniformLocation(shaderProgramObject, "u_projectionMatrix");
    isLightOnUniform = glGetUniformLocation(shaderProgramObject, "u_isLightOn");
    
    light_0_AmbientUniform = glGetUniformLocation  (shaderProgramObject, "u_light_0_Ambient");
    light_0_SpecularUniform = glGetUniformLocation (shaderProgramObject, "u_light_0_Specular");
    light_0_DiffusedUniform = glGetUniformLocation (shaderProgramObject, "u_light_0_Diffuse");
    light_0_PossitionUniform = glGetUniformLocation(shaderProgramObject, "u_light_0_position");

    light_1_AmbientUniform = glGetUniformLocation  (shaderProgramObject, "u_light_1_Ambient");
    light_1_SpecularUniform = glGetUniformLocation (shaderProgramObject, "u_light_1_Specular");
    light_1_DiffusedUniform = glGetUniformLocation (shaderProgramObject, "u_light_1_Diffuse");
    light_1_PossitionUniform = glGetUniformLocation(shaderProgramObject, "u_light_1_position");

    materialDiffuseUniform = glGetUniformLocation  (shaderProgramObject, "u_materialDiffuse");
    materialAmbientUniform = glGetUniformLocation  (shaderProgramObject, "u_materialAmbient");
    materialSpecularUniform = glGetUniformLocation (shaderProgramObject, "u_materialSpecular");
    materialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_materialshininess");
    
    return YES;
}

-(void)loadObjects
{
    // ***** vertex, colors, shadersatribs , vbo_pyramid_position, vao_pyramid initialization ********/
    glGenVertexArrays(1, &vao_pyramid);
    glBindVertexArray(vao_pyramid);

    // triangle
    // position
const GLfloat pyramidVertices[] =
                {
                    // FRONT FACE
                    0.0f, 1.0f, 0.0f,
                    -1.0f,-1.0f, 1.0f,
                    1.0f, -1.0f,1.0f,
                    
                    // RIGHT FACE
                    0.0f, 1.0f, 0.0f,
                    1.0f, -1.0f, 1.0f,
                    1.0f, -1.0f, -1.0f,

                    // BACK FACE
                    0.0f, 1.0f, 0.0f,
                    1.0f, -1.0f,-1.0f,
                    -1.0f, -1.0f,-1.0f,

                    // LEFT FACE
                    0.0f, 1.0f, 0.0f,
                    -1.0f, -1.0f,-1.0f,
                    -1.0f, -1.0f,1.0f,
                };


    glGenBuffers(1, &vbo_pyramid_position);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramid_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);    
    glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);

    GLfloat pyramid_normals[] = {
        0.0f, 0.447214f, 0.894427f, //FRONT 
        0.0f, 0.447214f, 0.894427f, //FRONT 
        0.0f, 0.447214f, 0.894427f, //FRONT         
        
        0.894427f, 0.447214f, 0.0f, // RIGHT
        0.894427f, 0.447214f, 0.0f, // RIGHT
        0.894427f, 0.447214f, 0.0f, // RIGHT
        
        0.0f, 0.447214f, -0.894427f, // BACK
        0.0f, 0.447214f, -0.894427f, // BACK
        0.0f, 0.447214f, -0.894427f, // BACK
        
        -0.894427f, 0.447214f, 0.0f, // LEFT
        -0.894427f, 0.447214f, 0.0f, // LEFT
        -0.894427f, 0.447214f, 0.0f // LEFT
    };

    glGenBuffers(1, &vbo_pyramid_normals);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramid_normals);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramid_normals), pyramid_normals, GL_STATIC_DRAW);
    glVertexAttribPointer(HRH_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(HRH_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo_pyramid_position

    glBindVertexArray(0); // end for vao_pyramid
}

-(void)layoutSubviews
{
    //code
      glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
      
      [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *) [self layer]];
      
      GLint width;
      GLint height;
      
      glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
      glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
      
      glGenRenderbuffers(1, &depthRenderBuffer);
      glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
      glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
      glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
      
      if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
      {
          printf("Framebuffer is not complete\n");
          [self unintialize];
      }
    if(height < 0)
    {
        height = 1;
    }
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    // glORtho(left, right, bottom, top, near, far);
    perspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); // no 0.1f
    
    [self drawView];
}

-(void)drawView
{
    // VARIABLE DECLARATION
    static GLfloat angle = 0.0f;

    //code
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);

    // glUseProgram
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // start from here

    // START USING OpenGL program object
    glUseProgram(shaderProgramObject);

    // OpenGL Drawing
    // set modelview & modelviewProjection matrices to indentity
    mat4 translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
    mat4 rotateMatrix = vmath::rotate((GLfloat)angle, 0.0f, 1.0f, 0.0f);
    mat4 viewMatrix = mat4::identity();
    mat4 modelMatrix = mat4::identity();    
    
    if (isLightOn == 1)
    {
        glUniform4fv(light_0_PossitionUniform, 1, (const GLfloat*) lightPosition_0);
        glUniform3fv(light_0_DiffusedUniform, 1, (const GLfloat*) lightDiffuse_0);
        glUniform3fv(light_0_AmbientUniform,  1 , (const GLfloat*)lightAmbient_0);
        glUniform3fv(light_0_SpecularUniform, 1 , (const GLfloat*)lightSpecular_0);

        glUniform4fv(light_1_PossitionUniform, 1, (const GLfloat*)lightPosition_1);
        glUniform3fv(light_1_DiffusedUniform, 1, (const GLfloat*)lightDiffuse_1);
        glUniform3fv(light_1_AmbientUniform, 1, (const GLfloat*)lightAmbient_1);
        glUniform3fv(light_1_SpecularUniform, 1, (const GLfloat*)lightSpecular_1);

        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*) materialAmbient);
        glUniform1f (materialShininessUniform, materialShininess);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*) materialSpecular);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*) materialDiffuse);
    }

    glUniform1i(isLightOnUniform, isLightOn);
    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);
    glUniformMatrix4fv(viewUniform, 1, GL_FALSE, (const float*) viewMatrix);
    glUniformMatrix4fv(perspectiveUniform, 1, GL_FALSE, (const GLfloat *) perspectiveProjectionMatrix);

    modelMatrix = translateMatrix * rotateMatrix;
    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);
    // *** bind vao **
    glBindVertexArray(vao_pyramid);
    
    glDrawArrays(GL_TRIANGLES, 0, 12);

    glBindVertexArray(0);

    // stop using OpenGL program object
    glUseProgram(0);

    if(angle > 360.0f)
    {
        angle = 0.0f;
    }
    angle = angle + 0.1f;

    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void)startAnimation
{
    //code
    if(isAnimating == NO)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    //code
    if(isAnimating == YES)
    {
        [displayLink invalidate];
        displayLink = nil;
        isAnimating = NO;
    }
}
-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    //code
    if (bAnimate == false)
    {
        bAnimate = true;
    }
    else
    {
        bAnimate = false;
    }
    
}

-(void) onDoubleTap:(UITapGestureRecognizer *)gr
{
    //code
    if (isLightOn == 0)
    {
        isLightOn = 1;
    }
    else
    {
        isLightOn = 0;
    }

}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    //code
    [self unintialize];
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    //code

    
}

-(void)unintialize
{
    //code
    
    // destroy vao_pyramid
    if (vao_pyramid)
    {
        glDeleteVertexArrays(1, &vao_pyramid);
        vao_pyramid = 0;
    }

    // destroy vbo_pyramid_position
    if (vbo_pyramid_position)
    {
        glDeleteBuffers(1, &vbo_pyramid_position);
        vbo_pyramid_position = 0;
    }
    
    // deatch vertex shader from shader program object
    glDetachShader(shaderProgramObject, vertexShaderObject);

    // detach fragment shader from shader program obejct
    glDetachShader(shaderProgramObject, fragmentShaderObject);

    // delete vertex shader object
    glDeleteShader(vertexShaderObject);

    vertexShaderObject = 0;

    // delete fragment shader object
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;

    // delete shader program object
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;

    // unlink shader program
    glUseProgram(0);

    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer = 0;
    }
    
    if(colorRenderBuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderBuffer);
        colorRenderBuffer = 0;
    }
    
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer = 0;
    }
    
    if(eaglContext)
    {
        if([EAGLContext currentContext] == eaglContext )
        {
            [EAGLContext setCurrentContext:nil];
            [eaglContext release];
            eaglContext = nil;
        }
    }
}


-(void)dealloc
{
    //code
    [self unintialize];
    [super dealloc];
}

@end
