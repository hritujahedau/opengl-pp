//
//  MyView.m
//  com.myioswindow
//
//  Created by user166446 on 7/3/21.
//  Copyright © 2021 user166446. All rights reserved.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"

using namespace vmath;

enum
{
    HRH_ATTRIBUTE_POSITION = 0 ,
    HRH_ATTRIBUTE_COLOR ,
    HRH_ATTRIBUTE_NORMAL ,
    HRH_ATTRIBUTE_TEXTURE_0
};

GLfloat graphColor[] = { 0.0, 0.0, 1.0f,0.0, 0.0, 1.0f };
GLfloat vertices[] = { 1.0f, 0.0, 0.0f, -1.0f, 0.0f, 0.0f };

@implementation GLESView
{
    @private
    EAGLContext *eaglContext;
    
    GLuint defaultFrameBuffer;
    GLuint colorRenderBuffer;
    GLuint depthRenderBuffer;
    
    //CADisplayLink *displayLink;
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    GLuint gVao_graph;
    GLuint gVbo_graph_position;
    GLuint gVbo_graph_color;

    GLuint gMVPUniform;

    mat4 gPerspectiveProjectionMatrix;
    
}

+(Class)layerClass
{
    //code
    return ([CAEAGLLayer class]);
}

-(id)initWithFrame:(CGRect)frame
{
    //code
    self = [super initWithFrame:frame];
    if(self)
    {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)[super layer];
        [eaglLayer setOpaque:YES];
        [eaglLayer setDrawableProperties: [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil]
         ];
        
        eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            printf("OpenGL-ES Context creation failed\n");
            return nil;
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFrameBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        glGenRenderbuffers(1, &colorRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Framebuffer is not complete\n");
            [self unintialize];
            return nil;
        }
        
        animationFrameInterval = 60;
        isAnimating = NO;

        if( [self loadShaders] == NO)
        {
           [self unintialize];
           return nil;
        }
        [self loadObjects];
        
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        
        //glShadeModel(GL_SMOOTH);
        //glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        //glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

        // we will always cull face for better performance
        glEnable(GL_CULL_FACE);
        gPerspectiveProjectionMatrix = mat4::identity();
        
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [swipeGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        UILongPressGestureRecognizer *longPressesGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [longPressesGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:longPressesGestureRecognizer];
        
    }
    return self;
}

-(BOOL)loadShaders
{
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    //provide souce code to shader
    const GLchar* vertexShaderSourceCode =
                                            "#version 300 es\n"\
                                            "in vec4 vPosition;" \
                                            "in vec4 vColor;" \
                                            "out vec4 out_color; " \
                                            "uniform mat4 u_mvpMatrix;" \
                                            "void main()"\
                                            "{" \
                                            "out_color = vColor;" \
                                            "gl_Position = u_mvpMatrix * vPosition;"\
                                            "}";


    glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

    // compile shader
    glCompileShader(gVertexShaderObject);

    // check compilation error
    GLint iInfoLogLength = 0;
    GLint iShaderCompileStatus = 0;
    char* szInfoLog = NULL;
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("Vertex shader compilation error : %s \n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    /***************** FRAGMENT SHADER **********************/

    // CREATE SHADER
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    //PROVIDE SOURCE CODE

    const char* fragmentShaderSourceCode =
                                            "#version 300 es\n"\
                                            "precision highp float;" \
                                            "in vec4 out_color;" \
                                            "out vec4 FragColor; " \
                                            "void main()"\
                                            "{"\
                                            "FragColor = out_color;" \
                                            "}";
    

    glShaderSource(gFragmentShaderObject, 1, (const char**)&fragmentShaderSourceCode, NULL);

    // COMPILE SHADER
    glCompileShader(gFragmentShaderObject);

    // compilation error checking

    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("Fragment Shader Compilation Error : %s\n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    // SHADER PROGRAM
    //CREATE SHADER PROGRAM

    gShaderProgramObject = glCreateProgram();

    // attach vertex shader to shader program
    glAttachShader(gShaderProgramObject, gVertexShaderObject);

    // attch fragment shader to shader program
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);

    // pri-link binding of shader object with vertex shader possition attribute
    glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_COLOR, "vColor");

    // LINK PROGRAM
    glLinkProgram(gShaderProgramObject);

    // CHECK LINKING ERROR
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                printf("Shader Program Link Log : %s", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }
    
    // get MVP uniform location
    gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvpMatrix");
    
    return YES;
}

-(void)loadObjects
{
    glGenVertexArrays(1, &gVao_graph);
    glBindVertexArray(gVao_graph);

    // position
    glGenBuffers(1, &gVbo_graph_position);
    glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position); //Bind gVbo
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), NULL, GL_DYNAMIC_DRAW); // feed the data #1 
    glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);  // kontya variable sathi, kiti elements, type #2    
    glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION); // #3
    glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo

    // color
    glGenBuffers(1, &gVbo_graph_color); // Bind gVbo_color 
    glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(graphColor), graphColor, GL_DYNAMIC_DRAW); // #1
    glVertexAttribPointer(HRH_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL); // #2
    glEnableVertexAttribArray(HRH_ATTRIBUTE_COLOR); // #3
    glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind Bind gVbo_color
    
    // unbind gVao
    glBindVertexArray(0); // unbind for gVao
}

-(void)layoutSubviews
{
    //code
 //     glGenRenderbuffers(1, &colorRenderBuffer);
      glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
      
      [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *) [self layer]];
  //    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
      
      GLint width;
      GLint height;
      
      glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
      glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
      
      glGenRenderbuffers(1, &depthRenderBuffer);
      glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
      glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
      glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
      
      if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
      {
          printf("Framebuffer is not complete\n");
          [self unintialize];
      }
    if(height < 0)
    {
        height = 1;
    }
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    // glORtho(left, right, bottom, top, near, far);
    gPerspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); 
    
    [self drawView];
}

-(void)drawView
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);

    // glUseProgram
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // start here 

    glUseProgram(gShaderProgramObject);

    mat4 translateMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    mat4 modelViewMatrix = translateMatrix;
    mat4 modelViewProjectionMatrix = mat4::identity();    
    modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix; // ORDER IS IMPORTANT
    glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

    [self graph];
    [self kundali];

    glUseProgram(0);

    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
}

-(void)kundali
{
    // VARIABLE DECLARATION
    //  GLfloat rectangleLeft = -0.75f, rectangleTop = 0.5f, rectangleWidth = 1.5f, rectangleHight = 1.0f;
    GLfloat rectangleLeft = -0.75f, rectangleTop = 0.5f, rectangleWidth = 1.5f, rectangleHight = 1.0f;
    GLfloat kundali_color[] = {
        1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
    };

    GLfloat kundali_vertices_1[] =
    {
        rectangleLeft, rectangleTop,  0.0f,
        rectangleLeft, rectangleTop - rectangleHight, 0.0f,
        rectangleLeft + rectangleWidth, rectangleTop - rectangleHight, 0.0f,
        rectangleLeft + rectangleWidth, rectangleTop, 0.0f,
        rectangleLeft, rectangleTop,  0.0f,
    };

    
    glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(kundali_color), kundali_color, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(kundali_vertices_1), kundali_vertices_1, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(gVao_graph);
    glDrawArrays(GL_LINES, 0, 2);
    glDrawArrays(GL_LINES, 1, 2);
    glDrawArrays(GL_LINES, 2, 2);
    glDrawArrays(GL_LINES, 3, 2);
    glBindVertexArray(0);
    
    GLfloat kundali_vertices_2[] = {
        rectangleLeft, rectangleTop - (rectangleHight / 2),  0.0f,
        rectangleLeft + (rectangleWidth / 2), rectangleTop - rectangleHight, 0.0f,
        rectangleLeft + rectangleWidth, rectangleTop - (rectangleHight / 2), 0.0f,
        rectangleLeft + (rectangleWidth / 2), rectangleTop, 0.0f,
        rectangleLeft, rectangleTop - (rectangleHight / 2),  0.0f,
    };

    glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(kundali_color), kundali_color, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(kundali_vertices_2), kundali_vertices_2, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(gVao_graph);
    glDrawArrays(GL_LINES, 0, 2);
    glDrawArrays(GL_LINES, 1, 2);
    glDrawArrays(GL_LINES, 2, 2);
    glDrawArrays(GL_LINES, 3, 2);
    glBindVertexArray(0);

    GLfloat kundali_vertices_3[] = {
        rectangleLeft + rectangleWidth, rectangleTop,  0.0f,
        rectangleLeft, rectangleTop - rectangleHight, 0.0f,
        rectangleLeft + rectangleWidth, rectangleTop - rectangleHight, 0.0f,
        rectangleLeft , rectangleTop, 0.0f,
        rectangleLeft + rectangleWidth, rectangleTop,  0.0f,
    };
    glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(kundali_color), kundali_color, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(kundali_vertices_3), kundali_vertices_3, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(gVao_graph);
    glDrawArrays(GL_LINES, 0, 2);
    glDrawArrays(GL_LINES, 1, 2);
    glDrawArrays(GL_LINES, 2, 2);
    glDrawArrays(GL_LINES, 3, 2);
    glBindVertexArray(0);
}

-(void)graph
{
    
    GLfloat distanceForX = 0, distanceForY = 0;

    graphColor[0] = 0.0f;
    graphColor[1] = 0.0f;
    graphColor[2] = 1.0f;
    graphColor[3] = 0.0f;
    graphColor[4] = 0.0f;
    graphColor[5] = 1.0f;

    glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(graphColor), graphColor, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    for (int i = 0; i < 50; i++) {

        // verticle up
        vertices[0] = 1.5f;
        vertices[1] = distanceForX;
        vertices[2] = 0.0f;

        vertices[3] = -1.5f;
        vertices[4] = distanceForX;
        vertices[5] = 0.0f;

        glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glBindVertexArray(gVao_graph);
        glDrawArrays(GL_LINES, 0, 2);
        glBindVertexArray(0);

        // verticle down
        vertices[0] = 1.5f;
        vertices[1] = -distanceForX;
        vertices[2] = 0.0f;

        vertices[3] = -1.5f;
        vertices[4] = -distanceForX;
        vertices[5] = 0.0f;

        glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glBindVertexArray(gVao_graph);
        glDrawArrays(GL_LINES, 0, 2);
        glBindVertexArray(0);

        // horizontal right
        vertices[0] = -distanceForY;
        vertices[1] = 1.5;
        vertices[2] = 0.0f;

        vertices[3] = -distanceForY;
        vertices[4] = -1.5f;
        vertices[5] = 0.0f;

        glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glBindVertexArray(gVao_graph);
        glDrawArrays(GL_LINES, 0, 2);
        glBindVertexArray(0);

        // horizontal left
        vertices[0] = distanceForY;
        vertices[1] = 1.5;
        vertices[2] = 0.0f;

        vertices[3] = distanceForY;
        vertices[4] = -1.5f;
        vertices[5] = 0.0f;

        glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glBindVertexArray(gVao_graph);
        glDrawArrays(GL_LINES, 0, 2);
        glBindVertexArray(0);


        distanceForX += (GLfloat)1 / 46;
        distanceForY += (GLfloat)1 / 25;
    }
    graphColor[0] = 0.0f;
    graphColor[1] = 1.0f;
    graphColor[2] = 0.0f;
    graphColor[3] = 0.0f;
    graphColor[4] = 1.0f;
    graphColor[5] = 0.0f;

    glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(graphColor), graphColor, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // horizontal green line
    vertices[0] = 1.5f;
    vertices[1] = 0.0f;
    vertices[2] = 0.0f;

    vertices[3] = -1.5f;
    vertices[4] = 0.0f;
    vertices[5] = 0.0f;

    glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(gVao_graph);
    glDrawArrays(GL_LINES, 0, 2);
    glBindVertexArray(0);

    // verticle green line
    vertices[0] = 0.0f;
    vertices[1] = 1.5f;
    vertices[2] = 0.0f;

    vertices[3] = 0.0f;
    vertices[4] = -1.5f;
    vertices[5] = 0.0f;

    glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(gVao_graph);
    glDrawArrays(GL_LINES, 0, 2);
    glBindVertexArray(0);

}

-(void)startAnimation
{
    //code
    if(isAnimating == NO)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    //code
    if(isAnimating == YES)
    {
        [displayLink invalidate];
        displayLink = nil;
        isAnimating = NO;
    }
}
-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    //code
    
    
}

-(void) onDoubleTap:(UITapGestureRecognizer *)gr
{
    //code
  
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
        //code
    //centralText = @"swipe";
   //[self setNeedsDisplay];
    [self unintialize];
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    //code

    
}

-(void)unintialize
{
    //code
    
    // destroy vao
    if (gVao_graph)
    {
        glDeleteVertexArrays(1, &gVao_graph);
        gVao_graph = 0;
    }

    if (gVbo_graph_color)
    {
        glDeleteBuffers(1, &gVbo_graph_color);
        gVbo_graph_color = 0;
    }

    if (gVbo_graph_position)
    {
        glDeleteBuffers(1, &gVbo_graph_position);
        gVbo_graph_position = 0;
    }

    // deatch vertex shader from shader program object
    glDetachShader(gShaderProgramObject, gVertexShaderObject);

    // detach fragment shader from shader program obejct
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);

    // delete vertex shader object
    glDeleteShader(gVertexShaderObject);

    gVertexShaderObject = 0;

    // delete fragment shader object
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject = 0;

    // delete shader program object
    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject = 0;

    // unlink shader program
    glUseProgram(0);

    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer = 0;
    }
    
    if(colorRenderBuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderBuffer);
        colorRenderBuffer = 0;
    }
    
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer = 0;
    }
    
    if(eaglContext)
    {
        if([EAGLContext currentContext] == eaglContext )
        {
            [EAGLContext setCurrentContext:nil];
            [eaglContext release];
            eaglContext = nil;
        }
    }
}


-(void)dealloc
{
    //code
    [self unintialize];
    [super dealloc];
}

@end
