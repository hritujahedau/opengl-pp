//
//  main.m
//  com.myioswindow
//
//  Created by user166446 on 7/3/21.
//  Copyright © 2021 user166446. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    // code
    NSString *appDelegateClassName;
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

    appDelegateClassName = NSStringFromClass([AppDelegate class]);
    int ret = UIApplicationMain(argc, argv, nil, appDelegateClassName);
    
    [pool release];
    return (ret);
}
