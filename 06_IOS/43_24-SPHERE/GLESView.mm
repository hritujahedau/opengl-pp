//
//  MyView.m
//  com.myioswindow
//
//  Created by user166446 on 7/3/21.
//  Copyright © 2021 user166446. All rights reserved.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"
#include "Sphere.h"

using namespace vmath;

enum
{
    HRH_ATTRIBUTE_POSITION = 0 ,
    HRH_ATTRIBUTE_COLOR ,
    HRH_ATTRIBUTE_NORMAL ,
    HRH_ATTRIBUTE_TEXTURE_0
};

const float RADIAN_VALUE = 3.14159f/180.0f;

int isLightPerVertexShader = 0, isLightPerFragmentShader = 0, isLightOn = 0;

int countForPerVertex = 0, countForPerFragment = 0;

GLfloat lightPosition[] = { 0.0f, 0.0f, 0.0f, 1.0f }; 
GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f }; 
GLfloat lightDefuse[] = { 1.0f, 1.0f, 1.0f  } ; 
GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f }; 

GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f };
GLfloat materialDefuse[] = { 1.0f, 1.0f, 1.0f  }; 
GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0f};
GLfloat materialShinyness = 128.0f;

int key_x = 0, key_y = 0, key_z = 0;
GLfloat angle_for_light = 0.0f;
GLfloat sin_angle = 0.0f, cos_angle = 0.0f, radius = 10;

// first column
// 1
GLfloat materialAmbient_emerald[] = { 0.0215f, 0.1745f, 0.0215f, 1.0f };
GLfloat materialDefuse_emerald[] = { 0.07568f, 0.61424f, 0.07568f, 1.0f };
GLfloat materialSpecular_emerald[] = { 0.633f, 0.727811f, 0.633f, 1.0f };
GLfloat materialShinyness_emerald = 0.6 * 128;

// 2
GLfloat materialAmbient_jade[] = { 0.135f, 0.2225f, 0.1575f, 1.0f };
GLfloat materialDefuse_jade[] = { 0.54f, 0.89f, 0.63f, 1.0f };
GLfloat materialSpecular_jade[] = { 0.316228f, 0.316228f, 0.316228f, 1.0f };
GLfloat materialShinyness_jade = 0.1 * 128;

// 3
GLfloat materialAmbient_obsidian[] = { 0.05375f, 0.05f, 0.06625f, 1.0f };
GLfloat materialDefuse_obsidian[] = { 0.18275f, 0.17f, 0.22525f, 1.0f };
GLfloat materialSpecular_obsidian[] = { 0.332741f, 0.328634f, 0.346435f, 1.0f };
GLfloat materialShinyness_obsidian = 0.3 * 128;

// 4 pearl
GLfloat materialAmbient_pearl[] = { 0.25f, 0.20725f, 0.20725f, 1.0f };
GLfloat materialDefuse_pearl[] = { 1.0f, 0.829f, 0.829f, 1.0f };
GLfloat materialSpecular_pearl[] = { 0.296648f, 0.296648f, 0.296648f, 1.0f };
GLfloat materialShinyness_pearl = 0.088 * 128;

// 5 ruby 
GLfloat materialAmbient_ruby[] = { 0.1745f, 0.01175f, 0.01175f, 1.0f };
GLfloat materialDefuse_ruby[] = { 0.61424f, 0.04136f, 0.04136f, 1.0f };
GLfloat materialSpecular_ruby[] = { 0.727811f, 0.626959f, 0.626959f, 1.0f };
GLfloat materialShinyness_ruby = 0.6 * 128;

// 6 turquoise
GLfloat materialAmbient_turquoise[] = { 0.1f, 0.18725f, 0.1745f, 1.0f };
GLfloat materialDefuse_turquoise[] = { 0.396f, 0.74151f, 0.69102f, 1.0f };
GLfloat materialSpecular_turquoise[] = { 0.297254f, 0.30829f, 0.306678f, 1.0f };
GLfloat materialShinyness_turquoise = 0.1 * 128;

// second column
// 7 brass 
GLfloat materialAmbient_brass[] = { 0.329412f, 0.223529f, 0.027451f, 1.0f };
GLfloat materialDefuse_brass[] = { 0.780392f, 0.568627f, 0.113725f, 1.0f };
GLfloat materialSpecular_brass[] = { 0.992157f, 0.941176f, 0.807843f, 1.0f };
GLfloat materialShinyness_brass = 0.21794872 * 128;

// 8 bronze
GLfloat materialAmbient_bronze[] = { 0.2125f, 0.1275f, 0.054f, 1.0f };
GLfloat materialDefuse_bronze[] = { 0.714f, 0.4284f, 0.18144f, 1.0f };
GLfloat materialSpecular_bronze[] = { 0.393548f, 0.271906f, 0.166721f, 1.0f };
GLfloat materialShinyness_bronze = 0.2 * 128;

// 9 chrome
GLfloat materialAmbient_chrome[] = { 0.25f, 0.25f, 0.25f, 1.0f };
GLfloat materialDefuse_chrome[] = { 0.4f, 0.4f, 0.4f, 1.0f };
GLfloat materialSpecular_chrome[] = { 0.774597f, 0.774597f, 0.774597f, 1.0f };
GLfloat materialShinyness_chrome = 0.2 * 128;

// 10 4th sphere on 2nd column, copper  
GLfloat materialAmbient_copper[] = { 0.19125f, 0.19125f, 0.0225f, 1.0f };
GLfloat materialDefuse_copper[] = { 0.7038f, 0.27048f, 0.0828f, 1.0f };
GLfloat materialSpecular_copper[] = { 0.256777f, 0.137622f, 0.086014f, 1.0f };
GLfloat materialShinyness_copper = 0.1 * 128;

// 11 5th sphere on 2nd column, gold
GLfloat materialAmbient_gold[] = { 0.24725f, 0.1995f, 0.0745f, 1.0f };
GLfloat materialDefuse_gold[] = { 0.75164f, 0.60648f, 0.22648f, 1.0f };
GLfloat materialSpecular_gold[] = { 0.628281f, 0.555802f, 0.366065f, 1.0f };
GLfloat materialShinyness_gold = 0.4 * 128;

// 12 6th sphere on 2nd column, silver
GLfloat materialAmbient_silver[] = { 0.19225, 0.1995f, 0.19225f, 1.0f };
GLfloat materialDefuse_silver[] = { 0.50754f, 0.50754f, 0.50754f, 1.0f };
GLfloat materialSpecular_silver[] = { 0.508273f, 0.508273f, 0.508273f, 1.0f };
GLfloat materialShinyness_silver = 0.4 * 128;

// 13 1st sphere on 3rd column, black 
GLfloat materialAmbient_black[] = { 0.0, 0.0f, 0.0f, 1.0f };
GLfloat materialDefuse_black[] = { 0.01f, 0.01f, 0.01f, 1.0f };
GLfloat materialSpecular_black[] = { 0.5f, 0.5f, 0.5f, 1.0f };
GLfloat materialShinyness_black = 0.25 * 128;

// 14 2nd sphere on 3rd column, cyan
GLfloat materialAmbient_cyan[] = { 0.0, 0.1f, 0.06f, 1.0f };
GLfloat materialDefuse_cyan[] = { 0.0f, 0.50980392f, 0.50980392f, 1.0f };
GLfloat materialSpecular_cyan[] = { 0.50196078f, 0.50196078f, 0.50196078f, 1.0f };
GLfloat materialShinyness_cyan = 0.25 * 128;

// 15 3rd sphere on 2nd column, green
GLfloat materialAmbient_green[] = { 0.0, 0.0f, 0.0f, 1.0f };
GLfloat materialDefuse_green[] = { 0.1f, 0.35f, 0.1f, 1.0f };
GLfloat materialSpecular_green[] = { 0.45f, 0.55f, 0.45f, 1.0f };
GLfloat materialShinyness_green = 0.25 * 128;

// 16 4th sphere on 3rd column, red
GLfloat materialAmbient_red[] = { 0.0, 0.0f, 0.0f, 1.0f };
GLfloat materialDefuse_red[] = { 0.5, 0.0f, 0.0f, 1.0f };
GLfloat materialSpecular_red[] = { 0.7, 0.6f, 0.6f, 1.0f };
GLfloat materialShinyness_red = 0.25 * 128;

// 17 5th sphere on 3rd column, white
GLfloat materialAmbient_white[] = { 0.0, 0.0f, 0.0f, 1.0f };
GLfloat materialDefuse_white[] = { 0.55, 0.55f, 0.55f, 1.0f };
GLfloat materialSpecular_white[] = { 0.7, 0.7f, 0.7f, 1.0f };
GLfloat materialShinyness_white = 0.25 * 128;

// 18 6th sphere on 3rd column, yellow plastic
GLfloat materialAmbient_plastic[] = { 0.0, 0.0f, 0.0f, 1.0f };
GLfloat materialDefuse_plastic[] = { 0.5, 0.5f, 0.0f, 1.0f };
GLfloat materialSpecular_plastic[] = { 0.60, 0.60f, 0.50f, 1.0f };
GLfloat materialShinyness_plastic = 0.25 * 128;

// 19  1st sphere on 4th column, black
GLfloat materialAmbient_black_2[] = { 0.02, 0.02f, 0.02f, 1.0f };
GLfloat materialDefuse_black_2[] = { 0.01, 0.01f, 0.01f, 1.0f };
GLfloat materialSpecular_black_2[] = { 0.4f, 0.4f, 0.4f, 1.0f };
GLfloat materialShinyness_black_2 = 0.078125 * 128;

// 20  2nd sphere on 4th column, cyan
GLfloat materialAmbient_cyan_2[] = { 0.0, 0.05f, 0.05f, 1.0f };
GLfloat materialDefuse_cyan_2[] = { 0.4, 0.5f, 0.5f, 1.0f };
GLfloat materialSpecular_cyan_2[] = { 0.04f, 0.7f, 0.7f, 1.0f };
GLfloat materialShinyness_cyan_2 = 0.078125 * 128;

// 21  3rd sphere on 4th column, green
GLfloat materialAmbient_green_2[] = { 0.0, 0.05f, 0.05f, 1.0f };
GLfloat materialDefuse_green_2[] = { 0.4, 0.5f, 0.4f, 1.0f };
GLfloat materialSpecular_green_2[] = { 0.04f, 0.7f, 0.04f, 1.0f };
GLfloat materialShinyness_green_2 = 0.078125 * 128;

// 22   4th sphere on 4th column, red 
GLfloat materialAmbient_red_2[] = { 0.05, 0.0f, 0.0f, 1.0f };
GLfloat materialDefuse_red_2[] = { 0.5, 0.4f, 0.4f, 1.0f };
GLfloat materialSpecular_red_2[] = { 0.7f, 0.04f, 0.04f, 1.0f };
GLfloat materialShinyness_red_2 = 0.078125 * 128;

// 23   5th sphere on 4th column, white
GLfloat materialAmbient_white_2[] = { 0.05, 0.05f, 0.05f, 1.0f };
GLfloat materialDefuse_white_2[] = { 0.5, 0.5f, 0.5f, 1.0f };
GLfloat materialSpecular_white_2[] = { 0.7f, 0.7f, 0.7f, 1.0f };
GLfloat materialShinyness_white_2 = 0.078125 * 128;

// 24   6th sphere on 4th column, yellow rubber
GLfloat materialAmbient_rubber[] = { 0.05, 0.05f, 0.0f, 1.0f };
GLfloat materialDefuse_rubber[] = { 0.5, 0.5f, 0.4f, 1.0f };
GLfloat materialSpecular_rubber[] = { 0.7f, 0.7f, 0.04f, 1.0f };
GLfloat materialShinyness_rubber = 0.078125 * 128;


GLfloat translate_x = 0.0f, translate_y = 2.0f, translate_z = 0.0f, diiference_y = 0.7;


@implementation GLESView
{
    @private
    EAGLContext *eaglContext;
    
    GLuint defaultFrameBuffer;
    GLuint colorRenderBuffer;
    GLuint depthRenderBuffer;
    
    //CADisplayLink *displayLink;
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint vertexShaderObjectPerFragment;
    GLuint fragmentShaderObjectPerFragment;
    GLuint shaderProgramObjectPerFragment;
    
    GLuint vertexShaderObjectPerVertex;
    GLuint fragmentShaderObjectPerVertex;
    GLuint shaderProgramObjectPerVertex;
    
    GLuint currentShaderProgram;
    
    GLuint vao_sphere;
    GLuint vbo_sphere_position;
    GLuint vbo_sphere_normal;
    GLuint vbo_sphere_element;
    
    GLuint modelUniform;
    GLuint viewUniform;
    GLuint perspectiveUniform;

    GLuint lightAmbientUniform;
    GLuint lightSpecularUniform;
    GLuint lightDiffusedUniform;
    GLuint lightPossitionUniform;
    
    GLuint materialSpecularUniform;
    GLuint materialAmbientUniform;
    GLuint materialDiffuseUniform;
    GLuint materialShininessUniform;
    
    GLuint isPerVertexShaderUniform;
    GLuint isPerFragmentShaderUniform;

    GLfloat sphere_vertices[1146];
    GLfloat Sphere_normals[1146];
    GLfloat Sphere_textrure[764];
    unsigned short sphere_elements[2280];
    
    int gNumVertices;
    int gNumElements;

    mat4 perspectiveProjectionMatrix;    
}

+(Class)layerClass
{
    //code
    return ([CAEAGLLayer class]);
}

-(id)initWithFrame:(CGRect)frame
{
    //code
    self = [super initWithFrame:frame];
    if(self)
    {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)[super layer];
        [eaglLayer setOpaque:YES];
        [eaglLayer setDrawableProperties: [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil]
         ];
        
        eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            printf("OpenGL-ES Context creation failed\n");
            return nil;
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFrameBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
        glGenRenderbuffers(1, &colorRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Framebuffer is not complete\n");
            [self unintialize];
            return nil;
        }
        
        animationFrameInterval = 60;
        isAnimating = NO;

        if( [self loadShaders] == NO)
        {
           [self unintialize];
           return nil;
        }
        [self loadObjects];
        
        glClearColor(0.20f, 0.20f, 0.20f, 1.0f);
        
        //glShadeModel(GL_SMOOTH);
        //glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        //glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

        // we will always cull face for better performance
        glEnable(GL_CULL_FACE);
        perspectiveProjectionMatrix = mat4::identity();
        
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [swipeGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        UILongPressGestureRecognizer *longPressesGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [longPressesGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:longPressesGestureRecognizer];
        
    }
    return self;
}

-(BOOL)loadShaders
{
    vertexShaderObjectPerVertex = glCreateShader(GL_VERTEX_SHADER);
    
    //provide souce code to shader
    const GLchar* vertexShaderSourceCodePerVertex =
                                            " #version 300 es\n"\
                                            "in vec4 v_position;" \
                                            "in vec3 v_normals; " \
                                            "uniform mat4 u_modelMatrix;" \
                                            "uniform mat4 u_viewMatrix;" \
                                            "uniform mat4 u_projectionMatrix;" \
                                            "uniform int u_isPerVertexLightOn;" \
                                            "uniform vec3 u_lightAmbient;" \
                                            "uniform vec3 u_lightSpecular;" \
                                            "uniform vec3 u_lightDiffuse;" \
                                            "uniform vec4 u_light_position; " \
                                            "uniform vec3 u_materialAmbient; " \
                                            "uniform vec3 u_materialSpecular; "\
                                            "uniform vec3 u_materialDiffuse; "\
                                            "uniform float u_materialshininess; "\
                                            "out vec3 phoung_ads_lighting;" \
                                            "void main()" \
                                            "{" \
                                            "if (u_isPerVertexLightOn == 1) " \
                                            "{" \
                                            "   vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" \
                                            "   vec3 transformed_normal = normalize( mat3( u_viewMatrix * u_modelMatrix ) * v_normals); " \
                                            "   vec3 light_direction = normalize( vec3 (u_light_position - eye_coordinates) ); " \
                                            "   vec3 reflection_vector = reflect (-light_direction, transformed_normal); " \
                                            "   vec3 view_vector = normalize ( vec3 ( -eye_coordinates)); " \
                                            "   vec3 ambient = u_lightAmbient * u_materialAmbient; " \
                                            "   vec3 diffuse_light = u_lightDiffuse * u_materialDiffuse * max ( dot (light_direction , transformed_normal), 0.0); "\
                                            "   vec3 specular = u_lightSpecular * u_materialSpecular * pow( max ( dot ( reflection_vector , view_vector), 0.0) , u_materialshininess);" \
                                            "   phoung_ads_lighting = ambient + diffuse_light + specular;" \
                                            "}" \
                                            "else " \
                                            "{ " \
                                            "phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" \
                                            "}" \
                                            "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" \
                                            "}";

    glShaderSource(vertexShaderObjectPerVertex, 1, (const GLchar**)&vertexShaderSourceCodePerVertex, NULL);

    // compile shader
    glCompileShader(vertexShaderObjectPerVertex);

    // check compilation error
    GLint iInfoLogLength = 0;
    GLint iShaderCompileStatus = 0;
    char* szInfoLog = NULL;
    glGetShaderiv(vertexShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
                printf("Vertex shader compilation error : %s \n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    /***************** FRAGMENT SHADER **********************/

    // CREATE SHADER
    fragmentShaderObjectPerVertex = glCreateShader(GL_FRAGMENT_SHADER);

    //PROVIDE SOURCE CODE

    const char* fragmentShaderSourceCodePerVertex =
                                            " #version 300 es\n"\
                                            " precision highp float;" \
                                            " in vec3 phoung_ads_lighting;"\
                                            " out vec4 FragColor; " \
                                            " void main()" \
                                            " {"\
                                            " FragColor = vec4(phoung_ads_lighting, 1.0); " \
                                            " }";
    
    glShaderSource(fragmentShaderObjectPerVertex, 1, (const char**)&fragmentShaderSourceCodePerVertex, NULL);

    // COMPILE SHADER
    glCompileShader(fragmentShaderObjectPerVertex);

    // compilation error checking

    glGetShaderiv(fragmentShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
                printf("Fragment Shader Compilation Error : %s\n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    // SHADER PROGRAM
    //CREATE SHADER PROGRAM

    shaderProgramObjectPerVertex = glCreateProgram();

    // attach vertex shader to shader program
    glAttachShader(shaderProgramObjectPerVertex, vertexShaderObjectPerVertex);

    // attch fragment shader to shader program
    glAttachShader(shaderProgramObjectPerVertex, fragmentShaderObjectPerVertex);

    // pri-link binding of shader object with vertex shader possition attribute
    glBindAttribLocation(shaderProgramObjectPerVertex, HRH_ATTRIBUTE_POSITION, "v_position");
    glBindAttribLocation(shaderProgramObjectPerVertex, HRH_ATTRIBUTE_NORMAL, "v_normals");

    // LINK PROGRAM
    glLinkProgram(shaderProgramObjectPerVertex);

    // CHECK LINKING ERROR
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(shaderProgramObjectPerVertex, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(shaderProgramObjectPerVertex, iInfoLogLength, &written, szInfoLog);
                printf("Shader Program Link Log : %s", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }
    
    /*************************************************LIGHT PER FRGAMENT********************************************************/

    vertexShaderObjectPerFragment = glCreateShader(GL_VERTEX_SHADER);
    
    //provide souce code to shader
    const GLchar* vertexShaderSourceCodePerFragment =
                                            " #version 300 es\n"\
                                            "in vec4 v_position;" \
                                            "in vec3 v_normals; " \
                                            "uniform mat4 u_modelMatrix;" \
                                            "uniform mat4 u_viewMatrix;" \
                                            "uniform mat4 u_projectionMatrix;" \
                                            "uniform int u_isPerFragmentLightOn;" \
                                            "uniform vec4 u_light_position; " \
                                            "out vec3 light_direction; "\
                                            "out vec3 transformed_normal;"\
                                            "out vec3 view_vector;"\
                                            "void main()" \
                                            "{" \
                                            "if (u_isPerFragmentLightOn == 1) " \
                                            "{" \
                                            "   vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" \
                                            "   transformed_normal =  mat3( u_viewMatrix * u_modelMatrix ) * v_normals;" \
                                            "   light_direction = vec3 (u_light_position - eye_coordinates);" \
                                            "   view_vector =  vec3 ( -eye_coordinates);" \
                                            "}" \
                                            "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" \
                                            "}";


    glShaderSource(vertexShaderObjectPerFragment, 1, (const GLchar**)&vertexShaderSourceCodePerFragment, NULL);

    // compile shader
    glCompileShader(vertexShaderObjectPerFragment);

    // check compilation error
    iInfoLogLength = 0;
    iShaderCompileStatus = 0;
    szInfoLog = NULL;
    glGetShaderiv(vertexShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
                printf("Vertex shader compilation error : %s \n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    /***************** FRAGMENT SHADER **********************/

    // CREATE SHADER
    fragmentShaderObjectPerFragment = glCreateShader(GL_FRAGMENT_SHADER);

    //PROVIDE SOURCE CODE

    const char* fragmentShaderSourceCodePerFragment =
                                            " #version 300 es\n"\
                                            " precision highp float;" \
                                            "vec3 phoung_ads_lighting;" \
                                            "out vec4 FragColor; " \
                                            "in vec3 light_direction; "\
                                            "in vec3 transformed_normal;"\
                                            "in vec3 view_vector;"\
                                            "vec3 normalized_light_direction; "\
                                            "vec3 normalized_transformed_normal;"\
                                            "vec3 normalized_view_vector;"\
                                            "uniform highp vec3 u_lightAmbient;" \
                                            "uniform highp vec3 u_lightSpecular;" \
                                            "uniform highp vec3 u_lightDiffuse;" \
                                            "uniform highp vec3 u_materialAmbient; " \
                                            "uniform highp vec3 u_materialSpecular; "\
                                            "uniform highp vec3 u_materialDiffuse; "\
                                            "uniform highp float u_materialshininess; "\
                                            "uniform highp int u_isPerFragmentLightOn;" \
                                            "void main()" \
                                            "{"\
                                            "if (u_isPerFragmentLightOn == 1) " \
                                            "{" \
                                            "   normalized_light_direction = normalize(light_direction);"\
                                            "   normalized_transformed_normal = normalize(transformed_normal);"\
                                            "   normalized_view_vector = normalize(view_vector);"\
                                            "   vec3 reflection_vector = reflect (-normalized_light_direction, normalized_transformed_normal); " \
                                            "   vec3 ambient = u_lightAmbient * u_materialAmbient; " \
                                            "   vec3 diffuse_light = u_lightDiffuse * u_materialDiffuse * max ( dot (normalized_light_direction , normalized_transformed_normal), 0.0); "\
                                            "   vec3 specular = u_lightSpecular * u_materialSpecular * pow( max ( dot ( reflection_vector , normalized_view_vector), 0.0) , u_materialshininess);" \
                                            "   phoung_ads_lighting = ambient + diffuse_light + specular;" \
                                            "}"\
                                            "else " \
                                            "{ " \
                                            "phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" \
                                            "} " \
                                            "FragColor = vec4(phoung_ads_lighting, 1.0); " \
                                            "}";

    
    glShaderSource(fragmentShaderObjectPerFragment, 1, (const char**)&fragmentShaderSourceCodePerFragment, NULL);

    // COMPILE SHADER
    glCompileShader(fragmentShaderObjectPerFragment);

    // compilation error checking

    glGetShaderiv(fragmentShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
                printf("Fragment Shader Compilation Error : %s\n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    // SHADER PROGRAM
    //CREATE SHADER PROGRAM

    shaderProgramObjectPerFragment = glCreateProgram();

    // attach vertex shader to shader program
    glAttachShader(shaderProgramObjectPerFragment, vertexShaderObjectPerFragment);

    // attch fragment shader to shader program
    glAttachShader(shaderProgramObjectPerFragment, fragmentShaderObjectPerFragment);

    // pri-link binding of shader object with vertex shader possition attribute
    glBindAttribLocation(shaderProgramObjectPerFragment, HRH_ATTRIBUTE_POSITION, "v_position");
    glBindAttribLocation(shaderProgramObjectPerFragment, HRH_ATTRIBUTE_NORMAL, "v_normals");

    // LINK PROGRAM
    glLinkProgram(shaderProgramObjectPerFragment);

    // CHECK LINKING ERROR
    iShaderProgramLinkStatus = 0;
    glGetProgramiv(shaderProgramObjectPerFragment, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(shaderProgramObjectPerFragment, iInfoLogLength, &written, szInfoLog);
                printf("Shader Program Link Log : %s", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    return YES;
}

-(void)loadObjects
{
    getSphereVertexData(sphere_vertices, Sphere_normals, Sphere_textrure, sphere_elements);
    gNumVertices = getNumberOfSphereVertices();
    gNumElements = getNumberOfSphereElements();

    glGenVertexArrays(1, &vao_sphere);
    glBindVertexArray(vao_sphere);

    glGenBuffers(1, &vbo_sphere_position);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo

    glGenBuffers(1, &vbo_sphere_normal);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Sphere_normals), Sphere_normals, GL_STATIC_DRAW);
    glVertexAttribPointer(HRH_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(HRH_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // element vbo
    glGenBuffers(1, &vbo_sphere_element);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glBindVertexArray(0); // end for Vao

}

-(void)layoutSubviews
{
    //code
 //     glGenRenderbuffers(1, &colorRenderBuffer);
      glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
      
      [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *) [self layer]];
  //    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
      
      GLint width;
      GLint height;
      
      glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
      glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
      
      glGenRenderbuffers(1, &depthRenderBuffer);
      glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
      glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
      glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
      
      if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
      {
          printf("Framebuffer is not complete\n");
          [self unintialize];
      }
    if(height < 0)
    {
        height = 1;
    }
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    // glORtho(left, right, bottom, top, near, far);
    perspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); 
    
    [self drawView];
}

-(void)drawView
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);

    // glUseProgram
    GLfloat scaleSphere = 0.5f, sphere_translate_x = 0.0f, sphere_translate_y = 2.0f, sphere_translate_z = 0.0f , translate_z = 8.0f, diiference_y = 0.7;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // start from here    
  //  currentShaderProgram = (isLightPerVertexShader == 1) ? shaderProgramObjectPerVertex : shaderProgramObjectPerFragment;

    glUseProgram(currentShaderProgram);

    mat4 temp_pos;

    mat4 viewMatrix = vmath::lookat(
        vmath::vec3(0.0f, 0.0f, translate_z),
        vmath::vec3(0.0f, 0.0f, 0.0f),
        vmath::vec3(0.0f, 1.0f, 0.0f)
    );

    // OpenGL Drawing
    // set modelview & modelviewProjection matrices to indentity
    mat4 translateMatrix = vmath::translate(sphere_translate_x, sphere_translate_y, sphere_translate_z);
    mat4 scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
    mat4 modelMatrix = translateMatrix * scaleMatrix;

    modelUniform = glGetUniformLocation(currentShaderProgram, "u_modelMatrix");
    viewUniform = glGetUniformLocation(currentShaderProgram, "u_viewMatrix");
    perspectiveUniform = glGetUniformLocation(currentShaderProgram, "u_projectionMatrix");
    isPerFragmentShaderUniform = glGetUniformLocation(currentShaderProgram, "u_isPerFragmentLightOn");
    isPerVertexShaderUniform = glGetUniformLocation(currentShaderProgram, "u_isPerVertexLightOn");

    lightAmbientUniform = glGetUniformLocation(currentShaderProgram, "u_lightAmbient");
    lightSpecularUniform = glGetUniformLocation(currentShaderProgram, "u_lightSpecular");
    lightDiffusedUniform = glGetUniformLocation(currentShaderProgram, "u_lightDiffuse");
    lightPossitionUniform = glGetUniformLocation(currentShaderProgram, "u_light_position");

    materialDiffuseUniform = glGetUniformLocation(currentShaderProgram, "u_materialDiffuse");
    materialAmbientUniform = glGetUniformLocation(currentShaderProgram, "u_materialAmbient");
    materialSpecularUniform = glGetUniformLocation(currentShaderProgram, "u_materialSpecular");
    materialShininessUniform = glGetUniformLocation(currentShaderProgram, "u_materialshininess");

    sin_angle = radius * sin(angle_for_light * RADIAN_VALUE);
    cos_angle = radius * cos(angle_for_light * RADIAN_VALUE);

    if (key_x == 1)
    {
        lightPosition[0] = 0.0f;
        lightPosition[1] = sin_angle;
        lightPosition[2] = -translate_z + cos_angle;
        lightPosition[3] = 1.0f;
    } else if (key_y == 1)
    {
        lightPosition[0] = sin_angle;
        lightPosition[1] = 0.0f;
        lightPosition[2] = -translate_z + cos_angle;
        lightPosition[3] = 1.0f;
    } else if (key_z == 1)
    {
        lightPosition[0] = sin_angle;
        lightPosition[1] = cos_angle;
        lightPosition[2] = -translate_z;
        lightPosition[3] = 1.0f;
    }

    if (key_x == 1 || key_y == 1 || key_z == 1)
    {
        if (angle_for_light >= 360.0f)
        {
            angle_for_light = 0.0f;
        }
        angle_for_light = angle_for_light + 0.5f;
    }

    if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
    {
        glUniform4fv(lightPossitionUniform, 1, (const GLfloat*)lightPosition);
        glUniform3fv(lightDiffusedUniform, 1, (const GLfloat*)lightDefuse);
        glUniform3fv(lightAmbientUniform, 1, (const GLfloat*)lightAmbient);
        glUniform3fv(lightSpecularUniform, 1, (const GLfloat*)lightSpecular);
    }
    glUniform1i(isPerFragmentShaderUniform, isLightPerFragmentShader);
    glUniform1i(isPerVertexShaderUniform, isLightPerVertexShader);
    glUniformMatrix4fv(viewUniform, 1, GL_FALSE, (const float*)viewMatrix);
    //glUniformMatrix4fv(perspectiveUniform, 1, GL_FALSE, (const GLfloat*)gOrthographicProjectionMatrix);
    glUniformMatrix4fv(perspectiveUniform, 1, GL_FALSE, (const GLfloat*)perspectiveProjectionMatrix);
    
    /********************************************First Row**************************************************************/
    // first sphere
    translateMatrix = vmath::translate(sphere_translate_x - 1.5f, sphere_translate_y, sphere_translate_z);
    scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
    modelMatrix = translateMatrix * scaleMatrix;

    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);

    if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
    {
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*)materialAmbient_emerald);
        glUniform1f(materialShininessUniform, materialShinyness_emerald);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*)materialSpecular_emerald);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*)materialDefuse_emerald);
    }

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0); 
    // ** unbind vao **
    glBindVertexArray(0);

    // second sphere
    translateMatrix = vmath::translate(sphere_translate_x - 0.5f, sphere_translate_y, sphere_translate_z);
    scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
    modelMatrix = translateMatrix * scaleMatrix;

    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);

    if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
    {
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*)materialAmbient_jade);
        glUniform1f(materialShininessUniform, materialShinyness_jade);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*)materialSpecular_jade);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*)materialDefuse_jade);
    }

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    // ** unbind vao **
    glBindVertexArray(0);

    // third sphere
    translateMatrix = vmath::translate(sphere_translate_x + 0.5f, sphere_translate_y, sphere_translate_z);
    scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
    modelMatrix = translateMatrix * scaleMatrix;

    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);

    if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
    {
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*)materialAmbient_obsidian);
        glUniform1f(materialShininessUniform, materialShinyness_obsidian);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*)materialSpecular_obsidian);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*)materialDefuse_obsidian);
    }

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    // ** unbind vao **
    glBindVertexArray(0);


    // forth sphere
    translateMatrix = vmath::translate(sphere_translate_x + 1.5f, sphere_translate_y, sphere_translate_z);
    scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
    modelMatrix = translateMatrix * scaleMatrix;

    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);

    if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
    {
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*)materialAmbient_pearl);
        glUniform1f(materialShininessUniform, materialShinyness_pearl);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*)materialSpecular_pearl);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*)materialDefuse_pearl);
    }

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    // ** unbind vao **
    glBindVertexArray(0);

    /********************************************Second Row**************************************************************/

    // first sphere
    translateMatrix = vmath::translate(sphere_translate_x - 1.5f, sphere_translate_y - diiference_y, sphere_translate_z);
    scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
    modelMatrix = translateMatrix * scaleMatrix;

    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);

    if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
    {
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*)materialAmbient_ruby);
        glUniform1f(materialShininessUniform, materialShinyness_ruby);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*)materialSpecular_ruby);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*)materialDefuse_ruby);
    }

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    // ** unbind vao **
    glBindVertexArray(0);

    // second sphere
    translateMatrix = vmath::translate(sphere_translate_x - 0.5f, sphere_translate_y - diiference_y, sphere_translate_z);
    scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
    modelMatrix = translateMatrix * scaleMatrix;

    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);

    if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
    {
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*)materialAmbient_turquoise);
        glUniform1f(materialShininessUniform, materialShinyness_turquoise);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*)materialSpecular_turquoise);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*)materialDefuse_turquoise);
    }

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    // ** unbind vao **
    glBindVertexArray(0);

    // third sphere
    translateMatrix = vmath::translate(sphere_translate_x + 0.5f, sphere_translate_y - diiference_y, sphere_translate_z);
    scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
    modelMatrix = translateMatrix * scaleMatrix;

    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);

    if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
    {
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*)materialAmbient_brass);
        glUniform1f(materialShininessUniform, materialShinyness_brass);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*)materialSpecular_brass);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*)materialDefuse_brass);
    }

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    // ** unbind vao **
    glBindVertexArray(0);


    // forth sphere
    translateMatrix = vmath::translate(sphere_translate_x + 1.5f, sphere_translate_y - diiference_y, sphere_translate_z);
    scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
    modelMatrix = translateMatrix * scaleMatrix;

    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);

    if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
    {
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*)materialAmbient_bronze);
        glUniform1f(materialShininessUniform, materialShinyness_bronze);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*)materialSpecular_bronze);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*)materialDefuse_bronze);
    }

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    // ** unbind vao **
    glBindVertexArray(0);


    /********************************************THIRD ROW**************************************************************/
    diiference_y = diiference_y + 0.7f;
    // first sphere
    translateMatrix = vmath::translate(sphere_translate_x - 1.5f, sphere_translate_y - diiference_y, sphere_translate_z);
    scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
    modelMatrix = translateMatrix * scaleMatrix;

    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);

    if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
    {
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*)materialAmbient_chrome);
        glUniform1f(materialShininessUniform, materialShinyness_chrome);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*)materialSpecular_chrome);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*)materialDefuse_chrome);
    }

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    // ** unbind vao **
    glBindVertexArray(0);

    // second sphere
    translateMatrix = vmath::translate(sphere_translate_x - 0.5f, sphere_translate_y - diiference_y, sphere_translate_z);
    scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
    modelMatrix = translateMatrix * scaleMatrix;

    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);

    if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
    {
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*)materialAmbient_copper);
        glUniform1f(materialShininessUniform, materialShinyness_copper);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*)materialSpecular_copper);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*)materialDefuse_copper);
    }

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    // ** unbind vao **
    glBindVertexArray(0);

    // third sphere
    translateMatrix = vmath::translate(sphere_translate_x + 0.5f, sphere_translate_y - diiference_y, sphere_translate_z);
    scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
    modelMatrix = translateMatrix * scaleMatrix;

    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);

    if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
    {
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*)materialAmbient_gold);
        glUniform1f(materialShininessUniform, materialShinyness_gold);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*)materialSpecular_gold);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*)materialDefuse_gold);
    }

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    // ** unbind vao **
    glBindVertexArray(0);


    // forth sphere
    translateMatrix = vmath::translate(sphere_translate_x + 1.5f, sphere_translate_y - diiference_y, sphere_translate_z);
    scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
    modelMatrix = translateMatrix * scaleMatrix;

    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);

    if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
    {
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*)materialAmbient_silver);
        glUniform1f(materialShininessUniform, materialShinyness_silver);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*)materialSpecular_silver);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*)materialDefuse_silver);
    }

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    // ** unbind vao **
    glBindVertexArray(0);

    /********************************************FORTH ROW**************************************************************/
    diiference_y = diiference_y + 0.7f;
    // first sphere
    translateMatrix = vmath::translate(sphere_translate_x - 1.5f, sphere_translate_y - diiference_y, sphere_translate_z);
    scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
    modelMatrix = translateMatrix * scaleMatrix;

    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);

    if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
    {
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*)materialAmbient_black);
        glUniform1f(materialShininessUniform, materialShinyness_black);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*)materialSpecular_black);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*)materialDefuse_black);
    }

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    // ** unbind vao **
    glBindVertexArray(0);

    // second sphere
    translateMatrix = vmath::translate(sphere_translate_x - 0.5f, sphere_translate_y - diiference_y, sphere_translate_z);
    scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
    modelMatrix = translateMatrix * scaleMatrix;

    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);

    if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
    {
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*)materialAmbient_cyan);
        glUniform1f(materialShininessUniform, materialShinyness_cyan);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*)materialSpecular_cyan);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*)materialDefuse_cyan);
    }

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    // ** unbind vao **
    glBindVertexArray(0);

    // third sphere
    translateMatrix = vmath::translate(sphere_translate_x + 0.5f, sphere_translate_y - diiference_y, sphere_translate_z);
    scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
    modelMatrix = translateMatrix * scaleMatrix;

    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);

    if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
    {
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*)materialAmbient_green);
        glUniform1f(materialShininessUniform, materialShinyness_green);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*)materialSpecular_green);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*)materialDefuse_green);
    }

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    // ** unbind vao **
    glBindVertexArray(0);


    // forth sphere
    translateMatrix = vmath::translate(sphere_translate_x + 1.5f, sphere_translate_y - diiference_y, sphere_translate_z);
    scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
    modelMatrix = translateMatrix * scaleMatrix;

    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);

    if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
    {
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*)materialAmbient_red);
        glUniform1f(materialShininessUniform, materialShinyness_red);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*)materialSpecular_red);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*)materialDefuse_red);
    }

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    // ** unbind vao **
    glBindVertexArray(0);


    /********************************************FIFTH ROW**************************************************************/
    diiference_y = diiference_y + 0.7f;
    // first sphere
    translateMatrix = vmath::translate(sphere_translate_x - 1.5f, sphere_translate_y - diiference_y, sphere_translate_z);
    scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
    modelMatrix = translateMatrix * scaleMatrix;

    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);

    if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
    {
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*)materialAmbient_white);
        glUniform1f(materialShininessUniform, materialShinyness_white);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*)materialSpecular_white);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*)materialDefuse_white);
    }

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    // ** unbind vao **
    glBindVertexArray(0);

    // second sphere
    translateMatrix = vmath::translate(sphere_translate_x - 0.5f, sphere_translate_y - diiference_y, sphere_translate_z);
    scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
    modelMatrix = translateMatrix * scaleMatrix;

    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);

    if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
    {
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*)materialAmbient_plastic);
        glUniform1f(materialShininessUniform, materialShinyness_plastic);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*)materialSpecular_plastic);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*)materialDefuse_plastic);
    }

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    // ** unbind vao **
    glBindVertexArray(0);

    // third sphere
    translateMatrix = vmath::translate(sphere_translate_x + 0.5f, sphere_translate_y - diiference_y, sphere_translate_z);
    scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
    modelMatrix = translateMatrix * scaleMatrix;

    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);

    if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
    {
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*)materialAmbient_black_2);
        glUniform1f(materialShininessUniform, materialShinyness_black_2);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*)materialSpecular_black_2);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*)materialDefuse_black_2);
    }

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    // ** unbind vao **
    glBindVertexArray(0);


    // forth sphere
    translateMatrix = vmath::translate(sphere_translate_x + 1.5f, sphere_translate_y - diiference_y, sphere_translate_z);
    scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
    modelMatrix = translateMatrix * scaleMatrix;

    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);

    if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
    {
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*)materialAmbient_cyan_2);
        glUniform1f(materialShininessUniform, materialShinyness_cyan_2);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*)materialSpecular_cyan_2);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*)materialDefuse_cyan_2);
    }

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    // ** unbind vao **
    glBindVertexArray(0);

    /********************************************SIXTH ROW**************************************************************/
    diiference_y = diiference_y + 0.7f;
    // first sphere
    translateMatrix = vmath::translate(sphere_translate_x - 1.5f, sphere_translate_y - diiference_y, sphere_translate_z);
    scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
    modelMatrix = translateMatrix * scaleMatrix;

    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);

    if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
    {
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*)materialAmbient_green_2);
        glUniform1f(materialShininessUniform, materialShinyness_green_2);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*)materialSpecular_green_2);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*)materialDefuse_green_2);
    }

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    // ** unbind vao **
    glBindVertexArray(0);

    // second sphere
    translateMatrix = vmath::translate(sphere_translate_x - 0.5f, sphere_translate_y - diiference_y, sphere_translate_z);
    scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
    modelMatrix = translateMatrix * scaleMatrix;

    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);

    if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
    {
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*)materialAmbient_red_2);
        glUniform1f(materialShininessUniform, materialShinyness_red_2);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*)materialSpecular_red_2);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*)materialDefuse_red_2);
    }

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    // ** unbind vao **
    glBindVertexArray(0);

    // third sphere
    translateMatrix = vmath::translate(sphere_translate_x + 0.5f, sphere_translate_y - diiference_y, sphere_translate_z);
    scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
    modelMatrix = translateMatrix * scaleMatrix;

    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);

    if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
    {
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*)materialAmbient_white_2);
        glUniform1f(materialShininessUniform, materialShinyness_white_2);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*)materialSpecular_white_2);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*)materialDefuse_white_2);
    }

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    // ** unbind vao **
    glBindVertexArray(0);


    // forth sphere
    translateMatrix = vmath::translate(sphere_translate_x + 1.5f, sphere_translate_y - diiference_y, sphere_translate_z);
    scaleMatrix = vmath::scale(scaleSphere, scaleSphere, scaleSphere);
    modelMatrix = translateMatrix * scaleMatrix;

    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);

    if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
    {
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*)materialAmbient_rubber);
        glUniform1f(materialShininessUniform, materialShinyness_rubber);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*)materialSpecular_rubber);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*)materialDefuse_rubber);
    }

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    // ** unbind vao **
    glBindVertexArray(0);

    // stop using OpenGL program object
    glUseProgram(0);


    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
}

-(void)startAnimation
{
    //code
    if(isAnimating == NO)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    //code
    if(isAnimating == YES)
    {
        [displayLink invalidate];
        displayLink = nil;
        isAnimating = NO;
    }
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    //code
    currentShaderProgram = shaderProgramObjectPerVertex;
    isLightPerFragmentShader = 0;
    isLightPerVertexShader = 1;
    if(countForPerVertex == 0)
    {
        key_x = 1;
        key_y = 0;
        key_z = 0;
    } else if(countForPerVertex == 1)
    {
        key_x = 0;
        key_y = 1;
        key_z = 0;
    } else if(countForPerVertex == 2)
    {
        key_x = 0;
        key_y = 0;
        key_z = 1;
    }
    countForPerVertex = countForPerVertex + 1;
    if(countForPerVertex > 2)
    {
        countForPerVertex = 0;
    }
    
}

-(void) onDoubleTap:(UITapGestureRecognizer *)gr
{
    //code
    currentShaderProgram = shaderProgramObjectPerFragment;
    isLightPerFragmentShader = 1;
    isLightPerVertexShader = 0;
    if(countForPerFragment == 0)
    {
        key_x = 1;
        key_y = 0;
        key_z = 0;
    } else if(countForPerFragment == 1)
    {
        key_x = 0;
        key_y = 1;
        key_z = 0;
    } else if(countForPerFragment == 2)
    {
        key_x = 0;
        key_y = 0;
        key_z = 1;
    }
    countForPerFragment = countForPerFragment + 1;
    if(countForPerFragment > 2)
    {
        countForPerFragment = 0;
    }
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
        //code
    //centralText = @"swipe";
   //[self setNeedsDisplay];
    [self unintialize];
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    //code
    if (isLightOn == 1)
    {
        isLightOn = 0;
        isLightPerFragmentShader = 0;
    	isLightPerVertexShader = 0;
        angle_for_light = 0.0f;
    }
    else
    {
        key_x = 0;
        key_y = 0;
        key_z = 0;
        isLightOn = 1;
    }
}

-(void)unintialize
{
    //code
    if (vao_sphere)
    {
        glDeleteVertexArrays(1, &vao_sphere);
        vao_sphere = 0;
    }

    // destroy vbo
    if (vbo_sphere_position)
    {
        glDeleteBuffers(1, &vbo_sphere_position);
        vbo_sphere_position = 0;
    }

    if (vbo_sphere_element)
    {
        glDeleteBuffers(1, &vbo_sphere_element);
        vbo_sphere_element = 0;
    }

    if (vbo_sphere_normal)
    {
        glDeleteBuffers(1, &vbo_sphere_normal);
        vbo_sphere_normal = 0;
    }

    // deatch vertex shader from shader program object
    glDetachShader(shaderProgramObjectPerFragment, vertexShaderObjectPerFragment);

    // detach fragment shader from shader program obejct
    glDetachShader(shaderProgramObjectPerFragment, fragmentShaderObjectPerFragment);

    // delete vertex shader object
    glDeleteShader(vertexShaderObjectPerFragment);

    vertexShaderObjectPerFragment = 0; 

    // delete fragment shader object
    glDeleteShader(fragmentShaderObjectPerFragment);
    fragmentShaderObjectPerFragment = 0;

    // delete shader program object
    glDeleteProgram(shaderProgramObjectPerFragment);
    shaderProgramObjectPerFragment = 0;


    glDetachShader(shaderProgramObjectPerVertex, vertexShaderObjectPerVertex);
    glDetachShader(shaderProgramObjectPerVertex, fragmentShaderObjectPerVertex);

    glDeleteShader(vertexShaderObjectPerVertex);
    vertexShaderObjectPerVertex = 0;

    glDeleteShader(fragmentShaderObjectPerVertex);
    fragmentShaderObjectPerVertex = 0;

    // unlink shader program
    glUseProgram(0);

    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer = 0;
    }
    
    if(colorRenderBuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderBuffer);
        colorRenderBuffer = 0;
    }
    
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer = 0;
    }
    
    if(eaglContext)
    {
        if([EAGLContext currentContext] == eaglContext )
        {
            [EAGLContext setCurrentContext:nil];
            [eaglContext release];
            eaglContext = nil;
        }
    }
}


-(void)dealloc
{
    //code
    [self unintialize];
    [super dealloc];
}

@end
