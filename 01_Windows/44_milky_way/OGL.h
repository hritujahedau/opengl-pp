#pragma once
#include <windows.h>
#include <gl\GL.h>
#include "vmath.h"

#define MYICON 1287
#define IDI_MYICON MAKEINTRESOURCE(MYICON)

#define MOON_BITMAP 1300
#define EARTH_BITMAP 1301
#define SUN_BITMAP 1302
#define MILKY_WAY_BITMAP 1303


#define ID_MOON MAKEINTRESOURCE(MOON_BITMAP)
#define ID_SUN MAKEINTRESOURCE(SUN_BITMAP)
#define ID_EARTH MAKEINTRESOURCE(EARTH_BITMAP)
#define ID_MILKY_WAY MAKEINTRESOURCE(MILKY_WAY_BITMAP)


typedef struct node
{
	vmath::mat4 matrix;
	struct node* prev;
} node;

node* head = NULL;