// header file declaration
#include <windows.h>
#include<stdio.h>

#include <GL\glew.h>
#include <gl/GL.h>
#include "OGL.h"
#include "vmath.h"

#pragma comment(lib, "C:\\glew-2.1.0\\lib\\Release\\Win32\\glew32.lib")
#pragma comment(lib, "opengl32.lib")

#define WIDTH 1000
#define HIGHT 800

using namespace vmath;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;

int gMeshWidth = 64, gMeshHeight = 64;
int my_array_size = gMeshWidth * gMeshHeight * 4;
float *gPosition;
float animation_time = 0.0f;

float color_array[3] = { 1.0f, 0.5f, 0.0f};

bool isBkBlack = true;

enum
{
	HRH_ATTRIBUTE_POSITION = 0 ,
	HRH_ATTRIBUTE_COLOR ,
	HRH_ATTRIBUTE_NORMAL ,
	HRH_ATTRIBUTE_TEXTURE_0
};

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao;
GLuint gVbo_position;
GLuint gVbo_color;
GLuint gMVPUniform;

mat4 gPerspectiveProjectionMatrix;

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) 
{
	// function declaration
	void Initialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("h_ogl");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 

	if (fopen_s(&gpFile, "windows.log", "w") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("OGL"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HIGHT/2 ,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			Display();
			if (gbActiveWindow) {
				
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	void Resize(int,int);

	HDC hdc;
	HBRUSH hBrush;
	PAINTSTRUCT ps;
	RECT rc;

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_CHAR:
			switch (wParam)
			{
			case 'i':
			case 'I':
				gMeshHeight = gMeshHeight * 2;
				gMeshWidth = gMeshWidth * 2;
				my_array_size = gMeshWidth * gMeshHeight * 4;
				gPosition = (float*)realloc(gPosition, my_array_size * sizeof(float));
				break;
			case 'd':
			case 'D':
				if (gMeshWidth > 64)
				{
					gMeshHeight = gMeshHeight / 2;
					gMeshWidth = gMeshWidth / 2;
					my_array_size = gMeshWidth * gMeshHeight * 4;
					gPosition = (float*)realloc(gPosition, my_array_size * sizeof(float));
				}
				break;
			case 'r':
			case 'R':
				color_array[0] = 1.0f;
				color_array[1] = 0.0f;
				color_array[2] = 0.0f;
				if (isBkBlack == false)
				{
					isBkBlack = true;
				}
				break;
			case 'g':
			case 'G':
				color_array[0] = 0.0f;
				color_array[1] = 1.0f;
				color_array[2] = 0.0f;
				if (isBkBlack == false)
				{
					isBkBlack = true;
				}
				break;
			case 'b':
			case 'B':
				color_array[0] = 0.0f;
				color_array[1] = 0.0f;
				color_array[2] = 1.0f;
				if (isBkBlack == false)
				{
					isBkBlack = true;
				}
				break;
			case 'c':
			case 'C':
				color_array[0] = 0.0f;
				color_array[1] = 1.0f;
				color_array[2] = 1.0f;
				if (isBkBlack == false)
				{
					isBkBlack = true;
				}
				break;
			case 'm':
			case 'M':
				color_array[0] = 1.0f;
				color_array[1] = 0.0f;
				color_array[2] = 1.0f;
				if (isBkBlack == false)
				{
					isBkBlack = true;
				}
				break;
			case 'y':
			case 'Y':
				color_array[0] = 1.0f;
				color_array[1] = 1.0f;
				color_array[2] = 0.0f;
				if (isBkBlack == false)
				{
					isBkBlack = true;
				}
				break;
			case 'k':
			case 'K':
				color_array[0] = 0.0f;
				color_array[1] = 0.0f;
				color_array[2] = 0.0f;			
				if (isBkBlack == true)
				{
					isBkBlack = false;
				}
				break;
			case 'w':
			case 'W':
				color_array[0] = 1.0f;
				color_array[1] = 1.0f;
				color_array[2] = 1.0f;
				if (isBkBlack == false)
				{
					isBkBlack = true;
				}
				break;
			case 'o':
			case 'O':
				color_array[0] = 1.0f;
				color_array[1] = 0.5f;
				color_array[2] = 0.0f;
				if (isBkBlack == false)
				{
					isBkBlack = true;
				}
				break;
			}
			break;
		case WM_PAINT:
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int,int);
	void Uninitialize();

	// Function declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}
	
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		DestroyWindow(ghwnd);
	}

	//OpenGL Logs
	fprintf(gpFile, "OpenGL Logs");
	fprintf(gpFile, "OpenGL Vendor: %s \n ", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer: %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "OpenGL Shading Language : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	fprintf(gpFile, "Open Enabled extension\n");
	GLint numExtension = 0;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	fprintf(gpFile, "Open Enabled total extension %d \n", numExtension);

	for (int i = 0; i < numExtension; i++)
	{		
		//fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	/********** VERTEX SHADER ***********/
	// CREATE SHADER

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	
	//provide souce code to shader
	const GLchar* vertexShaderSourceCode =  " #version 450 core\n							" \
											" in vec4 vPosition;							" \
											" in vec3 vColor;								" \
											" out vec3 oColor;								" \
											" uniform mat4 u_mvpMatrix;						" \
											" void main()									" \
											" {												" \
											"	gl_Position = u_mvpMatrix * vPosition;		" \
											"	oColor = vColor;							" \
											" }												" ;

	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCode =  " #version 450 core\n						" \
											" out vec4 FragColor;						" \
											" in vec3 oColor;							" \
											" void main()								" \
											" {											" \
											" 	FragColor = vec4(oColor, 1.0);			" \
											" }											" ;
	

	glShaderSource(gFragmentShaderObject, 1, (const char**)&fragmentShaderSourceCode, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_COLOR, "vColor");

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObject);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// get MVP uniform location
	gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvpMatrix");

	fprintf(gpFile, "\nAfter Shaders");
	fflush(gpFile);

	gPosition = (float*)malloc(sizeof(float) * my_array_size);

	for (int i = 0; i < gMeshWidth; i++)
	{
		for (int j = 0; j < gMeshHeight; j++)
		{
			for (int k = 0; k < 4; k++) 
			{
				gPosition[i * gMeshHeight * 4 + j * 4 + k] = 0.0f;
			}
		}
	}

	fprintf(gpFile, "\nAfter gPosition initialization");
	fflush(gpFile);

	glGenVertexArrays(1, &gVao);
	glBindVertexArray(gVao);

	glGenBuffers(1, &gVbo_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_position);
	glBufferData(GL_ARRAY_BUFFER, my_array_size * sizeof(float), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 4, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glVertexAttrib3f(HRH_ATTRIBUTE_COLOR, color_array[0], color_array[1], color_array[2]);

	glBindVertexArray(0);

	fprintf(gpFile, "\nAfter vao");
	fflush(gpFile);

	// set-up depth buffers
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// we will always cull face for better performance
	//glEnable(GL_CULL_FACE);

	gPerspectiveProjectionMatrix = mat4::identity();

	Resize(WIDTH, HIGHT);
}

void Resize(int width, int height) {
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) height);

	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f);

}

void Display() 
{
	// function declaration
	void launchSineWaveFromCPU(int meshWidth, int meshHeight, float time);

	if (isBkBlack)
	{
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	}
	else
	{
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	}

	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glUseProgram(gShaderProgramObject);

	mat4 modelViewMatrix = vmath::translate(0.0f, 0.0f, -1.5f);
	mat4 modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	fprintf(gpFile, "\nBefore launchSineWaveFromCPU");
	fflush(gpFile);

	launchSineWaveFromCPU(gMeshWidth, gMeshHeight, animation_time);

	fprintf(gpFile, "\nAfter launchSineWaveFromCPU");
	fflush(gpFile);
	
	glPointSize(5.0f);

	glBindBuffer(GL_ARRAY_BUFFER, gVbo_position);
	glBufferData(GL_ARRAY_BUFFER, my_array_size * sizeof(float), gPosition, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 4, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);

	glVertexAttrib3f(HRH_ATTRIBUTE_COLOR, color_array[0], color_array[1], color_array[2]);
	
	glBindVertexArray(gVao);
	glDrawArrays(GL_POINTS, 0, gMeshWidth * gMeshHeight);
	glBindVertexArray(0);

	animation_time = animation_time + 0.01f;

	glUseProgram(0);
	
	SwapBuffers(ghdc);
}

void launchSineWaveFromCPU(int meshWidth, int meshHeight, float time)
{
	for (int i = 0; i < meshWidth; i++)
	{
		for (int j = 0; j < meshHeight; j++)
		{			
			for (int k = 0; k < 4; k++)
			{
				float u = i / (float)meshWidth;
				float v = j / (float)meshHeight;
				u = u * 2.0f - 1.0f;
				v = v * 2.0f - 1.0f;
				float frequency = 4.0f;
				float w = sinf(u * frequency + time) * cosf(v * frequency + time) * 0.5f;

				if (k == 0)
				{
					gPosition[i * meshHeight  * 4 + j * 4 + k ] = u;
				}
				if (k == 1)
				{
					gPosition[i * meshHeight * 4 + j * 4 + k] = w;
				}
				if (k == 2)
				{
					gPosition[i * meshHeight * 4 + j * 4 + k] = v;
				}
				if (k == 3)
				{
					gPosition[i * meshHeight * 4 + j * 4 + k] = 1.0f;
				}
			}
		}
	}
}

void Uninitialize() {

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	// destroy vao
	if (gVao)
	{
		glDeleteVertexArrays(1, &gVao);
		gVao = 0;
	}

	// destroy vbo
	if (gVbo_position)
	{
		glDeleteBuffers(1, &gVbo_position);
		gVbo_position = 0;
	}

	// deatch vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	// detach fragment shader from shader program obejct
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject);

	gVertexShaderObject = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	// unlink shader program
	glUseProgram(0);

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile) {
		fprintf(gpFile, "\n Closing File Successfully");
		fclose(gpFile);
	}
}
