extern "C" void makeSphere(float fRadius,int iSlices,int iStacks);
extern "C" void getSphereVertexData(int iNumIndices, float* spherePositionCoords, float* sphereNormalCoords, float* sphereTexCoords, unsigned short* sphereElements);
extern "C" int getNumberOfSphereElements();
extern "C" int getNumberOfSphereVertices();
