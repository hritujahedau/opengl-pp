// cl.exe /c /EHsc Window.c
// rc.exe Windows.rc
// link.exe Window.obj user32.lib gdi32.lib /SUBSYSTEM:WINDOWS

// header files
#include <windows.h>
#include <stdio.h>
#include "../include/OGL.h"

// OpenGL header file
#include <GL/glew.h>		// This must be above gl.h
#include <GL/gl.h>

#include "../include/vmath.h"

#include "../include/assimp/Importer.hpp"
#include "../include/assimp/scene.h"
#include "../include/assimp/postprocess.h"
#include "../include/mesh.h"
#include "../include/filesystem.h"

using namespace vmath;

// OpenGL Library
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glew32.lib")

#define WINDOW_WIDTH	800
#define WINDOW_HEIGHT	600

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
BOOL gbFullscreen = FALSE;
FILE* gpFile = NULL;
BOOL gbActiveWindow = FALSE;

// Programmable pipeline related variables
GLuint shaderProgramObject;

// mesh loading
GLuint vao;
GLuint vbo;
GLuint ebo;

vector<Vertex>       vertices;
vector<unsigned int> indices;
vector<Texture>      textures;

// uniforms
GLuint modelMatrixUniform;
GLuint viewMatrixUniform;
GLuint projectionMatrixUniform;

mat4 perspectiveProjectionMatrix;
GLuint diffuseMapUniform;
GLuint isDiffuseMapUniform;
GLuint isNormalMapUniform;

GLuint backpackDiffuseTexture1;

string modelObjPath = "../resources/objects/backpack/backpack.obj";

// entry-point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function declarations
	int initialize(void);
	void display(void);
	void update(void);
	void uninitialize(void);

	// variable declarations
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");
	BOOL bDone = FALSE;
	int iReturnValue;

	// code
	if (fopen_s(&gpFile, ".//log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Not able to open file"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "File open Successfully");
	}

	// initialization of WNDCLASSEX structure
	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.lpfnWndProc = WndProc;
	wndClass.hInstance = hInstance;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndClass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szAppName;
	wndClass.lpszMenuName = NULL;
	wndClass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	// Registering above WNDCLASS
	RegisterClassEx(&wndClass);

	int window_x = (GetSystemMetrics(SM_CXFULLSCREEN) / 2) - (WINDOW_WIDTH / 2);
	int window_y = (GetSystemMetrics(SM_CYFULLSCREEN) / 2) - (WINDOW_HEIGHT / 2);

	hwnd = CreateWindowEx(
			WS_EX_APPWINDOW,
			szAppName,
			TEXT("OGL"),
			WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
			window_x,
			window_y,
			WINDOW_WIDTH,
			WINDOW_HEIGHT,
			NULL,
			NULL,
			hInstance,
			NULL
			);

	ghwnd = hwnd;

	// initialize
	iReturnValue = initialize();

	if (iReturnValue == -1)
	{
		fprintf(gpFile, "\nChoose pixel format failed");
		uninitialize();
	}
	else if (iReturnValue == -2)
	{
		fprintf(gpFile, "\nSet pixel format failed");
		uninitialize();
	}
	else if (iReturnValue == -3)
	{
		fprintf(gpFile, "\nCreate OpenGL context failed");
		uninitialize();
	}
	else if (iReturnValue == -4)
	{
		fprintf(gpFile, "\nMaking OpenGL context as current context failed");
		uninitialize();
	}
	else if (iReturnValue == -5)
	{
		fprintf(gpFile, "\nglewInit() failed");
		uninitialize();
	}
	else
	{
		fprintf(gpFile, "\nInitialize() successfull");
	}

	ShowWindow(hwnd, iCmdShow);

	// remove 
	// UpdateWindow(hwnd);

	// foregrounding and focusing the window
	SetForegroundWindow(hwnd); // we can use ghwnd, but use hwnd , ghwnd is used out of WinMain()
	SetFocus(hwnd);

	// message loop
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = TRUE;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == TRUE)
			{
				// render seen
				display();

				// update the seen
				update();
			}
		}
	}

	return ((int)(msg.wParam));
}

// callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggleFullscreen();
	void resize(int, int);
	void uninitialize();

	// code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = TRUE;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = FALSE;
		break;
	case WM_ERASEBKGND:
		return 0;
		// break; don't do break, we dont want to go WM_PAINT call to DefWindowProc() i.e OS 
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case 27:
			DestroyWindow(hwnd);
			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullscreen();
			break;
		default:
			break;
		}
		break;
	case WM_CLOSE:
		DestroyWindow(ghwnd);
		break;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	default:
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	// variable declarations
	static DWORD dwStyle;
	static WINDOWPLACEMENT wp;
	MONITORINFO mi;

	// code
	wp.length = sizeof(WINDOWPLACEMENT);
	if (gbFullscreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi.cbSize = sizeof(MONITORINFO);
			if (GetWindowPlacement(ghwnd, &wp) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
			// SWP_FRAMECHANGED - WM_NCCALSIZE NC - NON CLIENT CAL - CALCULATE SIZE
			ShowCursor(FALSE);
			gbFullscreen = TRUE;
		}
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wp);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
		gbFullscreen = FALSE;
	}
}

// following camel notion to because we will use this skeleton on all 6 platform on other platform hungerian notaion is not convention.
// but camel notaion is convention on other platform , so to keep same on all platform using camel notation
int initialize(void)
{
	// function declarations
	void resize(int, int);
	void printGLInfo();
	BOOL LoadGLTexture(GLuint*, TCHAR[]);
	void LoadModel(string const &path);
	void uninitialize(void);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// intialization of PIXELFORMATDESCRIPTOR
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;	 // we can use 24		// new change

	// Get DC
	ghdc = GetDC(ghwnd);

	// choose pixel format
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return -1;
	}

	// set the choosen pixel format
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return -2;
	}

	// create OpenGL Rendering context
	// 1st briding api call
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return -3;
	}

	// make rendering context as current context
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}

	// glew initialization()
	if (glewInit() != GLEW_OK)
	{
		return -5;
	}

	printGLInfo();

	// vertex shader
	const GLchar* vertexShaderSourceCode =
										" #version 460 core 																				\n " \
										"																									\n " \
										" in vec4 a_position;																				\n " \
										" in vec3 a_normal;																					\n " \
										" in vec2 a_texCoords;																				\n " \
										"																									\n " \
										" uniform mat4 u_modelMatrix;																		\n " \
										" uniform mat4 u_viewMatrix;																		\n " \
										" uniform mat4 u_projectionMatrix;																	\n " \
										"																									\n " \
										" out vec2 texCoords;																				\n " \
										"																									\n " \
										" void main()																						\n " \
										" {																									\n " \
										"																									\n " \
										"	texCoords = a_texCoords;																		\n " \
										"	gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;					\n " \
										"																									\n " \
										" }																									\n " \
										"																									\n " ;

	GLuint vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL); 
	// 2nd parameter : 1 - one shader, number of shader
	// 3rd parameter : array of shader object
	// 4th parameter : all shader strings are NULL terminated

	glCompileShader(vertexShaderObject);

	GLint status;
	GLint infoLogLength;
	char* log = NULL;
	GLsizei written;

	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength * sizeof(GLchar));
			if (log != NULL)
			{
				glGetShaderInfoLog(vertexShaderObject, infoLogLength, &written, log);
				fprintf(gpFile, "Vertex Shader compilation log :\n%s\n", log);
				free(log);
				log = NULL;
				uninitialize();
			}
		}
	}

	// fragment shader
	const GLchar* fragmentShader =
									" #version 460 core																			\n " \
									" 																							\n " \
									" out vec4 FragColor;																		\n " \
									" in vec2 texCoords;																		\n " \
									" uniform sampler2D texture_diffuse1;														\n " \
									" 																							\n " \
									" void main()																				\n " \
									" {																							\n " \
									"																							\n " \
									"		FragColor = texture(texture_diffuse1, texCoords);									\n " \
									" }																							\n " \
									"																							\n " ;

	GLuint fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShaderObject, 1, (GLchar**) & fragmentShader, NULL);

	glCompileShader(fragmentShaderObject);

	status = 0;
	infoLogLength = 0;
	log = NULL;
	written = 0;

	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			log = (GLchar*)malloc(infoLogLength);
			if (log != NULL)
			{
				glGetShaderInfoLog(fragmentShaderObject, infoLogLength, &written, log);
				fprintf(gpFile, "Fragment Shader compilation log:\n%s\n", log);
				free(log);
				log = NULL;
				uninitialize();
			}
		}
	}

	// shader program object
	shaderProgramObject = glCreateProgram();

	glAttachShader(shaderProgramObject, vertexShaderObject);
	glAttachShader(shaderProgramObject, fragmentShaderObject);

	glBindAttribLocation(shaderProgramObject, HRH_ATTRIBUTE_POSITION, "a_position");
	glBindAttribLocation(shaderProgramObject, HRH_ATTRIBUTE_NORMAL, "a_normal");
	glBindAttribLocation(shaderProgramObject, HRH_ATTRIBUTE_TEXTURE0, "a_texCoords");

	glLinkProgram(shaderProgramObject);

	status = 0;
	infoLogLength = 0;
	log = NULL;
	written = 0;

	glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &status);
	if (status == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			log = (GLchar*)malloc(infoLogLength);
			if (log != NULL)
			{
				glGetProgramInfoLog(shaderProgramObject, infoLogLength, &written, log);
				fprintf(gpFile, "Shader Program Link Fail. Log:\n%s\n", log);
				free(log);
				log = NULL;
				uninitialize();
			}
		}
	}

	modelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_modelMatrix");
	viewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
	projectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projectionMatrix");
	isDiffuseMapUniform = glGetUniformLocation(shaderProgramObject, "u_isDiffuseMap");
	diffuseMapUniform = glGetUniformLocation(shaderProgramObject, "texture_diffuse1");
	
	//LoadModel(FileSystem::getPath(modelObjPath));
	LoadModel(modelObjPath);

	// texture Laoding
	LoadGLTexture(&backpackDiffuseTexture1, ID_BACKPACK_DIFFUSE1);

	perspectiveProjectionMatrix = mat4::identity();

	// here stars OpenGL code
	// depth and clear related code
	// clear the screen using blue color	
	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

	// MUST for 3D Shapes
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	
	// this below two lines need only in FFP that's why removing from here (depreacted)
	// glShadeModel(GL_SMOOTH); 
	// glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	// warm up resize call
	resize(WINDOW_WIDTH, WINDOW_HEIGHT);

	return 0;
}

void printGLInfo()
{
	// function declarations
	void printfHorizontalLine();

	// Local variable declarations
	GLint numExtensions = 0;

	// Code
	printfHorizontalLine();
	
	fprintf(gpFile, "This program is written by Hrituja Ramkrishna Hedau under AMC assignment");
	
	printfHorizontalLine();
	
	fprintf(gpFile, "OpenGL Vendor (Graphics Card's Company) : %s \n", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer (Graphics Card Name) : %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));
	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);
	fprintf(gpFile, "Number of supported extensions : %d\n", numExtensions);

	for (int i = 0; i < numExtensions; i++)
	{
		// fprintf(gpFile, "%s \n", glGetStringi(GL_EXTENSIONS, i));
	}
	printfHorizontalLine();
}

void printfHorizontalLine()
{
	fprintf(gpFile, "\n\n================================================================================\n\n");
}

BOOL LoadGLTexture(GLuint* texture, TCHAR resourceID[])
{
	//VARIABLE DECLARATION
	BOOL bResult = FALSE;
	HBITMAP hBitmap = NULL;
	BITMAP bmp;

	//CODE
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), resourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap)
	{
		bResult = TRUE;
		GetObject(hBitmap, sizeof(BITMAP), &bmp);

		// from here start OpenGL Texture code
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1); //IN FFP 4
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);

		//setting texture parameter
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		// following call will actually push the data with help of graphics driver
		//gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);
		glTexImage2D(GL_TEXTURE_2D, 0, 3, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, bmp.bmBits);
		glGenerateMipmap(GL_TEXTURE_2D);

		DeleteObject(hBitmap);
	}
	return bResult;
}


void resize(int width, int height)
{
	// code
	if (height == 0)
	{
		height = 1;		// to avoid 'divided by 0' in future function call
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); // no 0.1f
}

void display(void)
{
	// Function Declarations
	void DrawModel();

	// Variable Declarations
	static float rotateAngle = 0.0;
	
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// variable declarations
	// transformation
	mat4 modelMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
	mat4 rotateMatrix = vmath::rotate(0.0f, rotateAngle, 0.0f);
	mat4 viewMatrix = mat4::identity();

	// Code
	// use shader program object
	glUseProgram(shaderProgramObject);

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix * rotateMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	DrawModel();

	// unuse shader program object
	glUseProgram(0);

	rotateAngle += 0.2f;
	
	SwapBuffers(ghdc);
}

void update(void)
{
	// code
}

void uninitialize(void)
{
	// function declarations
	void ToggleFullscreen(void);
	
	// code
	if (gbFullscreen == TRUE)
	{
		ToggleFullscreen();
	}

	// deletion and unitialization of vbo
	if (vbo)
	{
		glDeleteBuffers(1, &vbo);
		vbo = 0;
	}

	// deletion and unitialization of vao
	if (vao)
	{
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}

	// shader unitialize
	GLsizei numberOfAttachedShader;
	if (shaderProgramObject)
	{
		glUseProgram(shaderProgramObject);
	
		glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &numberOfAttachedShader);
		if (numberOfAttachedShader > 0)
		{
			GLuint* pAttachedShaderObject = NULL;
			pAttachedShaderObject = (GLuint*)malloc(numberOfAttachedShader);
			if (pAttachedShaderObject != NULL)
			{
				glGetAttachedShaders(shaderProgramObject, numberOfAttachedShader, &numberOfAttachedShader, pAttachedShaderObject);
				for (GLsizei shader = 0; shader < numberOfAttachedShader; shader++)
				{
					glDetachShader(shaderProgramObject, pAttachedShaderObject[shader]);
					glDeleteShader(pAttachedShaderObject[shader]);
					pAttachedShaderObject[shader] = 0;
				}
			}
			free(pAttachedShaderObject);
			pAttachedShaderObject = NULL;
		}
		glUseProgram(0);
		glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;
	}	
		
	
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (ghwnd)
	{
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "\nFile closed successfully");
		fclose(gpFile);
		gpFile = NULL;
	}
}
