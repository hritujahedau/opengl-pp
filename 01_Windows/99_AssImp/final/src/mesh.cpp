#include <windows.h>
#include <stdio.h>
#include "../include/OGL.h"

// OpenGL header file
#include <GL/glew.h>		// This must be above gl.h
#include <GL/gl.h>

#include "../include/vmath.h"

using namespace vmath;

#include "../include/mesh.h"
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

// mesh loading
extern GLuint vao;
extern GLuint vbo;
extern GLuint ebo;

Mesh CreateMesh(vector<Vertex> vertices, vector<unsigned int> indices, vector<Texture> textures)
{
	// Variable Declarations
	void setUpMeshVertexData(vector<Vertex> vertices, vector<unsigned int> indices, vector<Texture> textures,
		GLuint * vao, GLuint * vbo, GLuint * ebo);

	Mesh mesh;
	mesh.vertices = vertices;
	mesh.indices = indices;
	mesh.textures = textures;

	// now that we have all the required data, set the vertex buffers and its attribute pointers.
	setUpMeshVertexData(vertices,indices, textures, &mesh.vao, &mesh.vbo, &mesh.ebo);

	return mesh;
}

void setUpMeshVertexData(vector<Vertex> vertices, vector<unsigned int> indices, vector<Texture> textures,
						GLuint *vao, GLuint *vbo, GLuint *ebo)
{
	// vao and vbo related code
	glGenVertexArrays(1, vao);
	glBindVertexArray(*vao);

	glGenBuffers(1, vbo);
	glBindBuffer(GL_ARRAY_BUFFER, *vbo);

	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

	glGenBuffers(1, ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *ebo);

	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

	// vertex position
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);

	// vertex normals
	glEnableVertexAttribArray(HRH_ATTRIBUTE_NORMAL);
	glVertexAttribPointer(HRH_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Normal));

	// vertex texcoord
	glEnableVertexAttribArray(HRH_ATTRIBUTE_TEXTURE0);
	glVertexAttribPointer(HRH_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, TexCoords));

	// vertex tangent
	glEnableVertexAttribArray(HRH_ATTRIBUTE_TANGENT);
	glVertexAttribPointer(HRH_ATTRIBUTE_TANGENT, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Tangent));

	// vertex bitangent
	glEnableVertexAttribArray(HRH_ATTRIBUTE_BITANGENT);
	glVertexAttribPointer(HRH_ATTRIBUTE_BITANGENT, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Bitangent));

	// ids
	glEnableVertexAttribArray(HRH_ATTRIBUTE_ID);
	glVertexAttribPointer(HRH_ATTRIBUTE_ID, 4, GL_INT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, m_BoneIDs));

	// weights
	glEnableVertexAttribArray(HRH_ATTRIBUTE_WIGHTS);
	glVertexAttribPointer(HRH_ATTRIBUTE_WIGHTS, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, m_Weights));

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
}
