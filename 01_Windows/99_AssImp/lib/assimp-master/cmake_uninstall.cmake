IF(NOT EXISTS "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/install_manifest.txt")
  MESSAGE(FATAL_ERROR "Cannot find install manifest: \"F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/install_manifest.txt\"")
ENDIF(NOT EXISTS "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/install_manifest.txt")

FILE(READ "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/install_manifest.txt" files)
STRING(REGEX REPLACE "\n" ";" files "${files}")
FOREACH(file ${files})
  MESSAGE(STATUS "Uninstalling \"$ENV{DESTDIR}${file}\"")
  EXEC_PROGRAM(
    "C:/Program Files/CMake/bin/cmake.exe" ARGS "-E remove \"$ENV{DESTDIR}${file}\""
    OUTPUT_VARIABLE rm_out
    RETURN_VALUE rm_retval
    )
  IF(NOT "${rm_retval}" STREQUAL 0)
    MESSAGE(FATAL_ERROR "Problem when removing \"$ENV{DESTDIR}${file}\"")
  ENDIF(NOT "${rm_retval}" STREQUAL 0)
ENDFOREACH(file)
