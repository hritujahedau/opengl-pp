# Install script for directory: F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/Assimp")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xlibassimp5.2.0-devx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/lib/Debug/assimp-vc143-mtd.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/lib/Release/assimp-vc143-mt.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/lib/MinSizeRel/assimp-vc143-mt.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/lib/RelWithDebInfo/assimp-vc143-mt.lib")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xlibassimp5.2.0x" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/bin/Debug/assimp-vc143-mtd.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/bin/Release/assimp-vc143-mt.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/bin/MinSizeRel/assimp-vc143-mt.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/bin/RelWithDebInfo/assimp-vc143-mt.dll")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xassimp-devx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/assimp" TYPE FILE FILES
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/anim.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/aabb.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/ai_assert.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/camera.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/color4.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/color4.inl"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/config.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/ColladaMetaData.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/commonMetaData.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/defs.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/cfileio.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/light.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/material.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/material.inl"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/matrix3x3.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/matrix3x3.inl"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/matrix4x4.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/matrix4x4.inl"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/mesh.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/ObjMaterial.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/pbrmaterial.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/GltfMaterial.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/postprocess.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/quaternion.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/quaternion.inl"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/scene.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/metadata.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/texture.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/types.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/vector2.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/vector2.inl"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/vector3.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/vector3.inl"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/version.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/cimport.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/importerdesc.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/Importer.hpp"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/DefaultLogger.hpp"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/ProgressHandler.hpp"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/IOStream.hpp"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/IOSystem.hpp"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/Logger.hpp"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/LogStream.hpp"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/NullLogger.hpp"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/cexport.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/Exporter.hpp"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/DefaultIOStream.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/DefaultIOSystem.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/ZipArchiveIOSystem.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/SceneCombiner.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/fast_atof.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/qnan.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/BaseImporter.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/Hash.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/MemoryIOWrapper.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/ParsingUtils.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/StreamReader.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/StreamWriter.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/StringComparison.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/StringUtils.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/SGSpatialSort.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/GenericProperty.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/SpatialSort.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/SkeletonMeshBuilder.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/SmallVector.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/SmoothingGroups.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/SmoothingGroups.inl"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/StandardShapes.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/RemoveComments.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/Subdivision.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/Vertex.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/LineSplitter.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/TinyFormatter.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/Profiler.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/LogAux.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/Bitmap.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/XMLTools.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/IOStreamBuffer.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/CreateAnimMesh.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/XmlParser.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/BlobIOSystem.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/MathFunctions.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/Exceptional.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/ByteSwapper.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/Base64.hpp"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xassimp-devx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/assimp/Compiler" TYPE FILE FILES
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/Compiler/pushpack1.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/Compiler/poppack1.h"
    "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/../include/assimp/Compiler/pstdint.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE FILE FILES "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/Debug/assimp-vc143-mtd.pdb")
  endif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE FILE FILES "F:/Windows 10/RTR/RTR_3/RTR2020_Class_Assignment/01_OpenGL/02_PP/01_Windows/99_AssImp/lib/assimp-master/code/RelWithDebInfo/assimp-vc143-mt.pdb")
  endif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
endif()

