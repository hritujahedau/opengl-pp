// header file declaration
#include <windows.h>
#include<stdio.h>

#include <GL\glew.h>
#include <gl/GL.h>
#include "OGL.h"
#include "vmath.h"
#include "Sphere.h"

#pragma comment(lib, "C:\\glew-2.1.0\\lib\\Release\\Win32\\glew32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "Sphere.lib")

#define WIDTH 1000
#define HIGHT 800

using namespace vmath;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int width, hight;

enum
{
	HRH_ATTRIBUTE_POSITION = 0 ,
	HRH_ATTRIBUTE_COLOR ,
	HRH_ATTRIBUTE_NORMAL ,
	HRH_ATTRIBUTE_TEXTURE_0
};

GLuint gVertexShaderObjectPerFragment;
GLuint gFragmentShaderObjectPerFragment;
GLuint gShaderProgramObjectPerFragment;

GLuint gVertexShaderObjectPerVertex;
GLuint gFragmentShaderObjectPerVertex;
GLuint gShaderProgramObjectPerVertex;

GLuint gCurrentShaderProgram;

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint gModelUniform;
GLuint gViewUniform;
GLuint gPerspectiveUniform;

GLuint gLightAmbientUniform;
GLuint gLightSpecularUniform;
GLuint gLightDiffusedUniform;
GLuint gLightPossitionUniform;

GLuint gMaterialSpecularUniform;
GLuint gMaterialAmbientUniform;
GLuint gMaterialDiffuseUniform;
GLuint gMaterialShininessUniform;

GLuint gIsPerVertexShaderUniform;
GLuint gIsPerFragmentShaderUniform;

GLfloat sphere_vertices[1146];
GLfloat Sphere_normals[1146];
GLfloat Sphere_textrure[764];
unsigned short sphere_elements[2280]; //  Base indices from FFP tea pot

int isLightPerVertexShader = 1, isLightPerFragmentShader = 0, isLightOn = 0;
int gNumVertices;
int gNumElements;

mat4 gPerspectiveProjectionMatrix;

GLfloat lightPossition[] = { 100.0f, 100.0f, 100.0f, 1.0f };
GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f }; // make all 0.1 and last 1.0f for albedo
//GLfloat lightDefuse[] = { 1.0f, 1.0f, 1.0f  } ; //{ 0.5f, 0.2f, 0.7f };
//GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f }; // { 0.7f, 0.7f, 0.7f };

GLfloat lightDefuse[] = { 0.5f, 0.2f, 0.7f }; //{ 0.5f, 0.2f, 0.7f };
GLfloat lightSpecular[] = { 0.7f, 0.7f, 0.7f }; // { 0.7f, 0.7f, 0.7f };

GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f };
GLfloat materialDefuse[] = { 1.0f, 1.0f, 1.0f  }; 
GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0f};
GLfloat materialShinyness = 128.0f;


//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	void Initialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("h_ogl");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 

	if (fopen_s(&gpFile, "windows.log", "w") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("OGL"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HIGHT/2 ,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow) {
				Display();
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	void Resize(int,int);

	// code
	switch (uMsg) {
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return 0L;
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'v':
		case 'V':
			if (isLightPerVertexShader == 1)
			{
				isLightPerVertexShader = 0;
			}
			else
			{
				isLightPerVertexShader = 1;
				isLightPerFragmentShader = 0;
			}
			break;
		case 'f':
		case 'F':
			if (isLightPerFragmentShader == 0)
			{
				isLightPerFragmentShader = 1;
				isLightPerVertexShader = 0;
			}
			else
			{
				isLightPerFragmentShader = 0;
			}
			break;
		case 'q':
		case 'Q':
			ToggleFullScreen();
			break;
		}
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;				
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int,int);
	void Uninitialize();
	void getSphereVertexData(float spherePositionCoords[1146], float sphereNormalCoords[1146], float sphereTexCoords[764], unsigned short sphereElements[2280]);
	unsigned int getNumberOfSphereVertices(void);
	unsigned int getNumberOfSphereElements(void);


	// Function declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}
	
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		DestroyWindow(ghwnd);
	}

	//OpenGL Logs
	fprintf(gpFile, "OpenGL Logs");
	fprintf(gpFile, "OpenGL Vendor: %s \n ", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer: %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "OpenGL Shading Language : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	fprintf(gpFile, "Open Enabled extension\n");
	GLint numExtension = 0;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	fprintf(gpFile, "Open Enabled total extension %d \n", numExtension);

	for (int i = 0; i < numExtension; i++)
	{		
		//fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	/***************************************PER VERTEX SHADER*******************************************************************/
	gVertexShaderObjectPerVertex = glCreateShader(GL_VERTEX_SHADER);

	//provide souce code to shader
	const GLchar* vertexShaderSourceCodePerVertex = "#version 450 core\n"  \
		"in vec4 v_position;" \
		"in vec3 v_normals; " \
		"uniform mat4 u_modelMatrix;" \
		"uniform mat4 u_viewMatrix;" \
		"uniform mat4 u_projectionMatrix;" \
		"uniform int u_isPerVertexLightOn;" \
		"uniform vec3 u_lightAmbient;" \
		"uniform vec3 u_lightSpecular;" \
		"uniform vec3 u_lightDiffuse;" \
		"uniform vec4 u_light_position; " \
		"uniform vec3 u_materialAmbient; " \
		"uniform vec3 u_materialSpecular; "\
		"uniform vec3 u_materialDiffuse; "\
		"uniform float u_materialshininess; "\
		"out vec3 phoung_ads_lighting;" \
		"void main()" \
		"{" \
		"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" \
		"if (u_isPerVertexLightOn == 1) " \
		"{" \
		"vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" \
		"vec3 transformed_normal = normalize( mat3( u_viewMatrix * u_modelMatrix ) * v_normals); " \
		"vec3 light_direction = normalize( vec3 (u_light_position - eye_coordinates) ); " \
		"vec3 reflection_vector = reflect (-light_direction, transformed_normal); " \
		"vec3 view_vector = normalize ( vec3 ( -eye_coordinates)); " \
		"vec3 ambient = u_lightAmbient * u_materialAmbient; " \
		"vec3 diffuse_light = u_lightDiffuse * u_materialDiffuse * max ( dot (light_direction , transformed_normal), 0.0); "\
		"vec3 specular = u_lightSpecular * u_materialSpecular * pow( max ( dot ( reflection_vector , view_vector), 0.0) , u_materialshininess);" \
		"phoung_ads_lighting = ambient + diffuse_light + specular;" \
		"}" \
		"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" \
		"}";

	glShaderSource(gVertexShaderObjectPerVertex, 1, (const GLchar**)&vertexShaderSourceCodePerVertex, NULL);

	// compile shader
	glCompileShader(gVertexShaderObjectPerVertex);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObjectPerVertex = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCodePerVertex = "#version 450 core\n" \
		"in vec3 phoung_ads_lighting;" \
		"out vec4 FragColor; " \
		"void main()" \
		"{"\
		"FragColor = vec4(phoung_ads_lighting, 1.0); " \
		"}";



	glShaderSource(gFragmentShaderObjectPerVertex, 1, (const char**)&fragmentShaderSourceCodePerVertex, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObjectPerVertex);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObjectPerVertex = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);

	// pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(gShaderProgramObjectPerVertex, HRH_ATTRIBUTE_POSITION, "v_position");
	glBindAttribLocation(gShaderProgramObjectPerVertex, HRH_ATTRIBUTE_NORMAL, "v_normals");

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObjectPerVertex);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObjectPerVertex, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectPerVertex, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	/**********************************************************************************************************************************************************/

	/**************************************PER FRAGMENT SHADER******************************************************************/
	/********** VERTEX SHADER ***********/
	// CREATE SHADER

	gVertexShaderObjectPerFragment = glCreateShader(GL_VERTEX_SHADER);
	
	//provide souce code to shader
	const GLchar* vertexShaderSourceCodePerFragment =  "#version 450 core\n"  \
	"in vec4 v_position;" \
	"in vec3 v_normals; " \
	"uniform mat4 u_modelMatrix;" \
	"uniform mat4 u_viewMatrix;" \
	"uniform mat4 u_projectionMatrix;" \
	"uniform int u_isPerFragmentLightOn;" \
	"uniform vec4 u_light_position; " \
	"out vec3 light_direction; "\
	"out vec3 transformed_normal;"\
	"out vec3 view_vector;"\
	"void main()" \
	"{" \
	"if (u_isPerFragmentLightOn == 1) " \
	"{" \
	"vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" \
	"transformed_normal =  mat3( u_viewMatrix * u_modelMatrix ) * v_normals;" \
	"light_direction = vec3 (u_light_position - eye_coordinates);" \
	"view_vector =  vec3 ( -eye_coordinates);" \
	"}" \
	"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" \
	"}";

	glShaderSource(gVertexShaderObjectPerFragment, 1, (const GLchar**)&vertexShaderSourceCodePerFragment, NULL);

	// compile shader
	glCompileShader(gVertexShaderObjectPerFragment);

	// check compilation error
	iInfoLogLength = 0;
	iShaderCompileStatus = 0;
	szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObjectPerFragment = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCodePerFragment = "#version 450 core\n" \
		"vec3 phoung_ads_lighting;" \
		"out vec4 FragColor; " \
		"in vec3 light_direction; "\
		"in vec3 transformed_normal;"\
		"in vec3 view_vector;"\
		"vec3 normalized_light_direction; "\
		"vec3 normalized_transformed_normal;"\
		"vec3 normalized_view_vector;"\
		"uniform vec3 u_lightAmbient;" \
		"uniform vec3 u_lightSpecular;" \
		"uniform vec3 u_lightDiffuse;" \
		"uniform vec3 u_materialAmbient; " \
		"uniform vec3 u_materialSpecular; "\
		"uniform vec3 u_materialDiffuse; "\
		"uniform float u_materialshininess; "\
		"uniform int u_isPerFragmentLightOn;" \
		"void main()" \
		"{"\
		"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" \
		"if (u_isPerFragmentLightOn == 1) " \
		"{" \
		"normalized_light_direction = normalize(light_direction);"\
		"normalized_transformed_normal = normalize(transformed_normal);"\
		"normalized_view_vector = normalize(view_vector);"\
		"vec3 reflection_vector = reflect (-normalized_light_direction, normalized_transformed_normal); " \
		"vec3 ambient = u_lightAmbient * u_materialAmbient; " \
		"vec3 diffuse_light = u_lightDiffuse * u_materialDiffuse * max ( dot (normalized_light_direction , normalized_transformed_normal), 0.0); "\
		"vec3 specular = u_lightSpecular * u_materialSpecular * pow( max ( dot ( reflection_vector , normalized_view_vector), 0.0) , u_materialshininess);" \
		"phoung_ads_lighting = ambient + diffuse_light + specular;" \
		"}"\
		"FragColor = vec4(phoung_ads_lighting, 1.0); " \
		"}";

	glShaderSource(gFragmentShaderObjectPerFragment, 1, (const char**)&fragmentShaderSourceCodePerFragment, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObjectPerFragment);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObjectPerFragment = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);

	// pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(gShaderProgramObjectPerFragment, HRH_ATTRIBUTE_POSITION, "v_position");
	glBindAttribLocation(gShaderProgramObjectPerFragment, HRH_ATTRIBUTE_NORMAL, "v_normals");

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObjectPerFragment);

	// CHECK LINKING ERROR
	iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObjectPerFragment, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectPerFragment, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}
	

	// get MVP uniform location
	//gModelUniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_modelMatrix");
	//gViewUniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_viewMatrix");
	//gPerspectiveUniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_projectionMatrix");
	//gIsPerFragmentShaderUniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_isPerFragmentLightOn");
	//
	//gLightAmbientUniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_lightAmbient");
	//gLightSpecularUniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_lightSpecular");
	//gLightDiffusedUniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_lightDiffuse");
	//gLightPossitionUniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_light_position");
	//
	//gMaterialDiffuseUniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_materialDiffuse");
	//gMaterialAmbientUniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_materialAmbient");
	//gMaterialSpecularUniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_materialSpecular");
	//gMaterialShininessUniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_materialshininess");

	/**********************************************************************************************************************************************************/

	getSphereVertexData(sphere_vertices, Sphere_normals, Sphere_textrure, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// ***** vertex, colors, shadersatribs , vbo, vao initialization *********
	
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo

	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Sphere_normals), Sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0); // end for Vao

	// set-up depth buffers
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // don't work

	// we will always cull face for better performance
	glEnable(GL_CULL_FACE);

	gPerspectiveProjectionMatrix = mat4::identity();

	Resize(WIDTH, HIGHT);
}

void Resize(int width, int height) {
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) height);

	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); // no 0.1f

}

void Display() {
	// CODE
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	/************************************************PER FRAGMENT***************************************************************/
	// START USING OpenGL program object
	gCurrentShaderProgram = (isLightPerVertexShader == 1) ? gShaderProgramObjectPerVertex: gShaderProgramObjectPerFragment ;

	glUseProgram(gCurrentShaderProgram);

	// OpenGL Drawing
	// set modelview & modelviewProjection matrices to indentity
	mat4 translateMatrix = vmath::translate(0.0f, 0.0f, -1.55f);
	mat4 viewMatrix = mat4::identity();
	mat4 modelMatrix = translateMatrix;
	
	gModelUniform = glGetUniformLocation(gCurrentShaderProgram, "u_modelMatrix");
	gViewUniform = glGetUniformLocation(gCurrentShaderProgram, "u_viewMatrix");
	gPerspectiveUniform = glGetUniformLocation(gCurrentShaderProgram, "u_projectionMatrix");
	gIsPerFragmentShaderUniform = glGetUniformLocation(gCurrentShaderProgram, "u_isPerFragmentLightOn");
	gIsPerVertexShaderUniform = glGetUniformLocation(gCurrentShaderProgram, "u_isPerVertexLightOn");

	gLightAmbientUniform = glGetUniformLocation(gCurrentShaderProgram, "u_lightAmbient");
	gLightSpecularUniform = glGetUniformLocation(gCurrentShaderProgram, "u_lightSpecular");
	gLightDiffusedUniform = glGetUniformLocation(gCurrentShaderProgram, "u_lightDiffuse");
	gLightPossitionUniform = glGetUniformLocation(gCurrentShaderProgram, "u_light_position");

	gMaterialDiffuseUniform = glGetUniformLocation(gCurrentShaderProgram, "u_materialDiffuse");
	gMaterialAmbientUniform = glGetUniformLocation(gCurrentShaderProgram, "u_materialAmbient");
	gMaterialSpecularUniform = glGetUniformLocation(gCurrentShaderProgram, "u_materialSpecular");
	gMaterialShininessUniform = glGetUniformLocation(gCurrentShaderProgram, "u_materialshininess");

	if (isLightPerFragmentShader == 1 || isLightPerVertexShader == 1)
	{
		glUniform4fv(gLightPossitionUniform, 1, (const GLfloat*) lightPossition);
		glUniform3fv(gLightDiffusedUniform, 1, (const GLfloat*) lightDefuse);
		glUniform3fv(gLightAmbientUniform,  1 , (const GLfloat*) lightAmbient);
		glUniform3fv(gLightSpecularUniform, 1 , (const GLfloat*) lightSpecular);

		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*) materialAmbient);
		glUniform1f(gMaterialShininessUniform, materialShinyness);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*) materialSpecular);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*) materialDefuse);
	}

	glUniform1i(gIsPerFragmentShaderUniform, isLightPerFragmentShader);
	glUniform1i(gIsPerVertexShaderUniform, isLightPerVertexShader);
	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*) modelMatrix);
	glUniformMatrix4fv(gViewUniform, 1, GL_FALSE, (const float*) viewMatrix);
	glUniformMatrix4fv(gPerspectiveUniform, 1, GL_FALSE, (const GLfloat *) gPerspectiveProjectionMatrix);

	// *** bind vao **
	glBindVertexArray(gVao_sphere);

	// ** draw , either by glDrawTriangles() or glDrawArrays() or glDrawElements();
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0); //3 (each with its x,y,z) value in triangleVertices array

	// ** unbind vao **
	glBindVertexArray(0);

	// stop using OpenGL program object
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void Uninitialize() {

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	// destroy vao
	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}

	// destroy vbo
	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}

	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}

	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}

	// deatch vertex shader from shader program object
	glDetachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);

	// detach fragment shader from shader program obejct
	glDetachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObjectPerFragment);

	gVertexShaderObjectPerFragment = 0;	

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObjectPerFragment);
	gFragmentShaderObjectPerFragment = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObjectPerFragment);
	gShaderProgramObjectPerFragment = 0;


	glDetachShader(gShaderProgramObjectPerVertex, gShaderProgramObjectPerVertex);
	glDetachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);

	glDeleteShader(gVertexShaderObjectPerVertex);
	gVertexShaderObjectPerVertex = 0;

	glDeleteShader(gFragmentShaderObjectPerVertex);
	gFragmentShaderObjectPerVertex = 0;

	// unlink shader program
	glUseProgram(0);

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile) {
		fprintf(gpFile, "\n Closing File Successfully");
		fclose(gpFile);
	}
}
