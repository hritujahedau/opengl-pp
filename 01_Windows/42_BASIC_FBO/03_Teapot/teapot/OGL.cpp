// header file declaration
#include <windows.h>
#include<stdio.h>

#include <GL\glew.h>
#include <gl/GL.h>
#include "OGL.h"
#include "vmath.h"
#include "TeaPot.h"

#pragma comment(lib, "C:\\glew-2.1.0\\lib\\Release\\Win32\\glew32.lib")
#pragma comment(lib, "opengl32.lib")

#define WIDTH 1000
#define HIGHT 800

using namespace vmath;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int width, hight;

enum
{
	HRH_ATTRIBUTE_POSITION = 0 ,
	HRH_ATTRIBUTE_COLOR ,
	HRH_ATTRIBUTE_NORMAL ,
	HRH_ATTRIBUTE_TEXTURE_0
};

GLuint gVertexShaderObjectTeapot;
GLuint gFragmentShaderObjectTeapot;
GLuint gShaderProgramObjectTeapot;

GLuint gVao_teapot;
GLuint gVbo_position_teapot;
GLuint gVbo_normals_teapot;
GLuint gVbo_texture_teapot;
GLuint gVbo_element_teapot;

GLuint gModelUniformTeapot;
GLuint gViewUniformTeapot;
GLuint gPerspectiveUniformTeapot;

GLuint gLightAmbientUniform;
GLuint gLightSpecularUniform;
GLuint gLightDiffusedUniform;
GLuint gLightPossitionUniform;

GLuint gMaterialSpecularUniform;
GLuint gMaterialAmbientUniform;
GLuint gMaterialDiffuseUniform;
GLuint gMaterialShininessUniform;
GLuint gTextureSamplerUniform;

GLuint gIsLightOnUniform;
GLuint gIsTextureOnUniform;

mat4 gPerspectiveProjectionMatrix;

GLfloat teapotVertices[141336];     // total 141336
GLfloat teapotNormal[141336];       // total 141336 
GLfloat teapotTexcoord[94224];      // total  94224
short teapotFaceIndices[15704 * 8];  // total 15704 * 9

bool gbAnimate = false;

GLfloat angle_for_rotate_teapot = 0.0f;

GLuint marble_texture = 0;

GLfloat lightPossition[] = { 100.0f, 100.0f, 100.0f, 1.0f };
GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f };
GLfloat lightDefuse[]   = { 1.0f, 1.0f, 1.0f };
GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f };

//GLfloat lightDefuse[]   = {0.5f, 0.5f, 0.5f};
//GLfloat lightSpecular[] =  {0.5f, 0.5f, 0.5f};

GLfloat materialAmbient[]  = { 0.0f, 0.0f, 0.0f };
GLfloat materialDefuse[]   = { 1.0f, 1.0f, 1.0f };
GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0f };

//GLfloat materialDefuse[]   = { 0.5f, 0.5f, 0.5f };
//GLfloat materialSpecular[] = { 0.5f, 0.5f, 0.5f };

GLfloat materialShinyness = 50.0f;

int isLightOn = 0;
int isTextureOn = 0;

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	void Initialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("h_ogl");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 

	if (fopen_s(&gpFile, "windows.log", "w") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("OGL"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HIGHT/2 ,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow) {
				Display();
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	void Resize(int,int);

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_CHAR:
			switch (wParam)
			{
				case 't':
				case 'T':
					if (isTextureOn)
					{
						isTextureOn = 0;
					}
					else
					{
						isTextureOn = 1;
					}
					break;
				case 'l':
				case 'L':
					if (isLightOn == 0)
					{
						isLightOn = 1;
					}
					else
					{
						isLightOn = 0;
					}
					break;
				case 'a':
				case 'A':
					if (gbAnimate)
					{
						gbAnimate = false;
					}
					else
					{
						gbAnimate = true;
					}
					break;
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int,int);
	bool LoadGLTexture(GLuint * texture, TCHAR resourceID[]);
	void Uninitialize();

	// Function declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}
	
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		DestroyWindow(ghwnd);
	}

	//OpenGL Logs
	fprintf(gpFile, "OpenGL Logs");
	fprintf(gpFile, "OpenGL Vendor: %s \n ", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer: %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "OpenGL Shading Language : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	fprintf(gpFile, "Open Enabled extension\n");
	GLint numExtension = 0;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	fprintf(gpFile, "Open Enabled total extension %d \n", numExtension);

	for (int i = 0; i < numExtension; i++)
	{		
		//fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	/********** VERTEX SHADER ***********/
	// CREATE SHADER

	gVertexShaderObjectTeapot = glCreateShader(GL_VERTEX_SHADER);
	
	//provide souce code to shader
	const GLchar* vertexShaderSourceCode =  
		"#version 450 core\n"  \
		"in vec4 v_position;" \
		"in vec2 vTexCoord; "\
		"out vec2 vTexCoord_out;" \
		"in vec3 v_normals; " \
		"uniform mat4 u_modelMatrix;" \
		"uniform mat4 u_viewMatrix;" \
		"uniform mat4 u_projectionMatrix;" \
		"uniform int u_isLightOn;" \
		"uniform vec4 u_light_position; " \
		"out vec3 light_direction; "\
		"out vec3 transformed_normal;"\
		"out vec3 view_vector;"\
		"void main()" \
		"{" \
		"if (u_isLightOn == 1) " \
		"{" \
		"vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" \
		"transformed_normal =  mat3( u_viewMatrix * u_modelMatrix ) * v_normals;" \
		"light_direction = vec3 (u_light_position - eye_coordinates);" \
		"view_vector =  vec3 ( -eye_coordinates);" \
		"}" \
		"vTexCoord_out = vTexCoord;" \
		"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" \
		"}";

	glShaderSource(gVertexShaderObjectTeapot, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObjectTeapot);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObjectTeapot, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectTeapot, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectTeapot, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObjectTeapot = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCode = 
		"#version 450 core\n" \
		"in vec2 vTexCoord_out;"\
		"vec3 phoung_ads_lighting;" \
		"out vec4 FragColor; " \
		"in vec3 light_direction; "\
		"in vec3 transformed_normal;"\
		"in vec3 view_vector;"\
		"vec3 normalized_light_direction; "\
		"vec3 normalized_transformed_normal;"\
		"vec3 normalized_view_vector;"\
		"uniform vec3 u_lightAmbient;" \
		"uniform vec3 u_lightSpecular;" \
		"uniform vec3 u_lightDiffuse;" \
		"uniform vec3 u_materialAmbient; " \
		"uniform vec3 u_materialSpecular; "\
		"uniform vec3 u_materialDiffuse; "\
		"uniform float u_materialshininess; "\
		"uniform int u_isLightOn;" \
		"uniform int u_isTextureOn;" \
		"uniform sampler2D u_sampler_2d; "\
		"void main()" \
		"{"\
			"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" \
			"if (u_isLightOn == 1) " \
			"{" \
			"normalized_light_direction = normalize(light_direction);"\
			"normalized_transformed_normal = normalize(transformed_normal);"\
			"normalized_view_vector = normalize(view_vector);"\
			"vec3 reflection_vector = reflect (-normalized_light_direction, normalized_transformed_normal); " \
			"vec3 ambient = u_lightAmbient * u_materialAmbient; " \
			"vec3 diffuse_light = u_lightDiffuse * u_materialDiffuse * max ( dot (normalized_light_direction , normalized_transformed_normal), 0.0); "\
			"vec3 specular = u_lightSpecular * u_materialSpecular * pow( max ( dot ( reflection_vector , normalized_view_vector), 0.0) , u_materialshininess);" \
			"phoung_ads_lighting = ambient + diffuse_light + specular;" \
			"}"\
			"if(u_isLightOn==1 && u_isTextureOn == 1)" \
			"{" \
				"FragColor = vec4(phoung_ads_lighting, 1.0) * texture(u_sampler_2d, vTexCoord_out) ; " \
			"}" \
			"else if( u_isTextureOn == 1 )" \
			"{" \
				"FragColor = texture(u_sampler_2d, vTexCoord_out) ; " \
			"}" \
			"else" \
			"{" \
				"FragColor = vec4(phoung_ads_lighting, 1.0); " \
			"}" \
		"}";

	// mix(vec4(phoung_ads_lighting, 1.0), texture(u_sampler_2d, vTexCoord_out) , 0.5);
	// texture(u_sampler_2d, vTexCoord_out) + vec4(phoung_ads_lighting, 1.0);

	glShaderSource(gFragmentShaderObjectTeapot, 1, (const char**)&fragmentShaderSourceCode, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObjectTeapot);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObjectTeapot, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectTeapot, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectTeapot, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObjectTeapot = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObjectTeapot, gVertexShaderObjectTeapot);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObjectTeapot, gFragmentShaderObjectTeapot);

	// pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(gShaderProgramObjectTeapot, HRH_ATTRIBUTE_POSITION, "v_position");
	glBindAttribLocation(gShaderProgramObjectTeapot, HRH_ATTRIBUTE_NORMAL, "v_normals");
	glBindAttribLocation(gShaderProgramObjectTeapot, HRH_ATTRIBUTE_TEXTURE_0, "vTexCoord");

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObjectTeapot);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObjectTeapot, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectTeapot, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectTeapot, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// get MVP uniform location
	gModelUniformTeapot = glGetUniformLocation(gShaderProgramObjectTeapot, "u_modelMatrix");
	gViewUniformTeapot = glGetUniformLocation(gShaderProgramObjectTeapot, "u_viewMatrix");
	gPerspectiveUniformTeapot = glGetUniformLocation(gShaderProgramObjectTeapot, "u_projectionMatrix");
	gIsLightOnUniform = glGetUniformLocation(gShaderProgramObjectTeapot, "u_isLightOn");
	gIsTextureOnUniform = glGetUniformLocation(gShaderProgramObjectTeapot, "u_isTextureOn");

	gLightAmbientUniform = glGetUniformLocation(gShaderProgramObjectTeapot, "u_lightAmbient");
	gLightSpecularUniform = glGetUniformLocation(gShaderProgramObjectTeapot, "u_lightSpecular");
	gLightDiffusedUniform = glGetUniformLocation(gShaderProgramObjectTeapot, "u_lightDiffuse");
	gLightPossitionUniform = glGetUniformLocation(gShaderProgramObjectTeapot, "u_light_position");

	gMaterialDiffuseUniform = glGetUniformLocation(gShaderProgramObjectTeapot, "u_materialDiffuse");
	gMaterialAmbientUniform = glGetUniformLocation(gShaderProgramObjectTeapot, "u_materialAmbient");
	gMaterialSpecularUniform = glGetUniformLocation(gShaderProgramObjectTeapot, "u_materialSpecular");
	gMaterialShininessUniform = glGetUniformLocation(gShaderProgramObjectTeapot, "u_materialshininess");
	gTextureSamplerUniform = glGetUniformLocation(gShaderProgramObjectTeapot, "u_texture_sampler");

	// ***** vertex, colors, shadersatribs , vbo, vao initialization *********

	int totalElement = 0;
	int totalTexcoord = 0;
	int v = 0, n = 0, t = 0, f = 0;
	for (int i = 0; i < (sizeof(face_indicies) / sizeof(face_indicies[0])); i++)
	{
		for (int j = 0; j < 3; j++)
		{
			int vi = face_indicies[i][j];
			int ni = face_indicies[i][j + 3];
			int ti = face_indicies[i][j + 6];

			//teapotFaceIndices[f] = face_indicies[i][j];
			//teapotFaceIndices[f + 1] = face_indicies[i][j + 3];
			//teapotFaceIndices[f + 2] = face_indicies[i][j + 6];
			//
			//f = f + 3;

			//glNormal3f(normals[ni][0], normals[ni][1], normals[ni][2]);
			//glTexCoord2f(textures[ti][0], textures[ti][1]);
			//glVertex3f(vertices[vi][0], vertices[vi][1], vertices[vi][2]);

			teapotNormal[n + 0] = normals[ni][0];
			teapotNormal[n + 1] = normals[ni][1];
			teapotNormal[n + 2] = normals[ni][2];

			n = n + 3;

			teapotTexcoord[t] = textures[ti][0];
			teapotTexcoord[t + 1] = textures[ti][1];

			t = t + 2;

			teapotVertices[v + 0] = vertices[vi][0];
			teapotVertices[v + 1] = vertices[vi][1];
			teapotVertices[v + 2] = vertices[vi][2];

			v = v + 3;
			totalElement = totalElement + 3;
			totalTexcoord = totalTexcoord + 2;
		}
	}

	f = 0;
	for (int i = 0; i < 15704; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			teapotFaceIndices[f] = face_indicies[i][j];
			f = f + 1;
		}
	}

	fprintf(gpFile, "\nf = %d", f);

	glGenVertexArrays(1, &gVao_teapot);
	glBindVertexArray(gVao_teapot);

	glGenBuffers(1, &gVbo_position_teapot);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_position_teapot);
	glBufferData(GL_ARRAY_BUFFER, sizeof(teapotVertices), teapotVertices, GL_STATIC_DRAW);	
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo

	glGenBuffers(1, &gVbo_normals_teapot);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_normals_teapot);
	glBufferData(GL_ARRAY_BUFFER, sizeof(teapotNormal), teapotNormal, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo

	glGenBuffers(1, &gVbo_texture_teapot);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_texture_teapot);
	glBufferData(GL_ARRAY_BUFFER, sizeof(teapotTexcoord), teapotTexcoord, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_TEXTURE_0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_TEXTURE_0);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo

	glGenBuffers(1, &gVbo_element_teapot);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_element_teapot);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(teapotFaceIndices), teapotFaceIndices, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0); // end for Vao

	LoadGLTexture(&marble_texture, ID_MARBLE_BITMAP);
	glEnable(GL_TEXTURE_2D);

	// set-up depth buffers
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); 

	// we will always cull face for better performance
	glEnable(GL_CULL_FACE);

	gPerspectiveProjectionMatrix = mat4::identity();

	Resize(WIDTH, HIGHT);
}

// Texture code
bool LoadGLTexture(GLuint* texture, TCHAR resourceID[])
{
	//VARIABLE DECLARATION
	bool bResult = false;
	HBITMAP hBitmap = NULL;
	BITMAP bmp;

	//CODE
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), resourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap)
	{
		bResult = true;
		GetObject(hBitmap, sizeof(BITMAP), &bmp);

		// from here start OpenGL Texture code
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1); //IN FFP 4
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);

		//setting texture parameter
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		// following call will actually push the data with help of graphics driver
		//gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);
		glTexImage2D(GL_TEXTURE_2D, 0, 3, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, bmp.bmBits);
		glGenerateMipmap(GL_TEXTURE_2D);

		DeleteObject(hBitmap);
	}
	return bResult;
}


void Resize(int width, int height) {
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) height);

	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); // no 0.1f

}

void Display() {
	// CODE
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// START USING OpenGL program object
	glUseProgram(gShaderProgramObjectTeapot);

	//OpenGL Drawing
	// set modelview & modelviewProjection matrices to indentity
	mat4 translateMatrix = vmath::translate(0.0f, 0.0f, -1.55f);
	mat4 rotateMatrixtoMakeStraight = vmath::rotate(90.0f, 0.0f, 1.0f, 0.0f);
	mat4 rotateMatrix = vmath::rotate(angle_for_rotate_teapot, 0.0f, 1.0f, 0.0f);
	mat4 viewMatrix = mat4::identity();
	mat4 modelMatrix = translateMatrix  * rotateMatrix;

	if (isLightOn == 1)
	{
		glUniform4fv(gLightPossitionUniform, 1, (const GLfloat*) lightPossition);
		glUniform3fv(gLightDiffusedUniform, 1, (const GLfloat*) lightDefuse);
		glUniform3fv(gLightAmbientUniform,  1 , (const GLfloat*) lightAmbient);
		glUniform3fv(gLightSpecularUniform, 1 , (const GLfloat*) lightSpecular);

		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*) materialAmbient);
		glUniform1f(gMaterialShininessUniform, materialShinyness);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*) materialSpecular);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*) materialDefuse);
	}

	glUniform1i(gIsLightOnUniform, isLightOn);
	glUniform1i(gIsTextureOnUniform, isTextureOn);
	glUniformMatrix4fv(gModelUniformTeapot, 1, GL_FALSE, (const float*) modelMatrix);
	glUniformMatrix4fv(gViewUniformTeapot, 1, GL_FALSE, (const float*) viewMatrix);
	glUniformMatrix4fv(gPerspectiveUniformTeapot, 1, GL_FALSE, (const GLfloat *) gPerspectiveProjectionMatrix);

	if (isTextureOn)
	{
		glEnable(GL_TEXTURE_2D);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, marble_texture);
		glUniform1i(gTextureSamplerUniform, 0);
	}
	else
	{
		glDisable(GL_TEXTURE_2D);
	}
	glBindVertexArray(gVao_teapot);
	glDrawArrays(GL_TRIANGLES, 0, 47112);
//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_element_teapot);
//	glDrawElements(GL_TRIANGLES, 15704 * 8 * 2, GL_UNSIGNED_SHORT, 0);
//	glDrawElements(GL_TRIANGLES, 47112 * 3, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	if (gbAnimate)
	{
		angle_for_rotate_teapot = angle_for_rotate_teapot + 0.2f;
		if (angle_for_rotate_teapot > 360)
		{
			angle_for_rotate_teapot = 0.0f;
		}
	}

	// stop using OpenGL program object
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void Uninitialize() {

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	// destroy vao
	if (gVao_teapot)
	{
		glDeleteVertexArrays(1, &gVao_teapot);
		gVao_teapot = 0;
	}

	// destroy vbo
	if (gVbo_position_teapot)
	{
		glDeleteBuffers(1, &gVbo_position_teapot);
		gVbo_position_teapot = 0;
	}

	if (gVbo_normals_teapot)
	{
		glDeleteBuffers(1, &gVbo_normals_teapot);
		gVbo_normals_teapot = 0;
	}

	if (gVbo_texture_teapot)
	{
		glDeleteBuffers(1, &gVbo_texture_teapot);
		gVbo_texture_teapot = 0;
	}

	// deatch vertex shader from shader program object
	glDetachShader(gShaderProgramObjectTeapot, gVertexShaderObjectTeapot);

	// detach fragment shader from shader program obejct
	glDetachShader(gShaderProgramObjectTeapot, gFragmentShaderObjectTeapot);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObjectTeapot);

	gVertexShaderObjectTeapot = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObjectTeapot);
	gFragmentShaderObjectTeapot = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObjectTeapot);
	gShaderProgramObjectTeapot = 0;

	// unlink shader program
	glUseProgram(0);

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile) {
		fprintf(gpFile, "\nClosing File Successfully");
		fclose(gpFile);
	}
}
