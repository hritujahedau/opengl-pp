// header file declaration
#include <windows.h>
#include<stdio.h>

#include <GL\glew.h>
#include <gl/GL.h>
#include "OGL.h"
#include "vmath.h"

#pragma comment(lib, "C:\\glew-2.1.0\\lib\\Release\\Win32\\glew32.lib")
#pragma comment(lib, "opengl32.lib")

#define WIDTH 1000
#define HIGHT 800

using namespace vmath;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int width, hight;


enum
{
	HRH_ATTRIBUTE_POSITION = 0 ,
	HRH_ATTRIBUTE_COLOR ,
	HRH_ATTRIBUTE_NORMAL ,
	HRH_ATTRIBUTE_TEXTURE_0
};

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao_graph;
GLuint gVbo_graph_position;
GLuint gVbo_graph_color;

GLuint gMVPUniform;

GLfloat graphColor[] = { 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat tringleColor[] = { 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f,1.0f, 1.0f, 0.0f, };
GLfloat vertices[] = { 1.0f, 0.0, 0.0f, -1.0f, 0.0f, 0.0f };

typedef struct __POINTF
{
	GLfloat x, y;
}POINTF;

mat4 gPerspectiveProjectionMatrix;

POINTF a, b, c, d, e;

GLfloat radius = 1.0f;

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	void Initialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("h_ogl");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 

	if (fopen_s(&gpFile, "windows.log", "w") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("OGL"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HIGHT/2 ,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow) {
				Display();
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	void Resize(int,int);

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int,int);
	void Uninitialize();

	// Function declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}
	
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		DestroyWindow(ghwnd);
	}

	//OpenGL Logs
	fprintf(gpFile, "OpenGL Logs");
	fprintf(gpFile, "OpenGL Vendor: %s \n ", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer: %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "OpenGL Shading Language : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	fprintf(gpFile, "Open Enabled extension\n");
	GLint numExtension = 0;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	fprintf(gpFile, "Open Enabled total extension %d \n", numExtension);

	for (int i = 0; i < numExtension; i++)
	{		
		//fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	/********** VERTEX SHADER ***********/
	// CREATE SHADER

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	
	//provide souce code to shader
	const GLchar* vertexShaderSourceCode =  "#version 450 core\n"\
											"in vec4 vPosition;" \
											"in vec4 vColor;" \
											"out vec4 out_color; " \
											"uniform mat4 u_mvpMatrix;" \
											"void main()"\
											"{" \
											"out_color = vColor;" \
											"gl_Position = u_mvpMatrix * vPosition;"\
											"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCode = "#version 450 core\n"\
											"in vec4 out_color;" \
											"out vec4 FragColor; " \
											"void main()"\
											"{"\
											"FragColor = out_color;" \
											"}";
	

	glShaderSource(gFragmentShaderObject, 1, (const char**)&fragmentShaderSourceCode, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_COLOR, "vColor");

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObject);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// get MVP uniform location
	gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvpMatrix");
	
	glGenVertexArrays(1, &gVao_graph);
	glBindVertexArray(gVao_graph);

	/****
	If we don't use gVao need to write #1, #2, #3 function in display again.
	****/

	/***********************************gVao*********************************************************/

	// position
	glGenBuffers(1, &gVbo_graph_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position); //Bind gVbo
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), NULL, GL_DYNAMIC_DRAW); // feed the data #1	
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);	// kontya variable sathi, kiti elements, type #2	
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION); // #3
	glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo

	// color
	glGenBuffers(1, &gVbo_graph_color); // Bind gVbo_color 
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(graphColor), graphColor, GL_DYNAMIC_DRAW); // #1
	glVertexAttribPointer(HRH_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL); // #2
	glEnableVertexAttribArray(HRH_ATTRIBUTE_COLOR); // #3
	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind Bind gVbo_color
	
	// unbind gVao
	glBindVertexArray(0); // unbind for gVao

	/***************************************************************************************************************/
	// set-up depth buffers
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // don't work

	// we will always cull face for better performance
	glEnable(GL_CULL_FACE);

	gPerspectiveProjectionMatrix = mat4::identity();

	Resize(WIDTH, HIGHT);
}

void Resize(int width, int height) {
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) height);

	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); // no 0.1f

}

void Display() {
	// FUNCTION DECLARATION
	void graph();
	void hallowRectangle();
	void hallowBigCircle();
	void hallowSmallCircle();
	void hallowTriangle();

	// CODE
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// START USING OpenGL program object
	
	glUseProgram(gShaderProgramObject);

	//OpenGL Drawing For Trianlge
	// set modelview & modelviewProjection matrices to indentity
	mat4 translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	mat4 modelViewMatrix = translateMatrix;
	mat4 modelViewProjectionMatrix = mat4::identity();
	

	// multiply the modelview and orthographic matrix to get modelviewProjectyion matrix
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix; // ORDER IS IMPORTANT

	// pass above modelviewprojection matrix to vertex shader 'u_mvpMatrix' shader variable
	// whose value we already have calculated in inititialization() by uisng glGetUniformLocation()
	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	graph();
	hallowBigCircle();
	hallowRectangle();
	hallowTriangle();
	hallowSmallCircle();
	
	// stop using OpenGL program object
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void hallowTriangle()
{	
	a.x = radius * (cos(35.0f * 0.01745329));
	a.y = radius * (sin(35.0f * 0.01745329));

	e.x = (a.x + b.x) / 2;
	e.y = (a.y + b.y) / 2;

	c.x = radius * (cos(215.0f * 0.01745329));
	c.y = radius * (sin(215.0f * 0.01745329));

	d.x = radius * (cos(325.0f * 0.01745329));
	d.y = radius * (sin(325.0f * 0.01745329));

	GLfloat hallowTriangle[] =
	{
		e.x, e.y, 0.0f,
		c.x, c.y, 0.0f,
		d.x, d.y, 0.0f,
		e.x, e.y, 0.0f,
	};

	glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(tringleColor), tringleColor, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(hallowTriangle), hallowTriangle, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(gVao_graph);
	glDrawArrays(GL_LINES, 0, 2);
	glDrawArrays(GL_LINES, 1, 2);
	glDrawArrays(GL_LINES, 2, 2);
	glDrawArrays(GL_LINES, 3, 2);
	glBindVertexArray(0);
}


void hallowSmallCircle()
{
	// function declarations
	GLfloat getLengthFromVertises(GLfloat, GLfloat, GLfloat, GLfloat);

	// variable declarations
	GLfloat circleLines[] =
	{
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f
	};
	GLfloat circleColor[] =
	{
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f
	};

	GLfloat circle_points = 360, angle;

	// CODE	
	GLfloat s = 0.0f, length_ec = 0.0f, length_cd = 0.0f, length_de = 0.0f;
	GLfloat hallowSmallCircleRadius = 0.0f;
	length_ec = getLengthFromVertises(e.x, e.y, c.x, c.y);
	length_cd = getLengthFromVertises(c.x, c.y, d.x, d.y);
	length_de = getLengthFromVertises(d.x, d.y, e.x, e.y);

	s = (length_ec + length_cd + length_de) / 2;
	hallowSmallCircleRadius = sqrt(s * (s - length_ec) * (s - length_cd) * (s - length_de)) / s;

	for (int i = 1; i < circle_points; i++)
	{
		angle = (i - 1) * 0.01745329;
		circleLines[0] = hallowSmallCircleRadius * cos(angle);
		circleLines[1] = -0.15f + hallowSmallCircleRadius * sin(angle);
		circleLines[2] = 0.0f;
		angle = i * 0.01745329;
		circleLines[3] = hallowSmallCircleRadius * cos(angle);
		circleLines[4] = -0.15f + hallowSmallCircleRadius * sin(angle);
		circleLines[5] = 0.0f;

		glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_color);
		glBufferData(GL_ARRAY_BUFFER, sizeof(circleColor), circleColor, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
		glBufferData(GL_ARRAY_BUFFER, sizeof(circleLines), circleLines, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(gVao_graph);
		glDrawArrays(GL_LINES, 0, 2);
		glBindVertexArray(0);
	}
}

GLfloat getLengthFromVertises(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2)
{
	// eqaution for calculating legth between 2 points
	return sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
}

void hallowBigCircle()
{
	// variable declarations
	GLfloat circleLines[] = 
	{
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f
	};
	GLfloat circleColor[] = 
	{
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f
	};

	GLfloat circle_points = 360, angle;

	// CODE

	for (int i = 1; i < circle_points; i++) 
	{
		angle = (i - 1) * 0.01745329;
		circleLines[0] = radius * cos(angle);
		circleLines[1] = radius * sin(angle);
		circleLines[2] = 0.0f;
		angle = i * 0.01745329;
		circleLines[3] = radius * cos(angle);
		circleLines[4] = radius * sin(angle);
		circleLines[5] = 0.0f;

		glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_color);
		glBufferData(GL_ARRAY_BUFFER, sizeof(circleColor), circleColor, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
		glBufferData(GL_ARRAY_BUFFER, sizeof(circleLines), circleLines, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(gVao_graph);
		glDrawArrays(GL_LINES, 0, 2);
		glBindVertexArray(0);
	}
}

void hallowRectangle()
{
	// VARIABLE DECLARATION

	a.x = radius * (cos(35.0f * 0.01745329));
	a.y = radius * (sin(35.0f * 0.01745329));

	b.x = radius * (cos(145.0f * 0.01745329));
	b.y = radius * (sin(145.0f * 0.01745329));

	c.x = radius * (cos(215.0f * 0.01745329));
	c.y = radius * (sin(215.0f * 0.01745329));

	d.x = radius * (cos(325.0f * 0.01745329));
	d.y = radius * (sin(325.0f * 0.01745329));

	GLfloat hallowRectangle[] = 
	{ 
		a.x, a.y, 0.0f,
		b.x, b.y, 0.0f,
		c.x, c.y, 0.0f,
		d.x, d.y, 0.0f,
		a.x, a.y, 0.0f,
	};

	// CODE

	glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(tringleColor), tringleColor, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(hallowRectangle), hallowRectangle, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(gVao_graph);
	glDrawArrays(GL_LINES, 0, 2);
	glDrawArrays(GL_LINES, 1, 2);
	glDrawArrays(GL_LINES, 2, 2);
	glDrawArrays(GL_LINES, 3, 2);
	glBindVertexArray(0);

}

void graph()
{
	GLfloat distanceForX = 0, distanceForY = 0;
	GLfloat x = 3.0f, y = 3.0f;

	graphColor[0] = 0.0f;
	graphColor[1] = 0.0f;
	graphColor[2] = 1.0f;
	graphColor[3] = 0.0f;
	graphColor[4] = 0.0f;
	graphColor[5] = 1.0f;

	glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(graphColor), graphColor, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	for (int i = 0; i < 60; i++) {

		// verticle up
		vertices[0] = x;
		vertices[1] = distanceForX;
		vertices[2] = 0.0f;

		vertices[3] = -x;
		vertices[4] = distanceForX;
		vertices[5] = 0.0f;

		glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(gVao_graph);
		glDrawArrays(GL_LINES, 0, 2);
		glBindVertexArray(0);

		// verticle down
		vertices[0] = x;
		vertices[1] = -distanceForX;
		vertices[2] = 0.0f;

		vertices[3] = -x;
		vertices[4] = -distanceForX;
		vertices[5] = 0.0f;

		glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(gVao_graph);
		glDrawArrays(GL_LINES, 0, 2);
		glBindVertexArray(0);

		// horizontal right
		vertices[0] = -distanceForY;
		vertices[1] = y;
		vertices[2] = 0.0f;

		vertices[3] = -distanceForY;
		vertices[4] = -y;
		vertices[5] = 0.0f;

		glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(gVao_graph);
		glDrawArrays(GL_LINES, 0, 2);
		glBindVertexArray(0);

		// horizontal left
		vertices[0] = distanceForY;
		vertices[1] = y;
		vertices[2] = 0.0f;

		vertices[3] = distanceForY;
		vertices[4] = -y;
		vertices[5] = 0.0f;

		glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(gVao_graph);
		glDrawArrays(GL_LINES, 0, 2);
		glBindVertexArray(0);


		distanceForX += (GLfloat)1 / 15; // 46
		distanceForY += (GLfloat)1 / 10; // 25
	}

	graphColor[0] = 0.0f;
	graphColor[1] = 1.0f;
	graphColor[2] = 0.0f;
	graphColor[3] = 0.0f;
	graphColor[4] = 1.0f;
	graphColor[5] = 0.0f;

	glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(graphColor), graphColor, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// horizontal green line
	vertices[0] = x;
	vertices[1] = 0.0f;
	vertices[2] = 0.0f;

	vertices[3] = -x;
	vertices[4] = 0.0f;
	vertices[5] = 0.0f;

	glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(gVao_graph);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	// verticle green line
	vertices[0] = 0.0f;
	vertices[1] = y;
	vertices[2] = 0.0f;

	vertices[3] = 0.0f;
	vertices[4] = -y;
	vertices[5] = 0.0f;

	glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(gVao_graph);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

}

void Uninitialize() {

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	// destroy gVao_hallowTriangle
	if (gVao_graph)
	{
		glDeleteVertexArrays(1, &gVao_graph);
		gVao_graph = 0;
	}

	if (gVbo_graph_color)
	{
		glDeleteBuffers(1, &gVbo_graph_color);
		gVbo_graph_color = 0;
	}

	if (gVbo_graph_position)
	{
		glDeleteBuffers(1, &gVbo_graph_position);
		gVbo_graph_position = 0;
	}

	// deatch vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	// detach fragment shader from shader program obejct
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject);

	gVertexShaderObject = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	// unlink shader program
	glUseProgram(0);

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile) {
		fprintf(gpFile, "\n Closing File Successfully");
		fclose(gpFile);
	}
}
