// header file declaration
// envmapsphere_d
#include <windows.h>
#include<stdio.h>
#include <memory.h>

#include <GL\glew.h>
#include <gl/GL.h>
#include "OGL.h"
#include "vmath.h"

#pragma comment(lib, "C:\\glew-2.1.0\\lib\\Release\\Win32\\glew32.lib")
#pragma comment(lib, "opengl32.lib")

#define WIDTH 1000
#define HIGHT 800
#define MAX_SCENE_WIDTH  2048
#define MAX_SCENE_HEIGHT  2048
#define SPHERE_COUNT 32

#define SB6M_FOURCC(a,b,c,d)            ( ((unsigned int)(a) << 0) | ((unsigned int)(b) << 8) | ((unsigned int)(c) << 16) | ((unsigned int)(d) << 24) )
#define SB6M_VERTEX_ATTRIB_FLAG_NORMALIZED      0x00000001

using namespace vmath;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int width, hight;

int mode = 1;

int w, h;

enum
{
	HRH_ATTRIBUTE_POSITION = 0 ,
	HRH_ATTRIBUTE_NORMAL,
	HRH_ATTRIBUTE_COLOR ,	
	HRH_ATTRIBUTE_TEXTURE_0
};

typedef enum SB6M_CHUNK_TYPE_t
{
	SB6M_CHUNK_TYPE_INDEX_DATA = SB6M_FOURCC('I', 'N', 'D', 'X'),
	SB6M_CHUNK_TYPE_VERTEX_DATA = SB6M_FOURCC('V', 'R', 'T', 'X'),
	SB6M_CHUNK_TYPE_VERTEX_ATTRIBS = SB6M_FOURCC('A', 'T', 'R', 'B'),
	SB6M_CHUNK_TYPE_SUB_OBJECT_LIST = SB6M_FOURCC('O', 'L', 'S', 'T'),
	SB6M_CHUNK_TYPE_COMMENT = SB6M_FOURCC('C', 'M', 'N', 'T'),
	SB6M_CHUNK_TYPE_DATA = SB6M_FOURCC('D', 'A', 'T', 'A')
} SB6M_CHUNK_TYPE;

GLuint gVertexShaderObject_render;
GLuint gFragmentShaderObject_render;
GLuint gProgramObject_render;

GLuint gVertexShaderObject_filter;
GLuint gFragmentShaderObject_filter;
GLuint gProgramObject_filter;

GLuint gVertexShaderObject_resolve;
GLuint gFragmentShaderObject_resolve;
GLuint gProgramObject_resolve;

GLuint gVao;
GLuint gVao_sqaure;
GLuint data_buffer;
GLuint ubo_transform;
GLuint ubo_material;

GLuint render_fbo;

GLuint filter_fbo[2];
GLuint tex_filter[2];

GLuint gMVUniform;
GLuint gProjUniform;
GLuint gSampler2D;

GLuint g_bloom_thresh_min_uniform;
GLuint g_bloom_thresh_max_uniform;
GLuint g_bloom_factor_uniform, g_scene_factor_uniform;
GLuint g_exposure_uniform, g_light_pos_uniform;
GLuint g_horizontal_uniform;

GLuint gUniformExposure;

mat4 gPerspectiveProjectionMatrix;

GLuint tex_scene, tex_brightpass, tex_depth , tex_lut;

SB6M_CHUNK_HEADER* chunk_vertex_attribs;
SB6M_CHUNK_HEADER* chunk_vertex_data;
SB6M_CHUNK_HEADER* chunk_sub_object_list;

SB6M_VERTEX_ATTRIB_CHUNK* vertex_attrib_chunk;
SB6M_CHUNK_VERTEX_DATA* vertex_data_chunk;

bool show_bloom = false, show_scene = true, show_prefilter = false, paused = false;
float bloom_thresh_min = 0.8f, bloom_thresh_max = 1.2f;
float directional_light[] = {100.0f, 100.0f, 100.0f};
GLfloat exposure = 1.0f;
GLfloat bloom_factor = 1.0f ;

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	void Initialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("h_ogl");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 

	if (fopen_s(&gpFile, "windows.log", "w") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL SuperBible - HDR Bloom"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HIGHT/2 ,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			Display();
			if (gbActiveWindow) {
				
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Resize(int,int);
	void Uninitialize();

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			w = LOWORD(lParam);
			h = HIWORD(lParam);
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_CHAR:
			switch (wParam)
			{
			case '1':
				mode = 1;
				break;
			case '2':
				mode = 2;
				break;
			case '3':
				mode = 3;
				break;
			case 'h':
				exposure = exposure * 1.1f;
				break;
			case 'H':
				exposure = exposure / 1.1f;
				break;
			case 'b':
			case 'B':
				show_bloom = !show_bloom;
				break;
			case 'v':
			case 'V':
				show_scene = !show_scene;
			case 'A':
			case 'a':
				bloom_factor = bloom_factor + 0.1f;
				break;
			case 'Z':
			case 'z':
				bloom_factor = bloom_factor - 0.1f;
				break;
			case 's':
			case 'S':
				bloom_thresh_min = bloom_thresh_min + 0.1f;
				break;
			case 'x':
			case 'X':
				bloom_thresh_min = bloom_thresh_min - 0.1f;
				break;
			case 'D':
			case 'd':
				bloom_thresh_max = bloom_thresh_max + 0.1f;
				break;
			case 'c':
			case 'C':
				bloom_thresh_max = bloom_thresh_max - 0.1f;
				break;
			case 'N':
			case 'n':
				show_prefilter = !show_prefilter;
				break;
			case 'p':
			case 'P':
				paused = !paused;
				break;
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int, int);
	void LoadTexture(const char * filename, unsigned int* tex);
	void horizontalLine();
	void loadShader();
	void loadObject(const char* filename);
	void Uninitialize();

	// Function declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		DestroyWindow(ghwnd);
	}

	//OpenGL Logs
	fprintf(gpFile, "OpenGL Logs");
	fprintf(gpFile, "OpenGL Vendor: %s \n ", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer: %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "OpenGL Shading Language : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	fprintf(gpFile, "Open Enabled extension\n");
	GLint numExtension = 0;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	fprintf(gpFile, "Open Enabled total extension %d \n", numExtension);

	for (int i = 0; i < numExtension; i++)
	{
		//fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}


	
	loadShader();

	// load object 

	glGenVertexArrays(1, &gVao_sqaure);
	glBindVertexArray(gVao_sqaure);
	glBindVertexArray(0);

	glGenFramebuffers(1, &render_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, render_fbo);

	glGenTextures(1, &tex_scene);
	glBindTexture(GL_TEXTURE_2D, tex_scene);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA16F, MAX_SCENE_WIDTH, MAX_SCENE_HEIGHT);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, tex_scene, 0);

	glGenTextures(1, &tex_brightpass);
	glBindTexture(GL_TEXTURE_2D, tex_brightpass);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA16F, MAX_SCENE_WIDTH, MAX_SCENE_HEIGHT);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, tex_brightpass, 0);

	glGenTextures(1, &tex_depth);
	glBindTexture(GL_TEXTURE_2D, tex_depth);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT32F, MAX_SCENE_WIDTH, MAX_SCENE_HEIGHT);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, tex_depth, 0);

	static const GLenum buffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };

	glDrawBuffers(2, buffers);
	glGenFramebuffers(2, &filter_fbo[0]);
	glGenTextures(2, &tex_filter[0]);
	for (int i = 0; i < 2; i++)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, filter_fbo[i]);
		glBindTexture(GL_TEXTURE_2D, tex_filter[i]);
		glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA16F, i ? MAX_SCENE_WIDTH : MAX_SCENE_HEIGHT, i ? MAX_SCENE_HEIGHT : MAX_SCENE_WIDTH);
		//glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA16F,  MAX_SCENE_WIDTH , MAX_SCENE_HEIGHT );
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, tex_filter[i], 0);
		glDrawBuffers(1, buffers);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	static const GLfloat exposureLUT[20] = { 11.0f, 6.0f, 3.2f, 2.8f, 2.2f, 1.90f, 1.80f, 1.80f, 1.70f, 1.70f,  1.60f, 1.60f, 1.50f, 1.50f, 1.40f, 1.40f, 1.30f, 1.20f, 1.10f, 1.00f };

	glGenTextures(1, &tex_lut);
	glBindTexture(GL_TEXTURE_1D, tex_lut);
	glTexStorage1D(GL_TEXTURE_1D, 1, GL_R32F, 20);
	glTexSubImage1D(GL_TEXTURE_1D, 0, 0, 20, GL_RED, GL_FLOAT, exposureLUT);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);

	loadObject("./objects/torus.sbm");

	glGenBuffers(1, &ubo_transform);
	glBindBuffer(GL_UNIFORM_BUFFER, ubo_transform);
	glBufferData(GL_UNIFORM_BUFFER, (2 + SPHERE_COUNT) * sizeof(vmath::mat4), NULL, GL_DYNAMIC_DRAW);

	struct material
	{
		vmath::vec3     diffuse_color;
		unsigned int : 32;           // padding
		vmath::vec3     specular_color;
		float           specular_power;
		vmath::vec3     ambient_color;
		unsigned int : 32;           // padding
	};

	glGenBuffers(1, &ubo_material);
	glBindBuffer(GL_UNIFORM_BUFFER, ubo_material);
	glBufferData(GL_UNIFORM_BUFFER, SPHERE_COUNT * sizeof(material), NULL, GL_DYNAMIC_DRAW);

	material* m = (material*)glMapBufferRange(GL_UNIFORM_BUFFER, 0, SPHERE_COUNT * sizeof(material), GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);;
	float ambient = 0.002f;
	for (int i = 0; i < SPHERE_COUNT; i++)
	{
		float fi = 3.14159267f * (float)i / 0.8f;
		m[i].diffuse_color = vmath::vec3(sinf(fi) * 0.5f + 0.5f, sinf(fi + 1.345f) * 0.5f + 0.5f, sinf(fi + 2.567f) * 0.5f + 0.5f);
		m[i].specular_color = vmath::vec3(2.8f, 2.8f, 2.9f);
		m[i].specular_power = 30.0f;
		m[i].ambient_color = vmath::vec3(ambient * 0.025f);
		ambient *= 1.5f;
	}
	glUnmapBuffer(GL_UNIFORM_BUFFER);

	//gPerspectiveProjectionMatrix = mat4::identity();

	Resize(WIDTH, HIGHT);
	w = WIDTH;
	h = HIGHT;
}

void loadObject(const char* filename)
{
	// FUNCTION DECLARATIONS
	void Uninitialize();

	// create vao
	char* data;
	FILE* infile;
	size_t file_size;

	infile = fopen(filename, "rb");

	if (infile == NULL)
	{
		fprintf(gpFile, "\nFile for object not able to open");
		Uninitialize();
		DestroyWindow(ghwnd);
	}

	fprintf(gpFile, "\nFile for object open successafully");

	fseek(infile, 0, SEEK_END);
	file_size = ftell(infile);
	fseek(infile, 0, SEEK_SET);

	data = (char*)malloc(sizeof(char) * file_size);

	if (fread(data, file_size, 1, infile) == 0)
	{
		fprintf(gpFile, "\nNot able to read Object file data read");
		Uninitialize();
		DestroyWindow(ghwnd);
	}
	fprintf(gpFile, "\nObject file data read successully");

	char* ptr = data;
	SB6M_HEADER* header = (SB6M_HEADER*)ptr;
	ptr = ptr + header->size;

	chunk_vertex_attribs = (SB6M_CHUNK_HEADER*)ptr;
	ptr = ptr + chunk_vertex_attribs->size;
	chunk_vertex_data = (SB6M_CHUNK_HEADER*)ptr;
	ptr = ptr + chunk_vertex_data->size;


	fprintf(gpFile, "\n\n");

	fprintf(gpFile, "\nchunk_vertex_attribs->chunk_type = %d", chunk_vertex_attribs->chunk_type);
	fprintf(gpFile, "\nchunk_vertex_data->chunk_type = %d", chunk_vertex_data->chunk_type);

	fprintf(gpFile, "\n\n");

	fprintf(gpFile, "\nheader->num_chunks = %d", header->num_chunks);
	fprintf(gpFile, "\nSB6M_CHUNK_TYPE_DATA = %d ", SB6M_CHUNK_TYPE_DATA);
	fprintf(gpFile, "\nSB6M_CHUNK_TYPE_VERTEX_ATTRIBS = %d ", SB6M_CHUNK_TYPE_VERTEX_ATTRIBS);
	fprintf(gpFile, "\nSB6M_CHUNK_TYPE_VERTEX_DATA = %d ", SB6M_CHUNK_TYPE_VERTEX_DATA);
	fprintf(gpFile, "\nSB6M_CHUNK_TYPE_INDEX_DATA = %d ", SB6M_CHUNK_TYPE_INDEX_DATA);
	fprintf(gpFile, "\nSB6M_CHUNK_TYPE_SUB_OBJECT_LIST = %d ", SB6M_CHUNK_TYPE_SUB_OBJECT_LIST);

	// copy vertex attrib chunck
	vertex_attrib_chunk = (SB6M_VERTEX_ATTRIB_CHUNK*)chunk_vertex_attribs;
	vertex_data_chunk = (SB6M_CHUNK_VERTEX_DATA*)chunk_vertex_data;


	glGenVertexArrays(1, &gVao);
	glBindVertexArray(gVao);

	glGenBuffers(1, &data_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, data_buffer);
	glBufferData(GL_ARRAY_BUFFER, vertex_data_chunk->data_size, NULL, GL_STATIC_DRAW);

	glBufferSubData(GL_ARRAY_BUFFER, 0, vertex_data_chunk->data_size, data + vertex_data_chunk->data_offset);

	fprintf(gpFile, "\nvertex_attrib_chunk->attrib_count = %d", vertex_attrib_chunk->attrib_count);

	SB6M_VERTEX_ATTRIB_DECL& attrib_decl = vertex_attrib_chunk->attrib_data[0];
	glVertexAttribPointer(0,
		attrib_decl.size,
		attrib_decl.type,
		attrib_decl.flags & SB6M_VERTEX_ATTRIB_FLAG_NORMALIZED ? GL_TRUE : GL_FALSE,
		attrib_decl.stride,
		(GLvoid*)(uintptr_t)attrib_decl.data_offset);
	glEnableVertexAttribArray(0);

	attrib_decl = vertex_attrib_chunk->attrib_data[1];
	glVertexAttribPointer(1,
		attrib_decl.size,
		attrib_decl.type,
		attrib_decl.flags & SB6M_VERTEX_ATTRIB_FLAG_NORMALIZED ? GL_TRUE : GL_FALSE,
		attrib_decl.stride,
		(GLvoid*)(uintptr_t)attrib_decl.data_offset);
	glEnableVertexAttribArray(1);

	attrib_decl = vertex_attrib_chunk->attrib_data[2];
	glVertexAttribPointer(2,
		attrib_decl.size,
		attrib_decl.type,
		attrib_decl.flags & SB6M_VERTEX_ATTRIB_FLAG_NORMALIZED ? GL_TRUE : GL_FALSE,
		attrib_decl.stride,
		(GLvoid*)(uintptr_t)attrib_decl.data_offset);
	glEnableVertexAttribArray(2);


}

void loadShader()
{
	// Render

	gVertexShaderObject_render = glCreateShader(GL_VERTEX_SHADER);

	//provide souce code to shader
	const GLchar* vertexShaderSourceCode_render = "#version 450 core\n					" \
		"layout (location = 0) in vec4 position;										" \
		"layout (location = 1) in vec3 normal;											" \
		"out VS_OUT																		" \
		"{																				" \
		"	vec3 N;																		" \
		"	vec3 L;																		" \
		"	vec3 V;																		" \
		"	flat int material_index;													" \
		"} vs_out;																		" \
		"uniform vec3 light_pos;														" \
		"layout (binding = 0, std140) uniform TRANSFORM_BLOCK							" \
		"{																				" \
		"	mat4 mat_proj;																" \
		"	mat4 mat_view;																" \
		"	mat4 mat_model[32];															" \
		"} transforms;																	" \
		"void main()																	" \
		"{																				" \
		"	mat4 mat_mv = transforms.mat_view * transforms.mat_model[gl_InstanceID];	" \
		"	vec4 P = mat_mv * position ;												" \
		"	vs_out.N = mat3(mat_mv) * normal;											" \
		"	vs_out.L = light_pos - P.xyz;												" \
		"	vs_out.V = -P.xyz;															" \
		"	vs_out.material_index = gl_InstanceID;										" \
		"	gl_Position = transforms.mat_proj * P;										" \
		"}";

	glShaderSource(gVertexShaderObject_render, 1, (const GLchar**)&vertexShaderSourceCode_render, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject_render);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject_render, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_render, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_render, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// CREATE SHADER
	gFragmentShaderObject_render = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCode_render = "#version 450 core\n																	" \
		"layout (location = 0) out vec4 color0;														" \
		"layout (location = 1) out vec4 color1;														" \
		"in VS_OUT																					" \
		"{																							" \
		"	vec3 N;																					" \
		"	vec3 L;																					" \
		"	vec3 V;																					" \
		"	flat int material_index;																" \
		"} fs_in;																					" \
		"uniform float bloom_thresh_min = 0.8;														" \
		"uniform float bloom_thresh_max = 1.2;														" \
		"struct material_t																			" \
		"{																							" \
		"	vec3 diffuse_color;																		" \
		"	vec3 specular_color;																	" \
		"	float specular_power;																	" \
		"	vec3 ambient_color;																		" \
		"};																							" \
		"layout (binding = 1, std140) uniform MATERIAL_BLOCK										" \
		"{																							" \
		"	material_t material[32];																" \
		"} materials;																				" \
		"void main()																				" \
		"{																							" \
		"	vec3 N = normalize(fs_in.N);															" \
		"	vec3 L = normalize(fs_in.L);															" \
		"	vec3 V = normalize(fs_in.V);															" \
		"	vec3 R = reflect(-L, N);																" \
		"	material_t m = materials.material[fs_in.material_index];								" \
		"	vec3 diffuse = max( dot (N, L), 0.0) * m.diffuse_color;									" \
		"	vec3 specular = pow (max (dot (R, V), 0.0) , m.specular_power) * m.specular_color;		" \
		"	vec3 ambient = m.ambient_color ;														" \
		"	vec3 color = ambient + diffuse + specular ; 											" \
		"	color0 = vec4 (color, 1.0);																" \
		"	float Y = dot(color, vec3(0.299, 0.587, 0.144));										" \
		"	color = color * 4.0 * smoothstep (bloom_thresh_min, bloom_thresh_max, Y);				" \
		"	color1 = vec4(color, 1.0);																" \
		"}";


	glShaderSource(gFragmentShaderObject_render, 1, (const char**)&fragmentShaderSourceCode_render, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject_render);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObject_render, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_render, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_render, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gProgramObject_render = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gProgramObject_render, gVertexShaderObject_render);

	// attch fragment shader to shader program
	glAttachShader(gProgramObject_render, gFragmentShaderObject_render);

	// LINK PROGRAM
	glLinkProgram(gProgramObject_render);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gProgramObject_render, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gProgramObject_render, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gProgramObject_render, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	g_bloom_thresh_min_uniform = glGetUniformLocation(gProgramObject_render, "bloom_thresh_min");
	g_bloom_thresh_max_uniform = glGetUniformLocation(gProgramObject_render, "bloom_thresh_max");
	g_light_pos_uniform = glGetUniformLocation(gProgramObject_render, "light_pos");

	// filter
	gVertexShaderObject_filter = glCreateShader(GL_VERTEX_SHADER);

	const char* vertexShaderSourceCode_filter = 
		"#version 450	core												\n"
		"void main(){														\n"\
		"float coord = 1.0;													" \
		"const vec4 vertices[] = vec4[](vec4(-coord, -coord, 0.5, 1.0),		" \
		"								vec4(coord, -coord, 0.5, 1.0),		" \
		"								vec4(-coord, coord, 0.5, 1.0),		" \
		"								vec4(coord, coord, 0.5, 1.0) );		" \
		"gl_Position = vertices[gl_VertexID];								" \
		"}";

	glShaderSource(gVertexShaderObject_filter, 1, (const GLchar**)&vertexShaderSourceCode_filter, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject_filter);

	// check compilation error
	iInfoLogLength = 0;
	iShaderCompileStatus = 0;
	szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject_filter, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_filter, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_filter, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	gFragmentShaderObject_filter = glCreateShader(GL_FRAGMENT_SHADER);

	const char* fragmentShaderSourceCode_filter = 
		"#version 450 core  \n					" \
		"layout (binding = 0) uniform sampler2D hdr_image;														" \
		"uniform int horizontal;																				" \
		"out vec4 color;																						" \
		"const float weights[] = float[](0.0024499299678342,   													" \
		"                                0.0043538453346397,   													" \
		"                                0.0073599963704157,   													" \
		"                                0.0118349786570722,   													" \
		"                                0.0181026699707781,   													" \
		"                                0.0263392293891488,   													" \
		"                                0.0364543006660986,   													" \
		"                                0.0479932050577658,   													" \
		"                                0.0601029809166942,   													" \
		"                                0.0715974486241365,   													" \
		"                                0.0811305381519717,   													" \
		"                                0.0874493212267511,   													" \
		"                                0.0896631113333857,   													" \
		"                                0.0874493212267511,   													" \
		"                                0.0811305381519717,   													" \
		"                                0.0715974486241365,   													" \
		"                                0.0601029809166942,   													" \
		"                                0.0479932050577658,   													" \
		"                                0.0364543006660986,   													" \
		"                                0.0263392293891488,   													" \
		"                                0.0181026699707781,   													" \
		"                                0.0118349786570722,   													" \
		"                                0.0073599963704157,   													" \
		"                                0.0043538453346397,   													" \
		"                                0.0024499299678342);  													" \
		"void main()																							" \
		"{																										" \
		"	vec4 c = vec4(0.0);																					" \
		"	int i;																								" \
		"	ivec2 P;																							" \
		"	if(horizontal == 0)																					" \
		"	{																									" \
		"		P = ivec2(gl_FragCoord.xy) - ivec2( 0, weights.length() >> 1);									" \
		"		for ( i = 0 ; i < weights.length(); i++)														" \
		"		{																								" \
		"			c = c + texelFetch(hdr_image, P + ivec2(0, i), 0) * weights[i];								" \
		"		}																								" \
		"	}																									" \
		"	else																								" \
		"	{																									" \
		"		P = ivec2(gl_FragCoord.xy) - ivec2( weights.length() >> 1, 0);									" \
		"		for ( i = 0 ; i < weights.length(); i++)														" \
		"		{																								" \
		"			c = c + texelFetch(hdr_image, P + ivec2(i, 0), 0) * weights[i];								" \
		"		}																								" \
		"	}																									" \
		"	color = c;																							" \
		"}																										" \
		"																										" ;

	glShaderSource(gFragmentShaderObject_filter, 1, (const char**)&fragmentShaderSourceCode_filter, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject_filter);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObject_filter, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_filter, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_filter, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	gProgramObject_filter = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gProgramObject_filter, gVertexShaderObject_filter);

	// attch fragment shader to shader program
	glAttachShader(gProgramObject_filter, gFragmentShaderObject_filter);

	// LINK PROGRAM
	glLinkProgram(gProgramObject_filter);

	// CHECK LINKING ERROR
	iShaderProgramLinkStatus = 0;
	glGetProgramiv(gProgramObject_filter, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gProgramObject_filter, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gProgramObject_filter, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	g_horizontal_uniform = glGetUniformLocation(gProgramObject_filter, "horizontal");

	// resolve

	gVertexShaderObject_resolve = glCreateShader(GL_VERTEX_SHADER);

	const char* vertexShaderSourceCode_resolve = 
		"#version 450	core												\n"
		"void main(){														\n"\
		"float coord = 1.0;													" \
		"const vec4 vertices[] = vec4[](vec4(-coord, -coord, 0.5, 1.0),		" \
		"								vec4(coord, -coord, 0.5, 1.0),		" \
		"								vec4(-coord, coord, 0.5, 1.0),		" \
		"								vec4(coord, coord, 0.5, 1.0) );		" \
		"gl_Position = vertices[gl_VertexID];								" \
		"}";

	glShaderSource(gVertexShaderObject_resolve, 1, (const GLchar**)&vertexShaderSourceCode_resolve, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject_resolve);

	// check compilation error
	iInfoLogLength = 0;
	iShaderCompileStatus = 0;
	szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject_resolve, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_resolve, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_resolve, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	gFragmentShaderObject_resolve = glCreateShader(GL_FRAGMENT_SHADER);

	const char* fragmentShaderSourceCode_resolve = "#version 450 core					\n	" \
		"layout (binding = 0) uniform sampler2D hdr_image;									" \
		"layout (binding = 1) uniform sampler2D bloom_image;								" \
		"uniform float exposure = 0.9;														" \
		"uniform float bloom_factor = 1.0;													" \
		"uniform float scene_factor = 1.0;													" \
		"out vec4 color;																	" \
		"void main()																		" \
		"{																					" \
		"	vec4 c = vec4(0.0);																" \
		"	c = c + texelFetch (hdr_image , ivec2(gl_FragCoord.xy), 0) * scene_factor;		" \
		"	c = c + texelFetch (bloom_image, ivec2(gl_FragCoord.xy), 0) * bloom_factor;		" \
		"	c.rgb = vec3(1.0) - exp(-c.rgb * exposure);										" \
		"	color = c;																		" \
		"}																					" \
		"";

	glShaderSource(gFragmentShaderObject_resolve, 1, (const char**)&fragmentShaderSourceCode_resolve, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject_resolve);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObject_resolve, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_resolve, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_resolve, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	gProgramObject_resolve = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gProgramObject_resolve, gVertexShaderObject_resolve);

	// attch fragment shader to shader program
	glAttachShader(gProgramObject_resolve, gFragmentShaderObject_resolve);

	// LINK PROGRAM
	glLinkProgram(gProgramObject_resolve);

	// CHECK LINKING ERROR
	iShaderProgramLinkStatus = 0;
	glGetProgramiv(gProgramObject_resolve, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gProgramObject_resolve, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gProgramObject_resolve, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	g_exposure_uniform = glGetUniformLocation(gProgramObject_resolve, "exposure");
	g_bloom_factor_uniform = glGetUniformLocation(gProgramObject_resolve, "bloom_factor");
	g_scene_factor_uniform = glGetUniformLocation(gProgramObject_resolve, "scene_factor");

}

void horizontalLine()
{
	fprintf(gpFile, "\n\n=======================================================================================\n\n");
}

void Resize(int width, int height) {
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) height);

	//gPerspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); 

}

void Display() {
	// CODE
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	static const GLfloat black[] = {0.0f, 0.0f, 0.0f, 1.0};
	static const GLfloat one = 1.0f;
	int i; 

	static float t = 0.0f;
	
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);

	glBindFramebuffer(GL_FRAMEBUFFER, render_fbo);
	glClearBufferfv(GL_COLOR, 0, black);
	glClearBufferfv(GL_COLOR, 1, black);
	glClearBufferfv(GL_DEPTH, 0, &one);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	
	glUseProgram(gProgramObject_render);

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, ubo_transform);
	struct transforms_t
	{
		mat4 mat_proj;
		mat4 mat_view;
		mat4 mat_model[SPHERE_COUNT];
	} *transforms = (transforms_t*)glMapBufferRange(GL_UNIFORM_BUFFER, 0, sizeof(transforms_t), GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
	transforms->mat_proj = vmath::perspective(50.0f, (float)w / (float)h, 1.0f, 1000.0f);
	transforms->mat_view = vmath::translate(0.0f, 0.0f, -20.0f);

	for (i = 0; i < SPHERE_COUNT; i++)
	{
		float fi = 3.141592f * (float)i / 16.0f;
		float r = (i & 2) ? 0.6f : 1.5f;
		transforms->mat_model[i] = vmath::translate(cosf(t + fi) * 5.0f * r, sinf(t + fi * 4.0f) * 4.0f, sinf(t + fi) * 5.0f * r) *
			vmath::rotate(sinf(t + fi * 2.13f) * 75.0f, cosf(t + fi * 1.37f) * 92.0f, 0.0f);
	}

	if (t > 360)
	{
		t = 0;
	}
	if (!paused)
	{
		t = t + 0.005f;
	}

	glUnmapBuffer(GL_UNIFORM_BUFFER);
	glBindBufferBase(GL_UNIFORM_BUFFER, 1, ubo_material);

	glUniform1f(g_bloom_thresh_min_uniform, bloom_thresh_min);
	glUniform1f(g_bloom_thresh_max_uniform, bloom_thresh_max);
	glUniform3fv(g_light_pos_uniform, 3, directional_light);

	// render torus
	glBindVertexArray(gVao);
	glDrawArraysInstancedBaseInstance(GL_TRIANGLES,
		0,
		vertex_data_chunk->total_vertices,
		SPHERE_COUNT,
		0);
	glBindVertexArray(0);

	glDisable(GL_DEPTH_TEST);
	
	// use filter program
	glUseProgram(gProgramObject_filter);

	// First Pass
	glUniform1i(g_horizontal_uniform, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, filter_fbo[0]);
	glBindTexture(GL_TEXTURE_2D, tex_brightpass);
	glBindVertexArray(gVao_sqaure);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	
	// Second pass
	glUniform1i(g_horizontal_uniform, 1);
	glBindFramebuffer(GL_FRAMEBUFFER, filter_fbo[1]);
	glBindTexture(GL_TEXTURE_2D, tex_filter[0]);
	glBindVertexArray(gVao_sqaure);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	// use resolve program
	glUseProgram(gProgramObject_resolve);

	glUniform1f(g_exposure_uniform, exposure);
	if (show_prefilter)
	{
		glUniform1f(g_bloom_factor_uniform, 0.0f);
		glUniform1f(g_scene_factor_uniform, 1.0f);
	}
	else
	{
		glUniform1f(g_bloom_factor_uniform, show_bloom ?  bloom_factor : 0.0f);
		glUniform1f(g_scene_factor_uniform, show_scene ?  1.0f : 0.0f);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, tex_filter[1]);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, show_prefilter ? tex_brightpass : tex_scene);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	
	SwapBuffers(ghdc);
}

void Uninitialize() {

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile) {
		fprintf(gpFile, "\n\nClosing File Successfully");
		fclose(gpFile);
	}
}
