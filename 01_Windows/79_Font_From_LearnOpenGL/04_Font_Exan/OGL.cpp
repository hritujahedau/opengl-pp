// header file declaration
#include <windows.h>
#include<stdio.h>
#include <stdlib.h>

#include <GL\glew.h>
#include <gl/GL.h>
#include "OGL.h"
#include "vmath.h"

// #include "ft2build.h"
// #include ".\\freetype\\freetype.h"

#include <ft2build.h>
#include <freetype/freetype.h>

#pragma comment(lib, "C:\\glew-2.1.0\\lib\\Release\\x64\\glew32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "freetype.lib")

#define WIDTH 1000
#define HIGHT 800

using namespace vmath;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int width, hight;

enum
{
	HRH_ATTRIBUTE_POSITION = 0 ,
	HRH_ATTRIBUTE_COLOR ,
	HRH_ATTRIBUTE_NORMAL ,
	HRH_ATTRIBUTE_TEXTURE_0
};

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao;
GLuint gVbo;

GLuint gProjectionUniform;
GLuint gTextUniform;
GLuint gTexColorUniform;

mat4 gOrthographicProjectionMatrix;

struct Character
{
	unsigned int TextureID;
	vec2 Size;
	vec2 Bearing;
	unsigned int Advance;
};

struct Character Characters[128];

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	void Initialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("h_ogl");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 

	if (fopen_s(&gpFile, "windows.log", "w") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("OGL"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HIGHT/2 ,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow) {
				Display();
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	void Resize(int,int);

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int,int);
	int LoadFreeTypeLibrary();
	void Uninitialize();

	// Function declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}
	
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		DestroyWindow(ghwnd);
	}

	//OpenGL Logs
	fprintf(gpFile, "OpenGL Logs");
	fprintf(gpFile, "OpenGL Vendor: %s \n ", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer: %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "OpenGL Shading Language : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	fprintf(gpFile, "Open Enabled extension\n");
	GLint numExtension = 0;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	fprintf(gpFile, "Open Enabled total extension %d \n", numExtension);

	for (int i = 0; i < numExtension; i++)
	{		
		//fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	/********** VERTEX SHADER ***********/
	// CREATE SHADER

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	
	//provide souce code to shader
	const GLchar* vertexShaderSourceCode =  
		" #version 450 core															\n " \
		" layout (location = 0) in vec4 vertex;										\n " \
		" out vec2 TexCoords;														\n " \
		" 																			\n " \
		" uniform mat4 projection;													\n " \
		" 																			\n " \
		" void main()																\n " \
		" {																			\n " \
		" 	gl_Position = projection * vec4 (vertex.xy, 0.0, 1.0);					\n " \
		" 	TexCoords = vertex.zw;													\n " \
		" }																			\n " ;

	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCode = 
		" #version 450 core																\n " \
		" in vec2 TexCoords;															\n " \
		" out vec4 color;																\n " \
		" 																				\n " \
		" uniform sampler2D text;														\n " \
		" uniform vec3 textColor;														\n " \
		" 																				\n " \
		" void main()																	\n " \
		" {																				\n " \
		" 	vec4 sampled = vec4(1.0, 1.0, 1.0, texture(text, TexCoords).r);				\n " \
		" 	color = vec4(textColor, 1.0) * sampled;										\n " \
		" }																				\n " ;	

	glShaderSource(gFragmentShaderObject, 1, (const char**)&fragmentShaderSourceCode, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_POSITION, "vertex");

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObject);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// get MVP uniform location
	gProjectionUniform = glGetUniformLocation(gShaderProgramObject, "projection");
	gTextUniform = glGetUniformLocation(gShaderProgramObject, "text");
	gTexColorUniform = glGetUniformLocation(gShaderProgramObject, "textColor");
	
	glGenVertexArrays(1, &gVao);
	glBindVertexArray(gVao);

	glGenBuffers(1, &gVbo);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 6 * 4, NULL, GL_DYNAMIC_DRAW);	
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	if (LoadFreeTypeLibrary() != 0)
	{
		fprintf(gpFile, "Failed To load LoadFreeTypeLibrary(), Exiting...");
		DestroyWindow(ghwnd);
	}

	// set-up depth buffers
	//glClearColor(0.2f, 0.3f, 0.3f, 1.0f);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	// we will always cull face for better performance
	

	gOrthographicProjectionMatrix = mat4::identity();

	Resize(WIDTH, HIGHT);
}

int LoadFreeTypeLibrary()
{
	FT_Library ft;

	if (FT_Init_FreeType(&ft))
	{
		fprintf(gpFile, "Could not init FreeType Library");
		return -1;
	}

	char font_name[] = "D:\\HRITUJA\\RTR\\CLASS_ASSIGNMENT\\hrituja_rtr2020\\02_PP\\79_Font_From_LearnOpenGL\\04_Font_Exan\\fonts\\Exan-Regular.ttf";

	FT_Face face;
	if (FT_New_Face(ft, font_name, 0, &face) != 0)
	{
		fprintf(gpFile, "Failed to load font");
		return -1;
	}

	FT_Set_Pixel_Sizes(face, 0, 20);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	int i = 0;

	for (unsigned char c = 0; c < 128; c++, i++)
	{
		if (FT_Load_Char(face, c, FT_LOAD_RENDER))
		{
			fprintf(gpFile, "Failed to load GLyph for %c ", c);
			continue;
		}

		unsigned int texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);

		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			GL_RED,
			face->glyph->bitmap.width,
			face->glyph->bitmap.rows,
			0,
			GL_RED,
			GL_UNSIGNED_BYTE,
			face->glyph->bitmap.buffer
			);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		// struct Character char = 
		// 						{
		// 							texture,
		// 							vec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
		// 							vec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
		// 							(unsigned int)(face->glyph->advance.x)
		// 						};
		
		Characters[i].TextureID = texture;
		Characters[i].Size = vec2(face->glyph->bitmap.width, face->glyph->bitmap.rows);
		Characters[i].Bearing = vec2(face->glyph->bitmap_left, face->glyph->bitmap_top);
		Characters[i].Advance = (unsigned int)(face->glyph->advance.x);
		
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	FT_Done_Face(face);
	FT_Done_FreeType(ft);

	return 0;
}

void Resize(int width, int height) {
	if (height == 0)
		height = 1;

	//width = 800;
	//height = 600;

	glViewport(0, 0, (GLsizei) width,(GLsizei) height);

	// glORtho(left, right, bottom, top, near, far);
	if (width <= height)
	{
		gOrthographicProjectionMatrix = ortho(-100.0f, 100.0f,
												(-100.0f * (height / width)),
												(100.0f * (height / width)),
												-100.0f,
												100.0f
											);
	}
	else
	{
		gOrthographicProjectionMatrix = ortho(-100.0f, 100.0f,
												(-100.0f * (width / height)),
												(100.0f *(width / height)),
												-100.0f,
												100.0f
											);
	}

}

void RenderText(char* text, int strLength, float x, float y, float scale, vec3 color)
{
	glUniform3fv(gTexColorUniform, 1, color);

	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(gVao);

	for (int i = 0; i < strLength; i++)
	{
		Character ch = Characters[(int)text[i]];

		float xpos = x + ch.Bearing[0] * scale;
		float ypos = y - (ch.Size[1] - ch.Bearing[1]) * scale;

		float w = ch.Size[0] * scale;
		float h = ch.Size[1] * scale;

		float vertices[6][4] =
							{
								{ xpos,		ypos + h, 0.0f, 0.0f },
								{ xpos,		ypos	, 0.0f, 1.0f },
								{ xpos + w, ypos	, 1.0f, 1.0f },

								{ xpos,		ypos + h, 0.0f, 0.0f },
								{ xpos + w,	ypos	, 1.0f, 1.0f },
								{ xpos + w,	ypos + h, 1.0f, 0.0f },
							};

		glBindTexture(GL_TEXTURE_2D, ch.TextureID);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glDrawArrays(GL_TRIANGLES, 0, 6);
		x += (ch.Advance >> 6) * scale;
	}

	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Display() 
{

	// function declarations
	void RenderText(char * text, int strLen, float x, float y, float scale, vec3 color);

	// CODE
	glClear(GL_COLOR_BUFFER_BIT);
	glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	glUseProgram(gShaderProgramObject);

	glUniformMatrix4fv(gProjectionUniform, 1, GL_FALSE, gOrthographicProjectionMatrix);

	char str1[] = "This is sample text";
	char str2[] = "(1c): From LearnOpenGL.com";

	RenderText(str1, strlen(str1), -100.0f, 25.0f, 0.5f, vec3(0.5f, 0.8f, 0.2f));
	RenderText(str2, strlen(str2), -100.0f, 0.0f, 0.3f, vec3(0.3f, 0.7f, 0.9f));

	glUseProgram(0);

	SwapBuffers(ghdc);
}

void Uninitialize() {

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	// destroy vao
	if (gVao)
	{
		glDeleteVertexArrays(1, &gVao);
		gVao = 0;
	}

	// destroy vbo
	if (gVbo)
	{
		glDeleteBuffers(1, &gVbo);
		gVbo = 0;
	}

	// deatch vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	// detach fragment shader from shader program obejct
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject);

	gVertexShaderObject = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	// unlink shader program
	glUseProgram(0);

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile) {
		fprintf(gpFile, "\n Closing File Successfully");
		fclose(gpFile);
	}
}
