// header file declaration
// envmapsphere_d
#include <windows.h>
#include<stdio.h>
#include <memory.h>

#include <GL\glew.h>
#include <gl/GL.h>
#include "OGL.h"
#include "vmath.h"
#include "TeaPot.h"

#pragma comment(lib, "C:\\glew-2.1.0\\lib\\Release\\Win32\\glew32.lib")
#pragma comment(lib, "opengl32.lib")

#define WIDTH 1000
#define HIGHT 800

#define SB6M_FOURCC(a,b,c,d)            ( ((unsigned int)(a) << 0) | ((unsigned int)(b) << 8) | ((unsigned int)(c) << 16) | ((unsigned int)(d) << 24) )
#define SB6M_VERTEX_ATTRIB_FLAG_NORMALIZED      0x00000001

using namespace vmath;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int width, hight;

enum
{
	HRH_ATTRIBUTE_POSITION = 0 ,
	HRH_ATTRIBUTE_NORMAL,
	HRH_ATTRIBUTE_COLOR ,	
	HRH_ATTRIBUTE_TEXTURE_0
};

typedef enum SB6M_CHUNK_TYPE_t
{
	SB6M_CHUNK_TYPE_INDEX_DATA = SB6M_FOURCC('I', 'N', 'D', 'X'),
	SB6M_CHUNK_TYPE_VERTEX_DATA = SB6M_FOURCC('V', 'R', 'T', 'X'),
	SB6M_CHUNK_TYPE_VERTEX_ATTRIBS = SB6M_FOURCC('A', 'T', 'R', 'B'),
	SB6M_CHUNK_TYPE_SUB_OBJECT_LIST = SB6M_FOURCC('O', 'L', 'S', 'T'),
	SB6M_CHUNK_TYPE_COMMENT = SB6M_FOURCC('C', 'M', 'N', 'T'),
	SB6M_CHUNK_TYPE_DATA = SB6M_FOURCC('D', 'A', 'T', 'A')
} SB6M_CHUNK_TYPE;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao_dragon;
GLuint data_buffer_dragon;

GLuint gVao_teapot;
GLuint gVbo_position_teapot;
GLuint gVbo_normals_teapot;
GLuint gVbo_texture_teapot;
GLuint gVbo_element_teapot;

GLfloat teapotVertices[141336];     
GLfloat teapotNormal[141336];       
GLfloat teapotTexcoord[94224];      
short teapotFaceIndices[15704 * 8]; 

GLuint gMVUniform;
GLuint gProjUniform;
GLuint gSampler2D;

mat4 gPerspectiveProjectionMatrix;

GLuint envmaps_0, envmaps_1, envmaps_2, texture ;

SB6M_CHUNK_HEADER* chunk_vertex_attribs;
SB6M_CHUNK_HEADER* chunk_vertex_data;

SB6M_VERTEX_ATTRIB_CHUNK* vertex_attrib_chunk;
SB6M_CHUNK_VERTEX_DATA* vertex_data_chunk;

int isDragon , isTeapot;

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	void Initialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("h_ogl");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 

	if (fopen_s(&gpFile, "windows.log", "w") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("OGL"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HIGHT/2 ,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow) {
				Display();
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	void Resize(int,int);

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_CHAR:
			switch (wParam)
			{
			case '1':
				texture = envmaps_0;
				break;
			case '2':
				texture = envmaps_1;
				break;
			case '3':
				texture = envmaps_2;
				break;
			case 'd':
			case 'D':
				if (isDragon)
				{
					isDragon = false;
				}
				else
				{
					isDragon = true;
					isTeapot = false;
				}
				break;
			case 't':
			case 'T':
				if (isTeapot)
				{
					isTeapot = false;
				}
				else
				{
					isTeapot = true;
					isDragon = false;
				}
				break;
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int, int);
	void LoadTexture(const char * filename, unsigned int* tex);
	void horizontalLine();
	void Uninitialize();

	// Function declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		DestroyWindow(ghwnd);
	}

	//OpenGL Logs
	fprintf(gpFile, "OpenGL Logs");
	fprintf(gpFile, "OpenGL Vendor: %s \n ", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer: %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "OpenGL Shading Language : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	fprintf(gpFile, "Open Enabled extension\n");
	GLint numExtension = 0;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	fprintf(gpFile, "Open Enabled total extension %d \n", numExtension);

	for (int i = 0; i < numExtension; i++)
	{
		//fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	/********** VERTEX SHADER ***********/
	// CREATE SHADER

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//provide souce code to shader
	const GLchar* vertexShaderSourceCode = 	"#version 450 core\n" \
											"layout (location = 0) in vec4 vPosition;" \
											"layout (location = 1) in vec3 normal; " \
											"uniform mat4 mv_matrix;" \
											"uniform mat4 proj_matrix;" \
											"out VS_OUT" \
											"{" \
											"	vec3 normal;" \
											"	vec3 view;" \
											"} vs_out;" \
											"void main()"\
											"{" \
											"vec4 pos_vs = mv_matrix * vPosition;" \
											"vs_out.normal = mat3 (mv_matrix) * normal; " \
											"vs_out.view = pos_vs.xyz; "\
											"gl_Position = proj_matrix * pos_vs;"\
											"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCode = "#version 450 core\n"\
											"out vec4 color; " \
											"uniform sampler2D tex_envmap;" \
											"in VS_OUT" \
											"{" \
											"	vec3 normal;" \
											"	vec3 view;" \
											"} fs_in;" \
											"void main()" \
											"{" \
											"		vec3 u = normalize ( fs_in.view); " \
											"		vec3 r = reflect ( u, normalize(fs_in.normal) ) ;" \
											"		r.z = r.z + 1.0; " \
											"		float m = 0.5 * inversesqrt(dot(r,r)); " \
											"		color = texture ( tex_envmap , r.xy * m + vec2(0.5) ); " \
											"}";
	

	glShaderSource(gFragmentShaderObject, 1, (const char**)&fragmentShaderSourceCode, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// pri-link binding of shader object with vertex shader possition attribute
	//glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_POSITION, "vPosition");
	//glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_NORMAL, "normal");

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObject);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	gMVUniform = glGetUniformLocation(gShaderProgramObject, "mv_matrix");
	gProjUniform = glGetUniformLocation(gShaderProgramObject, "proj_matrix");
	gSampler2D = glGetUniformLocation(gShaderProgramObject, "tex_envmap");

	horizontalLine();

	// create vao
	char* data;
	FILE *infile;
	size_t file_size;

	infile = fopen("./objects/dragon.sbm", "rb");

	if (infile == NULL)
	{
		fprintf(gpFile, "\nFile for object not able to open");
		Uninitialize();
		DestroyWindow(ghwnd);
	}

	fprintf(gpFile, "\nFile for object open successafully");
	
	fseek(infile, 0, SEEK_END);
	file_size = ftell(infile);
	fseek(infile, 0, SEEK_SET);

	data = (char*)malloc(sizeof(char) * file_size);

	if (fread(data, file_size, 1, infile) == 0)
	{
		fprintf(gpFile, "\nNot able to read Object file data read");
		Uninitialize();
		DestroyWindow(ghwnd);
	}
	fprintf(gpFile, "\nObject file data read successully");

	char* ptr = data;
	SB6M_HEADER* header = (SB6M_HEADER *) ptr;
	ptr = ptr + header->size;

	chunk_vertex_attribs = (SB6M_CHUNK_HEADER*)ptr;
	ptr = ptr + chunk_vertex_attribs->size;
	chunk_vertex_data = (SB6M_CHUNK_HEADER*)ptr;
	ptr = ptr + chunk_vertex_data->size;

	fprintf(gpFile, "\n\n");

	fprintf(gpFile, "\nchunk_vertex_attribs->chunk_type = %d", chunk_vertex_attribs->chunk_type);
	fprintf(gpFile, "\nchunk_vertex_data->chunk_type = %d", chunk_vertex_data->chunk_type);

	fprintf(gpFile, "\n\n");

	fprintf(gpFile, "\nheader->num_chunks = %d", header->num_chunks);
	fprintf(gpFile, "\nSB6M_CHUNK_TYPE_DATA = %d ", SB6M_CHUNK_TYPE_DATA);
	fprintf(gpFile, "\nSB6M_CHUNK_TYPE_VERTEX_ATTRIBS = %d ", SB6M_CHUNK_TYPE_VERTEX_ATTRIBS);
	fprintf(gpFile, "\nSB6M_CHUNK_TYPE_VERTEX_DATA = %d ", SB6M_CHUNK_TYPE_VERTEX_DATA);
	fprintf(gpFile, "\nSB6M_CHUNK_TYPE_INDEX_DATA = %d ", SB6M_CHUNK_TYPE_INDEX_DATA);
	fprintf(gpFile, "\nSB6M_CHUNK_TYPE_SUB_OBJECT_LIST = %d ", SB6M_CHUNK_TYPE_SUB_OBJECT_LIST);

	// copy vertex attrib chunck
	vertex_attrib_chunk = (SB6M_VERTEX_ATTRIB_CHUNK*)chunk_vertex_attribs;
	vertex_data_chunk = (SB6M_CHUNK_VERTEX_DATA*)chunk_vertex_data;

	// VAO FOR DRAGON

	glGenVertexArrays(1, &gVao_dragon);
	glBindVertexArray(gVao_dragon);

	glGenBuffers(1, &data_buffer_dragon);
	glBindBuffer(GL_ARRAY_BUFFER, data_buffer_dragon);
	glBufferData(GL_ARRAY_BUFFER, vertex_data_chunk->data_size, NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, vertex_data_chunk->data_size, data + vertex_data_chunk->data_offset);

	fprintf(gpFile, "\nvertex_attrib_chunk->attrib_count = %d", vertex_attrib_chunk->attrib_count);

	SB6M_VERTEX_ATTRIB_DECL &attrib_decl = vertex_attrib_chunk->attrib_data[0];
	glVertexAttribPointer(0,
		attrib_decl.size,
		attrib_decl.type,
		attrib_decl.flags & SB6M_VERTEX_ATTRIB_FLAG_NORMALIZED ? GL_TRUE : GL_FALSE,
		attrib_decl.stride,
		(GLvoid*) (uintptr_t)attrib_decl.data_offset);
	glEnableVertexAttribArray(0);

	attrib_decl = vertex_attrib_chunk->attrib_data[1];
	glVertexAttribPointer(1,
		attrib_decl.size,
		attrib_decl.type,
		attrib_decl.flags & SB6M_VERTEX_ATTRIB_FLAG_NORMALIZED ? GL_TRUE : GL_FALSE,
		attrib_decl.stride,
		(GLvoid*)(uintptr_t)attrib_decl.data_offset);
	glEnableVertexAttribArray(1);

	attrib_decl = vertex_attrib_chunk->attrib_data[2];
	glVertexAttribPointer(2,
		attrib_decl.size,
		attrib_decl.type,
		attrib_decl.flags& SB6M_VERTEX_ATTRIB_FLAG_NORMALIZED ? GL_TRUE : GL_FALSE,
		attrib_decl.stride,
		(GLvoid*)(uintptr_t)attrib_decl.data_offset);
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//VAO FOR TEAPOT

	int totalElement = 0;
	int totalTexcoord = 0;
	int v = 0, n = 0, t = 0, f = 0;
	for (int i = 0; i < (sizeof(face_indicies) / sizeof(face_indicies[0])); i++)
	{
		for (int j = 0; j < 3; j++)
		{
			int vi = face_indicies[i][j];
			int ni = face_indicies[i][j + 3];
			int ti = face_indicies[i][j + 6];

			teapotNormal[n + 0] = normals[ni][0];
			teapotNormal[n + 1] = normals[ni][1];
			teapotNormal[n + 2] = normals[ni][2];

			n = n + 3;

			teapotTexcoord[t] = textures[ti][0];
			teapotTexcoord[t + 1] = textures[ti][1];

			t = t + 2;

			teapotVertices[v + 0] = vertices[vi][0];
			teapotVertices[v + 1] = vertices[vi][1];
			teapotVertices[v + 2] = vertices[vi][2];

			v = v + 3;
			totalElement = totalElement + 3;
			totalTexcoord = totalTexcoord + 2;
		}
	}

	f = 0;
	for (int i = 0; i < 15704; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			teapotFaceIndices[f] = face_indicies[i][j];
			f = f + 1;
		}
	}

	fprintf(gpFile, "\nf = %d", f);

	glGenVertexArrays(1, &gVao_teapot);
	glBindVertexArray(gVao_teapot);

	glGenBuffers(1, &gVbo_position_teapot);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_position_teapot);
	glBufferData(GL_ARRAY_BUFFER, sizeof(teapotVertices), teapotVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo

	glGenBuffers(1, &gVbo_normals_teapot);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_normals_teapot);
	glBufferData(GL_ARRAY_BUFFER, sizeof(teapotNormal), teapotNormal, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo

	glGenBuffers(1, &gVbo_texture_teapot);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_texture_teapot);
	glBufferData(GL_ARRAY_BUFFER, sizeof(teapotTexcoord), teapotTexcoord, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_TEXTURE_0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_TEXTURE_0);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo

	glBindVertexArray(0); // end for Vao

	// TEXTURE
	LoadTexture("./textures/envmaps/spheremap1.ktx", &envmaps_0);
	LoadTexture("./textures/envmaps/spheremap2.ktx", &envmaps_1);
	LoadTexture("./textures/envmaps/spheremap3.ktx", &envmaps_2);

	texture = envmaps_0;

	// set-up depth buffers
	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // don't work

	// we will always cull face for better performance
	glEnable(GL_CULL_FACE);

	gPerspectiveProjectionMatrix = mat4::identity();

	Resize(WIDTH, HIGHT);
}

void horizontalLine()
{
	fprintf(gpFile, "\n\n=======================================================================================\n\n");
}

void LoadTexture(const char* filename, unsigned int* tex)
{
	void horizontalLine();

	FILE* fp;
	header h;
	size_t data_start, data_end;
	unsigned char* data;
	GLenum target = GL_NONE;
	horizontalLine();
	fprintf(gpFile, "Entering in LoadTexture() For file %s" , filename);

	fp = fopen(filename, "rb");

	if (fp == NULL)
	{
		fprintf(gpFile, "File for texture not able to open");
		return;
	}

	if (fread(&h, sizeof(h), 1, fp) != 1)
	{
		fprintf(gpFile, "Not able to read data");
		return;
	}

	data_start = ftell(fp) + h.keypairbytes;
	fseek(fp, 0, SEEK_END);
	data_end = ftell(fp);
	fseek(fp, data_start, SEEK_SET);

	data = (unsigned char*)malloc(sizeof(unsigned char) * (data_end - data_start));

	if (data == NULL)
	{
		fprintf(gpFile, "Not able to allocate memory to data");
		return;
	}
	fprintf(gpFile, "\nAllocated memory to data successfully");
	fread(data, 1, data_end - data_start, fp);

	// print header structure values 
	fprintf(gpFile, "\nh.pixelheight = %d \nh.pixeldepth = %d\nh.arrayelements =%d\nh.faces=%d\nh.keypairbytes=%d\nh.miplevels=%d\nh.gltype=%d",
		h.pixelheight, h.pixeldepth, h.arrayelements, h.faces, h.keypairbytes, h.miplevels, h.gltype);

	if (memcmp(h.identifier, identifier, sizeof(identifier)) != 0)
	{
		fprintf(gpFile, "idenitifier not match");
		return;
	}

	if (h.endianness == 0x04030201)
	{
		fprintf(gpFile, "\nNO swap needed %d" , h.endianness);
	}
	else if (h.endianness == 0x01020304)
	{
		fprintf(gpFile, "\nSwap needed");
	}

	glGenTextures(1, tex);
	glBindTexture(GL_TEXTURE_2D, *tex);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	if (h.miplevels == 0)
	{
		fprintf(gpFile, "\nMipmap level is 0 setting to 1");
		h.miplevels = 1;
	}
	
	glTexStorage2D(GL_TEXTURE_2D, h.miplevels, h.glinternalformat, h.pixelwidth ,h.pixelheight);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, h.pixelwidth, h.pixelheight, h.glformat, h.gltype, data);
	glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0);
	fprintf(gpFile, "\nExiting successfully from LoadTexture()");
}

void Resize(int width, int height) {
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) height);

	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); // no 0.1f

}

void Display() {
	// CODE
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// START USING OpenGL program object
	glUseProgram(gShaderProgramObject);

	//OpenGL Drawing
	// set modelview & modelviewProjection matrices to indentity
	static GLfloat rotateAngle = 0.0f;
	mat4 translateMatrix = mat4::identity();;
	mat4 rotateMatrix = vmath::rotate(rotateAngle, 0.0f, 1.0f, 0.0f);
	mat4 modelViewMatrix = mat4::identity();;
	mat4 modelViewProjectionMatrix = mat4::identity();
	
	
	glUniformMatrix4fv(gProjUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindTexture(GL_TEXTURE_2D, texture);
	if (isDragon)
	{
		translateMatrix = vmath::translate(0.0f, -4.0f, -20.0f);
		modelViewMatrix = translateMatrix * rotateMatrix;
		glUniformMatrix4fv(gMVUniform, 1, GL_FALSE, modelViewMatrix);

		glBindVertexArray(gVao_dragon);
		glDrawArraysInstancedBaseInstance(GL_TRIANGLES, 0, vertex_data_chunk->total_vertices, 1, 0);
		glBindVertexArray(0);	
	}
	else if (isTeapot)
	{
		translateMatrix = vmath::translate(0.0f, 0.0f, -1.50f);
		modelViewMatrix = translateMatrix * rotateMatrix;
		glUniformMatrix4fv(gMVUniform, 1, GL_FALSE, modelViewMatrix);

		glBindVertexArray(gVao_teapot);
		glDrawArrays(GL_TRIANGLES, 0, 47112);
		glBindVertexArray(0);
	}

	// stop using OpenGL program object
	glUseProgram(0);

	rotateAngle = rotateAngle + 0.3f;

	if (rotateAngle > 360.0f)
	{
		rotateAngle = 0;
	}

	SwapBuffers(ghdc);
}

void Uninitialize() {

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	// destroy vao
	if (gVao_teapot)
	{
		glDeleteVertexArrays(1, &gVao_teapot);
		gVao_teapot = 0;
	}

	// destroy vbo
	if (gVbo_position_teapot)
	{
		glDeleteBuffers(1, &gVbo_position_teapot);
		gVbo_position_teapot = 0;
	}

	if (gVbo_normals_teapot)
	{
		glDeleteBuffers(1, &gVbo_normals_teapot);
		gVbo_normals_teapot = 0;
	}

	if (gVbo_texture_teapot)
	{
		glDeleteBuffers(1, &gVbo_texture_teapot);
		gVbo_texture_teapot = 0;
	}

	// deatch vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	// detach fragment shader from shader program obejct
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject);

	gVertexShaderObject = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	// unlink shader program
	glUseProgram(0);

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile) {
		fprintf(gpFile, "\n\nClosing File Successfully");
		fclose(gpFile);
	}
}
