// header file declaration
#include <windows.h>
#include<stdio.h>

#include <GL\glew.h>
#include <gl/GL.h>
#include "OGL.h"

#pragma comment(lib, "C:\\glew-2.1.0\\lib\\Release\\Win32\\glew32.lib")
#pragma comment(lib, "opengl32.lib")

#define WIDTH 1000
#define HIGHT 800
#define NUM_ELEMENTS 2048

using namespace vmath;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int gWidth, gHeight;

enum
{
	HRH_ATTRIBUTE_POSITION = 0 ,
	HRH_ATTRIBUTE_COLOR ,
	HRH_ATTRIBUTE_NORMAL ,
	HRH_ATTRIBUTE_TEXTURE_0
};

GLuint gComputeShaderObject;
GLuint gComputeShaderProgramObject;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint data_buffer[2];

float input_data[NUM_ELEMENTS];
float output_data[NUM_ELEMENTS];

GLuint images[3];

GLuint gDummyVao;
GLuint gTextureSampleUniform;

mat4 gPerspectiveProjectionMatrix;

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	void Initialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("h_ogl");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 

	if (fopen_s(&gpFile, "windows.log", "w") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("OGL - 2D Prefix Sum"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HIGHT/2 ,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow) {
				Display();
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	void Resize(int,int);

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int,int);
	void LoadShaders();
	float random_float();
	void prefix_sum(const float *input, float * output, int element);
	void LoadTexture(const char*, unsigned int*);
	void Uninitialize();

	// Function declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}
	
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		DestroyWindow(ghwnd);
	}

	//OpenGL Logs
	fprintf(gpFile, "OpenGL Logs");
	fprintf(gpFile, "OpenGL Vendor: %s \n ", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer: %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "OpenGL Shading Language : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	fprintf(gpFile, "Open Enabled extension\n");
	GLint numExtension = 0;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	fprintf(gpFile, "Open Enabled total extension %d \n", numExtension);

	for (int i = 0; i < numExtension; i++)
	{		
		//fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	LoadShaders();

	glGenTextures(3, images);

	LoadTexture(".//textures//salad-gray.ktx", &images[0]);

	glBindTexture(GL_TEXTURE_2D, images[1]);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_R32F, NUM_ELEMENTS, NUM_ELEMENTS);

	glBindTexture(GL_TEXTURE_2D, images[2]);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_R32F, NUM_ELEMENTS, NUM_ELEMENTS);

	glGenVertexArrays(1, &gDummyVao);
	glBindVertexArray(gDummyVao);

	// set-up depth buffers
	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// we will always cull face for better performance
	//glEnable(GL_CULL_FACE);

	Resize(WIDTH, HIGHT);
}

float random_float()
{
	static unsigned int seed = 0x13371337;

	float res;
	unsigned int temp;

	seed = seed * 16807;

	temp = seed ^ (seed >> 4) ^ (seed << 15);
	*((unsigned*)&res) = (temp >> 9) | 0x3F800000;

	return (res - 1.0f);

}

void prefix_sum(const float * input, float *output, int element)
{
	float f = 0.0f;
	int i;

	for (int i = 0; i < element; i++)
	{
		f += input[i];
		output[i] = f;
	}
}

void LoadShaders()
{
	// function declaration
	void LoadComputeShader();
	void LoadMyShadersAndProgram();
	void LoadVertexAndFragmentShaderAndProgramObject();

	LoadVertexAndFragmentShaderAndProgramObject();
	//LoadMyShadersAndProgram();
	LoadComputeShader();
}

void LoadMyShadersAndProgram()
{

}

void LoadVertexAndFragmentShaderAndProgramObject()
{
	// function declaration
	
	//code

	// VERTEX SHADER
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//provide souce code to shader
	const GLchar* vertexShaderSourceCode = 
			" #version 450 core														\n " \
			" out vec2 out_texcoord;												\n " \
			" void main()															\n " \
			" {																		\n " \
			" 	const vec4 vertices[] = vec4[](vec4(1.0, 1.0,  0.0 , 1.0),			\n " \
			" 									vec4(-1.0, 1.0,  0.0 , 1.0),		\n " \
			" 									vec4(-1.0, -1.0,  0.0 , 1.0),		\n " \
			" 									vec4(1.0,  -1.0,  0.0 , 1.0));		\n " \
			"	const vec2 texcoord[]   = vec2[](vec2(1.0, 1.0),					\n " \
			" 							vec2(0.0, 1.0),	  							\n " \
			" 							vec2(0.0, 0.0),	  							\n " \
			" 							vec2(1.0, 0.0));  							\n " \
			"	out_texcoord = texcoord[gl_VertexID];								\n " \
			"	gl_Position = vertices[gl_VertexID];								\n " \
			" }																		\n " ;

	//" 	gl_Position = vertices[gl_VertexID];							\n " \

	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}


	// FRAGMENT SHADER

	// CREATE SHADER
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCode = 
		" #version 450 core											\n " \
		" 															\n " \
		" layout (binding = 0) uniform sampler2D input_image;		\n " \
		" 															\n " \
		" layout (location = 0) out vec4 color;						\n " \
		" 															\n " \
		" in vec2 out_texcoord;										\n " \
		"															\n " \
		" void main(void)											\n " \
		" {															\n " \
		" 	float m = 1.0 + gl_FragCoord.x / 30.0;					\n " \
		" 	vec2 s = 1.0 / textureSize(input_image, 0);				\n " \
		" 															\n " \
		" 	vec2 C = gl_FragCoord.xy;								\n " \
		" 	C.y = 1800 - C.y;										\n " \
		" 	C.x = 100 + C.x;										\n " \
		" 															\n " \
		" 	vec2 p0 = vec2(C * 2.0) + vec2(-m, -m);					\n " \
		" 	vec2 p1 = vec2(C * 2.0) + vec2(-m , m);					\n " \
		" 	vec2 p2 = vec2(C * 2.0) + vec2(m, -m);					\n " \
		" 	vec2 p3 = vec2(C * 2.0) + vec2(m, m);					\n " \
		" 															\n " \
		" 	p0 = p0 * s;											\n " \
		" 	p1 = p1 * s;											\n " \
		" 	p2 = p2 * s;											\n " \
		" 	p3 = p3 * s;											\n " \
		" 															\n " \
		" 	float a = textureLod(input_image, p0, 0).r;				\n " \
		" 	float b = textureLod(input_image, p1, 0).r;				\n " \
		" 	float c = textureLod(input_image, p2, 0).r;				\n " \
		" 	float d = textureLod(input_image, p3, 0).r;				\n " \
		" 															\n " \
		" 	float f = a - b - c + d;								\n " \
		" 															\n " \
		" 	m = m * 2;												\n " \
		" 															\n " \
		" 	color = vec4(f) / float( m * m);						\n " \
		" 															\n " \
		" }															\n " ;

	// color = vec4(f) / float( m * m);
	// color = texture(input_image, out_texcoord);	

	glShaderSource(gFragmentShaderObject, 1, (const char**)&fragmentShaderSourceCode, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM
	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);
	
	// LINK PROGRAM
	glLinkProgram(gShaderProgramObject);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}
}

void LoadComputeShader()
{
	/********** COMPUTE SHADER ***********/
	// CREATE SHADER

	gComputeShaderObject = glCreateShader(GL_COMPUTE_SHADER);

	//provide souce code to shader
	const GLchar* computeShaderSourceCode =
		" #version 450 core															\n " \
		" 																			\n " \
		" layout (local_size_x = 1024) in;							    			\n " \
		" 																			\n " \
		" shared float shared_data[gl_WorkGroupSize.x * 2];             			\n " \
		" 																			\n " \
		" layout (binding = 0, r32f) readonly uniform image2D input_image;   		\n " \
		" 																	 		\n " \
		" layout (binding = 1, r32f) writeonly uniform image2D output_image; 		\n " \
		" 																			\n " \
		" void main(void)                                               			\n " \
		" {                                                             			\n " \
		" 	uint id = gl_LocalInvocationID.x;                          				\n " \
		" 	uint rd_id;																\n " \
		" 	uint wr_id;																\n " \
		" 	uint mask;																\n " \
		"	ivec2 p0 = ivec2(id * 2, gl_WorkGroupID.x);                 			\n " \
		"	ivec2 p1 = ivec2(id * 2 + 1, gl_WorkGroupID.x);             			\n " \
		"	                                                            			\n " \
		"	const uint steps = uint (log2(gl_WorkGroupSize.x)) + 1;     			\n " \
		"	uint step = 0;                                              			\n " \
		"	                                                            			\n " \
		"	shared_data[p0.x] = imageLoad(input_image, p0).r;           			\n " \
		"	shared_data[p1.x] = imageLoad(input_image, p1).r;           			\n " \
		"	                                                            			\n " \
		"	barrier();                                                  			\n " \
		"	                                                            			\n " \
		"	for(step = 0; step < steps; step++)                         			\n " \
		"	{                                                           			\n " \
		"		mask = (1 << step) - 1;                                 			\n " \
		"		rd_id = ((id >> step) << (step + 1)) + mask;            			\n " \
		"		wr_id = rd_id + 1 + (id & mask);                        			\n " \
		"		                                                        			\n " \
		"		shared_data[wr_id] += shared_data[rd_id];               			\n " \
		"		                                                        			\n " \
		"		barrier();                                              			\n " \
		"	}                                                           			\n " \
		"	                                                            			\n " \
		"	imageStore(output_image, p0.yx, vec4(shared_data[p0.x]));   			\n " \
		"	imageStore(output_image, p1.yx, vec4(shared_data[p1.x]));   			\n " \
		"}                                                              			\n " ;

	glShaderSource(gComputeShaderObject, 1, (const GLchar**)&computeShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gComputeShaderObject);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gComputeShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gComputeShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gComputeShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Compute shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	//CREATE SHADER PROGRAM
	gComputeShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gComputeShaderProgramObject, gComputeShaderObject);

	// LINK PROGRAM
	glLinkProgram(gComputeShaderProgramObject);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gComputeShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gComputeShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gComputeShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}
}

void Resize(int width, int height) {
	if (height == 0)
		height = 1;

	gWidth = width;
	gHeight = height;

	glViewport(0, 0, (GLsizei) width,(GLsizei) height);

	//gPerspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); // no 0.1f

}

void Display() 
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glUseProgram(gComputeShaderObject);

	glBindImageTexture(0, images[0], 0, GL_FALSE, 0, GL_READ_ONLY, GL_R32F);
	glBindImageTexture(1, images[1], 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32F);

	glDispatchCompute(NUM_ELEMENTS, 1, 1);

	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	glBindImageTexture(0, images[1], 0, GL_FALSE, 0, GL_READ_ONLY, GL_R32F);
	glBindImageTexture(1, images[2], 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32F);

	glDispatchCompute(NUM_ELEMENTS, 1, 1);

	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	glBindTexture(GL_TEXTURE_2D, images[2]);
	
	glUseProgram(gShaderProgramObject);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, images[2]);
	
	glBindVertexArray(gDummyVao);
	//glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 3);

	SwapBuffers(ghdc);
}

void Uninitialize() {

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	// unlink shader program
	glUseProgram(0);

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile) {
		fprintf(gpFile, "\nClosing File Successfully");
		fclose(gpFile);
	}
}
