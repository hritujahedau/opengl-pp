// header file declaration
#include <windows.h>
#include<stdio.h>

#include <GL\glew.h>
#include <gl/GL.h>
#include "OGL.h"
#include "vmath.h"

#pragma comment(lib, "C:\\glew-2.1.0\\lib\\Release\\Win32\\glew32.lib")
#pragma comment(lib, "opengl32.lib")

#define WIDTH 1000
#define HIGHT 800

using namespace vmath;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int width, hight;

enum
{
	HRH_ATTRIBUTE_POSITION = 0 ,
	HRH_ATTRIBUTE_COLOR ,
	HRH_ATTRIBUTE_NORMAL ,
	HRH_ATTRIBUTE_TEXTURE_0
};

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao_pyramid;
GLuint gVbo_pyramid_position;
GLuint gVbo_pyramid_texcoord;

GLuint gVao_cube;
GLuint gVbo_cube_position;
GLuint gVbo_cube_texcoord;

GLuint gMVPUniform;
GLuint gTextureSamplerUniform;
GLuint stone_texture = 0, kundali_texture = 0;

mat4 gPerspectiveProjectionMatrix;

const GLfloat x = 1.0f, y = 1.0f, z = 1.0f;
const GLfloat appex = 1.0f, left = 1.0f, right = 1.0f, depth = 1.0f;

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	void Initialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("h_ogl");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 

	if (fopen_s(&gpFile, "windows.log", "w") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("OGL"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HIGHT/2 ,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow) {
				Display();
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	void Resize(int,int);

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int,int);
	void Uninitialize();
	bool LoadGLTexture(GLuint*, TCHAR[]);

	// Function declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}
	
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		DestroyWindow(ghwnd);
	}

	//OpenGL Logs
	fprintf(gpFile, "OpenGL Logs");
	fprintf(gpFile, "OpenGL Vendor: %s \n ", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer: %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "OpenGL Shading Language : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	fprintf(gpFile, "Open Enabled extension\n");
	GLint numExtension = 0;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	fprintf(gpFile, "Open Enabled total extension %d \n", numExtension);

	for (int i = 0; i < numExtension; i++)
	{		
		//fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	/********** VERTEX SHADER ***********/
	// CREATE SHADER

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	
	//provide souce code to shader
	const GLchar* vertexShaderSourceCode =  "#version 450 core\n"\
											"in vec4 vPosition;" \
											"in vec2 vTexCoord;" \
											"out vec2 outTexCoord; " \
											"uniform mat4 u_mvpMatrix;" \
											"void main()"\
											"{" \
											"outTexCoord = vTexCoord;" \
											"gl_Position = u_mvpMatrix * vPosition;"\
											"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCode = "#version 450 core\n"\
											"in vec2 outTexCoord;" \
											"out vec4 FragColor; " \
											"uniform sampler2D u_texture_sampler;" \
											"void main()"\
											"{"\
											"FragColor = texture(u_texture_sampler,outTexCoord) ;" \
											"}";
	

	glShaderSource(gFragmentShaderObject, 1, (const char**)&fragmentShaderSourceCode, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_TEXTURE_0, "vTexCoord");

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObject);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// get MVP uniform location
	gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvpMatrix");
	gTextureSamplerUniform = glGetUniformLocation(gShaderProgramObject, "u_texture_sampler");

	// ***** vertex, colors, shadersatribs , vbo, vao initialization *********
	const GLfloat pyramidVertices[] =
				{
					// FRONT FACE
					0.0f, 1.0f, 0.0f,
					-1.0f,-1.0f, 1.0f,
					1.0f, -1.0f,1.0f,
					
					// RIGHT FACE
					0.0f, 1.0f, 0.0f,
					1.0f, -1.0f, 1.0f,
					1.0f, -1.0f, -1.0f,

					// BACK FACE
					0.0f, 1.0f, 0.0f,
					1.0f, -1.0f,-1.0f,
					-1.0f, -1.0f,-1.0f,

					// LEFT FACE
					0.0f, 1.0f, 0.0f,
					-1.0f, -1.0f,-1.0f,
					-1.0f, -1.0f,1.0f,
				};

	const GLfloat pyramidTexCoordMatrix[] = 
				{   
					//1
					0.5f, 1.0f,
					0.0f, 0.0f,
					1.0f, 0.0f,

					//2
					0.5f, 1.0f,
					1.0f, 0.0f,
					0.0f, 0.0f,


					//3
					0.5f, 1.0f,
					0.0f,0.0f,
					1.0f,0.0f,

					
					//4
					0.5f, 1.0f,
					1.0f, 0.0f,
					0.0f, 0.0f,

					
				};

	const GLfloat cubeVertices[] =
	{
		// FRONT
		1.0f,1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,


		// RIGHT
		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f,-1.0f,		

		//BACK
		1.0f, -1.0f,-1.0f,
		-1.0f, -1.0f,-1.0f,
		-1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,
		
		// LEFT
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,

		// TOP
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		// BOTTOM
		1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f
		
	};
						

	const GLfloat cubeTexCoordMatrix[] =
						{   
							// 1
							0.0f, 0.0f,
							1.0f, 0.0f,
							1.0f,1.0f,
							0.0f,1.0f,

							//2
							1.0f, 0.0f,
							1.0f,1.0f,
							0.0f,1.0f,
							0.0f, 0.0f,

							//3
							1.0f, 0.0f,
							1.0f, 1.0f,
							0.0f, 1.0f,
							0.0f, 0.0f,

							//4
							0.0f, 0.0f,
							1.0f, 0.0f,
							1.0f, 1.0f,
							0.0f, 1.0f,

							//5
							0.0f,1.0f,
							0.0f, 0.0f,
							1.0f, 0.0f,
							1.0f, 1.0f,

							//6
							1.0f, 1.0f,
							0.0f, 1.0f,
							0.0f, 0.0f,
							1.0f, 0.0f,
							 
						};

	glGenVertexArrays(1, &gVao_pyramid);
	glBindVertexArray(gVao_pyramid);

	/****
	If we don't use gVao need to write #1, #2, #3 function in display again.
	****/

	/***********************************gVao*********************************************************/

	//gVbo_triangle_position
	glGenBuffers(1, &gVbo_pyramid_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_pyramid_position); //Bind gVbo
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW); // feed the data #1	
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);	// kontya variable sathi, kiti elements, type #2	
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION); // #3
	glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for gVbo_pyramid_position

	//gVbo_color
	glGenBuffers(1, &gVbo_pyramid_texcoord); // Bind gVbo_color 
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_pyramid_texcoord);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidTexCoordMatrix), pyramidTexCoordMatrix, GL_STATIC_DRAW); // #1
	glVertexAttribPointer(HRH_ATTRIBUTE_TEXTURE_0, 2, GL_FLOAT, GL_FALSE, 0, NULL); // #2		
	glEnableVertexAttribArray(HRH_ATTRIBUTE_TEXTURE_0); // #3
	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind Bind gVbo_color
	
	// unbind gVao_triangle 
	glBindVertexArray(0); // unbind for gVao

	LoadGLTexture(&stone_texture, ID_STONE_BITMAP);
	glEnable(GL_TEXTURE_2D);

	/**********************************gVao_square*******************************************************/

	glGenVertexArrays(1, &gVao_cube);
	glBindVertexArray(gVao_cube);

	// now give vertex for square geometry
	glGenBuffers(1, &gVbo_cube_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_cube_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &gVbo_cube_texcoord);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_cube_texcoord);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeTexCoordMatrix), cubeTexCoordMatrix, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_TEXTURE_0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_TEXTURE_0);	
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// unbind to gVao_square
	glBindVertexArray(0);

	LoadGLTexture(&kundali_texture, ID_KUNDALI_BITMAP);
	glEnable(GL_TEXTURE_2D);

	/***************************************************************************************************************/
	// set-up depth buffers
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // don't work

	// we will always cull face for better performance
	glEnable(GL_CULL_FACE);

	gPerspectiveProjectionMatrix = mat4::identity();

	Resize(WIDTH, HIGHT);
}

// Texture code
bool LoadGLTexture(GLuint* texture, TCHAR resourceID[])
{
	//VARIABLE DECLARATION
	bool bResult = false;
	HBITMAP hBitmap = NULL;
	BITMAP bmp;

	//CODE
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), resourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap)
	{
		bResult = true;
		GetObject(hBitmap, sizeof(BITMAP), &bmp);

		// from here start OpenGL Texture code
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1); //IN FFP 4
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);

		//setting texture parameter
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		// following call will actually push the data with help of graphics driver
		//gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);
		glTexImage2D(GL_TEXTURE_2D, 0, 3, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, bmp.bmBits);		
		glGenerateMipmap(GL_TEXTURE_2D);

		DeleteObject(hBitmap);
	}
	return bResult;
}


void Resize(int width, int height) {
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) height);

	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); // no 0.1f

}

void Display() {
	// CODE
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	static GLfloat angle = 0.0f;

	// START USING OpenGL program object
	glUseProgram(gShaderProgramObject);

	//OpenGL Drawing For Trianlge
	// set modelview & modelviewProjection matrices to indentity
	mat4 translateMatrix = vmath::translate(-1.5f, 0.0f, -6.0f);
	mat4 scaleMatrix = mat4::identity();
	mat4 rotateMatrix = vmath::rotate(angle, 0.0f, 1.0f, 0.0f);
	mat4 modelViewMatrix = translateMatrix * rotateMatrix ;
	mat4 modelViewProjectionMatrix = mat4::identity();
	

	// multiply the modelview and orthographic matrix to get modelviewProjectyion matrix
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix; // ORDER IS IMPORTANT

	// pass above modelviewprojection matrix to vertex shader 'u_mvpMatrix' shader variable
	// whose value we already have calculated in inititialization() by uisng glGetUniformLocation()
	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, stone_texture);
	glUniform1i(gTextureSamplerUniform, 0);

	// *** bind vao **
	glBindVertexArray(gVao_pyramid);

	// ** draw , either by glDrawTriangles() or glDrawArrays() or glDrawElements();
	glDrawArrays(GL_TRIANGLES, 0, 12); //3 (each with its x,y,z) value in triangleVertices array

	// ** unbind vao **
	glBindVertexArray(0);

	// for square
	translateMatrix = vmath::translate(1.5f, 0.0f, -6.0f);
	rotateMatrix = vmath::rotate(angle, 1.0f, 0.0f, 0.0f);
	scaleMatrix = vmath::scale(0.8f, 0.8f, 0.8f);
	modelViewMatrix = translateMatrix * scaleMatrix * rotateMatrix ;
	modelViewProjectionMatrix = mat4::identity();

	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, kundali_texture);
	glUniform1i(gTextureSamplerUniform, 0);

	glBindVertexArray(gVao_cube);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
	glBindVertexArray(0);

	// stop using OpenGL program object
	glUseProgram(0);

	angle = angle + 0.1f;

	SwapBuffers(ghdc);
}

void Uninitialize() {

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	// destroy gVao_triangle
	if (gVao_pyramid)
	{
		glDeleteVertexArrays(1, &gVao_pyramid);
		gVao_pyramid = 0;
	}

	if (gVbo_pyramid_texcoord)
	{
		glDeleteBuffers(1, &gVbo_pyramid_texcoord);
		gVbo_pyramid_texcoord = 0;
	}

	if (gVbo_pyramid_position)
	{
		glDeleteBuffers(1, &gVbo_pyramid_position);
		gVbo_pyramid_position = 0;
	}

	if (gVao_cube)
	{
		glDeleteVertexArrays(1, &gVao_cube);
		gVao_cube = 0;
	}

	if (gVbo_cube_texcoord)
	{
		glDeleteBuffers(1, &gVbo_cube_texcoord);
		gVbo_cube_texcoord = 0;
	}

	if (gVbo_cube_position)
	{
		glDeleteBuffers(1, &gVbo_cube_position);
		gVbo_cube_position = 0;
	}

	if (stone_texture)
	{
		glDeleteTextures(1, &stone_texture);
	}

	if (kundali_texture)
	{
		glDeleteTextures(1, &kundali_texture);
	}

	// deatch vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	// detach fragment shader from shader program obejct
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject);

	gVertexShaderObject = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	// unlink shader program
	glUseProgram(0);

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile) {
		fprintf(gpFile, "\n Closing File Successfully");
		fclose(gpFile);
	}
}
