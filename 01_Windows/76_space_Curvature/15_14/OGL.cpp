// header file declaration
#include <windows.h>
#include <Windowsx.h>
#include<stdio.h>

#include <GL\glew.h>
#include <gl/GL.h>
#include "OGL.h"
#include "vmath.h"
#include "HRH_Sphere.h"

#pragma comment(lib, "C:\\glew-2.1.0\\lib\\Release\\Win32\\glew32.lib")
#pragma comment(lib, "opengl32.lib")

#define WIDTH 1000
#define HEIGHT 800

using namespace vmath;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int gWidth, gHeight;

enum
{
	HRH_ATTRIBUTE_POSITION = 0 ,
	HRH_ATTRIBUTE_COLOR ,
	HRH_ATTRIBUTE_NORMAL ,
	HRH_ATTRIBUTE_TEXTURE_0
};

GLuint gVertexShaderObject_Cubemap;
GLuint gFragmentShaderObject_Cubemap;
GLuint gShaderProgramObject_Cubemap;

GLuint gModelUniform;
GLuint gPerspectiveUniform;

GLuint gIsLightOnUniform;

mat4 gPerspectiveProjectionMatrix;

// new added 
GLuint gTimeUniform, gMouseUnifrom, gResolutionUniform, gCamPosUniform;
float gTime = 100.0f;
vec3 gMouse;
vec2 iResolution;

vec3 gMouseWheelRotation;

bool mouseDown = false, mouseMove = false;

GLuint gViewMatrixWithRotationUniform;
GLuint gViewMatrixWithoutRotationUniform;

float zoom = 10.0f;
float zoom_z = zoom;
float zoom_x = zoom;
vec3 gCamPos_Cubemap = vec3(zoom_x, 0.0f, zoom_z);
vec3 gCamTar_Cubemap = vec3(1.0f, 0.0f, 0.0);
vec3 world_up = vec3(0.0, 1.0, 0.0);
float gCameraWheelSpeed = 0.5, cameraKeyMoveSpeed = 0.05;

float field_of_view = 45.0f;

mat4 viewMatrixWithRotation_ForCubemap = mat4::identity(), viewMatrixWithoutRotation_ForCubemap = mat4::identity();

float mouse_last_x = WIDTH / 2.0f, mouse_last_y = HEIGHT / 2.0f;
float mouse_x_offset = 0, mouse_y_offset = 0;
float mouse_x_pos = 0.0f, mouse_y_pos = 0.0f;
float mouse_sensitivity = 0.1f;

float yaw = -179.0f, pitch = 0.0f;
bool firstMouse = false;
bool isMouseCtrlKeyPreseed;

GLuint gTextureSamplerUniform;
GLuint stars_texture = 0;

GLuint gVao_cube_cubemap;
GLuint gVbo_cube_position;

float starTime_Cubemap = 800.0f;

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	void Initialize();
	void Display_Cubemap();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("h_ogl");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 

	if (fopen_s(&gpFile, "windows.log", "w") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("OGL"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HEIGHT/2 ,
		WIDTH,
		HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			Display_Cubemap();
			if (gbActiveWindow) {
				
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	void Resize(int,int);

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			gWidth = LOWORD(lParam);
			gHeight = HIWORD(lParam);
			break;		
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_MOUSEWHEEL:
			if (GET_WHEEL_DELTA_WPARAM(wParam) / WHEEL_DELTA >= 0)
			{
				// increament
				field_of_view = field_of_view + gCameraWheelSpeed;
			}
			else
			{
				// decrement
				field_of_view = field_of_view - gCameraWheelSpeed;
			}

			if (field_of_view < 1.0f)
			{
				field_of_view = 1.0f;
			}

			if (field_of_view > 45.0f)
			{
				field_of_view = 45.0f;
			}
			break;
		case WM_LBUTTONDOWN:
			gMouse[2] = 1;
			mouseDown = true;
			break;
		case WM_LBUTTONUP:
			mouseDown = false;
			mouse_last_x = gMouse[0];
			mouse_last_y = gMouse[1];
			gMouse[2] = -1;			
			break;
		case WM_MOUSEMOVE:
			mouse_x_pos = GET_X_LPARAM(lParam);
			mouse_y_pos = GET_Y_LPARAM(lParam);

			gMouse[0] = mouse_x_pos;
			gMouse[1] = mouse_y_pos;

			if (mouseDown == true)
			{
				gMouse[2] = 1;
			}			
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
			//	ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		//ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int,int);
	void Uninitialize();
	void getSphereVertexData(int, float*, float*, float*, unsigned short*);
	int getNumberOfSphereVertices(void);
	int getNumberOfSphereElements(void);
	void makeSphere(float fRadius, int iSlices, int iStacks);
	bool LoadGLTexture(GLuint*, TCHAR[]);

	// Function declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}
	
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		DestroyWindow(ghwnd);
	}
	
	//OpenGL Logs
	fprintf(gpFile, "OpenGL Logs");
	fprintf(gpFile, "OpenGL Vendor: %s \n ", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer: %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "OpenGL Shading Language : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	fprintf(gpFile, "Open Enabled extension\n");
	GLint numExtension = 0;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	fprintf(gpFile, "Open Enabled total extension %d \n", numExtension);

	for (int i = 0; i < numExtension; i++)
	{		
		//fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	/********** VERTEX SHADER ***********/
	// CREATE SHADER

	gVertexShaderObject_Cubemap = glCreateShader(GL_VERTEX_SHADER);
	
	//provide souce code to shader
	const GLchar* vertexShaderSourceCode =  
	" #version 450 core\n"  \
	" in vec4 v_position;" \
	" uniform mat4 u_modelMatrix;" \
	" uniform mat4 u_projectionMatrix;" \
	" uniform mat4 u_camPosWithoutRotation;																			\n " \
	" void main()																									\n " \
	" {																												\n " \
	"	gl_Position = u_projectionMatrix * u_camPosWithoutRotation * u_modelMatrix *  vec4(v_position);	\n " \
	" }																												\n " ;

	//u_projectionMatrix * u_viewMatrix * u_modelMatrix  * 

	glShaderSource(gVertexShaderObject_Cubemap, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject_Cubemap);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject_Cubemap, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_Cubemap, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_Cubemap, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObject_Cubemap = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCode = 
		" #version 450 core																									\n " \
		" uniform sampler2D u_texture_sampler;																				\n " \
		" out vec4 FragColor;																								\n " \
		" uniform mat4 u_viewMatrixWithRotation;																			\n " \
		" uniform vec2 iResolution;																							\n " \
		" uniform float iTime;																							\n " \
		" uniform vec4 u_camPos;																							\n " \
		"																													\n " \
		" vec3 fancyCube(sampler2D sam, in vec3 d, in float s, in float b)													\n " \
		" {																													\n " \
		" 	vec3 colx = texture(sam, 0.5 + s * d.yz / d.x, b).xyz;															\n " \
		" 	vec3 coly = texture(sam, 0.5 + s * d.zx / d.y, b).xyz;															\n " \
		" 	vec3 colz = texture(sam, 0.5 + s * d.xy / d.z, b).xyz;															\n " \
		" 																													\n " \
		" 	vec3 n = d * d;																									\n " \
		" 																													\n " \
		" 	vec3 ans = vec3 ( ( colx * n.x + coly * n.y + colz*n.z ) / ( n.x + n.y + n.z ));						\n " \
		" 																													\n " \
		" 	return ans;																							\n " \
		" }																													\n " \
		" 																													\n " \
		" vec2 hash( vec2 p)																								\n " \
		" {																													\n " \
		" 	p = vec2 ( dot (p, vec2(127.1, 311.7)) , dot ( p , vec2(269.5, 183.3)) );										\n " \
		" 	return fract(sin(p) * 43758.5453);																				\n " \
		" }																													\n " \
		" 																													\n " \
		" vec2 voronoi( in vec2 x)																							\n " \
		" {																													\n " \
		" 	vec2 n = floor( x );																							\n " \
		" 	vec2 f = fract( x);																								\n " \
		" 																													\n " \
		" 	vec3 m = vec3 ( 8.0, 0.0, 0.0);																							\n " \
		" 																													\n " \
		" 	for(int j = -1; j <= 1 ; j++)																					\n " \
		" 	{																												\n " \
		" 		for( int i = -1; i <= 1; i++)																				\n " \
		" 		{																											\n " \
		" 			vec2 g = vec2 ( float(i), float(j));																	\n " \
		" 			vec2 o = hash(n + g);																					\n " \
		" 			vec2 r = g - f + o;																						\n " \
		" 			float d = dot(r,r);																						\n " \
		" 			if(d < m.x)																								\n " \
		" 			{																										\n " \
		" 				m = vec3 ( d, o);																					\n " \
		" 			}																										\n " \
		" 		}																											\n " \
		" 	}																												\n " \
		" 	return vec2( sqrt(m.x), m.y + m.z);																				\n " \
		" }																													\n " \
		" 																													\n " \
		" vec3 background(in vec3 d)																						\n " \
		" {																													\n " \
		" 	vec3 col = vec3(0.0, 0.0, 0.0);																					\n " \
		" 																													\n " \
		" 	col += 0.5 * pow ( fancyCube( u_texture_sampler, d, 0.05, 5.0 ).zyx, vec3(2.0) );								\n " \
		" 	col += 0.2 * pow ( fancyCube( u_texture_sampler, d, 0.10, 3.0 ).zyx, vec3(1.5) );								\n " \
		" 	col += 0.8 * vec3( 0.80, 0.5, 0.6) *  pow ( fancyCube( u_texture_sampler, d, 0.1, 0.0 ).xxx, vec3(6.0) );		\n " \
		" 																													\n " \
		" 	float stars = smoothstep(0.3, 0.7, fancyCube( u_texture_sampler, d, 0.91, 0.0 ).x);								\n " \
		" 																													\n " \
		" 	vec3 n = abs(d);																								\n " \
		" 	n = n * n * n;																									\n " \
		" 																													\n " \
		" 	vec2 vxy = voronoi( 50.0 * d.xy);																				\n " \
		" 	vec2 vyz = voronoi( 50.0 * d.yz);																				\n " \
		" 	vec2 vzx = voronoi( 50.0 * d.zx);																				\n " \
		" 																													\n " \
		" 	vec2 r = vec2( (vyz * n.x + vzx*n.y + vxy*n.z) / (n.x + n.y + n.z) );										\n " \
		" 																													\n " \
		" 	col += vec3( 0.9 * stars * clamp(1.0 - (3.0 + r.y * 5.0) * r.x,  0.0,  1.0) );											\n " \
		" 																													\n " \
		" 	col = vec3(1.5 * col - 0.2);																							\n " \
		" 																													\n " \
		" 	col += vec3(-0.05, 0.1, 0.0);																					\n " \
		" 																													\n " \
		" 																													\n " \
		" 	return col;																										\n " \
		" }																													\n " \
		" void main()																										\n " \
		" {																													\n " \
		" 																													\n " \
		"	vec2 p = (-iResolution.xy + 2.0 * gl_FragCoord.xy) / iResolution.y;												\n " \
		" 																													\n " \
		"	vec3 rayDirection = normalize ( mat3(u_viewMatrixWithRotation) * vec3(p, -2.0) );								\n " \
		" 	vec3 stars = background(rayDirection);																		\n " \
		" 																													\n " \
		" 	stars *= smoothstep(0.0, 6.0, iTime);																			\n " \
		" 																													\n " \
		" 	vec2 q = gl_FragCoord.xy / iResolution.xy;																		\n " \
		" 	stars *= 0.2 + 0.8 * pow( 16.0 * q.x * q.y * ( 1.0 - q.x) * ( 1.0 - q.y), 0.1);									\n " \
		" 																													\n " \
		"	FragColor =  vec4(stars, 1.0) ;																					\n " \
		"	}																												\n ";

		
	glShaderSource(gFragmentShaderObject_Cubemap, 1, (const char**)&fragmentShaderSourceCode, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject_Cubemap);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObject_Cubemap, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_Cubemap, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_Cubemap, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObject_Cubemap = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject_Cubemap, gVertexShaderObject_Cubemap);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObject_Cubemap, gFragmentShaderObject_Cubemap);

	// pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(gShaderProgramObject_Cubemap, HRH_ATTRIBUTE_POSITION, "v_position");
	glBindAttribLocation(gShaderProgramObject_Cubemap, HRH_ATTRIBUTE_NORMAL, "v_normals");

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObject_Cubemap);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject_Cubemap, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_Cubemap, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_Cubemap, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// get MVP uniform location
	

	float cubecoord = 10.0f;

	const GLfloat cubeVertices[] =
	{
		// FRONT
		 cubecoord,  cubecoord, cubecoord,
		-cubecoord,  cubecoord, cubecoord,
		-cubecoord, -cubecoord, cubecoord,
		 cubecoord, -cubecoord, cubecoord,


		// RIGHT
		cubecoord,  cubecoord, -cubecoord,
		cubecoord,  cubecoord,  cubecoord,
		cubecoord, -cubecoord,  cubecoord,
		cubecoord, -cubecoord, -cubecoord,

		//BACK
		 cubecoord, -cubecoord,  -cubecoord,
		-cubecoord, -cubecoord,  -cubecoord,
		-cubecoord,  cubecoord,  -cubecoord,
		 cubecoord,  cubecoord,  -cubecoord,

		// LEFT
		-cubecoord,  cubecoord,  cubecoord,
		-cubecoord,  cubecoord, -cubecoord,
		-cubecoord, -cubecoord, -cubecoord,
		-cubecoord, -cubecoord,  cubecoord,

		// TOP
		 cubecoord, cubecoord, -cubecoord,
		-cubecoord, cubecoord, -cubecoord,
		-cubecoord, cubecoord,  cubecoord,
		 cubecoord, cubecoord,  cubecoord,

		// BOTTOM
		 cubecoord, -cubecoord,  cubecoord,
		-cubecoord, -cubecoord,  cubecoord,
		-cubecoord, -cubecoord, -cubecoord,
		 cubecoord, -cubecoord, -cubecoord

	};

	glGenVertexArrays(1, &gVao_cube_cubemap);
	glBindVertexArray(gVao_cube_cubemap);

	// now give vertex for square geometry
	glGenBuffers(1, &gVbo_cube_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_cube_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// unbind to gVao_square
	glBindVertexArray(0);

	LoadGLTexture(&stars_texture, ID_STARS_BITMAP);

	// set-up depth buffers
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // don't work

	// we will always cull face for better performance
	glEnable(GL_CULL_FACE);

	gPerspectiveProjectionMatrix = mat4::identity();

	Resize(WIDTH, HEIGHT);
}

// Texture code
bool LoadGLTexture(GLuint* texture, TCHAR resourceID[])
{
	//VARIABLE DECLARATION
	bool bResult = false;
	HBITMAP hBitmap = NULL;
	BITMAP bmp;

	//CODE
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), resourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap)
	{
		bResult = true;
		GetObject(hBitmap, sizeof(BITMAP), &bmp);

		// from here start OpenGL Texture code
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1); //IN FFP 4
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);

		//setting texture parameter
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		// following call will actually push the data with help of graphics driver
		//gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);
		glTexImage2D(GL_TEXTURE_2D, 0, 3, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, bmp.bmBits);
		glGenerateMipmap(GL_TEXTURE_2D);

		DeleteObject(hBitmap);
	}
	return bResult;
}

void Resize(int width, int height) {
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) height);

	gPerspectiveProjectionMatrix = vmath::perspective(field_of_view, ((GLfloat)gWidth / gHeight), ((GLfloat)gHeight / gWidth), 100.0f); // no 0.1f

}

void Display_Cubemap() {
	// function declarations
	void getCameraValuesOnMouseLeftKeyPressed_Cubemap();
	void getCameraWithoutRotation_Cubemap();

	// CODE
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// START USING OpenGL program object
	glUseProgram(gShaderProgramObject_Cubemap);

	gPerspectiveProjectionMatrix = vmath::perspective(field_of_view, ((GLfloat)gWidth / gHeight), ((GLfloat)gHeight / gWidth), 100.0f); // no 0.1f

	// OpenGL Drawing
	// set modelview & modelviewProjection matrices to indentity
	//mat4 translateMatrix = vmath::translate(0.0f, 0.0f, -1.0f);
	//mat4 viewMatrix = mat4::identity();
	mat4 modelMatrix = mat4::identity();
	
	gModelUniform = glGetUniformLocation(gShaderProgramObject_Cubemap, "u_modelMatrix");
	gViewMatrixWithRotationUniform = glGetUniformLocation(gShaderProgramObject_Cubemap, "u_viewMatrixWithRotation");
	gViewMatrixWithoutRotationUniform = glGetUniformLocation(gShaderProgramObject_Cubemap, "u_camPosWithoutRotation");
	gPerspectiveUniform = glGetUniformLocation(gShaderProgramObject_Cubemap, "u_projectionMatrix");

	gMouseUnifrom = glGetUniformLocation(gShaderProgramObject_Cubemap, "iMouse");
	gTimeUniform = glGetUniformLocation(gShaderProgramObject_Cubemap, "iTime");
	gResolutionUniform = glGetUniformLocation(gShaderProgramObject_Cubemap, "iResolution");
	gTextureSamplerUniform = glGetUniformLocation(gShaderProgramObject_Cubemap, "u_texture_sampler");

	getCameraValuesOnMouseLeftKeyPressed_Cubemap();

	getCameraWithoutRotation_Cubemap();
	
	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*) modelMatrix);
	glUniformMatrix4fv(gViewMatrixWithRotationUniform, 1, GL_FALSE, (const float*) viewMatrixWithRotation_ForCubemap);
	glUniformMatrix4fv(gViewMatrixWithoutRotationUniform, 1, GL_FALSE, (const float*) viewMatrixWithoutRotation_ForCubemap);	
	glUniformMatrix4fv(gPerspectiveUniform, 1, GL_FALSE, (const GLfloat *) gPerspectiveProjectionMatrix);

	iResolution[0] = gWidth;
	iResolution[1] = gHeight;
	glUniform2fv(gResolutionUniform, 1, (const float*)iResolution);

	glUniform3fv(gMouseUnifrom, 1, (const float*)gMouse);

	glUniform1f(gTimeUniform, starTime_Cubemap);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, stars_texture);
	glUniform1i(gTextureSamplerUniform, 0);

	
	glBindVertexArray(gVao_cube_cubemap);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
	glBindVertexArray(0);
	
	// stop using OpenGL program object
	glUseProgram(0);

	starTime_Cubemap += 0.003;

	gTime = gTime + 0.003;

	SwapBuffers(ghdc);
}

void getCameraValuesOnMouseLeftKeyPressed_Cubemap()
{
	// function declarations
	void getCameraWithDefaultRotation_ForCubemap();

	mat4 Camera(vec3 camPos, vec3 camTar, vec3 camUp);

	if (mouseDown)
	{
		vec2 m = vec2(gMouse[0], gMouse[1]);

		float cx ;
		float sx ;
		float cy ;

		if (gMouse[2] > 0)
		{
			cx = cos(0.02 * m[0]);
			sx = sin(0.02 * m[0]);
			cy = cos(0.032 * m[1]);
		}

		vec3 camPos = vec3(cx - sx * gCamPos_Cubemap[0], cy * gCamPos_Cubemap[0], (sx + cx) * gCamPos_Cubemap[2]);
		viewMatrixWithRotation_ForCubemap = Camera(camPos, gCamTar_Cubemap, world_up);
	}
	else
	{
		getCameraWithDefaultRotation_ForCubemap();
	}
}


void getCameraWithoutRotation_Cubemap()
{
	// variable declarations
	mat4 Camera(vec3 camPos, vec3 camTar, vec3 camUp);

	float cx = cos(0.1 + 3.55);
	float sx = sin(0.1 + 3.55);
	float cy = 0;

	vec3 camPos = vec3(cx - sx * gCamPos_Cubemap[0], cy * gCamPos_Cubemap[1], (sx + cx) * gCamPos_Cubemap[2]);
	
	viewMatrixWithoutRotation_ForCubemap = Camera(camPos, gCamTar_Cubemap, world_up);
}


void getCameraWithDefaultRotation_ForCubemap()
{
	// function declarations
	float mySmoothStep(float, float, float);

	// variable declarations
	mat4 Camera(vec3 camPos, vec3 camTar, vec3 camUp);

	float zoom_out = 1.0 + mySmoothStep(5.0, 10.0, fabsf(gTime - 48.0f));

	float angle = 3.0 + 0.05 * gTime + 6.0 * gMouse[0] / iResolution[0];

	float cx = cos(0.1 * gTime + 3.55);
	float sx = sin(0.1 * gTime + 3.55);
	float cy = 0;

	vec3 camPos = zoom_out * vec3(gCamPos_Cubemap[0] * cos(angle), gCamPos_Cubemap[1], gCamPos_Cubemap[2] * sin(angle));

	float cr = 0.35;
	vec3 cameraUp = vec3(sin(cr), cos(cr), 0.0);

	viewMatrixWithRotation_ForCubemap = Camera(camPos, gCamTar_Cubemap, cameraUp);
}


mat4 Camera(vec3 camPos, vec3 camTar, vec3 camUp)
{
	
	vec3 camDir = normalize(camTar - camPos);     // cw	
	vec3 uu = normalize(cross(camDir, camUp));   // cp
	vec3 vv = normalize(cross(uu, camDir));
	mat4 viewMatrix;

	viewMatrix = mat4(
		vec4(uu, 0.0),
		vec4(vv, 0.0),
		vec4(-camDir, 0.0),
		vec4(dot(uu, camPos), dot(vv, camPos), dot(camDir, camPos), 1.0f)
	);
	return viewMatrix;
	
}


float mySmoothStep(float edge0, float edge1, float x)
{
	// function declarations
	float clamp(float, float, float);

	x = clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0);

	return x * x * (3 - 2 * x);
}

float clamp(float x, float lowerLimit, float upperlimit)
{
	if (x < lowerLimit)
	{
		x = lowerLimit;
	}
	if (x > upperlimit)
	{
		x = upperlimit;
	}
	return x;
}

void Uninitialize() {

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
	
	if (gVao_cube_cubemap)
	{
		glDeleteVertexArrays(1, &gVao_cube_cubemap);
		gVao_cube_cubemap = 0;
	}

	if (gVbo_cube_position)
	{
		glDeleteBuffers(1, &gVbo_cube_position);
		gVbo_cube_position = 0;
	}

	// deatch vertex shader from shader program object
	glDetachShader(gShaderProgramObject_Cubemap, gVertexShaderObject_Cubemap);

	// detach fragment shader from shader program obejct
	glDetachShader(gShaderProgramObject_Cubemap, gFragmentShaderObject_Cubemap);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject_Cubemap);

	gVertexShaderObject_Cubemap = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject_Cubemap);
	gFragmentShaderObject_Cubemap = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject_Cubemap);
	gShaderProgramObject_Cubemap = 0;

	// unlink shader program
	glUseProgram(0);

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile) {
		fprintf(gpFile, "\n Closing File Successfully");
		fclose(gpFile);
	}
}
