// header file declaration
#include <windows.h>
#include<stdio.h>

#include <GL\glew.h>
#include <gl/GL.h>
#include "OGL.h"
#include "vmath.h"
#include "TeaPot.h"

#pragma comment(lib, "C:\\glew-2.1.0\\lib\\Release\\Win32\\glew32.lib")
#pragma comment(lib, "opengl32.lib")

#define WIDTH 1000
#define HIGHT 800

using namespace vmath;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int width, hight;

enum
{
	HRH_ATTRIBUTE_POSITION = 0 ,
	HRH_ATTRIBUTE_COLOR ,
	HRH_ATTRIBUTE_NORMAL ,
	HRH_ATTRIBUTE_TEXTURE_0
};

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao_teapot;
GLuint gVbo_teapot_position;
GLuint gVbo_teapot_normal;
GLuint gVbo_teapot_element;
GLuint gVbo_teapot_texture;

GLuint gModelUniform;
GLuint gViewUniform;
GLuint gPerspectiveUniform;

GLuint gLightAmbientUniform;
GLuint gLightSpecularUniform;
GLuint gLightDiffusedUniform;
GLuint gLightPossitionUniform;

GLuint gMaterialSpecularUniform;
GLuint gMaterialAmbientUniform;
GLuint gMaterialDiffuseUniform;
GLuint gMaterialShininessUniform;
GLuint gTextureSamplerUniform;

GLuint gIsLightOnUniform;

const int vertices_row = sizeof(vertices) / sizeof(vertices[0]);
const int vertices_column = sizeof(vertices[0]) / sizeof(vertices[0][0]);
const int vertices_elements = vertices_row * vertices_column;

const int normals_row = sizeof(normals) / sizeof(normals[0]);
const int normals_column = sizeof(normals[0]) / sizeof(normals[0][0]);
const int normals_elements = normals_row * normals_column;

const int texture_row = sizeof(textures) / sizeof(textures[0]);
const int texture_column = sizeof(textures[0]) / sizeof(textures[0][0]);
const int texture_elements = texture_row * texture_column;

const int faceindices_row = sizeof(face_indicies) / sizeof(face_indicies[0]);
const int faceindices_column = sizeof(face_indicies[0]) / sizeof(face_indicies[0][0]);
const int faceindices_element = faceindices_row * faceindices_column;

GLfloat teapot_vertices[141336];
GLfloat teapot_normals[141336];
GLfloat teapot_textrure[94224];
unsigned short teapot_elements[15704 * 8]; //  Base indices from FFP tea pot

int count = 0, count_for_texcoord = 0;

int isLightOn = 0;

mat4 gPerspectiveProjectionMatrix;

GLuint marble_texture = 0;

GLfloat lightPossition[] = { 100.0f, 100.0f, 100.0f, 1.0f };
GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f }; 
GLfloat lightDefuse[] = { 1.0f, 1.0f, 1.0f};
GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f };

GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f };
GLfloat materialDefuse[] = { 1.0f, 1.0f, 1.0f  }; 
GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0f };
GLfloat materialShinyness = 128.0f;


//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	void Initialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("h_ogl");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 

	if (fopen_s(&gpFile, "windows.log", "w") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}
	fprintf(gpFile, "\nIn Main()\n");

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("OGL"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HIGHT/2 ,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow) {
				Display();
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	void Resize(int,int);

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_CHAR:
			switch (wParam)
			{
			case 'l':
			case 'L':
				if (isLightOn == 0)
				{
					isLightOn = 1;
				}
				else
				{
					isLightOn = 0;
				}
				break;
			}
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int,int);
	void Uninitialize();
	void copyTeaPot2DArraysToSingleArray();
	bool LoadGLTexture(GLuint * texture, TCHAR resourceID[]);


	// Function declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}
	
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		DestroyWindow(ghwnd);
	}

	//OpenGL Logs
	fprintf(gpFile, "OpenGL Logs");
	fprintf(gpFile, "OpenGL Vendor: %s \n ", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer: %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "OpenGL Shading Language : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	fprintf(gpFile, "Open Enabled extension\n");
	GLint numExtension = 0;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	fprintf(gpFile, "Open Enabled total extension %d \n", numExtension);

	for (int i = 0; i < numExtension; i++)
	{		
		//fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	/********** VERTEX SHADER ***********/
	// CREATE SHADER

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	
	//provide souce code to shader
	const GLchar* vertexShaderSourceCode =  "#version 450 core\n"  \
	"in vec4 v_position;" \
	"in vec2 vTexCoord; "\
	"out vec2 vTexCoord_out;" \
	"in vec3 v_normals; " \
	"uniform mat4 u_modelMatrix;" \
	"uniform mat4 u_viewMatrix;" \
	"uniform mat4 u_projectionMatrix;" \
	"uniform int u_isLightOn;" \
	"uniform vec4 u_light_position; " \
	"out vec3 light_direction; "\
	"out vec3 transformed_normal;"\
	"out vec3 view_vector;"\
	"void main()" \
	"{" \
	"if (u_isLightOn == 1) " \
	"{" \
	"vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" \
	"transformed_normal =  mat3( u_viewMatrix * u_modelMatrix ) * v_normals;" \
	"light_direction = vec3 (u_light_position - eye_coordinates);" \
	"view_vector =  vec3 ( -eye_coordinates);" \
	"}" \
	"vTexCoord_out = vTexCoord;" \
	"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" \
	"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCode = "#version 450 core\n" \
		"in vec2 vTexCoord_out;"\
		"vec3 phoung_ads_lighting;" \
		"out vec4 FragColor; " \
		"in vec3 light_direction; "\
		"in vec3 transformed_normal;"\
		"in vec3 view_vector;"\
		"vec3 normalized_light_direction; "\
		"vec3 normalized_transformed_normal;"\
		"vec3 normalized_view_vector;"\
		"uniform vec3 u_lightAmbient;" \
		"uniform vec3 u_lightSpecular;" \
		"uniform vec3 u_lightDiffuse;" \
		"uniform vec3 u_materialAmbient; " \
		"uniform vec3 u_materialSpecular; "\
		"uniform vec3 u_materialDiffuse; "\
		"uniform float u_materialshininess; "\
		"uniform int u_isLightOn;" \
		"uniform sampler2D u_sampler_2d; "\
		"void main()" \
		"{"\
		"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" \
		"if (u_isLightOn == 1) " \
		"{" \
		"normalized_light_direction = normalize(light_direction);"\
		"normalized_transformed_normal = normalize(transformed_normal);"\
		"normalized_view_vector = normalize(view_vector);"\
		"vec3 reflection_vector = reflect (-normalized_light_direction, normalized_transformed_normal); " \
		"vec3 ambient = u_lightAmbient * u_materialAmbient; " \
		"vec3 diffuse_light = u_lightDiffuse * u_materialDiffuse * max ( dot (normalized_light_direction , normalized_transformed_normal), 0.0); "\
		"vec3 specular = u_lightSpecular * u_materialSpecular * pow( max ( dot ( reflection_vector , normalized_view_vector), 0.0) , u_materialshininess);" \
		"phoung_ads_lighting = ambient + diffuse_light + specular;" \
		"}"\
		"FragColor =  vec4(phoung_ads_lighting, 1.0) + texture(u_sampler_2d, vTexCoord_out) ; " \
		"}";


	glShaderSource(gFragmentShaderObject, 1, (const char**)&fragmentShaderSourceCode, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_POSITION, "v_position");
	glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_NORMAL, "v_normals");
	glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_TEXTURE_0, "vTexCoord");

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObject);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// get MVP uniform location
	gModelUniform = glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
	gViewUniform = glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
	gPerspectiveUniform = glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");
	gIsLightOnUniform = glGetUniformLocation(gShaderProgramObject, "u_isLightOn");
	
	gLightAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_lightAmbient");
	gLightSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_lightSpecular");
	gLightDiffusedUniform = glGetUniformLocation(gShaderProgramObject, "u_lightDiffuse");
	gLightPossitionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");

	gMaterialDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_materialDiffuse");
	gMaterialAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_materialAmbient");
	gMaterialSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_materialSpecular");
	gMaterialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_materialshininess");
	gTextureSamplerUniform = glGetUniformLocation(gShaderProgramObject, "u_texture_sampler");

	copyTeaPot2DArraysToSingleArray();
	

	// ***** vertex, colors, shadersatribs , vbo, vao initialization *********	
	glGenVertexArrays(1, &gVao_teapot);
	glBindVertexArray(gVao_teapot);

	glGenBuffers(1, &gVbo_teapot_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_teapot_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(teapot_vertices), teapot_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo

	glGenBuffers(1, &gVbo_teapot_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_teapot_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(teapot_normals), teapot_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &gVbo_teapot_texture);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_teapot_texture);
	glBufferData(GL_ARRAY_BUFFER, sizeof(teapot_textrure), teapot_textrure, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_TEXTURE_0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_TEXTURE_0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_teapot_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_teapot_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(teapot_elements), teapot_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0); // end for Vao

	LoadGLTexture(&marble_texture, ID_MARBLE_BITMAP);
	glEnable(GL_TEXTURE_2D);

	// set-up depth buffers
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // don't work

	// we will always cull face for better performance
	glEnable(GL_CULL_FACE);

	gPerspectiveProjectionMatrix = mat4::identity();

	Resize(WIDTH, HIGHT);
}

// Texture code
bool LoadGLTexture(GLuint* texture, TCHAR resourceID[])
{
	//VARIABLE DECLARATION
	bool bResult = false;
	HBITMAP hBitmap = NULL;
	BITMAP bmp;

	//CODE
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), resourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap)
	{
		bResult = true;
		GetObject(hBitmap, sizeof(BITMAP), &bmp);

		// from here start OpenGL Texture code
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1); //IN FFP 4
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);

		//setting texture parameter
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		// following call will actually push the data with help of graphics driver
		//gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);
		glTexImage2D(GL_TEXTURE_2D, 0, 3, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, bmp.bmBits);
		glGenerateMipmap(GL_TEXTURE_2D);

		DeleteObject(hBitmap);
	}
	return bResult;
}


void copyTeaPot2DArraysToSingleArray()
{
	count_for_texcoord = 0;
	count = 0;
	int element = 0;
	for (int i = 0; i < (sizeof(face_indicies) / sizeof(face_indicies[0])) ; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			
			int vi = face_indicies[i][j];
			int ni = face_indicies[i][j + 3];
			int ti = face_indicies[i][j + 6];

			teapot_normals[count] = normals[ni][0];
			teapot_normals[count + 1] = normals[ni][1];
			teapot_normals[count + 2] = normals[ni][2];

			teapot_vertices[count] = vertices[vi][0];
			teapot_vertices[count + 1] = vertices[vi][1];
			teapot_vertices[count + 2] = vertices[vi][2];

			count = count + 3;

			teapot_textrure[count_for_texcoord] = textures[ti][0];
			teapot_textrure[count_for_texcoord + 1] = textures[ti][1];

			count_for_texcoord = count_for_texcoord + 2;


			//teapot_elements[i * faceindices_column + j] = face_indicies[i][j];
			teapot_elements[element] = face_indicies[i][j];
			teapot_elements[element + 1] = face_indicies[i][j + 3];
			teapot_elements[element + 2] = face_indicies[i][j + 6];

			element = element + 3;
			
		}
	}
} 


/*void copyTeaPot2DArraysToSingleArray()
{
	// vertices
	for (int i = 0; i < vertices_row; i++)
	{
		for (int j = 0; j < vertices_column; j++)
		{
			teapot_vertices[i * vertices_column + j] = vertices[i][j];
		}
	}

	// normals
	for (int i = 0; i < normals_row; i++)
	{
		for (int j = 0; j < normals_column; j++)
		{
			teapot_normals[i * normals_column + j] = normals[i][j];
		}
	}

	// textures
	for (int i = 0; i < texture_row; i++)
	{
		for (int j = 0; j < texture_column; j++)
		{
			teapot_textrure[i * texture_column + j] = textures[i][j];
		}
	}

	//indices
	for (int i = 0; i < faceindices_row; i++)
	{
		for (int j = 0; j < faceindices_column; j++)
		{
			teapot_elements[i * faceindices_column + j] = face_indicies[i][j];
		}
	}
}
*/


void Resize(int width, int height) {
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) height);

	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); // no 0.1f

}

void Display() {
	// CODE
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// START USING OpenGL program object
	glUseProgram(gShaderProgramObject);

	static GLfloat rotateAngle = 0.0f;

	// OpenGL Drawing
	// set modelview & modelviewProjection matrices to indentity
	mat4 translateMatrix = vmath::translate(0.0f, 0.0f, -1.55f);
	mat4 rotateMatrixtoMakeStraight = vmath::rotate(90.0f, 0.0f, 1.0f, 0.0f);
	mat4 rotateMatrix = vmath::rotate(rotateAngle, 0.0f, 1.0f, 0.0f);
	mat4 viewMatrix = mat4::identity();
	mat4 modelMatrix = translateMatrix * rotateMatrixtoMakeStraight * rotateMatrix;
	
	if (isLightOn == 1)
	{
		glUniform4fv(gLightPossitionUniform, 1, (const GLfloat*) lightPossition);
		glUniform3fv(gLightDiffusedUniform, 1, (const GLfloat*) lightDefuse);
		glUniform3fv(gLightAmbientUniform,  1 , (const GLfloat*) lightAmbient);
		glUniform3fv(gLightSpecularUniform, 1 , (const GLfloat*) lightSpecular);

		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*) materialAmbient);
		glUniform1f(gMaterialShininessUniform, materialShinyness);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*) materialSpecular);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*) materialDefuse);
	}

	glUniform1i(gIsLightOnUniform, isLightOn);
	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*) modelMatrix);
	glUniformMatrix4fv(gViewUniform, 1, GL_FALSE, (const float*) viewMatrix);
	glUniformMatrix4fv(gPerspectiveUniform, 1, GL_FALSE, (const GLfloat *) gPerspectiveProjectionMatrix);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, marble_texture);
	glUniform1i(gTextureSamplerUniform, 0);

	// *** bind vao **
	glBindVertexArray(gVao_teapot);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_teapot_element);
	glDrawElements(GL_TRIANGLES, faceindices_element, GL_UNSIGNED_SHORT, NULL);
	glBindVertexArray(0);

	// stop using OpenGL program object
	glUseProgram(0);

	rotateAngle = rotateAngle + 0.2f;

	SwapBuffers(ghdc);
}

void Uninitialize() {

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	// destroy vao
	if (gVao_teapot)
	{
		glDeleteVertexArrays(1, &gVao_teapot);
		gVao_teapot = 0;
	}

	// destroy vbo
	if (gVbo_teapot_position)
	{
		glDeleteBuffers(1, &gVbo_teapot_position);
		gVbo_teapot_position = 0;
	}

	if (gVbo_teapot_element)
	{
		glDeleteBuffers(1, &gVbo_teapot_element);
		gVbo_teapot_element = 0;
	}

	if (gVbo_teapot_normal)
	{
		glDeleteBuffers(1, &gVbo_teapot_normal);
		gVbo_teapot_normal = 0;
	}

	// deatch vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	// detach fragment shader from shader program obejct
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject);

	gVertexShaderObject = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	// unlink shader program
	glUseProgram(0);

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile) {
		fprintf(gpFile, "\n Closing File Successfully");
		fclose(gpFile);
	}
}
