#pragma once
#include <windows.h>
#include <gl\GL.h>
#include "vmath.h"

#define MYICON 1287
#define IDI_MYICON MAKEINTRESOURCE(MYICON)

#define HDR_IMAGE 1300
#define ID_HDR_IMAGE MAKEINTRESOURCE(HDR_IMAGE)

extern FILE* gpFile;

struct header
{
    unsigned char       identifier[12];
    unsigned int        endianness;
    unsigned int        gltype;
    unsigned int        gltypesize;
    unsigned int        glformat;
    unsigned int        glinternalformat;
    unsigned int        glbaseinternalformat;
    unsigned int        pixelwidth;
    unsigned int        pixelheight;
    unsigned int        pixeldepth;
    unsigned int        arrayelements;
    unsigned int        faces;
    unsigned int        miplevels;
    unsigned int        keypairbytes;
};

static const unsigned char identifier[] =
{
    0xAB, 0x4B, 0x54, 0x58, 0x20, 0x31, 0x31, 0xBB, 0x0D, 0x0A, 0x1A, 0x0A
};


typedef struct SB6M_HEADER_t
{
    union
    {
        unsigned int    magic;
        char            magic_name[4];
    };
    unsigned int        size;
    unsigned int        num_chunks;
    unsigned int        flags;
} SB6M_HEADER;

typedef struct SB6M_CHUNK_HEADER_t
{
    union
    {
        unsigned int    chunk_type;
        char            chunk_name[4];
    };
    unsigned int        size;
} SB6M_CHUNK_HEADER;


typedef struct SB6M_DATA_CHUNK_t
{
    SB6M_CHUNK_HEADER           header;
    unsigned int                encoding;
    unsigned int                data_offset;
    unsigned int                data_length;
} SB6M_DATA_CHUNK;


typedef struct SB6M_VERTEX_ATTRIB_DECL_t
{
    char                name[64];
    unsigned int        size;
    unsigned int        type;
    unsigned int        stride;
    unsigned int        flags;
    unsigned int        data_offset;
} SB6M_VERTEX_ATTRIB_DECL;

typedef struct SB6M_VERTEX_ATTRIB_CHUNK_t
{
    SB6M_CHUNK_HEADER           header;
    unsigned int                attrib_count;
    SB6M_VERTEX_ATTRIB_DECL     attrib_data[1];
} SB6M_VERTEX_ATTRIB_CHUNK;

typedef struct SB6M_CHUNK_VERTEX_DATA_t
{
    SB6M_CHUNK_HEADER   header;
    unsigned int        data_size;
    unsigned int        data_offset;
    unsigned int        total_vertices;
} SB6M_CHUNK_VERTEX_DATA;

void LoadTexture(const char* filename, unsigned int* tex)
{
	void horizontalLine();

	FILE* fp;
	header h;
	size_t data_start, data_end;
	unsigned char* data;
	GLenum target = GL_NONE;
	horizontalLine();
	fprintf(gpFile, "Entering in LoadTexture() For file %s", filename);

	fp = fopen(filename, "rb");

	if (fp == NULL)
	{
		fprintf(gpFile, "File for texture not able to open");
		return;
	}

	if (fread(&h, sizeof(h), 1, fp) != 1)
	{
		fprintf(gpFile, "Not able to read data");
		return;
	}

	data_start = ftell(fp) + h.keypairbytes;
	fseek(fp, 0, SEEK_END);
	data_end = ftell(fp);
	fseek(fp, data_start, SEEK_SET);

	data = (unsigned char*)malloc(sizeof(unsigned char) * (data_end - data_start));

	if (data == NULL)
	{
		fprintf(gpFile, "Not able to allocate memory to data");
		return;
	}
	fprintf(gpFile, "\nAllocated memory to data successfully");
	fread(data, 1, data_end - data_start, fp);

	// print header structure values 
	fprintf(gpFile, "\nh.pixelheight = %d \nh.pixeldepth = %d\nh.arrayelements =%d\nh.faces=%d\nh.keypairbytes=%d\nh.miplevels=%d\nh.gltype=%d",
		h.pixelheight, h.pixeldepth, h.arrayelements, h.faces, h.keypairbytes, h.miplevels, h.gltype);

	if (memcmp(h.identifier, identifier, sizeof(identifier)) != 0)
	{
		fprintf(gpFile, "idenitifier not match");
		return;
	}

	if (h.endianness == 0x04030201)
	{
		fprintf(gpFile, "\nNO swap needed %d", h.endianness);
	}
	else if (h.endianness == 0x01020304)
	{
		fprintf(gpFile, "\nSwap needed");
	}

	glGenTextures(1, tex);
	glBindTexture(GL_TEXTURE_2D, *tex);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	if (h.miplevels == 0)
	{
		fprintf(gpFile, "\nMipmap level is 0 setting to 1");
		h.miplevels = 1;
	}

	glTexStorage2D(GL_TEXTURE_2D, h.miplevels, h.glinternalformat, h.pixelwidth, h.pixelheight);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, h.pixelwidth, h.pixelheight, h.glformat, h.gltype, data);
	glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0);
	fprintf(gpFile, "\nExiting successfully from LoadTexture()");
}

// Texture code
bool LoadGLTexture(GLuint* texture, TCHAR resourceID[])
{
	//VARIABLE DECLARATION
	bool bResult = false;
	HBITMAP hBitmap = NULL;
	BITMAP bmp;

	//CODE
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), resourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap)
	{
		bResult = true;
		GetObject(hBitmap, sizeof(BITMAP), &bmp);

		// from here start OpenGL Texture code
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1); //IN FFP 4
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);

		//setting texture parameter
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		// following call will actually push the data with help of graphics driver
		//gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);
		glTexImage2D(GL_TEXTURE_2D, 0, 3, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, bmp.bmBits);
		glGenerateMipmap(GL_TEXTURE_2D);

		DeleteObject(hBitmap);
	}
	return bResult;
}


