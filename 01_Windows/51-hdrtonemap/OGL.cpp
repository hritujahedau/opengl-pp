// header file declaration
// envmapsphere_d
#include <windows.h>
#include<stdio.h>
#include <memory.h>

#include <GL\glew.h>
#include <gl/GL.h>
#include "OGL.h"
#include "vmath.h"

#pragma comment(lib, "C:\\glew-2.1.0\\lib\\Release\\Win32\\glew32.lib")
#pragma comment(lib, "opengl32.lib")

#define WIDTH 1000
#define HIGHT 800

#define SB6M_FOURCC(a,b,c,d)            ( ((unsigned int)(a) << 0) | ((unsigned int)(b) << 8) | ((unsigned int)(c) << 16) | ((unsigned int)(d) << 24) )
#define SB6M_VERTEX_ATTRIB_FLAG_NORMALIZED      0x00000001

using namespace vmath;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int width, hight;

GLfloat exposure = 1.0f;

int mode = 1;

enum
{
	HRH_ATTRIBUTE_POSITION = 0 ,
	HRH_ATTRIBUTE_NORMAL,
	HRH_ATTRIBUTE_COLOR ,	
	HRH_ATTRIBUTE_TEXTURE_0
};

typedef enum SB6M_CHUNK_TYPE_t
{
	SB6M_CHUNK_TYPE_INDEX_DATA = SB6M_FOURCC('I', 'N', 'D', 'X'),
	SB6M_CHUNK_TYPE_VERTEX_DATA = SB6M_FOURCC('V', 'R', 'T', 'X'),
	SB6M_CHUNK_TYPE_VERTEX_ATTRIBS = SB6M_FOURCC('A', 'T', 'R', 'B'),
	SB6M_CHUNK_TYPE_SUB_OBJECT_LIST = SB6M_FOURCC('O', 'L', 'S', 'T'),
	SB6M_CHUNK_TYPE_COMMENT = SB6M_FOURCC('C', 'M', 'N', 'T'),
	SB6M_CHUNK_TYPE_DATA = SB6M_FOURCC('D', 'A', 'T', 'A')
} SB6M_CHUNK_TYPE;

GLuint gVertexShaderObject;

GLuint gFragmentShaderObject_naive;
GLuint gShaderProgramObject_naive;

GLuint gFragmentShaderObject_adaptive;
GLuint gShaderProgramObject_adaptive;

GLuint gFragmentShaderObject_exposure;
GLuint gShaderProgramObject_exposure;

GLuint gVao;
GLuint data_buffer;

GLuint gMVUniform;
GLuint gProjUniform;
GLuint gSampler2D;

GLuint gUniformExposure;

mat4 gPerspectiveProjectionMatrix;

GLuint envmaps_0, envmaps_1, envmaps_2, texture_src, texture_lut ;

SB6M_CHUNK_HEADER* chunk_vertex_attribs;
SB6M_CHUNK_HEADER* chunk_vertex_data;

SB6M_VERTEX_ATTRIB_CHUNK* vertex_attrib_chunk;
SB6M_CHUNK_VERTEX_DATA* vertex_data_chunk;

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	void Initialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("h_ogl");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 

	if (fopen_s(&gpFile, "windows.log", "w") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL SuperBible - HDR Tone Mapping"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HIGHT/2 ,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow) {
				Display();
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	void Resize(int,int);

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_CHAR:
			switch (wParam)
			{
			case '1':
				mode = 1;
				break;
			case '2':
				mode = 2;
				break;
			case '3':
				mode = 3;
				break;
			case 'h':
				exposure = exposure * 1.1f;
				break;
			case 'H':
				exposure = exposure / 1.1f;
				break;
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int, int);
	void LoadTexture(const char * filename, unsigned int* tex);
	bool LoadGLTexture(GLuint * texture, TCHAR resourceID[]);
	void horizontalLine();
	void Uninitialize();

	// Function declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		DestroyWindow(ghwnd);
	}

	//OpenGL Logs
	fprintf(gpFile, "OpenGL Logs");
	fprintf(gpFile, "OpenGL Vendor: %s \n ", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer: %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "OpenGL Shading Language : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	fprintf(gpFile, "Open Enabled extension\n");
	GLint numExtension = 0;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	fprintf(gpFile, "Open Enabled total extension %d \n", numExtension);

	for (int i = 0; i < numExtension; i++)
	{
		//fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	/********** VERTEX SHADER ***********/
	// CREATE SHADER

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//provide souce code to shader
	const GLchar* vertexShaderSourceCode = 	"#version 450 core\n" \
											"void main(){											\n"\
											"const vec4 vertices[] = vec4[](vec4(-1.0, -1.0, 0.5, 1.0),"\
											"								vec4(1.0, -1.0, 0.5, 1.0), " \
											"								vec4(-1.0, 1.0, 0.5, 1.0), "\
											"								vec4(1.0, 1.0, 0.5, 1.0) );"\
											"gl_Position = vertices[gl_VertexID];"\
											"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObject_naive = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCode_naive = "#version 450 core\n"\
											"uniform sampler2D s;" \
											"uniform float exposure;" \
											"out vec4 color;"
											"void main()" \
											"{" \
											"		color = texture ( s , 2.0 * gl_FragCoord.xy / textureSize(s, 0) ); " \
											"}";
	

	glShaderSource(gFragmentShaderObject_naive, 1, (const char**)&fragmentShaderSourceCode_naive, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject_naive);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObject_naive, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_naive, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_naive, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObject_naive = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject_naive, gVertexShaderObject);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObject_naive, gFragmentShaderObject_naive);

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObject_naive);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject_naive, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_naive, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_naive, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	/******************************************************************* for adaptive *********************************/

	// CREATE SHADER
	gFragmentShaderObject_adaptive = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCode_adaptive = "#version 450 core\n"\
													"in vec2 vTex;" \
													"layout (binding = 0) uniform sampler2D hdr_image;" \
													"out vec4 oColor;" \
													"void main()" \
													"{" \
													"		int i;																									\n" \
													"		float lum[25];																							\n" \
													"																												\n" \
													"		vec2 tex_scale = vec2(1.0) / textureSize(hdr_image, 0);													\n" \
													"																												\n" \
													"		for(int i =0; i < 25; i++)																				\n" \
													"		{																										\n" \
													"			vec2 tc = (2.0 * gl_FragCoord.xy + 3.5 * vec2( i % 5 -2 , i / 5-2));								\n" \
													"			vec3 col = texture(hdr_image, tc * tex_scale).rgb;													\n" \
													"			lum[i] = dot(col, vec3(0.3, 0.59, 0.11));															\n" \
													"		}																										\n" \
													"																												\n" \
													"		// calcultae weighted color of region																	\n" \
													"		vec3 vColor = texelFetch(hdr_image, 2 * ivec2(gl_FragCoord.xy) , 0).rgb;								\n" \
													"																												\n" \
													"		 float kernelLuminance = (																				\n" \
													"		       (1.0  * (lum[0] + lum[4] + lum[20] + lum[24])) +													\n" \
													"		       (4.0  * (lum[1] + lum[3] + lum[5] + lum[9] +														\n" \
													"		               lum[15] + lum[19] + lum[21] + lum[23])) +												\n" \
													"		       (7.0  * (lum[2] + lum[10] + lum[14] + lum[22])) +												\n" \
													"		       (16.0 * (lum[6] + lum[8] + lum[16] + lum[18])) +													\n" \
													"		       (26.0 * (lum[7] + lum[11] + lum[13] + lum[17])) +												\n" \
													"		       (41.0 * lum[12])																					\n" \
													"		       ) / 273.0;																						\n" \
													"			  																									\n" \
													"		// compute the corresponding exposure																	\n" \
													"		float exposure = sqrt( 8.0 / (kernelLuminance + 0.25 ));												\n" \
													"																												\n" \
													"		// Apply the exposure to this texel																		\n" \
													"		oColor.rgb = 1.0 - exp2(-vColor * exposure);															\n" \
													"		oColor.a = 1.0f;																						\n" \
													"}";


	glShaderSource(gFragmentShaderObject_adaptive, 1, (const char**)&fragmentShaderSourceCode_adaptive, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject_adaptive);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObject_adaptive, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_adaptive, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_adaptive, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObject_adaptive = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject_adaptive, gVertexShaderObject);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObject_adaptive, gFragmentShaderObject_adaptive);

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObject_adaptive);

	// CHECK LINKING ERROR
	iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject_adaptive, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_adaptive, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_adaptive, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	/********************************************************** for exposure **********************************************/

	// CREATE SHADER
	gFragmentShaderObject_exposure = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCode_exposure = "#version 450 core\n"\
													"layout (binding = 0) uniform sampler2D hdr_image;" \
													"uniform float exposure = 1.0f;" \
													"out vec4 color;"
													"void main()" \
													"{" \
													"		vec4 c = texelFetch(hdr_image, 2 * ivec2(gl_FragCoord.xy), 0);" \
													"		c.rgb = vec3(1.0) - exp(-c.rgb * exposure); "
													"		color = c; " \
													"}";


	glShaderSource(gFragmentShaderObject_exposure, 1, (const char**)&fragmentShaderSourceCode_exposure, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject_exposure);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObject_exposure, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_exposure, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_exposure, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObject_exposure = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject_exposure, gVertexShaderObject);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObject_exposure, gFragmentShaderObject_exposure);

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObject_exposure);

	// CHECK LINKING ERROR
	iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject_exposure, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_exposure, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_exposure, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}


	// get uniform

	gUniformExposure = glGetUniformLocation(gShaderProgramObject_exposure, "exposure");

	horizontalLine();

	// create vaox
	glGenVertexArrays(1, &gVao);
	glBindVertexArray(gVao);

	glBindVertexArray(0);

//	LoadTexture("./textures/treelights_2k.ktx", &texture_src);
	LoadGLTexture(&texture_src, ID_HDR_IMAGE);

	static const GLfloat exposureLUT[20] = { 11.0f, 6.0f, 3.2f, 2.8f, 2.2f, 1.90f, 1.80f, 1.80f, 1.70f, 1.70f,  1.60f, 1.60f, 1.50f, 1.50f, 1.40f, 1.40f, 1.30f, 1.20f, 1.10f, 1.00f };

	glGenTextures(1, &texture_lut);
	glBindTexture(GL_TEXTURE_1D, texture_lut);
	glTexStorage1D(GL_TEXTURE_1D, 1, GL_R32F, 20);
	glTexSubImage1D(GL_TEXTURE_1D, 0, 0, 20, GL_RED, GL_FLOAT, exposureLUT);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);

	// set-up depth buffers
	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // don't work

	// we will always cull face for better performance
	glEnable(GL_CULL_FACE);

	gPerspectiveProjectionMatrix = mat4::identity();

	Resize(WIDTH, HIGHT);
}

void horizontalLine()
{
	fprintf(gpFile, "\n\n=======================================================================================\n\n");
}

void Resize(int width, int height) {
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) height);

	//gPerspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); // no 0.1f

}

void Display() {
	// CODE
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// START USING OpenGL program object

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_1D, texture_lut);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_src);

	switch (mode)
	{
	case 1:
		glUseProgram(gShaderProgramObject_naive);
		break;
	case 2:
		glUseProgram(gShaderProgramObject_adaptive);
		break;
	case 3:
		glUseProgram(gShaderProgramObject_exposure);
		glUniform1f(gUniformExposure, exposure);
		break;
	}
	
	glBindVertexArray(gVao);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(ghdc);
}

void Uninitialize() {

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}


	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile) {
		fprintf(gpFile, "\n\nClosing File Successfully");
		fclose(gpFile);
	}
}
