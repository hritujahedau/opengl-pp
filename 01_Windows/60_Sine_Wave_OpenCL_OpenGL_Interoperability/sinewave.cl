

extern FILE* gpFile;

__kernel void sineWave_kernel(__global float4* pPosition, unsigned int width, unsigned int height, float time)
{
	unsigned short y_row = get_global_id(1);
	unsigned short x_col = get_global_id(0);

	float u = x_col / (float)width;
	float v = y_row / (float)height;
	u = u * 2.0f - 1.0f;
	v = v * 2.0f - 1.0f;
	float frequency = 4.0f;
	float w = sinf(u * frequency + time) * cosf(v * frequency + time) * 0.5f;

	pPosition[y_row * width + x_col] = (float4)(u, w, v, 1.0f);
}

void launchSineWaveFromOpenCLKernel(float4* pPosition, unsigned int gMeshWidth, unsigned int gMeshHeight, float time)
{
	dim3 block = dim3(8, 8, 1);
	dim3 grid = dim3(ceil(gMeshWidth / block.x), ceil(gMeshHeight / block.y), 1);

	fprintf(gpFile, "\nIn launchSineWaveFromCudaKernel() before sineWave_kernel\n");

	sineWave_kernel << <grid, block >> > (pPosition, gMeshWidth, gMeshHeight, time);

	fprintf(gpFile, "\nIn launchSineWaveFromCudaKernel() after sineWave_kernel\n");
}
