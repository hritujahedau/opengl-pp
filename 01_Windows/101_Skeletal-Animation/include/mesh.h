#ifndef MESH_H
#define MESH_H

#include <windows.h>
#include <stdio.h>
#include "../include/OGL.h"

#include <string>
#include <vector>

// OpenGL header file
#include <GL/glew.h>		// This must be above gl.h
#include <GL/gl.h>

#include "../include/vmath.h"

using namespace std;
using namespace vmath;

#define MAX_BONE_INFLUENCE 4

struct Vertex 
{
    // position
    vec3 Position;
    // normal
    vec3 Normal;
    // texCoords
    vec2 TexCoords;
    // tangent
    vec3 Tangent;
    // bitangent
    vec3 Bitangent;
	//bone indexes which will influence this vertex
	int m_BoneIDs[MAX_BONE_INFLUENCE];
	//weights from each bone
	float m_Weights[MAX_BONE_INFLUENCE];
};

struct Texture 
{
    unsigned int id;
    string type;
    string path;
};

typedef struct vector_vertex 
{
    struct Vertex** vertex;
    size_t size;
}vector_vertex_t;

typedef struct vector_2d_int 
{
    GLint** pp_arr;
    size_t size;
}vector_2d_int_t;

typedef struct vector_textures 
{
    struct Texture** textures;
    size_t size;
}vector_textures_t;

typedef struct vector_2d_float_vertex 
{
    GLfloat** pp_arr;
    size_t size;
}vector_2d_float_vertex_t;

/*
typedef struct Mesh
{
    // mesh Data
    vector_vertex_t* vertices;
    vector_2d_int_t* indices;
    vector_textures_t* textures;
} Mesh;
*/

typedef struct Mesh
{
    // mesh Data
    vector<Vertex>       vertices;
    vector<unsigned int> indices;
    vector<Texture>      textures;
    unsigned int vao;
    unsigned int vbo;
    unsigned int ebo;
} Mesh;

#endif
