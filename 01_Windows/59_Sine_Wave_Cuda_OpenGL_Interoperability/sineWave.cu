
#include <cuda.h>
#include <stdio.h>

extern FILE* gpFile;

__global__ void sineWave_kernel(float4* pPosition, unsigned int width, unsigned int height, float time)
{
	unsigned short y_row = blockIdx.y * blockDim.y + threadIdx.y;
	unsigned short x_col = blockIdx.x * blockDim.x + threadIdx.x;	

	float u = x_col / (float)width;
	float v = y_row / (float)height;
	u = u * 2.0f - 1.0f;
	v = v * 2.0f - 1.0f;
	float frequency = 4.0f;
	float w = sinf(u * frequency + time) * cosf(v * frequency + time) * 0.5f;
	
	pPosition[y_row * width + x_col] = make_float4(u, w, v, 1.0f);
}

void launchSineWaveFromCudaKernel(float4* pPosition, unsigned int gMeshWidth, unsigned int gMeshHeight, float time)
{
	dim3 block = dim3(8, 8, 1);
	dim3 grid = dim3(ceil(gMeshWidth / block.x), ceil(gMeshHeight / block.y), 1);

	fprintf(gpFile, "\nIn launchSineWaveFromCudaKernel() before sineWave_kernel\n");

	sineWave_kernel <<<grid, block >>> (pPosition, gMeshWidth, gMeshHeight, time);

	fprintf(gpFile, "\nIn launchSineWaveFromCudaKernel() after sineWave_kernel\n");
}
