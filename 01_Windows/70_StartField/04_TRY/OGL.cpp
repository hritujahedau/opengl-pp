// header file declaration
#include <windows.h>
#include<stdio.h>

#include <GL\glew.h>
#include <gl/GL.h>
#include "OGL.h"
#include "vmath.h"
#include <cmath>
#include <stdlib.h>

#pragma comment(lib, "C:\\glew-2.1.0\\lib\\Release\\Win32\\glew32.lib")
#pragma comment(lib, "opengl32.lib")

#define WIDTH 1000
#define HIGHT 800

#define NUM_STARS	3000
static unsigned int seed = 0X13371337;

float random_float();

using namespace vmath;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int width, hight;

enum
{
	HRH_ATTRIBUTE_POSITION = 0 ,
	HRH_ATTRIBUTE_COLOR ,
	HRH_ATTRIBUTE_NORMAL ,
	HRH_ATTRIBUTE_TEXTURE_0
};

GLuint gVertexShaderObject_starfield;
GLuint gFragmentShaderObject_starfield;
GLuint gShaderProgramObject_starfield;

GLuint gStarVao, gStarVbo_position, gStarVbo_color, gStarVbo;

GLuint gProjUniform, gTimeUniform;
GLuint gTextureSamplerUniform;
GLuint star_texture = 0;

mat4 gPerspectiveProjectionMatrix;

float time = 0.0f;

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	void Initialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("h_ogl");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 

	if (fopen_s(&gpFile, "windows.log", "w") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("OGL"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HIGHT/2 ,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow) {
				Display();
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	void Resize(int,int);

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int,int);
	void Uninitialize();
	bool LoadGLTexture(GLuint*, TCHAR[]);
	void LoadTexture(const char* filename, unsigned int* tex);

	// Function declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}
	
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		DestroyWindow(ghwnd);
	}

	//OpenGL Logs
	fprintf(gpFile, "OpenGL Logs");
	fprintf(gpFile, "OpenGL Vendor: %s \n ", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer: %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "OpenGL Shading Language : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	fprintf(gpFile, "Open Enabled extension\n");
	GLint numExtension = 0;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	fprintf(gpFile, "Open Enabled total extension %d \n", numExtension);

	for (int i = 0; i < numExtension; i++)
	{		
		//fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	/********** VERTEX SHADER ***********/
	// CREATE SHADER

	gVertexShaderObject_starfield = glCreateShader(GL_VERTEX_SHADER);
	
	//provide souce code to shader
	const GLchar* vertexShaderSourceCode =  " #version 450 core\n													" \
											" layout (location = 0) in vec4 position;								" \
											" layout (location = 1) in vec4 color;									" \
											" uniform float time;													" \
											" uniform mat4 u_projMatrix;											" \
											" flat out vec4 starColor;												" \
											" void main()															" \
											" {																		" \
											"	vec4 newVertex = position;											" \
											"	newVertex.z += time;												" \
											"	newVertex.z = fract(newVertex.z);									" \
											"	float size = (20.0 * newVertex.z * newVertex.z);					" \
											"	starColor = smoothstep(1.0, 7.0, size) * color;						" \
											"	newVertex.z = (999.9 * newVertex.z) - 1000.0;						" \
											"	gl_Position = u_projMatrix * newVertex;								" \
											"	gl_PointSize = size;												" \
											" }																		" ;

	glShaderSource(gVertexShaderObject_starfield, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject_starfield);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject_starfield, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_starfield, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_starfield, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObject_starfield = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCode =  " #version 450 core\n													" \
											" layout (location = 0) out vec4 color;									" \
											" uniform sampler2D u_texture_sampler;									" \
											" flat in vec4 starColor;												" \
											" void main()															" \
											" {																		" \
											"	color = starColor * texture(u_texture_sampler,gl_PointCoord)  ;		" \
											" }																		" ;
	// * texture(u_texture_sampler,gl_PointCoord)

	glShaderSource(gFragmentShaderObject_starfield, 1, (const char**)&fragmentShaderSourceCode, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject_starfield);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObject_starfield, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_starfield, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_starfield, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObject_starfield = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject_starfield, gVertexShaderObject_starfield);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObject_starfield, gFragmentShaderObject_starfield);

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObject_starfield);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject_starfield, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_starfield, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_starfield, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// get MVP uniform location
	gProjUniform = glGetUniformLocation(gShaderProgramObject_starfield, "u_projMatrix");
	gTimeUniform = glGetUniformLocation(gShaderProgramObject_starfield, "time");
	gTextureSamplerUniform = glGetUniformLocation(gShaderProgramObject_starfield, "u_texture_sampler");

	glGenVertexArrays(1, &gStarVao);
	glBindVertexArray(gStarVao);

	// glMapBufferRange(GL_ARRAY_BUFFER, 0, NUM_STARS * sizeof(star_t), GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
	struct star_t
	{
		vec3 position;
		vec3 color;
	};

	glGenBuffers(1, &gStarVbo);
	glBindBuffer(GL_ARRAY_BUFFER, gStarVbo);
	glBufferData(GL_ARRAY_BUFFER, NUM_STARS * sizeof(star_t), NULL, GL_STATIC_DRAW);

	star_t* star = (star_t*)glMapBufferRange(GL_ARRAY_BUFFER, 0, NUM_STARS * sizeof(star_t), GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);

	for (int i = 0; i < NUM_STARS; i++)
	{
		star[i].position[0] = (random_float() * 2.0f - 1.0f) * 400.0f;
		star[i].position[1] = (random_float() * 2.0f - 1.0f) * 400.0f;
		star[i].position[2] = random_float();

		star[i].color[0] = 0.8f + random_float() * 0.2f;
		star[i].color[1] = 0.8f + random_float() * 0.2f;
		star[i].color[2] = 0.8f + random_float() * 0.2f;
	}

	glUnmapBuffer(GL_ARRAY_BUFFER);

	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(star_t), NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);

	glVertexAttribPointer(HRH_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, sizeof(star_t), (void *)sizeof(vec3));
	glEnableVertexAttribArray(HRH_ATTRIBUTE_COLOR);

	LoadGLTexture(&star_texture, ID_STAR_BITMAP);
	
	/***************************************************************************************************************/
		
	// we will always cull face for better performance
	//glEnable(GL_CULL_FACE);

	gPerspectiveProjectionMatrix = mat4::identity();

	Resize(WIDTH, HIGHT);
}


float random_float()
{
	float res;
	unsigned int tmp;

	seed *= 16807;

	tmp = seed ^ (seed >> 4) ^ (seed << 15);

	*((unsigned int*)&res) = (tmp >> 9) | 0x3F800000;

	return (res - 1.0f);
}

// Texture code
bool LoadGLTexture(GLuint* texture, TCHAR resourceID[])
{
	//VARIABLE DECLARATION
	bool bResult = false;
	HBITMAP hBitmap = NULL;
	BITMAP bmp;

	//CODE
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), resourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap)
	{
		bResult = true;
		GetObject(hBitmap, sizeof(BITMAP), &bmp);

		// from here start OpenGL Texture code
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1); //IN FFP 4
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);

		//setting texture parameter
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		// following call will actually push the data with help of graphics driver  GL_BGR
		glTexImage2D(GL_TEXTURE_2D, 0, 3, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, bmp.bmBits);
		glGenerateMipmap(GL_TEXTURE_2D);
		
		DeleteObject(hBitmap);
	}
	return bResult;
}


void Resize(int width, int height) {
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) height);

	gPerspectiveProjectionMatrix = vmath::perspective(50.0f, ((GLfloat)width / height), ((GLfloat)height / width), 1000.0f); // no 0.1f

}

void Display() {
	// CODE
	
	float t;

	static const GLfloat black[] = {0.0f, 0.0f, 0.0f, 0.0f};
	static const GLfloat one[] = { 1.0f };
	
	glClearBufferfv(GL_COLOR, 0, black);
	glClearBufferfv(GL_DEPTH, 0, one);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glUseProgram(gShaderProgramObject_starfield);

	t = time;
	t *= 0.1f;
	t -= floor(t);

	glUniform1f(gTimeUniform, t);
	glUniformMatrix4fv(gProjUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);	

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, star_texture);
	glUniform1i(gTextureSamplerUniform, 0);

	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);

	glBindVertexArray(gStarVao);
	glEnable(GL_POINT_SPRITE);
	glEnable(GL_PROGRAM_POINT_SIZE);
	glDrawArrays(GL_POINTS, 0, NUM_STARS);

	glUseProgram(0);
	
	time = time + 0.0001f;
	if (time > 1.0f)
	{
		time = 0.0f;
	}
	
	SwapBuffers(ghdc);
}

void Uninitialize() {

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	// destroy gVao_triangle
	if (gStarVao)
	{
		glDeleteVertexArrays(1, &gStarVao);
		gStarVao = 0;
	}

	if (gStarVbo_color)
	{
		glDeleteBuffers(1, &gStarVbo_color);
		gStarVbo_color = 0;
	}

	if (gStarVbo_position)
	{
		glDeleteBuffers(1, &gStarVbo_position);
		gStarVbo_position = 0;
	}

	if (star_texture)
	{
		glDeleteTextures(1, &star_texture);
	}

	// deatch vertex shader from shader program object
	glDetachShader(gShaderProgramObject_starfield, gVertexShaderObject_starfield);

	// detach fragment shader from shader program obejct
	glDetachShader(gShaderProgramObject_starfield, gFragmentShaderObject_starfield);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject_starfield);

	gVertexShaderObject_starfield = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject_starfield);
	gFragmentShaderObject_starfield = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject_starfield);
	gShaderProgramObject_starfield = 0;

	// unlink shader program
	glUseProgram(0);

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile) {
		fprintf(gpFile, "\n Closing File Successfully");
		fclose(gpFile);
	}
}
