// header file declaration
#include <windows.h>
#include<stdio.h>
#include <stdlib.h>

#include <GL\glew.h>
#include <gl/GL.h>
#include "OGL.h"
#include "vmath.h"

// #include "ft2build.h"
// #include ".\\freetype\\freetype.h"

#include <ft2build.h>
#include <freetype/freetype.h>

#pragma comment(lib, "C:\\glew-2.1.0\\lib\\Release\\x64\\glew32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "freetype.lib")

#define WIDTH 1000
#define HIGHT 800

using namespace vmath;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int width, hight;

enum
{
	HRH_ATTRIBUTE_POSITION = 0 ,
	HRH_ATTRIBUTE_COLOR ,
	HRH_ATTRIBUTE_NORMAL ,
	HRH_ATTRIBUTE_TEXTURE_0
};

GLuint gVertexShaderObject_font;
GLuint gFragmentShaderObject_font;
GLuint gShaderProgramObject_font;

GLuint gVao_font;
GLuint gVbo_font;

GLuint gProjectionUniform_font;
GLuint gTextUniform_font;
GLuint gTexColorUniform_font;

mat4 gOrthographicProjectionMatrix_font;

struct Character
{
	unsigned int TextureID;
	vec2 Size;
	vec2 Bearing;
	unsigned int Advance;
};

struct Character Characters[128];

struct planetItemInfo
{
	char* str;
	size_t sizeOfStr;
};

struct planetItemMetaInfo
{
	struct planetItemInfo* m_planetItemInfo;
};

int selectedPlanetID = 0;

GLuint gVertexShaderObject_rectangle;
GLuint gFragmentShaderObject_rectangle;
GLuint gShaderProgramObject_rectangle;

GLuint gVao_rectangle;
GLuint gVbo_rectangle_position;
GLuint gVbo_rectangle_color;

GLuint gMVPUniform_rectangle;

mat4 gPerspectiveProjectionMatrix_rectangle;

static float font_y = -10.0f;

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	void Initialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("h_ogl");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 

	if (fopen_s(&gpFile, "windows.log", "w") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("OGL"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HIGHT/2 ,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow) {
				
			}
			Display();
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	void Resize(int,int);

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int,int);
	int LoadFreeTypeLibrary();
	void GenerateAndBindRectangle();
	void LoadRectangleShader();
	void Uninitialize();

	// Function declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}
	
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		DestroyWindow(ghwnd);
	}

	//OpenGL Logs
	fprintf(gpFile, "OpenGL Logs");
	fprintf(gpFile, "OpenGL Vendor: %s \n ", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer: %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "OpenGL Shading Language : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	fprintf(gpFile, "Open Enabled extension\n");
	GLint numExtension = 0;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	fprintf(gpFile, "Open Enabled total extension %d \n", numExtension);

	for (int selectedPlanetID = 0; selectedPlanetID < numExtension; selectedPlanetID++)
	{		
		//fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,selectedPlanetID));
	}

	/********** VERTEX SHADER ***********/
	// CREATE SHADER

	gVertexShaderObject_font = glCreateShader(GL_VERTEX_SHADER);
	
	//provide souce code to shader
	const GLchar* vertexShaderSourceCode =  
		" #version 450 core															\n " \
		" layout (location = 0) in vec4 vertex;										\n " \
		" out vec2 TexCoords;														\n " \
		" 																			\n " \
		" uniform mat4 projection;													\n " \
		" 																			\n " \
		" void main()																\n " \
		" {																			\n " \
		" 	gl_Position = projection * vec4 (vertex.xy, 0.0, 1.0);					\n " \
		" 	TexCoords = vertex.zw;													\n " \
		" }																			\n " ;

	glShaderSource(gVertexShaderObject_font, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject_font);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject_font, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_font, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_font, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObject_font = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCode = 
		" #version 450 core																\n " \
		" in vec2 TexCoords;															\n " \
		" out vec4 color;																\n " \
		" 																				\n " \
		" uniform sampler2D text;														\n " \
		" uniform vec3 textColor;														\n " \
		" 																				\n " \
		" void main()																	\n " \
		" {																				\n " \
		" 	vec4 sampled = vec4(1.0, 1.0, 1.0, texture(text, TexCoords).r);				\n " \
		" 	color = vec4(textColor, 1.0) * sampled;										\n " \
		" }																				\n " ;	

	glShaderSource(gFragmentShaderObject_font, 1, (const char**)&fragmentShaderSourceCode, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject_font);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObject_font, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_font, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_font, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObject_font = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject_font, gVertexShaderObject_font);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObject_font, gFragmentShaderObject_font);

	// pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(gShaderProgramObject_font, HRH_ATTRIBUTE_POSITION, "vertex");

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObject_font);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject_font, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_font, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_font, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// get MVP uniform location
	gProjectionUniform_font = glGetUniformLocation(gShaderProgramObject_font, "projection");
	gTextUniform_font = glGetUniformLocation(gShaderProgramObject_font, "text");
	gTexColorUniform_font = glGetUniformLocation(gShaderProgramObject_font, "textColor");
	
	LoadRectangleShader();
	
	glGenVertexArrays(1, &gVao_font);
	glBindVertexArray(gVao_font);

	glGenBuffers(1, &gVbo_font);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_font);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 6 * 4, NULL, GL_DYNAMIC_DRAW);	
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	GenerateAndBindRectangle();

	if (LoadFreeTypeLibrary() != 0)
	{
		fprintf(gpFile, "Failed To load LoadFreeTypeLibrary(), Exiting...");
		DestroyWindow(ghwnd);
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	// we will always cull face for better performance
	gOrthographicProjectionMatrix_font = mat4::identity();

	gPerspectiveProjectionMatrix_rectangle = mat4::identity();

	Resize(WIDTH, HIGHT);
}

void LoadRectangleShader()
{
	/********** VERTEX SHADER ***********/
// CREATE SHADER

	gVertexShaderObject_rectangle = glCreateShader(GL_VERTEX_SHADER);

	//provide souce code to shader
	const GLchar* vertexShaderSourceCode = "#version 450 core\n"\
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"out vec4 out_color; " \
		"uniform mat4 u_mvpMatrix;" \
		"void main()"\
		"{" \
		"out_color = vColor;" \
		"gl_Position = u_mvpMatrix * vPosition;"\
		"}";

	glShaderSource(gVertexShaderObject_rectangle, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject_rectangle);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject_rectangle, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_rectangle, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_rectangle, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObject_rectangle = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCode = "#version 450 core\n"\
		"in vec4 out_color;" \
		"out vec4 FragColor; " \
		"void main()"\
		"{"\
		"FragColor = out_color;" \
		"}";


	glShaderSource(gFragmentShaderObject_rectangle, 1, (const char**)&fragmentShaderSourceCode, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject_rectangle);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObject_rectangle, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_rectangle, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_rectangle, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObject_rectangle = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject_rectangle, gVertexShaderObject_rectangle);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObject_rectangle, gFragmentShaderObject_rectangle);

	// pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(gShaderProgramObject_rectangle, HRH_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject_rectangle, HRH_ATTRIBUTE_COLOR, "vColor");

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObject_rectangle);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject_rectangle, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_rectangle, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_rectangle, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// get MVP uniform location
	gMVPUniform_rectangle = glGetUniformLocation(gShaderProgramObject_rectangle, "u_mvpMatrix");

}

void GenerateAndBindRectangle()
{
	float value_width = 80.0f;
	float value_height = 38.0f;
	GLfloat rectangleVertices[] = {
		 value_width,  value_height, 0.0f,
		-value_width,  value_height, 0.0f,
		-value_width, -value_height, 0.0f,
		 value_width, -value_height, 0.0f ,
		 value_width,  value_height, 0.0f,
	};
	GLfloat rectangleColor[] = {
		0.0f, 1.0f, 1.0f,
		1.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 1.0f, 1.0f
	};

	glGenVertexArrays(1, &gVao_rectangle);
	glBindVertexArray(gVao_rectangle);

	/***********************************gVao_font*********************************************************/

	// position
	glGenBuffers(1, &gVbo_rectangle_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_rectangle_position); //Bind gVbo_font
	glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleVertices), rectangleVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// color
	glGenBuffers(1, &gVbo_rectangle_color);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_rectangle_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleColor), rectangleColor, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

}

int LoadFreeTypeLibrary()
{
	FT_Library ft;

	if (FT_Init_FreeType(&ft))
	{
		fprintf(gpFile, "Could not init FreeType Library");
		return -1;
	}

	char font_name[] = "D:\\HRITUJA\\RTR\\CLASS_ASSIGNMENT\\hrituja_rtr2020\\02_PP\\80_Font_GPUTEKS_For_Project\\10\\fonts\\Gputeks-Bold.ttf";

	FT_Face face;
	if (FT_New_Face(ft, font_name, 0, &face) != 0)
	{
		fprintf(gpFile, "Failed to load font");
		return -1;
	}

	FT_Set_Pixel_Sizes(face, 20, 25);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	int selectedPlanetID = 0;

	for (unsigned char c = 0; c < 128; c++, selectedPlanetID++)
	{
		if (FT_Load_Char(face, c, FT_LOAD_RENDER))
		{
			fprintf(gpFile, "Failed to load GLyph for %c ", c);
			continue;
		}

		unsigned int texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);

		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			GL_RED,
			face->glyph->bitmap.width,
			face->glyph->bitmap.rows,
			0,
			GL_RED,
			GL_UNSIGNED_BYTE,
			face->glyph->bitmap.buffer
			);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		Characters[selectedPlanetID].TextureID = texture;
		Characters[selectedPlanetID].Size = vec2(face->glyph->bitmap.width, face->glyph->bitmap.rows);
		Characters[selectedPlanetID].Bearing = vec2(face->glyph->bitmap_left, face->glyph->bitmap_top);
		Characters[selectedPlanetID].Advance = (unsigned int)(face->glyph->advance.x);
		
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	FT_Done_Face(face);
	FT_Done_FreeType(ft);

	return 0;
}

void Resize(int width, int height) {
	if (height == 0)
		height = 1;

	width = 1000;
	height = 770;

	glViewport(200, -350, (GLsizei) width,(GLsizei) height);

	float value = 100.0f;

	// glOrtho(left, right, bottom, top, near, far);
	if (width <= height)
	{
		gOrthographicProjectionMatrix_font = ortho(-value, value,
												(-value * (height / width)),
												(value * (height / width)),
												-value,
												value
											);
	}
	else
	{
		gOrthographicProjectionMatrix_font = ortho(-value, value,
												(-value * (width / height)),
												(value *(width / height)),
												-value,
												value
											);
	}

	gPerspectiveProjectionMatrix_rectangle = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); // no 0.1f

}

void RenderText(char* text, int strLength, float x, float y, float scale, vec3 color)
{
	glUniform3fv(gTexColorUniform_font, 1, color);

	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(gVao_font);

	for (int selectedPlanetID = 0; selectedPlanetID < strLength; selectedPlanetID++)
	{
		Character ch = Characters[(int)text[selectedPlanetID]];

		float xpos = x + ch.Bearing[0] * scale;
		float ypos = y - (ch.Size[1] - ch.Bearing[1]) * scale;

		float w = ch.Size[0] * scale;
		float h = ch.Size[1] * scale;

		float vertices[6][4] =
							{
								{ xpos,		ypos + h, 0.0f, 0.0f },
								{ xpos,		ypos	, 0.0f, 1.0f },
								{ xpos + w, ypos	, 1.0f, 1.0f },

								{ xpos,		ypos + h, 0.0f, 0.0f },
								{ xpos + w,	ypos	, 1.0f, 1.0f },
								{ xpos + w,	ypos + h, 1.0f, 0.0f },
							};

		glBindTexture(GL_TEXTURE_2D, ch.TextureID);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_font);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glDrawArrays(GL_TRIANGLES, 0, 6);
		x += (ch.Advance >> 6) * scale;
	}

	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Display() 
{
	// function declarations
	void RenderText(char * text, int strLen, float x, float y, float scale, vec3 color);
	void hallowRectangle();

	// variable declarations
	

	char szMercury_PlanetName[] = "Mercury";
	char szMercury_Diameter[] = "3,031 miles (4,878 km)";
	char szMercury_Orbit[] = "88 Earth days";
	char szMercury_Day[] = "58.6 Earth days";
	char szMercury_NumSatelite[] = "0";
	char szMercury_NameSatelite[] = NULL;

	char szVenus_PlanetName[] = "Venus";
	char szVenus_Diameter[] = "7,521 miles (12,104 km)";
	char szVenus_Orbit[] = "225 Earth days";
	char szVenus_Day[] = "241 Earth days";
	char szVenus_NumSatelite[] = "0";
	char szVenus_NameSatelite[] = "NULL";

	char szEarth_PlanetName[] = "Earth";
	char szEarth_Diameter[] = "7,926 miles (12,760 km)";
	char szEarth_Orbit[] = "365.24 days";
	char szEarth_Day[] = "23 hours, 56 minutes";
	char szEarth_NumSatelite[] = "1";
	char szEarth_NameSatelite[] = "Moon";

	char szMars_PlanetName[] = "Mars";
	char szMars_Diameter[] = "4,217 miles (6,787 km)";
	char szMars_Orbit[] = "687 Earth days";
	char szMars_Day[] = "24 hours, 37 minutes";
	char szMars_NumSatelite[] = "2";
	char szMars_NameSatelite[] = "Phobos, Deimos";

	char szJupitor_PlanetName[] = "Jupitor";
	char szJupitor_Diameter[] = "86,881 miles (139,822 km)";
	char szJupitor_Orbit[] = "11.9 Earth years";
	char szJupitor_Day[] = "9.8 Earth hours";
	char szJupitor_NumSatelite[] = "4";
	char szJupitor_NameSatelite[] = "Callisto, Ganymede, Europa, Io";

	char szSaturn_PlanetName[] = "Saturn";
	char szSaturn_Diameter[] = "74,900 miles (120,500 km)";
	char szSaturn_Orbit[] = "29.5 Earth years";
	char szSaturn_Day[] = "About 10.5 Earth hours";
	char szSaturn_NumSatelite[] = "62";
	char szSaturn_NameSatelite[] = "Titan, Mimas, Enceladus etc.";
	
	char szUranus_PlanetName[] = "Uranus";
	char szUranus_Diameter[] = "31,763 miles (51,120 km)";
	char szUranus_Orbit[] = "84 Earth years";
	char szUranus_Day[] = "18 Earth hours";
	char szUranus_NumSatelite[] = "27";
	char szUranus_NameSatelite[] = "Ariel, Miranda, Umbriel, etc.";

	char szNaptune_PlanetName[] = "Naptune";
	char szNaptune_Diameter[] = "30,775 miles (49,530 km)";
	char szNaptune_Orbit[] = "165 Earth years";
	char szNaptune_Day[] = "19 Earth hours";
	char szNaptune_NumSatelite[] = "14";
	char szNaptune_NameSatelite[] = "Triton, Hippocamp, Proteus, etc.";
	
	struct planetItemInfo Mercury[] =
	{
		{ szMercury_PlanetName,	  strlen(szMercury_PlanetName) },
		{ szMercury_Diameter,	  strlen(szMercury_Diameter) },
		{ szMercury_Orbit,		  strlen(szMercury_Orbit) },
		{ szMercury_Day,		  strlen(szMercury_Day) },
		{ szMercury_NumSatelite,  strlen(szMercury_NumSatelite) },
		{ szMercury_NameSatelite, strlen(szMercury_NameSatelite) }
	};

	struct planetItemInfo Venus[] =
	{
		{ szVenus_PlanetName,	  strlen(szVenus_PlanetName) },
		{ szVenus_Diameter,		strlen(szVenus_Diameter) },
		{ szVenus_Orbit,		strlen(szVenus_Orbit) },
		{ szVenus_Day,			strlen(szVenus_Day) },
		{ szVenus_NumSatelite,	strlen(szVenus_NumSatelite) },
		{ szVenus_NameSatelite, strlen(szVenus_NameSatelite) }
	};

	struct planetItemInfo Earth[] =
	{
		{ szEarth_PlanetName,	  strlen(szEarth_PlanetName) },
		{ szEarth_Diameter, strlen(szEarth_Diameter) },
		{ szEarth_Orbit, strlen(szEarth_Orbit) },
		{ szEarth_Day, strlen(szEarth_Day) },
		{ szEarth_NumSatelite, strlen(szEarth_NumSatelite) },
		{ szEarth_NameSatelite, strlen(szEarth_NameSatelite) }
	};	

	struct planetItemInfo Mars[] =
	{
		{ szMars_PlanetName,	  strlen(szMars_PlanetName) },
		{ szMars_Diameter, strlen(szMars_Diameter) },
		{ szMars_Orbit, strlen(szMars_Orbit) },
		{ szMars_Day, strlen(szMars_Day) },
		{ szMars_NumSatelite, strlen(szMars_NumSatelite) },
		{ szMars_NameSatelite, strlen(szMars_NameSatelite) }
	};

	struct planetItemInfo Jupitor[] =
	{
		{ szJupitor_PlanetName,	  strlen(szJupitor_PlanetName) },
		{ szJupitor_Diameter, strlen(szJupitor_Diameter) },
		{ szJupitor_Orbit, strlen(szJupitor_Orbit) },
		{ szJupitor_Day, strlen(szJupitor_Day) },
		{ szJupitor_NumSatelite, strlen(szJupitor_NumSatelite) },
		{ szJupitor_NameSatelite, strlen(szJupitor_NameSatelite) }
	};

	struct planetItemInfo Saturn[] =
	{
		{ szSaturn_PlanetName,	  strlen(szSaturn_PlanetName) },
		{ szSaturn_Diameter, strlen(szSaturn_Diameter) },
		{ szSaturn_Orbit, strlen(szSaturn_Orbit) },
		{ szSaturn_Day, strlen(szSaturn_Day) },
		{ szSaturn_NumSatelite, strlen(szSaturn_NumSatelite) },
		{ szSaturn_NameSatelite, strlen(szSaturn_NameSatelite) }
	};

	struct planetItemInfo Uranus[] =
	{
		{ szUranus_PlanetName,	  strlen(szUranus_PlanetName) },
		{ szUranus_Diameter, strlen(szUranus_Diameter) },
		{ szUranus_Orbit, strlen(szUranus_Orbit) },
		{ szUranus_Day, strlen(szUranus_Day) },
		{ szUranus_NumSatelite, strlen(szUranus_NumSatelite) },
		{ szUranus_NameSatelite, strlen(szUranus_NameSatelite) }
	};

	struct planetItemInfo Naptune[] =
	{
		{ szNaptune_PlanetName,	  strlen(szNaptune_PlanetName) },
		{ szNaptune_Diameter, strlen(szNaptune_Diameter) },
		{ szNaptune_Orbit, strlen(szNaptune_Orbit) },
		{ szNaptune_Day, strlen(szNaptune_Day) },
		{ szNaptune_NumSatelite, strlen(szNaptune_NumSatelite) },
		{ szNaptune_NameSatelite, strlen(szNaptune_NameSatelite) }
	};

	struct planetItemMetaInfo planeMetaInformation[] =
	{
		Mercury,
		Venus,
		Earth,
		Mars,
		Jupitor,
		Saturn,
		Uranus,
		Naptune
	};

	const int numberOfPlanetInfo = sizeof(planeMetaInformation) / sizeof(planeMetaInformation[0]);

	char szPlanetName[]		= "                   Planet Name : ";
	char szDiameter[]		= "                       Diameter : " ;
	char szOrbit[]			= "                            Orbit : " ;
	char szDay[]			= "                             Day : " ;
	char szNRSatellites[]	= " Number of Natural Satellites : " ;
	char szNameSatellites[]	= "   Name of Natural Satellites : " ;

	struct Titles
	{
		char* strTitles;
		size_t sizeOfTitles;
	};
	
	struct Titles titles[] =
	{
		{szPlanetName, sizeof(szPlanetName)},
		{ szDiameter, sizeof(szDiameter) },
		{ szOrbit, sizeof(szOrbit) },			
		{ szDay, sizeof(szDay) },
		{ szNRSatellites, sizeof(szNRSatellites) },
		{ szNameSatellites, sizeof(szNameSatellites) }
	};

	vec3 PlanetInfoColorArray[]
	{		
		vec3( 0.0f, 1.0f, 0.0f ),
		vec3( 0.0f, 0.0f, 1.0f ),
		vec3( 1.0f, 10.f, 0.0f ),
		vec3( 1.0f, 0.0f, 0.0f),
		vec3( 0.0f, 1.0f, 1.0f ),
		vec3( 1.0f, 0.0f, 1.0f ),		
	};

	// CODE
	glClear(GL_COLOR_BUFFER_BIT);
	glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	glUseProgram(gShaderProgramObject_font);

	glUniformMatrix4fv(gProjectionUniform_font, 1, GL_FALSE, gOrthographicProjectionMatrix_font);
	
	float differenceInLine = font_y;
	for (int j = 0; j < 6; j++)
	{
		const int sizeOfStr = (int)titles[j].sizeOfTitles + (int)planeMetaInformation[selectedPlanetID].m_planetItemInfo[j].sizeOfStr;
		char str[300]; 

		strcpy(str, "");
		strcat(str, titles[j].strTitles);
		strcat(str, planeMetaInformation[selectedPlanetID].m_planetItemInfo[j].str);

		differenceInLine = differenceInLine - 12;

		RenderText 
		( 
			str, 
			sizeOfStr , 
			-50.0f, 
			differenceInLine,
			0.2f, 
			PlanetInfoColorArray[j]
		);

		if (j == 3 && strcmp("0", planeMetaInformation[selectedPlanetID].m_planetItemInfo[j].str) == 0 )
		{
			break;
		}
	}

	glUseProgram(0);

	glUseProgram(gShaderProgramObject_rectangle);

	hallowRectangle();

	glUseProgram(0);

	if (font_y < 100)
	{
		font_y += 0.01;
	}

	SwapBuffers(ghdc);
}

void hallowRectangle()
{
	// VARIABLE DECLARATION	
	static float rect_y = -50.0f; // till 60.0
	// CODE
	//mat4 translateMatrix = vmath::translate(0.5f, 60.0f, -1.0f);
	mat4 translateMatrix = vmath::translate(20.0f, rect_y, -1.0f);
	mat4 modelViewMatrix = translateMatrix;
	mat4 modelViewProjectionMatrix = mat4::identity();

	modelViewProjectionMatrix = gOrthographicProjectionMatrix_font * modelViewMatrix;

	glUniformMatrix4fv(gMVPUniform_rectangle, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(gVao_rectangle);
	glDrawArrays(GL_LINES, 0, 2);
	glDrawArrays(GL_LINES, 1, 2);
	glDrawArrays(GL_LINES, 2, 2);
	glDrawArrays(GL_LINES, 3, 2);
	glBindVertexArray(0);

	if (rect_y < 60.0f)
	{
		rect_y += 0.01f;
	}

}

void Uninitialize() {

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	// destroy vao
	if (gVao_font)
	{
		glDeleteVertexArrays(1, &gVao_font);
		gVao_font = 0;
	}

	// destroy vbo
	if (gVbo_font)
	{
		glDeleteBuffers(1, &gVbo_font);
		gVbo_font = 0;
	}

	// deatch vertex shader from shader program object
	glDetachShader(gShaderProgramObject_font, gVertexShaderObject_font);

	// detach fragment shader from shader program obejct
	glDetachShader(gShaderProgramObject_font, gFragmentShaderObject_font);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject_font);

	gVertexShaderObject_font = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject_font);
	gFragmentShaderObject_font = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject_font);
	gShaderProgramObject_font = 0;

	// unlink shader program
	glUseProgram(0);

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile) {
		fprintf(gpFile, "\n Closing File Successfully");
		fclose(gpFile);
	}
}
