#pragma once
#include <windows.h>
#include <gl\GL.h>
#include "vmath.h"

#define MYICON 1287
#define IDI_MYICON MAKEINTRESOURCE(MYICON)

typedef struct node
{
	vmath::mat4 matrix;
	struct node* prev;
} node;

node* head = NULL;