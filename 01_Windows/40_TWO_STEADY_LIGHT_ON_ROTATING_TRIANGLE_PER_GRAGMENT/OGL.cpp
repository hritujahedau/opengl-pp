// header file declaration
#include <windows.h>
#include<stdio.h>

#include <GL\glew.h>
#include <gl/GL.h>
#include "OGL.h"
#include "Sphere.h"

#pragma comment(lib, "C:\\glew-2.1.0\\lib\\Release\\Win32\\glew32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "Sphere.lib")

#define WIDTH 1000
#define HIGHT 800

using namespace vmath;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int width, hight;

enum
{
	HRH_ATTRIBUTE_POSITION = 0 ,
	HRH_ATTRIBUTE_COLOR ,
	HRH_ATTRIBUTE_NORMAL ,
	HRH_ATTRIBUTE_TEXTURE_0
};

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao_pyramid;
GLuint gVbo_pyramid_position;
GLuint gVbo_pyramid_normals;

GLuint gModelUniform;
GLuint gViewUniform;
GLuint gPerspectiveUniform;

GLuint gLight_0_AmbientUniform;
GLuint gLight_0_SpecularUniform;
GLuint gLight_0_DiffusedUniform;
GLuint gLight_0_PossitionUniform;


GLuint gLight_1_AmbientUniform;
GLuint gLight_1_SpecularUniform;
GLuint gLight_1_DiffusedUniform;
GLuint gLight_1_PossitionUniform;

GLuint gMaterialSpecularUniform;
GLuint gMaterialAmbientUniform;
GLuint gMaterialDiffuseUniform;
GLuint gMaterialShininessUniform;

GLuint gIsLightOnUniform;

int isLightOn = 0;

mat4 gPerspectiveProjectionMatrix;

GLfloat lightPosition_0[] = { 100.0f, 100.0f, 100.0f, 1.0f };
GLfloat lightAmbient_0[] = { 1.0f, 0.0f, 0.0f};
GLfloat lightDiffuse_0[] = { 1.0f, 0.0f, 0.0f};
GLfloat lightSpecular_0[] = { 0.0f, 0.0f, 1.0f};

GLfloat lightPosition_1[] = { -100.0f, 100.0f, 100.0f, 1.0f };
GLfloat lightAmbient_1[] = { 0.0f, 0.0f, 1.0f};
GLfloat lightDiffuse_1[] = { 0.0f, 0.0f, 1.0f};
GLfloat lightSpecular_1[] = { 0.0f, 0.0f, 1.0 };

GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f};
GLfloat materialDiffuse[] = { 1.0f, 1.0f, 1.0f};
GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0 };
GLfloat materialShininess = 128.0f;

bool bAnimate = false;

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	void Initialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("h_ogl");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 

	if (fopen_s(&gpFile, "windows.log", "w") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("OGL"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HIGHT/2 ,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {			
			if (gbActiveWindow) {
				Display();
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	void Resize(int,int);

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_CHAR:
			switch (wParam)
			{
			case 'a':
			case 'A':
				if (bAnimate == false)
				{
					bAnimate = true;
				}
				else
				{
					bAnimate = false;
				}
				break;
			case 'l':
			case 'L':
				if (isLightOn == 0)
				{
					isLightOn = 1;
				}
				else
				{
					isLightOn = 0;
				}
				break;
			}
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int,int);
	void Uninitialize();


	// Function declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}
	
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		DestroyWindow(ghwnd);
	}

	//OpenGL Logs
	fprintf(gpFile, "OpenGL Logs");
	fprintf(gpFile, "OpenGL Vendor: %s \n ", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer: %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "OpenGL Shading Language : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	fprintf(gpFile, "Open Enabled extension\n");
	GLint numExtension = 0;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	fprintf(gpFile, "Open Enabled total extension %d \n", numExtension);

	for (int i = 0; i < numExtension; i++)
	{		
		//fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	/********** VERTEX SHADER ***********/
	// CREATE SHADER

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	
	//provide souce code to shader
	const GLchar* vertexShaderSourceCode =  "#version 450 core\n"  \
	"in vec4 v_position;" \
	"in vec3 v_normals; " \
	"uniform mat4 u_modelMatrix;" \
	"uniform mat4 u_viewMatrix;" \
	"uniform mat4 u_projectionMatrix;" \
	"uniform int u_isLightOn;" \
	"uniform vec4 u_light_0_position; " \
	"uniform vec4 u_light_1_position; " \
	"out vec3 light_direction_0; "\
	"out vec3 light_direction_1; "\
	"out vec3 transformed_normal;"\
	"out vec3 view_vector;"\
	"void main()" \
	"{" \
	"if (u_isLightOn == 1) " \
	"{" \
		"vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" \
		"transformed_normal =  mat3( u_viewMatrix * u_modelMatrix ) * v_normals;" \
		"light_direction_0 = vec3 ( u_light_0_position  - eye_coordinates);" \
		"light_direction_1 = vec3 ( u_light_1_position  - eye_coordinates);" \
		"view_vector =  vec3 ( -eye_coordinates);" \
	"}" \
	"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" \
	"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCode = "#version 450 core\n" \
		"vec3 phoung_ads_lighting;" \
		"out vec4 FragColor; " \
		"in vec3 light_direction_0; "\
		"in vec3 light_direction_1; "\
		"in vec3 transformed_normal;"\
		"in vec3 view_vector;"\
		"vec3 normalized_light_direction_0; "\
		"vec3 normalized_light_direction_1; "\
		"vec3 normalized_transformed_normal;"\
		"vec3 normalized_view_vector;"\
		"uniform vec3 u_light_0_Ambient;" \
		"uniform vec3 u_light_0_Specular;" \
		"uniform vec3 u_light_0_Diffuse;" \
		"uniform vec3 u_light_1_Ambient;" \
		"uniform vec3 u_light_1_Specular;" \
		"uniform vec3 u_light_1_Diffuse;" \
		"uniform vec3 u_materialAmbient; " \
		"uniform vec3 u_materialSpecular; "\
		"uniform vec3 u_materialDiffuse; "\
		"uniform float u_materialshininess; "\
		"uniform int u_isLightOn;" \
		"void main()" \
		"{"\
		"phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" \
		"if (u_isLightOn == 1) " \
		"{" \
			"normalized_light_direction_0 = normalize(light_direction_0);"\
			"normalized_light_direction_1 = normalize(light_direction_1);"\
			"normalized_transformed_normal = normalize(transformed_normal);"\
			"normalized_view_vector = normalize(view_vector);"\
			"vec3 reflection_vector = reflect (-normalized_light_direction_0, normalized_transformed_normal); " \
			"vec3 ambient = u_light_0_Ambient  * u_materialAmbient; " \
			"vec3 diffuse_light = u_light_0_Diffuse * u_materialDiffuse * max ( dot (normalized_light_direction_0 , normalized_transformed_normal), 0.0); "\
			"vec3 specular = u_light_0_Specular  * u_materialSpecular * pow( max ( dot ( reflection_vector , normalized_view_vector), 0.0) , u_materialshininess);" \
			"phoung_ads_lighting = ambient + diffuse_light + specular;" \
			"reflection_vector = reflect (-normalized_light_direction_1, normalized_transformed_normal); " \
			"ambient = u_light_1_Ambient  * u_materialAmbient; " \
			"diffuse_light = u_light_1_Diffuse * u_materialDiffuse * max ( dot (normalized_light_direction_1 , normalized_transformed_normal), 0.0); "\
			"specular = u_light_1_Specular  * u_materialSpecular * pow( max ( dot ( reflection_vector , normalized_view_vector), 0.0) , u_materialshininess);" \
			"phoung_ads_lighting = phoung_ads_lighting + ambient + diffuse_light + specular;" \
		"}"\
			"FragColor = vec4(phoung_ads_lighting, 1.0); " \
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const char**)&fragmentShaderSourceCode, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_POSITION, "v_position");
	glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_NORMAL, "v_normals");

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObject);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// get MVP uniform location
	gModelUniform = glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
	gViewUniform = glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
	gPerspectiveUniform = glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");
	gIsLightOnUniform = glGetUniformLocation(gShaderProgramObject, "u_isLightOn");
	
	gLight_0_AmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_light_0_Ambient");
	gLight_0_SpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_light_0_Specular");
	gLight_0_DiffusedUniform = glGetUniformLocation(gShaderProgramObject, "u_light_0_Diffuse");
	gLight_0_PossitionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_0_position");

	gLight_1_AmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_light_1_Ambient");
	gLight_1_SpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_light_1_Specular");
	gLight_1_DiffusedUniform = glGetUniformLocation(gShaderProgramObject, "u_light_1_Diffuse");
	gLight_1_PossitionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_1_position");

	gMaterialDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_materialDiffuse");
	gMaterialAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_materialAmbient");
	gMaterialSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_materialSpecular");
	gMaterialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_materialshininess");

	// ***** vertex, colors, shadersatribs , vbo, vao initialization *********
	
	glGenVertexArrays(1, &gVao_pyramid);
	glBindVertexArray(gVao_pyramid);

	GLfloat pyramid_vertices[] =
	{
		// FRONT FACE
		0.0f, 1.0f, 0.0f,
		-1.0f,-1.0f, 1.0f,
		1.0f, -1.0f,1.0f,

		// RIGHT FACE
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,

		// BACK FACE
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f,-1.0f,
		-1.0f, -1.0f,-1.0f,

		// LEFT FACE
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f,-1.0f,
		-1.0f, -1.0f,1.0f,
	};

	glGenBuffers(1, &gVbo_pyramid_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_pyramid_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramid_vertices), pyramid_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo

	GLfloat pyramid_normals[] = {
		0.0f, 0.447214f, 0.894427f, //FRONT	
		0.0f, 0.447214f, 0.894427f, //FRONT	
		0.0f, 0.447214f, 0.894427f, //FRONT	
		0.0f, 0.447214f, 0.894427f, //FRONT			
		
		0.894427f, 0.447214f, 0.0f, // RIGHT
		0.894427f, 0.447214f, 0.0f, // RIGHT	
		0.894427f, 0.447214f, 0.0f, // RIGHT	
		0.894427f, 0.447214f, 0.0f, // RIGHT	
		
		0.0f, 0.447214f, -0.894427f, // BACK
		0.0f, 0.447214f, -0.894427f, // BACK
		0.0f, 0.447214f, -0.894427f, // BACK
		0.0f, 0.447214f, -0.894427f, // BACK
		
		-0.894427f, 0.447214f, 0.0f // LEFT
		-0.894427f, 0.447214f, 0.0f // LEFT
		-0.894427f, 0.447214f, 0.0f // LEFT
		-0.894427f, 0.447214f, 0.0f // LEFT
	};

	glGenBuffers(1, &gVbo_pyramid_normals);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_pyramid_normals);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramid_normals), pyramid_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0); // end for Vao

	// set-up depth buffers
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // don't work

	// we will always cull face for better performance
	//glEnable(GL_CULL_FACE);

	gPerspectiveProjectionMatrix = mat4::identity();

	Resize(WIDTH, HIGHT);
}

void Resize(int width, int height) {
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) height);

	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); // no 0.1f

}

void Display() {

	// VARIABLE DECLARATION
	static GLfloat angle = 0.0f;

	// CODE
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// START USING OpenGL program object
	glUseProgram(gShaderProgramObject);

	// OpenGL Drawing
	// set modelview & modelviewProjection matrices to indentity
	mat4 translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
	mat4 rotateMatrix = vmath::rotate((GLfloat)angle, 0.0f, 1.0f, 0.0f);
	mat4 viewMatrix = mat4::identity();
	/*	vmath::lookat(
		vmath::vec3(0.0f, 0.0f, 6.0f),
		vmath::vec3(0.0f, 0.0f, 0.0f),
		vmath::vec3(0.0f, 1.0f, 0.0f)
	); */
	mat4 modelMatrix = translateMatrix * rotateMatrix;
	
	if (isLightOn == 1)
	{
		glUniform4fv(gLight_0_PossitionUniform, 1, (const GLfloat*) lightPosition_0);
		glUniform3fv(gLight_0_DiffusedUniform, 1, (const GLfloat*) lightDiffuse_0);
		glUniform3fv(gLight_0_AmbientUniform,  1 , (const GLfloat*)lightAmbient_0);
		glUniform3fv(gLight_0_SpecularUniform, 1 , (const GLfloat*)lightSpecular_0);

		glUniform4fv(gLight_1_PossitionUniform, 1, (const GLfloat*)lightPosition_1);
		glUniform3fv(gLight_1_DiffusedUniform, 1, (const GLfloat*)lightDiffuse_1);
		glUniform3fv(gLight_1_AmbientUniform, 1, (const GLfloat*)lightAmbient_1);
		glUniform3fv(gLight_1_SpecularUniform, 1, (const GLfloat*)lightSpecular_1);

		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*) materialAmbient);
		glUniform1f(gMaterialShininessUniform, materialShininess);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*) materialSpecular);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*) materialDiffuse);
	}

	glUniform1i(gIsLightOnUniform, isLightOn);
	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*) modelMatrix);
	glUniformMatrix4fv(gViewUniform, 1, GL_FALSE, (const float*) viewMatrix);
	glUniformMatrix4fv(gPerspectiveUniform, 1, GL_FALSE, (const GLfloat *) gPerspectiveProjectionMatrix);

	// *** bind vao **
	glBindVertexArray(gVao_pyramid);
	
	glDrawArrays(GL_TRIANGLES, 0, 12);

	glBindVertexArray(0);

	// stop using OpenGL program object
	glUseProgram(0);

	if (bAnimate == true)
	{
		angle = angle + 0.25f;
		if (angle >= 360)
		{
			angle = 0.0f;
		}
	}

	SwapBuffers(ghdc);
}

void pushMatrix(mat4 in_matrix)
{
	node* temp = (node*)malloc(sizeof(node));
	temp->matrix = in_matrix;

	if (head == NULL)
	{
		temp->prev = NULL;
		head = temp;
	}
	else
	{
		temp->prev = head;
		head = temp;
	}
}

mat4 popMatrix()
{
	node* temp;
	mat4 matrix;

	if (head == NULL)
	{
		return mat4::identity();
	}

	temp = head;
	head = head->prev;
	matrix = temp->matrix;
	free(temp);
	return matrix;
}


void Uninitialize() {

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	// destroy vao
	if (gVao_pyramid)
	{
		glDeleteVertexArrays(1, &gVao_pyramid);
		gVao_pyramid = 0;
	}

	// destroy vbo
	if (gVbo_pyramid_position)
	{
		glDeleteBuffers(1, &gVbo_pyramid_position);
		gVbo_pyramid_position = 0;
	}

	if (gVbo_pyramid_normals)
	{
		glDeleteBuffers(1, &gVbo_pyramid_normals);
		gVbo_pyramid_normals = 0;
	}

	// deatch vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	// detach fragment shader from shader program obejct
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject);

	gVertexShaderObject = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	// unlink shader program
	glUseProgram(0);

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile) {
		fprintf(gpFile, "\n Closing File Successfully");
		fclose(gpFile);
	}
}
