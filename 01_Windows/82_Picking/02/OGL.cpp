// header file declaration
#include <windows.h>
#include<stdio.h>

#include <GL\glew.h>
#include <gl/GL.h>
#include "OGL.h"
#include "Sphere.h"

#pragma comment(lib, "C:\\glew-2.1.0\\lib\\Release\\Win32\\glew32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "Sphere.lib")

#define WIDTH 1000
#define HIGHT 800

using namespace vmath;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int width, hight;

enum
{
	HRH_ATTRIBUTE_POSITION = 0 ,
	HRH_ATTRIBUTE_COLOR ,
	HRH_ATTRIBUTE_NORMAL ,
	HRH_ATTRIBUTE_TEXTURE_0
};

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gFragmentShaderObject_selection;
GLuint gShaderProgramObject_selection;

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;
GLuint gVbo_sphere_color;

GLuint gModelMatrixUniform;
GLuint gViewMatrixUniform;
GLuint gPerspectiveMatrixUniform;

GLuint gCodeUniform;

GLfloat sphere_vertices[1146];
GLfloat Sphere_normals[1146];
GLfloat Sphere_textrure[764];
unsigned short sphere_elements[2280]; //  Base indices from FFP tea pot

mat4 gPerspectiveProjectionMatrix;

int gNumVertices;
int gNumElements;

int year = 0, day = 0, moon = 0, moon_self = 0;

float gMouse[3];

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	void Initialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("h_ogl");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 

	if (fopen_s(&gpFile, "windows.log", "w") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("OGL"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HIGHT/2 ,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow) {
				Display();
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	void Resize(int,int);

	gMouse[2] = 0;

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_CHAR:
			switch (wParam)
			{
			case 'y':
				year = (year + 3) % 360;
				day = (day + 6) % 360;
				moon = (moon + 3) % 360;
				moon_self = (moon_self + 6) % 360;
				break;
			case 'Y':
				year = (year - 3) % 360;
				day = (day - 6) % 360;
				moon = (moon - 3) % 360;
				moon_self = (moon_self - 6) % 360;
				break;
			case 'd':
				day = (day + 6) % 360;
				moon = (moon + 3) % 360;
				moon_self = (moon_self + 6) % 360;
				break;
			case 'D':
				day = (day - 6) % 360;
				moon = (moon - 3) % 360;
				moon_self = (moon_self - 6) % 360;
				break;
			case 'm':
				moon_self = (moon_self + 6) % 360;
				break;
			case 'M':
				moon_self = (moon_self - 6) % 360;
				break;
			}
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_LBUTTONUP:
			gMouse[0] = LOWORD(lParam);
			gMouse[1] = HIWORD(lParam);
			gMouse[2] = 1;
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int,int);
	void Uninitialize();
	void getSphereVertexData(float spherePositionCoords[1146], float sphereNormalCoords[1146], float sphereTexCoords[764], unsigned short sphereElements[2280]);
	unsigned int getNumberOfSphereVertices(void);
	unsigned int getNumberOfSphereElements(void);
	void LoadSelectionShader();

	// Function declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}
	
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		DestroyWindow(ghwnd);
	}

	//OpenGL Logs
	fprintf(gpFile, "OpenGL Logs");
	fprintf(gpFile, "OpenGL Vendor: %s \n ", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer: %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "OpenGL Shading Language : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	fprintf(gpFile, "Open Enabled extension\n");
	GLint numExtension = 0;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	fprintf(gpFile, "Open Enabled total extension %d \n", numExtension);

	for (int i = 0; i < numExtension; i++)
	{		
		//fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	/********** VERTEX SHADER ***********/
	// CREATE SHADER

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	
	//provide souce code to shader
	const GLchar* vertexShaderSourceCode =  "#version 450 core\n"\
											"in vec4 vPosition;" \
											"in vec3 color;"\
											"out vec3 color_out;"\
											"uniform mat4 u_modelMatrix;" \
											"uniform mat4 u_viewMatrix;" \
											"uniform mat4 u_projectionMatrix;" \
											"void main()"\
											"{" \
											"color_out = color;"\
											"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;"\
											"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCode = "#version 450 core\n"\
											"out vec4 FragColor; " \
											"in vec3 color_out;"
											"void main()"\
											"{"\
											"FragColor = vec4(color_out, 1.0);" \
											"}";
	

	glShaderSource(gFragmentShaderObject, 1, (const char**)&fragmentShaderSourceCode, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_POSITION, "vPosition");

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObject);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	LoadSelectionShader();
	

	getSphereVertexData(sphere_vertices, Sphere_normals, Sphere_textrure, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// ***** vertex, colors, shadersatribs , vbo, vao initialization *********

	
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo

	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Sphere_normals), Sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

//	glVertexAttrib3f(HRH_ATTRIBUTE_COLOR, 1.0f, 1.0f, 0.0f);

//	glGenBuffer(1, &gVbo_sphere_color);
//	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_color);
//	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 3)

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0); // end for Vao

	// set-up depth buffers
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // don't work

	// we will always cull face for better performance
	glEnable(GL_CULL_FACE);

	gPerspectiveProjectionMatrix = mat4::identity();

	Resize(WIDTH, HIGHT);
}

void LoadSelectionShader()
{
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObject_selection = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCode = "#version 450 core\n"\
		"out vec4 FragColor; " \
		"uniform int code;"
		"void main()"\
		"{"\
		"FragColor = vec4(code/255.0, 0.0f, 0.0f, 1.0);" \
		"}";


	glShaderSource(gFragmentShaderObject_selection, 1, (const char**)&fragmentShaderSourceCode, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject_selection);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObject_selection, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_selection, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_selection, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObject_selection = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject_selection, gVertexShaderObject);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObject_selection, gFragmentShaderObject_selection);

	// pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(gShaderProgramObject_selection, HRH_ATTRIBUTE_POSITION, "vPosition");

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObject_selection);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject_selection, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_selection, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}
}

void Resize(int width, int height) {
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) height);

	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); // no 0.1f

}

void Display() 
{
	// FUNCTION DECLARATION	
	void renderPlanets();
	void processSelection();
		
	renderPlanets();

	processSelection();

	SwapBuffers(ghdc);
		
}

void processSelection()
{
	// function declarations
	void renderSelection();


	if (gMouse[2] == 1)
	{
		unsigned char res[4];
		GLint viewport[4];

		renderSelection();

		glGetIntegerv(GL_VIEWPORT, viewport);
		glReadPixels(gMouse[0], viewport[3] - gMouse[1], 2, 2, GL_RGBA, GL_UNSIGNED_BYTE, &res);
		switch(res[0])
		{
		case 0:
			MessageBox(ghwnd, TEXT("Black"), TEXT("Planet"), MB_OK);
			break;
		case 1:
			MessageBox(ghwnd, TEXT("Yellow"), TEXT("Planet"), MB_OK);
			break;
		case 2:
			MessageBox(ghwnd, TEXT("Red"), TEXT("Planet"), MB_OK);
			break;
		case 3:
			MessageBox(ghwnd, TEXT("Blue"), TEXT("Planet"), MB_OK);
			break;
		default:
			break;
		}
	}
}
void renderSelection()
{
	void pushMatrix(mat4);
	mat4 popMatrix(void);

	// get MVP uniform location
	gModelMatrixUniform = glGetUniformLocation(gShaderProgramObject_selection, "u_modelMatrix");
	gViewMatrixUniform = glGetUniformLocation(gShaderProgramObject_selection, "u_viewMatrix");
	gCodeUniform = glGetUniformLocation(gShaderProgramObject_selection, "code");
	gPerspectiveMatrixUniform = glGetUniformLocation(gShaderProgramObject_selection, "u_projectionMatrix");
	

	// START USING OpenGL program object
	glUseProgram(gShaderProgramObject_selection);

	// OpenGL Drawing
	// set modelview & modelviewProjection matrices to indentity
	mat4 viewMatrix = vmath::lookat(
		vmath::vec3(0.0f, 0.0f, 5.0f),
		vmath::vec3(0.0f, 0.0f, 0.0f),
		vmath::vec3(0.0f, 1.0f, 0.0f)
	);

	pushMatrix(viewMatrix);

	mat4 translateMatrix = mat4::identity();
	mat4 rotateMatrix = mat4::identity();
	mat4 modelMatrix = translateMatrix * rotateMatrix;
	mat4 currentMatrix;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(gPerspectiveMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glUniform1i(gCodeUniform, 3);

	glVertexAttrib3f(HRH_ATTRIBUTE_COLOR, 1.0f, 1.0f, 0.0f);

	// *** bind vao **
	glBindVertexArray(gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// ** unbind vao **
	glBindVertexArray(0);

	currentMatrix = popMatrix();
	pushMatrix(currentMatrix);
	viewMatrix = currentMatrix;

	mat4 rotateMatrix_year = vmath::rotate((GLfloat)year, 0.0f, 1.0f, 0.0f);
	translateMatrix = vmath::translate(1.3f, 0.0f, 0.0f);
	mat4 scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
	mat4 rotateMatrix_day = vmath::rotate((GLfloat)day, 0.0f, 1.0f, 0.0f);
	modelMatrix = rotateMatrix_year * translateMatrix * rotateMatrix_day * scaleMatrix;

	pushMatrix(rotateMatrix_year * translateMatrix); // push

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(gPerspectiveMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glUniform1i(gCodeUniform, 2);

	glVertexAttrib3f(HRH_ATTRIBUTE_COLOR, 0.0f, 1.0f, 1.0f);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// *** bind vao **
	glBindVertexArray(gVao_sphere);

	// ** draw , either by glDrawTriangles() or glDrawArrays() or glDrawElements();
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0); //3 (each with its x,y,z) value in triangleVertices array

	// ** unbind vao **
	glBindVertexArray(0);

	currentMatrix = popMatrix();

	rotateMatrix_year = vmath::rotate((GLfloat)moon, 0.0f, 1.0f, 0.0f);
	translateMatrix = vmath::translate(0.5f, 0.2f, 0.0f);
	scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
	rotateMatrix_day = vmath::rotate((GLfloat)moon_self, 0.0f, 1.0f, 0.0f);
	modelMatrix = currentMatrix * rotateMatrix_year * translateMatrix * rotateMatrix_day * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(gPerspectiveMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glUniform1i(gCodeUniform, 1);

	glVertexAttrib3f(HRH_ATTRIBUTE_COLOR, 1.0f, 1.0f, 1.0f);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// *** bind vao **
	glBindVertexArray(gVao_sphere);

	// ** draw , either by glDrawTriangles() or glDrawArrays() or glDrawElements();
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0); //3 (each with its x,y,z) value in triangleVertices array

	// ** unbind vao **
	glBindVertexArray(0);

	popMatrix();
	popMatrix();
	popMatrix();
	// stop using OpenGL program object
	glUseProgram(0);

}

void renderPlanets()
{
	void pushMatrix(mat4 in_matrix);
	mat4 popMatrix(void);

	// CODE
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// get MVP uniform location
	gModelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
	gViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
	gPerspectiveMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");

	// START USING OpenGL program object
	glUseProgram(gShaderProgramObject);

	// OpenGL Drawing
	// set modelview & modelviewProjection matrices to indentity
	mat4 viewMatrix = vmath::lookat(
		vmath::vec3(0.0f, 0.0f, 5.0f),
		vmath::vec3(0.0f, 0.0f, 0.0f),
		vmath::vec3(0.0f, 1.0f, 0.0f)
	);

	pushMatrix(viewMatrix);

	mat4 translateMatrix = mat4::identity();
	mat4 rotateMatrix = mat4::identity();
	mat4 modelMatrix = translateMatrix * rotateMatrix;
	mat4 currentMatrix;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(gPerspectiveMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glVertexAttrib3f(HRH_ATTRIBUTE_COLOR, 1.0f, 1.0f, 0.0f);

	// *** bind vao **
	glBindVertexArray(gVao_sphere);

	// ** draw , either by glDrawTriangles() or glDrawArrays() or glDrawElements();
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0); //3 (each with its x,y,z) value in triangleVertices array

	// ** unbind vao **
	glBindVertexArray(0);

	currentMatrix = popMatrix();
	pushMatrix(currentMatrix);
	viewMatrix = currentMatrix;

	mat4 rotateMatrix_year = vmath::rotate((GLfloat)year, 0.0f, 1.0f, 0.0f);
	translateMatrix = vmath::translate(1.3f, 0.0f, 0.0f);
	mat4 scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
	mat4 rotateMatrix_day = vmath::rotate((GLfloat)day, 0.0f, 1.0f, 0.0f);
	modelMatrix = rotateMatrix_year * translateMatrix * rotateMatrix_day * scaleMatrix;

	pushMatrix(rotateMatrix_year * translateMatrix); // push

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(gPerspectiveMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glVertexAttrib3f(HRH_ATTRIBUTE_COLOR, 0.0f, 1.0f, 1.0f);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// *** bind vao **
	glBindVertexArray(gVao_sphere);

	// ** draw , either by glDrawTriangles() or glDrawArrays() or glDrawElements();
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0); //3 (each with its x,y,z) value in triangleVertices array

	// ** unbind vao **
	glBindVertexArray(0);

	currentMatrix = popMatrix();

	rotateMatrix_year = vmath::rotate((GLfloat)moon, 0.0f, 1.0f, 0.0f);
	translateMatrix = vmath::translate(0.5f, 0.2f, 0.0f);
	scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
	rotateMatrix_day = vmath::rotate((GLfloat)moon_self, 0.0f, 1.0f, 0.0f);
	modelMatrix = currentMatrix * rotateMatrix_year * translateMatrix * rotateMatrix_day * scaleMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(gPerspectiveMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glVertexAttrib3f(HRH_ATTRIBUTE_COLOR, 1.0f, 1.0f, 1.0f);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// *** bind vao **
	glBindVertexArray(gVao_sphere);

	// ** draw , either by glDrawTriangles() or glDrawArrays() or glDrawElements();
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0); //3 (each with its x,y,z) value in triangleVertices array

	// ** unbind vao **
	glBindVertexArray(0);

	popMatrix();
	popMatrix();
	popMatrix();
	// stop using OpenGL program object
	glUseProgram(0);
	
}

void pushMatrix(mat4 in_matrix)
{
	node *temp = (node *)malloc(sizeof(node));
	temp->matrix = in_matrix;

	if (head == NULL)
	{
		temp->prev = NULL;
		head = temp;
	}
	else
	{
		temp->prev = head;
		head = temp;
	}
}

mat4 popMatrix()
{
	node *temp;
	mat4 matrix;

	if (head == NULL)
	{
		return mat4::identity();
	}

	temp = head;
	head = head->prev;
	matrix = temp->matrix;
	free(temp);
	return matrix;
}

void Uninitialize() {

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	// destroy vao
	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}

	// destroy vbo
	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}

	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}

	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}

	// deatch vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	// detach fragment shader from shader program obejct
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject);

	gVertexShaderObject = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	// unlink shader program
	glUseProgram(0);

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile) {
		fprintf(gpFile, "\n Closing File Successfully");
		fclose(gpFile);
	}
}
