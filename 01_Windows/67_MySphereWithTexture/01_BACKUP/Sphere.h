#pragma once

#include <stdio.h>
#include <math.h>

#define HRH_PI 3.14159265358979323846

extern FILE* gpFile;

int maxElements = 0;
int numElements = 0;
int numVertices = 0;

unsigned short *model_elements;
float *model_vertices;
float *model_normals;
float *model_textures;

void allocateMemoryToArrays(int numIndices)
{
	maxElements = numIndices;
	numElements = 0;
	numVertices = 0;

	int iNumIndices = numIndices / 3;

	model_elements = (unsigned short*)malloc(sizeof(short) * iNumIndices * 3 * 2);
	model_vertices = (float*)malloc(sizeof(float) * iNumIndices * 3 * 4);
	model_normals = (float*)malloc(sizeof(float) * iNumIndices * 3 * 4);
	model_textures = (float*)malloc(sizeof(float) * iNumIndices * 2 * 4);
}

int getNumberOfSphereVertices()
{
	// code
	return numVertices;
}

int getNumberOfSphereElements()
{
	// code
	return numElements;
}

void makeSphere(float fRadius,int iSlices,int iStacks)
{
	// function declarations
	void allocateMemoryToArrays(int numIndices);
	void addTriangle(float[3][3], float[3][3], float[3][2]);

	// variable declarations
	float drho = HRH_PI / (float)iStacks;
	float dtheta = 2.0f * HRH_PI / (float)iSlices;
	float ds = 1.0f / (float) iSlices;
	float dt = 1.0f / (float)iStacks;
	float t = 1.0f;
	float s = 0.0f;
	int i = 0;
	int j = 0;

	// code

	allocateMemoryToArrays(iSlices * iStacks * 6);

	fprintf(gpFile, "\nEntering in MakeSphere()");

	for (i = 0; i < iStacks; i++)
	{
		float rho = i * drho;
		float srho = sin(rho);
		float crho = cos(rho);
		float srhodrho = sin(rho + drho);
		float crhodrho = cos(rho + drho);

		s = 0.0f;

		float vertex[4][3];
		float normals[4][3];
		float texture[4][2];

		for (j = 0; j < iSlices; j++)
		{
			float theta = (j == iSlices) ? 0.0f : j * dtheta;
			float stheta = -sin(theta);
			float ctheta = cos(theta);

			float x = stheta * srho;
			float y = ctheta * srho;
			float z = crho;

			// first vertex
			texture[0][0] = s;
			texture[0][1] = t;

			normals[0][0] = x;
			normals[0][1] = y;
			normals[0][2] = z;

			vertex[0][0] = x * fRadius;
			vertex[0][1] = y * fRadius;
			vertex[0][2] = z * fRadius;

			// second vertex
			x = stheta * srhodrho;
			y = ctheta * srhodrho;
			z = crhodrho;

			texture[1][0] = s;
			texture[1][1] = t - dt;

			normals[1][0] = x;
			normals[1][1] = y;
			normals[1][2] = z;
			
			vertex[1][0] = x * fRadius;
			vertex[1][1] = y * fRadius;
			vertex[1][2] = z * fRadius;

			// third vertex
			theta = ((j + 1) == iSlices) ? 0.0f : (j + 1) * dtheta;
			stheta = -sin(theta);
			ctheta = cos(theta);

			x = stheta * srho;
			y = ctheta * srho;
			z = crho;

			s = s + ds;

			texture[2][0] = s;
			texture[2][1] = t;

			normals[2][0] = x;
			normals[2][1] = y;
			normals[2][2] = z;
			
			vertex[2][0] = x * fRadius;
			vertex[2][1] = y * fRadius;
			vertex[2][2] = z * fRadius;

			// forth vertex

			x = stheta * srhodrho;
			y = ctheta * srhodrho;
			z = crhodrho;

			texture[3][0] = s;
			texture[3][1] = t - dt;

			normals[3][0] = x;
			normals[3][1] = y;
			normals[3][2] = z;

			vertex[3][0] = x * fRadius;
			vertex[3][1] = y * fRadius;
			vertex[3][2] = z * fRadius;

			addTriangle(vertex, normals, texture);

			// Rearrange for next triangle
			vertex[0][0] = vertex[1][0];
			vertex[0][1] = vertex[1][1];
			vertex[0][2] = vertex[1][2];
			
			normals[0][0] = normals[1][0];
			normals[0][1] = normals[1][1];
			normals[0][2] = normals[1][2];

			texture[0][0] = texture[1][0];
			texture[0][1] = texture[1][1];

			vertex[1][0] = vertex[3][0];
			vertex[1][1] = vertex[3][1];
			vertex[1][2] = vertex[3][2];

			normals[1][0] = normals[3][0];
			normals[1][1] = normals[3][1];
			normals[1][2] = normals[3][2];

			texture[1][0] = texture[3][0];
			texture[1][1] = texture[3][1];

			addTriangle(vertex, normals, texture);
		}
		t = t - dt;
	}
	fprintf(gpFile, "\nExiting from MakeSphere()");
}

void addTriangle(float single_vertex[3][3], float single_normal[3][3], float single_texture[3][2])
{
	// function declarations
	void normalizeVector(float[]);
	bool isFoundIdentical(const float val1, const float val2, const float diff);

	//variable declarations
	const float diff = 0.00001f;
	int i, j;

	// code
	// normals should be of unit length
	normalizeVector(single_normal[0]);
	normalizeVector(single_normal[1]);
	normalizeVector(single_normal[2]);

	fprintf(gpFile, "\nEntering in addTriangle()");

	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < numVertices; j++) //for the first ever iteration of 'j', numVertices will be 0 because of it's initialization in the parameterized constructor
		{
			if (isFoundIdentical(model_vertices[j * 3], single_vertex[i][0], diff) &&
				isFoundIdentical(model_vertices[(j * 3) + 1], single_vertex[i][1], diff) &&
				isFoundIdentical(model_vertices[(j * 3) + 2], single_vertex[i][2], diff) &&

				isFoundIdentical(model_normals[j * 3], single_normal[i][0], diff) &&
				isFoundIdentical(model_normals[(j * 3) + 1], single_normal[i][1], diff) &&
				isFoundIdentical(model_normals[(j * 3) + 2], single_normal[i][2], diff) &&

				isFoundIdentical(model_textures[j * 2], single_texture[i][0], diff) &&
				isFoundIdentical(model_textures[(j * 2) + 1], single_texture[i][1], diff))
			{
				model_elements[numElements] = (short)j;
				numElements++;
				fprintf(gpFile, "\nIn addTriangle() for loop for numElements = %d ", numElements);
				break;
			}
		}

		//If the single vertex, normal and texture do not match with the given, then add the corressponding triangle to the end of the list
		if (j == numVertices && numVertices < maxElements && numElements < maxElements)
		{

			model_vertices[numVertices * 3] = single_vertex[i][0];
			model_vertices[(numVertices * 3) + 1] = single_vertex[i][1];
			model_vertices[(numVertices * 3) + 2] = single_vertex[i][2];

			model_normals[numVertices * 3] = single_normal[i][0];
			model_normals[(numVertices * 3) + 1] = single_normal[i][1];
			model_normals[(numVertices * 3) + 2] = single_normal[i][2];

			model_textures[numVertices * 2] = single_texture[i][0];
			model_textures[(numVertices * 2) + 1] = single_texture[i][1];

			model_elements[numElements] = (short)numVertices; //adding the index to the end of the list of elements/indices
			numElements++; //incrementing the 'end' of the list
			numVertices++; //incrementing coun of vertices
			fprintf(gpFile, "\nIn addTriangle() second for loop for numElements = %d and numVertices = %d", numElements, numVertices);

		}
	}
	fprintf(gpFile, "\nExiting from addTriangle()");
}

void normalizeVector(float v[])
{
	// code

	// square the vector length
	float squaredVectorLength = (v[0] * v[0]) + (v[1] * v[1]) + (v[2] * v[2]);

	// get square root of above 'squared vector length'
	float squareRootOfSquaredVectorLength = (float)sqrt(squaredVectorLength);

	// scale the vector with 1/squareRootOfSquaredVectorLength
	v[0] = v[0] * 1.0f / squareRootOfSquaredVectorLength;
	v[1] = v[1] * 1.0f / squareRootOfSquaredVectorLength;
	v[2] = v[2] * 1.0f / squareRootOfSquaredVectorLength;
}

bool isFoundIdentical(const float val1, const float val2, const float diff)
{
	// code
	if (fabsf(val1 - val2) < diff)
		return(true);
	else
		return(false);
}

void getSphereVertexData(int iNumIndices, float *spherePositionCoords, float *sphereNormalCoords, float *sphereTexCoords, unsigned short *sphereElements)
{
	// variable declarations
	const int SPHERE_VERTEX_INDEX = iNumIndices * 4 * 3;
	const int SPHERE_NORMAL_INDEX = iNumIndices * 4 * 3;
	const int SPHERE_TEXTURE_INDEX = iNumIndices * 4 * 2;
	const int SPHERE_ELEMENT_INDEX = iNumIndices * 3 * 2;

	// code
	for (int i = 0; i < SPHERE_VERTEX_INDEX; i++)
	{
		spherePositionCoords[i] = model_vertices[i];
	}

	for (int i = 0; i < SPHERE_NORMAL_INDEX; i++)
	{
		sphereNormalCoords[i] = model_normals[i];
	}

	for (int i = 0; i < SPHERE_TEXTURE_INDEX; i++)
	{
		sphereTexCoords[i] = model_textures[i];
	}

	for (int i = 0; i < SPHERE_ELEMENT_INDEX; i++)
	{
		sphereElements[i] = model_elements[i];
	}
}

