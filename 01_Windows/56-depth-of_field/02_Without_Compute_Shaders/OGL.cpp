
/*
* Name: Hrituja Ramkrishna Hedau
* Batch: RTR3.0
* Refernce: Super Bible 7th edition
* Special Effect: Depth Of Field
*/

// header file declaration
#include <windows.h>
#include<stdio.h>
#include <memory.h>

#include <GL\glew.h>
#include <gl/GL.h>
#include "OGL.h"
#include "vmath.h"
#include "TeaPot.h"

#pragma comment(lib, "C:\\glew-2.1.0\\lib\\Release\\Win32\\glew32.lib")
#pragma comment(lib, "opengl32.lib")

#define WIDTH 1000
#define HIGHT 800

#define FBO_SIZE			2048
#define FRUSTUM_DEPTH		1000

#define SB6M_FOURCC(a,b,c,d)            ( ((unsigned int)(a) << 0) | ((unsigned int)(b) << 8) | ((unsigned int)(c) << 16) | ((unsigned int)(d) << 24) )
#define SB6M_VERTEX_ATTRIB_FLAG_NORMALIZED      0x00000001

using namespace vmath;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int gWidth, gHeight;

enum
{
	HRH_ATTRIBUTE_POSITION = 0 ,
	HRH_ATTRIBUTE_NORMAL,
	HRH_ATTRIBUTE_COLOR ,	
	HRH_ATTRIBUTE_TEXTURE_0
};

typedef enum SB6M_CHUNK_TYPE_t
{
	SB6M_CHUNK_TYPE_INDEX_DATA = SB6M_FOURCC('I', 'N', 'D', 'X'),
	SB6M_CHUNK_TYPE_VERTEX_DATA = SB6M_FOURCC('V', 'R', 'T', 'X'),
	SB6M_CHUNK_TYPE_VERTEX_ATTRIBS = SB6M_FOURCC('A', 'T', 'R', 'B'),
	SB6M_CHUNK_TYPE_SUB_OBJECT_LIST = SB6M_FOURCC('O', 'L', 'S', 'T'),
	SB6M_CHUNK_TYPE_COMMENT = SB6M_FOURCC('C', 'M', 'N', 'T'),
	SB6M_CHUNK_TYPE_DATA = SB6M_FOURCC('D', 'A', 'T', 'A')
} SB6M_CHUNK_TYPE;

GLuint gRenderVertexShaderObject;
GLuint gRenderFragmentShaderObject;
GLuint gRenderShaderProgramObject;

GLuint gDisplayVertexShaderObject;
GLuint gDisplayFragmentShaderObject;
GLuint gDisplayShaderProgramObject;

GLuint gComputeShaderObject;
GLuint gComputeShaderProgramObject;

GLuint gVao_dragon;
GLuint data_buffer_dragon;

GLuint gVao_teapot;
GLuint gVbo_position_teapot;
GLuint gVbo_normals_teapot;
GLuint gVbo_texture_teapot;
GLuint gVbo_element_teapot;

GLuint gVao_quad;

GLfloat teapotVertices[141336];     
GLfloat teapotNormal[141336];       
GLfloat teapotTexcoord[94224];      
short teapotFaceIndices[15704 * 8]; 

GLuint gMVUniform;
GLuint gProjUniform;
GLuint gFull_shadingUniform;
GLuint gDiffuse_albedoUniform;
GLuint gFocal_distanceUniform;
GLuint gFocal_depthUniform;


GLfloat gFocal_distance = 40.0f;
GLfloat gFocal_depth = 50.0f;

GLuint gSampler2D;

mat4 gPerspectiveProjectionMatrix;

GLuint envmaps_0, envmaps_1, envmaps_2, texture ;

GLuint depth_fbo, depth_texture, color_texture, temp_texture;

SB6M_CHUNK_HEADER* chunk_vertex_attribs;
SB6M_CHUNK_HEADER* chunk_vertex_data;

SB6M_VERTEX_ATTRIB_CHUNK* vertex_attrib_chunk;
SB6M_CHUNK_VERTEX_DATA* vertex_data_chunk;

int isDragon = 1, isTeapot;

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	void Initialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("h_ogl");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 

	if (fopen_s(&gpFile, "windows.log", "w") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("OGL - Depth Of Field"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HIGHT/2 ,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow) {
				
			}
			Display();
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	void Resize(int,int);

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_CHAR:
			switch (wParam)
			{
			case 'd':
			case 'D':
				if (isDragon)
				{
					isDragon = false;
				}
				else
				{
					isDragon = true;
					isTeapot = false;
				}
				break;
			case 't':
			case 'T':
				if (isTeapot)
				{
					isTeapot = false;
				}
				else
				{
					isTeapot = true;
					isDragon = false;
				}
				break;
			case 'q':
			case 'Q':
				gFocal_distance = gFocal_distance * 1.1f;
				break;
			case 'a':
			case 'A':
				gFocal_distance = gFocal_distance / 1.1f;
				break;
			case 'W':
			case 'w':
				gFocal_depth = gFocal_depth * 1.1f;
				break;
			case 's':
			case 'S':
				gFocal_depth = gFocal_depth / 1.1f;
				break;
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int, int);
	void LoadTexture(const char * filename, unsigned int* tex);
	void horizontalLine();
	void LoadObject();
	void LoadShaders();
	void Uninitialize();

	// Function declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		DestroyWindow(ghwnd);
	}

	//OpenGL Logs
	fprintf(gpFile, "OpenGL Logs");
	fprintf(gpFile, "OpenGL Vendor: %s \n ", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer: %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "OpenGL Shading Language : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	fprintf(gpFile, "Open Enabled extension\n");
	GLint numExtension = 0;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	fprintf(gpFile, "Open Enabled total extension %d \n", numExtension);

	for (int i = 0; i < numExtension; i++)
	{
		//fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	LoadShaders();

	horizontalLine();

	LoadObject();

	glGenFramebuffers(1, &depth_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, depth_fbo);

	glGenTextures(1, &depth_texture);
	glBindTexture(GL_TEXTURE_2D, depth_texture);
	glTexStorage2D(GL_TEXTURE_2D, 11, GL_DEPTH_COMPONENT32F, FBO_SIZE, FBO_SIZE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenTextures(1, &color_texture);
	glBindTexture(GL_TEXTURE_2D, color_texture);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA32F, FBO_SIZE, FBO_SIZE);

	glGenTextures(1, &temp_texture);
	glBindTexture(GL_TEXTURE_2D, temp_texture);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA32F, FBO_SIZE, FBO_SIZE);

	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depth_texture, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, color_texture, 0);

	glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glGenVertexArrays(1, &gVao_quad);
	glBindVertexArray(gVao_quad);

	// set-up depth buffers
	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// we will always cull face for better performance
	glEnable(GL_CULL_FACE);

	gPerspectiveProjectionMatrix = mat4::identity();

	Resize(WIDTH, HIGHT);
}

void LoadShaders()
{
	void LoadRenderShader();
	void LoadDisplayShader();
	void LoadComputeShader();

	LoadRenderShader();
	LoadDisplayShader();
	LoadComputeShader();
}

void LoadComputeShader()
{
	/********** COMPUTE SHADER ***********/
// CREATE SHADER

	gComputeShaderObject = glCreateShader(GL_COMPUTE_SHADER);

	//provide souce code to shader
	const GLchar* computeShaderSourceCode =
		" #version 450 core															\n " \
		" 																			\n " \
		" layout (local_size_x = 1024) in;							    			\n " \
		" 																			\n " \
		" shared vec3 shared_data[gl_WorkGroupSize.x * 2];             				\n " \
		" 																			\n " \
		" layout (binding = 0, rgba32f) readonly uniform image2D input_image;   	\n " \
		" layout (binding = 1, rgba32f) writeonly uniform image2D output_image; 	\n " \
		" 																			\n " \
		" void main(void)                                               			\n " \
		" {                                                             			\n " \
		" 	uint id = gl_LocalInvocationID.x;                          				\n " \
		" 	uint rd_id;																\n " \
		" 	uint wr_id;																\n " \
		" 	uint mask;																\n " \
		"	ivec2 p0 = ivec2(id * 2, gl_WorkGroupID.x);                 			\n " \
		"	ivec2 p1 = ivec2(id * 2 + 1, gl_WorkGroupID.x);             			\n " \
		"	                                                            			\n " \
		"	const uint steps = uint (log2(gl_WorkGroupSize.x)) + 1;     			\n " \
		"	uint step = 0;                                              			\n " \
		"	                                                            			\n " \
		" 	vec4 i0 = imageLoad(input_image, p0);									\n " \
		" 	vec4 i1 = imageLoad(input_image, p1);									\n " \
		"																			\n " \
		"	shared_data[p0.x] = i0.rgb; 	          								\n " \
		"	shared_data[p1.x] = i1.rgb;           									\n " \
		"	                                                            			\n " \
		"	barrier();                                                  			\n " \
		"	                                                            			\n " \
		"	for(step = 0; step < steps; step++)                         			\n " \
		"	{                                                           			\n " \
		"		mask = (1 << step) - 1;                                 			\n " \
		"		rd_id = ((id >> step) << (step + 1)) + mask;            			\n " \
		"		wr_id = rd_id + 1 + (id & mask);                        			\n " \
		"		                                                        			\n " \
		"		shared_data[wr_id] += shared_data[rd_id];               			\n " \
		"		                                                        			\n " \
		"		barrier();                                              			\n " \
		"	}                                                           			\n " \
		"	                                                            			\n " \
		"	imageStore(output_image, p0.yx, vec4(shared_data[p0.x], i0.a));   		\n " \
		"	imageStore(output_image, p1.yx, vec4(shared_data[p1.x], i1.a));   		\n " \
		"}                                                              			\n " ;

	glShaderSource(gComputeShaderObject, 1, (const GLchar**)&computeShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gComputeShaderObject);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gComputeShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gComputeShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gComputeShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Compute shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	//CREATE SHADER PROGRAM
	gComputeShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gComputeShaderProgramObject, gComputeShaderObject);

	// LINK PROGRAM
	glLinkProgram(gComputeShaderProgramObject);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gComputeShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gComputeShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gComputeShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Compute Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

}

void LoadDisplayShader()
{
	// VERTEX SHADER
	gDisplayVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//provide souce code to shader
	const GLchar* vertexShaderSourceCode =
		" #version 450 core															\n " \
		" void main()																\n " \
		" {																			\n " \
		" 	const vec4 vertices[] = vec4[](vec4 (-1.0, -1.0, 0.5, 1.0),				\n " \
		" 									vec4( 1.0, -1.0, 0.5, 1.0),				\n " \
		" 									vec4(-1.0,  1.0, 0.5, 1.0),				\n " \
		" 									vec4( 1.0,  1.0, 0.5, 1.0));			\n " \
		"	gl_Position = vertices[gl_VertexID];									\n " \
		" }																			\n " ;

	glShaderSource(gDisplayVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gDisplayVertexShaderObject);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gDisplayVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gDisplayVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gDisplayVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Display Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}


	// FRAGMENT SHADER

	// CREATE SHADER
	gDisplayFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* displayFragmentShaderSourceCode =
		" #version 450 core															\n " \
		" 																			\n " \
		" layout (binding = 0, rgba32f) readonly uniform image2D input_image;		\n " \
		" layout (location = 0) out vec4 color;										\n " \
		" 																			\n " \
		" uniform float focal_distance = 50.0;										\n " \
		" uniform float focal_depth = 30.0;											\n " \
		" 																			\n " \
		" uniform int height;														\n " \
		" uniform int width;														\n " \
		"																			\n " \
		" image2D myFunc(image2D input_image, int x_dimentions)						\n " \
		" {																			\n " \
		"	shared vec3 shared_data[x_dimentions * 2];								\n " \
		" 	uint id = gl_LocalInvocationID.x;                          				\n " \
		" 	uint rd_id;																\n " \
		" 	uint wr_id;																\n " \
		" 	uint mask;																\n " \
		"	ivec2 p0 = ivec2(id * 2, gl_WorkGroupID.x);                				\n " \
		"	ivec2 p1 = ivec2(id * 2 + 1, gl_WorkGroupID.x);            				\n " \
		"	                                                            			\n " \
		"	const uint steps = uint (log2(x_dimentions)) + 1;     					\n " \
		"	uint step = 0;                                              			\n " \
		"	                                                            			\n " \
		" 	vec4 i0 = imageLoad(input_image, p0);									\n " \
		" 	vec4 i1 = imageLoad(input_image, p1);									\n " \
		"																			\n " \
		"	shared_data[p0.x] = i0.rgb; 	          								\n " \
		"	shared_data[p1.x] = i1.rgb;           									\n " \
		"	                                                            			\n " \
		"	barrier();                                                  			\n " \
		"	                                                            			\n " \
		"	for(step = 0; step < steps; step++)                         			\n " \
		"	{                                                           			\n " \
		"		mask = (1 << step) - 1;                                 			\n " \
		"		rd_id = ((id >> step) << (step + 1)) + mask;            			\n " \
		"		wr_id = rd_id + 1 + (id & mask);                        			\n " \
		"		                                                        			\n " \
		"		shared_data[wr_id] += shared_data[rd_id];               			\n " \
		"		                                                        			\n " \
		"		barrier();                                              			\n " \
		"	}                                                           			\n " \
		"	                                                            			\n " \
		"	imageStore(output_image, p0.yx, vec4(shared_data[p0.x], i0.a));   		\n " \
		"	imageStore(output_image, p1.yx, vec4(shared_data[p1.x], i1.a));   		\n " \
		" }                                                              			\n " \
		" void main(void)															\n " \
		" {																			\n " \
		" 	vec2 s = 1.0 / textureSize(input_image, 0);								\n " \
		" 	vec2 C = gl_FragCoord.xy;												\n " \
		"	vec4 v = texelFetch(input_image, ivec2(gl_FragCoord.xy), 0).rgba; 		\n " \
		" 	float m; 																\n " \
		"																			\n " \
		"	if (v.w == 0.0)															\n " \
		"	{																		\n " \
		"		m = 0.5;															\n " \
		"	}																		\n " \
		"	else																	\n " \
		"	{																		\n " \
		"		m = abs(v.w - focal_distance);										\n " \
		"																			\n " \
		"		m = 0.5 + smoothstep(0.0, focal_depth, m) * 7.5;					\n " \
		"	}																		\n " \
		"																			\n " \
		" 	vec2 p0 = vec2(C * 1.0) + vec2(-m, -m);									\n " \
		" 	vec2 p1 = vec2(C * 1.0) + vec2(-m , m);									\n " \
		" 	vec2 p2 = vec2(C * 1.0) + vec2(m, -m);									\n " \
		" 	vec2 p3 = vec2(C * 1.0) + vec2(m, m);									\n " \
		" 																			\n " \
		" 	p0 = p0 * s;															\n " \
		" 	p1 = p1 * s;															\n " \
		" 	p2 = p2 * s;															\n " \
		" 	p3 = p3 * s;															\n " \
		" 																			\n " \
		" 	vec3 a = textureLod(input_image, p0, 0).rgb;							\n " \
		" 	vec3 b = textureLod(input_image, p1, 0).rgb;							\n " \
		" 	vec3 c = textureLod(input_image, p2, 0).rgb;							\n " \
		" 	vec3 d = textureLod(input_image, p3, 0).rgb;							\n " \
		" 																			\n " \
		" 	vec3 f = a - b - c + d;													\n " \
		" 																			\n " \
		" 	m = m * 2;																\n " \
		" 																			\n " \
		" 	f = f / float(m * m);													\n " \
		" 																			\n " \
		" 	color = vec4(f , 1.0);													\n " \
		" 																			\n " \
		" }																			\n " ;
		
	
	glShaderSource(gDisplayFragmentShaderObject, 1, (const char**)&displayFragmentShaderSourceCode, NULL);

	// COMPILE SHADER
	glCompileShader(gDisplayFragmentShaderObject);

	// compilation error checking

	glGetShaderiv(gDisplayFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gDisplayFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gDisplayFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Display Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM
	gDisplayShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gDisplayShaderProgramObject, gDisplayVertexShaderObject);

	// attch fragment shader to shader program
	glAttachShader(gDisplayShaderProgramObject, gDisplayFragmentShaderObject);

	// LINK PROGRAM
	glLinkProgram(gDisplayShaderProgramObject);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gDisplayShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gDisplayShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gDisplayShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Display Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	gFocal_distanceUniform = glGetUniformLocation(gDisplayShaderProgramObject, "focal_distance");
	gFocal_depthUniform = glGetUniformLocation(gDisplayShaderProgramObject, "focal_depth");

}
void LoadRenderShader()
{
	// function declaration
	void Uninitialize();

	/********** VERTEX SHADER ***********/
	// CREATE SHADER

	gRenderVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//provide souce code to shader
	const GLchar* renderVertexShaderSourceCode = 
		" #version 450 core					                            \n " \
		" 										                        \n " \
		" uniform mat4 mv_matrix;                                       \n " \
		" uniform mat4 proj_matrix;					                    \n " \
		" 											                    \n " \
		" layout (location = 0) in vec4 position;	                    \n " \
		" layout (location = 1) in vec3 normal;			                \n " \
		" 												                \n " \
		" out VS_OUT										            \n " \
		" {										                        \n " \
		" 	vec3 N;														\n " \
		" 	vec3 L;														\n " \
		" 	vec3 V;														\n " \
		" } vs_out;														\n " \
		" 																\n " \
		" // position of light											\n " \
		" uniform vec3 light_pos = vec3(100.0, 100.0, 100.0);			\n " \
		" 																\n " \
		" void main()											        \n " \
		" {														        \n " \
		" 	// calculation view-space coordinate						\n " \
		" 	vec4 P = mv_matrix * position;								\n " \
		" 																\n " \
		" 	// calculate normal in view-space 							\n " \
		" 	vs_out.N = mat3(mv_matrix) * normal;						\n " \
		" 																\n " \
		" 	//calculate light vector									\n " \
		" 	vs_out.L = light_pos - P.xyz;								\n " \
		" 																\n " \
		" 	//calculate view vector										\n " \
		" 	vs_out.V = -P.xyz;											\n " \
		" 																\n " \
		" 	//calculate the clip-space position of each vertex			\n " \
		" 	gl_Position = proj_matrix * P;								\n " \
		" }																\n ";

	glShaderSource(gRenderVertexShaderObject, 1, (const GLchar**)&renderVertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gRenderVertexShaderObject);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gRenderVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gRenderVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gRenderVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Render Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gRenderFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* renderFragmentShaderSourceCode = 
		" #version 450 core																	\n " \
		" 																					\n " \
		" layout (location = 0) out vec4 color;												\n " \
		" 																					\n " \
		" in VS_OUT																			\n " \
		" {																					\n " \
		" 	vec3 N;																			\n " \
		" 	vec3 L;																			\n " \
		" 	vec3 V;																			\n " \
		" } fs_in;																			\n " \
		" 																					\n " \
		" //material properties																\n " \
		" uniform vec3 diffuse_albedo = vec3(0.9, 0.8, 1.0);								\n " \
		" uniform vec3 specular_albedo = vec3(0.7);										    \n " \
		" uniform float specular_power = 300.0;											    \n " \
		" uniform bool full_shading = true;												    \n " \
		" 																					\n " \
		" void main(void) 																    \n " \
		" {																				    \n " \
		" 	// Normalize the incoming N, L and V vectors								    \n " \
		" 	vec3 N = normalize(fs_in.N);												    \n " \
		" 	vec3 L = normalize(fs_in.L);												    \n " \
		" 	vec3 V = normalize(fs_in.V);													\n " \
		" 																					\n " \
		" 	// calculate R locally															\n " \
		" 	vec3 R = reflect(-L, N);														\n " \
		" 																					\n " \
		" 	// compute the diffuse and specular components for each							\n " \
		" 	vec3 diffuse = max(dot(N, L) , 0.0) * diffuse_albedo;							\n " \
		" 	vec3 specular = pow(max(dot(R, V), 0.0), specular_power) * specular_albedo;		\n " \
		" 																					\n " \
		" 	// write final color to framebuffer 											\n " \
		" 	color = vec4(diffuse + specular, fs_in.V.z);									\n " \
		" }																				    \n ";
		
	glShaderSource(gRenderFragmentShaderObject, 1, (const char**)&renderFragmentShaderSourceCode, NULL);

	// COMPILE SHADER
	glCompileShader(gRenderFragmentShaderObject);

	// compilation error checking

	glGetShaderiv(gRenderFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gRenderFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gRenderFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Render Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gRenderShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gRenderShaderProgramObject, gRenderVertexShaderObject);

	// attch fragment shader to shader program
	glAttachShader(gRenderShaderProgramObject, gRenderFragmentShaderObject);

	// LINK PROGRAM
	glLinkProgram(gRenderShaderProgramObject);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gRenderShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gRenderShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gRenderShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Render Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	gMVUniform = glGetUniformLocation(gRenderShaderProgramObject, "mv_matrix");
	gProjUniform = glGetUniformLocation(gRenderShaderProgramObject, "proj_matrix");
	gFull_shadingUniform = glGetUniformLocation(gRenderShaderProgramObject, "full_shading");
	gDiffuse_albedoUniform = glGetUniformLocation(gRenderShaderProgramObject, "diffuse_albedo");

}

void LoadObject()
{
	// function declaration
	void Uninitialize();
	
	// create vao
	char* data;
	FILE* infile;
	size_t file_size;

	infile = fopen("./objects/dragon.sbm", "rb");

	if (infile == NULL)
	{
		fprintf(gpFile, "\nFile for object not able to open");
		Uninitialize();
		DestroyWindow(ghwnd);
	}

	fprintf(gpFile, "\nFile for object open successafully");

	fseek(infile, 0, SEEK_END);
	file_size = ftell(infile);
	fseek(infile, 0, SEEK_SET);

	data = (char*)malloc(sizeof(char) * file_size);

	if (fread(data, file_size, 1, infile) == 0)
	{
		fprintf(gpFile, "\nNot able to read Object file data read");
		Uninitialize();
		DestroyWindow(ghwnd);
	}
	fprintf(gpFile, "\nObject file data read successully");

	char* ptr = data;
	SB6M_HEADER* header = (SB6M_HEADER*)ptr;
	ptr = ptr + header->size;

	chunk_vertex_attribs = (SB6M_CHUNK_HEADER*)ptr;
	ptr = ptr + chunk_vertex_attribs->size;
	chunk_vertex_data = (SB6M_CHUNK_HEADER*)ptr;
	ptr = ptr + chunk_vertex_data->size;

	fprintf(gpFile, "\n\n");

	fprintf(gpFile, "\nchunk_vertex_attribs->chunk_type = %d", chunk_vertex_attribs->chunk_type);
	fprintf(gpFile, "\nchunk_vertex_data->chunk_type = %d", chunk_vertex_data->chunk_type);

	fprintf(gpFile, "\n\n");

	fprintf(gpFile, "\nheader->num_chunks = %d", header->num_chunks);
	fprintf(gpFile, "\nSB6M_CHUNK_TYPE_DATA = %d ", SB6M_CHUNK_TYPE_DATA);
	fprintf(gpFile, "\nSB6M_CHUNK_TYPE_VERTEX_ATTRIBS = %d ", SB6M_CHUNK_TYPE_VERTEX_ATTRIBS);
	fprintf(gpFile, "\nSB6M_CHUNK_TYPE_VERTEX_DATA = %d ", SB6M_CHUNK_TYPE_VERTEX_DATA);
	fprintf(gpFile, "\nSB6M_CHUNK_TYPE_INDEX_DATA = %d ", SB6M_CHUNK_TYPE_INDEX_DATA);
	fprintf(gpFile, "\nSB6M_CHUNK_TYPE_SUB_OBJECT_LIST = %d ", SB6M_CHUNK_TYPE_SUB_OBJECT_LIST);

	// copy vertex attrib chunck
	vertex_attrib_chunk = (SB6M_VERTEX_ATTRIB_CHUNK*)chunk_vertex_attribs;
	vertex_data_chunk = (SB6M_CHUNK_VERTEX_DATA*)chunk_vertex_data;

	// VAO FOR DRAGON

	glGenVertexArrays(1, &gVao_dragon);
	glBindVertexArray(gVao_dragon);

	glGenBuffers(1, &data_buffer_dragon);
	glBindBuffer(GL_ARRAY_BUFFER, data_buffer_dragon);
	glBufferData(GL_ARRAY_BUFFER, vertex_data_chunk->data_size, NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, vertex_data_chunk->data_size, data + vertex_data_chunk->data_offset);

	fprintf(gpFile, "\nvertex_attrib_chunk->attrib_count = %d", vertex_attrib_chunk->attrib_count);

	SB6M_VERTEX_ATTRIB_DECL& attrib_decl = vertex_attrib_chunk->attrib_data[0];
	glVertexAttribPointer(0,
		attrib_decl.size,
		attrib_decl.type,
		attrib_decl.flags & SB6M_VERTEX_ATTRIB_FLAG_NORMALIZED ? GL_TRUE : GL_FALSE,
		attrib_decl.stride,
		(GLvoid*)(uintptr_t)attrib_decl.data_offset);
	glEnableVertexAttribArray(0);

	attrib_decl = vertex_attrib_chunk->attrib_data[1];
	glVertexAttribPointer(1,
		attrib_decl.size,
		attrib_decl.type,
		attrib_decl.flags & SB6M_VERTEX_ATTRIB_FLAG_NORMALIZED ? GL_TRUE : GL_FALSE,
		attrib_decl.stride,
		(GLvoid*)(uintptr_t)attrib_decl.data_offset);
	glEnableVertexAttribArray(1);

	attrib_decl = vertex_attrib_chunk->attrib_data[2];
	glVertexAttribPointer(2,
		attrib_decl.size,
		attrib_decl.type,
		attrib_decl.flags & SB6M_VERTEX_ATTRIB_FLAG_NORMALIZED ? GL_TRUE : GL_FALSE,
		attrib_decl.stride,
		(GLvoid*)(uintptr_t)attrib_decl.data_offset);
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//VAO FOR TEAPOT

	int totalElement = 0;
	int totalTexcoord = 0;
	int v = 0, n = 0, t = 0, f = 0;
	for (int i = 0; i < (sizeof(face_indicies) / sizeof(face_indicies[0])); i++)
	{
		for (int j = 0; j < 3; j++)
		{
			int vi = face_indicies[i][j];
			int ni = face_indicies[i][j + 3];
			int ti = face_indicies[i][j + 6];

			teapotNormal[n + 0] = normals[ni][0];
			teapotNormal[n + 1] = normals[ni][1];
			teapotNormal[n + 2] = normals[ni][2];

			n = n + 3;

			teapotTexcoord[t] = textures[ti][0];
			teapotTexcoord[t + 1] = textures[ti][1];

			t = t + 2;

			teapotVertices[v + 0] = vertices[vi][0];
			teapotVertices[v + 1] = vertices[vi][1];
			teapotVertices[v + 2] = vertices[vi][2];

			v = v + 3;
			totalElement = totalElement + 3;
			totalTexcoord = totalTexcoord + 2;
		}
	}

	f = 0;
	for (int i = 0; i < 15704; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			teapotFaceIndices[f] = face_indicies[i][j];
			f = f + 1;
		}
	}

	fprintf(gpFile, "\nf = %d", f);

	glGenVertexArrays(1, &gVao_teapot);
	glBindVertexArray(gVao_teapot);

	glGenBuffers(1, &gVbo_position_teapot);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_position_teapot);
	glBufferData(GL_ARRAY_BUFFER, sizeof(teapotVertices), teapotVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo

	glGenBuffers(1, &gVbo_normals_teapot);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_normals_teapot);
	glBufferData(GL_ARRAY_BUFFER, sizeof(teapotNormal), teapotNormal, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo

	glGenBuffers(1, &gVbo_texture_teapot);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_texture_teapot);
	glBufferData(GL_ARRAY_BUFFER, sizeof(teapotTexcoord), teapotTexcoord, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_TEXTURE_0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_TEXTURE_0);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo

	glBindVertexArray(0); // end for Vao

}

void horizontalLine()
{
	fprintf(gpFile, "\n\n=======================================================================================\n\n");
}

void LoadTexture(const char* filename, unsigned int* tex)
{
	void horizontalLine();

	FILE* fp;
	header h;
	size_t data_start, data_end;
	unsigned char* data;
	GLenum target = GL_NONE;
	horizontalLine();
	fprintf(gpFile, "Entering in LoadTexture() For file %s" , filename);

	fp = fopen(filename, "rb");

	if (fp == NULL)
	{
		fprintf(gpFile, "File for texture not able to open");
		return;
	}

	if (fread(&h, sizeof(h), 1, fp) != 1)
	{
		fprintf(gpFile, "Not able to read data");
		return;
	}

	data_start = ftell(fp) + h.keypairbytes;
	fseek(fp, 0, SEEK_END);
	data_end = ftell(fp);
	fseek(fp, data_start, SEEK_SET);

	data = (unsigned char*)malloc(sizeof(unsigned char) * (data_end - data_start));

	if (data == NULL)
	{
		fprintf(gpFile, "Not able to allocate memory to data");
		return;
	}
	fprintf(gpFile, "\nAllocated memory to data successfully");
	fread(data, 1, data_end - data_start, fp);

	// print header structure values 
	fprintf(gpFile, "\nh.pixelheight = %d \nh.pixeldepth = %d\nh.arrayelements =%d\nh.faces=%d\nh.keypairbytes=%d\nh.miplevels=%d\nh.gltype=%d",
		h.pixelheight, h.pixeldepth, h.arrayelements, h.faces, h.keypairbytes, h.miplevels, h.gltype);

	if (memcmp(h.identifier, identifier, sizeof(identifier)) != 0)
	{
		fprintf(gpFile, "idenitifier not match");
		return;
	}

	if (h.endianness == 0x04030201)
	{
		fprintf(gpFile, "\nNO swap needed %d" , h.endianness);
	}
	else if (h.endianness == 0x01020304)
	{
		fprintf(gpFile, "\nSwap needed");
	}

	glGenTextures(1, tex);
	glBindTexture(GL_TEXTURE_2D, *tex);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	if (h.miplevels == 0)
	{
		fprintf(gpFile, "\nMipmap level is 0 setting to 1");
		h.miplevels = 1;
	}
	
	glTexStorage2D(GL_TEXTURE_2D, h.miplevels, h.glinternalformat, h.pixelwidth ,h.pixelheight);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, h.pixelwidth, h.pixelheight, h.glformat, h.gltype, data);
	glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0);
	fprintf(gpFile, "\nExiting successfully from LoadTexture()");
}

void Resize(int width, int height) {
	if (height == 0)
		height = 1;

	gWidth = width;
	gHeight = height;

	glViewport(0, 0, (GLsizei) width,(GLsizei) height);

	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); // no 0.1f

}

void Display() {
	// FUNCTION DECLARATION
	void RenderObject();

	// CODE	
	glEnable(GL_DEPTH_TEST);
	RenderObject();
	/*
	glUseProgram(gComputeShaderProgramObject);

	glBindImageTexture(0, color_texture, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA32F);
	glBindImageTexture(1, temp_texture, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA32F);

	glDispatchCompute(gHeight, 1, 1);

	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	glBindImageTexture(0, temp_texture, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA32F);
	glBindImageTexture(1, color_texture, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA32F);

	glDispatchCompute(gWidth, 1, 1);

	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
	*/
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, color_texture);
	glDisable(GL_DEPTH_TEST);

	glUseProgram(gDisplayShaderProgramObject);
	glUniform1f(gFocal_distanceUniform, gFocal_distance);
	glUniform1f(gFocal_depthUniform, gFocal_depth);
	glBindVertexArray(gVao_quad);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	glUseProgram(0);

	SwapBuffers(ghdc);
}

void RenderObject()
{
	// variable declaration
	static const GLenum attachments[] = { GL_COLOR_ATTACHMENT0 };
	static const GLfloat ones[] = { 1.0f };
	static const GLfloat zero[] = { 0.0f };
	static const GLfloat gray[] = { 0.1, 0.1, 0.1, 0.0f };

	// START USING OpenGL program object
	glBindFramebuffer(GL_FRAMEBUFFER, depth_fbo);

	glDrawBuffers(1, attachments);
	glViewport(0, 0, gWidth, gHeight);
	glClearBufferfv(GL_COLOR, 0, gray);
	glClearBufferfv(GL_DEPTH, 0, ones);
	
	glUseProgram(gRenderShaderProgramObject);

	//OpenGL Drawing
	// set modelview & modelviewProjection matrices to indentity
	mat4 cameraMatrix = mat4::identity();

	static GLfloat rotateAngle = 0.0f;
	mat4 translateMatrix = mat4::identity();;
	mat4 rotateMatrix = vmath::rotate(rotateAngle, 0.0f, 1.0f, 0.0f);
	mat4 modelViewMatrix = mat4::identity();

	glUniformMatrix4fv(gProjUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glClearBufferfv(GL_DEPTH, 0, ones);

	// first object
	GLfloat object_colors_1[] = { 1.0f, 0.7f, 0.8f };
	glUniform3fv(gDiffuse_albedoUniform, 1, (const GLfloat *)object_colors_1);
	if (isDragon)
	{
		cameraMatrix = vmath::lookat(
			vec3(0.0f, 0.0f, 40.0f),
			vec3(0.0f, 0.0f, 0.0f),
			vec3(0.0f, 1.0f, 0.0f)
		);
		translateMatrix = vmath::translate(5.0f, -4.0f, 20.0f);
		modelViewMatrix = cameraMatrix * translateMatrix * rotateMatrix;
		glUniformMatrix4fv(gMVUniform, 1, GL_FALSE, modelViewMatrix);

		glBindVertexArray(gVao_dragon);
		glDrawArraysInstancedBaseInstance(GL_TRIANGLES, 0, vertex_data_chunk->total_vertices, 1, 0);
		glBindVertexArray(0);
	}
	else if (isTeapot)
	{
		cameraMatrix = vmath::lookat(
			vec3(0.0f, 0.0f, 5.0f),
			vec3(0.0f, 0.0f, 0.0f),
			vec3(0.0f, 1.0f, 0.0f)
		);
		translateMatrix = vmath::translate(0.0f, 0.0f, -1.50f);
		modelViewMatrix = cameraMatrix * translateMatrix * rotateMatrix;
		glUniformMatrix4fv(gMVUniform, 1, GL_FALSE, modelViewMatrix);

		glBindVertexArray(gVao_teapot);
		glDrawArrays(GL_TRIANGLES, 0, 47112);
		glBindVertexArray(0);
	}

	// second object
	GLfloat object_colors_2[] = { 0.7f, 0.8f, 1.0f };
	glUniform3fv(gDiffuse_albedoUniform, 1, (const GLfloat*)object_colors_2);
	if (isDragon)
	{
		cameraMatrix = vmath::lookat(
			vec3(0.0f, 0.0f, 40.0f),
			vec3(0.0f, 0.0f, 0.0f),
			vec3(0.0f, 1.0f, 0.0f)
		);
		translateMatrix = vmath::translate(-5.0f, -4.0f, -3.0f);
		modelViewMatrix = cameraMatrix * translateMatrix * rotateMatrix;
		glUniformMatrix4fv(gMVUniform, 1, GL_FALSE, modelViewMatrix);

		glBindVertexArray(gVao_dragon);
		glDrawArraysInstancedBaseInstance(GL_TRIANGLES, 0, vertex_data_chunk->total_vertices, 1, 0);
		glBindVertexArray(0);
	}
	else if (isTeapot)
	{
		cameraMatrix = vmath::lookat(
			vec3(0.0f, 0.0f, 5.0f),
			vec3(0.0f, 0.0f, 0.0f),
			vec3(0.0f, 1.0f, 0.0f)
		);
		translateMatrix = vmath::translate(-1.0f, 0.0f, -3.0f);
		modelViewMatrix = cameraMatrix * translateMatrix * rotateMatrix;
		glUniformMatrix4fv(gMVUniform, 1, GL_FALSE, modelViewMatrix);

		glBindVertexArray(gVao_teapot);
		glDrawArrays(GL_TRIANGLES, 0, 47112);
		glBindVertexArray(0);
	}

	// third object

	GLfloat object_colors_3[] = { 0.3f, 0.9f, 0.4f };
	glUniform3fv(gDiffuse_albedoUniform, 1, (const GLfloat*)object_colors_3);
	if (isDragon)
	{
		cameraMatrix = vmath::lookat(
			vec3(0.0f, 0.0f, 40.0f),
			vec3(0.0f, 0.0f, 0.0f),
			vec3(0.0f, 1.0f, 0.0f)
		);
		translateMatrix = vmath::translate(-15.0f, -4.0f, -20.0f);
		modelViewMatrix = cameraMatrix * translateMatrix * rotateMatrix;
		glUniformMatrix4fv(gMVUniform, 1, GL_FALSE, modelViewMatrix);

		glBindVertexArray(gVao_dragon);
		glDrawArraysInstancedBaseInstance(GL_TRIANGLES, 0, vertex_data_chunk->total_vertices, 1, 0);
		glBindVertexArray(0);
	}
	else if (isTeapot)
	{
		cameraMatrix = vmath::lookat(
			vec3(0.0f, 0.0f, 5.0f),
			vec3(0.0f, 0.0f, 0.0f),
			vec3(0.0f, 1.0f, 0.0f)
		);
		translateMatrix = vmath::translate(-3.0f, 0.0f, -4.5f);
		modelViewMatrix = cameraMatrix * translateMatrix * rotateMatrix;
		glUniformMatrix4fv(gMVUniform, 1, GL_FALSE, modelViewMatrix);

		glBindVertexArray(gVao_teapot);
		glDrawArrays(GL_TRIANGLES, 0, 47112);
		glBindVertexArray(0);
	}

	// forth object
	GLfloat object_colors_4[] = { 0.6f, 0.4f, 0.9f};
	glUniform3fv(gDiffuse_albedoUniform, 1, (const GLfloat*)object_colors_4);
	if (isDragon)
	{
		cameraMatrix = vmath::lookat(
			vec3(0.0f, 0.0f, 40.0f),
			vec3(0.0f, 0.0f, 0.0f),
			vec3(0.0f, 1.0f, 0.0f)
		);
		translateMatrix = vmath::translate(-25.0f, -4.0f, -40.0f);
		modelViewMatrix = cameraMatrix * translateMatrix * rotateMatrix;
		glUniformMatrix4fv(gMVUniform, 1, GL_FALSE, modelViewMatrix);

		glBindVertexArray(gVao_dragon);
		glDrawArraysInstancedBaseInstance(GL_TRIANGLES, 0, vertex_data_chunk->total_vertices, 1, 0);
		glBindVertexArray(0);
	}
	else if (isTeapot)
	{
		cameraMatrix = vmath::lookat(
			vec3(0.0f, 0.0f, 5.0f),
			vec3(0.0f, 0.0f, 0.0f),
			vec3(0.0f, 1.0f, 0.0f)
		);
		translateMatrix = vmath::translate(-4.0f, 0.0f, -6.0f);
		modelViewMatrix = cameraMatrix * translateMatrix * rotateMatrix;
		glUniformMatrix4fv(gMVUniform, 1, GL_FALSE, modelViewMatrix);

		glBindVertexArray(gVao_teapot);
		glDrawArrays(GL_TRIANGLES, 0, 47112);
		glBindVertexArray(0);
	}


	// fifth object
	GLfloat object_colors_5[] = { 0.8f, 0.2f, 0.1f };
	glUniform3fv(gDiffuse_albedoUniform, 1, (const GLfloat*)object_colors_5);
	if (isDragon)
	{
		cameraMatrix = vmath::lookat(
			vec3(0.0f, 0.0f, 40.0f),
			vec3(0.0f, 0.0f, 0.0f),
			vec3(0.0f, 1.0f, 0.0f)
		);
		translateMatrix = vmath::translate(-35.0f, -4.0f, -60.0f);
		modelViewMatrix = cameraMatrix * translateMatrix * rotateMatrix;
		glUniformMatrix4fv(gMVUniform, 1, GL_FALSE, modelViewMatrix);

		glBindVertexArray(gVao_dragon);
		glDrawArraysInstancedBaseInstance(GL_TRIANGLES, 0, vertex_data_chunk->total_vertices, 1, 0);
		glBindVertexArray(0);
	}
	else if (isTeapot)
	{
		cameraMatrix = vmath::lookat(
			vec3(0.0f, 0.0f, 5.0f),
			vec3(0.0f, 0.0f, 0.0f),
			vec3(0.0f, 1.0f, 0.0f)
		);
		translateMatrix = vmath::translate(-5.0f, 0.0f, -7.5f);
		modelViewMatrix = cameraMatrix * translateMatrix * rotateMatrix;
		glUniformMatrix4fv(gMVUniform, 1, GL_FALSE, modelViewMatrix);

		glBindVertexArray(gVao_teapot);
		glDrawArrays(GL_TRIANGLES, 0, 47112);
		glBindVertexArray(0);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	
	// update
	rotateAngle = rotateAngle + 0.3f;

	if (rotateAngle > 360.0f)
	{
		rotateAngle = 0;
	}
}

void Uninitialize() {

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	// destroy vao
	if (gVao_teapot)
	{
		glDeleteVertexArrays(1, &gVao_teapot);
		gVao_teapot = 0;
	}

	// destroy vbo
	if (gVbo_position_teapot)
	{
		glDeleteBuffers(1, &gVbo_position_teapot);
		gVbo_position_teapot = 0;
	}

	if (gVbo_normals_teapot)
	{
		glDeleteBuffers(1, &gVbo_normals_teapot);
		gVbo_normals_teapot = 0;
	}

	if (gVbo_texture_teapot)
	{
		glDeleteBuffers(1, &gVbo_texture_teapot);
		gVbo_texture_teapot = 0;
	}

	// first programm
	// deatch vertex shader from shader program object
	glDetachShader(gRenderShaderProgramObject, gRenderVertexShaderObject);

	// detach fragment shader from shader program obejct
	glDetachShader(gRenderShaderProgramObject, gRenderFragmentShaderObject);

	// delete vertex shader object
	glDeleteShader(gRenderVertexShaderObject);

	gRenderVertexShaderObject = 0;

	// delete fragment shader object
	glDeleteShader(gRenderFragmentShaderObject);
	gRenderFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gRenderShaderProgramObject);
	gRenderShaderProgramObject = 0;

	// second program
	// deatch vertex shader from shader program object
	glDetachShader(gDisplayShaderProgramObject, gDisplayFragmentShaderObject);

	// detach fragment shader from shader program obejct
	glDetachShader(gDisplayShaderProgramObject, gDisplayVertexShaderObject);

	// delete vertex shader object
	glDeleteShader(gDisplayShaderProgramObject);

	gDisplayShaderProgramObject = 0;

	// delete fragment shader object
	glDeleteShader(gDisplayFragmentShaderObject);
	gDisplayFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gDisplayVertexShaderObject);
	gDisplayVertexShaderObject = 0;

	// 3rd
	// deatch vertex shader from shader program object
	glDetachShader(gComputeShaderProgramObject, gComputeShaderObject);

	// delete vertex shader object
	glDeleteShader(gComputeShaderProgramObject);

	gComputeShaderProgramObject = 0;

	// delete fragment shader object
	glDeleteShader(gComputeShaderObject);
	gComputeShaderObject = 0;

	// unlink shader program
	glUseProgram(0);

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile) {
		fprintf(gpFile, "\n\nClosing File Successfully");
		fclose(gpFile);
	}
}
