// header file declaration
#include <windows.h>
#include<stdio.h>

#include <GL\glew.h>
#include <gl/GL.h>
#include "OGL.h"
#include "vmath.h"

#pragma comment(lib, "C:\\glew-2.1.0\\lib\\Release\\Win32\\glew32.lib")
#pragma comment(lib, "opengl32.lib")

#define WIDTH 1000
#define HIGHT 800

using namespace vmath;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int width, hight;

enum
{
	HRH_ATTRIBUTE_POSITION = 0 ,
	HRH_ATTRIBUTE_COLOR ,
	HRH_ATTRIBUTE_NORMAL ,
	HRH_ATTRIBUTE_TEXTURE_0
};

GLuint gVertexShaderObject_for_light;
GLuint gFragmentShaderObject_for_light;
GLuint gShaderProgramObject_for_light;

GLuint gVertexShaderObject_for_cube;
GLuint gFragmentShaderObject_for_cube;
GLuint gShaderProgramObject_for_cube;

GLuint gVao_cube;
GLuint gVbo_cube_position;
GLuint gVbo_cube_texcoord;
GLuint gvbo_cube_normals;

GLuint gVao_light_cube;

GLuint gModelUniform_for_light;
GLuint gViewUniform_for_light;
GLuint gPerspectiveUniform_for_light;

GLuint gModelUniform_for_cube;
GLuint gViewUniform_for_cube;
GLuint gPerspectiveUniform_for_cube;

GLuint gTextureSamplerUniform_diffuse, gTextureSamplerUniform_specular, gTextureSamplerUniform_emission;
GLuint texture_diffuse = 0, texture_specular = 0, texture_emission = 0;
GLuint gUniformTime = 0;

GLuint gLightAmbientUniform;
GLuint gLightSpecularUniform;
GLuint gLightDiffuseUniform;
GLuint gLightPositionUniform;

GLuint gMaterialSpecularUniform;
GLuint gMaterialAmbientUniform;
GLuint gMaterialDiffuseUniform;
GLuint gMaterialShininessUniform;

GLuint gIsLightOnUniform;
GLuint gIsColorOnUniform;
GLuint gIsTextureOnUniform;

int isLightOn = 0, isColorOn = 0, isTextureOn = 0;

mat4 gPerspectiveProjectionMatrix;

GLfloat lightPosition[] = { 1.2f, 1.0f, 2.0f, 1.0f };
GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f };
GLfloat lightDiffuse[] = { 1.0f, 1.0f, 1.0f };
GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f };

GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f };
GLfloat materialDiffuse[] = { 1.0f, 1.0f, 1.0f };
GLfloat materialSpecular[] = { 0.5f, 0.5f, 0.5f };
GLfloat materialShinyness = 64.0f;

GLfloat time;

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	void Initialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("h_ogl");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 

	if (fopen_s(&gpFile, "windows.log", "w") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("OGL"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HIGHT/2 ,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow) {
				Display();
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	void Resize(int,int);

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_CHAR:
			switch (wParam)
			{
			case 'l':
			case 'L':
				isLightOn = !isLightOn;
				break;
			case 'c':
			case 'C':
				isColorOn = !isColorOn;
				break;
			case 't':
			case 'T':
				isTextureOn = !isTextureOn;
				break;
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else 
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int, int);
	void Uninitialize();
	bool LoadGLTexture(GLuint*, TCHAR[]);

	// Function declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		DestroyWindow(ghwnd);
	}

	//OpenGL Logs
	fprintf(gpFile, "OpenGL Logs");
	fprintf(gpFile, "OpenGL Vendor: %s \n ", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer: %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "OpenGL Shading Language : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	fprintf(gpFile, "Open Enabled extension\n");
	GLint numExtension = 0;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	fprintf(gpFile, "Open Enabled total extension %d \n", numExtension);

	for (int i = 0; i < numExtension; i++)
	{
		//fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	/********************************************************* shaders for cube ********************************************************/

	gVertexShaderObject_for_cube = glCreateShader(GL_VERTEX_SHADER);

	const char* vertexShaderSourceCodeForCube =
		" #version 450 core\n																		" \
		" in vec4 vPosition;																		" \
		" uniform mat4 u_projectionMatrix;															" \
		" uniform mat4 u_modelMatrix;																" \
		" uniform mat4 u_viewMatrix;																" \
		" void main()																				" \
		" {																							" \
		"	gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;			" \
		" }																							";

	glShaderSource(gVertexShaderObject_for_cube, 1, (const GLchar**)&vertexShaderSourceCodeForCube, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject_for_cube);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject_for_cube, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_for_cube, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_for_cube, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	gFragmentShaderObject_for_cube = glCreateShader(GL_FRAGMENT_SHADER);

	const char* fragmentShaderSourceCodeForCube =
		" #version 450 core\n																							" \
		" out vec4 FragColor;																							" \
		" void main()																									" \
		" {																												" \
		"		FragColor = vec4(1.0);																					" \
		" }																												";

	glShaderSource(gFragmentShaderObject_for_cube, 1, (const char**)&fragmentShaderSourceCodeForCube, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject_for_cube);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObject_for_cube, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_for_cube, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_for_cube, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObject_for_cube = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject_for_cube, gVertexShaderObject_for_cube);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObject_for_cube, gFragmentShaderObject_for_cube);

	// pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(gShaderProgramObject_for_cube, HRH_ATTRIBUTE_POSITION, "vPosition");

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObject_for_cube);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject_for_cube, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_for_cube, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_for_cube, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	gModelUniform_for_cube = glGetUniformLocation(gShaderProgramObject_for_cube, "u_modelMatrix");
	gViewUniform_for_cube = glGetUniformLocation(gShaderProgramObject_for_cube, "u_viewMatrix");
	gPerspectiveUniform_for_cube = glGetUniformLocation(gShaderProgramObject_for_cube, "u_projectionMatrix");

	/********************************************************* shaders for light ********************************************************/

	/********** VERTEX SHADER ***********/
	// CREATE SHADER

	gVertexShaderObject_for_light = glCreateShader(GL_VERTEX_SHADER);

	//provide souce code to shader
	const char* vertexShaderSourceCodeForLight =
		" #version 450 core\n																		" \
		" in vec4 vPosition;																		" \
		" in vec3 vNormals;																			" \
		" in vec2 vTexCoord;																		" \
		" in vec3 vColor;																			" \
		" out vec3 outColor;																		" \
		" out vec2 outTexCoord;																		" \
		" uniform mat4 u_projectionMatrix;															" \
		" uniform mat4 u_modelMatrix;																" \
		" uniform mat4 u_viewMatrix;																" \
		" uniform int u_isLightOn;																	" \
		" uniform vec4 u_light_position;															" \
		" out vec3 light_direction;																	" \
		" out vec3 transformed_normal;																" \
		" out vec3 view_vector;																		" \
		" void main()																				" \
		" {																							" \
		"	if(u_isLightOn == 1)																	" \
		"	{																						" \
		"		vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * vPosition;					" \
		"		transformed_normal = mat3(u_viewMatrix * u_modelMatrix) * vNormals ;				" \
		"		light_direction = vec3(u_light_position - eye_coordinates );						" \
		"		view_vector = vec3 (-eye_coordinates);												" \
		"	}																						" \
		"	outTexCoord = vTexCoord;																" \
		"	gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;			" \
		"	outColor = vColor;																		" \
		" }																							";

	glShaderSource(gVertexShaderObject_for_light, 1, (const GLchar**)&vertexShaderSourceCodeForLight, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject_for_light);

	// check compilation error
	iInfoLogLength = 0;
	iShaderCompileStatus = 0;
	szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject_for_light, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_for_light, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_for_light, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gFragmentShaderObject_for_light = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCodeForLight =
		" #version 450 core\n																																		" \
		" vec3 phoung_ads_lighting;																																	" \
		" in vec3 light_direction;																																	" \
		" in vec3 transformed_normal;																																" \
		" in vec3 view_vector;																																		" \
		" vec3 normalized_light_direction;																															" \
		" vec3 normalized_transformed_normal;																														" \
		" vec3 normalized_view_vector;																																" \
		" uniform vec3 u_lightAmbient;																																" \
		" uniform vec3 u_lightSpecular;																																" \
		" uniform vec3 u_lightDiffuse;																																" \
		" uniform vec3 u_materialAmbient;																															" \
		" uniform vec3 u_materialSpecular;																															" \
		" uniform vec3 u_materialDiffuse;																															" \
		" uniform float u_materialShininess;																														" \
		" uniform int u_isLightOn;																																	" \
		" uniform int u_isTexture;																																	" \
		" in vec2 outTexCoord;		 																																" \
		" out vec4 FragColor;																																		" \
		" in vec3 outColor;																																			" \
		" uniform float u_time; " \
		" uniform sampler2D u_texture_sampler_diffuse, u_texture_sampler_specular, u_texture_sampler_emission;																														" \
		" void main()																																				" \
		" {																																							" \
		"	phoung_ads_lighting = vec3(1.0, 1.0, 1.0);																												" \
		"	if(u_isLightOn == 1)																																	" \
		"	{																																						" \
		"		normalized_light_direction = normalize(light_direction);																							" \
		"		normalized_transformed_normal = normalize(transformed_normal);																						" \
		"		normalized_view_vector = normalize(view_vector);																									" \
		"		vec3 reflection_vector = reflect (-normalized_light_direction, normalized_transformed_normal);														" \
		"		vec3 ambient = u_lightAmbient * texture(u_texture_sampler_diffuse, outTexCoord).rgb;																		" \
		"		vec3 diffuse = u_lightDiffuse * texture(u_texture_sampler_diffuse, outTexCoord).rgb  * max( dot ( normalized_light_direction, normalized_transformed_normal), 0.0);	" \
		"		vec3 specular = u_lightSpecular * texture(u_texture_sampler_specular, outTexCoord).rgb * pow( max ( dot (reflection_vector , normalized_view_vector), 0.0) , u_materialShininess);	" \
		"		vec3 emission = vec3(0.0); " \
		"		if( texture(u_texture_sampler_specular, outTexCoord).r == 0.0 ) " \
		"		{ " \
		"			emission = texture(u_texture_sampler_emission, outTexCoord).rgb;			 " \
		"			emission = texture(u_texture_sampler_emission , outTexCoord + vec2(0.0f, u_time)).rgb; " \
		"			emission = emission * (sin(u_time) * 0.5 + 0.5) * 2.0;  " \
		"		}" \
		"		phoung_ads_lighting = ambient + diffuse + specular + emission ;																									" \
		"	}																																						" \
		"	if(u_isLightOn == 1)																																	" \
		"	{																																						" \
		"		FragColor = vec4(phoung_ads_lighting, 1.0);																								" \
		"	}																																						" \
		"	else																																					" \
		"	{																																						" \
		"	FragColor = vec4(1.0, 1.0, 1.0, 1.0);																													" \
		"	}																																						" \
		" }																																							" ;
	
	glShaderSource(gFragmentShaderObject_for_light, 1, (const char**)&fragmentShaderSourceCodeForLight, NULL);

	// COMPILE SHADER
	glCompileShader(gFragmentShaderObject_for_light);

	// compilation error checking

	glGetShaderiv(gFragmentShaderObject_for_light, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_for_light, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_for_light, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gShaderProgramObject_for_light = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject_for_light, gVertexShaderObject_for_light);

	// attch fragment shader to shader program
	glAttachShader(gShaderProgramObject_for_light, gFragmentShaderObject_for_light);

	// pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(gShaderProgramObject_for_light, HRH_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject_for_light, HRH_ATTRIBUTE_TEXTURE_0, "vTexCoord");
	glBindAttribLocation(gShaderProgramObject_for_light, HRH_ATTRIBUTE_NORMAL, "vNormals");

	// LINK PROGRAM
	glLinkProgram(gShaderProgramObject_for_light);

	// CHECK LINKING ERROR
	iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject_for_light, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_for_light, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_for_light, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// get MVP uniform location

	gModelUniform_for_light = glGetUniformLocation(gShaderProgramObject_for_light, "u_modelMatrix");
	gViewUniform_for_light = glGetUniformLocation(gShaderProgramObject_for_light, "u_viewMatrix");
	gPerspectiveUniform_for_light = glGetUniformLocation(gShaderProgramObject_for_light, "u_projectionMatrix");

	gTextureSamplerUniform_diffuse = glGetUniformLocation(gShaderProgramObject_for_light, "u_texture_sampler_diffuse");
	gTextureSamplerUniform_specular = glGetUniformLocation(gShaderProgramObject_for_light, "u_texture_sampler_specular");
	gTextureSamplerUniform_emission = glGetUniformLocation(gShaderProgramObject_for_light, "u_texture_sampler_emission");
	gUniformTime = glGetUniformLocation(gShaderProgramObject_for_light, "u_time");

	gIsLightOnUniform = glGetUniformLocation(gShaderProgramObject_for_light, "u_isLightOn");
	gIsTextureOnUniform = glGetUniformLocation(gShaderProgramObject_for_light, "u_isTexture");

	gLightAmbientUniform = glGetUniformLocation(gShaderProgramObject_for_light, "u_lightAmbient");
	gLightSpecularUniform = glGetUniformLocation(gShaderProgramObject_for_light, "u_lightSpecular");
	gLightDiffuseUniform = glGetUniformLocation(gShaderProgramObject_for_light, "u_lightDiffuse");
	gLightPositionUniform = glGetUniformLocation(gShaderProgramObject_for_light, "u_light_position");

	gMaterialDiffuseUniform = glGetUniformLocation(gShaderProgramObject_for_light, "u_materialDiffuse");
	gMaterialAmbientUniform = glGetUniformLocation(gShaderProgramObject_for_light, "u_materialAmbient");
	gMaterialSpecularUniform = glGetUniformLocation(gShaderProgramObject_for_light, "u_materialSpecular");
	gMaterialShininessUniform = glGetUniformLocation(gShaderProgramObject_for_light, "u_materialShininess");

	/**********************************gVao_square*******************************************************/
	const GLfloat cubeVertices[] =
	{
		// positions        
		-1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,
		 1.0f,  1.0f, -1.0f,
		 1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		 1.0f, -1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		-1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,

		 1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,

		-1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f,  1.0f,
		 1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f, -1.0f,

		-1.0f,  1.0f, -1.0f,
		 1.0f,  1.0f, -1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f, -1.0f,
	};


	const GLfloat cubeTexCoordMatrix[] =
	{
		// texture 
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,

		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,

		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,

		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,

		0.0f, 1.0f,
		1.0f, 1.0f,
		1.0f, 0.0f,
		1.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 1.0f,

		0.0f, 1.0f,
		1.0f, 1.0f,
		1.0f, 0.0f,
		1.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 1.0f
	};

	GLfloat glNormal_coord[] =
	{
		// normals         
		0.0f,  0.0f, -1.0f,
		0.0f,  0.0f, -1.0f,
		0.0f,  0.0f, -1.0f,
		0.0f,  0.0f, -1.0f,
		0.0f,  0.0f, -1.0f,
		0.0f,  0.0f, -1.0f,

		0.0f,  0.0f, 1.0f,
		0.0f,  0.0f, 1.0f,
		0.0f,  0.0f, 1.0f,
		0.0f,  0.0f, 1.0f,
		0.0f,  0.0f, 1.0f,
		0.0f,  0.0f, 1.0f,

		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,

		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,

		0.0f, -1.0f,  0.0f,
		0.0f, -1.0f,  0.0f,
		0.0f, -1.0f,  0.0f,
		0.0f, -1.0f,  0.0f,
		0.0f, -1.0f,  0.0f,
		0.0f, -1.0f,  0.0f,

		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,
	};

		
	glGenVertexArrays(1, &gVao_cube);
	glBindVertexArray(gVao_cube);

	// now give vertex for square geometry
	glGenBuffers(1, &gVbo_cube_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_cube_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &gVbo_cube_texcoord);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_cube_texcoord);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeTexCoordMatrix), cubeTexCoordMatrix, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_TEXTURE_0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_TEXTURE_0);	
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &gvbo_cube_normals);
	glBindBuffer(GL_ARRAY_BUFFER, gvbo_cube_normals);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glNormal_coord), glNormal_coord, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// unbind to gVao_square
	glBindVertexArray(0);

	glGenVertexArrays(1, &gVao_light_cube);
	glBindVertexArray(gVao_light_cube);
	
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_cube_position);
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);

	LoadGLTexture(&texture_diffuse, ID_CONTAINER_BITMAP);
	LoadGLTexture(&texture_specular, ID_CONTAINER_SPECULAR);
	LoadGLTexture(&texture_emission, ID_MATRIX_BITMAP);

	/***************************************************************************************************************/
	// set-up depth buffers
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// we will always cull face for better performance
	//glEnable(GL_CULL_FACE);

	gPerspectiveProjectionMatrix = mat4::identity();

	Resize(WIDTH, HIGHT);
}

// Texture code
bool LoadGLTexture(GLuint* texture, TCHAR resourceID[])
{
	//VARIABLE DECLARATION
	bool bResult = false;
	HBITMAP hBitmap = NULL;
	BITMAP bmp;

	//CODE
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), resourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap)
	{
		bResult = true;
		GetObject(hBitmap, sizeof(BITMAP), &bmp);

		// from here start OpenGL Texture code
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1); //IN FFP 4
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);

		//setting texture parameter
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		// following call will actually push the data with help of graphics driver
		//gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);
		glTexImage2D(GL_TEXTURE_2D, 0, 3, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, bmp.bmBits);		
		glGenerateMipmap(GL_TEXTURE_2D);

		DeleteObject(hBitmap);
	}
	return bResult;
}

void Resize(int width, int height) {
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) height);

	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); // no 0.1f

}

void Display() {
	// CODE
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	static GLfloat angle = 0.0f;

	// START USING OpenGL program object
	glUseProgram(gShaderProgramObject_for_light);

	//OpenGL Drawing For Trianlge
	// set modelview & modelviewProjection matrices to indentity
	mat4 translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);

	//angle = 50;

	mat4 scaleMatrix = mat4::identity();
	mat4 rotateMatrix = //vmath::rotate(angle, 0.0f, 1.0f, 0.0f);
						vmath::rotate(angle, 1.0f, 0.0f, 0.0f) *
						vmath::rotate(angle, 0.0f, 1.0f, 0.0f) *
						vmath::rotate(angle, 0.0f, 0.0f, 1.0f) ;

	mat4 viewMatrix = mat4::identity();
	mat4 modelMatrix = translateMatrix * rotateMatrix ;

	if (isLightOn == 1)
	{
		glUniform4fv(gLightPositionUniform, 1, (const GLfloat*)lightPosition);
		glUniform3fv(gLightDiffuseUniform, 1, (const GLfloat*)lightDiffuse);
		glUniform3fv(gLightAmbientUniform, 1, (const GLfloat*)lightAmbient);
		glUniform3fv(gLightSpecularUniform, 1, (const GLfloat*)lightSpecular);

		glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*)materialAmbient);
		glUniform1f(gMaterialShininessUniform, materialShinyness);
		glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*)materialSpecular);
		glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*)materialDiffuse);
	}

	glUniform1i(gIsLightOnUniform, isLightOn);
	glUniform1f(gUniformTime, time);

	glUniformMatrix4fv(gModelUniform_for_light, 1, GL_FALSE, (const float*)modelMatrix);
	glUniformMatrix4fv(gViewUniform_for_light, 1, GL_FALSE, (const float*)viewMatrix);
	glUniformMatrix4fv(gPerspectiveUniform_for_light, 1, GL_FALSE, (const GLfloat*)gPerspectiveProjectionMatrix);
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_diffuse);
	glUniform1i(gTextureSamplerUniform_diffuse, 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, texture_specular);
	glUniform1i(gTextureSamplerUniform_specular, 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, texture_emission);
	glUniform1i(gTextureSamplerUniform_emission, 2);

	glBindVertexArray(gVao_cube);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);

	// stop using OpenGL program object
	glUseProgram(0);

	// why added below code?
	
	glUseProgram(gShaderProgramObject_for_cube);
	mat4 model_cube = vmath::translate(lightPosition[0], lightPosition[1], lightPosition[2]);
	mat4 scale_cube = vmath::scale(vec3(1.2f));

	glUniformMatrix4fv(gModelUniform_for_cube, 1, GL_FALSE, (const float*)model_cube);
	glUniformMatrix4fv(gViewUniform_for_cube, 1, GL_FALSE, (const float*)viewMatrix);
	glUniformMatrix4fv(gPerspectiveUniform_for_cube, 1, GL_FALSE, (const GLfloat*)gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_light_cube);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);

	glUseProgram(0);

	angle = angle + 0.035f;
	if (angle > 360.0f)
	{
		angle = 0.0f;
	}

	time = time + 0.0007f;

	SwapBuffers(ghdc);
}

void Uninitialize() {

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	// destroy gVao_triangle
	
	if (gVao_cube)
	{
		glDeleteVertexArrays(1, &gVao_cube);
		gVao_cube = 0;
	}

	if (gVbo_cube_texcoord)
	{
		glDeleteBuffers(1, &gVbo_cube_texcoord);
		gVbo_cube_texcoord = 0;
	}

	if (gVbo_cube_position)
	{
		glDeleteBuffers(1, &gVbo_cube_position);
		gVbo_cube_position = 0;
	}

	glDetachShader(gShaderProgramObject_for_cube, gVertexShaderObject_for_cube);

	glDetachShader(gShaderProgramObject_for_cube, gFragmentShaderObject_for_cube);

	glDeleteShader(gFragmentShaderObject_for_cube);

	glDeleteShader(gVertexShaderObject_for_cube);

	glDeleteProgram(gShaderProgramObject_for_cube);

	// deatch vertex shader from shader program object
	glDetachShader(gShaderProgramObject_for_light, gVertexShaderObject_for_light);

	// detach fragment shader from shader program obejct
	glDetachShader(gShaderProgramObject_for_light, gFragmentShaderObject_for_light);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject_for_light);

	gVertexShaderObject_for_light = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject_for_light);
	gFragmentShaderObject_for_light = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject_for_light);
	gShaderProgramObject_for_light = 0;

	// unlink shader program
	glUseProgram(0);

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile) {
		fprintf(gpFile, "\n Closing File Successfully");
		fclose(gpFile);
	}
}
