// header file declaration
#include <windows.h>
#include<stdio.h>

#include <GL\glew.h>
#include <gl/GL.h>
#include "OGL.h"
#include "vmath.h"

#pragma comment(lib, "C:\\glew-2.1.0\\lib\\Release\\Win32\\glew32.lib")
#pragma comment(lib, "opengl32.lib")

#define WIDTH 1000
#define HIGHT 800

using namespace vmath;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int width, hight;

enum
{
	HRH_ATTRIBUTE_POSITION = 0 ,
	HRH_ATTRIBUTE_COLOR ,
	HRH_ATTRIBUTE_NORMAL ,
	HRH_ATTRIBUTE_TEXTURE_0
};

GLuint gSimpleDepthVertexShaderObject;
GLuint gSimpleDepthFragmentShaderObject;
GLuint gSimpleDepthShaderProgramObject;

GLuint gDebugDepthQuadVertexShaderObject;
GLuint gDebugDepthQuadFragmentShaderObject;
GLuint gDebugDepthQuadShaderProgramObject;

GLuint gVao_cube;
GLuint gVbo_cube_position;
GLuint gVbo_cube_texcoord;
GLuint gVbo_cube_color;
GLuint gvbo_cube_normals;

GLuint gModelUniform;
GLuint gViewUniform;
GLuint gPerspectiveUniform;
GLuint gLightSpaceMatrixUniform;

GLuint gDepthMapUniform;
GLuint gNearPlaneUniform;
GLuint gFarPlaneUniform;

GLuint gTextureSamplerUniform;
GLuint texture = 0;

GLuint gLightAmbientUniform;
GLuint gLightSpecularUniform;
GLuint gLightDiffuseUniform;
GLuint gLightPositionUniform;

GLuint gMaterialSpecularUniform;
GLuint gMaterialAmbientUniform;
GLuint gMaterialDiffuseUniform;
GLuint gMaterialShininessUniform;

GLuint gIsLightOnUniform;
GLuint gIsColorOnUniform;
GLuint gIsTextureOnUniform;

int isLightOn = 0, isColorOn = 0, isTextureOn = 0;

mat4 gPerspectiveProjectionMatrix;

GLfloat lightPosition[] = { -2.0f, 4.0f, -1.0f };
GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f };
GLfloat lightDiffuse[] = { 1.0f, 1.0f, 1.0f };
GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f };

GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f };
GLfloat materialDiffuse[] = { 1.0f, 1.0f, 1.0f };
GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0f };
GLfloat materialShinyness = 128.0f;

GLuint depthMapFBO;
GLuint quadVAO = 0, quadVBO;
GLuint planeVAO = 0, planeVBO;

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	void Initialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("h_ogl");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 

	if (fopen_s(&gpFile, "windows.log", "w") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("OGL"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HIGHT/2 ,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow) {
				Display();
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	void Resize(int,int);

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_CHAR:
			switch (wParam)
			{
			case 'l':
			case 'L':
				isLightOn = !isLightOn;
				break;
			case 'c':
			case 'C':
				isColorOn = !isColorOn;
				break;
			case 't':
			case 'T':
				isTextureOn = !isTextureOn;
				break;
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else 
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int, int);
	void LoadShaders();
	void Uninitialize();
	bool LoadGLTexture(GLuint*, TCHAR[]);

	// Function declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		DestroyWindow(ghwnd);
	}

	//OpenGL Logs
	fprintf(gpFile, "OpenGL Logs");
	fprintf(gpFile, "OpenGL Vendor: %s \n ", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer: %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "OpenGL Shading Language : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	fprintf(gpFile, "Open Enabled extension\n");
	GLint numExtension = 0;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	fprintf(gpFile, "Open Enabled total extension %d \n", numExtension);

	for (int i = 0; i < numExtension; i++)
	{
		fprintf(gpFile, "%s\t", glGetStringi(GL_EXTENSIONS,i));
	}

	LoadShaders();
		
	/**********************************gVao_square*******************************************************/
	const GLfloat cubeVertices[] =
	{
		// FRONT
		1.0f,1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,


		// RIGHT
		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f,-1.0f,

		//BACK
		1.0f, -1.0f,-1.0f,
		-1.0f, -1.0f,-1.0f,
		-1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,

		// LEFT
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,

		// TOP
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		// BOTTOM
		1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f

	};


	const GLfloat cubeTexCoordMatrix[] =
	{
		// 1
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f,1.0f,
		0.0f,1.0f,

		//2
		1.0f, 0.0f,
		1.0f,1.0f,
		0.0f,1.0f,
		0.0f, 0.0f,

		//3
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,

		//4
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,

		//5
		0.0f,1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,

		//6
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,

	};

	const GLfloat cubeColorMatrix[] =
	{
		// 1
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,

		//2
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,

		//3
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,

		//4
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,

		//5
		0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f,

		//6
		1.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 1.0f,

	};

	GLfloat glNormal_coord[] =
	{

		0.0f,  0.0f,  1.0f , //front
		0.0f,  0.0f,  1.0f , //front
		0.0f,  0.0f,  1.0f , //front
		0.0f,  0.0f,  1.0f , //front

		1.0f,  0.0f,  0.0f , // right
		1.0f,  0.0f,  0.0f , // right
		1.0f,  0.0f,  0.0f , // right
		1.0f,  0.0f,  0.0f , // right

		0.0f, 0.0f,  -1.0f , //back
		0.0f, 0.0f,  -1.0f , //back
		0.0f, 0.0f,  -1.0f , //back
		0.0f, 0.0f,  -1.0f , //back

		-1.0f,  0.0f, 0.0f , //left
		-1.0f,  0.0f, 0.0f , //left
		-1.0f,  0.0f, 0.0f , //left
		-1.0f,  0.0f, 0.0f , //left

		0.0f,  1.0f,  0.0f , // top
		0.0f,  1.0f,  0.0f , // top
		0.0f,  1.0f,  0.0f , // top
		0.0f,  1.0f,  0.0f , // top

		0.0f, -1.0f,  0.0f , // bottom
		0.0f, -1.0f,  0.0f , // bottom
		0.0f, -1.0f,  0.0f , // bottom
		0.0f, -1.0f,  0.0f , // bottom

	};

	glGenVertexArrays(1, &gVao_cube);
	glBindVertexArray(gVao_cube);

	// now give vertex for square geometry
	glGenBuffers(1, &gVbo_cube_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_cube_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &gVbo_cube_texcoord);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_cube_texcoord);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeTexCoordMatrix), cubeTexCoordMatrix, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_TEXTURE_0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_TEXTURE_0);	
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &gVbo_cube_color);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_cube_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeColorMatrix), cubeColorMatrix, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &gvbo_cube_normals);
	glBindBuffer(GL_ARRAY_BUFFER, gvbo_cube_normals);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glNormal_coord), glNormal_coord, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	float quadVertices[] = {
		// positions        // texture Coords
		-1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
		-1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
		 1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
		 1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
	};

	glGenVertexArrays(1, &quadVAO);
	glGenBuffers(1, &quadVBO);
	glBindVertexArray(quadVAO);
	glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_TEXTURE_0);
	glVertexAttribPointer(HRH_ATTRIBUTE_TEXTURE_0, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

	float planeVertices[] = {
		// positions            // normals         // texcoords
		 25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
		-25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,   0.0f,  0.0f,
		-25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,

		 25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
		-25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,
		 25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,  25.0f, 10.0f
	};

	glGenVertexArrays(1, &planeVAO);
	glBindVertexArray(planeVAO);

	glGenBuffers(1, &planeVBO);

	glBindBuffer(GL_ARRAY_BUFFER, planeVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(planeVertices), &planeVertices, GL_STATIC_DRAW);

	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);

	glEnableVertexAttribArray(HRH_ATTRIBUTE_NORMAL);
	glVertexAttribPointer(HRH_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));

	glEnableVertexAttribArray(HRH_ATTRIBUTE_TEXTURE_0);
	glVertexAttribPointer(HRH_ATTRIBUTE_TEXTURE_0, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));

	glBindVertexArray(0);

	// unbind to gVao_square
	glBindVertexArray(0);

	LoadGLTexture(&texture, ID_MARBLE_BITMAP);

	glGenFramebuffers(1, &depthMapFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, texture, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	/***************************************************************************************************************/
	// set-up depth buffers
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	gPerspectiveProjectionMatrix = mat4::identity();

	Resize(WIDTH, HIGHT);
}

void LoadShaders()
{
	// function declarartions
	void LoadSimpleDepthShaders();
	void LoadDebugDepthQuadShaders();

	// code
	LoadSimpleDepthShaders();
	LoadDebugDepthQuadShaders();

}

void LoadSimpleDepthShaders()
{
	/********** VERTEX SHADER ***********/
	// CREATE SHADER

	gSimpleDepthVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//provide souce code to shader
	const GLchar* vertexShaderSourceCode =
		" #version 450 core 												\n " \
		" in vec3 aPos;														\n " \
		"                                                                   \n " \
		" uniform mat4 lightSpaceMatrix;                                    \n " \
		" uniform mat4 model;                                               \n " \
		"                                                                   \n " \
		" void main()                                                       \n " \
		" {                                                                 \n " \
		"     gl_Position = lightSpaceMatrix * model * vec4(aPos, 1.0);     \n " \
		" }                                                                 \n ";

	glShaderSource(gSimpleDepthVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gSimpleDepthVertexShaderObject);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gSimpleDepthVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gSimpleDepthVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gSimpleDepthVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gSimpleDepthFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCode =
		" #version 450 core 									\n " \
		"                                                       \n " \
		" void main()                                           \n " \
		" {                                                     \n " \
		"     // gl_FragDepth = gl_FragCoord.z;                 \n " \
		" }                                                     \n " ;

	glShaderSource(gSimpleDepthFragmentShaderObject, 1, (const char**)&fragmentShaderSourceCode, NULL);

	// COMPILE SHADER
	glCompileShader(gSimpleDepthFragmentShaderObject);

	// compilation error checking

	glGetShaderiv(gSimpleDepthFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gSimpleDepthFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gSimpleDepthFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gSimpleDepthShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gSimpleDepthShaderProgramObject, gSimpleDepthVertexShaderObject);

	// attch fragment shader to shader program
	glAttachShader(gSimpleDepthShaderProgramObject, gSimpleDepthFragmentShaderObject);

	// pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(gSimpleDepthShaderProgramObject, HRH_ATTRIBUTE_POSITION, "aPos");

	// LINK PROGRAM
	glLinkProgram(gSimpleDepthShaderProgramObject);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gSimpleDepthShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gSimpleDepthShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gSimpleDepthShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// get MVP uniform location

	gModelUniform = glGetUniformLocation(gSimpleDepthShaderProgramObject, "u_modelMatrix");
	gLightSpaceMatrixUniform = glGetUniformLocation(gSimpleDepthShaderProgramObject, "lightSpaceMatrix");
}

void LoadDebugDepthQuadShaders()
{
	/********** VERTEX SHADER ***********/
	// CREATE SHADER

	gDebugDepthQuadVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//provide souce code to shader
	const GLchar* vertexShaderSourceCode =
		" #version 450 core												\n " \
		" in vec3 aPos;													\n " \
		" in vec2 aTexCoords;											\n " \
		"                                                               \n " \
		" out vec2 TexCoords;                                           \n " \
		"                                                               \n " \
		" void main()                                                   \n " \
		" {                                                             \n " \
		"     TexCoords = aTexCoords;                                   \n " \
		"     gl_Position = vec4(aPos, 1.0);                            \n " \
		" }                                                             \n ";

	glShaderSource(gDebugDepthQuadVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gDebugDepthQuadVertexShaderObject);

	// check compilation error
	GLint iInfoLogLength = 0;
	GLint iShaderCompileStatus = 0;
	char* szInfoLog = NULL;
	glGetShaderiv(gDebugDepthQuadVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gDebugDepthQuadVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gDebugDepthQuadVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation error : %s \n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	/***************** FRAGMENT SHADER **********************/

	// CREATE SHADER
	gDebugDepthQuadFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//PROVIDE SOURCE CODE

	const char* fragmentShaderSourceCode =
		" #version 450 core                                                                                             \n " \
		" out vec4 FragColor;                                                                                           \n " \
		"                                                                                                               \n " \
		" in vec2 TexCoords;                                                                                            \n " \
		"                                                                                                               \n " \
		" uniform sampler2D depthMap;                                                                                   \n " \
		" uniform float near_plane;                                                                                     \n " \
		" uniform float far_plane;                                                                                      \n " \
		"                                                                                                               \n " \
		" // required when using a perspective projection matrix                                                        \n " \
		" float LinearizeDepth(float depth)                                                                             \n " \
		" {                                                                                                             \n " \
		"     float z = depth * 2.0 - 1.0; // Back to NDC                                                               \n " \
		"     return (2.0 * near_plane * far_plane) / (far_plane + near_plane - z * (far_plane - near_plane));			\n " \
		" }                                                                                                             \n " \
		"                                                                                                               \n " \
		" void main()                                                                                                   \n " \
		" {                                                                                                             \n " \
		"     float depthValue = texture(depthMap, TexCoords).r;                                                        \n " \
		"     // FragColor = vec4(vec3(LinearizeDepth(depthValue) / far_plane), 1.0); // perspective                    \n " \
		"     FragColor = vec4(vec3(depthValue), 1.0); // orthographic                                                  \n " \
		" }                                                                                                             \n " ;

	glShaderSource(gDebugDepthQuadFragmentShaderObject, 1, (const char**)&fragmentShaderSourceCode, NULL);

	// COMPILE SHADER
	glCompileShader(gDebugDepthQuadFragmentShaderObject);

	// compilation error checking

	glGetShaderiv(gDebugDepthQuadFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gDebugDepthQuadFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gDebugDepthQuadFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Error : %s\n", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// SHADER PROGRAM
	//CREATE SHADER PROGRAM

	gDebugDepthQuadShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gDebugDepthQuadShaderProgramObject, gDebugDepthQuadVertexShaderObject);

	// attch fragment shader to shader program
	glAttachShader(gDebugDepthQuadShaderProgramObject, gDebugDepthQuadFragmentShaderObject);

	// pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(gDebugDepthQuadShaderProgramObject, HRH_ATTRIBUTE_POSITION, "aPos");
	glBindAttribLocation(gDebugDepthQuadShaderProgramObject, HRH_ATTRIBUTE_TEXTURE_0, "aTexCoords");

	// LINK PROGRAM
	glLinkProgram(gDebugDepthQuadShaderProgramObject);

	// CHECK LINKING ERROR
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gDebugDepthQuadShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gDebugDepthQuadShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gDebugDepthQuadShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}

	// get MVP uniform location
	gDepthMapUniform = glGetUniformLocation(gDebugDepthQuadShaderProgramObject, "depthMap");
	gNearPlaneUniform = glGetUniformLocation(gDebugDepthQuadShaderProgramObject, "near_plane");
	gFarPlaneUniform = glGetUniformLocation(gDebugDepthQuadShaderProgramObject, "far_plane");
}


// Texture code
bool LoadGLTexture(GLuint* texture, TCHAR resourceID[])
{
	//VARIABLE DECLARATION
	bool bResult = false;
	HBITMAP hBitmap = NULL;
	BITMAP bmp;

	//CODE
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), resourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap)
	{
		bResult = true;
		GetObject(hBitmap, sizeof(BITMAP), &bmp);

		// from here start OpenGL Texture code
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1); //IN FFP 4
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);

		//setting texture parameter
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		// following call will actually push the data with help of graphics driver
		//gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);
		glTexImage2D(GL_TEXTURE_2D, 0, 3, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, bmp.bmBits);		
		glGenerateMipmap(GL_TEXTURE_2D);

		DeleteObject(hBitmap);
	}
	return bResult;
}

void Resize(int width, int height) {
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) height);

	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); // no 0.1f

}

void Display() 
{
	// function declarartions
	void renderScene();

	// variable declarations
	static GLfloat angle = 0.0f;

	// CODE
	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// START USING OpenGL program object
	
	// OpenGL Drawing For Trianlge
	// set modelview & modelviewProjection matrices to indentity
	mat4 translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
	mat4 scaleMatrix = mat4::identity();
	mat4 rotateMatrix = vmath::rotate(angle, 1.0f, 0.0f, 0.0f) *
						vmath::rotate(angle, 0.0f, 1.0f, 0.0f) *
						vmath::rotate(angle, 0.0f, 0.0f, 1.0f) ;
	mat4 viewMatrix = mat4::identity();
	mat4 modelMatrix = translateMatrix * rotateMatrix ;

	mat4 lightProjection, lightView, lightSpaceMatrix;
	float near_plane = 1.0f, far_plane = 7.5f;

	lightProjection = vmath::ortho(-10.0f, 10.0f, -10.0f, 10.0f, near_plane, far_plane);
	lightView = vmath::lookat(  vmath::vec3(-2.0f, 4.0f, -1.0f),
								vmath::vec3( 0.0f, 0.0f,  0.0f),
								vmath::vec3( 0.0f, 1.0f,  0.0f)
							);
	lightSpaceMatrix = lightProjection * lightView;

	glUseProgram(gSimpleDepthShaderProgramObject);

	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);
	glUniformMatrix4fv(gViewUniform, 1, GL_FALSE, (const float*)viewMatrix);
	glUniformMatrix4fv(gPerspectiveUniform, 1, GL_FALSE, (const GLfloat*)gPerspectiveProjectionMatrix);
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	glUniform1i(gTextureSamplerUniform, 0);

	// stop using OpenGL program object
	glUseProgram(0);

	angle = angle + 0.1f;

	SwapBuffers(ghdc);
}

void renderScene()
{
	// variable declarations
	void renderCube(void);

	glBindVertexArray(planeVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);

	// first cube
	mat4 modelMatrix = mat4::identity();
	mat4 translateMatrix = vmath::translate(0.0f, 1.5f, 0.0f);
	mat4 scaleMatrix = vmath::scale(0.5f, 0.5f, 0.5f);
	modelMatrix = translateMatrix * scaleMatrix;
	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	renderCube();

	// second cube
	modelMatrix = mat4::identity();
	translateMatrix = vmath::translate(2.0f, 0.0f, 0.0f);
	scaleMatrix = vmath::scale(0.5f, 0.5f, 0.5f);
	modelMatrix = translateMatrix * scaleMatrix;
	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	renderCube();

	// third cube
	modelMatrix = mat4::identity();
	translateMatrix = vmath::translate(-1.0f, 0.0f, 2.0f);
	scaleMatrix = vmath::scale(0.25f, 0.25f, 0.25f);
	modelMatrix = translateMatrix * scaleMatrix;
	glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*)modelMatrix);

	renderCube();
}

void renderCube()
{
	glBindVertexArray(gVao_cube);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
	glBindVertexArray(0);
}

void renderQuad()
{
	glBindVertexArray(quadVAO);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glBindVertexArray(0);
}

void Uninitialize() {

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	// destroy gVao_triangle
	
	if (gVao_cube)
	{
		glDeleteVertexArrays(1, &gVao_cube);
		gVao_cube = 0;
	}

	if (gVbo_cube_texcoord)
	{
		glDeleteBuffers(1, &gVbo_cube_texcoord);
		gVbo_cube_texcoord = 0;
	}

	if (gVbo_cube_position)
	{
		glDeleteBuffers(1, &gVbo_cube_position);
		gVbo_cube_position = 0;
	}

	if (gVbo_cube_color)
	{
		glDeleteBuffers(1, &gVbo_cube_color);
		gVbo_cube_color = 0;
	}

	// unlink shader program
	glUseProgram(0);

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile) {
		fprintf(gpFile, "\n Closing File Successfully");
		fclose(gpFile);
	}
}
