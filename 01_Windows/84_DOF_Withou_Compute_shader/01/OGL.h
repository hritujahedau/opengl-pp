#pragma once
#include <windows.h>
#include <gl\GL.h>
#include "vmath.h"

#define MYICON 1287
#define IDI_MYICON MAKEINTRESOURCE(MYICON)

struct header
{
    unsigned char       identifier[12];
    unsigned int        endianness;
    unsigned int        gltype;
    unsigned int        gltypesize;
    unsigned int        glformat;
    unsigned int        glinternalformat;
    unsigned int        glbaseinternalformat;
    unsigned int        pixelwidth;
    unsigned int        pixelheight;
    unsigned int        pixeldepth;
    unsigned int        arrayelements;
    unsigned int        faces;
    unsigned int        miplevels;
    unsigned int        keypairbytes;
};

static const unsigned char identifier[] =
{
    0xAB, 0x4B, 0x54, 0x58, 0x20, 0x31, 0x31, 0xBB, 0x0D, 0x0A, 0x1A, 0x0A
};


typedef struct SB6M_HEADER_t
{
    union
    {
        unsigned int    magic;
        char            magic_name[4];
    };
    unsigned int        size;
    unsigned int        num_chunks;
    unsigned int        flags;
} SB6M_HEADER;

typedef struct SB6M_CHUNK_HEADER_t
{
    union
    {
        unsigned int    chunk_type;
        char            chunk_name[4];
    };
    unsigned int        size;
} SB6M_CHUNK_HEADER;


typedef struct SB6M_DATA_CHUNK_t
{
    SB6M_CHUNK_HEADER           header;
    unsigned int                encoding;
    unsigned int                data_offset;
    unsigned int                data_length;
} SB6M_DATA_CHUNK;


typedef struct SB6M_VERTEX_ATTRIB_DECL_t
{
    char                name[64];
    unsigned int        size;
    unsigned int        type;
    unsigned int        stride;
    unsigned int        flags;
    unsigned int        data_offset;
} SB6M_VERTEX_ATTRIB_DECL;

typedef struct SB6M_VERTEX_ATTRIB_CHUNK_t
{
    SB6M_CHUNK_HEADER           header;
    unsigned int                attrib_count;
    SB6M_VERTEX_ATTRIB_DECL     attrib_data[1];
} SB6M_VERTEX_ATTRIB_CHUNK;

typedef struct SB6M_CHUNK_VERTEX_DATA_t
{
    SB6M_CHUNK_HEADER   header;
    unsigned int        data_size;
    unsigned int        data_offset;
    unsigned int        total_vertices;
} SB6M_CHUNK_VERTEX_DATA;


