//
//  window.m
//  
//
//  Created by user166446 on 7/8/21.
//

// stdio.h is inlcuded in Foundation.h 
#import <Foundation/Foundation.h> // similar to standard library
#import <Cocoa/Cocoa.h> // xlib.h windows.h
#import <QuartzCore/CVDisplayLink.h>     //core video
#import <OpenGL/GL3.h>      // gl.h

#import "vmath.h"

using namespace vmath;

// global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef , const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags , CVOptionFlags *, void *);

// global variable declarations
FILE *gpFile = NULL;

enum
{
    HRH_ATTRIBUTE_POSITION = 0 ,
    HRH_ATTRIBUTE_COLOR ,
    HRH_ATTRIBUTE_NORMAL ,
    HRH_ATTRIBUTE_TEXTURE_0
};

// NS - NextSTEP
// Delegation performance <>
@interface AppDelegate:NSObject <NSApplicationDelegate,NSWindowDelegate>
@end

int main(int argc, char *argv[])
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    
    NSApp = [NSApplication sharedApplication];
    
    [NSApp setDelegate:[[AppDelegate alloc]init]];
    
    [NSApp run]; // run loop/ message loop/ event loop
    
    [pool release];
    
    return 0;
}

@interface MyOpenGLView:NSOpenGLView

@end

@implementation AppDelegate
{
    @private
    NSWindow *window ;
    MyOpenGLView *myOpenGLview;
}

// WM_CREATE
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // code
    // internally CGrect, core graphics , c library
    // first 2 variable together NSPoint , CGPoint
    // last 2 variable together NSSize structure , CGSzie

    NSBundle *appBundle = [NSBundle mainBundle];
    NSString *appDirPath = [appBundle bundlePath]; // /User/Hrituja/Desktop/RTR3.0/02_LogFile/window.app
    NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent]; // remove window.app give /User/Hrituja/Desktop/RTR3.0/02_LogFile
    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/log.txt", parentDirPath];
    const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    
    gpFile = fopen(pszLogFileNameWithPath, "w");
    if(gpFile == NULL)
    {
        [self release];
        [NSApp terminate:self];
        // end application 
    }
    
    fprintf(gpFile, "\nFile open successfully\n");

    NSRect win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);
    window = [[NSWindow alloc]initWithContentRect:win_rect
                                        styleMask:NSWindowStyleMaskTitled |
                                                NSWindowStyleMaskClosable |
                                                NSWindowStyleMaskMiniaturizable |
                                                NSWindowStyleMaskResizable
                                          backing:NSBackingStoreBuffered
                                           defer:NO
              ];
    [window setTitle:@"HRH: OGL"];
    [window center];
    
    myOpenGLview = [[MyOpenGLView alloc]initWithFrame:win_rect];
    
    [window setContentView:myOpenGLview];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

// WM_DESTTROY
-(void)applicationWillTerminate:(NSNotification *)aNotification
{
    //code

    if(gpFile)
    {
        fprintf(gpFile , "\nProgram terminated successfully\n");
        fclose(gpFile);
        gpFile = NULL;
    }
}

-(void)windowWillClose:(NSNotification *)aNotification
{
    //code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [myOpenGLview release];
    [window release];
    [super dealloc];

}
@end

@implementation MyOpenGLView
{
    @private
    CVDisplayLinkRef cvDisplayLinkRef;

    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;

    GLuint vao_cube;
    GLuint vbo_cube_position;
    GLuint vbo_cube_normals;

    GLuint modelViewUniform;
    GLuint perspectiveProjectionUniform;
    GLuint lKeyPressedUniform;
    GLuint ldUniform;
    GLuint kdUniform;
    GLuint lightPossitionUniform;

    BOOL bAnimate ;
    BOOL bLight ;

    GLuint mvpUniform;

    mat4 perspectiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame
{
    //code
    self = [super initWithFrame:frame];

    if(self)
    {
        // pixel format descripor
        NSOpenGLPixelFormatAttribute attributes[] =
                                                    {
                                                        NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
                                                        NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
                                                        NSOpenGLPFANoRecovery,
                                                        NSOpenGLPFAAccelerated,
                                                        NSOpenGLPFAColorSize, 24,
                                                        NSOpenGLPFADepthSize, 24,
                                                        NSOpenGLPFAAlphaSize, 8,
                                                        NSOpenGLPFADoubleBuffer,
                                                        0
                                                    };
        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attributes]autorelease];
    
        if(pixelFormat == nil)
        {
            fprintf(gpFile, "\nPixel Format failed");
            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease]; // get pixell format

        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext];
    }



    return self;
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)output_time
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

    [self drawView];

    [pool release];

    return kCVReturnSuccess;
}

-(void)prepareOpenGL
{
    //code
    [super prepareOpenGL];

    [[self openGLContext]makeCurrentContext];

    // swap interval
    GLint swapInt = 1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];  // cp - context parameter

    
    // write shaders here
    if( [self loadShaders] == NO)
    {
       [self unintialize];
    }
    
    [self loadObjects];

    glClearColor(0.0f, 0.0f, 0.0f, 1.0);
    //glShadeModel(GL_SMOOTH);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    //glEnable(GL_CULL_FACE);

    perspectiveProjectionMatrix = mat4::identity();

    // core video and core graphics related code

    // 1. Create display link
    // 2. Set callback for display link
    // 3. NSContext -> CGL
    // 4. NSPixelFormat -> CGL
    // 5. Set current CGL context and CGL pixel format 
 

    CVDisplayLinkCreateWithActiveCGDisplays(&cvDisplayLinkRef);
    CVDisplayLinkSetOutputCallback(cvDisplayLinkRef, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(cvDisplayLinkRef, cglContext, cglPixelFormat);
    CVDisplayLinkStart(cvDisplayLinkRef);

}

-(BOOL)loadShaders
{
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    //provide souce code to shader
    const GLchar* vertexShaderSourceCode =
                                            "#version 410 core\n"\
											"in vec4 v_position; " \
											"in vec3 v_normals; " \
											"uniform mat4 u_projection_matrix_uniform; " \
											"uniform mat4 u_model_view_matrix; " \
											"uniform int u_lKeyPressed;" \
											"uniform vec3 u_kd; " \
											"uniform vec3 u_ld; " \
											"uniform vec4 u_light_possition; " \
											"out vec3 diffuse_light; " \
											"void main() " \
											"{ " \
											"if(u_lKeyPressed == 1) " \
											"{ " \
												"vec4 eye_coordinates = u_model_view_matrix  * v_position;" \
												"mat3 normal_matrix = mat3( transpose (inverse ( u_model_view_matrix ) ) );" \
												"vec3 transformed_normal = normalize ( normal_matrix * v_normals) ;" \
												"vec3 source = normalize ( vec3  ( u_light_possition - eye_coordinates)) ;  " \
												"diffuse_light = u_ld * u_kd * max ( dot (source, transformed_normal), 0.0);  " \
											"}" \
											"gl_Position = u_projection_matrix_uniform * u_model_view_matrix * v_position;"\
											"}";

    glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

    // compile shader
    glCompileShader(vertexShaderObject);

    // check compilation error
    GLint iInfoLogLength = 0;
    GLint iShaderCompileStatus = 0;
    char* szInfoLog = NULL;
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("Vertex shader compilation error : %s \n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    /***************** FRAGMENT SHADER **********************/

    // CREATE SHADER
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    //PROVIDE SOURCE CODE

    const char* fragmentShaderSourceCode =
                                            "#version 410 core\n"\
											"in vec3 diffuse_light; " \
											"uniform int u_lKeyPressed; "  \
											"out vec4 FragColor; " \
											"void main()" \
											"{" \
											"vec4 color; " \
											"if( u_lKeyPressed == 1) " \
											"{ " \
											"color = vec4(diffuse_light , 1.0); " \
											"} " \
											"else " \
											"{ " \
											"color = vec4(1.0, 1.0, 1.0, 1.0); " \
											"} " \
											"FragColor = color; " \
											"} ";
    

    glShaderSource(fragmentShaderObject, 1, (const char**)&fragmentShaderSourceCode, NULL);

    // COMPILE SHADER
    glCompileShader(fragmentShaderObject);

    // compilation error checking

    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("Fragment Shader Compilation Error : %s\n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    // SHADER PROGRAM
    //CREATE SHADER PROGRAM

    shaderProgramObject = glCreateProgram();

    // attach vertex shader to shader program
    glAttachShader(shaderProgramObject, vertexShaderObject);

    // attch fragment shader to shader program
    glAttachShader(shaderProgramObject, fragmentShaderObject);

    // pri-link binding of shader object with vertex shader possition attribute
	glBindAttribLocation(shaderProgramObject, HRH_ATTRIBUTE_POSITION, "v_position");
	glBindAttribLocation(shaderProgramObject, HRH_ATTRIBUTE_NORMAL, "v_normals");

    // LINK PROGRAM
    glLinkProgram(shaderProgramObject);

    // CHECK LINKING ERROR
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                printf("Shader Program Link Log : %s", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }
    
    // get MVP uniform location
    modelViewUniform = glGetUniformLocation(shaderProgramObject, "u_model_view_matrix");
	perspectiveProjectionUniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix_uniform");
	lKeyPressedUniform = glGetUniformLocation(shaderProgramObject, "u_lKeyPressed");
	ldUniform = glGetUniformLocation(shaderProgramObject, "u_ld");
	kdUniform = glGetUniformLocation(shaderProgramObject, "u_kd");
	lightPossitionUniform = glGetUniformLocation(shaderProgramObject, "u_light_possition");
    
    return YES;
}

-(void)loadObjects
{
    // ***** vertex, colors, shadersatribs , vbo_pyramid_position, vao_pyramid initialization ********/
    // square
	const GLfloat cubeVertices[] =
	{
		// FRONT
	   1.0f,1.0f, 1.0f,
	   -1.0f, 1.0f, 1.0f,
	   -1.0f, -1.0f, 1.0f,
	   1.0f, -1.0f, 1.0f,

	   // RIGHT
	   1.0f, 1.0f, -1.0f,
	   1.0f, 1.0f, 1.0f,
	   1.0f, -1.0f, 1.0f,
	   1.0f, -1.0f,-1.0f,

	   //BACK
	   1.0f, -1.0f,-1.0f,
	   -1.0f, -1.0f,-1.0f,
	   -1.0f, 1.0f, -1.0f,
	   1.0f, 1.0f, -1.0f,

	   // LEFT
	   -1.0f, 1.0f, 1.0f,
	   -1.0f, 1.0f, -1.0f,
	   -1.0f, -1.0f, -1.0f,
	   -1.0f, -1.0f, 1.0f,

	   // TOP
	   1.0f, 1.0f, -1.0f,
	   -1.0f, 1.0f, -1.0f,
	   -1.0f, 1.0f, 1.0f,
	   1.0f, 1.0f, 1.0f,

	   // BOTTOM
	   1.0f, -1.0f, 1.0f,
	   -1.0f, -1.0f, 1.0f,
	   -1.0f, -1.0f, -1.0f,
	   1.0f, -1.0f, -1.0f,

	};

	GLfloat glNormal_coord[] = 
				{
				0.0f,  0.0f,  1.0f , //front
				0.0f,  0.0f,  1.0f , //front
				0.0f,  0.0f,  1.0f , //front
				0.0f,  0.0f,  1.0f , //front

				1.0f,  0.0f,  0.0f , // right
				1.0f,  0.0f,  0.0f , // right
				1.0f,  0.0f,  0.0f , // right
				1.0f,  0.0f,  0.0f , // right

				0.0f, 0.0f,  -1.0f , //back
				0.0f, 0.0f,  -1.0f , //back
				0.0f, 0.0f,  -1.0f , //back
				0.0f, 0.0f,  -1.0f , //back

				-1.0f,  0.0f, 0.0f , //left
				-1.0f,  0.0f, 0.0f , //left
				-1.0f,  0.0f, 0.0f , //left
				-1.0f,  0.0f, 0.0f , //left


				0.0f,  1.0f,  0.0f , // top
				0.0f,  1.0f,  0.0f , // top
				0.0f,  1.0f,  0.0f , // top
				0.0f,  1.0f,  0.0f , // top

				0.0f, -1.0f,  0.0f , // bottom
				0.0f, -1.0f,  0.0f , // bottom
				0.0f, -1.0f,  0.0f , // bottom
				0.0f, -1.0f,  0.0f , // bottom

				};

	glGenVertexArrays(1, &vao_cube);
	glBindVertexArray(vao_cube);

	// now give vertex for square geometry
	glGenBuffers(1, &vbo_cube_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_cube_normals);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_normals);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glNormal_coord), glNormal_coord, GL_STATIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
    glBindVertexArray(0); // end for vao_pyramid
}

-(void)reshape
{
    //code
    [super reshape];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    NSRect rect = [self bounds];
    if(rect.size.height <= 0)
    {
        rect.size.height = 1;
    }

    glViewport(0, 0, (GLsizei)rect.size.width, (GLsizei)rect.size.height);
    
    GLuint width = rect.size.width;
    GLuint height = rect.size.height;

    // projection

    // glORtho(left, right, bottom, top, near, far);
    perspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); 


    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(void)drawRect:(NSRect)dirty_rect
{ 
    //code
    [self drawView];
}

-(void)drawView
{
    //code
    static GLfloat angle = 0.0f;

    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // start from here

    // START USING OpenGL program object
    glUseProgram(shaderProgramObject);

    // triangle
	mat4 translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
	mat4 rotateMatrix_x = vmath::rotate(angle, 1.0f, 0.0f, 0.0f);
	mat4 rotateMatrix_y = vmath::rotate(angle, 0.0f, 1.0f, 0.0f);
	mat4 rotateMatrix_z = vmath::rotate(angle, 0.0f, 0.0f, 1.0f);
	mat4 modelViewMatrix =  translateMatrix * rotateMatrix_x * rotateMatrix_y * rotateMatrix_z;
	mat4 projectionMatrix = mat4::identity();

	projectionMatrix = perspectiveProjectionMatrix; // ORDER IS IMPORTANT
	
	if (bLight == true)
	{
		glUniform1i(lKeyPressedUniform, 1);
		glUniform3f(ldUniform, 1.0f, 1.0f, 1.0f);
		glUniform3f(kdUniform, 0.5f, 0.5f, 0.5f);
		GLfloat light_possition[] = { 0.0f, 0.0f, 2.0f, 1.0f };
		glUniform4fv(lightPossitionUniform, 1, light_possition);
	}
	else
	{
		glUniform1i(lKeyPressedUniform, 0);
	}
	glUniformMatrix4fv(modelViewUniform, 1, GL_FALSE, modelViewMatrix);
	glUniformMatrix4fv(perspectiveProjectionUniform, 1, GL_FALSE, (const GLfloat*)projectionMatrix);

	glBindVertexArray(vao_cube);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
	glBindVertexArray(0);

	// stop using OpenGL program object
	glUseProgram(0);

    if(bAnimate)
    {
        if(angle > 360.0f)
        {
            angle = 0.0f;
        }
        angle = angle + 0.1f;
    }

    // update();

    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder
{
    //code
    [[self window]makeFirstResponder:self];
    return YES;
}

// WM_KEYDOWN
-(void)keyDown:(NSEvent *)theEvent
{
    //code
    int key = [[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        //ESC
        case 27:
            // self i.e. MyView
            [self release];
            // WM_DESTROY
            [NSApp terminate:self];
            break;
        case 'a':
        case 'A':
        	bAnimate = !bAnimate;
        	break;
        case 'l':
        case 'L':
        	bLight = !bLight;
        	break;
        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];
            break;
    }
}

// left mouse
-(void)mouseDown:(NSEvent *)theEvent
{
    //code
    
}

// right
-(void)rightMouseDown:(NSEvent *)theEvent
{
    //code
    
}

-(void)otherMouseDown:(NSEvent *)theEvent
{
    //code
    
}

-(void)dealloc
{
    //code

    // Unintialize();

    [self unintialize];

    [super dealloc];

    CVDisplayLinkStop(cvDisplayLinkRef);

    CVDisplayLinkRelease(cvDisplayLinkRef);
}

-(void)unintialize
{
    //code
    
    // destroy vao_pyramid
    if (vao_cube)
    {
        glDeleteVertexArrays(1, &vao_cube);
        vao_cube = 0;
    }

    // destroy vbo_pyramid_position
    if (vbo_cube_position)
    {
        glDeleteBuffers(1, &vbo_cube_position);
        vbo_cube_position = 0;
    }

    if (vbo_cube_normals)
	{
		glDeleteBuffers(1, &vbo_cube_normals);
		vbo_cube_normals = 0;
	}
    
    // deatch vertex shader from shader program object
    glDetachShader(shaderProgramObject, vertexShaderObject);

    // detach fragment shader from shader program obejct
    glDetachShader(shaderProgramObject, fragmentShaderObject);

    // delete vertex shader object
    glDeleteShader(vertexShaderObject);

    vertexShaderObject = 0;

    // delete fragment shader object
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;

    // delete shader program object
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;

    // unlink shader program
    glUseProgram(0);

}


@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *now, const CVTimeStamp *outputTime, CVOptionFlags in,
                                CVOptionFlags *out, void *displayLinkContext)
 {
    //code
    CVReturn result = [(MyOpenGLView *)displayLinkContext getFrameForTime:outputTime];

    return result;
 }
