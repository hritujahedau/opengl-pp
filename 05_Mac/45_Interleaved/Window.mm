//
//  window.m
//  
//
//  Created by user166446 on 7/8/21.
//

// stdio.h is inlcuded in Foundation.h 
#import <Foundation/Foundation.h> // similar to standard library
#import <Cocoa/Cocoa.h> // xlib.h windows.h
#import <QuartzCore/CVDisplayLink.h>     //core video
#import <OpenGL/GL3.h>      // gl.h

#import "vmath.h"

using namespace vmath;

// global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef , const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags , CVOptionFlags *, void *);

// global variable declarations
FILE *gpFile = NULL;

enum
{
    HRH_ATTRIBUTE_POSITION = 0 ,
    HRH_ATTRIBUTE_COLOR ,
    HRH_ATTRIBUTE_NORMAL ,
    HRH_ATTRIBUTE_TEXTURE_0
};

int isLightOn = 0, isColorOn = 0, isTextureOn = 0;

GLfloat lightPosition[] = { 100.0f, 100.0f, 100.0f, 1.0f };
GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f };
GLfloat lightDiffuse[] = { 1.0f, 1.0f, 1.0f };
GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f };

GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f };
GLfloat materialDiffuse[] = { 1.0f, 1.0f, 1.0f };
GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0f };
GLfloat materialShinyness = 128.0f;

GLuint texture = 0;

// NS - NextSTEP
// Delegation performance <>
@interface AppDelegate:NSObject <NSApplicationDelegate,NSWindowDelegate>
@end

int main(int argc, char *argv[])
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    
    NSApp = [NSApplication sharedApplication];
    
    [NSApp setDelegate:[[AppDelegate alloc]init]];
    
    [NSApp run]; // run loop/ message loop/ event loop
    
    [pool release];
    
    return 0;
}

@interface MyOpenGLView:NSOpenGLView

@end

@implementation AppDelegate
{
    @private
    NSWindow *window ;
    MyOpenGLView *myOpenGLview;
}

// WM_CREATE
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // code
    // internally CGrect, core graphics , c library
    // first 2 variable together NSPoint , CGPoint
    // last 2 variable together NSSize structure , CGSzie

    NSBundle *appBundle = [NSBundle mainBundle];
    NSString *appDirPath = [appBundle bundlePath]; // /User/Hrituja/Desktop/RTR3.0/02_LogFile/window.app
    NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent]; // remove window.app give /User/Hrituja/Desktop/RTR3.0/02_LogFile
    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/log.txt", parentDirPath];
    const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    
    gpFile = fopen(pszLogFileNameWithPath, "w");
    if(gpFile == NULL)
    {
        [self release];
        [NSApp terminate:self];
        // end application 
    }
    
    fprintf(gpFile, "\nFile open successfully\n");

    NSRect win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);
    window = [[NSWindow alloc]initWithContentRect:win_rect
                                        styleMask:NSWindowStyleMaskTitled |
                                                NSWindowStyleMaskClosable |
                                                NSWindowStyleMaskMiniaturizable |
                                                NSWindowStyleMaskResizable
                                          backing:NSBackingStoreBuffered
                                           defer:NO
              ];
    [window setTitle:@"HRH: OGL"];
    [window center];
    
    myOpenGLview = [[MyOpenGLView alloc]initWithFrame:win_rect];
    
    [window setContentView:myOpenGLview];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

// WM_DESTTROY
-(void)applicationWillTerminate:(NSNotification *)aNotification
{
    //code

    if(gpFile)
    {
        fprintf(gpFile , "\nProgram terminated successfully\n");
        fclose(gpFile);
        gpFile = NULL;
    }
}

-(void)windowWillClose:(NSNotification *)aNotification
{
    //code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [myOpenGLview release];
    [window release];
    [super dealloc];

}
@end

@implementation MyOpenGLView
{
    @private
    CVDisplayLinkRef cvDisplayLinkRef;

    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;

    GLuint vao_cube;
    GLuint vbo_cube_position;
    GLuint vbo_cube_texcoord;
    GLuint vbo_cube_color;
    GLuint vbo_cube_normals;
    
    GLuint modelUniform;
    GLuint viewUniform;
    GLuint perspectiveUniform;
    
    GLuint textureSamplerUniform;
    
    GLuint lightAmbientUniform;
    GLuint lightSpecularUniform;
    GLuint lightDiffuseUniform;
    GLuint lightPositionUniform;
    
    GLuint materialSpecularUniform;
    GLuint materialAmbientUniform;
    GLuint materialDiffuseUniform;
    GLuint materialShininessUniform;
    
    GLuint isLightOnUniform;
    GLuint isColorOnUniform;
    GLuint isTextureOnUniform;

    mat4 perspectiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame
{
    //code
    self = [super initWithFrame:frame];

    if(self)
    {
        // pixel format descripor
        NSOpenGLPixelFormatAttribute attributes[] =
                                                    {
                                                        NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
                                                        NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
                                                        NSOpenGLPFANoRecovery,
                                                        NSOpenGLPFAAccelerated,
                                                        NSOpenGLPFAColorSize, 24,
                                                        NSOpenGLPFADepthSize, 24,
                                                        NSOpenGLPFAAlphaSize, 8,
                                                        NSOpenGLPFADoubleBuffer,
                                                        0
                                                    };
        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attributes]autorelease];
    
        if(pixelFormat == nil)
        {
            fprintf(gpFile, "\nPixel Format failed");
            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease]; // get pixell format

        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext];
    }



    return self;
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)output_time
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

    [self drawView];

    [pool release];

    return kCVReturnSuccess;
}

-(void)prepareOpenGL
{
    //code
    [super prepareOpenGL];

    [[self openGLContext]makeCurrentContext];

    // swap interval
    GLint swapInt = 1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];  // cp - context parameter

    
    // write shaders here
    if( [self loadShaders] == NO)
    {
       [self unintialize];
    }
    
    [self loadObjects];

    texture = [self loadTextureFromBmpFile:"marble.bmp"];

    glClearColor(0.0f, 0.0f, 0.0f, 1.0);
    //glShadeModel(GL_SMOOTH);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    //glEnable(GL_CULL_FACE);

    perspectiveProjectionMatrix = mat4::identity();

    // core video and core graphics related code

    // 1. Create display link
    // 2. Set callback for display link
    // 3. NSContext -> CGL
    // 4. NSPixelFormat -> CGL
    // 5. Set current CGL context and CGL pixel format 
 

    CVDisplayLinkCreateWithActiveCGDisplays(&cvDisplayLinkRef);
    CVDisplayLinkSetOutputCallback(cvDisplayLinkRef, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(cvDisplayLinkRef, cglContext, cglPixelFormat);
    CVDisplayLinkStart(cvDisplayLinkRef);

}

-(BOOL)loadShaders
{
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    //provide souce code to shader
    const GLchar* vertexShaderSourceCode =
                                            "#version 410 core\n"\
                                            " in vec4 vPosition;                                                                        " \
                                            " in vec3 vNormals;                                                                         " \
                                            " in vec2 vTexCoord;                                                                        " \
                                            " in vec3 vColor;                                                                           " \
                                            " out vec3 outColor;                                                                        " \
                                            " out vec2 outTexCoord;                                                                     " \
                                            " uniform mat4 u_projectionMatrix;                                                          " \
                                            " uniform mat4 u_modelMatrix;                                                               " \
                                            " uniform mat4 u_viewMatrix;                                                                " \
                                            " uniform int u_isLightOn;                                                                  " \
                                            " uniform vec4 u_light_position;                                                            " \
                                            " out vec3 light_direction;                                                                 " \
                                            " out vec3 transformed_normal;                                                              " \
                                            " out vec3 view_vector;                                                                     " \
                                            " void main()                                                                               " \
                                            " {                                                                                         " \
                                            "   if(u_isLightOn == 1)                                                                    " \
                                            "   {                                                                                       " \
                                            "       vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * vPosition;                    " \
                                            "       transformed_normal = mat3(u_viewMatrix * u_modelMatrix) * vNormals ;                " \
                                            "       light_direction = vec3(u_light_position - eye_coordinates );                        " \
                                            "       view_vector = vec3 (-eye_coordinates);                                              " \
                                            "   }                                                                                       " \
                                            "   outTexCoord = vTexCoord;                                                                " \
                                            "   gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;            " \
                                            "   outColor = vColor;                                                                      " \
                                            " }                                                                                         " ;

    glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

    // compile shader
    glCompileShader(vertexShaderObject);

    // check compilation error
    GLint iInfoLogLength = 0;
    GLint iShaderCompileStatus = 0;
    char* szInfoLog = NULL;
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("Vertex shader compilation error : %s \n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    /***************** FRAGMENT SHADER **********************/

    // CREATE SHADER
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    //PROVIDE SOURCE CODE

    const char* fragmentShaderSourceCode =
                                            "#version 410 core\n"\
" vec3 phoung_ads_lighting;                                                                                                                                 " \
        " in vec3 light_direction;                                                                                                                                  " \
        " in vec3 transformed_normal;                                                                                                                               " \
        " in vec3 view_vector;                                                                                                                                      " \
        " vec3 normalized_light_direction;                                                                                                                          " \
        " vec3 normalized_transformed_normal;                                                                                                                       " \
        " vec3 normalized_view_vector;                                                                                                                              " \
        " uniform vec3 u_lightAmbient;                                                                                                                              " \
        " uniform vec3 u_lightSpecular;                                                                                                                             " \
        " uniform vec3 u_lightDiffuse;                                                                                                                              " \
        " uniform vec3 u_materialAmbient;                                                                                                                           " \
        " uniform vec3 u_materialSpecular;                                                                                                                          " \
        " uniform vec3 u_materialDiffuse;                                                                                                                           " \
        " uniform float u_materialShininess;                                                                                                                        " \
        " uniform int u_isLightOn;                                                                                                                                  " \
        " uniform int u_isColor;                                                                                                                                    " \
        " uniform int u_isTexture;                                                                                                                                  " \
        " in vec2 outTexCoord;                                                                                                                                      " \
        " out vec4 FragColor;                                                                                                                                       " \
        " in vec3 outColor;                                                                                                                                         " \
        " uniform sampler2D u_texture_sampler;                                                                                                                      " \
        " void main()                                                                                                                                               " \
        " {                                                                                                                                                         " \
        "   phoung_ads_lighting = vec3(1.0, 1.0, 1.0);                                                                                                              " \
        "   if(u_isLightOn == 1)                                                                                                                                    " \
        "   {                                                                                                                                                       " \
        "       normalized_light_direction = normalize(light_direction);                                                                                            " \
        "       normalized_transformed_normal = normalize(transformed_normal);                                                                                      " \
        "       normalized_view_vector = normalize(view_vector);                                                                                                    " \
        "       vec3 reflection_vector = reflect (-normalized_light_direction, normalized_transformed_normal);                                                      " \
        "       vec3 ambient = u_lightAmbient * u_materialAmbient;                                                                                                  " \
        "       vec3 diffuse = u_lightDiffuse * u_materialDiffuse * max( dot ( normalized_light_direction, normalized_transformed_normal), 0.0);                    " \
        "       vec3 specular = u_lightSpecular * u_materialSpecular * pow( max ( dot (reflection_vector , normalized_view_vector), 0.0) , u_materialShininess);    " \
        "       phoung_ads_lighting = ambient + diffuse + specular;                                                                                                 " \
        "   }                                                                                                                                                       " \
        "   FragColor = vec4(1.0, 1.0, 1.0, 1.0);                                                                                                                   " \
        "   if(u_isColor == 1)                                                                                                                                      " \
        "   {                                                                                                                                                       " \
        "       FragColor = vec4(outColor, 1.0);                                                                                                                    " \
        "   }                                                                                                                                                       " \
        "   if(u_isTexture == 1)                                                                                                                                    " \
        "   {                                                                                                                                                       " \
        "       FragColor = FragColor * texture(u_texture_sampler, outTexCoord);                                                                                    " \
        "   }                                                                                                                                                       " \
        "   if(u_isLightOn == 1)                                                                                                                                    " \
        "   {                                                                                                                                                       " \
        "       FragColor = FragColor * vec4(phoung_ads_lighting, 1.0);                                                                                             " \
        "   }                                                                                                                                                       " \
        " }                                                                                                                                                         " ;
    

    glShaderSource(fragmentShaderObject, 1, (const char**)&fragmentShaderSourceCode, NULL);

    // COMPILE SHADER
    glCompileShader(fragmentShaderObject);

    // compilation error checking

    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("Fragment Shader Compilation Error : %s\n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    // SHADER PROGRAM
    //CREATE SHADER PROGRAM

    shaderProgramObject = glCreateProgram();

    // attach vertex shader to shader program
    glAttachShader(shaderProgramObject, vertexShaderObject);

    // attch fragment shader to shader program
    glAttachShader(shaderProgramObject, fragmentShaderObject);

    // pri-link binding of shader object with vertex shader possition attribute
    glBindAttribLocation(shaderProgramObject, HRH_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(shaderProgramObject, HRH_ATTRIBUTE_TEXTURE_0, "vTexCoord");
    glBindAttribLocation(shaderProgramObject, HRH_ATTRIBUTE_COLOR, "vColor");
    glBindAttribLocation(shaderProgramObject, HRH_ATTRIBUTE_NORMAL, "vNormals");


    // LINK PROGRAM
    glLinkProgram(shaderProgramObject);

    // CHECK LINKING ERROR
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                printf("Shader Program Link Log : %s", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }
    
    // get MVP uniform location
    modelUniform = glGetUniformLocation(shaderProgramObject, "u_modelMatrix");
    viewUniform = glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
    perspectiveUniform = glGetUniformLocation(shaderProgramObject, "u_projectionMatrix");

    textureSamplerUniform = glGetUniformLocation(shaderProgramObject, "u_texture_sampler");
    isLightOnUniform = glGetUniformLocation(shaderProgramObject, "u_isLightOn");
    isColorOnUniform = glGetUniformLocation(shaderProgramObject, "u_isColor");
    isTextureOnUniform = glGetUniformLocation(shaderProgramObject, "u_isTexture");

    lightAmbientUniform = glGetUniformLocation(shaderProgramObject, "u_lightAmbient");
    lightSpecularUniform = glGetUniformLocation(shaderProgramObject, "u_lightSpecular");
    lightDiffuseUniform = glGetUniformLocation(shaderProgramObject, "u_lightDiffuse");
    lightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_position");

    materialDiffuseUniform = glGetUniformLocation(shaderProgramObject, "u_materialDiffuse");
    materialAmbientUniform = glGetUniformLocation(shaderProgramObject, "u_materialAmbient");
    materialSpecularUniform = glGetUniformLocation(shaderProgramObject, "u_materialSpecular");
    materialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_materialShininess");
    
    return YES;
}

-(void)loadObjects
{
    // ***** vertex, colors, shadersatribs , vbo_pyramid_position, vao_pyramid initialization ********/
    
    // square
    const GLfloat cubeVertices[] =
    {
         // FRONT
        1.0f,1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,

        // RIGHT
        1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f,-1.0f,

        //BACK
        1.0f, -1.0f,-1.0f,
        -1.0f, -1.0f,-1.0f,
        -1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, -1.0f,
        
        // LEFT
        -1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f,

        // TOP
        1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,

        // BOTTOM
        1.0f, -1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,
        -1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,
        
    };

    const GLfloat cubeTexCoordMatrix[] =
    {
        // 1
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f,1.0f,
        0.0f,1.0f,

        //2
        1.0f, 0.0f,
        1.0f,1.0f,
        0.0f,1.0f,
        0.0f, 0.0f,

        //3
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,
        0.0f, 0.0f,

        //4
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,

        //5
        0.0f,1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,

        //6
        1.0f, 1.0f,
        0.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f,

    };

    const GLfloat cubeColorMatrix[] =
    {
        // 1
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        //2
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,

        //3
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,

        //4
        1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,

        //5
        0.0f, 1.0f, 1.0f,
        0.0f, 1.0f, 1.0f,
        0.0f, 1.0f, 1.0f,
        0.0f, 1.0f, 1.0f,

        //6
        1.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 1.0f,

    };

    GLfloat glNormal_coord[] =
    {

        0.0f,  0.0f,  1.0f , //front
        0.0f,  0.0f,  1.0f , //front
        0.0f,  0.0f,  1.0f , //front
        0.0f,  0.0f,  1.0f , //front

        1.0f,  0.0f,  0.0f , // right
        1.0f,  0.0f,  0.0f , // right
        1.0f,  0.0f,  0.0f , // right
        1.0f,  0.0f,  0.0f , // right

        0.0f, 0.0f,  -1.0f , //back
        0.0f, 0.0f,  -1.0f , //back
        0.0f, 0.0f,  -1.0f , //back
        0.0f, 0.0f,  -1.0f , //back

        -1.0f,  0.0f, 0.0f , //left
        -1.0f,  0.0f, 0.0f , //left
        -1.0f,  0.0f, 0.0f , //left
        -1.0f,  0.0f, 0.0f , //left

        0.0f,  1.0f,  0.0f , // top
        0.0f,  1.0f,  0.0f , // top
        0.0f,  1.0f,  0.0f , // top
        0.0f,  1.0f,  0.0f , // top

        0.0f, -1.0f,  0.0f , // bottom
        0.0f, -1.0f,  0.0f , // bottom
        0.0f, -1.0f,  0.0f , // bottom
        0.0f, -1.0f,  0.0f , // bottom

    };


    glGenVertexArrays(1, &vao_cube);
    glBindVertexArray(vao_cube);

    // now give vertex for square geometry
    glGenBuffers(1, &vbo_cube_position);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &vbo_cube_texcoord);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_texcoord);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeTexCoordMatrix), cubeTexCoordMatrix, GL_STATIC_DRAW);
    glVertexAttribPointer(HRH_ATTRIBUTE_TEXTURE_0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(HRH_ATTRIBUTE_TEXTURE_0); 
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &vbo_cube_color);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeColorMatrix), cubeColorMatrix, GL_STATIC_DRAW);
    glVertexAttribPointer(HRH_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(HRH_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &vbo_cube_normals);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_normals);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glNormal_coord), glNormal_coord, GL_STATIC_DRAW);
    glVertexAttribPointer(HRH_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(HRH_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0); // end for vao_pyramid
}

-(GLuint)loadTextureFromBmpFile:(const char *)imageFileName
{
    // code
    NSBundle *appBundle = [NSBundle mainBundle];
    NSString *appDirPath = [appBundle bundlePath]; // /User/Hrituja/Desktop/RTR3.0/02_LogFile/window.app
    NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent]; // remove window.app give /User/Hrituja/Desktop/RTR3.0/02_LogFile
    NSString *nsStringPath = [NSString stringWithFormat:@"%@/%s", parentDirPath, imageFileName];

    // 1. Get NS image representation of our image file
    NSImage *bmpIamge = [[NSImage alloc]initWithContentsOfFile:nsStringPath];

    if(!bmpIamge)
    {
        fprintf(gpFile, "\nNS image conversion fail");
        return 0;
    }
    
    // 2. Get CG image representation of NS img
    CGImageRef cgImage = [bmpIamge CGImageForProposedRect:nil context:nil hints:nil];

    // 1 - dfault rect default x, y, w, h
    // context - Core Graphics context
    // hint- like inverted, right rotate

    // 3. Get width and height of CG image
    int width = (int)CGImageGetWidth(cgImage);
    int height = (int)CGImageGetHeight(cgImage);

    // 4. Get Core foundation data representation of image
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));

    // 5. Convert CFDataRef format to void * generic format 
    void *pixel = (void *)CFDataGetBytePtr(imageData);

    GLuint tex = 0;

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1); //IN FFP 4
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);

    //setting texture parameter
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    // following call will actually push the data with help of graphics driver
    //gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixel);    
    glGenerateMipmap(GL_TEXTURE_2D);
    
    glBindTexture(GL_TEXTURE_2D, 0);

    CFRelease(imageData);

    return tex;
}


-(void)reshape
{
    //code
    [super reshape];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    NSRect rect = [self bounds];
    if(rect.size.height <= 0)
    {
        rect.size.height = 1;
    }

    glViewport(0, 0, (GLsizei)rect.size.width, (GLsizei)rect.size.height);
    
    GLuint width = rect.size.width;
    GLuint height = rect.size.height;

    // projection

    // glORtho(left, right, bottom, top, near, far);
    perspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); 


    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(void)drawRect:(NSRect)dirty_rect
{ 
    //code
    [self drawView];
}

-(void)drawView
{
    //code
    static GLfloat angle = 0.0f;

    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // start from here

    // START USING OpenGL program object
    glUseProgram(shaderProgramObject);

    //OpenGL Drawing For Trianlge
    // set modelview & modelviewProjection matrices to indentity
    mat4 translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);

    mat4 scaleMatrix = mat4::identity();
    mat4 rotateMatrix = vmath::rotate(angle, 1.0f, 0.0f, 0.0f) *
                        vmath::rotate(angle, 0.0f, 1.0f, 0.0f) *
                        vmath::rotate(angle, 0.0f, 0.0f, 1.0f) ;

    mat4 viewMatrix = mat4::identity();
    mat4 modelMatrix = translateMatrix * rotateMatrix ;

    if (isLightOn == 1)
    {
        glUniform4fv(lightPositionUniform, 1, (const GLfloat*)lightPosition);
        glUniform3fv(lightDiffuseUniform, 1, (const GLfloat*)lightDiffuse);
        glUniform3fv(lightAmbientUniform, 1, (const GLfloat*)lightAmbient);
        glUniform3fv(lightSpecularUniform, 1, (const GLfloat*)lightSpecular);

        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*)materialAmbient);
        glUniform1f (materialShininessUniform, materialShinyness);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*)materialSpecular);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*)materialDiffuse);
    }

    glUniform1i(isLightOnUniform, isLightOn);
    glUniform1i(isColorOnUniform, isColorOn);
    glUniform1i(isTextureOnUniform, isTextureOn);

    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);
    glUniformMatrix4fv(viewUniform, 1, GL_FALSE, (const float*)viewMatrix);
    glUniformMatrix4fv(perspectiveUniform, 1, GL_FALSE, (const GLfloat*)perspectiveProjectionMatrix);
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);
    glUniform1i(textureSamplerUniform, 0);

    glBindVertexArray(vao_cube);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
    glBindVertexArray(0);

    // stop using OpenGL program object
    glUseProgram(0);

    if(angle > 360.0f)
    {
        angle = 0.0f;
    }
    angle = angle + 0.1f;
    
    // update();

    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder
{
    //code
    [[self window]makeFirstResponder:self];
    return YES;
}

// WM_KEYDOWN
-(void)keyDown:(NSEvent *)theEvent
{
    //code
    int key = [[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        //ESC
        case 27:
            // self i.e. MyView
            [self release];
            // WM_DESTROY
            [NSApp terminate:self];
            break;
        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];
            break;
        case 'l':
        case 'L':
            isLightOn = !isLightOn;
            break;
        case 'c':
        case 'C':
            isColorOn = !isColorOn;
            break;
        case 't':
        case 'T':
            isTextureOn = !isTextureOn;
            break;
    }
}

// left mouse
-(void)mouseDown:(NSEvent *)theEvent
{
    //code
    
}

// right
-(void)rightMouseDown:(NSEvent *)theEvent
{
    //code
    
}

-(void)otherMouseDown:(NSEvent *)theEvent
{
    //code
    
}

-(void)dealloc
{
    //code

    // Unintialize();

    [self unintialize];

    [super dealloc];

    CVDisplayLinkStop(cvDisplayLinkRef);

    CVDisplayLinkRelease(cvDisplayLinkRef);
}

-(void)unintialize
{
    //code
    
    // destroy vao_pyramid
    if (vao_cube)
    {
        glDeleteVertexArrays(1, &vao_cube);
        vao_cube = 0;
    }

    // destroy vbo_pyramid_position
    if (vbo_cube_position)
    {
        glDeleteBuffers(1, &vbo_cube_position);
        vbo_cube_position = 0;
    }
    
    if (vbo_cube_texcoord)
    {
        glDeleteBuffers(1, &vbo_cube_texcoord);
        vbo_cube_texcoord = 0;
    }

    if (vbo_cube_color)
    {
        glDeleteBuffers(1, &vbo_cube_color);
        vbo_cube_color = 0;
    }

    // deatch vertex shader from shader program object
    glDetachShader(shaderProgramObject, vertexShaderObject);

    // detach fragment shader from shader program obejct
    glDetachShader(shaderProgramObject, fragmentShaderObject);

    // delete vertex shader object
    glDeleteShader(vertexShaderObject);

    vertexShaderObject = 0;

    // delete fragment shader object
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;

    // delete shader program object
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;

    // unlink shader program
    glUseProgram(0);

}


@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *now, const CVTimeStamp *outputTime, CVOptionFlags in,
                                CVOptionFlags *out, void *displayLinkContext)
 {
    //code
    CVReturn result = [(MyOpenGLView *)displayLinkContext getFrameForTime:outputTime];

    return result;
 }
