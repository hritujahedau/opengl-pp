//
//  window.m
//  
//
//  Created by user166446 on 7/8/21.
//

// stdio.h is inlcuded in Foundation.h 
#import <Foundation/Foundation.h> // similar to standard library
#import <Cocoa/Cocoa.h> // xlib.h windows.h
#import <QuartzCore/CVDisplayLink.h>     //core video
#import <OpenGL/GL3.h>      // gl.h

#import "vmath.h"

using namespace vmath;

// global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef , const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags , CVOptionFlags *, void *);

// global variable declarations
FILE *gpFile = NULL;

enum
{
    HRH_ATTRIBUTE_POSITION = 0 ,
    HRH_ATTRIBUTE_COLOR ,
    HRH_ATTRIBUTE_NORMAL ,
    HRH_ATTRIBUTE_TEXTURE_0
};

int isLightOn = 0;

GLfloat lightPosition_0[] = { 100.0f, 100.0f, 100.0f, 1.0f };
GLfloat lightAmbient_0[] = { 1.0f, 0.0f, 0.0f};
GLfloat lightDiffuse_0[] = { 1.0f, 0.0f, 0.0f};
GLfloat lightSpecular_0[] = { 0.0f, 0.0f, 1.0f};

GLfloat lightPosition_1[] = { -100.0f, 100.0f, 100.0f, 1.0f };
GLfloat lightAmbient_1[] = { 0.0f, 0.0f, 1.0f};
GLfloat lightDiffuse_1[] = { 0.0f, 0.0f, 1.0f};
GLfloat lightSpecular_1[] = { 0.0f, 0.0f, 1.0 };

GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f};
GLfloat materialDiffuse[] = { 1.0f, 1.0f, 1.0f};
GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0 };
GLfloat materialShininess = 128.0f;

bool bAnimate = false;


// NS - NextSTEP
// Delegation performance <>
@interface AppDelegate:NSObject <NSApplicationDelegate,NSWindowDelegate>
@end

int main(int argc, char *argv[])
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    
    NSApp = [NSApplication sharedApplication];
    
    [NSApp setDelegate:[[AppDelegate alloc]init]];
    
    [NSApp run]; // run loop/ message loop/ event loop
    
    [pool release];
    
    return 0;
}

@interface MyOpenGLView:NSOpenGLView

@end

@implementation AppDelegate
{
    @private
    NSWindow *window ;
    MyOpenGLView *myOpenGLview;
}

// WM_CREATE
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // code
    // internally CGrect, core graphics , c library
    // first 2 variable together NSPoint , CGPoint
    // last 2 variable together NSSize structure , CGSzie

    NSBundle *appBundle = [NSBundle mainBundle];
    NSString *appDirPath = [appBundle bundlePath]; // /User/Hrituja/Desktop/RTR3.0/02_LogFile/window.app
    NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent]; // remove window.app give /User/Hrituja/Desktop/RTR3.0/02_LogFile
    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/log.txt", parentDirPath];
    const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    
    gpFile = fopen(pszLogFileNameWithPath, "w");
    if(gpFile == NULL)
    {
        [self release];
        [NSApp terminate:self];
        // end application 
    }
    
    fprintf(gpFile, "\nFile open successfully\n");

    NSRect win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);
    window = [[NSWindow alloc]initWithContentRect:win_rect
                                        styleMask:NSWindowStyleMaskTitled |
                                                NSWindowStyleMaskClosable |
                                                NSWindowStyleMaskMiniaturizable |
                                                NSWindowStyleMaskResizable
                                          backing:NSBackingStoreBuffered
                                           defer:NO
              ];
    [window setTitle:@"HRH: OGL"];
    [window center];
    
    myOpenGLview = [[MyOpenGLView alloc]initWithFrame:win_rect];
    
    [window setContentView:myOpenGLview];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

// WM_DESTTROY
-(void)applicationWillTerminate:(NSNotification *)aNotification
{
    //code

    if(gpFile)
    {
        fprintf(gpFile , "\nProgram terminated successfully\n");
        fclose(gpFile);
        gpFile = NULL;
    }
}

-(void)windowWillClose:(NSNotification *)aNotification
{
    //code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [myOpenGLview release];
    [window release];
    [super dealloc];

}
@end

@implementation MyOpenGLView
{
    @private
    CVDisplayLinkRef cvDisplayLinkRef;

    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;

    GLuint vao_pyramid;
    GLuint vbo_pyramid_position;
    GLuint vbo_pyramid_normals;

    GLuint modelUniform;
    GLuint viewUniform;
    GLuint perspectiveUniform;
    
    GLuint light_0_AmbientUniform;
    GLuint light_0_SpecularUniform;
    GLuint light_0_DiffusedUniform;
    GLuint light_0_PossitionUniform;
    
    GLuint light_1_AmbientUniform;
    GLuint light_1_SpecularUniform;
    GLuint light_1_DiffusedUniform;
    GLuint light_1_PossitionUniform;
    
    GLuint materialSpecularUniform;
    GLuint materialAmbientUniform;
    GLuint materialDiffuseUniform;
    GLuint materialShininessUniform;
    
    GLuint isLightOnUniform;    

    mat4 perspectiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame
{
    //code
    self = [super initWithFrame:frame];

    if(self)
    {
        // pixel format descripor
        NSOpenGLPixelFormatAttribute attributes[] =
                                                    {
                                                        NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
                                                        NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
                                                        NSOpenGLPFANoRecovery,
                                                        NSOpenGLPFAAccelerated,
                                                        NSOpenGLPFAColorSize, 24,
                                                        NSOpenGLPFADepthSize, 24,
                                                        NSOpenGLPFAAlphaSize, 8,
                                                        NSOpenGLPFADoubleBuffer,
                                                        0
                                                    };
        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attributes]autorelease];
    
        if(pixelFormat == nil)
        {
            fprintf(gpFile, "\nPixel Format failed");
            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease]; // get pixell format

        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext];
    }



    return self;
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)output_time
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

    [self drawView];

    [pool release];

    return kCVReturnSuccess;
}

-(void)prepareOpenGL
{
    //code
    [super prepareOpenGL];

    [[self openGLContext]makeCurrentContext];

    // swap interval
    GLint swapInt = 1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];  // cp - context parameter

    
    // write shaders here
    if( [self loadShaders] == NO)
    {
       [self unintialize];
    }
    
    [self loadObjects];

    glClearColor(0.0f, 0.0f, 0.0f, 1.0);
    //glShadeModel(GL_SMOOTH);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    //glEnable(GL_CULL_FACE);

    perspectiveProjectionMatrix = mat4::identity();

    // core video and core graphics related code

    // 1. Create display link
    // 2. Set callback for display link
    // 3. NSContext -> CGL
    // 4. NSPixelFormat -> CGL
    // 5. Set current CGL context and CGL pixel format 
 

    CVDisplayLinkCreateWithActiveCGDisplays(&cvDisplayLinkRef);
    CVDisplayLinkSetOutputCallback(cvDisplayLinkRef, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(cvDisplayLinkRef, cglContext, cglPixelFormat);
    CVDisplayLinkStart(cvDisplayLinkRef);

}

-(BOOL)loadShaders
{
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    //provide souce code to shader
    const GLchar* vertexShaderSourceCode =
                                            " #version 410 core\n"\
                                            " in vec4 v_position;" \
                                            " in vec3 v_normals; " \
                                            " uniform mat4 u_modelMatrix;" \
                                            " uniform mat4 u_viewMatrix;" \
                                            " uniform mat4 u_projectionMatrix;" \
                                            " uniform int u_isLightOn;" \
                                            " uniform vec4 u_light_0_position; " \
                                            " uniform vec4 u_light_1_position; " \
                                            " out vec3 light_direction_0; "\
                                            " out vec3 light_direction_1; "\
                                            " out vec3 transformed_normal;"\
                                            " out vec3 view_vector;"\
                                            " void main()" \
                                            " {" \
                                            "       if (u_isLightOn == 1) " \
                                            "       {" \
                                            "       vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" \
                                            "       transformed_normal =  mat3( u_viewMatrix * u_modelMatrix ) * v_normals;" \
                                            "       light_direction_0 = vec3 ( u_light_0_position  - eye_coordinates);" \
                                            "       light_direction_1 = vec3 ( u_light_1_position  - eye_coordinates);" \
                                            "       view_vector =  vec3 ( -eye_coordinates);" \
                                            "       }" \
                                            "       gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" \
                                            " }";

    glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

    // compile shader
    glCompileShader(vertexShaderObject);

    // check compilation error
    GLint iInfoLogLength = 0;
    GLint iShaderCompileStatus = 0;
    char* szInfoLog = NULL;
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("Vertex shader compilation error : %s \n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    /***************** FRAGMENT SHADER **********************/

    // CREATE SHADER
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    //PROVIDE SOURCE CODE

    const char* fragmentShaderSourceCode =
                                            " #version 410 core\n"\
                                            " vec3 phoung_ads_lighting;" \
                                            " out vec4 FragColor; " \
                                            " in vec3 light_direction_0; "\
                                            " in vec3 light_direction_1; "\
                                            " in vec3 transformed_normal;"\
                                            " in vec3 view_vector;"\
                                            " vec3 normalized_light_direction_0; "\
                                            " vec3 normalized_light_direction_1; "\
                                            " vec3 normalized_transformed_normal;"\
                                            " vec3 normalized_view_vector;"\
                                            " uniform vec3 u_light_0_Ambient;" \
                                            " uniform vec3 u_light_0_Specular;" \
                                            " uniform vec3 u_light_0_Diffuse;" \
                                            " uniform vec3 u_light_1_Ambient;" \
                                            " uniform vec3 u_light_1_Specular;" \
                                            " uniform vec3 u_light_1_Diffuse;" \
                                            " uniform vec3 u_materialAmbient; " \
                                            " uniform vec3 u_materialSpecular; "\
                                            " uniform vec3 u_materialDiffuse; "\
                                            " uniform float u_materialshininess; "\
                                            " uniform int u_isLightOn;" \
                                            " void main()" \
                                            " {"\
                                            "      if (u_isLightOn == 1) " \
                                            "      {" \
                                            "           normalized_light_direction_0 = normalize(light_direction_0);"\
                                            "           normalized_light_direction_1 = normalize(light_direction_1);"\
                                            "           normalized_transformed_normal = normalize(transformed_normal);"\
                                            "           normalized_view_vector = normalize(view_vector);"\
                                            "           vec3 reflection_vector = reflect (-normalized_light_direction_0, normalized_transformed_normal); " \
                                            "           vec3 ambient = u_light_0_Ambient  * u_materialAmbient; " \
                                            "           vec3 diffuse_light = u_light_0_Diffuse * u_materialDiffuse * max ( dot (normalized_light_direction_0 , normalized_transformed_normal), 0.0); "\
                                            "           vec3 specular = u_light_0_Specular  * u_materialSpecular * pow( max ( dot ( reflection_vector , normalized_view_vector), 0.0) , u_materialshininess);" \
                                            "           phoung_ads_lighting = ambient + diffuse_light + specular;" \
                                            "           reflection_vector = reflect (-normalized_light_direction_1, normalized_transformed_normal); " \
                                            "           ambient = u_light_1_Ambient  * u_materialAmbient; " \
                                            "           diffuse_light = u_light_1_Diffuse * u_materialDiffuse * max ( dot (normalized_light_direction_1 , normalized_transformed_normal), 0.0); "\
                                            "           specular = u_light_1_Specular  * u_materialSpecular * pow( max ( dot ( reflection_vector , normalized_view_vector), 0.0) , u_materialshininess);" \
                                            "           phoung_ads_lighting = phoung_ads_lighting + ambient + diffuse_light + specular;" \
                                            "      }"\
                                            "      else"\
                                            "      {"\
                                                      "phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" \
                                            "      }"\
                                            "      FragColor = vec4(phoung_ads_lighting, 1.0); " \
                                            " }";

    

    glShaderSource(fragmentShaderObject, 1, (const char**)&fragmentShaderSourceCode, NULL);

    // COMPILE SHADER
    glCompileShader(fragmentShaderObject);

    // compilation error checking

    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("Fragment Shader Compilation Error : %s\n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    // SHADER PROGRAM
    //CREATE SHADER PROGRAM

    shaderProgramObject = glCreateProgram();

    // attach vertex shader to shader program
    glAttachShader(shaderProgramObject, vertexShaderObject);

    // attch fragment shader to shader program
    glAttachShader(shaderProgramObject, fragmentShaderObject);

    // pri-link binding of shader object with vertex shader possition attribute
    glBindAttribLocation(shaderProgramObject, HRH_ATTRIBUTE_POSITION, "v_position");
    glBindAttribLocation(shaderProgramObject, HRH_ATTRIBUTE_NORMAL, "v_normals");

    // LINK PROGRAM
    glLinkProgram(shaderProgramObject);

    // CHECK LINKING ERROR
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                printf("Shader Program Link Log : %s", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }
    
    // get MVP uniform location
    modelUniform = glGetUniformLocation(shaderProgramObject, "u_modelMatrix");
    viewUniform = glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
    perspectiveUniform = glGetUniformLocation(shaderProgramObject, "u_projectionMatrix");
    isLightOnUniform = glGetUniformLocation(shaderProgramObject, "u_isLightOn");
    
    light_0_AmbientUniform = glGetUniformLocation  (shaderProgramObject, "u_light_0_Ambient");
    light_0_SpecularUniform = glGetUniformLocation (shaderProgramObject, "u_light_0_Specular");
    light_0_DiffusedUniform = glGetUniformLocation (shaderProgramObject, "u_light_0_Diffuse");
    light_0_PossitionUniform = glGetUniformLocation(shaderProgramObject, "u_light_0_position");

    light_1_AmbientUniform = glGetUniformLocation  (shaderProgramObject, "u_light_1_Ambient");
    light_1_SpecularUniform = glGetUniformLocation (shaderProgramObject, "u_light_1_Specular");
    light_1_DiffusedUniform = glGetUniformLocation (shaderProgramObject, "u_light_1_Diffuse");
    light_1_PossitionUniform = glGetUniformLocation(shaderProgramObject, "u_light_1_position");

    materialDiffuseUniform = glGetUniformLocation  (shaderProgramObject, "u_materialDiffuse");
    materialAmbientUniform = glGetUniformLocation  (shaderProgramObject, "u_materialAmbient");
    materialSpecularUniform = glGetUniformLocation (shaderProgramObject, "u_materialSpecular");
    materialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_materialshininess");

    
    return YES;
}

-(void)loadObjects
{
    // ***** vertex, colors, shadersatribs , vbo_pyramid_position, vao_pyramid initialization ********/
    glGenVertexArrays(1, &vao_pyramid);
    glBindVertexArray(vao_pyramid);

    // triangle
    // position
const GLfloat pyramidVertices[] =
                {
                    // FRONT FACE
                    0.0f, 1.0f, 0.0f,
                    -1.0f,-1.0f, 1.0f,
                    1.0f, -1.0f,1.0f,
                    
                    // RIGHT FACE
                    0.0f, 1.0f, 0.0f,
                    1.0f, -1.0f, 1.0f,
                    1.0f, -1.0f, -1.0f,

                    // BACK FACE
                    0.0f, 1.0f, 0.0f,
                    1.0f, -1.0f,-1.0f,
                    -1.0f, -1.0f,-1.0f,

                    // LEFT FACE
                    0.0f, 1.0f, 0.0f,
                    -1.0f, -1.0f,-1.0f,
                    -1.0f, -1.0f,1.0f,
                };


    glGenBuffers(1, &vbo_pyramid_position);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramid_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);    
    glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);

    GLfloat pyramid_normals[] = {
        0.0f, 0.447214f, 0.894427f, //FRONT 
        0.0f, 0.447214f, 0.894427f, //FRONT 
        0.0f, 0.447214f, 0.894427f, //FRONT         
        
        0.894427f, 0.447214f, 0.0f, // RIGHT
        0.894427f, 0.447214f, 0.0f, // RIGHT
        0.894427f, 0.447214f, 0.0f, // RIGHT
        
        0.0f, 0.447214f, -0.894427f, // BACK
        0.0f, 0.447214f, -0.894427f, // BACK
        0.0f, 0.447214f, -0.894427f, // BACK
        
        -0.894427f, 0.447214f, 0.0f, // LEFT
        -0.894427f, 0.447214f, 0.0f, // LEFT
        -0.894427f, 0.447214f, 0.0f // LEFT
    };

    glGenBuffers(1, &vbo_pyramid_normals);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramid_normals);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramid_normals), pyramid_normals, GL_STATIC_DRAW);
    glVertexAttribPointer(HRH_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(HRH_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo_pyramid_position

    glBindVertexArray(0); // end for vao_pyramid
}

-(void)reshape
{
    //code
    [super reshape];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    NSRect rect = [self bounds];
    if(rect.size.height <= 0)
    {
        rect.size.height = 1;
    }

    glViewport(0, 0, (GLsizei)rect.size.width, (GLsizei)rect.size.height);
    
    GLuint width = rect.size.width;
    GLuint height = rect.size.height;

    // projection

    // glORtho(left, right, bottom, top, near, far);
    perspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); 


    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(void)drawRect:(NSRect)dirty_rect
{ 
    //code
    [self drawView];
}

-(void)drawView
{
    //code
    static GLfloat angle = 0.0f;

    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // start from here

    // START USING OpenGL program object
    glUseProgram(shaderProgramObject);

    // OpenGL Drawing
    // set modelview & modelviewProjection matrices to indentity
    mat4 translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
    mat4 rotateMatrix = vmath::rotate((GLfloat)angle, 0.0f, 1.0f, 0.0f);
    mat4 viewMatrix = mat4::identity();
    mat4 modelMatrix = mat4::identity();    
    
    if (isLightOn == 1)
    {
        glUniform4fv(light_0_PossitionUniform, 1, (const GLfloat*) lightPosition_0);
        glUniform3fv(light_0_DiffusedUniform, 1, (const GLfloat*) lightDiffuse_0);
        glUniform3fv(light_0_AmbientUniform,  1 , (const GLfloat*)lightAmbient_0);
        glUniform3fv(light_0_SpecularUniform, 1 , (const GLfloat*)lightSpecular_0);

        glUniform4fv(light_1_PossitionUniform, 1, (const GLfloat*)lightPosition_1);
        glUniform3fv(light_1_DiffusedUniform, 1, (const GLfloat*)lightDiffuse_1);
        glUniform3fv(light_1_AmbientUniform, 1, (const GLfloat*)lightAmbient_1);
        glUniform3fv(light_1_SpecularUniform, 1, (const GLfloat*)lightSpecular_1);

        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*) materialAmbient);
        glUniform1f (materialShininessUniform, materialShininess);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*) materialSpecular);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*) materialDiffuse);
    }

    glUniform1i(isLightOnUniform, isLightOn);
    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);
    glUniformMatrix4fv(viewUniform, 1, GL_FALSE, (const float*) viewMatrix);
    glUniformMatrix4fv(perspectiveUniform, 1, GL_FALSE, (const GLfloat *) perspectiveProjectionMatrix);

    modelMatrix = translateMatrix * rotateMatrix;
    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*)modelMatrix);
    // *** bind vao **
    glBindVertexArray(vao_pyramid);
    
    glDrawArrays(GL_TRIANGLES, 0, 12);

    glBindVertexArray(0);

    // stop using OpenGL program object
    glUseProgram(0);

    if(angle > 360.0f)
    {
        angle = 0.0f;
    }
    angle = angle + 0.1f;
    
    // update();

    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder
{
    //code
    [[self window]makeFirstResponder:self];
    return YES;
}

// WM_KEYDOWN
-(void)keyDown:(NSEvent *)theEvent
{
    //code
    int key = [[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        //ESC
        case 27:
            // self i.e. MyView
            [self release];
            // WM_DESTROY
            [NSApp terminate:self];
            break;
        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];
            break;
        case 'a':
        case 'A':
            if (bAnimate == false)
            {
                bAnimate = true;
            }
            else
            {
                bAnimate = false;
            }
            break;
        case 'l':
        case 'L':
            if (isLightOn == 0)
            {
                isLightOn = 1;
            }
            else
            {
                isLightOn = 0;
            }
            break;

    }
}

// left mouse
-(void)mouseDown:(NSEvent *)theEvent
{
    //code
    
}

// right
-(void)rightMouseDown:(NSEvent *)theEvent
{
    //code
    
}

-(void)otherMouseDown:(NSEvent *)theEvent
{
    //code
    
}

-(void)dealloc
{
    //code

    // Unintialize();

    [self unintialize];

    [super dealloc];

    CVDisplayLinkStop(cvDisplayLinkRef);

    CVDisplayLinkRelease(cvDisplayLinkRef);
}

-(void)unintialize
{
    //code
    
    // destroy vao_pyramid
    if (vao_pyramid)
    {
        glDeleteVertexArrays(1, &vao_pyramid);
        vao_pyramid = 0;
    }

    // destroy vbo_pyramid_position
    if (vbo_pyramid_position)
    {
        glDeleteBuffers(1, &vbo_pyramid_position);
        vbo_pyramid_position = 0;
    }
    
    // deatch vertex shader from shader program object
    glDetachShader(shaderProgramObject, vertexShaderObject);

    // detach fragment shader from shader program obejct
    glDetachShader(shaderProgramObject, fragmentShaderObject);

    // delete vertex shader object
    glDeleteShader(vertexShaderObject);

    vertexShaderObject = 0;

    // delete fragment shader object
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;

    // delete shader program object
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;

    // unlink shader program
    glUseProgram(0);

}


@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *now, const CVTimeStamp *outputTime, CVOptionFlags in,
                                CVOptionFlags *out, void *displayLinkContext)
 {
    //code
    CVReturn result = [(MyOpenGLView *)displayLinkContext getFrameForTime:outputTime];

    return result;
 }
