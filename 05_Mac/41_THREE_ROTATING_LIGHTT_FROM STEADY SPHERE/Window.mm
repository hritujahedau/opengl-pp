//
//  window.m
//  
//
//  Created by user166446 on 7/8/21.
//

// stdio.h is inlcuded in Foundation.h 
#import <Foundation/Foundation.h> // similar to standard library
#import <Cocoa/Cocoa.h> // xlib.h windows.h
#import <QuartzCore/CVDisplayLink.h>     //core video
#import <OpenGL/GL3.h>      // gl.h

#import "vmath.h"

#include "Sphere.h"

using namespace vmath;

// global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef , const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags , CVOptionFlags *, void *);

// global variable declarations
FILE *gpFile = NULL;

enum
{
    HRH_ATTRIBUTE_POSITION = 0 ,
    HRH_ATTRIBUTE_COLOR ,
    HRH_ATTRIBUTE_NORMAL ,
    HRH_ATTRIBUTE_TEXTURE_0
};

const float RADIAN_VALUE = 3.14159/180.0f;

int isLightOn = 0;
int isPerFragmentLight = 0;
int isPerVertexLight = 0;

GLfloat lightPosition[] = { 0.0f, 0.0f, 0.0f, 1.0f, 
                            0.0f, 0.0f, 0.0f, 1.0f,
                            0.0f, 0.0f, 0.0f, 1.0f
                        };
                        
GLfloat lightAmbient[] = {  0.0f, 0.0f, 0.0f,
                            0.0f, 0.0f, 0.0f,
                            0.0f, 0.0f, 0.0f
                        };
                        
GLfloat lightDiffuse[] = {  1.0f, 0.0f, 0.0f,
                            0.0f, 1.0f, 0.0f,
                            0.0f, 0.0f, 1.0f
                        };
                            
GLfloat lightSpecular[] = { 1.0f, 0.0f, 0.0f,
                            0.0f, 1.0f, 0.0f,
                            0.0f, 0.0f, 1.0
                        };

GLuint lightAmbientUniform;
GLuint lightSpecularUniform;
GLuint lightDiffusedUniform;
GLuint lightPossitionUniform;

GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f };
GLfloat materialDiffuse[] = { 1.0f, 1.0f, 1.0f };
GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0 };
GLfloat materialShininess = 128.0f;

bool bAnimate = false;

GLfloat sin_angle = 0.0f, cos_angle = 0.0f, radius = 2;

// NS - NextSTEP
// Delegation performance <>
@interface AppDelegate:NSObject <NSApplicationDelegate,NSWindowDelegate>
@end

int main(int argc, char *argv[])
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    
    NSApp = [NSApplication sharedApplication];
    
    [NSApp setDelegate:[[AppDelegate alloc]init]];
    
    [NSApp run]; // run loop/ message loop/ event loop
    
    [pool release];
    
    return 0;
}

@interface MyOpenGLView:NSOpenGLView

@end

@implementation AppDelegate
{
    @private
    NSWindow *window ;
    MyOpenGLView *myOpenGLview;
}

// WM_CREATE
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // code
    // internally CGrect, core graphics , c library
    // first 2 variable together NSPoint , CGPoint
    // last 2 variable together NSSize structure , CGSzie

    NSBundle *appBundle = [NSBundle mainBundle];
    NSString *appDirPath = [appBundle bundlePath]; // /User/Hrituja/Desktop/RTR3.0/02_LogFile/window.app
    NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent]; // remove window.app give /User/Hrituja/Desktop/RTR3.0/02_LogFile
    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/log.txt", parentDirPath];
    const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    
    gpFile = fopen(pszLogFileNameWithPath, "w");
    if(gpFile == NULL)
    {
        [self release];
        [NSApp terminate:self];
        // end application 
    }
    
    fprintf(gpFile, "\nFile open successfully\n");

    NSRect win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);
    window = [[NSWindow alloc]initWithContentRect:win_rect
                                        styleMask:NSWindowStyleMaskTitled |
                                                NSWindowStyleMaskClosable |
                                                NSWindowStyleMaskMiniaturizable |
                                                NSWindowStyleMaskResizable
                                          backing:NSBackingStoreBuffered
                                           defer:NO
              ];
    [window setTitle:@"HRH: OGL"];
    [window center];
    
    myOpenGLview = [[MyOpenGLView alloc]initWithFrame:win_rect];
    
    [window setContentView:myOpenGLview];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

// WM_DESTTROY
-(void)applicationWillTerminate:(NSNotification *)aNotification
{
    //code

    if(gpFile)
    {
        fprintf(gpFile , "\nProgram terminated successfully\n");
        fclose(gpFile);
        gpFile = NULL;
    }
}

-(void)windowWillClose:(NSNotification *)aNotification
{
    //code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [myOpenGLview release];
    [window release];
    [super dealloc];

}
@end

@implementation MyOpenGLView
{
    @private
    CVDisplayLinkRef cvDisplayLinkRef;

    GLuint vertexShaderObjectPerVertex;
    GLuint fragmentShaderObjectPerVertex;
    
    GLuint vertexShaderObjectPerFragment;
    GLuint fragmentShaderObjectPerFragment;
    
    GLuint shaderProgramObjectPerVertex;
    GLuint shaderProgramObjectPerFragment;
    GLuint shaderProgramObject;
        
    GLuint modelUniform;
    GLuint viewUniform;
    GLuint perspectiveUniform;
    
    GLuint materialSpecularUniform;
    GLuint materialAmbientUniform;
    GLuint materialDiffuseUniform;
    GLuint materialShininessUniform;
    
    GLuint isLightOnUniform;

    GLuint vao_sphere;
    GLuint vbo_sphere_position;
    GLuint vbo_sphere_normal;
    GLuint vbo_sphere_element;
    
    GLfloat sphere_vertices[1146];
    GLfloat Sphere_normals[1146];
    GLfloat Sphere_textrure[764];
    unsigned short sphere_elements[2280];
    
    int gNumVertices;
    int gNumElements;

    mat4 perspectiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame
{
    //code
    self = [super initWithFrame:frame];

    if(self)
    {
        // pixel format descripor
        NSOpenGLPixelFormatAttribute attributes[] =
                                                    {
                                                        NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
                                                        NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
                                                        NSOpenGLPFANoRecovery,
                                                        NSOpenGLPFAAccelerated,
                                                        NSOpenGLPFAColorSize, 24,
                                                        NSOpenGLPFADepthSize, 24,
                                                        NSOpenGLPFAAlphaSize, 8,
                                                        NSOpenGLPFADoubleBuffer,
                                                        0
                                                    };
        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attributes]autorelease];
    
        if(pixelFormat == nil)
        {
            fprintf(gpFile, "\nPixel Format failed");
            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease]; // get pixell format

        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext];
    }



    return self;
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)output_time
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

    [self drawView];

    [pool release];

    return kCVReturnSuccess;
}

-(void)prepareOpenGL
{
    //code
    [super prepareOpenGL];

    [[self openGLContext]makeCurrentContext];

    // swap interval
    GLint swapInt = 1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];  // cp - context parameter

    // write shaders here
    if( [self loadShaders] == NO)
    {
       [self unintialize];
    }
    
    [self loadObjects];

    fprintf(gpFile, "\ngNumVertices = %d,\ngNumElements = %d", gNumVertices, gNumElements);
    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    //glEnable(GL_CULL_FACE);

    perspectiveProjectionMatrix = mat4::identity();

    // core video and core graphics related code

    // 1. Create display link
    // 2. Set callback for display link
    // 3. NSContext -> CGL
    // 4. NSPixelFormat -> CGL
    // 5. Set current CGL context and CGL pixel format 
 

    CVDisplayLinkCreateWithActiveCGDisplays(&cvDisplayLinkRef);
    CVDisplayLinkSetOutputCallback(cvDisplayLinkRef, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(cvDisplayLinkRef, cglContext, cglPixelFormat);
    CVDisplayLinkStart(cvDisplayLinkRef);

}

-(BOOL)loadShaders
{
    vertexShaderObjectPerVertex = glCreateShader(GL_VERTEX_SHADER);
    
    //provide souce code to shader
    const GLchar* vertexShaderSourceCodePerVertex =
                                            " #version 410 core\n"\
                                            " in vec4 v_position;" \
                                            " in vec3 v_normals; " \
                                            " uniform mat4 u_modelMatrix;" \
                                            " uniform mat4 u_viewMatrix;" \
                                            " uniform mat4 u_projectionMatrix;" \
                                            " uniform int u_isLightOn;" \
                                            " uniform vec3 u_lightAmbient[3];" \
                                            " uniform vec3 u_lightSpecular[3];" \
                                            " uniform vec3 u_lightDiffuse[3];" \
                                            " uniform vec4 u_lightposition[3]; " \
                                            " uniform vec3 u_materialAmbient; " \
                                            " uniform vec3 u_materialSpecular; "\
                                            " uniform vec3 u_materialDiffuse; "\
                                            " uniform float u_materialshininess; "\
                                            " out vec3 phoung_ads_lighting;" \
                                            " vec4 eye_coordinates;"\
                                            " vec3 transformed_normal, light_direction, reflection_vector, view_vector;"\
                                            " vec3 ambient, diffuse_light, specular;"\
                                            " void main()" \
                                            " {" \
                                            " phoung_ads_lighting = vec3(0.0, 0.0, 0.0);" \
                                            " if (u_isLightOn == 1) " \
                                            " {" \
                                                 "eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" \
                                                 "transformed_normal = normalize( mat3( u_viewMatrix * u_modelMatrix ) * v_normals); " \
                                                 "for(int i=0; i< 3;i++) "\
                                                 "{" \
                                                     " light_direction = normalize( vec3 ( ( u_lightposition[i]) - eye_coordinates) ); " \
                                                     " reflection_vector = reflect (-light_direction, transformed_normal); " \
                                                     " view_vector = normalize ( vec3 ( -eye_coordinates)); " \
                                                     " ambient = u_lightAmbient[i] * u_materialAmbient; " \
                                                     " diffuse_light = u_lightDiffuse[i] * u_materialDiffuse * max ( dot (light_direction , transformed_normal), 0.0); "\
                                                     " specular = u_lightSpecular[i] * u_materialSpecular * pow( max ( dot ( reflection_vector , view_vector), 0.0) , u_materialshininess);" \
                                                     " phoung_ads_lighting = phoung_ads_lighting + ambient + diffuse_light + specular;" \
                                                 "}"\
                                            " }" \
                                            " else"\
                                            " {"\
                                            " phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" \
                                            " }"\
                                            " gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" \
                                            " }";

    glShaderSource(vertexShaderObjectPerVertex, 1, (const GLchar**)&vertexShaderSourceCodePerVertex, NULL);

    // compile shader
    glCompileShader(vertexShaderObjectPerVertex);

    // check compilation error
    GLint iInfoLogLength = 0;
    GLint iShaderCompileStatus = 0;
    char* szInfoLog = NULL;
    glGetShaderiv(vertexShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
                printf("Vertex shader compilation error : %s \n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    /***************** FRAGMENT SHADER **********************/

    // CREATE SHADER
    fragmentShaderObjectPerVertex = glCreateShader(GL_FRAGMENT_SHADER);

    //PROVIDE SOURCE CODE

    const char* fragmentShaderSourceCodePerVertex =
                                            " #version 410 core\n"\
                                            " in vec3 phoung_ads_lighting;"\
                                            " out vec4 FragColor; " \
                                            " void main()" \
                                            " {"\
                                            " FragColor = vec4(phoung_ads_lighting, 1.0); " \
                                            " }";
    
    glShaderSource(fragmentShaderObjectPerVertex, 1, (const char**)&fragmentShaderSourceCodePerVertex, NULL);

    // COMPILE SHADER
    glCompileShader(fragmentShaderObjectPerVertex);

    // compilation error checking

    glGetShaderiv(fragmentShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
                printf("Fragment Shader Compilation Error : %s\n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    // SHADER PROGRAM
    //CREATE SHADER PROGRAM

    shaderProgramObjectPerVertex = glCreateProgram();

    // attach vertex shader to shader program
    glAttachShader(shaderProgramObjectPerVertex, vertexShaderObjectPerVertex);

    // attch fragment shader to shader program
    glAttachShader(shaderProgramObjectPerVertex, fragmentShaderObjectPerVertex);

    // pri-link binding of shader object with vertex shader possition attribute
    glBindAttribLocation(shaderProgramObjectPerVertex, HRH_ATTRIBUTE_POSITION, "v_position");
    glBindAttribLocation(shaderProgramObjectPerVertex, HRH_ATTRIBUTE_NORMAL, "v_normals");

    // LINK PROGRAM
    glLinkProgram(shaderProgramObjectPerVertex);

    // CHECK LINKING ERROR
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(shaderProgramObjectPerVertex, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(shaderProgramObjectPerVertex, iInfoLogLength, &written, szInfoLog);
                printf("Shader Program Link Log : %s", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }
    
    /*************************************************LIGHT PER FRGAMENT********************************************************/

    vertexShaderObjectPerFragment = glCreateShader(GL_VERTEX_SHADER);
    
    //provide souce code to shader
    const GLchar* vertexShaderSourceCodePerFragment =
                                            " #version 410 core\n"\
                                            " in vec4 v_position;" \
                                            " in vec3 v_normals; " \
                                            " uniform mat4 u_modelMatrix;" \
                                            " uniform mat4 u_viewMatrix;" \
                                            " uniform mat4 u_projectionMatrix;" \
                                            " uniform int u_isLightOn;" \
                                            " uniform vec4 u_lightposition[3]; " \
                                            " out vec3 light_direction[3]; "\
                                            " out vec3 transformed_normal;"\
                                            " out vec3 view_vector;"\
                                            " void main()" \
                                            " {" \
                                            " if (u_isLightOn == 1) " \
                                            " {" \
                                                 "vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" \
                                                 "transformed_normal =  mat3( u_viewMatrix * u_modelMatrix ) * v_normals;" \
                                                 "for(int i = 0; i< 3; i++)"\
                                                 "{"\
                                                     "light_direction[i] = vec3 ( u_lightposition[i]  - eye_coordinates);" \
                                                 "}"\
                                                 "view_vector =  vec3 ( -eye_coordinates);" \
                                            " }" \
                                            " gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" \
                                            " }";

    glShaderSource(vertexShaderObjectPerFragment, 1, (const GLchar**)&vertexShaderSourceCodePerFragment, NULL);

    // compile shader
    glCompileShader(vertexShaderObjectPerFragment);

    // check compilation error
    iInfoLogLength = 0;
    iShaderCompileStatus = 0;
    szInfoLog = NULL;
    glGetShaderiv(vertexShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
                printf("Vertex shader compilation error : %s \n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    /***************** FRAGMENT SHADER **********************/

    // CREATE SHADER
    fragmentShaderObjectPerFragment = glCreateShader(GL_FRAGMENT_SHADER);

    //PROVIDE SOURCE CODE

    const char* fragmentShaderSourceCodePerFragment =
                                            " #version 410 core\n"\
                                            " vec3 phoung_ads_lighting;" \
                                            " out vec4 FragColor; " \
                                            " in vec3 light_direction[3]; "\
                                            " in vec3 transformed_normal;"\
                                            " in vec3 view_vector;"\
                                            " vec3 normalized_light_direction; "\
                                            " vec3 normalized_transformed_normal;"\
                                            " vec3 normalized_view_vector;"\
                                            " uniform vec3 u_lightAmbient[3];" \
                                            " uniform vec3 u_lightSpecular[3];" \
                                            " uniform vec3 u_lightDiffuse[3];" \
                                            " uniform vec3 u_materialAmbient; " \
                                            " uniform vec3 u_materialSpecular; "\
                                            " uniform vec3 u_materialDiffuse; "\
                                            " uniform float u_materialshininess; "\
                                            " uniform int u_isLightOn;" \
                                            " vec3 reflection_vector, ambient, diffuse_light, specular;"\
                                            " void main()" \
                                            " {"\
                                            "phoung_ads_lighting = vec3(0.0, 0.0, 0.0);" \
                                            " if (u_isLightOn == 1) " \
                                            " {" \
                                                 "normalized_view_vector = normalize(view_vector);"\
                                                 "normalized_transformed_normal = normalize(transformed_normal);"\
                                                 "for(int i = 0; i < 3 ; i++)"\
                                                 "{"\
                                                     "normalized_light_direction = normalize(light_direction[i]);"\
                                                     "reflection_vector = reflect (-normalized_light_direction, normalized_transformed_normal); " \
                                                     "ambient = u_lightAmbient[i]  * u_materialAmbient; " \
                                                     "diffuse_light = u_lightDiffuse[i] * u_materialDiffuse * max ( dot (normalized_light_direction , normalized_transformed_normal), 0.0); "\
                                                     "specular = u_lightSpecular[i]  * u_materialSpecular * pow( max ( dot ( reflection_vector , normalized_view_vector), 0.0) , u_materialshininess);" \
                                                     "phoung_ads_lighting = phoung_ads_lighting + ambient + diffuse_light + specular;" \
                                                 "}"\
                                            " }"\
                                            " else"\
                                            " {"\
                                                 "phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" \
                                            " }"\
                                            " FragColor = vec4(phoung_ads_lighting, 1.0); " \
                                            " }";

    
    glShaderSource(fragmentShaderObjectPerFragment, 1, (const char**)&fragmentShaderSourceCodePerFragment, NULL);

    // COMPILE SHADER
    glCompileShader(fragmentShaderObjectPerFragment);

    // compilation error checking

    glGetShaderiv(fragmentShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
                printf("Fragment Shader Compilation Error : %s\n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    // SHADER PROGRAM
    //CREATE SHADER PROGRAM

    shaderProgramObjectPerFragment = glCreateProgram();

    // attach vertex shader to shader program
    glAttachShader(shaderProgramObjectPerFragment, vertexShaderObjectPerFragment);

    // attch fragment shader to shader program
    glAttachShader(shaderProgramObjectPerFragment, fragmentShaderObjectPerFragment);

    // pri-link binding of shader object with vertex shader possition attribute
    glBindAttribLocation(shaderProgramObjectPerFragment, HRH_ATTRIBUTE_POSITION, "v_position");
    glBindAttribLocation(shaderProgramObjectPerFragment, HRH_ATTRIBUTE_NORMAL, "v_normals");

    // LINK PROGRAM
    glLinkProgram(shaderProgramObjectPerFragment);

    // CHECK LINKING ERROR
    iShaderProgramLinkStatus = 0;
    glGetProgramiv(shaderProgramObjectPerFragment, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(shaderProgramObjectPerFragment, iInfoLogLength, &written, szInfoLog);
                printf("Shader Program Link Log : %s", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    return YES;
}

-(void)loadObjects
{

    getSphereVertexData(sphere_vertices, Sphere_normals, Sphere_textrure, sphere_elements);
    gNumVertices = getNumberOfSphereVertices();
    gNumElements = getNumberOfSphereElements();

    glGenVertexArrays(1, &vao_sphere);
    glBindVertexArray(vao_sphere);

    glGenBuffers(1, &vbo_sphere_position);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo

    glGenBuffers(1, &vbo_sphere_normal);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Sphere_normals), Sphere_normals, GL_STATIC_DRAW);
    glVertexAttribPointer(HRH_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(HRH_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // element vbo
    glGenBuffers(1, &vbo_sphere_element);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glBindVertexArray(0); // end for Vao

}


-(void)reshape
{
    //code
    [super reshape];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    NSRect rect = [self bounds];
    if(rect.size.height <= 0)
    {
        rect.size.height = 1;
    }

    glViewport(0, 0, (GLsizei)rect.size.width, (GLsizei)rect.size.height);
    
    GLuint width = rect.size.width;
    GLuint height = rect.size.height;

    // projection

    // glORtho(left, right, bottom, top, near, far);
    perspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); 


    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(void)drawRect:(NSRect)dirty_rect
{ 
    //code
    [self drawView];
}

-(void)drawView
{
    // varaible declaration
    static GLfloat angle_red = 360.0f,angle_blue = 0.0f, angle_green = 360.0f;

    //code
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // start from here
    shaderProgramObject = (isPerVertexLight == 1) ? shaderProgramObjectPerVertex : shaderProgramObjectPerFragment;

    glUseProgram(shaderProgramObject);

    // OpenGL Drawing
    // set modelview & modelviewProjection matrices to indentity
    mat4 translateMatrix = vmath::translate(0.0f, 0.0f, -1.55f);
    mat4 viewMatrix = mat4::identity();
    mat4 modelMatrix = translateMatrix;

    // get MVP uniform location
    modelUniform = glGetUniformLocation      (shaderProgramObject, "u_modelMatrix");
    viewUniform = glGetUniformLocation       (shaderProgramObject, "u_viewMatrix");
    perspectiveUniform = glGetUniformLocation(shaderProgramObject, "u_projectionMatrix");
    isLightOnUniform = glGetUniformLocation  (shaderProgramObject, "u_isLightOn");

    lightAmbientUniform = glGetUniformLocation  (shaderProgramObject, "u_lightAmbient");
    lightSpecularUniform = glGetUniformLocation (shaderProgramObject, "u_lightSpecular");
    lightDiffusedUniform = glGetUniformLocation (shaderProgramObject, "u_lightDiffuse");
    lightPossitionUniform = glGetUniformLocation(shaderProgramObject, "u_lightposition");

    materialDiffuseUniform = glGetUniformLocation  (shaderProgramObject, "u_materialDiffuse");
    materialAmbientUniform = glGetUniformLocation  (shaderProgramObject, "u_materialAmbient");
    materialSpecularUniform = glGetUniformLocation (shaderProgramObject, "u_materialSpecular");
    materialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_materialshininess");

    if (isLightOn == 1)
    {
        sin_angle = radius * sin(angle_red * RADIAN_VALUE);
        cos_angle = radius * cos(angle_red * RADIAN_VALUE);
        
        lightPosition[1] = sin_angle;
        lightPosition[2] = cos_angle;
        
        sin_angle = radius * sin(angle_blue * RADIAN_VALUE);
        cos_angle = radius * cos(angle_blue * RADIAN_VALUE);
        
        lightPosition[4] = sin_angle;
        lightPosition[6] = cos_angle;
        
        sin_angle = radius * sin(angle_green * RADIAN_VALUE);
        cos_angle = radius * cos(angle_green * RADIAN_VALUE);
        
        lightPosition[8] = sin_angle;
        lightPosition[9] = cos_angle;
        
        glUniform4fv(lightPossitionUniform, 3, (const GLfloat*) lightPosition);
        glUniform3fv(lightDiffusedUniform,  3 , (const GLfloat*) lightDiffuse);
        glUniform3fv(lightAmbientUniform,   3, (const GLfloat*) lightAmbient);
        glUniform3fv(lightSpecularUniform,  3, (const GLfloat*) lightSpecular);
        
        glUniform3fv(materialAmbientUniform, 1, (const GLfloat*) materialAmbient);
        glUniform1f (materialShininessUniform, materialShininess);
        glUniform3fv(materialSpecularUniform, 1, (const GLfloat*) materialSpecular);
        glUniform3fv(materialDiffuseUniform, 1, (const GLfloat*) materialDiffuse);
    }

    glUniform1i(isLightOnUniform, isLightOn);
    glUniformMatrix4fv(modelUniform, 1, GL_FALSE, (const float*) modelMatrix);
    glUniformMatrix4fv(viewUniform, 1, GL_FALSE, (const float*) viewMatrix);
    glUniformMatrix4fv(perspectiveUniform, 1, GL_FALSE, (const GLfloat *) perspectiveProjectionMatrix);

    // *** bind vao **
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);

    // stop using OpenGL program object
    glUseProgram(0);

    if (bAnimate == true)
    {
        angle_red = angle_red - 0.5;
        angle_blue = angle_blue + 0.5;
        angle_green = angle_green - 0.5;
        if (angle_red <= 0)
        {
            angle_red = 360;
        }
        if (angle_blue >= 360)
        {
            angle_blue = 1.5;
        }
        if (angle_green <= 0)
        {
            angle_green = 360;
        }
    }

    // update();

    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder
{
    //code
    [[self window]makeFirstResponder:self];
    return YES;
}

// WM_KEYDOWN
-(void)keyDown:(NSEvent *)theEvent
{
    //code
    int key = [[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        //ESC
        case 27:
            // self i.e. MyView
            [self release];
            // WM_DESTROY
            [NSApp terminate:self];
            break;
        case 'Q':
        case 'q':
            [[self window]toggleFullScreen:self];
            break;
        case 'a':
        case 'A':
            if (bAnimate == false)
            {
                bAnimate = true;
            }
            else
            {
                bAnimate = false;
            }
            break;
        case 'l':
        case 'L':
            if (isLightOn == 0)
            {
                isLightOn = 1;
            }
            else
            {
                isLightOn = 0;
            }
            break;
        case 'f':
        case 'F':
            if(isPerFragmentLight == 0)
            {
                isPerFragmentLight = 1;
                isPerVertexLight = 0;
                isLightOn = 1;
                bAnimate = 1;
                shaderProgramObject = shaderProgramObjectPerFragment;
            }
            else
            {
                isPerFragmentLight = 0;
            }
            break;
        case 'v':
        case 'V':
            if(isPerVertexLight == 0)
            {
                isPerVertexLight = 1;
                isPerFragmentLight =0;
                isLightOn = 1;
                bAnimate = 1;
                shaderProgramObject = shaderProgramObjectPerVertex;
            }
            else
            {
                isPerVertexLight = 0;
            }
            break;

    }
}

// left mouse
-(void)mouseDown:(NSEvent *)theEvent
{
    //code
    
}

// right
-(void)rightMouseDown:(NSEvent *)theEvent
{
    //code
    
}

-(void)otherMouseDown:(NSEvent *)theEvent
{
    //code
    
}

-(void)dealloc
{
    //code

    // Unintialize();

    [self unintialize];

    [super dealloc];

    CVDisplayLinkStop(cvDisplayLinkRef);

    CVDisplayLinkRelease(cvDisplayLinkRef);
}

-(void)unintialize
{
    //code
    if (vao_sphere)
    {
        glDeleteVertexArrays(1, &vao_sphere);
        vao_sphere = 0;
    }

    // destroy vbo
    if (vbo_sphere_position)
    {
        glDeleteBuffers(1, &vbo_sphere_position);
        vbo_sphere_position = 0;
    }

    if (vbo_sphere_element)
    {
        glDeleteBuffers(1, &vbo_sphere_element);
        vbo_sphere_element = 0;
    }

    if (vbo_sphere_normal)
    {
        glDeleteBuffers(1, &vbo_sphere_normal);
        vbo_sphere_normal = 0;
    }

    // deatch vertex shader from shader program object
    glDetachShader(shaderProgramObjectPerVertex, vertexShaderObjectPerVertex);

    // detach fragment shader from shader program obejct
    glDetachShader(shaderProgramObjectPerVertex, fragmentShaderObjectPerVertex);

    // delete vertex shader object
    glDeleteShader(vertexShaderObjectPerVertex);

    vertexShaderObjectPerVertex = 0;

    // delete fragment shader object
    glDeleteShader(fragmentShaderObjectPerVertex);
    fragmentShaderObjectPerVertex = 0;

    // delete shader program object
    glDeleteProgram(shaderProgramObjectPerVertex);
    shaderProgramObjectPerVertex = 0;

    // fragment
    // deatch vertex shader from shader program object
    glDetachShader(shaderProgramObjectPerFragment, vertexShaderObjectPerFragment);

    // detach fragment shader from shader program obejct
    glDetachShader(shaderProgramObjectPerFragment, fragmentShaderObjectPerFragment);

    // delete vertex shader object
    glDeleteShader(vertexShaderObjectPerFragment);

    vertexShaderObjectPerFragment = 0;

    // delete fragment shader object
    glDeleteShader(fragmentShaderObjectPerFragment);
    fragmentShaderObjectPerFragment = 0;

    // delete shader program object
    glDeleteProgram(shaderProgramObjectPerFragment);
    shaderProgramObjectPerFragment = 0;



    // unlink shader program
    glUseProgram(0);

}


@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *now, const CVTimeStamp *outputTime, CVOptionFlags in,
                                CVOptionFlags *out, void *displayLinkContext)
 {
    //code
    CVReturn result = [(MyOpenGLView *)displayLinkContext getFrameForTime:outputTime];

    return result;
 }
