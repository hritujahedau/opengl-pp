//
//  window.m
//  
//
//  Created by user166446 on 7/8/21.
//

// stdio.h is inlcuded in Foundation.h 
#import <Foundation/Foundation.h> // similar to standard library
#import <Cocoa/Cocoa.h> // xlib.h windows.h
#import <QuartzCore/CVDisplayLink.h>     //core video
#import <OpenGL/GL3.h>      // gl.h

#import "vmath.h"

#include "Sphere.h"

using namespace vmath;

// global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef , const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags , CVOptionFlags *, void *);

// global variable declarations
FILE *gpFile = NULL;

enum
{
    HRH_ATTRIBUTE_POSITION = 0 ,
    HRH_ATTRIBUTE_COLOR ,
    HRH_ATTRIBUTE_NORMAL ,
    HRH_ATTRIBUTE_TEXTURE_0
};

int isLightPerVertexShader = 0, isLightPerFragmentShader = 0, isLightOn = 0;

GLfloat lightPossition[] = { 100.0f, 100.0f, 100.0f, 1.0f };
GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f };

GLfloat lightDefuse[] = { 0.5f, 0.2f, 0.7f };
GLfloat lightSpecular[] = { 0.7f, 0.7f, 0.7f };

GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f };
GLfloat materialDefuse[] = { 1.0f, 1.0f, 1.0f  };
GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0f};
GLfloat materialShinyness = 128.0f;

// NS - NextSTEP
// Delegation performance <>
@interface AppDelegate:NSObject <NSApplicationDelegate,NSWindowDelegate>
@end

int main(int argc, char *argv[])
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    
    NSApp = [NSApplication sharedApplication];
    
    [NSApp setDelegate:[[AppDelegate alloc]init]];
    
    [NSApp run]; // run loop/ message loop/ event loop
    
    [pool release];
    
    return 0;
}

@interface MyOpenGLView:NSOpenGLView

@end

@implementation AppDelegate
{
    @private
    NSWindow *window ;
    MyOpenGLView *myOpenGLview;
}

// WM_CREATE
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // code
    // internally CGrect, core graphics , c library
    // first 2 variable together NSPoint , CGPoint
    // last 2 variable together NSSize structure , CGSzie

    NSBundle *appBundle = [NSBundle mainBundle];
    NSString *appDirPath = [appBundle bundlePath]; // /User/Hrituja/Desktop/RTR3.0/02_LogFile/window.app
    NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent]; // remove window.app give /User/Hrituja/Desktop/RTR3.0/02_LogFile
    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/log.txt", parentDirPath];
    const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    
    gpFile = fopen(pszLogFileNameWithPath, "w");
    if(gpFile == NULL)
    {
        [self release];
        [NSApp terminate:self];
        // end application 
    }
    
    fprintf(gpFile, "\nFile open successfully\n");

    NSRect win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);
    window = [[NSWindow alloc]initWithContentRect:win_rect
                                        styleMask:NSWindowStyleMaskTitled |
                                                NSWindowStyleMaskClosable |
                                                NSWindowStyleMaskMiniaturizable |
                                                NSWindowStyleMaskResizable
                                          backing:NSBackingStoreBuffered
                                           defer:NO
              ];
    [window setTitle:@"HRH: OGL"];
    [window center];
    
    myOpenGLview = [[MyOpenGLView alloc]initWithFrame:win_rect];
    
    [window setContentView:myOpenGLview];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

// WM_DESTTROY
-(void)applicationWillTerminate:(NSNotification *)aNotification
{
    //code

    if(gpFile)
    {
        fprintf(gpFile , "\nProgram terminated successfully\n");
        fclose(gpFile);
        gpFile = NULL;
    }
}

-(void)windowWillClose:(NSNotification *)aNotification
{
    //code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [myOpenGLview release];
    [window release];
    [super dealloc];

}
@end

@implementation MyOpenGLView
{
    @private
    CVDisplayLinkRef cvDisplayLinkRef;

    GLuint gVertexShaderObjectPerFragment;
    GLuint gFragmentShaderObjectPerFragment;
    GLuint gShaderProgramObjectPerFragment;
    
    GLuint gVertexShaderObjectPerVertex;
    GLuint gFragmentShaderObjectPerVertex;
    GLuint gShaderProgramObjectPerVertex;

    GLuint gCurrentShaderProgram;

    GLuint vao_sphere;
    GLuint vbo_sphere_position;
    GLuint vbo_sphere_normal;
    GLuint vbo_sphere_element;
    
    GLuint gModelUniform;
    GLuint gViewUniform;
    GLuint gPerspectiveUniform;
    
    GLuint gLightAmbientUniform;
    GLuint gLightSpecularUniform;
    GLuint gLightDiffusedUniform;
    GLuint gLightPossitionUniform;
    
    GLuint gMaterialSpecularUniform;
    GLuint gMaterialAmbientUniform;
    GLuint gMaterialDiffuseUniform;
    GLuint gMaterialShininessUniform;
    
    GLuint gIsPerVertexShaderUniform;
    GLuint gIsPerFragmentShaderUniform;

    GLfloat sphere_vertices[1146];
    GLfloat Sphere_normals[1146];
    GLfloat Sphere_textrure[764];
    unsigned short sphere_elements[2280];
    
    int gNumVertices;
    int gNumElements;

    mat4 perspectiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame
{
    //code
    self = [super initWithFrame:frame];

    if(self)
    {
        // pixel format descripor
        NSOpenGLPixelFormatAttribute attributes[] =
                                                    {
                                                        NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
                                                        NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
                                                        NSOpenGLPFANoRecovery,
                                                        NSOpenGLPFAAccelerated,
                                                        NSOpenGLPFAColorSize, 24,
                                                        NSOpenGLPFADepthSize, 24,
                                                        NSOpenGLPFAAlphaSize, 8,
                                                        NSOpenGLPFADoubleBuffer,
                                                        0
                                                    };
        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attributes]autorelease];
    
        if(pixelFormat == nil)
        {
            fprintf(gpFile, "\nPixel Format failed");
            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease]; // get pixell format

        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext];
    }



    return self;
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)output_time
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

    [self drawView];

    [pool release];

    return kCVReturnSuccess;
}

-(void)prepareOpenGL
{
    //code
    [super prepareOpenGL];

    [[self openGLContext]makeCurrentContext];

    // swap interval
    GLint swapInt = 1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];  // cp - context parameter

    // write shaders here
    if( [self loadShaders] == NO)
    {
       [self unintialize];
    }
    
    [self loadObjects];

    fprintf(gpFile, "\ngNumVertices = %d,\ngNumElements = %d", gNumVertices, gNumElements);
    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    //glEnable(GL_CULL_FACE);

    perspectiveProjectionMatrix = mat4::identity();

    // core video and core graphics related code

    // 1. Create display link
    // 2. Set callback for display link
    // 3. NSContext -> CGL
    // 4. NSPixelFormat -> CGL
    // 5. Set current CGL context and CGL pixel format 
 

    CVDisplayLinkCreateWithActiveCGDisplays(&cvDisplayLinkRef);
    CVDisplayLinkSetOutputCallback(cvDisplayLinkRef, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(cvDisplayLinkRef, cglContext, cglPixelFormat);
    CVDisplayLinkStart(cvDisplayLinkRef);

}

-(BOOL)loadShaders
{
    gVertexShaderObjectPerVertex = glCreateShader(GL_VERTEX_SHADER);
    
    //provide souce code to shader
    const GLchar* vertexShaderSourceCodePerVertex =
                                            "#version 410 core\n"\
                                            "in vec4 v_position;" \
                                            "in vec3 v_normals; " \
                                            "uniform mat4 u_modelMatrix;" \
                                            "uniform mat4 u_viewMatrix;" \
                                            "uniform mat4 u_projectionMatrix;" \
                                            "uniform int u_isPerVertexLightOn;" \
                                            "uniform vec3 u_lightAmbient;" \
                                            "uniform vec3 u_lightSpecular;" \
                                            "uniform vec3 u_lightDiffuse;" \
                                            "uniform vec4 u_light_position; " \
                                            "uniform vec3 u_materialAmbient; " \
                                            "uniform vec3 u_materialSpecular; "\
                                            "uniform vec3 u_materialDiffuse; "\
                                            "uniform float u_materialshininess; "\
                                            "out vec3 phoung_ads_lighting;" \
                                            "void main()" \
                                            "{" \
                                            "phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" \
                                            "if (u_isPerVertexLightOn == 1) " \
                                            "{" \
                                            "vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" \
                                            "vec3 transformed_normal = normalize( mat3( u_viewMatrix * u_modelMatrix ) * v_normals); " \
                                            "vec3 light_direction = normalize( vec3 (u_light_position - eye_coordinates) ); " \
                                            "vec3 reflection_vector = reflect (-light_direction, transformed_normal); " \
                                            "vec3 view_vector = normalize ( vec3 ( -eye_coordinates)); " \
                                            "vec3 ambient = u_lightAmbient * u_materialAmbient; " \
                                            "vec3 diffuse_light = u_lightDiffuse * u_materialDiffuse * max ( dot (light_direction , transformed_normal), 0.0); "\
                                            "vec3 specular = u_lightSpecular * u_materialSpecular * pow( max ( dot ( reflection_vector , view_vector), 0.0) , u_materialshininess);" \
                                            "phoung_ads_lighting = ambient + diffuse_light + specular;" \
                                            "}" \
                                            "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" \
                                            "}";


    glShaderSource(gVertexShaderObjectPerVertex, 1, (const GLchar**)&vertexShaderSourceCodePerVertex, NULL);

    // compile shader
    glCompileShader(gVertexShaderObjectPerVertex);

    // check compilation error
    GLint iInfoLogLength = 0;
    GLint iShaderCompileStatus = 0;
    char* szInfoLog = NULL;
    glGetShaderiv(gVertexShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
                printf("Vertex shader compilation error : %s \n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    /***************** FRAGMENT SHADER **********************/

    // CREATE SHADER
    gFragmentShaderObjectPerVertex = glCreateShader(GL_FRAGMENT_SHADER);

    //PROVIDE SOURCE CODE

    const char* fragmentShaderSourceCodePerVertex =
                                            "#version 410 core\n"\
                                            "in vec3 phoung_ads_lighting;" \
                                            "out vec4 FragColor; " \
                                            "void main()" \
                                            "{"\
                                            "FragColor = vec4(phoung_ads_lighting, 1.0); " \
                                            "}";
    
    glShaderSource(gFragmentShaderObjectPerVertex, 1, (const char**)&fragmentShaderSourceCodePerVertex, NULL);

    // COMPILE SHADER
    glCompileShader(gFragmentShaderObjectPerVertex);

    // compilation error checking

    glGetShaderiv(gFragmentShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
                printf("Fragment Shader Compilation Error : %s\n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    // SHADER PROGRAM
    //CREATE SHADER PROGRAM

    gShaderProgramObjectPerVertex = glCreateProgram();

    // attach vertex shader to shader program
    glAttachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);

    // attch fragment shader to shader program
    glAttachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);

    // pri-link binding of shader object with vertex shader possition attribute
    glBindAttribLocation(gShaderProgramObjectPerVertex, HRH_ATTRIBUTE_POSITION, "v_position");
    glBindAttribLocation(gShaderProgramObjectPerVertex, HRH_ATTRIBUTE_NORMAL, "v_normals");

    // LINK PROGRAM
    glLinkProgram(gShaderProgramObjectPerVertex);

    // CHECK LINKING ERROR
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(gShaderProgramObjectPerVertex, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObjectPerVertex, iInfoLogLength, &written, szInfoLog);
                printf("Shader Program Link Log : %s", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }
    

    // PER FRAGMENT

    gVertexShaderObjectPerFragment = glCreateShader(GL_VERTEX_SHADER);
    
    //provide souce code to shader
    const GLchar* vertexShaderSourceCodePerFragment =
                                            "#version 410 core\n"\
                                            "in vec4 v_position;" \
                                            "in vec3 v_normals; " \
                                            "uniform mat4 u_modelMatrix;" \
                                            "uniform mat4 u_viewMatrix;" \
                                            "uniform mat4 u_projectionMatrix;" \
                                            "uniform int u_isPerFragmentLightOn;" \
                                            "uniform vec4 u_light_position; " \
                                            "out vec3 light_direction; "\
                                            "out vec3 transformed_normal;"\
                                            "out vec3 view_vector;"\
                                            "void main()" \
                                            "{" \
                                            "if (u_isPerFragmentLightOn == 1) " \
                                            "{" \
                                            "vec4 eye_coordinates = u_viewMatrix * u_modelMatrix * v_position;" \
                                            "transformed_normal =  mat3( u_viewMatrix * u_modelMatrix ) * v_normals;" \
                                            "light_direction = vec3 (u_light_position - eye_coordinates);" \
                                            "view_vector =  vec3 ( -eye_coordinates);" \
                                            "}" \
                                            "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix  * v_position;" \
                                            "}";


    glShaderSource(gVertexShaderObjectPerFragment, 1, (const GLchar**)&vertexShaderSourceCodePerFragment, NULL);

    // compile shader
    glCompileShader(gVertexShaderObjectPerFragment);

    // check compilation error
    iInfoLogLength = 0;
    iShaderCompileStatus = 0;
    szInfoLog = NULL;
    glGetShaderiv(gVertexShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
                printf("Vertex shader compilation error : %s \n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    /***************** FRAGMENT SHADER **********************/

    // CREATE SHADER
    gFragmentShaderObjectPerFragment = glCreateShader(GL_FRAGMENT_SHADER);

    //PROVIDE SOURCE CODE

    const char* fragmentShaderSourceCodePerFragment =
                                            "#version 410 core\n"\
                                            "vec3 phoung_ads_lighting;" \
                                            "out vec4 FragColor; " \
                                            "in vec3 light_direction; "\
                                            "in vec3 transformed_normal;"\
                                            "in vec3 view_vector;"\
                                            "vec3 normalized_light_direction; "\
                                            "vec3 normalized_transformed_normal;"\
                                            "vec3 normalized_view_vector;"\
                                            "uniform vec3 u_lightAmbient;" \
                                            "uniform vec3 u_lightSpecular;" \
                                            "uniform vec3 u_lightDiffuse;" \
                                            "uniform vec3 u_materialAmbient; " \
                                            "uniform vec3 u_materialSpecular; "\
                                            "uniform vec3 u_materialDiffuse; "\
                                            "uniform float u_materialshininess; "\
                                            "uniform int u_isPerFragmentLightOn;" \
                                            "void main()" \
                                            "{"\
                                            "phoung_ads_lighting = vec3(1.0, 1.0, 1.0);" \
                                            "if (u_isPerFragmentLightOn == 1) " \
                                            "{" \
                                            "normalized_light_direction = normalize(light_direction);"\
                                            "normalized_transformed_normal = normalize(transformed_normal);"\
                                            "normalized_view_vector = normalize(view_vector);"\
                                            "vec3 reflection_vector = reflect (-normalized_light_direction, normalized_transformed_normal); " \
                                            "vec3 ambient = u_lightAmbient * u_materialAmbient; " \
                                            "vec3 diffuse_light = u_lightDiffuse * u_materialDiffuse * max ( dot (normalized_light_direction , normalized_transformed_normal), 0.0); "\
                                            "vec3 specular = u_lightSpecular * u_materialSpecular * pow( max ( dot ( reflection_vector , normalized_view_vector), 0.0) , u_materialshininess);" \
                                            "phoung_ads_lighting = ambient + diffuse_light + specular;" \
                                            "}"\
                                            "FragColor = vec4(phoung_ads_lighting, 1.0); " \
                                            "}";

    
    glShaderSource(gFragmentShaderObjectPerFragment, 1, (const char**)&fragmentShaderSourceCodePerFragment, NULL);

    // COMPILE SHADER
    glCompileShader(gFragmentShaderObjectPerFragment);

    // compilation error checking

    glGetShaderiv(gFragmentShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
                printf("Fragment Shader Compilation Error : %s\n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    // SHADER PROGRAM
    //CREATE SHADER PROGRAM

    gShaderProgramObjectPerFragment = glCreateProgram();

    // attach vertex shader to shader program
    glAttachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);

    // attch fragment shader to shader program
    glAttachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);

    // pri-link binding of shader object with vertex shader possition attribute
    glBindAttribLocation(gShaderProgramObjectPerFragment, HRH_ATTRIBUTE_POSITION, "v_position");
    glBindAttribLocation(gShaderProgramObjectPerFragment, HRH_ATTRIBUTE_NORMAL, "v_normals");

    // LINK PROGRAM
    glLinkProgram(gShaderProgramObjectPerFragment);


    // CHECK LINKING ERROR
    iShaderProgramLinkStatus = 0;
    glGetProgramiv(gShaderProgramObjectPerFragment, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObjectPerFragment, iInfoLogLength, &written, szInfoLog);
                printf("Shader Program Link Log : %s", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    return YES;
}

-(void)loadObjects
{

    getSphereVertexData(sphere_vertices, Sphere_normals, Sphere_textrure, sphere_elements);
    gNumVertices = getNumberOfSphereVertices();
    gNumElements = getNumberOfSphereElements();

    glGenVertexArrays(1, &vao_sphere);
    glBindVertexArray(vao_sphere);

    glGenBuffers(1, &vbo_sphere_position);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo

    glGenBuffers(1, &vbo_sphere_normal);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Sphere_normals), Sphere_normals, GL_STATIC_DRAW);
    glVertexAttribPointer(HRH_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(HRH_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // element vbo
    glGenBuffers(1, &vbo_sphere_element);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glBindVertexArray(0); // end for Vao

}


-(void)reshape
{
    //code
    [super reshape];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    NSRect rect = [self bounds];
    if(rect.size.height <= 0)
    {
        rect.size.height = 1;
    }

    glViewport(0, 0, (GLsizei)rect.size.width, (GLsizei)rect.size.height);
    
    GLuint width = rect.size.width;
    GLuint height = rect.size.height;

    // projection

    // glORtho(left, right, bottom, top, near, far);
    perspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); 


    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(void)drawRect:(NSRect)dirty_rect
{ 
    //code
    [self drawView];
}

-(void)drawView
{
    //code
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // start from here

    gCurrentShaderProgram = (isLightPerVertexShader == 1) ? gShaderProgramObjectPerVertex: gShaderProgramObjectPerFragment ;

    glUseProgram(gCurrentShaderProgram);

    // OpenGL Drawing
    // set modelview & modelviewProjection matrices to indentity
    mat4 translateMatrix = vmath::translate(0.0f, 0.0f, -1.55f);
    mat4 viewMatrix = mat4::identity();
    mat4 modelMatrix = translateMatrix;
    
    gModelUniform = glGetUniformLocation(gCurrentShaderProgram, "u_modelMatrix");
    gViewUniform = glGetUniformLocation(gCurrentShaderProgram, "u_viewMatrix");
    gPerspectiveUniform = glGetUniformLocation(gCurrentShaderProgram, "u_projectionMatrix");
    gIsPerFragmentShaderUniform = glGetUniformLocation(gCurrentShaderProgram, "u_isPerFragmentLightOn");
    gIsPerVertexShaderUniform = glGetUniformLocation(gCurrentShaderProgram, "u_isPerVertexLightOn");

    gLightAmbientUniform = glGetUniformLocation(gCurrentShaderProgram, "u_lightAmbient");
    gLightSpecularUniform = glGetUniformLocation(gCurrentShaderProgram, "u_lightSpecular");
    gLightDiffusedUniform = glGetUniformLocation(gCurrentShaderProgram, "u_lightDiffuse");
    gLightPossitionUniform = glGetUniformLocation(gCurrentShaderProgram, "u_light_position");

    gMaterialDiffuseUniform = glGetUniformLocation(gCurrentShaderProgram, "u_materialDiffuse");
    gMaterialAmbientUniform = glGetUniformLocation(gCurrentShaderProgram, "u_materialAmbient");
    gMaterialSpecularUniform = glGetUniformLocation(gCurrentShaderProgram, "u_materialSpecular");
    gMaterialShininessUniform = glGetUniformLocation(gCurrentShaderProgram, "u_materialshininess");

    if (isLightOn == 1)
    {
        glUniform4fv(gLightPossitionUniform, 1, (const GLfloat*) lightPossition);
        glUniform3fv(gLightDiffusedUniform, 1, (const GLfloat*) lightDefuse);
        glUniform3fv(gLightAmbientUniform,  1 , (const GLfloat*) lightAmbient);
        glUniform3fv(gLightSpecularUniform, 1 , (const GLfloat*) lightSpecular);

        glUniform3fv(gMaterialAmbientUniform, 1, (const GLfloat*) materialAmbient);
        glUniform1f(gMaterialShininessUniform, materialShinyness);
        glUniform3fv(gMaterialSpecularUniform, 1, (const GLfloat*) materialSpecular);
        glUniform3fv(gMaterialDiffuseUniform, 1, (const GLfloat*) materialDefuse);
    }

    glUniform1i(gIsPerFragmentShaderUniform, isLightPerFragmentShader);
    glUniform1i(gIsPerVertexShaderUniform, isLightPerVertexShader);
    glUniformMatrix4fv(gModelUniform, 1, GL_FALSE, (const float*) modelMatrix);
    glUniformMatrix4fv(gViewUniform, 1, GL_FALSE, (const float*) viewMatrix);
    glUniformMatrix4fv(gPerspectiveUniform, 1, GL_FALSE, (const GLfloat *) perspectiveProjectionMatrix);

    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);

    // till here
    glBindVertexArray(0);

    // update();

    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder
{
    //code
    [[self window]makeFirstResponder:self];
    return YES;
}

// WM_KEYDOWN
-(void)keyDown:(NSEvent *)theEvent
{
    //code
    int key = [[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        //ESC
        case 27:
            // self i.e. MyView
            [self release];
            // WM_DESTROY
            [NSApp terminate:self];
            break;
        case 'q':
        case 'Q':
            [[self window]toggleFullScreen:self];
            break;
        case 'l':
        case 'L':
            if (isLightOn == 1)
            {
                isLightOn = 0;
                isLightPerVertexShader = 0;
                isLightPerFragmentShader = 0;
            }
            else
            {
                isLightOn = 1;
            }
            break;
        case 'v':
        case 'V':
            if (isLightPerVertexShader == 1)
            {
                isLightPerVertexShader = 0;
                isLightOn = 0;
            }
            else
            {
                isLightPerVertexShader = 1;
                isLightPerFragmentShader = 0;
                isLightOn = 1;
            }
            break;
        case 'f':
        case 'F':
            if (isLightPerFragmentShader == 0)
            {
                isLightPerFragmentShader = 1;
                isLightPerVertexShader = 0;
                isLightOn = 1;
            }
            else
            {
                isLightPerFragmentShader = 0;
                isLightOn = 0;
            }
            break;
    }
}

// left mouse
-(void)mouseDown:(NSEvent *)theEvent
{
    //code
    
}

// right
-(void)rightMouseDown:(NSEvent *)theEvent
{
    //code
    
}

-(void)otherMouseDown:(NSEvent *)theEvent
{
    //code
    
}

-(void)dealloc
{
    //code

    // Unintialize();

    [self unintialize];

    [super dealloc];

    CVDisplayLinkStop(cvDisplayLinkRef);

    CVDisplayLinkRelease(cvDisplayLinkRef);
}

-(void)unintialize
{
    //code
    if (vao_sphere)
    {
        glDeleteVertexArrays(1, &vao_sphere);
        vao_sphere = 0;
    }

    // destroy vbo
    if (vbo_sphere_position)
    {
        glDeleteBuffers(1, &vbo_sphere_position);
        vbo_sphere_position = 0;
    }

    if (vbo_sphere_element)
    {
        glDeleteBuffers(1, &vbo_sphere_element);
        vbo_sphere_element = 0;
    }

    if (vbo_sphere_normal)
    {
        glDeleteBuffers(1, &vbo_sphere_normal);
        vbo_sphere_normal = 0;
    }

    // deatch vertex shader from shader program object
    glDetachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);

    // detach fragment shader from shader program obejct
    glDetachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);

    // delete vertex shader object
    glDeleteShader(gVertexShaderObjectPerFragment);

    gVertexShaderObjectPerFragment = 0; 

    // delete fragment shader object
    glDeleteShader(gFragmentShaderObjectPerFragment);
    gFragmentShaderObjectPerFragment = 0;

    // delete shader program object
    glDeleteProgram(gShaderProgramObjectPerFragment);
    gShaderProgramObjectPerFragment = 0;


    glDetachShader(gShaderProgramObjectPerVertex, gShaderProgramObjectPerVertex);
    glDetachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);

    glDeleteShader(gVertexShaderObjectPerVertex);
    gVertexShaderObjectPerVertex = 0;

    glDeleteShader(gFragmentShaderObjectPerVertex);
    gFragmentShaderObjectPerVertex = 0;

    // unlink shader program
    glUseProgram(0);

}


@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *now, const CVTimeStamp *outputTime, CVOptionFlags in,
                                CVOptionFlags *out, void *displayLinkContext)
 {
    //code
    CVReturn result = [(MyOpenGLView *)displayLinkContext getFrameForTime:outputTime];

    return result;
 }
