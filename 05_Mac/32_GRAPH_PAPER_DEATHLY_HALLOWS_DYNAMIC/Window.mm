//
//  window.m
//  
//
//  Created by user166446 on 7/8/21.
//

// stdio.h is inlcuded in Foundation.h 
#import <Foundation/Foundation.h> // similar to standard library
#import <Cocoa/Cocoa.h> // xlib.h windows.h
#import <QuartzCore/CVDisplayLink.h>     //core video
#import <OpenGL/GL3.h>      // gl.h

#import "vmath.h"

using namespace vmath;

// global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef , const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags , CVOptionFlags *, void *);

// global variable declarations
FILE *gpFile = NULL;

enum
{
    HRH_ATTRIBUTE_POSITION = 0 ,
    HRH_ATTRIBUTE_COLOR ,
    HRH_ATTRIBUTE_NORMAL ,
    HRH_ATTRIBUTE_TEXTURE_0
};

GLfloat color[] = { 0.0, 0.0, 1.0f,0.0, 0.0, 1.0f };
GLfloat vertices[] = { 1.0f, 0.0, 0.0f, -1.0f, 0.0f, 0.0f };

GLfloat radius = 0.0f;
GLfloat trianglePoint = 1.0f;
GLfloat y_final_position = trianglePoint;
GLfloat temp_for_triangle = 4.0f, temp_for_circle = 4.0f;

GLfloat transfer_x_triangle = 10.0f, transfer_y_triangle = 0.0f;
//GLfloat transfer_x_circle = -10.0f, transfer_y_circle = 0.0f;
GLfloat transfer_x_circle = -temp_for_circle * cos(208.0f * 0.01745329), transfer_y_circle = -(temp_for_circle * sin(208.0f * 0.01745329));

GLfloat rotateAngle = 0.01;
GLfloat linePoint = 5.0f;

const float TRANSFER_Z = - 7.0f;

// NS - NextSTEP
// Delegation performance <>
@interface AppDelegate:NSObject <NSApplicationDelegate,NSWindowDelegate>
@end

int main(int argc, char *argv[])
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    
    NSApp = [NSApplication sharedApplication];
    
    [NSApp setDelegate:[[AppDelegate alloc]init]];
    
    [NSApp run]; // run loop/ message loop/ event loop
    
    [pool release];
    
    return 0;
}

@interface MyOpenGLView:NSOpenGLView

@end

@implementation AppDelegate
{
    @private
    NSWindow *window ;
    MyOpenGLView *myOpenGLview;
}

// WM_CREATE
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // code
    // internally CGrect, core graphics , c library
    // first 2 variable together NSPoint , CGPoint
    // last 2 variable together NSSize structure , CGSzie

    NSBundle *appBundle = [NSBundle mainBundle];
    NSString *appDirPath = [appBundle bundlePath]; // /User/Hrituja/Desktop/RTR3.0/02_LogFile/window.app
    NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent]; // remove window.app give /User/Hrituja/Desktop/RTR3.0/02_LogFile
    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/log.txt", parentDirPath];
    const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    
    gpFile = fopen(pszLogFileNameWithPath, "w");
    if(gpFile == NULL)
    {
        [self release];
        [NSApp terminate:self];
        // end application 
    }
    
    fprintf(gpFile, "\nFile open successfully\n");

    NSRect win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);
    window = [[NSWindow alloc]initWithContentRect:win_rect
                                        styleMask:NSWindowStyleMaskTitled |
                                                NSWindowStyleMaskClosable |
                                                NSWindowStyleMaskMiniaturizable |
                                                NSWindowStyleMaskResizable
                                          backing:NSBackingStoreBuffered
                                           defer:NO
              ];
    [window setTitle:@"HRH: OGL"];
    [window center];
    
    myOpenGLview = [[MyOpenGLView alloc]initWithFrame:win_rect];
    
    [window setContentView:myOpenGLview];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

// WM_DESTTROY
-(void)applicationWillTerminate:(NSNotification *)aNotification
{
    //code

    if(gpFile)
    {
        fprintf(gpFile , "\nProgram terminated successfully\n");
        fclose(gpFile);
        gpFile = NULL;
    }
}

-(void)windowWillClose:(NSNotification *)aNotification
{
    //code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [myOpenGLview release];
    [window release];
    [super dealloc];

}
@end

@implementation MyOpenGLView
{
    @private
    CVDisplayLinkRef cvDisplayLinkRef;

	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;

	GLuint gVao_graph;
	GLuint gVbo_graph_position;
	GLuint gVbo_graph_color;

	GLuint gMVPUniform;

    mat4 gPerspectiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame
{
    //code
    self = [super initWithFrame:frame];

    if(self)
    {
        // pixel format descripor
        NSOpenGLPixelFormatAttribute attributes[] =
                                                    {
                                                        NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
                                                        NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
                                                        NSOpenGLPFANoRecovery,
                                                        NSOpenGLPFAAccelerated,
                                                        NSOpenGLPFAColorSize, 24,
                                                        NSOpenGLPFADepthSize, 24,
                                                        NSOpenGLPFAAlphaSize, 8,
                                                        NSOpenGLPFADoubleBuffer,
                                                        0
                                                    };
        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attributes]autorelease];
    
        if(pixelFormat == nil)
        {
            fprintf(gpFile, "\nPixel Format failed");
            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease]; // get pixell format

        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext];
    }



    return self;
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)output_time
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

    [self drawView];

    [pool release];

    return kCVReturnSuccess;
}

-(void)prepareOpenGL
{
    //code
    [super prepareOpenGL];

    [[self openGLContext]makeCurrentContext];

    // swap interval
    GLint swapInt = 1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];  // cp - context parameter

    
    // write shaders here
    if( [self loadShaders] == NO)
    {
       [self unintialize];
    }
    [self loadObjects];

    glClearColor(0.0f, 0.0f, 0.0f, 1.0);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    //glEnable(GL_CULL_FACE);

    gPerspectiveProjectionMatrix = mat4::identity();

    // core video and core graphics related code

    // 1. Create display link
    // 2. Set callback for display link
    // 3. NSContext -> CGL
    // 4. NSPixelFormat -> CGL
    // 5. Set current CGL context and CGL pixel format 
 

    CVDisplayLinkCreateWithActiveCGDisplays(&cvDisplayLinkRef);
    CVDisplayLinkSetOutputCallback(cvDisplayLinkRef, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(cvDisplayLinkRef, cglContext, cglPixelFormat);
    CVDisplayLinkStart(cvDisplayLinkRef);

}

-(BOOL)loadShaders
{
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    //provide souce code to shader
    const GLchar* vertexShaderSourceCode =
                                            "#version 410 core\n"\
                                            "in vec4 vPosition;" \
											"in vec4 vColor;" \
											"out vec4 out_color; " \
											"uniform mat4 u_mvpMatrix;" \
											"void main()"\
											"{" \
											"out_color = vColor;" \
											"gl_Position = u_mvpMatrix * vPosition;"\
											"}";

    glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

    // compile shader
    glCompileShader(gVertexShaderObject);

    // check compilation error
    GLint iInfoLogLength = 0;
    GLint iShaderCompileStatus = 0;
    char* szInfoLog = NULL;
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("Vertex shader compilation error : %s \n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    /***************** FRAGMENT SHADER **********************/

    // CREATE SHADER
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    //PROVIDE SOURCE CODE

    const char* fragmentShaderSourceCode =
                                            "#version 410 core\n"\
                                            "in vec4 out_color;" \
											"out vec4 FragColor; " \
											"void main()"\
											"{"\
											"FragColor = out_color;" \
											"}";
    

    glShaderSource(gFragmentShaderObject, 1, (const char**)&fragmentShaderSourceCode, NULL);

    // COMPILE SHADER
    glCompileShader(gFragmentShaderObject);

    // compilation error checking

    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("Fragment Shader Compilation Error : %s\n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    // SHADER PROGRAM
    //CREATE SHADER PROGRAM

    gShaderProgramObject = glCreateProgram();

    // attach vertex shader to shader program
    glAttachShader(gShaderProgramObject, gVertexShaderObject);

    // attch fragment shader to shader program
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);

    // pri-link binding of shader object with vertex shader possition attribute
    glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, HRH_ATTRIBUTE_COLOR, "vColor");

    // LINK PROGRAM
    glLinkProgram(gShaderProgramObject);

    // CHECK LINKING ERROR
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                printf("Shader Program Link Log : %s", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }
    
    // get MVP uniform location
    gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvpMatrix");
    
    return YES;
}

-(void)loadObjects
{
    glGenVertexArrays(1, &gVao_graph);
	glBindVertexArray(gVao_graph);

	// position
	glGenBuffers(1, &gVbo_graph_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position); //Bind gVbo
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), NULL, GL_DYNAMIC_DRAW); // feed the data #1	
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);	// kontya variable sathi, kiti elements, type #2	
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION); // #3
	glBindBuffer(GL_ARRAY_BUFFER, 0); // glEnd() for vbo

	// color
	glGenBuffers(1, &gVbo_graph_color); // Bind gVbo_color 
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(color), color, GL_DYNAMIC_DRAW); // #1
	glVertexAttribPointer(HRH_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL); // #2
	glEnableVertexAttribArray(HRH_ATTRIBUTE_COLOR); // #3
	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind Bind gVbo_color
	
	// unbind gVao
	glBindVertexArray(0); // unbind for gVao

}


-(void)reshape
{
    //code
    [super reshape];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    NSRect rect = [self bounds];
    if(rect.size.height <= 0)
    {
        rect.size.height = 1;
    }

    glViewport(0, 0, (GLsizei)rect.size.width, (GLsizei)rect.size.height);
    
    GLuint width = rect.size.width;
    GLuint height = rect.size.height;

    // projection

    // glORtho(left, right, bottom, top, near, far);
    gPerspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f);


    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(void)drawRect:(NSRect)dirty_rect
{ 
    //code
    [self drawView];
}

-(void)drawView
{
    //code

    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // start here 

    // START USING OpenGL program object
	glUseProgram(gShaderProgramObject);

	mat4 translateMatrix = vmath::translate(0.0f, 0.0f, TRANSFER_Z);
	mat4 rotateMatrix = vmath::rotate(rotateAngle, 0.0f, 1.0f, 0.0f);
	mat4 modelViewMatrix = translateMatrix * rotateMatrix;
	mat4 modelViewProjectionMatrix = mat4::identity();
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix; // ORDER IS IMPORTANT
	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glLineWidth(5.0f);

	[self line];

	// triangle
	translateMatrix = vmath::translate(-transfer_x_triangle, -transfer_y_triangle, TRANSFER_Z);
	rotateMatrix = vmath::rotate(rotateAngle, 0.0f, 1.0f, 0.0f);
	modelViewMatrix = translateMatrix * rotateMatrix;
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	[self triangle];

	// circle
	translateMatrix = vmath::translate(transfer_x_circle, -transfer_y_circle, TRANSFER_Z);
	rotateMatrix = vmath::rotate(rotateAngle, 0.0f, 1.0f, 0.0f);
	modelViewMatrix = translateMatrix * rotateMatrix;
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	
	[self circle];
	
	// stop using OpenGL program object
	glUseProgram(0);

    // till here

    // update();
    [self updateDraw];

    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(void)updateDraw
{
	static bool isTrianlgePlace = false, isCirclePlace = false, isRotate = true, lineAnimation = false;;
	if (temp_for_triangle * cos(208.0f * 0.01745329) < 0)
	{		
		transfer_x_triangle = -temp_for_triangle * cos(208.0f * 0.01745329f);
		transfer_y_triangle = -temp_for_triangle * sin(208.0f * 0.01745329f);
		temp_for_triangle -= 0.003f;
	}
	else
	{
		isTrianlgePlace = true;
	}
	if(isTrianlgePlace == true && (temp_for_circle * cos(208.0f * 0.01745329) < 0.0))
	{
		transfer_x_circle = -temp_for_circle * cos(208.0f * 0.01745329f);
		transfer_y_circle = -temp_for_circle * sin(208.0f * 0.01745329f);
		temp_for_circle -= 0.003f;
		if (temp_for_circle * cos(208.0f * 0.01745329) > 0.0)
		{
			isCirclePlace = true;
			lineAnimation = true;
		}
		//fprintf(gpFile, "\ntemp = %f, transfer_x = %f , transfer_y = %f", temp, transfer_x, transfer_y);
	}
	if (isRotate == true)
	{
		rotateAngle += 0.2f;
		if (rotateAngle > 360.0f)
		{
			rotateAngle = 0.0f;
		}
	}
	
	if (lineAnimation == true)
	{
		if (linePoint > 1)
			linePoint -= 0.003f;
		else
		{
			if (rotateAngle < 360.0f )
            {
				rotateAngle += 0.2f;
				isRotate = false;
			}
		}
	}
}

-(void)line
{
    GLfloat line[] =  { 
		0.0f, linePoint, 0.0f ,
		0.0f, linePoint - 2.0f, 0.0f
	};

	GLfloat lineColor[] = {
		0.0f, 1.0f, 0.0,
		0.0f, 1.0f, 0.0f,
	};

	glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(lineColor), lineColor, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(line), line, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(gVao_graph);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

}

-(void)circle
{
    // FUNCTION DECLARATION
    
	// CODE
	GLfloat circle_points = 1500, angle, radius ;
	GLfloat length = 0.0f, a = 0.0f, b = 0.0f, c = 0.0f, s = 0.0f;

	GLfloat circleLines[] = {
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f
	};
	GLfloat rectnagleColor[] = {
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f
	};

    a = [self getLengthFromVertises:trianglePoint :-trianglePoint :-trianglePoint :-trianglePoint];
    b = [self getLengthFromVertises:-trianglePoint :-trianglePoint :0 :trianglePoint];
    c = [self getLengthFromVertises:trianglePoint :-trianglePoint :0 :trianglePoint];
    
	s = (a + b + c) / 2;
	radius = sqrt(s * (s - a) * (s - b) * (s - c)) / s;


	for (int i = 1; i < circle_points; i++) {
		angle = (i - 1) * 0.01745329;
		circleLines[0] = radius * cos(angle);
		circleLines[1] = -0.38f + (radius * sin(angle));
		circleLines[2] = 0.0f;
		angle = i * 0.01745329;
		circleLines[3] = radius * cos(angle);
		circleLines[4] = -0.38f + (radius * sin(angle));
		circleLines[5] = 0.0f;

		glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_color);
		glBufferData(GL_ARRAY_BUFFER, sizeof(rectnagleColor), rectnagleColor, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
		glBufferData(GL_ARRAY_BUFFER, sizeof(circleLines), circleLines, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(gVao_graph);
		glDrawArrays(GL_LINES, 0, 2);
		glBindVertexArray(0);

	}
}

-(GLfloat)getLengthFromVertises:(GLfloat)x1 :(GLfloat)y1 :(GLfloat)x2 :(GLfloat)y2
{
    // eqaution for calculating legth between 2 points
    return sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
}

-(void)triangle
{
    // VARIABLE DECLARATION
	GLfloat triangle[] = {
		0.0f, trianglePoint, 0.0f,
		-trianglePoint, -trianglePoint, 0.0f,
		trianglePoint, -trianglePoint, 0.0f,
		0.0f, trianglePoint, 0.0f 
	};

	GLfloat tringleColor[] = {
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f
	};

	// CODE
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(tringleColor), tringleColor, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, gVbo_graph_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(gVao_graph);
	glDrawArrays(GL_LINES, 0, 2);
	glDrawArrays(GL_LINES, 1, 2);
	glDrawArrays(GL_LINES, 2, 2);
	glBindVertexArray(0);
    
}

-(BOOL)acceptsFirstResponder
{
    //code
    [[self window]makeFirstResponder:self];
    return YES;
}

// WM_KEYDOWN
-(void)keyDown:(NSEvent *)theEvent
{
    //code
    int key = [[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        //ESC
        case 27:
            // self i.e. MyView
            [self release];
            // WM_DESTROY
            [NSApp terminate:self];
            break;
        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];
            break;
    }
}

// left mouse
-(void)mouseDown:(NSEvent *)theEvent
{
    //code
    
}

// right
-(void)rightMouseDown:(NSEvent *)theEvent
{
    //code
    
}

-(void)otherMouseDown:(NSEvent *)theEvent
{
    //code
    
}

-(void)dealloc
{
    //code

    // Unintialize();

    [self unintialize];

    [super dealloc];

    CVDisplayLinkStop(cvDisplayLinkRef);

    CVDisplayLinkRelease(cvDisplayLinkRef);
}

-(void)unintialize
{
    //code
    
	// destroy gVao_triangle
	if (gVao_graph)
	{
		glDeleteVertexArrays(1, &gVao_graph);
		gVao_graph = 0;
	}

	if (gVbo_graph_color)
	{
		glDeleteBuffers(1, &gVbo_graph_color);
		gVbo_graph_color = 0;
	}

	if (gVbo_graph_position)
	{
		glDeleteBuffers(1, &gVbo_graph_position);
		gVbo_graph_position = 0;
	}

    // deatch vertex shader from shader program object
    glDetachShader(gShaderProgramObject, gVertexShaderObject);

    // detach fragment shader from shader program obejct
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);

    // delete vertex shader object
    glDeleteShader(gVertexShaderObject);

    gVertexShaderObject = 0;

    // delete fragment shader object
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject = 0;

    // delete shader program object
    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject = 0;

    // unlink shader program
    glUseProgram(0);
}


@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *now, const CVTimeStamp *outputTime, CVOptionFlags in,
                                CVOptionFlags *out, void *displayLinkContext)
 {
    //code
    CVReturn result = [(MyOpenGLView *)displayLinkContext getFrameForTime:outputTime];

    return result;
 }
