//
//  window.m
//  
//
//  Created by user166446 on 7/8/21.
//

// stdio.h is inlcuded in Foundation.h 
#import <Foundation/Foundation.h> // similar to standard library
#import <Cocoa/Cocoa.h> // xlib.h windows.h
#import <QuartzCore/CVDisplayLink.h>     //core video
#import <OpenGL/GL3.h>      // gl.h

#import "vmath.h"

using namespace vmath;

// global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef , const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags , CVOptionFlags *, void *);

void CopyArray(GLfloat input[], GLfloat output[]);

// global variable declarations
FILE *gpFile = NULL;

const int CHECK_IMAGE_WIDTH = 64, CHECK_IMAGE_HIGHT = 64;

enum
{
    HRH_ATTRIBUTE_POSITION = 0 ,
    HRH_ATTRIBUTE_COLOR ,
    HRH_ATTRIBUTE_NORMAL ,
    HRH_ATTRIBUTE_TEXTURE_0
};

// NS - NextSTEP
// Delegation performance <>
@interface AppDelegate:NSObject <NSApplicationDelegate,NSWindowDelegate>
@end

int main(int argc, char *argv[])
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    
    NSApp = [NSApplication sharedApplication];
    
    [NSApp setDelegate:[[AppDelegate alloc]init]];
    
    [NSApp run]; // run loop/ message loop/ event loop
    
    [pool release];
    
    return 0;
}

@interface MyOpenGLView:NSOpenGLView

@end

@implementation AppDelegate
{
    @private
    NSWindow *window ;
    MyOpenGLView *myOpenGLview;
}

// WM_CREATE
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // code
    // internally CGrect, core graphics , c library
    // first 2 variable together NSPoint , CGPoint
    // last 2 variable together NSSize structure , CGSzie

    NSBundle *appBundle = [NSBundle mainBundle];
    NSString *appDirPath = [appBundle bundlePath]; // /User/Hrituja/Desktop/RTR3.0/02_LogFile/window.app
    NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent]; // remove window.app give /User/Hrituja/Desktop/RTR3.0/02_LogFile
    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/log.txt", parentDirPath];
    const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    
    gpFile = fopen(pszLogFileNameWithPath, "w");
    if(gpFile == NULL)
    {
        [self release];
        [NSApp terminate:self];
        // end application 
    }
    
    fprintf(gpFile, "\nFile open successfully\n");

    NSRect win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);
    window = [[NSWindow alloc]initWithContentRect:win_rect
                                        styleMask:NSWindowStyleMaskTitled |
                                                NSWindowStyleMaskClosable |
                                                NSWindowStyleMaskMiniaturizable |
                                                NSWindowStyleMaskResizable
                                          backing:NSBackingStoreBuffered
                                           defer:NO
              ];
    [window setTitle:@"HRH: OGL"];
    [window center];
    
    myOpenGLview = [[MyOpenGLView alloc]initWithFrame:win_rect];
    
    [window setContentView:myOpenGLview];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

// WM_DESTTROY
-(void)applicationWillTerminate:(NSNotification *)aNotification
{
    //code

    if(gpFile)
    {
        fprintf(gpFile , "\nProgram terminated successfully\n");
        fclose(gpFile);
        gpFile = NULL;
    }
}

-(void)windowWillClose:(NSNotification *)aNotification
{
    //code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [myOpenGLview release];
    [window release];
    [super dealloc];

}
@end

@implementation MyOpenGLView
{
    @private
    CVDisplayLinkRef cvDisplayLinkRef;

    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;

    GLuint vao_square;
    GLuint vbo_square_position;
    GLuint vbo_square_texcoord;
    GLuint mvpUniform;

    GLuint texture;
    GLuint textureSamplerUniform;
    GLuint isColorUniform;

    mat4 perspectiveProjectionMatrix;

   GLubyte checkImage[CHECK_IMAGE_HIGHT][CHECK_IMAGE_WIDTH][4];
	
	GLuint isColor ;
}

-(id)initWithFrame:(NSRect)frame
{
    //code
    self = [super initWithFrame:frame];

    if(self)
    {
        // pixel format descripor
        NSOpenGLPixelFormatAttribute attributes[] =
                                                    {
                                                        NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
                                                        NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
                                                        NSOpenGLPFANoRecovery,
                                                        NSOpenGLPFAAccelerated,
                                                        NSOpenGLPFAColorSize, 24,
                                                        NSOpenGLPFADepthSize, 24,
                                                        NSOpenGLPFAAlphaSize, 8,
                                                        NSOpenGLPFADoubleBuffer,
                                                        0
                                                    };
        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attributes]autorelease];
    
        if(pixelFormat == nil)
        {
            fprintf(gpFile, "\nPixel Format failed");
            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease]; // get pixell format

        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext];
                
    }



    return self;
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)output_time
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

    [self drawView];

    [pool release];

    return kCVReturnSuccess;
}

-(void)prepareOpenGL
{
    //code
    [super prepareOpenGL];

    [[self openGLContext]makeCurrentContext];

    // swap interval
    GLint swapInt = 1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];  // cp - context parameter

    
    // write shaders here
    if( [self loadShaders] == NO)
    {
       [self unintialize];
    }
    
    [self loadObjects];

    glClearColor(0.0f, 0.0f, 0.0f, 1.0);
    //glShadeModel(GL_SMOOTH);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    //glEnable(GL_CULL_FACE);

    texture = [self loadTextureFromBmpFile:"Smiley.bmp"];

    if(texture == 0)
    {
    	[self unintialize];
    }
    
    fprintf(gpFile, "\nGot Texture");

    perspectiveProjectionMatrix = mat4::identity();

    // core video and core graphics related code

    // 1. Create display link
    // 2. Set callback for display link
    // 3. NSContext -> CGL
    // 4. NSPixelFormat -> CGL
    // 5. Set current CGL context and CGL pixel format 
 

    CVDisplayLinkCreateWithActiveCGDisplays(&cvDisplayLinkRef);
    CVDisplayLinkSetOutputCallback(cvDisplayLinkRef, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(cvDisplayLinkRef, cglContext, cglPixelFormat);
    CVDisplayLinkStart(cvDisplayLinkRef);

}

-(void)makeCheckImage:
{
	int i, j, c;

	for (int i = 0; i < CHECK_IMAGE_HIGHT; i++)
	{
		for (int j = 0; j < CHECK_IMAGE_WIDTH; j++)
		{
			c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;
			checkImage[i][j][0] = (GLubyte)c;
			checkImage[i][j][1] = (GLubyte)c;
			checkImage[i][j][2] = (GLubyte)c;
			checkImage[i][j][3] = (GLubyte)255;
		}
	}
}

-(GLuint)loadTextureFromBmpFile:(const char *)imageFileName
{
	// code
    GLuint tex = 0;

    [self makeCheckImage];

	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1); //IN FFP 4

	//setting texture parameter
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	// following call will actually push the data with help of graphics driver
	//gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, CHECK_IMAGE_WIDTH, CHECK_IMAGE_HIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, checkImage);	
	
	glTexEnf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    
    glBindTexture(GL_TEXTURE_2D, 0);

	CFRelease(imageData);

	return tex;
}

-(BOOL)loadShaders
{
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    //provide souce code to shader
    const GLchar* vertexShaderSourceCode =  "#version 410 core\n"\
											"in vec4 vPosition;" \
											"in vec2 vTexCoord;" \
											"out vec2 outTexCoord; " \
											"uniform mat4 u_mvpMatrix;" \
											"void main()"\
											"{" \
											"outTexCoord = vTexCoord;" \
											"gl_Position = u_mvpMatrix * vPosition;"\
											"}";

    glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

    // compile shader
    glCompileShader(vertexShaderObject);

    // check compilation error
    GLint iInfoLogLength = 0;
    GLint iShaderCompileStatus = 0;
    char* szInfoLog = NULL;
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("Vertex shader compilation error : %s \n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    /***************** FRAGMENT SHADER **********************/

    // CREATE SHADER
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    //PROVIDE SOURCE CODE

    const char* fragmentShaderSourceCode = "#version 410 core\n"\
											"in vec2 outTexCoord;" \
											"out vec4 FragColor; " \
											"uniform sampler2D u_texture_sampler;" \
											"void main()" \
											"{" \
											"	FragColor = texture(u_texture_sampler,outTexCoord);" \											
											"}";
    

    glShaderSource(fragmentShaderObject, 1, (const char**)&fragmentShaderSourceCode, NULL);

    // COMPILE SHADER
    glCompileShader(fragmentShaderObject);

    // compilation error checking

    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("Fragment Shader Compilation Error : %s\n", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }

    // SHADER PROGRAM
    //CREATE SHADER PROGRAM

    shaderProgramObject = glCreateProgram();

    // attach vertex shader to shader program
    glAttachShader(shaderProgramObject, vertexShaderObject);

    // attch fragment shader to shader program
    glAttachShader(shaderProgramObject, fragmentShaderObject);

    // pri-link binding of shader object with vertex shader possition attribute
    glBindAttribLocation(shaderProgramObject, HRH_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(shaderProgramObject, HRH_ATTRIBUTE_TEXTURE_0, "vTexCoord");

    // LINK PROGRAM
    glLinkProgram(shaderProgramObject);

    // CHECK LINKING ERROR
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                printf("Shader Program Link Log : %s", szInfoLog);
                free(szInfoLog);
                return NO;
            }
        }
    }
    
    // get MVP uniform location
    mvpUniform = glGetUniformLocation(shaderProgramObject, "u_mvpMatrix");
	textureSamplerUniform = glGetUniformLocation(shaderProgramObject, "u_texture_sampler");
    
    return YES;
}

-(void)loadObjects
{
    GLfloat fTexCoordArray[] = {0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f};

	glGenVertexArrays(1, &vao_square);
	glBindVertexArray(vao_square);

	glGenBuffers(1, &vbo_square_position); // bind vbo_square_position to buffer
	glBindBuffer(GL_ARRAY_BUFFER ,vbo_square_position);
	glBufferData(GL_ARRAY_BUFFER, 4 * 3 * sizeof(GLfloat), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind vbo_square_position
	
	glGenBuffers(1, &vbo_square_texcoord);  // bind vbo_square_texcoord
	glBindBuffer(GL_ARRAY_BUFFER, vbo_square_texcoord);
	glBufferData(GL_ARRAY_BUFFER, 4 * 2 * sizeof(GLfloat), fTexCoordArray, GL_STATIC_DRAW); 
	glVertexAttribPointer(HRH_ATTRIBUTE_TEXTURE_0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_TEXTURE_0);	 
	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind vbo_square_texcoord

	glBindVertexArray(0);

}

-(void)reshape
{
    //code
    [super reshape];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    NSRect rect = [self bounds];
    if(rect.size.height <= 0)
    {
        rect.size.height = 1;
    }

    glViewport(0, 0, (GLsizei)rect.size.width, (GLsizei)rect.size.height);
    
    GLuint width = rect.size.width;
    GLuint height = rect.size.height;

    // projection

    // glORtho(left, right, bottom, top, near, far);
    perspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)width / height), ((GLfloat)height / width), 100.0f); 


    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(void)drawRect:(NSRect)dirty_rect
{ 
    //code
    [self drawView];
}

-(void)drawView
{
    //code
    
   GLfloat fVertexArray_0[] = {
						 0.0f,  1.0f,  0.0f,
						 -2.0f,  1.0f,  0.0f,
						-2.0f,  -1.0f, 0.0f,
						 0.0f, -1.0f,  0.0f
					};

	GLfloat fVertexArray_1[] = {
		2.41421f, 1.0f,  -1.41421,
		1.0f,     1.0f,   0.0f,
		1.0f,     -1.0f,  0.0f,
		2.41421f, -1.0f, -1.41421,
	};
    
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // start from here
    glUseProgram(shaderProgramObject);
	mat4 modelViewMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
	mat4 modelViewProjectionMatrix = mat4::identity();
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	glUniform1i(textureSamplerUniform, 0);

	glBindBuffer(GL_ARRAY_BUFFER ,vbo_square_position);
	glBufferData(GL_ARRAY_BUFFER, 4 * 3 * sizeof(GLfloat), fVertexArray_0, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0); 
	
	glBindVertexArray(vao_square);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

	//second board

	glBindBuffer(GL_ARRAY_BUFFER ,vbo_square_position);
	glBufferData(GL_ARRAY_BUFFER, 4 * 3 * sizeof(GLfloat), fVertexArray_1, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(HRH_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(HRH_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0); 
	
	glBindVertexArray(vao_square);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);
    
	glUseProgram(0);
    
    // update();

    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder
{
    //code
    [[self window]makeFirstResponder:self];
    return YES;
}

// WM_KEYDOWN
-(void)keyDown:(NSEvent *)theEvent
{
    //code
    int key = [[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        //ESC
        case 27:
            // self i.e. MyView
            [self release];
            // WM_DESTROY
            [NSApp terminate:self];
            break;
        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];
            break;
        case '0':
			isColor = 0;
            break;
		case '1':
			isColor = 1;
			break;
		case '2':
			isColor = 2;
			break;
		case '3':
			isColor = 3;
			break;
        case '4':
            isColor = 4;
            break;
    }
}

// left mouse
-(void)mouseDown:(NSEvent *)theEvent
{
    //code
    
}

// right
-(void)rightMouseDown:(NSEvent *)theEvent
{
    //code
    
}

-(void)otherMouseDown:(NSEvent *)theEvent
{
    //code
    
}

-(void)dealloc
{
    //code

    // Unintialize();

    [self unintialize];

    [super dealloc];

    CVDisplayLinkStop(cvDisplayLinkRef);

    CVDisplayLinkRelease(cvDisplayLinkRef);
}

-(void)unintialize
{
    //code
	if(vao_square)
	{
		glDeleteVertexArrays(1, &vao_square);
		vao_square = 0;
	}

	if (vbo_square_position)
	{
		glDeleteBuffers(1, &vbo_square_position);
		vbo_square_position = 0;
	}

	if (vbo_square_texcoord)
	{
		glDeleteBuffers(1, &vbo_square_texcoord);
		vbo_square_texcoord = 0;
	}

	if (texture)
	{
		glDeleteTextures(1, &texture);
		texture = 0;
	}

    // deatch vertex shader from shader program object
    glDetachShader(shaderProgramObject, vertexShaderObject);

    // detach fragment shader from shader program obejct
    glDetachShader(shaderProgramObject, fragmentShaderObject);

    // delete vertex shader object
    glDeleteShader(vertexShaderObject);

    vertexShaderObject = 0;

    // delete fragment shader object
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;

    // delete shader program object
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;

    // unlink shader program
    glUseProgram(0);

}


@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *now, const CVTimeStamp *outputTime, CVOptionFlags in,
                                CVOptionFlags *out, void *displayLinkContext)
 {
    //code
    CVReturn result = [(MyOpenGLView *)displayLinkContext getFrameForTime:outputTime];

    return result;
 }

void CopyArray(GLfloat input[], GLfloat output[])
{
	for (int i = 0; i < 8; i++)
	{
		output[i] = input[i];
	}
}
