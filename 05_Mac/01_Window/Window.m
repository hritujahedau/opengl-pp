//
//  window.m
//  
//
//  Created by user166446 on 7/8/21.
//

#import <Foundation/Foundation.h> // similar to standard library
#import <Cocoa/Cocoa.h> // xlib.h windows.h

// NS - NextSTEP
// Delegation performance <>
@interface AppDelegate:NSObject <NSApplicationDelegate,NSWindowDelegate>
@end

int main(int argc, char *argv[])
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    
    NSApp = [NSApplication sharedApplication];
    
    [NSApp setDelegate:[[AppDelegate alloc]init]];
    
    [NSApp run]; // run loop/ message loop/ event loop
    
    [pool release];
    
    return 0;
}

@interface MyView:NSView

@end

@implementation AppDelegate
{
    @private
    NSWindow *window ;
    MyView *view;
}

// WM_CREATE
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // code
    // internally CGrect, core graphics , c library
    // first 2 variable together NSPoint , CGPoint
    // last 2 variable together NSSize structure , CGSzie
    NSRect win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);
    window = [[NSWindow alloc]initWithContentRect:win_rect
                                        styleMask:NSWindowStyleMaskTitled |
                                                NSWindowStyleMaskClosable |
                                                NSWindowStyleMaskMiniaturizable |
                                                NSWindowStyleMaskResizable
                                          backing:NSBackingStoreBuffered
                                           defer:NO
              ];
    [window setTitle:@"HRH: Mac OS Window"];
    [window center];
    
    
    view = [[MyView alloc]initWithFrame:win_rect];
    
    [window setContentView:view];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

// WM_DESTTROY
-(void)applicationWillTerminate:(NSNotification *)aNotification
{
    //code
}

-(void)windowWillClose:(NSNotification *)aNotification
{
    //code
    [NSApp terminate:self];
}

-(void)dealloc
{
    //code
    [view release];
    [window release];
    [super dealloc];

}
@end

@implementation MyView
{
    @private
    NSString *centralText;
}

-(id)initWithFrame:(NSRect)frame
{
    //code
    self = [super initWithFrame:frame];
    
    if(self)
    {
        centralText=@"Hello World";
    }
    return self;
}

-(void)drawRect:(NSRect)dirty_rect
{
    NSColor *backgroundColor = [NSColor blackColor];
    [backgroundColor set];
    NSRectFill(dirty_rect);
    
    NSDictionary *dictionaryForTextAttribute = [NSDictionary dictionaryWithObjectsAndKeys:
                                                [NSFont fontWithName:@"Helvetica" size:32],  NSFontAttributeName,
                                                [NSColor greenColor], NSForegroundColorAttributeName,
                                                nil
                                                ];
    NSSize textSize = [centralText sizeWithAttributes:dictionaryForTextAttribute];
    
    NSPoint point;
    point.x = (dirty_rect.size.width/2) - (textSize.width/2);
    point.y = (dirty_rect.size.height/2) - (textSize.height/2) + 12;

    [centralText drawAtPoint:point withAttributes:dictionaryForTextAttribute];
}

-(BOOL)acceptsFirstResponder
{
    //code
    [[self window]makeFirstResponder:self];
    return YES;
}

// WM_KEYDOWN
-(void)keyDown:(NSEvent *)theEvent
{
    //code
    int key = [[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        //ESC
        case 27:
            // self i.e. MyView
            [self release];
            // WM_DESTROY
            [NSApp terminate:self];
            break;
        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];
            break;
    }
}

// left mouse
-(void)mouseDown:(NSEvent *)theEvent
{
    //code
    centralText = @"Left mouse button is clicked";
    // call invalid rect
    [self setNeedsDisplay:YES];
}

// right
-(void)rightMouseDown:(NSEvent *)theEvent
{
    //code
    centralText = @"Right mouse button is clicked";
    // call invalid rect
    [self setNeedsDisplay:YES];
}

-(void)otherMouseDown:(NSEvent *)theEvent
{
    //code
    centralText = @"Other mouse button is clicked";
    // call invalid rect
    [self setNeedsDisplay:YES];
}

//-(void)dealloc
//{
//    //code
//    [super dealloc];
//}

@end
